﻿using System;
using System.Linq;

namespace NexCore.Legal
{
    public class LegalPersonName : BasePersonName
    {
        private string _short;

        private LegalPersonName()
        {
        }

        public LegalPersonName(string firstname, string lastname, string middlename)
            : base(firstname, lastname, middlename)
        {
        }

        protected override void Validate(string firstname, string lastname, string middlename)
        {
            if (string.IsNullOrWhiteSpace(firstname))
                throw new ArgumentException("Value cannot be null or empty.", nameof(firstname));
            if (string.IsNullOrWhiteSpace(lastname))
                throw new ArgumentException("Value cannot be null or empty.", nameof(lastname));
            if (string.IsNullOrWhiteSpace(middlename))
                throw new ArgumentException("Value cannot be null or empty.", nameof(middlename));
        }

        public static LegalPersonName Empty => new LegalPersonName();

        public string ShortName => _short ??
            (_short = GetValues().Any(v => v == null) ? null : $"{Firstname.First()}.{Middlename.First()}.{Lastname}");

        public void Assign(LegalPersonName name)
        {
            Firstname = name.Firstname;
            Lastname = name.Lastname;
            Middlename = name.Middlename;

            FullFirstNameFirst = $"{Firstname} {Lastname} {Middlename}".Replace("  ", " ").Trim();
            FullLastNameFirst = $"{Lastname} {Firstname} {Middlename}".Replace("  ", " ").Trim();
        }
    }
}