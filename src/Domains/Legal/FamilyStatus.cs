﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Legal
{
    public class FamilyStatus
    {
        public int Id { get; }

        public string Name { get; }

        public static readonly FamilyStatus Single = new FamilyStatus(1, "Single");

        public static readonly FamilyStatus Divorced = new FamilyStatus(2, "Divorced");

        public static readonly FamilyStatus Married = new FamilyStatus(3, "Married");

        public static readonly FamilyStatus CivilMarriage = new FamilyStatus(4, "CivilMarriage");

        public static IEnumerable<FamilyStatus> All => Items.ToList().AsReadOnly();

        private static readonly IEnumerable<FamilyStatus> Items = new List<FamilyStatus>
        {
            Single,
            Divorced,
            Married,
            CivilMarriage
        };

        private FamilyStatus(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public static FamilyStatus Get(int id)
        {
            return Has(id) ? All.ElementAt(id - 1) : throw new ArgumentOutOfRangeException();
        }

        public static bool Has(int id)
        {
            return All.Select(i => i.Id).Contains(id);
        }

        public bool Equals(FamilyStatus other)
        {
            return other != null && Id == other.Id;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(GetType(), Id);
        }

        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
