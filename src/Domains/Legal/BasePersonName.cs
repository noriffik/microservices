﻿using NexCore.Domain;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Legal
{
    public abstract class BasePersonName : ValueObject
    {
        protected string FullFirstNameFirst;
        protected string FullLastNameFirst;

        public string Firstname { get; protected set; }

        public string Lastname { get; protected set; }

        public string Middlename { get; protected set; }

        protected BasePersonName()
        {
        }

        protected BasePersonName(string firstname, string lastname, string middlename) : this()
        {
            Validate(firstname, lastname, middlename);

            Firstname = NormalizeName(firstname);
            Lastname = NormalizeName(lastname);
            Middlename = NormalizeName(middlename);

            FullFirstNameFirst = $"{Firstname} {Lastname} {Middlename}".Replace("  ", " ").Trim();
            FullLastNameFirst = $"{Lastname} {Firstname} {Middlename}".Replace("  ", " ").Trim();
        }

        protected abstract void Validate(string firstname, string lastname, string middlename);

        protected static string NormalizeName(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return null;

            var trimmed = value.Trim();
            return trimmed.First().ToString().ToUpperInvariant() + trimmed.Substring(1).ToLowerInvariant();
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return Firstname;
            yield return Lastname;
            yield return Middlename;
        }

        public override string ToString() => FullLastNameFirst;
    }
}
