﻿using NexCore.Domain;
using System;
using System.Collections.Generic;

namespace NexCore.Legal
{
    public class Passport : ValueObject
    {
        public string Series { get; private set; }

        public string Number { get; private set; }

        public Issue Issue { get; private set; }

        private Passport()
        {
            Issue = Issue.Empty;
        }

        public Passport(string series, string number, Issue issue)
        {
            Series = series?.ToUpperInvariant() ?? throw new ArgumentNullException(nameof(series));
            Number = number?.ToUpperInvariant() ?? throw new ArgumentNullException(nameof(number));
            Issue = issue ?? throw new ArgumentNullException(nameof(issue));
        }

        public static Passport Empty => new Passport();

        protected override IEnumerable<object> GetValues()
        {
            yield return Series;
            yield return Number;
            yield return Issue;
        }

        public void Assign(Passport passport)
        {
            if (passport == null)
                throw new ArgumentNullException(nameof(passport));

            Series = passport.Series;
            Number = passport.Number;
            Issue.Assign(passport.Issue);
        }
    }
}
