﻿using NexCore.Domain;
using System;
using System.Collections.Generic;

namespace NexCore.Legal
{
    public class LegalOrganizationName : ValueObject
    {
        public string ShortName { get; private set; }

        public string FullName { get; private set; }

        private LegalOrganizationName()
        {
        }

        public LegalOrganizationName(string shortName, string fullName)
        {
            if (string.IsNullOrWhiteSpace(shortName))
                throw new ArgumentException("Value cannot be null or empty.", nameof(shortName));
            if (string.IsNullOrWhiteSpace(fullName))
                throw new ArgumentException("Value cannot be null or empty.", nameof(fullName));

            ShortName = shortName;
            FullName = fullName;
        }

        public static LegalOrganizationName Empty => new LegalOrganizationName();

        public void Assign(LegalOrganizationName name)
        {
            ShortName = name.ShortName;
            FullName = name.FullName;
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return ShortName;
            yield return FullName;
        }
    }
}
