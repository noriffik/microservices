﻿using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Legal
{
    public class OrganizationRequisites : ValueObject
    {
        public LegalOrganizationName Name { get; private set; }

        public string Address { get; private set; }

        public string BankRequisites { get; private set; }

        public IEnumerable<string> Telephones { get; private set; }

        public LegalPersonName Director { get; private set; }

        private OrganizationRequisites()
        {
            Name = LegalOrganizationName.Empty;
            Director = LegalPersonName.Empty;
            Telephones = new List<string>();
        }

        public OrganizationRequisites(LegalOrganizationName name, string address, IEnumerable<string> telephones, string bankRequisites, LegalPersonName director)
        {
            if (name == null || name == LegalOrganizationName.Empty)
                throw new ArgumentException("Value cannot be null or empty.", nameof(name));
            if (string.IsNullOrWhiteSpace(address))
                throw new ArgumentException("Value cannot be null or empty.", nameof(address));
            if (string.IsNullOrWhiteSpace(bankRequisites))
                throw new ArgumentException("Value cannot be null or empty.", nameof(bankRequisites));
            if (telephones == null)
                throw new ArgumentNullException(nameof(telephones));
            if (director == null || director == LegalPersonName.Empty)
                throw new ArgumentException("Value cannot be null or empty.", nameof(director));

            var telephonesList = telephones.Distinct().ToList();
            if (telephonesList.Any(string.IsNullOrWhiteSpace))
                throw new ArgumentException("Telephone cannot be null or empty");

            Name = name;
            Address = address;
            Telephones = telephonesList;
            BankRequisites = bankRequisites;
            Director = director;
        }

        public static OrganizationRequisites Empty => new OrganizationRequisites();

        public void Assign(OrganizationRequisites requisites)
        {
            Name.Assign(requisites.Name);
            Address = requisites.Address;
            BankRequisites = requisites.BankRequisites;
            Telephones = requisites.Telephones;
            Director.Assign(requisites.Director);
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return Name;
            yield return Address;
            yield return Telephones;
            yield return BankRequisites;
            yield return Director;
        }
    }
}
