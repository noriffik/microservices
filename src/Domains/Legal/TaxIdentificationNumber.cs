﻿using NexCore.Domain;
using System;
using System.Collections.Generic;

namespace NexCore.Legal
{
    public class TaxIdentificationNumber : ValueObject
    {
        public string Number { get; private set; }

        public DateTime? RegisteredOn { get; private set; }

        public Issue Issue { get; private set; }

        private TaxIdentificationNumber()
        {
            Issue = Issue.Empty;
        }

        public TaxIdentificationNumber(string number, DateTime? registeredOn, Issue issue)
        {
            if (string.IsNullOrWhiteSpace(number))
                throw new ArgumentException("Value cannot be null or empty", nameof(number));

            Number = number;
            RegisteredOn = registeredOn?.Date;
            Issue = issue ?? throw new ArgumentNullException(nameof(issue));
        }

        public static TaxIdentificationNumber Empty => new TaxIdentificationNumber();

        public void Assign(TaxIdentificationNumber number)
        {
            Number = number.Number;
            RegisteredOn = number.RegisteredOn;
            Issue.Assign(number.Issue);
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return Number;
            yield return RegisteredOn;
            yield return Issue;
        }
    }
}
