﻿using System;

namespace NexCore.Legal
{
    public class PersonName : BasePersonName
    {
        private PersonName()
        {
        }

        public PersonName(string firstname, string lastname, string middlename)
            : base(firstname, lastname, middlename)
        {
        }

        protected override void Validate(string firstname, string lastname, string middlename)
        {
            if (string.IsNullOrWhiteSpace(firstname) && string.IsNullOrWhiteSpace(lastname) && string.IsNullOrWhiteSpace(middlename))
                throw new ArgumentException("Value cannot be null or empty.");
        }

        public static PersonName Empty => new PersonName();

        public virtual void Assign(PersonName name)
        {
            Firstname = name.Firstname;
            Middlename = name.Middlename;
            Lastname = name.Lastname;

            FullFirstNameFirst = $"{Firstname} {Lastname} {Middlename}".Replace("  ", " ").Trim();
            FullLastNameFirst = $"{Lastname} {Firstname} {Middlename}".Replace("  ", " ").Trim();
        }
    }
}