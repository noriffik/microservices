﻿using NexCore.Domain;
using System;
using System.Collections.Generic;

namespace NexCore.Legal
{
    public class Issue : ValueObject
    {
        public string Issuer { get; private set; }

        public DateTime? IssuedOn { get; private set; }

        private Issue()
        {
        }

        public Issue(string issuer, DateTime issuedOn)
        {
            if (string.IsNullOrWhiteSpace(issuer))
                throw new ArgumentException("Value cannot be null or empty", nameof(issuer));

            Issuer = issuer;
            IssuedOn = issuedOn.Date;
        }

        public static Issue Empty => new Issue();

        public void Assign(Issue issue)
        {
            if (issue == null)
                throw new ArgumentNullException(nameof(issue));

            Issuer = issue.Issuer;
            IssuedOn = issue.IssuedOn;
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return Issuer;
            yield return IssuedOn;
        }
    }
}
