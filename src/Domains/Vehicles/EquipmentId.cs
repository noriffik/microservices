﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public class EquipmentId : EntityId
    {
        private const int Length = 3;

        protected EquipmentId()
        {
        }

        protected EquipmentId(string value) : base(value)
        {
        }

        public static EquipmentId Next(ModelId modelId, char value)
        {
            if (modelId == null)
                throw new ArgumentNullException(nameof(modelId));

            return Parse(modelId.Value + value);
        }

        public static EquipmentId Parse(string value)
        {
            return AlphaNumericEntityIdParser.Instance.Parse<EquipmentId>(value, Length);
        }
        
        public static bool TryParse(string value, out EquipmentId equipmentId)
        {
            return AlphaNumericEntityIdParser.Instance.TryParse(value, out equipmentId, Length);
        }

        public static implicit operator string(EquipmentId id)
        {
            return id?.Value;
        }

        public static implicit operator EquipmentId(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
