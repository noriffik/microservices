﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Domain.Vehicles
{
    public class OptionId : EntityId
    {
        private const int Length = 5;

        protected OptionId()
        {
        }

        protected OptionId(string value) : base(value)
        {
        }

        protected OptionId(ModelId modelId, PrNumber prNumber) : base(modelId.Value + prNumber.Value)
        {
        }

        public static OptionId Next(ModelId modelId, PrNumber prNumber)
        {
            if (modelId == null)
                throw new ArgumentNullException(nameof(modelId));

            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));

            return Parse(modelId.Value + prNumber.Value);
        }

        public static OptionId Parse(string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (TryParse(value, out var optionId))
                return optionId;

            throw new FormatException();
        }

        public static bool TryParse(string value, out OptionId optionId)
        {
            if (value == null || value.Length != Length ||
                !ModelId.TryParse(value.Substring(0, 2), out var modelId) ||
                !PrNumber.TryParse(value.Substring(2, 3), out var prNumber))
            {
                optionId = null;
                return false;
            }

            optionId = new OptionId(modelId, prNumber);
            return true;
        }

        public PrNumber PrNumber => PrNumber.Parse(Value.Substring(2, 3));

        public ModelId ModelId => ModelId.Parse(Value.Substring(0, 2));

        public static IEnumerable<OptionId> Next(ModelId modelId, IValueSet<PrNumber> prNumbers)
        {
            if (modelId == null)
                throw new ArgumentNullException(nameof(modelId));

            if (prNumbers == null)
                throw new ArgumentNullException(nameof(prNumbers));

            return prNumbers.Values.Select(prNumber => Next(modelId, prNumber));
        }

        public static IEnumerable<OptionId> Next(ModelId modelId, IEnumerable<PrNumber> prNumbers)
        {
            if (modelId == null)
                throw new ArgumentNullException(nameof(modelId));

            if (prNumbers == null)
                throw new ArgumentNullException(nameof(prNumbers));

            return prNumbers.Select(prNumber => Next(modelId, prNumber));
        }

        public static implicit operator string(OptionId id)
        {
            return id?.Value;
        }

        public static implicit operator OptionId(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
