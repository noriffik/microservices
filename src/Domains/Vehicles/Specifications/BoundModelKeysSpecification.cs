﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.Domain.Vehicles.Specifications
{
    internal class BoundModelKeysSpecification : Specification<IEnumerable<ModelKey>>
    {
        public static readonly BoundModelKeysSpecification Instance = new BoundModelKeysSpecification();

        public override Expression<Func<IEnumerable<ModelKey>, bool>> ToExpression()
        {
            return set => set.Select(k => k.ModelId.Value).Distinct().Count() == 1;
        }

        public override string Description => "Is for uniq model";
    }
}
