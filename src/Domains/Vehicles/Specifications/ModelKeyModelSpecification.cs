﻿using System;
using System.Linq.Expressions;

namespace NexCore.Domain.Vehicles.Specifications
{
    public class ModelKeyModelSpecification : Specification<ModelKey>
    {
        public ModelId ModelId { get; }

        public ModelKeyModelSpecification(ModelId modelId)
        {
            ModelId = modelId ?? throw new ArgumentNullException(nameof(modelId));
        }

        public override Expression<Func<ModelKey, bool>> ToExpression()
        {
            return key => key.ModelId == ModelId;
        }

        public override string Description => $@"ModelKey for Model with id ""{ModelId.Value}""";
    }
}
