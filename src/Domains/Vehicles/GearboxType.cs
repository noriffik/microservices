﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;

namespace NexCore.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public class GearboxType : EntityId
    {
        private static readonly Regex Pattern = new Regex("^[a-zA-Z0-9]*?( 4[Xx]4)*$");

        protected GearboxType()
        {
        }

        protected GearboxType(string value) : base(value)
        {
        }

        public static GearboxType Parse(string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (!TryParse(value, out var gearboxType))
                throw new FormatException();

            return gearboxType;
        }

        protected static bool IsValid(string value)
        {
            return !string.IsNullOrEmpty(value) && Pattern.IsMatch(value);
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return Value;
        }

        public static bool TryParse(string value, out GearboxType gearboxType)
        {
            gearboxType = IsValid(value) ? new GearboxType(value) : null;

            return gearboxType != null;
        }

        public static implicit operator string(GearboxType type)
        {
            return type?.Value;
        }

        public static implicit operator GearboxType(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
