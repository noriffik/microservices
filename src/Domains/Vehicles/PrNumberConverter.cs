﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace NexCore.Domain.Vehicles
{
    public class PrNumberConverter : TypeConverter
    {
        public override bool CanConvertFrom(
            ITypeDescriptorContext context,
            Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context,
            CultureInfo culture, object value)
        {
            return value is string ? PrNumber.Parse(value.ToString()) : base.ConvertFrom(context, culture, value);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context,
            Type destinationType)
        {
            return destinationType == typeof(PrNumber) || base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context,
            CultureInfo culture, object value, Type destinationType)
        {
            return destinationType == typeof(PrNumber) && value is string
                ? PrNumber.Parse(value.ToString())
                : base.ConvertTo(context, culture, value, destinationType);
        }
    }
}