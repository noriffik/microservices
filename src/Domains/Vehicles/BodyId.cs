﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public class BodyId : EntityId
    {
        private const int Length = 3;

        public BodyId()
        {
        }

        protected BodyId(string value) : base(value)
        {
        }

        public static BodyId Next(ModelId modelId, char value)
        {
            if (modelId == null)
                throw new ArgumentNullException(nameof(modelId));

            return Parse(modelId.Value + value);
        }

        public static BodyId Parse(string value)
        {
            return AlphaNumericEntityIdParser.Instance.Parse<BodyId>(value, Length);
        }

        public static bool TryParse(string value, out BodyId bodyId)
        {
            return AlphaNumericEntityIdParser.Instance.TryParse(value, out bodyId, Length);
        }

        public static implicit operator string(BodyId id)
        {
            return id?.Value;
        }

        public static implicit operator BodyId(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
