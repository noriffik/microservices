﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public class ModelId : EntityId
    {
        private const int Length = 2;

        protected ModelId()
        {
        }

        protected ModelId(string value) : base(value)
        {
        }

        public static ModelId Parse(string value)
        {
            return AlphaNumericEntityIdParser.Instance.Parse<ModelId>(value, Length);
        }

        public static bool TryParse(string value, out ModelId modelId)
        {
            return AlphaNumericEntityIdParser.Instance.TryParse(value, out modelId, Length);
        }
        
        public static ModelId From(EntityId entityId)
        {
            if (entityId == null)
                throw new ArgumentNullException(nameof(entityId));

            if (entityId.Value.Length < 2)
                throw new ArgumentException("Code cannot be converted to ModelId.", nameof(entityId));
            
            return Parse(entityId.Value.Substring(0, 2));
        }

        public static implicit operator string(ModelId id)
        {
            return id?.Value;
        }

        public static implicit operator ModelId(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
