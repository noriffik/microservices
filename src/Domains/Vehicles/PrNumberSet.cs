﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Domain.Vehicles
{
    public class PrNumberSet : ValueObject, IValueSet<PrNumber>
    {
        private BaseValueSet<PrNumber> _inner;

        public string AsString
        {
            get => _inner.AsString;
            private set => _inner = new BaseValueSet<PrNumber>(value, PrNumber.Parse);
        }

        public static PrNumberSet Empty => Parse(string.Empty);

        public IEnumerable<PrNumber> Values => _inner.Values;

        public PrNumberSet()
        {
            _inner = new BaseValueSet<PrNumber>(PrNumber.Parse);
        }

        public PrNumberSet(IEnumerable<PrNumber> prNumbers)
        {
            _inner = new BaseValueSet<PrNumber>(prNumbers, PrNumber.Parse);
        }

        public PrNumberSet(params PrNumber[] prNumbers)
            : this(prNumbers.AsEnumerable())
        {
        }

        public PrNumberSet(IValueSet<PrNumber> set)
        {
            _inner = new BaseValueSet<PrNumber>(set, PrNumber.Parse);
        }
        
        public static PrNumberSet Parse(string value)
        {
            return Wrap(BaseValueSet<PrNumber>.Parse(value, PrNumber.Parse));
        }

        private static PrNumberSet Wrap(IValueSet<PrNumber> set)
        {
            return new PrNumberSet(set);
        }

        public static bool TryParse(string value, out PrNumberSet prNumberSet)
        {
            prNumberSet = null;

            var set = BaseValueSet<PrNumber>.TryParse(value, PrNumber.Parse);
            if (set == null)
                return false;

            prNumberSet = Wrap(set);
            return true;
        }

        public bool Has(PrNumberSet prNumberSet)
        {
            if (prNumberSet == null)
                throw new ArgumentNullException(nameof(prNumberSet));

            return Values.Intersect(prNumberSet.Values).SequenceEqual(prNumberSet.Values);
        }

        public bool HasExactlyOne(PrNumberSet prNumberSet)
        {
            if (prNumberSet == null)
                throw new ArgumentNullException(nameof(prNumberSet));

            return Values.Intersect(prNumberSet.Values).Count() == 1;
        }

        public bool HasAtLeast(PrNumberSet prNumberSet, int matchingCount)
        {
            if (prNumberSet == null)
                throw new ArgumentNullException(nameof(prNumberSet));

            return Values.Intersect(prNumberSet.Values).Count() >= matchingCount;
        }
        
        public PrNumberSet Expand(PrNumber prNumber) => Wrap(_inner.Expand(prNumber.Value));

        public PrNumberSet Expand(IEnumerable<PrNumber> with) => Wrap(_inner.Expand(with));

        public PrNumberSet Expand(IValueSet<PrNumber> with) => Wrap(_inner.Expand(with));
        
        public PrNumberSet Shrink(PrNumber prNumber) => Wrap(_inner.Shrink(prNumber.Value));

        public PrNumberSet Shrink(IEnumerable<PrNumber> with) => Wrap(_inner.Shrink(with));

        public PrNumberSet Shrink(IValueSet<PrNumber> with) => Wrap(_inner.Shrink(with));

        protected override IEnumerable<object> GetValues()
        {
            yield return AsString;
        }

        public bool Has(PrNumber prNumber)
        {
            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));

            return Values.Contains(prNumber);
        }

        public static implicit operator string(PrNumberSet prNumbers)
        {
            return prNumbers?.AsString;
        }

        public static implicit operator PrNumberSet(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
