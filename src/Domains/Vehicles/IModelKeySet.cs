﻿namespace NexCore.Domain.Vehicles
{
    public interface IModelKeySet : IValueSet<ModelKey>
    {

    }
}