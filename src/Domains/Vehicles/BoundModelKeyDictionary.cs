﻿using NexCore.Domain.Vehicles.Specifications;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Domain.Vehicles
{
    public class BoundModelKeyDictionary<T> : IReadOnlyDictionary<ModelKey, T>
    {
        private readonly IReadOnlyDictionary<ModelKey, T> _inner;
        private ModelId _modelId;

        public ModelId ModelId
        {
            get
            {
                if (_modelId == null)
                    _modelId = _inner.Keys.First().ModelId;

                return _modelId;
            }
        }

        public BoundModelKeyDictionary(IEnumerable<KeyValuePair<ModelKey, T>> values)
        {
            _inner = CreateDictionary(values);
        }

        public BoundModelKeyDictionary(IDictionary<ModelKey, T> values)
        {
            _inner = CreateDictionary(values);
        }

        private static IReadOnlyDictionary<ModelKey, T> CreateDictionary(IEnumerable<KeyValuePair<ModelKey, T>> values)
        {
            if (values == null)
                throw new ArgumentNullException(nameof(values));

            var pairs = values.ToList();
            if (!BoundModelKeysSpecification.Instance.IsSatisfiedBy(pairs.Select(k => k.Key)))
                throw new SingleModelViolationException();

            return new Dictionary<ModelKey, T>(pairs);
        }

        public IEnumerator<KeyValuePair<ModelKey, T>> GetEnumerator()
        {
            return _inner.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) _inner).GetEnumerator();
        }

        public int Count => _inner.Count;

        public bool ContainsKey(ModelKey key)
        {
            return _inner.ContainsKey(key);
        }

        public bool TryGetValue(ModelKey key, out T value)
        {
            return _inner.TryGetValue(key, out value);
        }

        public T this[ModelKey key] => _inner[key];

        public IEnumerable<ModelKey> Keys => _inner.Keys;

        public IEnumerable<T> Values => _inner.Values;
    }
}
