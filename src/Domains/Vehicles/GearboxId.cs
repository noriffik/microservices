﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public class GearboxId : EntityId
    {
        private const int Length = 3;

        protected GearboxId()
        {
        }

        protected GearboxId(string value) : base(value)
        {
        }

        public static GearboxId Next(ModelId modelId, char value)
        {
            if (modelId == null)
                throw new ArgumentNullException(nameof(modelId));

            return Parse(modelId.Value + value);
        }

        public static GearboxId Parse(string value)
        {
            return AlphaNumericEntityIdParser.Instance.Parse<GearboxId>(value, Length);
        }

        public static bool TryParse(string value, out GearboxId gearboxId)
        {
            return AlphaNumericEntityIdParser.Instance.TryParse(value, out gearboxId, Length);
        }

        public static implicit operator string(GearboxId id)
        {
            return id?.Value;
        }

        public static implicit operator GearboxId(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
