﻿using System.Diagnostics.CodeAnalysis;

namespace NexCore.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public class ColorId : EntityId
    {
        private const int Length = 4;

        protected ColorId()
        {
        }

        protected ColorId(string value) : base(value)
        {
        }

        public static ColorId Parse(string value)
        {
            return AlphaNumericEntityIdParser.Instance.Parse<ColorId>(value, Length);
        }

        public static bool TryParse(string value, out ColorId colorId)
        {
            return AlphaNumericEntityIdParser.Instance.TryParse(value, out colorId, Length);
        }

        public static implicit operator string(ColorId id)
        {
            return id?.Value;
        }

        public static implicit operator ColorId(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
