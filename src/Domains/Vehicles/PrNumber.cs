﻿using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    [TypeConverter(typeof(PrNumberConverter))]
    public class PrNumber : EntityId
    {
        private const int Length = 3;

        protected PrNumber()
        {
        }

        protected PrNumber(string value) : base(value)
        {
        }

        public static PrNumber Parse(string value)
        {
            return AlphaNumericEntityIdParser.Instance.Parse<PrNumber>(value, Length);
        }

        public static bool TryParse(string value, out PrNumber prNumber)
        {
            return AlphaNumericEntityIdParser.Instance.TryParse(value, out prNumber, Length);
        }

        public static implicit operator string(PrNumber prNumber)
        {
            return prNumber?.Value;
        }

        public static implicit operator PrNumber(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
