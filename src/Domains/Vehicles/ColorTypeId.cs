﻿using System.Diagnostics.CodeAnalysis;

namespace NexCore.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public class ColorTypeId : PrNumber
    {
        private const int Length = 3;

        protected ColorTypeId()
        {
        }

        protected ColorTypeId(string value) : base(value)
        {
        }

        public new static ColorTypeId Parse(string value)
        {
            return AlphaNumericEntityIdParser.Instance.Parse<ColorTypeId>(value, Length);
        }

        public static bool TryParse(string value, out ColorTypeId colorTypeId)
        {
            return AlphaNumericEntityIdParser.Instance.TryParse(value, out colorTypeId, Length);
        }

        public static implicit operator string(ColorTypeId id)
        {
            return id?.Value;
        }

        public static implicit operator ColorTypeId(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
