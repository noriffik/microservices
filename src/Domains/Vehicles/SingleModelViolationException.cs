﻿using System;

namespace NexCore.Domain.Vehicles
{
    public class SingleModelViolationException : InvalidOperationException
    {
        private const string DefaultMessage = "All model keys must be for the same model";

        public SingleModelViolationException() : base(DefaultMessage)
        {
        }

        public SingleModelViolationException(string message) : base(message)
        {
        }

        public SingleModelViolationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
