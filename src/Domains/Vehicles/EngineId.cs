﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public class EngineId : EntityId
    {
        private const int Length = 3;

        protected EngineId()
        {
        }

        protected EngineId(string value) : base(value)
        {
        }

        public static EngineId Next(ModelId modelId, char value)
        {
            if (modelId == null)
                throw new ArgumentNullException(nameof(modelId));

            return Parse(modelId.Value + value);
        }

        public static EngineId Parse(string value)
        {
            return AlphaNumericEntityIdParser.Instance.Parse<EngineId>(value, Length);
        }

        public static bool TryParse(string value, out EngineId engineId)
        {
            return AlphaNumericEntityIdParser.Instance.TryParse(value, out engineId, Length);
        }

        public static implicit operator string(EngineId id)
        {
            return id?.Value;
        }

        public static implicit operator EngineId(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
