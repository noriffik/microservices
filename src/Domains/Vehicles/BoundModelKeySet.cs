﻿using NexCore.Domain.Vehicles.Specifications;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace NexCore.Domain.Vehicles
{
    public class BoundModelKeySet : ValueObject, IModelKeySet
    {
        private ModelKeySet _inner;
        private ModelId _modelId;

        [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core.")]
        public string AsString
        {
            get => _inner.AsString;
            private set => _inner = ModelKeySet.Parse(value);
        }

        public IEnumerable<ModelKey> Values => _inner.Values;

        public ModelId ModelId
        {
            get
            {
                if (_modelId == null)
                    _modelId = _inner.Values.First().ModelId;

                return _modelId;
            }
        }

        public BoundModelKeySet(IEnumerable<ModelKey> modelKeys)
        {
            _inner = CreateSet(modelKeys);
        }
        
        public BoundModelKeySet(IModelKeySet modelKeys)
        {
            _inner = CreateSet(modelKeys.Values);
        }

        private static ModelKeySet CreateSet(IEnumerable<ModelKey> modelKeys)
        {
            if (modelKeys == null)
                throw new ArgumentNullException(nameof(modelKeys));

            var values = modelKeys.ToList();
            
            if (!BoundModelKeysSpecification.Instance.IsSatisfiedBy(values))
                throw new SingleModelViolationException();
            
            return new ModelKeySet(values);
        }

        private BoundModelKeySet()
        {
            _inner = new ModelKeySet();
        }

        public BoundModelKeySet(ModelId modelId) : this()
        {
            _modelId = modelId;
        }

        public static BoundModelKeySet Parse(string value)
        {
            return new BoundModelKeySet(ModelKeySet.Parse(value));
        }

        public static bool TryParse(string value, out BoundModelKeySet boundModelKeySet)
        {
            boundModelKeySet = null;

            if (!ModelKeySet.TryParse(value, out var inner))
            {
                return false;
            }

            try
            {
                boundModelKeySet = new BoundModelKeySet(inner);
            }
            catch (SingleModelViolationException)
            {
                return false;
            }

            return true;
        }

        public BoundModelKeySet Expand(string with)
        {
            return Expand(ModelKeySet.Parse(with));
        }

        public BoundModelKeySet Expand(IEnumerable<ModelKey> with)
        {
            return Expand(new ModelKeySet(with));
        }

        public BoundModelKeySet Expand(IModelKeySet with)
        {
            if (with.Values.Any() && with.Values.FirstOrDefault()?.ModelId != ModelId)
                throw new SingleModelViolationException();

            return new BoundModelKeySet(_inner.Expand(with));
        }

        public BoundModelKeySet Shrink(string with)
        {
            return new BoundModelKeySet(_inner.Shrink(with));
        }

        public BoundModelKeySet Shrink(IEnumerable<ModelKey> with)
        {
            return new BoundModelKeySet(_inner.Shrink(with));
        }

        public BoundModelKeySet Shrink(IModelKeySet with)
        {
            return new BoundModelKeySet(_inner.Shrink(with));
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return AsString;
        }

        public ModelKey this[int key] => Values.ElementAt(key);
    }
}