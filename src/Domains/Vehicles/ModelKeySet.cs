﻿using System.Collections.Generic;

namespace NexCore.Domain.Vehicles
{
    public class ModelKeySet : ValueObject, IModelKeySet
    {
        private readonly BaseValueSet<ModelKey> _inner;

        public string AsString => _inner.AsString;

        public IEnumerable<ModelKey> Values => _inner.Values;

        public ModelKeySet()
        {
            _inner = new BaseValueSet<ModelKey>(ModelKey.Parse);
        }

        public ModelKeySet(IValueSet<ModelKey> set)
        {
            _inner = new BaseValueSet<ModelKey>(set, ModelKey.Parse);
        }

        public ModelKeySet(IEnumerable<ModelKey> modelKeys)
        {
            _inner = new BaseValueSet<ModelKey>(modelKeys, ModelKey.Parse);
        }

        public static ModelKeySet Parse(string value)
        {
            return Wrap(BaseValueSet<ModelKey>.Parse(value, ModelKey.Parse));
        }

        private static ModelKeySet Wrap(IValueSet<ModelKey> set)
        {
            return new ModelKeySet(set);
        }

        public static bool TryParse(string value, out ModelKeySet modelKeySet)
        {
            modelKeySet = null;

            var set = BaseValueSet<ModelKey>.TryParse(value, ModelKey.Parse);
            if (set == null)
                return false;

            modelKeySet = Wrap(set);
            return true;
        }

        public ModelKeySet Expand(string with) => Wrap(_inner.Expand(with));

        public ModelKeySet Expand(IEnumerable<ModelKey> with) => Wrap(_inner.Expand(with));

        public ModelKeySet Expand(IModelKeySet with) => Wrap(_inner.Expand(with));

        public ModelKeySet Shrink(string with) => Wrap(_inner.Shrink(with));

        public ModelKeySet Shrink(IEnumerable<ModelKey> with) => Wrap(_inner.Shrink(with));

        public ModelKeySet Shrink(IModelKeySet with) => Wrap(_inner.Shrink(with));

        protected override IEnumerable<object> GetValues()
        {
            yield return AsString;
        }
    }
}
