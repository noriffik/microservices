﻿using System.Diagnostics.CodeAnalysis;

namespace NexCore.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public class ModelKey : EntityId
    {
        private const int Length = 6;

        protected ModelKey()
        {
        }

        protected ModelKey(string value) : base(value)
        {
        }

        public string ModelCode => Value.Substring(0, 2);

        public ModelId ModelId => ModelId.Parse(ModelCode);

        public char BodyCode => Value[2];

        public BodyId BodyId => BodyId.Next(ModelId, BodyCode);

        public char EquipmentCode => Value[3];

        public EquipmentId EquipmentId => EquipmentId.Next(ModelId, EquipmentCode);

        public char EngineCode => Value[4];

        public EngineId EngineId => EngineId.Next(ModelId, EngineCode);

        public char GearboxCode => Value[5];

        public GearboxId GearboxId => GearboxId.Next(ModelId, GearboxCode);
        
        public static ModelKey Parse(string value)
        {
            return AlphaNumericEntityIdParser.Instance.Parse<ModelKey>(value, Length);
        }

        public static bool TryParse(string value, out ModelKey modelKey)
        {
            return AlphaNumericEntityIdParser.Instance.TryParse(value, out modelKey, Length);
        }

        public static implicit operator string(ModelKey id)
        {
            return id?.Value;
        }

        public static implicit operator ModelKey(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
