﻿using NexCore.Domain;

namespace NexCore.DealerNetwork
{
    public class DealerCode : EntityId
    {
        private const int Length = 5;

        protected DealerCode()
        {
        }

        protected DealerCode(string value) : base(value)
        {
        }

        public static DealerCode Parse(string value)
        {
            return NumericEntityIdParser.Instance.Parse<DealerCode>(value, Length);
        }

        public static bool TryParse(string value, out DealerCode dealerCode)
        {
            return NumericEntityIdParser.Instance.TryParse(value, out dealerCode, Length);
        }

        public static implicit operator string(DealerCode dealerCode)
        {
            return dealerCode?.Value;
        }

        public static implicit operator DealerCode(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
