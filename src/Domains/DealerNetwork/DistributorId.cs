﻿using NexCore.Domain;

namespace NexCore.DealerNetwork
{
    public class DistributorId : EntityId
    {
        private const int Length = 3;

        protected DistributorId()
        {
        }

        protected DistributorId(string value) : base(value)
        {
        }

        public static DistributorId Parse(string value)
        {
            return AlphaNumericEntityIdParser.Instance.Parse<DistributorId>(value, Length);
        }

        public static bool TryParse(string value, out DistributorId distributorId)
        {
            return AlphaNumericEntityIdParser.Instance.TryParse(value, out distributorId, Length);
        }

        public static implicit operator string(DistributorId distributorId)
        {
            return distributorId?.Value;
        }

        public static implicit operator DistributorId(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
