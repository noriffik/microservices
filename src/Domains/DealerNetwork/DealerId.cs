﻿using NexCore.Domain;
using System;

namespace NexCore.DealerNetwork
{
    public class DealerId : EntityId
    {
        private const int Length = 8;

        protected DealerId()
        {
        }

        protected DealerId(string value) : base(value)
        {
        }

        protected DealerId(DistributorId distributorId, DealerCode dealerCode) : base(distributorId.Value + dealerCode.Value)
        {
        }

        public DistributorId DistributorId => DistributorId.Parse(Value.Substring(0, 3));

        public DealerCode DealerCode => DealerCode.Parse(Value.Substring(3, 5));

        public static DealerId Next(DistributorId distributorId, DealerCode dealerCode)
        {
            if (distributorId == null)
                throw new ArgumentNullException(nameof(distributorId));

            if (dealerCode == null)
                throw new ArgumentNullException(nameof(dealerCode));

            return Parse(distributorId.Value + dealerCode.Value);
        }

        public static DealerId Parse(string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (TryParse(value, out var dealerId))
                return dealerId;

            throw new FormatException();
        }

        public static DealerId Parse(string distributorId, string dealerCode)
        {
            if (distributorId == null)
                throw new ArgumentNullException(nameof(distributorId));
            if (dealerCode == null)
                throw new ArgumentNullException(nameof(dealerCode));

            return Parse(distributorId + dealerCode);
        }

        public static bool TryParse(string value, out DealerId dealerId)
        {
            if (value == null || value.Length != Length ||
                !DistributorId.TryParse(value.Substring(0, 3), out var distributorId) ||
                !DealerCode.TryParse(value.Substring(3, 5), out var dealerCode))
            {
                dealerId = null;
                return false;
            }

            dealerId = new DealerId(distributorId, dealerCode);
            return true;
        }

        public static bool TryParse(string distributorIdValue, string dealerCodeValue, out DealerId dealerId)
        {
            if (!DistributorId.TryParse(distributorIdValue, out var distributorId) ||
                !DealerCode.TryParse(dealerCodeValue, out var dealerCode))
            {
                dealerId = null;
                return false;
            }

            dealerId = new DealerId(distributorId, dealerCode);
            return true;
        }

        public static implicit operator string(DealerId id)
        {
            return id?.Value;
        }

        public static implicit operator DealerId(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
