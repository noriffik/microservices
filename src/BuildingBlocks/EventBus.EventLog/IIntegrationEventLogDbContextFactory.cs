﻿using System.Data.Common;

namespace NexCore.EventBus.EventLog
{
    public interface IIntegrationEventLogDbContextFactory
    {
        IntegrationEventLogDbContext Create(string connectionString);

        IntegrationEventLogDbContext Create(DbConnection connection);
    }
}