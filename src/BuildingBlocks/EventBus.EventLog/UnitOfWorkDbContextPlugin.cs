﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using NexCore.EventBus.Abstract;
using NexCore.Infrastructure;
using System;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.EventBus.EventLog
{
    public class UnitOfWorkDbContextPlugin : IUnitOfWorkDbContextPlugin
	{
        private readonly IIntegrationEventLogDbContextFactory _dbContextFactory;
		private readonly IIntegrationEventStorage _eventStorage;

        public UnitOfWorkDbContextPlugin(IIntegrationEventLogDbContextFactory dbContextFactory, IIntegrationEventStorage eventStorage)
        {
            _dbContextFactory = dbContextFactory ?? throw new ArgumentNullException(nameof(dbContextFactory));
            _eventStorage = eventStorage ?? throw new ArgumentNullException(nameof(eventStorage));
            UseTransaction = true;
        }

        public bool UseTransaction { get; set; }

        public async Task Commit(DbConnection connection, IDbContextTransaction transaction, CancellationToken token)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));

            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));

            using (var dbContext = _dbContextFactory.Create(connection))
            {
                await AddEventsFor(dbContext, transaction, token);
                await CommitEventsFor(dbContext, transaction, token);
            }
        }

        private async Task AddEventsFor(IntegrationEventLogDbContext dbContext, IDbContextTransaction transaction, CancellationToken token)
        {
            var events = await _eventStorage.Get();
            var entries = events
                .Select(e => new IntegrationEventLogEntry(e, transaction.TransactionId))
                .ToList();

            await dbContext.IntegrationEventLogs.AddRangeAsync(entries, token);
        }

        private async Task CommitEventsFor(DbContext dbContext, IDbContextTransaction transaction, CancellationToken token)
        {
            if (UseTransaction)
                dbContext.Database.UseTransaction(transaction.GetDbTransaction());

            await dbContext.SaveChangesAsync(token);
        }
    }
}