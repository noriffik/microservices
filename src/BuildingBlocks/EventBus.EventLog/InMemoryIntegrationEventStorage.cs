﻿using NexCore.EventBus.Abstract;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexCore.EventBus.EventLog
{
    public class InMemoryIntegrationEventStorage : IIntegrationEventStorage
    {
        private readonly List<IntegrationEvent> _events = new List<IntegrationEvent>();

		public Task<IEnumerable<IntegrationEvent>> Get()
        {
            var items = _events.AsReadOnly();

            return Task.FromResult((IEnumerable<IntegrationEvent>) items);
        }

		public Task Add(IntegrationEvent @event)
        {
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));

            _events.Add(@event);

            return Task.CompletedTask;
        }

        public Task AddRange(IEnumerable<IntegrationEvent> events)
        {
            if (events == null)
                throw new ArgumentNullException(nameof(events));

            _events.AddRange(events);

            return Task.CompletedTask;
        }

        public Task Clear()
        {
            _events.Clear();

            return Task.CompletedTask;
        }
    }
}