﻿using Newtonsoft.Json;
using NexCore.EventBus.Abstract;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace NexCore.EventBus.EventLog
{
    public class IntegrationEventLogEntry
	{
		private IntegrationEventLogEntry() { }

		public IntegrationEventLogEntry(IntegrationEvent @event, Guid transactionId)
		{
			EventId = @event.Id;
			EventTypeName = @event.GetType().FullName;
			CreationTime = @event.CreatedAt;
			Content = JsonConvert.SerializeObject(@event);
			LogState = IntegrationEventLogState.NotPublished;
			TimesSent = 0;
			TransactionId = transactionId.ToString();
		}

		public IntegrationEventLogEntry DeserializeJsonContent(Type type)
		{
			IntegrationEvent = JsonConvert.DeserializeObject(Content, type) as IntegrationEvent;
			return this;
		}

		[Key]
		public Guid EventId { get; private set; }

		[Required]
		public string EventTypeName { get; private set; }

		[NotMapped]
		public string EventTypeShortName => EventTypeName.Split('.')?.Last();

		[NotMapped]
		public IntegrationEvent IntegrationEvent { get; private set; }

		[Required]
		public IntegrationEventLogState LogState { get; set; }

		[Required]
		public int TimesSent { get; set; }

		[Required]
		public DateTime CreationTime { get; private set; }

		[Required]
		public string Content { get; private set; }

		public string TransactionId { get; private set; }
	}
}