﻿using NexCore.Infrastructure;

namespace NexCore.EventBus.EventLog
{
    public class IntegrationEventLogDesignTimeFactory : DbContextBaseDesignFactory<IntegrationEventLogDbContext>
    {
        public IntegrationEventLogDesignTimeFactory() : base("ConnectionString")
        {
        }
    }
}
