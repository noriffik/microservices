﻿namespace NexCore.EventBus.EventLog
{
	public enum IntegrationEventLogState
	{
		NotPublished = 0,
		InProgress = 1,
		Published = 2,
		PublishedFailed = 3
	}
}