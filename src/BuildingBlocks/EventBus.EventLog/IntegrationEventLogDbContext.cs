﻿using Microsoft.EntityFrameworkCore;

namespace NexCore.EventBus.EventLog
{
    public class IntegrationEventLogDbContext : DbContext
    {
		public IntegrationEventLogDbContext(DbContextOptions<IntegrationEventLogDbContext> options) : base(options)
		{
		}

		public DbSet<IntegrationEventLogEntry> IntegrationEventLogs { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
		{
			builder.Entity<IntegrationEventLogEntry>().ToTable("IntegrationEventLog", "event_log");
		}
	}
}