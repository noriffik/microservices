﻿using Microsoft.EntityFrameworkCore;
using System.Data.Common;

namespace NexCore.EventBus.EventLog
{
    public class IntegrationEventLogDbContextSqlServerFactory : IIntegrationEventLogDbContextFactory
    {
        public IntegrationEventLogDbContext Create(string connectionString)
        {
            return new IntegrationEventLogDbContext(
                new DbContextOptionsBuilder<IntegrationEventLogDbContext>()
                    .UseSqlServer(connectionString)
                    .Options);
        }

        public IntegrationEventLogDbContext Create(DbConnection connection)
        {
            return new IntegrationEventLogDbContext(
                new DbContextOptionsBuilder<IntegrationEventLogDbContext>()
                    .UseSqlServer(connection)
                    .Options);
        }
    }
}