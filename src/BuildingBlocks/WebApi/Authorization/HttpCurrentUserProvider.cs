﻿using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;

namespace NexCore.WebApi.Authorization
{
    public class HttpCurrentUserProvider : ICurrentUserProvider
    {
        private readonly IHttpContextAccessor _httpContext;

        public HttpCurrentUserProvider(IHttpContextAccessor httpContext)
        {
            _httpContext = httpContext ?? throw new ArgumentNullException(nameof(httpContext));
        }

        public ClaimsPrincipal GetUser()
        {
            return _httpContext.HttpContext.User;
        }
    }
}
