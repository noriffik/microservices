﻿using System.Security.Claims;

namespace NexCore.WebApi.Authorization
{
    public interface ICurrentUserProvider
    {
        ClaimsPrincipal GetUser(); 
    }
}
