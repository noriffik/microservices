﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using NexCore.WebApi.Filters;
using NexCore.WebApi.ModelBinders;
using System;

namespace NexCore.WebApi
{
    public class MvcDefaultOptionsSetup : IConfigureOptions<MvcOptions>
    {
        private readonly IHttpRequestStreamReaderFactory _readerFactory;
        private readonly ILoggerFactory _loggerFactory;

        public MvcDefaultOptionsSetup(IHttpRequestStreamReaderFactory readerFactory)
            : this(readerFactory, NullLoggerFactory.Instance)
        {
        }

        public MvcDefaultOptionsSetup(IHttpRequestStreamReaderFactory readerFactory, ILoggerFactory loggerFactory)
        {
            _readerFactory = readerFactory ?? throw new ArgumentNullException(nameof(readerFactory));
            _loggerFactory = loggerFactory ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        public virtual void Configure(MvcOptions options)
        {
            if (options == null)
                throw new ArgumentNullException(nameof(options));

            options.Filters.Add(typeof(ExceptionFilter));
            options.ModelBinderProviders.Insert(0, new CommandModelBinderProvider(options.InputFormatters, _readerFactory, _loggerFactory, options));
        }
    }
}
