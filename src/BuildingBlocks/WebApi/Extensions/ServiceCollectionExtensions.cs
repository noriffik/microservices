﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;

namespace NexCore.WebApi.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddJwtBearerAuthentication(this IServiceCollection services,
            IConfiguration configuration, string audience)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Remove("sub");

            var identityUrl = configuration.GetValue<string>("IdentityUrl");
            if (identityUrl == null)
                throw new InvalidOperationException("Failed to retrieve Identity Server internal url");

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(o =>
                {
                    o.Authority = identityUrl;
                    o.Audience = audience;
                    o.RequireHttpsMetadata = false;
                });

            return services;
        }

        public static IServiceCollection AddSwaggerGen(this IServiceCollection services,
            IConfiguration configuration, string title, string version, IDictionary<string, string> scopes)
        {
            if (title == null)
                throw new ArgumentNullException(nameof(title));

            if (version == null)
                throw new ArgumentNullException(nameof(version));

            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.CustomSchemaIds(type => type.FullName);
                options.SwaggerDoc(version, new Info
                {
                    Title = title,
                    Version = version
                });

                var identityUrl = configuration.GetValue<string>("IdentityExternalUrl");
                if (identityUrl == null)
                    throw new InvalidOperationException("Failed to retrieve Identity Server external url");

                options.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Flow = "implicit",
                    AuthorizationUrl = $"{identityUrl}/connect/authorize",
                    TokenUrl = $"{identityUrl}/connect/token",
                    Scopes = scopes
                });

                options.OperationFilter<SecurityRequirementsOperationFilter>();
            });

            return services;
        }
    }
}
