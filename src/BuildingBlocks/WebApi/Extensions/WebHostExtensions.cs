﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Application.Setup;
using System.Threading.Tasks;

namespace NexCore.WebApi.Extensions
{
    public static class WebHostExtensions
    {
        public static async Task<IWebHost> Setup<TSetup>(this IWebHost webHost) where TSetup : IApplicationSetupTask
        {
            using (var scope = webHost.Services.CreateScope())
            {
                var setup = scope.ServiceProvider.GetRequiredService<TSetup>();

                await setup.Execute();
            }

            return webHost;
        }
    }
}
