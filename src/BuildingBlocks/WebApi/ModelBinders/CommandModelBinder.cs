﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.ComponentModel;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;

namespace NexCore.WebApi.ModelBinders
{
    public class CommandModelBinder : IModelBinder
    {
        private readonly IModelBinder _innerBinder;

        public CommandModelBinder(IModelBinder innerBinder)
        {
            _innerBinder = innerBinder ?? throw new ArgumentNullException(nameof(innerBinder));
        }

        public async Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
                throw new ArgumentNullException(nameof(bindingContext));

            await _innerBinder.BindModelAsync(bindingContext);

            if (bindingContext.Result == ModelBindingResult.Failed())
                return;

            foreach (var (key, value) in bindingContext.ActionContext.RouteData.Values)
            {
                SetRouteValue(bindingContext, key, value);
            }
        }

        private void SetRouteValue(ModelBindingContext bindingContext, string key, object value)
        {
            var model = bindingContext.Result.Model;
            var property = model.GetType().GetProperty(key);
            if (property == null || !property.CanWrite)
                return;

            try
            {
                property.SetValue(model, TypeDescriptor.GetConverter(property.PropertyType).ConvertFrom(value));
            }
            catch (Exception e)
            {
                if (!(e is FormatException) && e.InnerException != null)
                    e = ExceptionDispatchInfo.Capture(e.InnerException).SourceException;

                bindingContext.ModelState.TryAddModelException(key, e);
                bindingContext.Result = ModelBindingResult.Failed();
            }
        }
    }
}
