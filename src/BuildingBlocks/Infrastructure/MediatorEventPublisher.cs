﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace NexCore.Infrastructure
{
    internal abstract class MediatorEventPublisher : IEntityEventPublisher
    {
        protected readonly DbContext Context;
        protected readonly IMediator Mediator;

        protected MediatorEventPublisher(DbContext context, IMediator mediator)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
            Mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        public abstract Task Publish();
    }
}
