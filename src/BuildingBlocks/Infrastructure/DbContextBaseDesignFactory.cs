﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace NexCore.Infrastructure
{
    public abstract class DbContextBaseDesignFactory<TDbContext> : IDesignTimeDbContextFactory<TDbContext> where TDbContext : DbContext
    {
        private readonly string _connectionStringKey;
        private readonly string _migrationsAssembly;
        private readonly Lazy<IConfiguration> _configuration = new Lazy<IConfiguration>(BuildConfiguration);

        protected DbContextBaseDesignFactory(string connectionStringKey, string migrationsAssembly = null)
        {
            if (string.IsNullOrEmpty(connectionStringKey))
                throw new ArgumentException("Value cannot be null or empty.", nameof(connectionStringKey));

            _connectionStringKey = connectionStringKey;
            _migrationsAssembly = migrationsAssembly;
        }

        public TDbContext CreateDbContext(string[] args)
        {
            var connectionString = Configuration[_connectionStringKey];
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new InvalidOperationException("Could not find a connection string");

            var builder = new DbContextOptionsBuilder<TDbContext>();
            builder.UseSqlServer(connectionString, o =>
            {
                if (_migrationsAssembly != null)
                    o.MigrationsAssembly(_migrationsAssembly);
            });

            return (TDbContext)Activator.CreateInstance(typeof(TDbContext), builder.Options);
        }

        private IConfiguration Configuration => _configuration.Value;

        private static IConfiguration BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            return builder.Build();
        }
    }
}
