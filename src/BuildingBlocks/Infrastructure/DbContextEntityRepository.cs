﻿using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("NexCore.Testing")]
[assembly: InternalsVisibleTo("NexCore.Infrastructure.UnitTests")]
namespace NexCore.Infrastructure
{
    internal sealed class DbContextEntityRepository : IEntityRepository
    {
        private readonly DbContext _context;

        public DbContextEntityRepository(DbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Task Add<TEntity>(TEntity entity) where TEntity : class, IAggregateRoot
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            return _context.Set<TEntity>().AddAsync(entity);
        }

        public Task Add<TEntity, TId>(TEntity entity) where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            return _context.Set<TEntity>().AddAsync(entity);
        }

        public Task Remove<TEntity>(TEntity entity) where TEntity : class, IAggregateRoot
        {
            _context.Set<TEntity>().Remove(entity);

            return Task.CompletedTask;
        }

        public Task Remove<TEntity, TId>(TEntity entity) where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            _context.Set<TEntity>().Remove(entity);

            return Task.CompletedTask;
        }

        //With cancellationToken

        public async Task<IEnumerable<TEntity>> Find<TEntity>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot
        {
            return await _context.Set<TEntity>().Apply(specification)
                .ToListAsync(cancellationToken);
        }

        public async Task<IEnumerable<TEntity>> Find<TEntity, TId>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            return await _context.Set<TEntity>().Apply(specification)
                .ToListAsync(cancellationToken);
        }

        public Task<IEnumerable<TEntity>> Find<TEntity>(int id, CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot
        {
            return Find(new IdSpecification<TEntity>(id), cancellationToken);
        }

        public Task<IEnumerable<TEntity>> Find<TEntity, TId>(TId id, CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            return Find<TEntity, TId>(new IdSpecification<TEntity, TId>(id), cancellationToken);
        }

        public async Task<bool> Has<TEntity>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot
        {
            return await _context.Has(specification, cancellationToken);
        }

        public async Task<bool> Has<TEntity, TId>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            return await _context.Has(specification, cancellationToken);
        }

        public Task<bool> Has<TEntity>(int id,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot
        {
            return Has(new IdSpecification<TEntity>(id), cancellationToken);
        }

        public Task<bool> Has<TEntity, TId>(TId id,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            return Has<TEntity, TId>(new IdSpecification<TEntity, TId>(id), cancellationToken);
        }

        public async Task<bool> HasMany<TEntity>(IEnumerable<int> ids,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot
        {
            var unsatisfied = await GetMissingIds<TEntity>(ids, cancellationToken);

            return unsatisfied.Any();
        }

        private async Task<ICollection<IdSpecification<TEntity>>> GetMissingIds<TEntity>(IEnumerable<int> ids, CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot
        {
            var specifications = ids.Distinct()
                .Select(id => new IdSpecification<TEntity>(id))
                .ToList();

            var matched = await _context.Set<TEntity>()
                .Apply(new AnySpecification<TEntity>(specifications))
                .Select(e => e.Id)
                .ToListAsync(cancellationToken);

            return specifications.Where(s => !matched.Contains(s.Id))
                .ToList();
        }

        public async Task<bool> HasMany<TEntity, TId>(IEnumerable<TId> ids,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            var unsatisfiedSpecifications = await GetMissingIds<TEntity, TId>(ids, cancellationToken);

            return unsatisfiedSpecifications.Any();
        }

        private async Task<ICollection<IdSpecification<TEntity, TId>>> GetMissingIds<TEntity, TId>(IEnumerable<TId> ids, CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            var specifications = ids.Distinct()
                .Select(id => new IdSpecification<TEntity, TId>(id))
                .ToList();

            var matched = await _context.Set<TEntity>()
                .Apply(new AnySpecification<TEntity>(specifications))
                .Select(e => e.Id)
                .ToListAsync(cancellationToken);

            return specifications.Where(s => !matched.Contains(s.Id))
                .ToList();
        }

        public async Task<int> Count<TEntity>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot
        {
            return await _context.Set<TEntity>().Apply(specification)
                .CountAsync(cancellationToken);
        }

        public async Task<int> Count<TEntity, TId>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            return await _context.Set<TEntity>().Apply(specification)
                .CountAsync(cancellationToken);
        }

        public Task<TEntity> SingleOrDefault<TEntity>(int id, CancellationToken cancellationToken) where TEntity : class, IAggregateRoot
        {
            return SingleOrDefault(new IdSpecification<TEntity>(id), cancellationToken);
        }

        public Task<TEntity> SingleOrDefault<TEntity, TId>(TId id, CancellationToken cancellationToken) where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            return SingleOrDefault<TEntity, TId>(new IdSpecification<TEntity, TId>(id), cancellationToken);
        }

        public Task<TEntity> SingleOrDefault<TEntity>(Specification<TEntity> specification, CancellationToken cancellationToken) where TEntity : class, IAggregateRoot
        {
            return _context.SingleOrDefault(specification, cancellationToken);
        }

        public Task<TEntity> SingleOrDefault<TEntity, TId>(Specification<TEntity> specification, CancellationToken cancellationToken) where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            return _context.SingleOrDefault(specification, cancellationToken);
        }

        //With cancellationToken - Require

        public async Task HasRequired<TEntity>(Specification<TEntity> specification,
            CancellationToken cancellationToken) where TEntity : class, IAggregateRoot
        {
            var isSatisfied = await Has(specification, cancellationToken);
            if (isSatisfied)
                return;

            ThrowEntityDoesNotExist(typeof(TEntity), specification);
        }

        public async Task HasRequired<TEntity, TId>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            var isSatisfied = await Has<TEntity, TId>(specification, cancellationToken);
            if (isSatisfied)
                return;

            ThrowEntityDoesNotExist(typeof(TEntity), specification);
        }

        private static void ThrowEntityDoesNotExist(MemberInfo entityType, ISpecification specification)
        {
            throw new SpecificationViolationException(
                $@"Entity ""{entityType.Name}"" matching ""{specification.Description}"" does not exist.", specification);
        }

        public Task HasRequired<TEntity>(int id,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot
        {
            return HasRequired(new IdSpecification<TEntity>(id), cancellationToken);
        }

        public Task HasRequired<TEntity, TId>(TId id,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            return HasRequired<TEntity, TId>(new IdSpecification<TEntity, TId>(id), CancellationToken.None);
        }

        public async Task HasManyRequired<TEntity>(IEnumerable<int> ids,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot
        {
            var unsatisfiedSpecifications = await GetMissingIds<TEntity>(ids, cancellationToken);

            if (unsatisfiedSpecifications.Any())
                ThrowManyEntitiesDoesNotExist(typeof(TEntity), unsatisfiedSpecifications.ToList());
        }

        private static void ThrowManyEntitiesDoesNotExist(MemberInfo entityType, IReadOnlyCollection<ISpecification> specification)
        {
            throw new AggregateDomainException(
                $@"""{entityType.Name}"" entities matching ""{specification.Select(s => s.Description)
                    .Aggregate((a, b) => $@"{a}"" and ""{b}")}"" does not exist.",
                specification.Select(s => new SpecificationViolationException(s)));
        }

        public async Task HasManyRequired<TEntity, TId>(IEnumerable<TId> ids,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            var unsatisfiedSpecifications = await GetMissingIds<TEntity, TId>(ids, cancellationToken);

            if (unsatisfiedSpecifications.Any())
                ThrowManyEntitiesDoesNotExist(typeof(TEntity), unsatisfiedSpecifications.ToList());
        }

        public Task<TEntity> Require<TEntity>(int id, CancellationToken cancellationToken) where TEntity : class, IAggregateRoot
        {
            return Require(new IdSpecification<TEntity>(id), cancellationToken);
        }

        public Task<TEntity> Require<TEntity, TId>(TId id,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            return Require<TEntity, TId>(new IdSpecification<TEntity, TId>(id), CancellationToken.None);
        }

        public async Task<TEntity> Require<TEntity>(Specification<TEntity> specification,
            CancellationToken cancellationToken) where TEntity : class, IAggregateRoot
        {
            var entity = await SingleOrDefault(specification, cancellationToken);

            ThrowWhenNotFound(entity, specification);

            return entity;
        }

        private static void ThrowWhenNotFound<TEntity>(TEntity entity, ISpecification specification) where TEntity : class
        {
            if (entity is null)
                throw new SpecificationViolationException(
                    $@"Failed to retrieve entity ""{typeof(TEntity).Name}"" matching ""{specification.Description}"".", specification);
        }

        public async Task<TEntity> Require<TEntity, TId>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            var entity = await SingleOrDefault<TEntity, TId>(specification, cancellationToken);

            ThrowWhenNotFound(entity, specification);

            return entity;
        }

        //Without cancellationToken

        public Task<IEnumerable<TEntity>> Find<TEntity>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot
        {
            return Find(specification, CancellationToken.None);
        }

        public Task<IEnumerable<TEntity>> Find<TEntity, TId>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId
        {
            return Find<TEntity, TId>(specification, CancellationToken.None);
        }

        public Task<bool> Has<TEntity>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot
        {
            return Has(specification, CancellationToken.None);
        }

        public Task<bool> Has<TEntity, TId>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId
        {
            return Has<TEntity, TId>(specification, CancellationToken.None);
        }

        public Task<bool> Has<TEntity>(int id)
            where TEntity : class, IAggregateRoot
        {
            return Has<TEntity>(id, CancellationToken.None);
        }

        public Task<bool> Has<TEntity, TId>(TId id)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId
        {
            return Has<TEntity, TId>(id, CancellationToken.None);
        }

        public Task<bool> HasMany<TEntity>(IEnumerable<int> ids)
            where TEntity : class, IAggregateRoot
        {
            return HasMany<TEntity>(ids, CancellationToken.None);
        }

        public Task<bool> HasMany<TEntity, TId>(IEnumerable<TId> ids)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId
        {
            return HasMany<TEntity, TId>(ids, CancellationToken.None);
        }

        public Task<int> Count<TEntity>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot
        {
            return Count(specification, CancellationToken.None);
        }

        public Task<int> Count<TEntity, TId>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId
        {
            return Count<TEntity, TId>(specification, CancellationToken.None);
        }

        public Task<TEntity> SingleOrDefault<TEntity>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot
        {
            return SingleOrDefault(specification, CancellationToken.None);
        }

        public Task<TEntity> SingleOrDefault<TEntity, TId>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId
        {
            return SingleOrDefault<TEntity, TId>(specification, CancellationToken.None);
        }

        public Task<TEntity> SingleOrDefault<TEntity>(int id)
            where TEntity : class, IAggregateRoot
        {
            return SingleOrDefault<TEntity>(id, CancellationToken.None);
        }

        public Task<TEntity> SingleOrDefault<TEntity, TId>(TId id)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId
        {
            return SingleOrDefault<TEntity, TId>(id, CancellationToken.None);
        }

        //Without cancellationToken - Require

        public Task HasRequired<TEntity>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot
        {
            return HasRequired(specification, CancellationToken.None);
        }

        public Task HasRequired<TEntity, TId>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            return HasRequired<TEntity, TId>(specification, CancellationToken.None);
        }

        public Task HasRequired<TEntity>(int id)
            where TEntity : class, IAggregateRoot
        {
            return HasRequired<TEntity>(id, CancellationToken.None);
        }

        public Task HasRequired<TEntity, TId>(TId id)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            return HasRequired<TEntity, TId>(id, CancellationToken.None);
        }

        public Task HasManyRequired<TEntity>(IEnumerable<int> ids)
            where TEntity : class, IAggregateRoot
        {
            return HasManyRequired<TEntity>(ids, CancellationToken.None);
        }

        public Task HasManyRequired<TEntity, TId>(IEnumerable<TId> ids)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            return HasManyRequired<TEntity, TId>(ids, CancellationToken.None);
        }

        public Task<TEntity> Require<TEntity>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot
        {
            return Require(specification, CancellationToken.None);
        }

        public Task<TEntity> Require<TEntity, TId>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            return Require<TEntity, TId>(specification, CancellationToken.None);
        }

        public Task<TEntity> Require<TEntity>(int id)
            where TEntity : class, IAggregateRoot
        {
            return Require<TEntity>(id, CancellationToken.None);
        }

        public Task<TEntity> Require<TEntity, TId>(TId id)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId
        {
            return Require<TEntity, TId>(id, CancellationToken.None);
        }
    }
}