﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Infrastructure
{
    internal class MediatorAggregateRootCreatedEventPublisher : MediatorEventPublisher
    {
        public MediatorAggregateRootCreatedEventPublisher(DbContext context, IMediator mediator) : base(context, mediator)
        {
        }

        public override async Task Publish()
        {
            var entities = Context.ChangeTracker
                .Entries()
                .Where(e => e.State == EntityState.Added)
                .Select(e => e.Entity)
                .ToList();
            
            foreach (var entity in entities)
            {
                if (!TryGetEventType(entity.GetType(), out var eventType))
                    continue;

                var @event = CreateEvent(entity, eventType);

                await Mediator.Publish(@event, CancellationToken.None);
            }
        }
        
        private static bool TryGetEventType(Type entityType, out Type eventType)
        {
            eventType = null;

            foreach (var @interface in entityType.GetInterfaces())
            {
                //Check entity type
                if (@interface == typeof(IAggregateRoot))
                {
                    //Create entity event type
                    eventType = typeof(AggregateRootCreatedEvent<>).MakeGenericType(entityType);
                    return true;
                }

                //Check entity type
                if (@interface.IsGenericType && @interface.GetGenericTypeDefinition() == typeof(IAggregateRoot<>))
                {
                    //Create entity event type
                    var idType = @interface.GetGenericArguments().Single();

                    eventType = typeof(AggregateRootCreatedEvent<,>).MakeGenericType(entityType, idType);
                    return true;
                }
            }

            return false;
        }

        private static IEntityEvent CreateEvent(object entity, Type eventType)
        {
            var id = entity.GetType().GetProperty("Id").GetValue(entity);

            return (IEntityEvent)Activator.CreateInstance(eventType, id);
        }
    }
}
