﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Infrastructure
{
    public interface IUnitOfWorkDbContextPlugin
	{
		Task Commit(DbConnection dbConnection, IDbContextTransaction transaction, CancellationToken token);
	}
}