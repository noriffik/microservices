﻿using System.Net.Http;

namespace NexCore.Infrastructure
{
    public interface IHttpClientFactory
    {
        HttpClient Create();
    }
}
