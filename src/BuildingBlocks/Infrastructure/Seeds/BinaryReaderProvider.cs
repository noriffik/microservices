﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace NexCore.Infrastructure.Seeds
{
    public class BinaryReaderProvider : IBinaryReaderProvider
    {
        private readonly string _filePath;

        public BinaryReaderProvider(string filePath)
        {
            _filePath = filePath ?? throw new ArgumentNullException(nameof(filePath));
        }

        public Task<byte[]> Get()
        {
            return File.ReadAllBytesAsync(_filePath);
        }
    }
}
