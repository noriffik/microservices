﻿using System.IO;

namespace NexCore.Infrastructure.Seeds
{
    public interface ITextReaderProvider
    {
        TextReader Get();
    }
}