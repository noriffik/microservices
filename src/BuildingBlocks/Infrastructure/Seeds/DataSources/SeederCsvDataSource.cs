﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Infrastructure.Seeds.DataSources
{
    public class SeederCsvDataSource<TRecord> : ISeederDataSource<TRecord>
        where TRecord : class
    {
        private readonly ITextReaderProvider _readerProvider;

        public SeederCsvDataSource(ITextReaderProvider readerProvider)
        {
            _readerProvider = readerProvider ?? throw new ArgumentNullException(nameof(readerProvider));
        }

        public IEnumerable<TRecord> Get()
        {
            using (var reader = _readerProvider.Get())
            using (var csv = new CsvReader(reader, new Configuration { Delimiter = ";" }))
            {
                return csv.GetRecords<TRecord>().ToList();
            }
        }
    }
}