﻿using AutoMapper;
using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;

namespace NexCore.Infrastructure.Seeds.DataSources
{
    public class EntitySeederCsvDataSource<TEntity, TRecord> : ISeederDataSource<TEntity>
        where TEntity : class
    {
        private readonly IMapper _mapper;
        private readonly ITextReaderProvider _readerProvider;

        public EntitySeederCsvDataSource(IMapper mapper, ITextReaderProvider readerProvider)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _readerProvider = readerProvider ?? throw new ArgumentNullException(nameof(readerProvider));
        }

        public IEnumerable<TEntity> Get()
        {
            using (var reader = _readerProvider.Get())
            using (var csv = new CsvReader(reader, new Configuration { Delimiter = ";" }))
            {
                return _mapper.Map<IEnumerable<TRecord>, IEnumerable<TEntity>>(csv.GetRecords<TRecord>());
            }
        }
    }
}