﻿using AutoMapper;
using System;
using System.Collections.Generic;

namespace NexCore.Infrastructure.Seeds.DataSources
{
    public class CompositeEntitySeederCsvDataSource<TEntity, TRecord> : ISeederDataSource<TEntity>
        where TEntity : class
    {
        private readonly IMapper _mapper;
        private readonly ITextReaderProvider[] _readerProviders;

        public CompositeEntitySeederCsvDataSource(IMapper mapper, params ITextReaderProvider[] readerProviders)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _readerProviders = readerProviders ?? throw new ArgumentNullException(nameof(readerProviders));
        }

        public IEnumerable<TEntity> Get()
        {
            var entities = new List<TEntity>();
            foreach (var provider in _readerProviders)
            {
                var inner = new EntitySeederCsvDataSource<TEntity, TRecord>(_mapper, provider);

                entities.AddRange(inner.Get());
            }

            return entities;
        }
    }
}