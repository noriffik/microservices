﻿using System.Collections.Generic;

namespace NexCore.Infrastructure.Seeds
{
    public interface ISeederDataSource<T> where T : class
    {
        IEnumerable<T> Get();
    }
}