﻿using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.Infrastructure.Seeds
{
    public class EntitySeeder<TContext, TEntity, TId> : ISeeder<TEntity>
        where TEntity : class, IAggregateRoot<TId>
        where TId : EntityId
        where TContext : DbContext
    {
        private readonly DbContext _dbContext;
        private readonly ISeederDataSource<TEntity> _dataSource;

        public EntitySeeder(TContext dbContext, ISeederDataSource<TEntity> dataSource)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _dataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
        }

        public Task Seed()
        {
            _dataSource.Get()
                .Where(IsNotSeeded)
                .ToList()
                .ForEach(m => _dbContext.Add(m));

            return _dbContext.SaveChangesAsync();
        }

        private bool IsNotSeeded(TEntity entity)
        {
            return !_dbContext.Set<TEntity>()
                .Any(m => Convert.ToString(m.Id) == entity.Id.Value);
        }
    }

    public class EntitySeeder<TContext, TEntity> : ISeeder<TEntity>
        where TEntity : class, IAggregateRoot
        where TContext : DbContext
    {
        private readonly DbContext _dbContext;
        private readonly ISeederDataSource<TEntity> _dataSource;

        public EntitySeeder(TContext dbContext, ISeederDataSource<TEntity> dataSource)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _dataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
        }

        public async Task Seed()
        {
            var hasAny = await _dbContext.Set<TEntity>().AnyAsync();
            if (hasAny) return;

            _dataSource.Get()
                .ToList()
                .ForEach(m => _dbContext.Add(m));

            await _dbContext.SaveChangesAsync();
        }
    }
}
