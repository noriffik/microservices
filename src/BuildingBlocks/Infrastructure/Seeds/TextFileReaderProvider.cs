﻿using System;
using System.IO;
using System.Text;

namespace NexCore.Infrastructure.Seeds
{
    public class TextFileReaderProvider : ITextReaderProvider
    {
        private readonly string _filePath;

        public TextFileReaderProvider(string filePath)
        {
            _filePath = filePath ?? throw new ArgumentNullException(nameof(filePath));
        }

        public TextReader Get()
        {
            return new StreamReader(_filePath, Encoding.UTF8);
        }
    }
}