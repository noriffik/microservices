﻿using System;
using System.IO;
using System.Resources;

namespace NexCore.Infrastructure.Seeds
{
    public class ResourceReaderProvider : ITextReaderProvider
    {
        private readonly string _name;
        private readonly ResourceManager _resourceManager;

        public ResourceReaderProvider(string name, ResourceManager resourceManager)
        {
            _name = name ?? throw new ArgumentNullException(nameof(name));
            _resourceManager = resourceManager ?? throw new ArgumentNullException(nameof(resourceManager));
        }

        public TextReader Get()
        {
            return new StringReader(_resourceManager.GetString(_name));
        }
    }
}
