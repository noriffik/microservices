﻿using System.Threading.Tasks;

namespace NexCore.Infrastructure.Seeds
{
    public interface IBinaryReaderProvider
    {
        Task<byte[]> Get();
    }
}