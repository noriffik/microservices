﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.Infrastructure.Seeds
{
    public class CompositeSeeder : ISeeder
    {
        private readonly List<ISeeder> _seeders;

        public CompositeSeeder(params ISeeder[] seeders)
        {
            _seeders = seeders.ToList();
        }
        
        public async Task Seed()
        {
            foreach (var seeder in _seeders)
                await seeder.Seed();
        }
    }
}
