﻿using Microsoft.EntityFrameworkCore;

namespace NexCore.Infrastructure.Seeds
{
    public interface IDbContextSeeder<TContext> : ISeeder where TContext : DbContext
    {
    }
}
