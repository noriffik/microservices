﻿using System;
using System.IO;

namespace NexCore.Infrastructure.Seeds
{
    public class StringReaderProvider : ITextReaderProvider
    {
        private readonly string _text;

        public StringReaderProvider(string text)
        {
            _text = text ?? throw new ArgumentNullException(nameof(text));
        }

        public TextReader Get()
        {
            return new StringReader(_text);
        }
    }
}