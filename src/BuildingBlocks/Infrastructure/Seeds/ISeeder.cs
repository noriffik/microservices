﻿using System.Threading.Tasks;

namespace NexCore.Infrastructure.Seeds
{
    public interface ISeeder<T> : ISeeder
    {
    }

    public interface ISeeder
    {
        Task Seed();
    }
}