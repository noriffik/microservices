﻿using Microsoft.EntityFrameworkCore;
using NexCore.Application.Queries;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Infrastructure.QueryHandlers
{
    public static class QueryPagination
    {
        public static async Task<PagedResponse<T>> Paginate<T>(this IQueryable<T> query, PagedRequest request, CancellationToken cancellationToken)
        {
            var page = PageOptions.From(request);

            var total = await query.CountAsync(cancellationToken);

            var items = await query
                .Skip((page.Number - 1) * page.Size)
                .Take(page.Size)
                .ToArrayAsync(cancellationToken);

            return new PagedResponse<T>(page, total, items);
        }
    }
}
