﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Infrastructure.Utility
{
    public interface IResilientTransaction
	{
		Task ExecuteAsync(DbContext context, Func<DbConnection, IDbContextTransaction, Task> action, CancellationToken cancellationToken);
	}
}