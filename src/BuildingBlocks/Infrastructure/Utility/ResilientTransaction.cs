﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Infrastructure.Utility
{
    public class ResilientTransaction : IResilientTransaction
	{
		public async Task ExecuteAsync(DbContext context, Func<DbConnection, IDbContextTransaction, Task> action, CancellationToken cancellationToken)
		{
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            if (action == null)
                throw new ArgumentNullException(nameof(action));

            var strategy = context.Database.CreateExecutionStrategy();

			await strategy.ExecuteAsync(async () =>
			{
				using (var transaction = context.Database.BeginTransaction())
				{
                    await context.SaveChangesAsync(cancellationToken);

					await action(context.Database.GetDbConnection(), transaction);

					transaction.Commit();
				}
			});
		}
	}
}