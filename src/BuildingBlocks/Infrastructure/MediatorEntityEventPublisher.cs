﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Infrastructure
{
    internal class MediatorEntityEventPublisher : MediatorEventPublisher
    {
        public const int EventLimit = 1000;
        
        private int _eventsProcessed;

        public MediatorEntityEventPublisher(DbContext context, IMediator mediator)
            : base(context, mediator)
        {
        }

        public override async Task Publish()
        {
            while (AnyEntitiesWithEvents())
            {
                var entity = Context.ChangeTracker
                    .Entries<IEventful>()
                    .Select(e => e.Entity)
                    .First(e => e.EntityEvents.Any());

                await PublishEntityEvents(entity);
            }
        }

        private async Task PublishEntityEvents(IEventful eventful)
        {
            while (eventful.EntityEvents.Any())
            {
                if (_eventsProcessed == EventLimit)
                {
                    _eventsProcessed = 0;

                    throw new InvalidOperationException($"Domain event limit of {EventLimit} per transaction has been exceeded");
                }

                var @event = eventful.EntityEvents.First();

                await Mediator.Publish(@event, CancellationToken.None);

                eventful.RemoveEntityEvent(@event);

                _eventsProcessed++;
            }
        }

        private bool AnyEntitiesWithEvents()
        {
            return Context.ChangeTracker
                .Entries<IEventful>()
                .Select(e => e.Entity)
                .Any(s => s.EntityEvents.Any());
        }
    }
}