﻿using Microsoft.Extensions.Options;
using NexCore.Application.Setup;
using Polly;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NexCore.Infrastructure.Setup
{
    public class MigrationServiceWaitTask : IApplicationSetupTask
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly MigrationServiceWaitOptions _options;

        public MigrationServiceWaitTask(IOptionsMonitor<MigrationServiceWaitOptions> options)
            : this(new DefaultHttpClientFactory(), options)
        {
        }

        public MigrationServiceWaitTask(IHttpClientFactory httpClientFactory, IOptionsMonitor<MigrationServiceWaitOptions> options)
        {
            if (options is null)
                throw new ArgumentNullException(nameof(options));

            _httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
            _options = options.CurrentValue;
        }

        public async Task Execute()
        {
            var policy = Policy
                .Handle<HttpRequestException>()
                .WaitAndRetryAsync(_options.RetryCount, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));

            var httpClient = _httpClientFactory.Create();

            await policy.ExecuteAsync(() => httpClient.GetAsync(_options.HealthUrl));
        }
    }
}
