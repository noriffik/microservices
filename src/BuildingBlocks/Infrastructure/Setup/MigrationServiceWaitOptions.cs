﻿using System.ComponentModel.DataAnnotations;

namespace NexCore.Infrastructure.Setup
{
    public class MigrationServiceWaitOptions
    {
        [Required]
        public string HealthUrl { get; set; }

        [Range(0, int.MaxValue)]
        public int RetryCount { get; set; } = 5;
    }
}
