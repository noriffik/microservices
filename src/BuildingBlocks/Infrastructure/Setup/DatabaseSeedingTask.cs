﻿using Microsoft.EntityFrameworkCore;
using NexCore.Application.Setup;
using NexCore.Infrastructure.Seeds;
using Polly;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace NexCore.Infrastructure.Setup
{
    public class DatabaseSeedingTask<TContext> : IApplicationSetupTask where TContext : DbContext
    {
        private readonly IDbContextSeeder<TContext> _seeder;

        public DatabaseSeedingTask(IDbContextSeeder<TContext> seeder)
        {
            _seeder = seeder ?? throw new ArgumentNullException(nameof(seeder));
        }

        public async Task Execute()
        {
            await Policy.Handle<SqlException>()
                .WaitAndRetry(new[]
                {
                    TimeSpan.FromSeconds(3),
                    TimeSpan.FromSeconds(5),
                    TimeSpan.FromSeconds(8)
                })
                .Execute(async () => await _seeder.Seed());
        }
    }
}
