﻿using Microsoft.EntityFrameworkCore;
using NexCore.Application.Setup;
using NexCore.Infrastructure.Seeds;
using Polly;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace NexCore.Infrastructure.Setup
{
    public class DatabaseSetupTask<TContext> : IApplicationSetupTask where TContext : DbContext
    {
        protected bool IsSeedingEnabled = true;

        private readonly TContext _context;
        private readonly IDbContextSeeder<TContext> _seeder;

        public DatabaseSetupTask(TContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public DatabaseSetupTask(TContext context, IDbContextSeeder<TContext> seeder)
            : this(context)
        {
            _seeder = seeder ?? throw new ArgumentNullException(nameof(seeder));
        }

        public async Task Execute()
        {
            await Policy.Handle<SqlException>()
                .WaitAndRetry(new[]
                {
                    TimeSpan.FromSeconds(3),
                    TimeSpan.FromSeconds(5),
                    TimeSpan.FromSeconds(8)
                })
                .Execute(async () =>
                {
                    _context.Database.Migrate();

                    if (IsSeedingEnabled && _seeder != null)
                        await _seeder.Seed();
                });
        }
    }
}
