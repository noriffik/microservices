﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using NexCore.Domain;
using NexCore.Infrastructure.Utility;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("NexCore.Testing")]
[assembly: InternalsVisibleTo("NexCore.Infrastructure.UnitTests")]

namespace NexCore.Infrastructure
{
    public abstract class UnitOfWorkDbContext : DbContext, IUnitOfWork
	{
        private IResilientTransaction _resilientTransaction;
        public IEnumerable<IEntityEventPublisher> EventPublishers { get; }

		public IEnumerable<IUnitOfWorkDbContextPlugin> Plugins { get; }

		protected UnitOfWorkDbContext(DbContextOptions options) : base(options)
		{
			EventPublishers = new List<IEntityEventPublisher>();
			EntityRepository = new DbContextEntityRepository(this);
			Plugins = new List<IUnitOfWorkDbContextPlugin>();
			ResilientTransaction = new ResilientTransaction();
		}

		protected UnitOfWorkDbContext(DbContextOptions options, IMediator mediator) : base(options)
        {
            if (mediator == null)
                throw new ArgumentNullException(nameof(mediator));

            EntityRepository = new DbContextEntityRepository(this);
            EventPublishers = DefaultEventPublishers(this, mediator);
            Plugins = new List<IUnitOfWorkDbContextPlugin>();
            ResilientTransaction = new ResilientTransaction();
        }

        protected UnitOfWorkDbContext(DbContextOptions options, IMediator mediator, 
			IEnumerable<IUnitOfWorkDbContextPlugin> plugins) :
			this(options, mediator)
		{
            EntityRepository = new DbContextEntityRepository(this);
            EventPublishers = DefaultEventPublishers(this, mediator);
            Plugins = plugins ?? throw new ArgumentNullException(nameof(plugins));
            ResilientTransaction = new ResilientTransaction();
		}

		protected UnitOfWorkDbContext(DbContextOptions options, IEnumerable<IEntityEventPublisher> eventPublishers, 
			IEnumerable<IUnitOfWorkDbContextPlugin> plugins) :
			base(options)
		{
			EntityRepository = new DbContextEntityRepository(this);
			EventPublishers = eventPublishers ?? throw new ArgumentNullException(nameof(eventPublishers));
			Plugins = plugins ?? throw new ArgumentNullException(nameof(plugins));
            ResilientTransaction = new ResilientTransaction();
		}

        private static IEnumerable<IEntityEventPublisher> DefaultEventPublishers(DbContext context, IMediator mediator)
        {
            return new IEntityEventPublisher[]
            {
                new MediatorAggregateRootCreatedEventPublisher(context, mediator),
                new MediatorEntityEventPublisher(context, mediator)
            };
        }

		public IEntityRepository EntityRepository { get; }

        public IResilientTransaction ResilientTransaction
        {
            get => _resilientTransaction;
            set
            {
                if (value != null) _resilientTransaction = value;
            }
        }

        public virtual async Task Commit(CancellationToken cancellationToken = default)
		{
            await PublishEvents();

			try
			{
				await ResilientTransaction.ExecuteAsync(this, (con, tran) => CommitPlugins(con, tran, cancellationToken), cancellationToken);
			}
			catch (DbUpdateConcurrencyException e)
			{
				throw new WorkConcurrencyException(e);
			}
			catch (DbUpdateException e)
			{
				throw new WorkFailedException(e);
			}
        }

		private async Task CommitPlugins(DbConnection connection, IDbContextTransaction transaction, CancellationToken token)
		{
            foreach (var plugin in Plugins)
                await plugin.Commit(connection, transaction, token);
        }

		private async Task PublishEvents()
		{
			foreach (var eventPublisher in EventPublishers)
			{
				await eventPublisher.Publish();
			}
		}
	}
}
