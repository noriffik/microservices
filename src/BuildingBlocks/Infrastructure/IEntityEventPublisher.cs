﻿using System.Threading.Tasks;

namespace NexCore.Infrastructure
{
    public interface IEntityEventPublisher
    {
        Task Publish();
    }
}