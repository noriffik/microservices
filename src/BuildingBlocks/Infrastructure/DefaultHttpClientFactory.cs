﻿using System.Net.Http;

namespace NexCore.Infrastructure
{
    public class DefaultHttpClientFactory : IHttpClientFactory
    {
        public HttpClient Create() => new HttpClient();
    }
}
