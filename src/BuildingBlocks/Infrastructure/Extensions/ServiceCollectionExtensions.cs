﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Domain;
using System;
using System.Reflection;

namespace NexCore.Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDbContext<TDbContext>(
            this IServiceCollection services, string connectionString, IConfiguration configuration) where TDbContext : DbContext
        {
            return services.AddDbContext<TDbContext>(connectionString, configuration, typeof(TDbContext).GetTypeInfo().Assembly);
        }

        public static IServiceCollection AddDbContext<TDbContext>(
            this IServiceCollection services, string connectionString, IConfiguration configuration, Assembly migrationAssembly) where TDbContext : DbContext
        {
            services.AddEntityFrameworkSqlServer()
                .AddDbContext<TDbContext>(
                    options => {
                        options.UseSqlServer(
                            connectionString,
                            sqlOptions =>
                            {
                                sqlOptions.MigrationsAssembly(migrationAssembly.GetName().Name);

                                var maxRetryCount = configuration.GetValue("SqlServer:Connection:MaxRetryCount", 10);
                                var maxRetryDelayInSeconds = configuration.GetValue("SqlServer:Connection:MaxRetryDelayInSeconds", 30);
                                
                                sqlOptions.EnableRetryOnFailure(
                                    maxRetryCount, TimeSpan.FromSeconds(maxRetryDelayInSeconds), null);
                            });
                    },
                    ServiceLifetime.Scoped);

            return services;
        }

        public static IServiceCollection AddUnitOfWork<TDbContext>(
            this IServiceCollection services, string connectionString, IConfiguration configuration) where TDbContext : DbContext, IUnitOfWork
        {
            services.AddDbContext<TDbContext>(connectionString, configuration);

            services.AddScoped<IUnitOfWork>(s => s.GetRequiredService<TDbContext>());

            services.AddScoped(s => s.GetRequiredService<IUnitOfWork>().EntityRepository);

            return services;
        }
    }
}
