﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using NexCore.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Infrastructure.Extensions
{
    public static class DbContextExtensions
    {
        public static IEnumerable<T> Apply<T>(this ChangeTracker changeTracker, Specification<T> specification) where T : class
        {
            return changeTracker.Entries<T>()
                .Select(e => e.Entity)
                .Apply(specification);
        }

        public static async Task<T> SingleOrDefault<T>(this DbContext context,
            Specification<T> specification, CancellationToken cancellationToken) where T : class
        {
            var entity = context.ChangeTracker
                .Apply(specification)
                .SingleOrDefault();

            if (entity != null)
                return entity;

            return await context.Set<T>()
                .Apply(specification)
                .SingleOrDefaultAsync(cancellationToken);
        }

        public static async Task<bool> Has<T>(this DbContext context,
            Specification<T> specification, CancellationToken cancellationToken) where T : class
        {
            if (context.ChangeTracker
                .Apply(specification)
                .Any())
            {
                return true;
            }

            return await context.Set<T>()
                .Apply(specification)
                .AnyAsync(cancellationToken);
        }
    }
}
