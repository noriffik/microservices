﻿using System.Threading.Tasks;

namespace NexCore.EventBus.Abstract
{
	public interface IIntegrationEventHandler<in TEvent> : IIntegrationEventHandler 
		where TEvent : IntegrationEvent
	{
		Task Handle(TEvent @event);
	}

	public interface IIntegrationEventHandler
    {
    }
}
