﻿using System;

namespace NexCore.EventBus.Exceptions
{
	public class EventBusException: Exception
	{
		public EventBusException()
		{
		}

		public EventBusException(string message) : base(message)
		{
		}

		public EventBusException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}