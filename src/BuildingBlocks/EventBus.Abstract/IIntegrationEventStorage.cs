﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexCore.EventBus.Abstract
{
    public interface IIntegrationEventStorage
	{
		Task<IEnumerable<IntegrationEvent>> Get();
		Task Add(IntegrationEvent @event);
        Task AddRange(IEnumerable<IntegrationEvent> events);
        Task Clear();
    }
}