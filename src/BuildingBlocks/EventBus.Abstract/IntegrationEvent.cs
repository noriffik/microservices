﻿using Newtonsoft.Json;
using NexCore.Common;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.EventBus.Abstract
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required for serialization purposes")]
    public class IntegrationEvent
    {
		[JsonProperty]
        public Guid Id { get; private set; }

        [JsonProperty]
		public DateTime CreatedAt { get; private set; }

		public IntegrationEvent() : this(GuidFactory.Instance.Next(), SystemTimeProvider.Instance.Get())
        {
        }

        [JsonConstructor]
        public IntegrationEvent(Guid id, DateTime createdAt)
        {
            Id = id;
            CreatedAt = createdAt;
        }
    }
}
