﻿using System.Threading.Tasks;

namespace NexCore.EventBus.Abstract
{
	public interface IDynamicIntegrationEventHandler
	{
		Task Handle(dynamic eventData);
	}
}