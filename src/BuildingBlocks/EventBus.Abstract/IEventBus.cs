﻿using System.Collections.Generic;

namespace NexCore.EventBus.Abstract
{
    public interface IEventBus
    {
        void Publish(IntegrationEvent @event);

        void Publish(IEnumerable<IntegrationEvent> events);

        void Subscribe<TEvent, TEventHandler>()
            where TEvent : IntegrationEvent
            where TEventHandler : IIntegrationEventHandler<TEvent>;

        void SubscribeDynamic<TH>(string eventName)
	        where TH : IDynamicIntegrationEventHandler;

        void UnsubscribeDynamic<TH>(string eventName)
	        where TH : IDynamicIntegrationEventHandler;

		void Unsubscribe<TEvent, TEventHandler>()
            where TEvent : IntegrationEvent
            where TEventHandler : IIntegrationEventHandler<TEvent>;
    }
}
