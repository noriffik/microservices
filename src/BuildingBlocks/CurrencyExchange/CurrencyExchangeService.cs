﻿using NexCore.CurrencyExchange.Abstract;
using NexCore.Domain;
using System;
using System.Threading.Tasks;
using NexCore.Common;

namespace NexCore.CurrencyExchange
{
    public class CurrencyExchangeService : ICurrencyExchangeService
    {
        private readonly ICurrencyRateDataSource _rateDataSource;

        public CurrencyExchangeService(ICurrencyRateDataSource rateDataSource)
        {
            _rateDataSource = rateDataSource ?? throw new ArgumentNullException(nameof(rateDataSource));
        }

        public Task<decimal> Convert(Currency from, Currency to, decimal sum) =>
            Convert(from, to, sum, SystemTimeProvider.Instance.Get());

        public async Task<decimal> Convert(Currency from, Currency to, decimal sum, DateTime onDate)
        {
            if (from == null)
                throw new ArgumentNullException(nameof(from));
            if (to == null)
                throw new ArgumentNullException(nameof(to));

            var rate = await _rateDataSource.Get(from, to, onDate);

            return rate * sum;
        }

        public Task<decimal> GetRate(Currency from, Currency to) =>
            GetRate(from, to, SystemTimeProvider.Instance.Get());

        public Task<decimal> GetRate(Currency from, Currency to, DateTime onDate)
        {
            if (from == null)
                throw new ArgumentNullException(nameof(from));
            if (to == null)
                throw new ArgumentNullException(nameof(to));

            return _rateDataSource.Get(from, to, onDate);
        }
    }
}
