﻿using NexCore.CurrencyExchange.Abstract;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace NexCore.CurrencyExchange.DataSources.InMemory
{
    public class InMemoryDataSource : BaseDataSource
    {
        private readonly ConcurrentDictionary<(Currency, Currency, DateTime), decimal> _rates;

        public InMemoryDataSource(ICurrencyRateDataSource inner = null) : base(inner)
        {
            _rates = new ConcurrentDictionary<(Currency, Currency, DateTime), decimal>();
        }

        protected override Task<decimal?> TryGetLocally(Currency from, Currency to, DateTime onDate)
        {
            if (_rates.TryGetValue((from, to, onDate.Date), out var rate))
                return Task.FromResult((decimal?)rate);

            return Task.FromResult((decimal?)null);
        }

        protected override Task Cache(Currency from, Currency to, DateTime onDate, decimal rate)
        {
            var tuple = (from, Currency.Uah, onDate.Date);

            _rates.AddOrUpdate(tuple, rate, (key, oldValue) => rate);

            return Task.CompletedTask;
        }
    }
}
