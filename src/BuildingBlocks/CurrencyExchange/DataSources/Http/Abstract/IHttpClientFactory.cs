﻿using System.Net.Http;

namespace NexCore.CurrencyExchange.DataSources.Http.Abstract
{
    public interface IHttpClientFactory
    {
        HttpClient GetHttpClient();
    }
}