﻿namespace NexCore.CurrencyExchange.DataSources.Http.Abstract
{
    public interface IHttpResponseInterpreter
    {
        decimal? Interpret(string response);
    }
}
