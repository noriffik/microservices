﻿using NexCore.CurrencyExchange.Abstract;
using System;

namespace NexCore.CurrencyExchange.DataSources.Http.Abstract
{
    public interface IUrlProvider
    {
        Uri Get(Currency from, Currency to, DateTime onDate);
    }
}