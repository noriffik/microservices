﻿using Newtonsoft.Json;
using NexCore.CurrencyExchange.DataSources.Http.Abstract;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace NexCore.CurrencyExchange.DataSources.Http.Nbu
{
    public class NbuHttpResponseInterpreter : IHttpResponseInterpreter
    {
        public decimal? Interpret(string response)
        {
            try
            {
                var rate = RateDto.MapFromJson(response);

                if (rate == null || rate.Value == default(int))
                    return null;

                return rate.Value;
            }
            catch (JsonReaderException)
            {
                return null;
            }
        }

        [DataContract]
        private class RateDto
        {
            [DataMember(Name = "rate")]
            public decimal Value { get; set; }

            public static RateDto MapFromJson(string json)
            {
                return JsonConvert.DeserializeObject<IEnumerable<RateDto>>(json)
                    .SingleOrDefault();
            }
        }
    }
}
