﻿using NexCore.CurrencyExchange.Abstract;
using NexCore.CurrencyExchange.DataSources.Http.Abstract;
using System;
using System.Text;

namespace NexCore.CurrencyExchange.DataSources.Http.Nbu
{
    public class NbuUrlProvider : IUrlProvider
    {
        private readonly string _urlTemplate;

        public NbuUrlProvider(string urlTemplate)
        {
            _urlTemplate = urlTemplate ?? throw new ArgumentNullException(nameof(urlTemplate));
        }
        
        public Uri Get(Currency from, Currency to, DateTime onDate)
        {
            if (from == null)
                throw new ArgumentNullException(nameof(from));
            if (to == null)
                throw new ArgumentNullException(nameof(to));

            var builder = new StringBuilder(_urlTemplate);
            builder.Replace("<![FROM]>", from.Code);
            builder.Replace("<![TO]>", to.Code);
            builder.Replace("<![DATE]>", onDate.ToString("yyyyMMdd"));

            return new Uri(builder.ToString());
        }
    }
}
