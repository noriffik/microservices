﻿using NexCore.CurrencyExchange.Abstract;
using NexCore.CurrencyExchange.DataSources.Http.Abstract;
using System;
using System.Threading.Tasks;

namespace NexCore.CurrencyExchange.DataSources.Http.Nbu
{
    public class NbuHttpDataSource : BaseHttpDataSource
    {
        public NbuHttpDataSource(IUrlProvider urlProvider, IHttpClientFactory httpFactory, ICurrencyRateDataSource inner = null)
            : base(urlProvider, httpFactory, new NbuHttpResponseInterpreter(), inner)
        {
        }

        protected override Task<decimal?> TryGetLocally(Currency from, Currency to, DateTime onDate)
        {
            if (to != Currency.Uah)
                throw new ArgumentException("to parameter must be Uah", nameof(to));

            return base.TryGetLocally(from, to, onDate);
        }
    }
}
