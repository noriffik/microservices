﻿using NexCore.CurrencyExchange.DataSources.Http.Abstract;
using System.Net.Http;

namespace NexCore.CurrencyExchange.DataSources.Http
{
    public class HttpClientFactory : IHttpClientFactory
    {
        public HttpClient GetHttpClient()
        {
            return new HttpClient();
        }
    }
}
