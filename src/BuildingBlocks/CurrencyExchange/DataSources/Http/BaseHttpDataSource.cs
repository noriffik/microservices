﻿using NexCore.CurrencyExchange.Abstract;
using NexCore.CurrencyExchange.DataSources.Http.Abstract;
using Polly;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NexCore.CurrencyExchange.DataSources.Http
{
    public abstract class BaseHttpDataSource : BaseDataSource
    {
        private readonly IUrlProvider _urlProvider;
        private readonly IHttpClientFactory _httpFactory;
        private readonly IHttpResponseInterpreter _responseInterpreter;

        protected BaseHttpDataSource(IUrlProvider urlProvider, IHttpClientFactory httpFactory, IHttpResponseInterpreter responseInterpreter, ICurrencyRateDataSource inner = null) : base(inner)
        {
            _urlProvider = urlProvider ?? throw new ArgumentNullException(nameof(urlProvider));
            _httpFactory = httpFactory ?? throw new ArgumentNullException(nameof(httpFactory));
            _responseInterpreter = responseInterpreter ?? throw new ArgumentNullException(nameof(responseInterpreter));
        }

        protected override async Task<decimal?> TryGetLocally(Currency from, Currency to, DateTime onDate)
        {
            try
            {
                return await GetRateResponse(from, to, onDate);
            }
            catch (HttpRequestException)
            {
                return null;
            }
        }

        private async Task<decimal?> GetRateResponse(Currency from, Currency to, DateTime onDate)
        {
            var policy = Policy
                .Handle<HttpRequestException>()
                .RetryAsync(3);

            var httpClient = _httpFactory.GetHttpClient();

            var response = await policy.ExecuteAsync(() =>
                httpClient.GetAsync(_urlProvider.Get(from, to, onDate)));

            return _responseInterpreter.Interpret(await response.Content.ReadAsStringAsync());
        }

        protected override Task Cache(Currency from, Currency to, DateTime onDate, decimal rate) => Task.CompletedTask;
    }
}
