﻿using NexCore.CurrencyExchange.Abstract;
using System;
using System.Threading.Tasks;

namespace NexCore.CurrencyExchange.DataSources
{
    public abstract class BaseDataSource : ICurrencyRateDataSource
    {
        private readonly ICurrencyRateDataSource _inner;

        protected BaseDataSource(ICurrencyRateDataSource inner = null)
        {
            _inner = inner;
        }

        public virtual async Task<decimal?> TryGet(Currency from, Currency to, DateTime onDate)
        {
            if (from == null)
                throw new ArgumentNullException(nameof(from));
            if (to == null)
                throw new ArgumentNullException(nameof(to));

            if (from == to)
                return 1;

            var rate = await TryGetLocally(from, to, onDate);

            if (rate.HasValue || _inner == null)
                return rate;

            rate = await _inner.TryGet(from, to, onDate);

            if (rate.HasValue)
                await Cache(from, to, onDate, rate.Value);

            return rate;
        }

        protected abstract Task<decimal?> TryGetLocally(Currency from, Currency to, DateTime onDate);

        protected abstract Task Cache(Currency from, Currency to, DateTime onDate, decimal rate);

        public virtual async Task<decimal> Get(Currency from, Currency to, DateTime onDate)
        {
            if (from == null)
                throw new ArgumentNullException(nameof(from));
            if (to == null)
                throw new ArgumentNullException(nameof(to));

            var rate = await TryGet(from, to, onDate);

            if (rate.HasValue)
                return rate.Value;

            throw new CurrencyExchangeException(
                $"Failed to get currency exchange from {GetType().Name}");
        }
    }
}
