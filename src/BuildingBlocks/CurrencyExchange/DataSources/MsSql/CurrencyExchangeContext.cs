﻿using Microsoft.EntityFrameworkCore;
using NexCore.CurrencyExchange.Abstract;
using NexCore.CurrencyExchange.DataSources.MsSql.EntityTypes;

namespace NexCore.CurrencyExchange.DataSources.MsSql
{
    public class CurrencyExchangeContext : DbContext
    {
        public const string Schema = "dbo";

        public DbSet<Rate> Rates { get; set; }

        public CurrencyExchangeContext(DbContextOptions<CurrencyExchangeContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.ApplyConfiguration(new RateTypeConfiguration(Schema));
        }
    }
}
