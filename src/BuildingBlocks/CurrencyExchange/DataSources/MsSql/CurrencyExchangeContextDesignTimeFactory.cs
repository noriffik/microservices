﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace NexCore.CurrencyExchange.DataSources.MsSql
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Used by EF tool")]
    public class CurrencyExchangeContextDesignTimeFactory : IDesignTimeDbContextFactory<CurrencyExchangeContext>
    {
        private const string ConnectionStringKey = "ConnectionString";

        private readonly Lazy<IConfiguration> _configuration = new Lazy<IConfiguration>(BuildConfiguration);
        
        public CurrencyExchangeContext CreateDbContext(string[] args)
        {
            var connectionString = Configuration[ConnectionStringKey];
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new InvalidOperationException("Could not find a connection string");

            var builder = new DbContextOptionsBuilder<CurrencyExchangeContext>();
            builder.UseSqlServer(connectionString);

            return (CurrencyExchangeContext)Activator.CreateInstance(typeof(CurrencyExchangeContext), builder.Options);
        }

        private IConfiguration Configuration => _configuration.Value;

        private static IConfiguration BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            return builder.Build();
        }
    }
}
