﻿using Microsoft.EntityFrameworkCore;
using NexCore.CurrencyExchange.Abstract;
using NexCore.CurrencyExchange.DataSources.MsSql.Specifications;
using NexCore.Domain;
using System;
using System.Threading.Tasks;

namespace NexCore.CurrencyExchange.DataSources.MsSql
{
    public class SqlDataSource : BaseDataSource
    {
        private readonly CurrencyExchangeContext _context;

        public SqlDataSource(CurrencyExchangeContext context, ICurrencyRateDataSource inner = null) : base(inner)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        protected override async Task<decimal?> TryGetLocally(Currency from, Currency to, DateTime onDate)
        {
            var rate = await _context.Rates
                .Apply(new FindSpecification(from, to, onDate.Date))
                .SingleOrDefaultAsync();

            return rate?.Value;
        }

        protected override async Task Cache(Currency from, Currency to, DateTime onDate, decimal rate)
        {
            await _context.AddAsync(new Rate(from, to, rate, onDate.Date));

            await _context.SaveChangesAsync();
        }
    }
}
