﻿using NexCore.CurrencyExchange.Abstract;
using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.CurrencyExchange.DataSources.MsSql.Specifications
{
    public class FindSpecification : Specification<Rate>
    {
        public Currency From { get; }

        public Currency To { get; }

        public DateTime Date { get; }

        public FindSpecification(Currency from, Currency to, DateTime date)
        {
            From = from ?? throw new ArgumentNullException(nameof(from));
            To = to ?? throw new ArgumentNullException(nameof(to));
            Date = date.Date;
        }

        public override Expression<Func<Rate, bool>> ToExpression()
        {
            return rate => Convert.ToString(rate.From) == From.Code
                           && Convert.ToString(rate.To) == To.Code
                           && rate.Date == Date;
        }

        public override string Description => $"{From.Code} rate to {To.Code} on {Date}";
    }
}
