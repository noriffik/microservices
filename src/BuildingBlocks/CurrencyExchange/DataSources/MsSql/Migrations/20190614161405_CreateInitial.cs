﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.CurrencyExchange.DataSources.MsSql.Migrations
{
    public partial class CreateInitial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateSequence(
                name: "sq_Rate",
                schema: "dbo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Rates",
                columns: table => new
                {
                    RateId = table.Column<int>(nullable: false),
                    From = table.Column<string>(nullable: false),
                    To = table.Column<string>(nullable: false),
                    Value = table.Column<decimal>(type: "decimal(18,10)", nullable: false),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rates", x => x.RateId);
                    table.UniqueConstraint("AK_Rates_From_To_Date", x => new { x.From, x.To, x.Date });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Rates");

            migrationBuilder.DropSequence(
                name: "sq_Rate",
                schema: "dbo");
        }
    }
}
