﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.CurrencyExchange.Abstract;

namespace NexCore.CurrencyExchange.DataSources.MsSql.EntityTypes
{
    public class RateTypeConfiguration : IEntityTypeConfiguration<Rate>
    {
        private readonly string _schema;

        public RateTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<Rate> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Rate), _schema)
                .HasColumnName(nameof(Rate) + "Id");

            builder.Property(r => r.From)
                .HasConversion(r => r.Code, r => r);

            builder.Property(r => r.To)
                .HasConversion(r => r.Code, r => r);

            builder.HasAlternateKey("From", "To", "Date");

            builder.Property(p => p.Value)
                .HasColumnType("decimal(18,10)");
        }
    }
}
