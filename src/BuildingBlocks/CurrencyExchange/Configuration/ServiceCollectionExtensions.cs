﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NexCore.CurrencyExchange.Abstract;
using NexCore.CurrencyExchange.DataSources.Http;
using NexCore.CurrencyExchange.DataSources.Http.Abstract;
using NexCore.CurrencyExchange.DataSources.Http.Nbu;
using NexCore.CurrencyExchange.DataSources.InMemory;
using NexCore.CurrencyExchange.DataSources.MsSql;
using System;
using System.Linq;
using System.Reflection;

namespace NexCore.CurrencyExchange.Configuration
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCurrencyExchange(this IServiceCollection services,
            string urlTemplate, string sqlConnectionString)
        {
            return services.AddNbuDataSource(urlTemplate)
                .AddSqlDataSource<NbuHttpDataSource>(sqlConnectionString)
                .AddInMemoryDataSource<SqlDataSource>()
                .AddCurrencyExchange<InMemoryDataSource>();
        }

        public static IServiceCollection AddCurrencyExchange<TDataSource>(this IServiceCollection services)
            where TDataSource : ICurrencyRateDataSource
        {
            var isRegistered = services.Any(s => s.ServiceType == typeof(ICurrencyExchangeService));
            if (isRegistered)
                return services;

            return services.AddScoped<ICurrencyExchangeService, CurrencyExchangeService>(
                s => new CurrencyExchangeService(s.GetRequiredService<TDataSource>()));
        }

        public static IServiceCollection AddNbuDataSource(this IServiceCollection services, string urlTemplate)
        {
            return services.AddTransient<IUrlProvider>(s => new NbuUrlProvider(urlTemplate))
                .AddTransient<IHttpClientFactory, HttpClientFactory>()
                .AddScoped<NbuHttpDataSource>();
        }

        public static IServiceCollection AddSqlDataSource<TInner>(this IServiceCollection services, string connectionString)
            where TInner : ICurrencyRateDataSource
        {
            return services.AddScoped(s =>
            {
                var context = s.GetRequiredService<CurrencyExchangeContext>();

                return new SqlDataSource(context, s.GetRequiredService<TInner>());
            })
                .AddDbContext<CurrencyExchangeContext>(connectionString);
        }

        public static IServiceCollection AddInMemoryDataSource<TInner>(this IServiceCollection services) where TInner : ICurrencyRateDataSource
        {
            return services.AddScoped(s => new InMemoryDataSource(s.GetRequiredService<TInner>()));
        }

        private static IServiceCollection AddDbContext<TDbContext>(
            this IServiceCollection services, string connectionString) where TDbContext : DbContext
        {
            services.AddEntityFrameworkSqlServer()
                .AddDbContext<TDbContext>(
                    options =>
                    {
                        var migrationAssembly = typeof(TDbContext).GetTypeInfo().Assembly.GetName().Name;

                        options.UseSqlServer(
                            connectionString,
                            sqlOptions =>
                            {
                                sqlOptions.MigrationsAssembly(migrationAssembly);

                                sqlOptions.EnableRetryOnFailure(
                                    10, TimeSpan.FromSeconds(30), null);
                            });
                    },
                    ServiceLifetime.Scoped);

            return services;
        }
    }
}
