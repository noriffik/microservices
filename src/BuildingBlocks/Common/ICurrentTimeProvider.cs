﻿using System;

namespace NexCore.Common
{
    public interface ICurrentTimeProvider
    {
        DateTime Get();
    }
}
