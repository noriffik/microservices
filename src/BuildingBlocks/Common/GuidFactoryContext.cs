﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace NexCore.Common
{
    public class GuidFactoryContext : IDisposable
    {
        private static readonly ThreadLocal<Stack<GuidFactoryContext>> ThreadScopeStack =
            new ThreadLocal<Stack<GuidFactoryContext>>(() => new Stack<GuidFactoryContext>());

        public Guid Value;

        public GuidFactoryContext(Guid value)
        {
            ThreadScopeStack.Value.Push(this);

            Value = value;
        }

        public static GuidFactoryContext Current =>
            ThreadScopeStack.Value.Count > 0
                ? ThreadScopeStack.Value.Peek()
                : null;

        public void Dispose()
        {
            ThreadScopeStack.Value.Pop();
        }
    }
}
