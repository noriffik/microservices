﻿using System;

namespace NexCore.Common
{
    public class GuidFactory
    {
        private static Lazy<GuidFactory> LazeInstance => new Lazy<GuidFactory>(() => new GuidFactory());

        public static GuidFactory Instance => LazeInstance.Value;

        private readonly Func<Guid> _next = Guid.NewGuid;

        private GuidFactory()
        {
        }
        
        public Guid Next()
        {
            return GuidFactoryContext.Current == null
                ? _next.Invoke()
                : GuidFactoryContext.Current.Value;
        }
    }
}
