﻿using System;

namespace NexCore.Common
{
    public class SystemTimeProvider : ICurrentTimeProvider
    {
        private static readonly Lazy<SystemTimeProvider> LazyInstance = new Lazy<SystemTimeProvider>(() => new SystemTimeProvider());
        
        public static SystemTimeProvider Instance => LazyInstance.Value;

        private readonly Func<DateTime> _current = () => DateTime.UtcNow;

        private SystemTimeProvider()
        {
        }
        
        public DateTime Get()
        {
            return SystemTimeContext.Current == null
                ? _current.Invoke()
                : SystemTimeContext.Current.Now;
        }
    }
}
