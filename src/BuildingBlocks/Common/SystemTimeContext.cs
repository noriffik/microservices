﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace NexCore.Common
{
    public class SystemTimeContext : IDisposable
    {
        private static readonly ThreadLocal<Stack<SystemTimeContext>> ThreadScopeStack =
            new ThreadLocal<Stack<SystemTimeContext>>(() => new Stack<SystemTimeContext>());

        public DateTime Now;

        public SystemTimeContext(DateTime now)
        {
            ThreadScopeStack.Value.Push(this);

            Now = now;
        }

        public static SystemTimeContext Current =>
            ThreadScopeStack.Value.Count > 0
                ? ThreadScopeStack.Value.Peek()
                : null;

        public void Dispose()
        {
            ThreadScopeStack.Value.Pop();
        }
    }
}