﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.Application.Queries
{
    public class PagedRequest<T> : PagedRequest, IRequest<PagedResponse<T>>
    {
    }

    public class PagedRequest
    {
        [DataMember]
        public int? PageNumber { get; set; }

        [DataMember]
        public int? PageSize { get; set; }
    }
}
