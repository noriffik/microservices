﻿using FluentValidation;

namespace NexCore.Application.Queries
{
    public class PagedRequestValidator : AbstractValidator<PagedRequest>
    {
        public PagedRequestValidator()
        {
            RuleFor(r => r.PageNumber).GreaterThanOrEqualTo(0);
            RuleFor(r => r.PageSize).GreaterThanOrEqualTo(0);
        }
    }
}
