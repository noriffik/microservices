﻿namespace NexCore.Application.Queries
{
    public enum SortDirection
    {
        Ascending = 0,
        Descending = 1
    }
}
