﻿using System;
using System.Runtime.Serialization;

namespace NexCore.Application.Dtos
{
    [DataContract]
    public class PeriodDto
    {
        [DataMember]
        public DateTime From { get; set; }

        [DataMember]
        public DateTime? To { get; set; }
    }
}
