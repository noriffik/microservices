﻿using FluentValidation;
using NexCore.Domain;

namespace NexCore.Application.Dtos
{
    public class PeriodDtoValidator : AbstractValidator<PeriodDto>
    {
        public PeriodDtoValidator()
        {
            RuleFor(v => v).NotEmpty()
                .NotNull()
                .Must(v => PeriodValidator.FromRequired.IsValid(v.From, v.To))
                .WithMessage("Invalid period.");
        }
    }
}
