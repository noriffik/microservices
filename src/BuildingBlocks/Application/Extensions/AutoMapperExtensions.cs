﻿using AutoMapper;
using NexCore.Application.Queries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Application.Extensions
{
    public static class AutoMapperExtensions
    {
        public static PagedResponse<TDestination> MapPagedResponse<TSource, TDestination>(this IMapper mapper, PagedResponse<TSource> source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            var options = new PageOptions(source.Current, source.Size);
            var items = mapper.Map<IEnumerable<TSource>, IEnumerable<TDestination>>(source.Items);

            return new PagedResponse<TDestination>(options, source.Total, items.ToArray());
        }
    }
}
