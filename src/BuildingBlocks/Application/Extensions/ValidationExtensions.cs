﻿using FluentValidation;
using FluentValidation.Results;
using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Application.Extensions
{
    public static class ValidationExtensions
    {
        public static IRuleBuilderOptions<T, TProperty> WithMessage<T, TProperty>(this IRuleBuilderOptions<T, TProperty> rule, IEnumerable<string> options)
        {
            return rule.WithMessage($"Expected values are {options.Aggregate((a, b) => a + ", " + b).TrimStart(',')}");
        }

        public static IRuleBuilderOptions<T, int> MustBeYear<T>(this IRuleBuilder<T, int> rule)
        {
            return rule.Must(c => DateTime.MinValue.Year <= c && DateTime.MaxValue.Year >= c)
                .WithMessage("Invalid year value.");
        }

        public static ValidationException ExceptionForCommandHandler(string propertyName,
            string errorMessage, object attemptedValue = null)
        {
            throw new ValidationException(
                "Failed to handle command.",
                new[] { new ValidationFailure(propertyName, errorMessage, attemptedValue) });
        }

        public static ValidationException ExceptionForInvalidEntityId(InvalidEntityIdException e, string propertyName = "")
        {
            if (e.EntityType != null && propertyName == string.Empty)
                propertyName = e.EntityType.Name + "Id";

            var id = e.Id is EntityId entityId ? entityId.Value : e.Id;

            throw new ValidationException(
                "Failed to handle command.",
                new[] { new ValidationFailure(propertyName, e.Message, id) });
        }
    }
}
