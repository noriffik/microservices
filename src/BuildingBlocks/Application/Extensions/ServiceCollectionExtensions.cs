﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Application.AutofacModules;
using System;
using System.Reflection;

namespace NexCore.Application.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static ContainerBuilder GetAutofacContainerBuilder(this IServiceCollection services,
            params Assembly[] assemblyTypes)
        {
            var container = new ContainerBuilder();
            
            container.Populate(services);
            
            container.RegisterModule(new MediatorModule(assemblyTypes));
            container.RegisterModule(new ValidationModule(assemblyTypes));

            return container;
        }

        public static void RegisterEntityCommandHandler(this ContainerBuilder builder, Type entity, Type genericCommand, Type genericHandler, Type genericValidator)
        {
            var commandType = genericCommand.MakeGenericType(entity);
            var handlerType = genericHandler.MakeGenericType(commandType, entity);
            var handlerAbstractType = typeof(IRequestHandler<,>).MakeGenericType(commandType, typeof(Unit));

            builder.RegisterType(handlerType)
                .IfNotRegistered(handlerAbstractType)
                .As(handlerAbstractType);

            var validatorType = genericValidator.MakeGenericType(entity);
            var validatorAbstractType = typeof(IValidator<>).MakeGenericType(commandType);

            builder.RegisterType(validatorType)
                .IfNotRegistered(validatorAbstractType)
                .As(validatorAbstractType);
        }
    }
}
