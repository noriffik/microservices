﻿using MediatR.Pipeline;
using NexCore.EventBus.Abstract;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Application.Processors
{
    public class IntegrationEventPublishProcessor<TRequest, TResponse> : IRequestPostProcessor<TRequest, TResponse>
	{
		private readonly IEventBus _eventBus;
		private readonly IIntegrationEventStorage _eventStorage;
		
		public IntegrationEventPublishProcessor(IEventBus eventBus, IIntegrationEventStorage eventStorage)
		{
			_eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
			_eventStorage = eventStorage ?? throw new ArgumentNullException(nameof(eventStorage));
        }

		public async Task Process(TRequest request, TResponse response, CancellationToken cancellationToken)
		{
            _eventBus.Publish(await _eventStorage.Get());
            
            await _eventStorage.Clear();
        }
	}
}