﻿using System.Threading.Tasks;

namespace NexCore.Application.Setup
{
    public interface IApplicationSetupTask
    {
        Task Execute();
    }
}
