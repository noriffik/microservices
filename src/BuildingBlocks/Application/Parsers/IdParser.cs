﻿using System.Collections.Generic;

namespace NexCore.Application.Parsers
{
    public static class IdParser
    {
        public static IEnumerable<int> ParseMany(string from)
        {
            var results = new List<int>();
            foreach (var id in from.TrimEnd(';').Split(';'))
            {
                if (int.TryParse(id, out var num) == false)
                    return null;

                results.Add(num);
            }

            return results;
        }
    }
} 
