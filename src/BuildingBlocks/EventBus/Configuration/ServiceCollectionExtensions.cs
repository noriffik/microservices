﻿using System;
using Autofac;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NexCore.EventBus.Abstract;
using NexCore.EventBus.Azure;
using NexCore.EventBus.RabbitMq;
using RabbitMQ.Client;

namespace NexCore.EventBus.Configuration
{
    public static class ServiceCollectionExtensions
    {
	    public static IServiceCollection ConfigureEventBus(this IServiceCollection services,
		    IConfiguration configuration, Action<IEventBus> customize)
	    {
		    services.Configure<EventBusConfiguration>(options =>
		    {
			    options.SubscriptionClientName = configuration["SubscriptionClientName"];
			    options.EventBusType = (EBusType) configuration.GetValue("EventBusType", EBusType.RabbitMq);
			    options.EventBusConnection = configuration["EventBusConnection"];
				options.EventBusRetryCount = configuration.GetValue("EventBusRetryCount", 5);
			    options.EventBusUserName = configuration["EventBusUserName"];
			    options.EventBusPassword = configuration["EventBusPassword"];
		    });

		    var monitor = services.BuildServiceProvider().GetService<IOptionsMonitor<EventBusConfiguration>>();

		    switch (monitor.CurrentValue.EventBusType)
		    {
			    case EBusType.Azure:
				    services.AddSingleton<IServiceBusPersistentConnection>(s =>
				    {
					    var logger = s.GetRequiredService<ILogger<DefaultServiceBusPersistentConnection>>();
					    var serviceBusConnection = new ServiceBusConnectionStringBuilder(monitor.CurrentValue.EventBusConnection);

					    return new DefaultServiceBusPersistentConnection(serviceBusConnection, logger);
				    });

				    services.AddSingleton<IEventBus, EventBusServiceBus>(s =>
				    {
					    var serviceBusConnection = s.GetRequiredService<IServiceBusPersistentConnection>();
					    var iLifetimeScope = s.GetRequiredService<ILifetimeScope>();
					    var logger = s.GetRequiredService<ILogger<EventBusServiceBus>>();
					    var subscriptionsManager = s.GetRequiredService<IEventBusSubscriptionsManager>();
						var eventBus = new EventBusServiceBus(serviceBusConnection, logger, subscriptionsManager, iLifetimeScope, monitor.CurrentValue.SubscriptionClientName);

						customize(eventBus);
						return eventBus;
				    });

					break;
			    case EBusType.RabbitMq:
				    services.AddSingleton<IRabbitMqPersistentConnection>(s =>
				    {
					    var logger = s.GetRequiredService<ILogger<DefaultRabbitMqPersistentConnection>>();

					    var factory = new ConnectionFactory
					    {
						    HostName = monitor.CurrentValue.EventBusConnection,
						    DispatchConsumersAsync = true
					    };

					    if (!string.IsNullOrEmpty(monitor.CurrentValue.EventBusUserName))
					    {
						    factory.UserName = monitor.CurrentValue.EventBusUserName;
					    }

					    if (!string.IsNullOrEmpty(monitor.CurrentValue.EventBusPassword))
					    {
						    factory.Password = monitor.CurrentValue.EventBusPassword;
					    }

					    return new DefaultRabbitMqPersistentConnection(factory, logger, monitor.CurrentValue.EventBusRetryCount);
				    });

				    services.AddSingleton<IEventBus, EventBusRabbitMq>(s =>
				    {
					    var serviceBusConnection = s.GetRequiredService<IRabbitMqPersistentConnection>();
					    var iLifetimeScope = s.GetRequiredService<ILifetimeScope>();
					    var logger = s.GetRequiredService<ILogger<EventBusRabbitMq>>();
					    var subscriptionsManager = s.GetRequiredService<IEventBusSubscriptionsManager>();

					    var eventBus = new EventBusRabbitMq(serviceBusConnection, logger, iLifetimeScope,
						    subscriptionsManager, monitor.CurrentValue.SubscriptionClientName, monitor.CurrentValue.EventBusRetryCount);

					    customize(eventBus);

					    return eventBus;
				    });
					break;
		    }

		    services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();

			return services;
	    }
    }
}
