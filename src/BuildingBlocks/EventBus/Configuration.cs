﻿using System.ComponentModel;

namespace NexCore.EventBus
{
	public class EventBusConfiguration
	{
		public EBusType EventBusType { get; set; }
		public string SubscriptionClientName { get; set; }
		public string EventBusConnection { get; set; }
		public string EventBusUserName { get; set; }
		public string EventBusPassword { get; set; }
		public int EventBusRetryCount { get; set; } = 5;
	}

	public enum EBusType
	{
		[Description("Asure")]
		Azure = 1,
		[Description("RabbitMq")]
		RabbitMq = 2
	}
}