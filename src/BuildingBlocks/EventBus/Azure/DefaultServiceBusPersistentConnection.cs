﻿using System;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Logging;

namespace NexCore.EventBus.Azure
{
    public class DefaultServiceBusPersistentConnection : IServiceBusPersistentConnection
	{
		private readonly ILogger<DefaultServiceBusPersistentConnection> _logger;
		private readonly ServiceBusConnectionStringBuilder _serviceBusConnectionStringBuilder;
		private ITopicClient _topicClient;

		bool _disposed;

		public DefaultServiceBusPersistentConnection(ServiceBusConnectionStringBuilder serviceBusConnectionStringBuilder,
			ILogger<DefaultServiceBusPersistentConnection> logger)
		{
			_serviceBusConnectionStringBuilder = serviceBusConnectionStringBuilder ?? throw new ArgumentNullException(nameof(serviceBusConnectionStringBuilder));
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
			_topicClient = new TopicClient(_serviceBusConnectionStringBuilder, RetryPolicy.Default);
		}

		public ServiceBusConnectionStringBuilder ServiceBusConnectionStringBuilder => _serviceBusConnectionStringBuilder;

		public void Dispose()
		{
			if (_disposed) return;
			_disposed = true;
		}

		public ITopicClient CreateModel()
		{
			if (_topicClient.IsClosedOrClosing)
			{
				_topicClient = new TopicClient(_serviceBusConnectionStringBuilder, RetryPolicy.Default);
			}

			return _topicClient;
		}
	}
}