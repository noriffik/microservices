﻿using System;
using Microsoft.Azure.ServiceBus;

namespace NexCore.EventBus.Azure
{
    public interface IServiceBusPersistentConnection : IDisposable
	{
		ServiceBusConnectionStringBuilder ServiceBusConnectionStringBuilder { get; }

		ITopicClient CreateModel();
	}
}
