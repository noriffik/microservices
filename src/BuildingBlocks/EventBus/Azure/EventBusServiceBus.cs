﻿using Autofac;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NexCore.EventBus.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexCore.EventBus.Azure
{
    public class EventBusServiceBus : IEventBus, IDisposable
	{
		private readonly IServiceBusPersistentConnection _serviceBusPersistentConnection;
		private readonly ILogger<EventBusServiceBus> _logger;
		private readonly IEventBusSubscriptionsManager _subsManager;
		private readonly SubscriptionClient _subscriptionClient;
		private readonly ILifetimeScope _autofac;
		private const string AutofacScopeName = "nexcore_event_bus";
		private const string IntegrationEventSufix = "IntegrationEvent";

		public EventBusServiceBus(IServiceBusPersistentConnection serviceBusPersistentConnection,
			ILogger<EventBusServiceBus> logger,
			IEventBusSubscriptionsManager subsManager, ILifetimeScope autofac, string subscriptionClientName)
		{
			_serviceBusPersistentConnection = serviceBusPersistentConnection ??
			                                  throw new ArgumentNullException(nameof(serviceBusPersistentConnection));
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
			_subsManager = subsManager ?? new InMemoryEventBusSubscriptionsManager();
			_subscriptionClient = new SubscriptionClient(serviceBusPersistentConnection.ServiceBusConnectionStringBuilder, subscriptionClientName);
			_autofac = autofac;

			RemoveDefaultRule();
			RegisterSubscriptionClientMessageHandler();
		}

		public void Publish(IntegrationEvent @event)
		{
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));

            var eventName = @event.GetType().Name.Replace(IntegrationEventSufix, "");
			var jsonMessage = JsonConvert.SerializeObject(@event);
			var body = Encoding.UTF8.GetBytes(jsonMessage);

			var message = new Message
			{
				MessageId = Guid.NewGuid().ToString(),
				Body = body,
				Label = eventName
			};

			var topicClient = _serviceBusPersistentConnection.CreateModel();

			topicClient.SendAsync(message).GetAwaiter().GetResult();
		}

        public void Publish(IEnumerable<IntegrationEvent> events)
        {
            if (events == null)
                throw new ArgumentNullException(nameof(events));

            events.ToList().ForEach(Publish);
        }

        public void Subscribe<TEvent, TEventHandler>() where TEvent : IntegrationEvent where TEventHandler : IIntegrationEventHandler<TEvent>
		{
			var eventName = typeof(TEvent).Name.Replace(IntegrationEventSufix, "");

			var containsKey = _subsManager.HasSubscriptionsForEvent<TEvent>();
			if (!containsKey)
			{
				try
				{
					_subscriptionClient.AddRuleAsync(new RuleDescription
					{
						Filter = new CorrelationFilter { Label = eventName },
						Name = eventName
					}).GetAwaiter().GetResult();
				}
				catch (ServiceBusException)
				{
					_logger.LogWarning("The messaging entity {eventName} already exists.", eventName);
				}
			}

			_logger.LogInformation("Subscribing to event {EventName} with {EventHandler}", eventName, nameof(TEventHandler));

			_subsManager.AddSubscription<TEvent, TEventHandler>();
		}

		public void SubscribeDynamic<TH>(string eventName) where TH : IDynamicIntegrationEventHandler
		{
			_logger.LogInformation("Subscribing to dynamic event {EventName} with {EventHandler}", eventName, nameof(TH));

			_subsManager.AddDynamicSubscription<TH>(eventName);
		}

		public void UnsubscribeDynamic<TH>(string eventName) where TH : IDynamicIntegrationEventHandler
		{
			_logger.LogInformation("Unsubscribing from dynamic event {EventName}", eventName);

			_subsManager.RemoveDynamicSubscription<TH>(eventName);
		}

		public void Unsubscribe<TEvent, TEventHandler>() where TEvent : IntegrationEvent where TEventHandler : IIntegrationEventHandler<TEvent>
		{
			var eventName = typeof(TEvent).Name.Replace(IntegrationEventSufix, "");

			try
			{
				_subscriptionClient.RemoveRuleAsync(eventName).GetAwaiter().GetResult();
			}
			catch (MessagingEntityNotFoundException)
			{
				_logger.LogWarning("The messaging entity {eventName} Could not be found.", eventName);
			}

			_logger.LogInformation("Unsubscribing from event {EventName}", eventName);

			_subsManager.RemoveSubscription<TEvent, TEventHandler>();
		}

		public void Dispose()
		{
			_subsManager.Clear();
		}

		private void RegisterSubscriptionClientMessageHandler()
		{
			_subscriptionClient.RegisterMessageHandler(
				async (message, token) =>
				{
					var eventName = $"{message.Label}{IntegrationEventSufix}";
					var messageData = Encoding.UTF8.GetString(message.Body);

					// Complete the message so that it is not received again.
					if (await ProcessEvent(eventName, messageData))
					{
						await _subscriptionClient.CompleteAsync(message.SystemProperties.LockToken);
					}
				},
				new MessageHandlerOptions(ExceptionReceivedHandler) { MaxConcurrentCalls = 10, AutoComplete = false });
		}

		private Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
		{
			var ex = exceptionReceivedEventArgs.Exception;
			var context = exceptionReceivedEventArgs.ExceptionReceivedContext;

			_logger.LogError(ex, "ERROR handling message: {ExceptionMessage} - Context: {@ExceptionContext}", ex.Message, context);

			return Task.CompletedTask;
		}

		private async Task<bool> ProcessEvent(string eventName, string message)
		{
			var processed = false;
			if (_subsManager.HasSubscriptionsForEvent(eventName))
			{
				using (var scope = _autofac.BeginLifetimeScope(AutofacScopeName))
				{
					var subscriptions = _subsManager.GetHandlersForEvent(eventName);
					foreach (var subscription in subscriptions)
					{
						if (subscription.IsDynamic)
						{
							if (!(scope.ResolveOptional(subscription.HandlerType) is IDynamicIntegrationEventHandler handler)) continue;
							dynamic eventData = JObject.Parse(message);
							await handler.Handle(eventData);
						}
						else
						{
							var handler = scope.ResolveOptional(subscription.HandlerType);
							if (handler == null) continue;
							var eventType = _subsManager.GetEventTypeByName(eventName);
							var integrationEvent = JsonConvert.DeserializeObject(message, eventType);
							var concreteType = typeof(IIntegrationEventHandler<>).MakeGenericType(eventType);
							await (Task)concreteType.GetMethod("Handle").Invoke(handler, new[] { integrationEvent });
						}
					}
				}
				processed = true;
			}
			return processed;
		}

		private void RemoveDefaultRule()
		{
			try
			{
				_subscriptionClient.RemoveRuleAsync(RuleDescription.DefaultRuleName).GetAwaiter().GetResult();
			}
			catch (MessagingEntityNotFoundException)
			{
				_logger.LogWarning("The messaging entity {DefaultRuleName} Could not be found.", RuleDescription.DefaultRuleName);
			}
		}
	}
}