﻿namespace NexCore.DocumentProcessor.Helpers
{
	internal class ParagraphStyleInfo
	{
		public ParagraphStyleInfo()
		{
		}

		public ParagraphStyleInfo(string styleId, string styleName)
		{
			StyleId = styleId;
			StyleName = styleName;
		}

		public string StyleId { get; set; }
		public string StyleName { get; set; }
	}
}