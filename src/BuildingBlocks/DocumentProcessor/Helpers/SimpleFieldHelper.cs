﻿using System;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

namespace NexCore.DocumentProcessor.Helpers
{
	public class SimpleFieldHelper
	{
		public SimpleFieldHelper(WordprocessingDocument document)
		{
			Document = document ?? throw new ArgumentNullException(nameof(document));
		}

		protected WordprocessingDocument Document { get; }

		protected Body Body => Document?.MainDocumentPart.Document.Body;

		private RunHelper _runHelper;

		public RunHelper RunHelper => _runHelper ?? (_runHelper = new RunHelper(Document));

		public void Add(string sentence)
		{
			SimpleField field = new SimpleField();
			field.AppendChild(RunHelper.CreateSimpleFieldText(sentence));
		}
	}
}