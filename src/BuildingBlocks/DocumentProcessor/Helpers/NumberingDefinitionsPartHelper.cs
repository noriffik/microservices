﻿using System;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

namespace NexCore.DocumentProcessor.Helpers
{
	public class NumberingDefinitionsPartHelper
	{
		public NumberingDefinitionsPartHelper(WordprocessingDocument document)
		{
			Document = document ?? throw new ArgumentNullException(nameof(document));
		}

		public Body Body => Document?.MainDocumentPart.Document.Body;
		public WordprocessingDocument Document { get; set; }

		public void AddNumberingInstance(NumberingInstance newNumberingInstance)
		{
			NumberingDefinitionsPart numberingPart = GetOrCreate();

			if (numberingPart.Numbering.Elements<NumberingInstance>().Any())
			{
				var lastNumberingInstance = numberingPart.Numbering.Elements<NumberingInstance>().Last();
				numberingPart.Numbering.InsertAfter(newNumberingInstance, lastNumberingInstance);
			}
			else
			{
				numberingPart.Numbering.Append(newNumberingInstance);
			}
		}

		public void AddAbstractNum(AbstractNum newAbstractNum)
		{
			NumberingDefinitionsPart numberingPart = GetOrCreate();

			if (numberingPart.Numbering.Elements<AbstractNum>().Any())
			{
				AbstractNum lastAbstractNum = numberingPart.Numbering.Elements<AbstractNum>().Last();
				numberingPart.Numbering.InsertAfter(newAbstractNum, lastAbstractNum);
			}
			else
			{
				numberingPart.Numbering.Append(newAbstractNum);
			}
		}

		public NumberingDefinitionsPart GetOrCreate()
		{
			// Introduce bulleted numbering in case it will be needed at some point
			NumberingDefinitionsPart numberingPart = Document.MainDocumentPart.NumberingDefinitionsPart;
			if (numberingPart == null)
			{
				numberingPart = Document.MainDocumentPart.AddNewPart<NumberingDefinitionsPart>("NumberingDefinitionsPart001");
				Numbering element = new Numbering();
				element.Save(numberingPart);
			}

			return numberingPart;
		}
	}
}