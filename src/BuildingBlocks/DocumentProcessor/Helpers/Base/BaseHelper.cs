﻿using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

namespace NexCore.DocumentProcessor.Helpers.Base
{
	public abstract class BaseHelper
	{
		private ParagraphHelper _paragraphHelper;

		private NumberingDefinitionsPartHelper _numberingDefinitionsPartHelper;

		public NumberingDefinitionsPartHelper NumberingDefinitionsPartHelper => _numberingDefinitionsPartHelper ?? (_numberingDefinitionsPartHelper = new NumberingDefinitionsPartHelper(Document));

		public ParagraphHelper ParagraphHelper => _paragraphHelper ?? (_paragraphHelper = new ParagraphHelper(Document));
		public WordprocessingDocument Document { get; set; }
		protected Body Body => Document?.MainDocumentPart.Document.Body;

		protected uint GetMaxDocPropertyId()
		{
			return Document
				       .MainDocumentPart
				       .RootElement
				       .Descendants<DocumentFormat.OpenXml.Drawing.Wordprocessing.DocProperties>()
				       .Max(x => (uint?) x.Id) ?? 0;
		}
	}
}