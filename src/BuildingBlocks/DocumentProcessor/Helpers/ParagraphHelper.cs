﻿using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Color = DocumentFormat.OpenXml.Wordprocessing.Color;
using FontSize = DocumentFormat.OpenXml.Wordprocessing.FontSize;
using Run = DocumentFormat.OpenXml.Wordprocessing.Run;

namespace NexCore.DocumentProcessor.Helpers
{
	public class ParagraphHelper
	{
		public ParagraphHelper(WordprocessingDocument document)
		{
			Document = document ?? throw new ArgumentNullException(nameof(document));
		}
		protected WordprocessingDocument Document { get; }

		protected Body Body => Document?.MainDocumentPart.Document.Body;

		private RunHelper _runHelper;

		public RunHelper RunHelper => _runHelper ?? (_runHelper = new RunHelper(Document));

		public Paragraph AddPageBreak()
		{
			return AddToBody(new List<Run> {RunHelper.CreateBreak(BreakValues.Page)});
		}

		public void RemoveParagraph(Paragraph paragraph)
		{
			Body.RemoveChild(paragraph);
		}

		public Paragraph AddToBody(string sentence)
		{
			List<Run> runList = RunHelper.ConvertToRunList(new List<string> { sentence });
			return AddToBody(runList);
		}

		public Paragraph AddToBody(Run sentence)
		{
			var runList = new List<Run> { sentence };
			return AddToBody(runList);
		}

		protected Paragraph AddToBody(List<Run> runList)
		{
			var newParagraph = new Paragraph();
			foreach (Run runItem in runList)
			{
				newParagraph.AppendChild(runItem);
			}

			AddToBody(newParagraph);

			return newParagraph;
		}

		public void AddToBody(Paragraph newParagraph)
		{
			var lastParagraph = Body.Elements().Any() ? Body.Elements<Paragraph>().Last() : null;
			if (lastParagraph == null)
			{
				Body.AppendChild(newParagraph);
			}
			else Body.InsertAfter(newParagraph, lastParagraph);
		}

		public void AddBookmarks(Paragraph p)
		{
			BookmarkStart bookmarkStart = new BookmarkStart {Name = "_GoBack", Id = "0"};
			BookmarkEnd bookmarkEnd = new BookmarkEnd {Id = "0"};
			p.AppendChild(bookmarkStart);
			p.AppendChild(bookmarkEnd);
		}

		public void ApplyJustification(Paragraph p, JustificationValues justification)
		{
			// If the paragraph has no ParagraphProperties object, create one.
			if (!p.Elements<ParagraphProperties>().Any())
			{
				p.PrependChild(new ParagraphProperties());
			}

			// Get the paragraph properties element of the paragraph.
			ParagraphProperties pPr = p.Elements<ParagraphProperties>().First();

			pPr.Justification = new Justification { Val = justification };
		}

		public void ApplyStyle(Paragraph p, ParagraphStylesEnum style)
		{
			if (style == ParagraphStylesEnum.None)
				return;

			// If the paragraph has no ParagraphProperties object, create one.
			if (!p.Elements<ParagraphProperties>().Any())
			{
				p.PrependChild(new ParagraphProperties());
			}

			// Get the paragraph properties element of the paragraph.
			ParagraphProperties pPr = p.Elements<ParagraphProperties>().First();

			// Get the Styles part for this document.
			StyleDefinitionsPart part = Document.MainDocumentPart.StyleDefinitionsPart;

			ParagraphStyleInfo info = GetIntParagraphStyleInfo(style);

			// If the Styles part does not exist, add it and then add the style.
			if (part == null)
			{
				part = AddStylesPartToPackage();
				AddNewStyle(part, style);
			}
			else
			{
				// If the style is not in the document, add it.
				if (IsStyleIdInDocument(info.StyleId) == false)
				{
					// No match on styleid, so let's try style name.
					string styleIdFromName = GetStyleIdFromStyleName(info.StyleName);
					if (styleIdFromName == null)
					{
						AddNewStyle(part, style);
					}
					else
						info.StyleId = styleIdFromName;
				}
			}

			// Set the style of the paragraph.
			pPr.ParagraphStyleId = new ParagraphStyleId { Val = info.StyleId };
		}

		private const string NormalStyleId = "Normal";
		private const string NormalStyleName = "Normal";
		private const string Heading1StyleId = "Heading1";
		private const string Heading1StyleName = "heading 1";
		private const string Heading2StyleId = "Heading2";
		private const string Heading2StyleName = "heading 2";

		/// <summary>Create a new style with the specified styleid and stylename and add it to the specified style definitions part.</summary>
		/// <remarks>
		/// Code from: https://msdn.microsoft.com/en-us/library/office/cc850838.aspx
		/// </remarks>
		private void AddNewStyle(StyleDefinitionsPart styleDefinitionsPart, ParagraphStylesEnum styleEnum)
		{
			// Get access to the root element of the styles part.
			Styles styles = styleDefinitionsPart.Styles;

			Style style;
			switch (styleEnum)
			{
				case ParagraphStylesEnum.H1:
					style = CreateH1Style();
					break;
				case ParagraphStylesEnum.H2:
					style = CreateH2Style();
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(styleEnum), styleEnum, null);
			}

			if (style != null)
			{
				styles.Append(style);
			}
		}

		/// <summary>Add a StylesDefinitionsPart to the document.  Returns a reference to it.</summary>
		/// <remarks>
		/// Code from: https://msdn.microsoft.com/en-us/library/office/cc850838.aspx
		/// </remarks>
		private StyleDefinitionsPart AddStylesPartToPackage()
		{
			var part = Document.MainDocumentPart.AddNewPart<StyleDefinitionsPart>();
			var root = new Styles();
			root.Save(part);
			return part;
		}

		private Style CreateH1Style()
		{
			Style style = new Style { Type = StyleValues.Paragraph, StyleId = Heading1StyleId };
			StyleName styleName = new StyleName { Val = Heading1StyleName };
			BasedOn basedOn = new BasedOn { Val = NormalStyleName };
			NextParagraphStyle nextParagraphStyle = new NextParagraphStyle { Val = NormalStyleName };
			LinkedStyle linkedStyle = new LinkedStyle { Val = "Heading1Char" };
			UIPriority uIPriority = new UIPriority { Val = 9 };
			PrimaryStyle primaryStyle = new PrimaryStyle();
			Rsid rsid = new Rsid { Val = "00445B57" };

			StyleRunProperties styleRunProperties = new StyleRunProperties();
			RunFonts runFonts = new RunFonts
			{
				AsciiTheme = ThemeFontValues.MajorHighAnsi,
				HighAnsiTheme = ThemeFontValues.MajorHighAnsi,
				EastAsiaTheme = ThemeFontValues.MajorEastAsia,
				ComplexScriptTheme = ThemeFontValues.MajorBidi
			};
			Color color = new Color { Val = "2E74B5", ThemeColor = ThemeColorValues.Accent1, ThemeShade = "BF" };
			FontSize fontSize = new FontSize { Val = "32" };
			FontSizeComplexScript fontSizeComplexScript = new FontSizeComplexScript { Val = "32" };

			styleRunProperties.Append(runFonts);
			styleRunProperties.Append(color);
			styleRunProperties.Append(fontSize);
			styleRunProperties.Append(fontSizeComplexScript);

			style.Append(styleName);
			style.Append(basedOn);
			style.Append(nextParagraphStyle);
			style.Append(linkedStyle);
			style.Append(uIPriority);
			style.Append(primaryStyle);
			style.Append(rsid);
			style.Append(styleRunProperties);

			return style;
		}

		private Style CreateH2Style()
		{
			Style style = new Style { Type = StyleValues.Paragraph, StyleId = Heading2StyleId };
			StyleName styleName = new StyleName { Val = Heading2StyleName };
			BasedOn basedOn = new BasedOn { Val = NormalStyleName };
			NextParagraphStyle nextParagraphStyle = new NextParagraphStyle { Val = NormalStyleName };
			LinkedStyle linkedStyle = new LinkedStyle { Val = "Heading2Char" };
			UIPriority uIPriority = new UIPriority { Val = 9 };
			Rsid rsid = new Rsid { Val = "00445B57" };

			StyleRunProperties styleRunProperties = new StyleRunProperties();
			RunFonts runFonts = new RunFonts {
				AsciiTheme = ThemeFontValues.MajorHighAnsi,
				HighAnsiTheme = ThemeFontValues.MajorHighAnsi,
				EastAsiaTheme = ThemeFontValues.MajorEastAsia,
				ComplexScriptTheme = ThemeFontValues.MajorBidi };
			Color color = new Color { Val = "2E74B5", ThemeColor = ThemeColorValues.Accent1, ThemeShade = "BF" };
			FontSize fontSize = new FontSize { Val = "26" };
			FontSizeComplexScript fontSizeComplexScript = new FontSizeComplexScript { Val = "26" };

			styleRunProperties.Append(runFonts);
			styleRunProperties.Append(color);
			styleRunProperties.Append(fontSize);
			styleRunProperties.Append(fontSizeComplexScript);

			style.Append(styleName);
			style.Append(basedOn);
			style.Append(nextParagraphStyle);
			style.Append(linkedStyle);
			style.Append(uIPriority);
			style.Append(rsid);
			style.Append(styleRunProperties);
			return style;
		}

		private ParagraphStyleInfo GetIntParagraphStyleInfo(ParagraphStylesEnum style)
		{
			switch (style)
			{
				case ParagraphStylesEnum.None:
					return new ParagraphStyleInfo(NormalStyleId, NormalStyleName);
				case ParagraphStylesEnum.H1:
					return new ParagraphStyleInfo(Heading1StyleId, Heading1StyleName);
				case ParagraphStylesEnum.H2:
					return new ParagraphStyleInfo(Heading2StyleId, Heading2StyleName);
				default:
					throw new ArgumentOutOfRangeException(nameof(style), style, null);
			}
		}

		/// <summary>Return styleId that matches the styleName, or null when there's no match.</summary>
		/// <remarks>
		/// Code from: https://msdn.microsoft.com/en-us/library/office/cc850838.aspx
		/// </remarks>
		private string GetStyleIdFromStyleName(string styleName)
		{
			StyleDefinitionsPart stylePart = Document.MainDocumentPart.StyleDefinitionsPart;
			string styleId = stylePart.Styles.Descendants<StyleName>().Where(s => s.Val.Value.Equals(styleName) 
			                                                                      && ((Style)s.Parent).Type == StyleValues.Paragraph).Select(n => ((Style)n.Parent).StyleId).FirstOrDefault();
			return styleId;
		}

		/// <summary> Return true if the style id is in the document, false otherwise.</summary>
		/// <remarks>
		/// Code from: https://msdn.microsoft.com/en-us/library/office/cc850838.aspx
		/// </remarks>
		private bool IsStyleIdInDocument(string styleId)
		{
			// Get access to the Styles element for this document.
			Styles s = Document.MainDocumentPart.StyleDefinitionsPart.Styles;

			// Check that there are styles and how many.
			int n = s.Elements<Style>().Count();
			if (n == 0)
				return false;

			// Look for a match on styleId.
			var style = s.Elements<Style>().FirstOrDefault(st => st.StyleId == styleId && st.Type == StyleValues.Paragraph);
			return style != null;
		}
	}
}