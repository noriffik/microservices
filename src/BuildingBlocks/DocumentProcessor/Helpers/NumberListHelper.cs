﻿using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using NexCore.DocumentProcessor.Helpers.Base;

namespace NexCore.DocumentProcessor.Helpers
{
	public class NumberListHelper : ListBaseHelper
	{
		public NumberListHelper(WordprocessingDocument document) : base(document)
		{
		}

		protected override int CreateNumberingEntries()
		{
			var numberingPart = NumberingDefinitionsPartHelper.GetOrCreate();

			// Add AbstractNum
			var abstractNumberId = numberingPart.Numbering.Elements<AbstractNum>().Count() + 1;
			var abstractNum = new AbstractNum { AbstractNumberId = abstractNumberId };

			// Add it
			abstractNum.Append(
				CreateLevel(1, "0409000F", false, NumberFormatValues.Decimal, LevelJustificationValues.Left),
				CreateLevel(2, "04090019", true, NumberFormatValues.LowerLetter, LevelJustificationValues.Left),
				CreateLevel(3, "0409001B", true, NumberFormatValues.LowerRoman, LevelJustificationValues.Right),
				CreateLevel(4, "0409000F", true, NumberFormatValues.Decimal, LevelJustificationValues.Left),
				CreateLevel(5, "04090019", true, NumberFormatValues.LowerLetter, LevelJustificationValues.Left),
				CreateLevel(6, "0409001B", true, NumberFormatValues.LowerRoman, LevelJustificationValues.Right),
				CreateLevel(7, "0409000F", true, NumberFormatValues.Decimal, LevelJustificationValues.Left),
				CreateLevel(8, "04090019", true, NumberFormatValues.LowerLetter, LevelJustificationValues.Left),
				CreateLevel(9, "0409001B", true, NumberFormatValues.LowerLetter, LevelJustificationValues.Right));

			NumberingDefinitionsPartHelper.AddAbstractNum(abstractNum); // Order matters and this method will take care of that.

			// Add NumberingInstance
			var numberId = numberingPart.Numbering.Elements<NumberingInstance>().Count() + 1;
			var numberingInstance = new NumberingInstance { NumberID = numberId };
			numberingInstance.Append(new AbstractNumId { Val = abstractNumberId });

			NumberingDefinitionsPartHelper.AddNumberingInstance(numberingInstance); // Order matters and this method will take care of that.

			return numberId;
		}

		private static Level CreateLevel(int number, string templateCode, bool tentative, NumberFormatValues numberFormat,
			LevelJustificationValues justification)
		{
			var level = new Level { LevelIndex = number - 1, TemplateCode = templateCode, Tentative = tentative };

			var numberingValue = new StartNumberingValue { Val = 1 };
			var numberingFormat = new NumberingFormat { Val = numberFormat };
			var levelText = new LevelText { Val = $"%{number}." };
			var levelJustification = new LevelJustification { Val = justification };

			level.Append(numberingValue, numberingFormat, levelText, levelJustification);

			return level;
		}
	}
}