﻿using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using NexCore.DocumentProcessor.Helpers.Base;

namespace NexCore.DocumentProcessor.Helpers
{
	public class BulletListHelper : ListBaseHelper
	{
		public BulletListHelper(WordprocessingDocument document) : base(document)
		{
		}

		protected override int CreateNumberingEntries()
		{
			NumberingDefinitionsPart numberingPart = NumberingDefinitionsPartHelper.GetOrCreate();

			// Add AbstractNum
			var abstractNumberId = numberingPart.Numbering.Elements<AbstractNum>().Count() + 1;
			var abstractNum = new AbstractNum { AbstractNumberId = abstractNumberId };

			// Level 1
			var level = new Level { LevelIndex = 0 };
			NumberingFormat numberingFormat = new NumberingFormat { Val = NumberFormatValues.Bullet };
			LevelText levelText = new LevelText { Val = "o" };
			level.Append(numberingFormat);
			level.Append(levelText);

			// Add it
			abstractNum.Append(level);
			NumberingDefinitionsPartHelper.AddAbstractNum(abstractNum); // Order matters and this method will take care of that.
			
			// Add NumberingInstance
			int numberId = numberingPart.Numbering.Elements<NumberingInstance>().Count() + 1;
			NumberingInstance numberingInstance = new NumberingInstance { NumberID = numberId };
			AbstractNumId abstractNumId = new AbstractNumId { Val = abstractNumberId };
			numberingInstance.Append(abstractNumId);
			NumberingDefinitionsPartHelper.AddNumberingInstance(numberingInstance); // Order matters and this method will take care of that.

			return numberId;
		}
	}
}