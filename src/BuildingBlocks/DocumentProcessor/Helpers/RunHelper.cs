﻿using System;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

namespace NexCore.DocumentProcessor.Helpers
{
	public class RunHelper
	{
		public RunHelper(WordprocessingDocument document)
		{
			Document = document ?? throw new ArgumentNullException(nameof(document));
		}

		protected WordprocessingDocument Document { get; }

		protected Body Body => Document?.MainDocumentPart.Document.Body;

		public Run CreateText(string someText)
		{
			var run = new Run();
			run.AppendChild(new Text(someText));
			return run;
		}

		public Run CreateSimpleFieldText(string someText)
		{
			var run = new Run();
			run.AppendChild(new RunProperties(new NoProof()));
			run.AppendChild(new Text(someText));
			return run;
		}

		public Run CreateBreak(BreakValues breakType)
		{
			return new Run(new Break { Type = breakType });
		}

		public List<Run> ConvertToRunList(List<string> sentences)
		{
			var list = new List<Run>();
			foreach (var item in sentences)
			{
				list.Add(CreateText(item));
			}

			return list;
		}

		public List<Run> ConvertToRunList(List<string> sentences, SimpleField fieldToMimic)
		{
			var list = new List<Run>();
			foreach (var item in sentences)
			{
				list.Add(CreateText(item));
			}

			return list;
		}

		public Run GetRunElementForText(string text, SimpleField placeHolder)
		{
			string rpr = null;
			if (placeHolder != null)
			{
				foreach (var placeholderRpr in placeHolder.Descendants<RunProperties>())
				{
					rpr = placeholderRpr.OuterXml;
					break; // break at first
				}
			}

			Run r = new Run();
			if (!string.IsNullOrEmpty(rpr))
			{
				r.Append(new RunProperties(rpr));
			}

			if (string.IsNullOrEmpty(text)) return r;
			// first process line breaks
			var split = text.Split(new[] { "\n" }, StringSplitOptions.None);
			var first = true;
			foreach (string s in split)
			{
				if (!first)
				{
					r.Append(new Break());
				}

				first = false;

				// then process tabs
				var firstTab = true;
				var tabSplit = s.Split(new[] { "\t" }, StringSplitOptions.None);
				foreach (var tabText in tabSplit)
				{
					if (!firstTab)
					{
						r.Append(new TabChar());
					}

					r.Append(new Text(tabText));
					firstTab = false;
				}
			}

			return r;
		}

		public void ApplyBold(Run run)
		{
			if(run.RunProperties == null)
				run.RunProperties = new RunProperties();
			run.RunProperties.Bold = new Bold();
		}

		public void ApplyUnderline(Run run, UnderlineValues underline)
		{
			if(run.RunProperties == null)
				run.RunProperties = new RunProperties();
			run.RunProperties.Underline = new Underline {Val = underline};
		}
	}
}