﻿using System;
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using NexCore.DocumentProcessor.Helpers;
using NexCore.DocumentProcessor.Helpers.Base;

namespace NexCore.DocumentProcessor
{
	public class WordProcessor : BaseHelper, IDisposable
	{
		private MemoryStream _ms;
		private BulletListHelper _bulletedList;
		private NumberListHelper _numberedList;

		public WordProcessor()
		{
			Reset();
		}

		public BulletListHelper BulletHelper => _bulletedList ?? (_bulletedList = new BulletListHelper(Document));

		public NumberListHelper NumberedListHelper => _numberedList ?? (_numberedList = new NumberListHelper(Document));

		public void Reset()
		{
			CloseAndDisposeOfDocument();

			if (_ms == null)
				_ms = new MemoryStream();
			else _ms.Position = 0;

			// These helpers have a reference to the Document so null them out!
			_bulletedList = null;
			_numberedList = null;

			// Create a new one
			Document = WordprocessingDocument.Create(_ms, WordprocessingDocumentType.Document);
			var mainDocumentPart = Document.AddMainDocumentPart();
			mainDocumentPart.Document = new Document(new Body());
		}

		public void Dispose()
		{
			CloseAndDisposeOfDocument();
			if (_ms == null) return;
			_ms.Dispose();
			_ms = null;
		}

		public MemoryStream SaveToStream()
		{
			CloseAndDisposeOfDocument();

			_ms.Position = 0;
			return _ms;
		}

		public void SaveToFile(string fileName)
		{
			CloseAndDisposeOfDocument();

			if (_ms == null)
				throw new ArgumentException("This object has already been disposed of so you cannot save it!");

			using (var fs = File.Create(fileName))
			{
				_ms.WriteTo(fs);
			}
		}

		private void CloseAndDisposeOfDocument()
		{
			if (Document == null) return;
			Document.Close();
			Document.Dispose();
			Document = null;
		}
	}
}