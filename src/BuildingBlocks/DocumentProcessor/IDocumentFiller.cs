﻿using System.Collections.Generic;
using System.IO;

namespace NexCore.DocumentProcessor
{
	public interface IDocumentFiller
	{
		void TransformListValues(Stream stream, IDictionary<string, IList<string>> values);
		void Transform(Stream stream, IDictionary<string, string> values);
	}
}