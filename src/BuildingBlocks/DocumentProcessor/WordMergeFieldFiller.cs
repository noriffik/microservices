﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using NexCore.DocumentProcessor.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace NexCore.DocumentProcessor
{
	public class WordMergeFieldFiller : IDocumentFiller
	{
		private BulletListHelper _bulletListHelper;
		private RunHelper _runHelper;
		private NumberingDefinitionsPartHelper _numberingDefinitionsPartHelper;

		public void TransformListValues(Stream stream, IDictionary<string, IList<string>> values)
		{
			if (values == null)
				throw new ArgumentNullException(nameof(values));

			using (var doc = WordprocessingDocument.Open(stream, true))
			{
				_bulletListHelper = new BulletListHelper(doc);
				_runHelper = new RunHelper(doc);
				_numberingDefinitionsPartHelper = new NumberingDefinitionsPartHelper(doc);

				var document = doc.MainDocumentPart.Document;

				ConvertFieldCodes(document);

				TransformElements(document, values);

				doc.Save();
				// process header(s)
				foreach (HeaderPart hPart in doc.MainDocumentPart.HeaderParts)
				{
					ConvertFieldCodes(hPart.Header);
					TransformElements(hPart.Header, values);
					hPart.Header.Save(); // save header back in package
				}

				// process footer(s)
				foreach (FooterPart fPart in doc.MainDocumentPart.FooterParts)
				{
					ConvertFieldCodes(fPart.Footer);
					TransformElements(fPart.Footer, values);
					fPart.Footer.Save(); // save footer back in package
				}
			}
		}

		public void Transform(Stream stream, IDictionary<string, string> values)
		{
			if (values == null)
				throw new ArgumentNullException(nameof(values));

			using (var doc = WordprocessingDocument.Open(stream, true))
			{
				var document = doc.MainDocumentPart.Document;
				doc.ChangeDocumentType(WordprocessingDocumentType.Document);

				ConvertFieldCodes(document);

				TransformElements(document, values);

				doc.Save();
				// process header(s)
				foreach (HeaderPart hPart in doc.MainDocumentPart.HeaderParts)
				{
					ConvertFieldCodes(hPart.Header);

					TransformElements(hPart.Header, values);
					hPart.Header.Save(); // save header back in package
				}

				// process footer(s)
				foreach (FooterPart fPart in doc.MainDocumentPart.FooterParts)
				{
					ConvertFieldCodes(fPart.Footer);

					TransformElements(fPart.Footer, values);
					fPart.Footer.Save(); // save footer back in package
				}
			}
		}

		internal void ConvertFieldCodes(OpenXmlElement mainElement)
		{
			var runs = mainElement.Descendants<Run>().ToArray();
			if (runs.Length == 0) return;

			var newFields = new Dictionary<Run, Run[]>();

			var cursor = 0;
			do
			{
				var run = runs[cursor];
				if (run.HasChildren && run.Descendants<FieldChar>().Any()
									&& (run.Descendants<FieldChar>().First().FieldCharType & FieldCharValues.Begin) ==
									FieldCharValues.Begin)
				{
					var innerRuns = new List<Run> { run };

					//  loop until we find the 'end' FieldChar
					var found = false;
					string instruction = null;
					RunProperties runProp = null;
					do
					{
						cursor++;
						run = runs[cursor];

						innerRuns.Add(run);
						if (run.HasChildren && run.Descendants<FieldCode>().Any())
							instruction += run.GetFirstChild<FieldCode>().Text;
						if (run.HasChildren && run.Descendants<FieldChar>().Any()
											&& (run.Descendants<FieldChar>().First().FieldCharType &
												FieldCharValues.End) == FieldCharValues.End)
						{
							found = true;
						}

						if (run.HasChildren && run.Descendants<RunProperties>().Any())
							runProp = run.GetFirstChild<RunProperties>();
					} while (found == false && cursor < runs.Length);

					//  something went wrong : found Begin but no End. Throw exception
					if (!found)
						throw new Exception("Found a Begin FieldChar but no End !");

					if (!string.IsNullOrEmpty(instruction))
					{
						//  build new Run containing a SimpleField
						var newRun = new Run();
						if (runProp != null)
							newRun.AppendChild(runProp.CloneNode(true));
						var simpleField = new SimpleField { Instruction = instruction };
						newRun.AppendChild(simpleField);

						newFields.Add(newRun, innerRuns.ToArray());
					}
				}

				cursor++;
			} while (cursor < runs.Length);

			foreach (var kvp in newFields)
			{
				kvp.Value[0].Parent.ReplaceChild(kvp.Key, kvp.Value[0]);
				for (var i = 1; i < kvp.Value.Length; i++)
					kvp.Value[i].Remove();
			}
		}

		private void TransformElements(OpenXmlElement element, IDictionary<string, IList<string>> values)
		{
			var emptyFields = new Dictionary<SimpleField, string[]>();

			var fieldList = element.Descendants<SimpleField>().ToList();

			foreach (var simpleField in fieldList)
			{
				var fieldName = GetFieldNameWithOptions(simpleField, out var switches, out var options);
				var r = GetFirstParent<Run>(simpleField);
				if (r == null)
					r = new Run();
				else
					r.RemoveChild(simpleField);
				if (!string.IsNullOrEmpty(fieldName))
				{
					if (values.ContainsKey(fieldName) && values.Values.Count != 0)
					{
						var fieldValues = values.First(x => x.Key == fieldName).Value.ToList();
						var runs = _runHelper.ConvertToRunList(fieldValues);
						PrepareListParagraphs(runs, r);
					}
				}
				else
				{
					// keep track of unknown or empty fields
					emptyFields[simpleField] = switches;
				}
			}

			foreach (var kvp in emptyFields)
			{
				ExecuteSwitches(kvp.Key, kvp.Value);
				if (kvp.Key.Parent != null)
					kvp.Key.Remove();
			}
		}

		private void TransformElements(OpenXmlElement element, IDictionary<string, string> values)
		{
			var emptyFields = new Dictionary<SimpleField, string[]>();

			var list = element.Descendants<SimpleField>().ToList();
			foreach (var field in list)
			{
				var fieldName = GetFieldNameWithOptions(field, out var switches, out var options);
				if (!string.IsNullOrEmpty(fieldName))
				{
					if (values.ContainsKey(fieldName)
					    && !string.IsNullOrEmpty(values[fieldName]))
					{
						var formattedText = ApplyFormatting(options[0],
							values[fieldName],
							options[1],
							options[2]);

						// Prepend any text specified to appear before the data in the MergeField
						if (!string.IsNullOrEmpty(options[1]))
						{
							field.Parent.InsertBeforeSelf<Paragraph>(
								GetPreOrPostParagraphToInsert(formattedText[1], field));
						}

						// Append any text specified to appear after the data in the MergeField
						if (!string.IsNullOrEmpty(options[2]))
						{
							field.Parent.InsertAfterSelf<Paragraph>(
								GetPreOrPostParagraphToInsert(formattedText[2], field));
						}

						// replace mergefield with text
						field.Parent.ReplaceChild<SimpleField>(GetRunElementForText(formattedText[0], field), field);
					}
					else
					{
						// keep track of unknown or empty fields
						emptyFields[field] = switches;
					}
				}
			}

			foreach (var kvp in emptyFields)
			{
				ExecuteSwitches(kvp.Key, kvp.Value);
				kvp.Key.Remove();
			}
		}

		internal void PrepareListParagraphs(List<Run> runs, Run r)
		{
			var paragraphs = _bulletListHelper.AddListToParagraphs(runs);
			var numberId = CreateNumberingEntries();

			foreach (Paragraph theParagraph in paragraphs)
			{
				// If the paragraph has no ParagraphProperties object, create one.
				if (!theParagraph.Elements<ParagraphProperties>().Any())
				{
					theParagraph.PrependChild(new ParagraphProperties());
				}

				// numberingProperties, spacingBetweenLines1, indentation, paragraphMarkRunProperties1
				// Get the paragraph properties element of the paragraph.
				ParagraphProperties pPr = theParagraph.Elements<ParagraphProperties>().First();

				var numberingProperties = new NumberingProperties(
					new NumberingLevelReference { Val = 0 },
					new NumberingId { Val = numberId });

				// Do not append since order matters.
				pPr.NumberingProperties = numberingProperties;
				r.AppendChild(theParagraph);
			}
		}

		internal Paragraph GetPreOrPostParagraphToInsert(string text, SimpleField fieldToMimic)
		{
			var runToInsert = GetRunElementForText(text, fieldToMimic);
			var paragraphToInsert = new Paragraph();
			paragraphToInsert.Append(runToInsert);

			return paragraphToInsert;
		}

		internal Run GetRunElementForText(string text, SimpleField placeHolder)
		{
			string rpr = null;
			if (placeHolder != null)
			{
				foreach (var placeholderRpr in placeHolder.Descendants<RunProperties>())
				{
					rpr = placeholderRpr.OuterXml;
					break; // break at first
				}
			}

			Run r = new Run();
			if (!string.IsNullOrEmpty(rpr))
			{
				r.Append(new RunProperties(rpr));
			}

			if (string.IsNullOrEmpty(text)) return r;
			// first process line breaks
			var split = text.Split(new[] { "\n" }, StringSplitOptions.None);
			var first = true;
			foreach (string s in split)
			{
				if (!first)
				{
					r.Append(new Break());
				}

				first = false;

				// then process tabs
				var firstTab = true;
				var tabSplit = s.Split(new[] { "\t" }, StringSplitOptions.None);
				foreach (var tabText in tabSplit)
				{
					if (!firstTab)
					{
						r.Append(new TabChar());
					}

					r.Append(new Text(tabText));
					firstTab = false;
				}
			}

			return r;
		}

		internal string[] ApplyFormatting(string format, string fieldValue, string preText, string postText)
		{
			var valuesToReturn = new string[3];

			switch (format)
			{
				case "UPPER":
					valuesToReturn[0] = fieldValue.ToUpper(CultureInfo.CurrentCulture);
					valuesToReturn[1] = preText.ToUpper(CultureInfo.CurrentCulture);
					valuesToReturn[2] = postText.ToUpper(CultureInfo.CurrentCulture);
					break;
				case "LOWER":
					valuesToReturn[0] = fieldValue.ToLower(CultureInfo.CurrentCulture);
					valuesToReturn[1] = preText.ToLower(CultureInfo.CurrentCulture);
					valuesToReturn[2] = postText.ToLower(CultureInfo.CurrentCulture);
					break;
				case "FirstCap":
					{
						if (!string.IsNullOrEmpty(fieldValue))
						{
							valuesToReturn[0] = fieldValue.Substring(0, 1).ToUpper(CultureInfo.CurrentCulture);
							if (fieldValue.Length > 1)
							{
								valuesToReturn[0] = valuesToReturn[0] +
													fieldValue.Substring(1).ToLower(CultureInfo.CurrentCulture);
							}
						}

						if (!string.IsNullOrEmpty(preText))
						{
							valuesToReturn[1] = preText.Substring(0, 1).ToUpper(CultureInfo.CurrentCulture);
							if (fieldValue != null && fieldValue.Length > 1)
							{
								valuesToReturn[1] = valuesToReturn[1] +
													preText.Substring(1).ToLower(CultureInfo.CurrentCulture);
							}
						}

						if (string.IsNullOrEmpty(postText)) return valuesToReturn;
						valuesToReturn[2] = postText.Substring(0, 1).ToUpper(CultureInfo.CurrentCulture);
						if (fieldValue != null && fieldValue.Length > 1)
						{
							valuesToReturn[2] =
								valuesToReturn[2] + postText.Substring(1).ToLower(CultureInfo.CurrentCulture);
						}

						break;
					}

				case "Caps":
					valuesToReturn[0] = ToTitleCase(fieldValue);
					valuesToReturn[1] = ToTitleCase(preText);
					valuesToReturn[2] = ToTitleCase(postText);
					break;
				default:
					valuesToReturn[0] = fieldValue;
					valuesToReturn[1] = preText;
					valuesToReturn[2] = postText;
					break;
			}

			return valuesToReturn;
		}

		internal string ToTitleCase(string toConvert)
		{
			return ToTitleCaseHelper(toConvert, string.Empty);
		}

		internal string ToTitleCaseHelper(string toConvert, string alreadyConverted)
		{
			if (string.IsNullOrEmpty(toConvert))
			{
				return alreadyConverted;
			}

			var indexOfFirstSpace = toConvert.IndexOf(' ');
			string firstWord, restOfString;

			// Check to see if we're on the last word or if there are more.
			if (indexOfFirstSpace != -1)
			{
				firstWord = toConvert.Substring(0, indexOfFirstSpace);
				restOfString = toConvert.Substring(indexOfFirstSpace).Trim();
			}
			else
			{
				firstWord = toConvert.Substring(0);
				restOfString = string.Empty;
			}

			var sb = new StringBuilder();

			sb.Append(alreadyConverted);
			sb.Append(" ");
			sb.Append(firstWord.Substring(0, 1).ToUpper(CultureInfo.CurrentCulture));

			if (firstWord.Length > 1)
			{
				sb.Append(firstWord.Substring(1).ToLower(CultureInfo.CurrentCulture));
			}

			return ToTitleCaseHelper(restOfString, sb.ToString());
		}

		internal string GetFieldNameWithOptions(SimpleField field, out string[] switches, out string[] options)
		{
			var instructionRegEx = new Regex(
				@"^[\s]*MERGEFIELD[\s]+(?<name>[#\w]*){1}
                            [\s]*(\\\*[\s]+(?<Format>[\w]*){1})?
                            [\s]*(\\b[\s]+[""]?(?<PreText>[^\\]*){1})?
                            [\s]*(\\f[\s]+[""]?(?<PostText>[^\\]*){1})?",
				RegexOptions.Compiled | RegexOptions.CultureInvariant |
				RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase |
				RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);
			var a = field.GetAttribute("instr", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
			switches = new string[0];
			options = new string[3];
			var fieldName = string.Empty;
			var instruction = a.Value;

			if (string.IsNullOrEmpty(instruction)) return fieldName;
			var m = instructionRegEx.Match(instruction);
			if (!m.Success) return fieldName;
			fieldName = m.Groups["name"].ToString().Trim();
			options[0] = m.Groups["Format"].Value.Trim();
			options[1] = m.Groups["PreText"].Value.Trim();
			options[2] = m.Groups["PostText"].Value.Trim();
			var pos = fieldName.IndexOf('#');
			if (pos <= 0) return fieldName;

			switches = fieldName.Substring(pos + 1).ToLower().Split(new[] { '#' }, StringSplitOptions.RemoveEmptyEntries);
			fieldName = fieldName.Substring(0, pos);

			return fieldName;
		}

		internal void ExecuteSwitches(OpenXmlElement element, string[] switches)
		{
			if (switches == null || !switches.Any())
			{
				return;
			}

			// check switches (switches are always lowercase)
			if (switches.Contains("dp"))
			{
				var p = GetFirstParent<Paragraph>(element);
				p?.Remove();
			}
			else if (switches.Contains("dr"))
			{
				var row = GetFirstParent<TableRow>(element);
				row?.Remove();
			}
			else if (switches.Contains("dt"))
			{
				var table = GetFirstParent<Table>(element);
				table?.Remove();
			}
		}

		internal T GetFirstParent<T>(OpenXmlElement element)
			where T : OpenXmlElement
		{
			if (element.Parent == null)
			{
				return null;
			}

			if (element.Parent.GetType() == typeof(T))
			{
				return element.Parent as T;
			}

			return GetFirstParent<T>(element.Parent);
		}

		protected int CreateNumberingEntries()
		{
			NumberingDefinitionsPart numberingPart = _numberingDefinitionsPartHelper.GetOrCreate();

			// Add AbstractNum
			var abstractNumberId = numberingPart.Numbering.Elements<AbstractNum>().Count() + 1;
			var abstractNum = new AbstractNum { AbstractNumberId = abstractNumberId };

			// Level 1
			var level = new Level { LevelIndex = 0 };
			NumberingFormat numberingFormat = new NumberingFormat { Val = NumberFormatValues.Bullet };
			LevelText levelText = new LevelText { Val = "o" };
			level.Append(numberingFormat);
			level.Append(levelText);

			// Add it
			abstractNum.Append(level);
			_numberingDefinitionsPartHelper.AddAbstractNum(abstractNum); // Order matters and this method will take care of that.

			// Add NumberingInstance
			int numberId = numberingPart.Numbering.Elements<NumberingInstance>().Count() + 1;
			NumberingInstance numberingInstance = new NumberingInstance { NumberID = numberId };
			AbstractNumId abstractNumId = new AbstractNumId { Val = abstractNumberId };
			numberingInstance.Append(abstractNumId);
			_numberingDefinitionsPartHelper.AddNumberingInstance(numberingInstance); // Order matters and this method will take care of that.

			return numberId;
		}
	}
}