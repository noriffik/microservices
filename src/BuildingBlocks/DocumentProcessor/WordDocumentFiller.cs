﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using NexCore.DocumentProcessor.Exceptions;
using NexCore.DocumentProcessor.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NexCore.DocumentProcessor
{
	public class WordDocumentFiller : IDocumentFiller
	{
		private const string KeyTemplate = "{0}:";
        private const string RequiredKeyTemplate = @"{0}*:";

        public virtual void Transform(Stream stream, IDictionary<string, string> values)
        {
            if (values == null)
                throw new ArgumentNullException(nameof(values));

            using (var wordDoc = WordprocessingDocument.Open(stream, true))
            {
	            Transform(wordDoc, values);
				wordDoc.Save();
            }
        }

        public void TransformListValues(Stream stream, IDictionary<string, IList<string>> values)
        {
			if (values == null)
				throw new ArgumentNullException(nameof(values));

			using (var wordDoc = WordprocessingDocument.Open(stream, true))
			{
				Transform(wordDoc, values);
				wordDoc.Save();
			}
		}

		private void Transform(WordprocessingDocument doc, IDictionary<string, string> fieldValues)
        {
	        var document = doc.MainDocumentPart.Document;
	        var elements = document.Descendants<Text>().ToList();

			foreach (var element in elements)
	        {
		        foreach (var (field, value) in fieldValues)
		        {
			        var key = string.Format(KeyTemplate, field);
			        var requiredKey = string.Format(RequiredKeyTemplate, field);

			        if (element.Text.Contains(key))
			        {
				        element.Text = element.Text.Replace(key, value);
			        }

			        if (!element.Text.Contains(requiredKey)) continue;
			        if (string.IsNullOrEmpty(value))
				        throw new MissingRequiredValueException(field);

			        element.Text = element.Text.Replace(requiredKey, value);
		        }
	        }
        }
		
		private void Transform(WordprocessingDocument doc, IDictionary<string, IList<string>> fieldValues)
        {
	        var bulletListHelper = new BulletListHelper(doc);
			var paragraphHelper = new ParagraphHelper(doc);

	        var document = doc.MainDocumentPart.Document;
	        var elements = document.Descendants<Text>().ToList();

			foreach (var element in elements)
	        {
		        foreach (var (field, value) in fieldValues)
		        {
			        var key = string.Format(KeyTemplate, field);
			        var requiredKey = string.Format(RequiredKeyTemplate, field);
			        if (element.Text.Contains(key) || element.Text.Contains(requiredKey))
			        {
				        bulletListHelper.AddList(value.ToList());
						paragraphHelper.RemoveParagraph((Paragraph)element.Parent.Parent);
			        }
		        }
			}
        }
	}
}