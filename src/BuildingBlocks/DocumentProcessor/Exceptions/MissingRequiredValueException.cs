﻿using System;

namespace NexCore.DocumentProcessor.Exceptions
{
    public class MissingRequiredValueException : Exception
    {
        public MissingRequiredValueException(string field)
        {
            Field = field;
        }

        public string Field { get; }
    }
}