﻿using System;
using System.Collections.Generic;

namespace NexCore.Domain
{
    public abstract class EntityId : EntityId<string>
    {
        protected EntityId()
        {
        }

        protected EntityId(string value)
        {
            if (string.IsNullOrEmpty(value))
                throw new ArgumentException("Value cannot be null or empty.", nameof(value));

            Value = value.ToUpperInvariant();
        }
    }

    public class EntityId<T> : ValueObject, IComparable<EntityId<T>>
        where T : IComparable<T>
    {
        public T Value { get; protected set; }

        protected EntityId()
        {
        }

        protected EntityId(T value)
        {
            Value = value;
        }
        
        protected override IEnumerable<object> GetValues()
        {
            yield return Value;
        }

        public int CompareTo(EntityId<T> other)
        {
            return Value.CompareTo(other.Value);
        }

        public static implicit operator T(EntityId<T> id)
        {
            return id.Value;
        }
    }
}
