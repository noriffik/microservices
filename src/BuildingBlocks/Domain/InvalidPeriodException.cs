﻿using System;

namespace NexCore.Domain
{
    public class InvalidPeriodException : Exception
    {
        private const string DefaultMessage = "Given dates is invalid for period.";

        public InvalidPeriodException() : base(DefaultMessage)
        {
        }

        public InvalidPeriodException(string message) : base(message)
        {
        }
    }
}
