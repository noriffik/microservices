﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.Domain
{
    public interface IPeriodValidator
    {
        bool IsValid(DateTime? from, DateTime? to);
    }

    public abstract class PeriodValidatorBase : IPeriodValidator
    {
        public virtual bool IsValid(DateTime? from, DateTime? to)
        {
            return to == null || from == null || from.Value.Date < to.Value.Date;
        }
    }

    public class PeriodValidator : PeriodValidatorBase
    {
        private readonly bool _isFromRequired;
        private readonly bool _isToRequired;
        private readonly bool _isOptional;
        
        public static readonly PeriodValidator NoneRequired = new PeriodValidator(false, false);
        public static readonly PeriodValidator FromRequired = new PeriodValidator(true, false);
        public static readonly PeriodValidator ToRequired = new PeriodValidator(false, true);
        public static readonly PeriodValidator BothRequired = new PeriodValidator(true, true);

        private PeriodValidator(bool isFromRequired, bool isToRequired, bool isOptional = false)
        {
            _isFromRequired = isFromRequired;
            _isToRequired = isToRequired;
            _isOptional = isOptional;
        }

        public PeriodValidator IsOptional()
        {
            return new PeriodValidator(_isFromRequired, _isToRequired, true);
        }

        public override bool IsValid(DateTime? from, DateTime? to)
        {
            if (_isOptional && !from.HasValue && !to.HasValue)
                return true;

            if (_isFromRequired && !from.HasValue)
                return false;

            if (_isToRequired && !to.HasValue)
                return false;

            return base.IsValid(from, to);
        }
    }

    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public abstract class PeriodBase : ValueObject
    {
        public DateTime? From { get; private set; }

        public DateTime? To { get; private set; }

        protected readonly IPeriodValidator Validator;

        protected PeriodBase(IPeriodValidator validator)
        {
            Validator = validator;
        }

        protected PeriodBase(IPeriodValidator validator, DateTime? from, DateTime? to)
        {
            if (!validator.IsValid(from, to))
                throw new InvalidPeriodException();

            Validator = validator;
            From = from?.Date;
            To = to?.Date;
        }

        public virtual bool IsIn(DateTime date)
        {
            return (!From.HasValue || From <= date.Date) && (date.Date < To || !To.HasValue);
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return From;
            yield return To;
        }

        public override string ToString()
        {
            if (From.HasValue && To.HasValue)
                return $"{From.Value:dd.MM.yyyy} - {To.Value:dd.MM.yyyy}";

            if (From.HasValue)
                return $"From {From.Value:dd.MM.yyyy}";

            if (To.HasValue)
                return $"To {To.Value:dd.MM.yyyy}";

            return string.Empty;
        }
    }

    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    public class Period : PeriodBase
    {
        protected Period() : base(PeriodValidator.FromRequired)
        {
        }

        public Period(DateTime from, DateTime? to) : base(PeriodValidator.FromRequired, from, to)
        {
        }

        public Period(DateTime from) : this(from, null)
        {
        }

        public Period(Year year) : this(new DateTime(year.Value, 1, 1), new DateTime(year.Value, 12, 31))
        {
        }

        internal Period(IPeriodValidator validator, DateTime? from, DateTime? to) : base(validator, from, to)
        {
        }

        public bool IsOverlapping(Period period) => !IsNonOverlapping(period);

        public bool IsNonOverlapping(Period period)
        {
            if (period == null)
                throw new ArgumentNullException(nameof(period));

            return IsNonOverlapping(this, period);
        }

        public static bool IsOverlapping(Period a, Period b) => !IsNonOverlapping(a, b);

        public static bool IsNonOverlapping(Period a, Period b)
        {
            if (a == null)
                throw new ArgumentNullException(nameof(a));

            if (b == null)
                throw new ArgumentNullException(nameof(b));

            return b.From < a.From && b.To < a.From || b.From > a.From && b.From > a.To;
        }

        public Period WithAnotherEnding(DateTime periodTo)
        {
            return new Period(From.GetValueOrDefault(), periodTo);
        }
    }
}
