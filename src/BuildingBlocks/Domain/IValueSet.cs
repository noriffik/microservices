﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Domain
{
    public interface IValueSet<TValue>
    {
        string AsString { get; }

        IEnumerable<TValue> Values { get; }
    }

    public class BaseValueSet<TValue> : ValueObject, IValueSet<TValue>
    {
        private readonly Func<string, TValue> _valueParser;

        protected HashSet<TValue> ValueSet;

        public string AsString { get; }

        public IEnumerable<TValue> Values
        {
            get
            {
                if (ValueSet == null)
                    ValueSet = ParseInternal(AsString);

                return ValueSet.ToList().AsReadOnly();
            }
        }

        public BaseValueSet(Func<string, TValue> valueParser)
        {
            ValueSet = new HashSet<TValue>();
            AsString = string.Empty;
            _valueParser = valueParser;
        }

        public BaseValueSet(IValueSet<TValue> set, Func<string, TValue> valueParser)
        {
            ValueSet = new HashSet<TValue>(set.Values);
            AsString = set.AsString;
            _valueParser = valueParser;
        }

        public BaseValueSet(IEnumerable<TValue> modelKeys, Func<string, TValue> valueParser)
        {
            ValueSet = new HashSet<TValue>(modelKeys.OrderBy(n => n.ToString()));
            AsString = FormatAsString(ValueSet);

            _valueParser = valueParser;
        }

        public static string FormatAsString(IEnumerable<TValue> values)
        {
            return values.Any()
                ? values.Select(n => n.ToString()).Aggregate((a, b) => $"{a} {b}")
                : string.Empty;
        }

        public BaseValueSet(string values, Func<string, TValue> valueParser)
        {
            _valueParser = valueParser;
            ValueSet = ParseInternal(values);
            AsString = FormatAsString(ValueSet);
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return AsString;
        }

        public static IValueSet<TValue> TryParse(string value, Func<string, TValue> valueParser)
        {
            if (value == null)
                return null;

            try
            {
                return Parse(value, valueParser);
            }
            catch (FormatException)
            {
                return null;
            }
        }

        public static IValueSet<TValue> Parse(string value, Func<string, TValue> valueParser)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            return new BaseValueSet<TValue>(value, valueParser);
        }

        protected HashSet<TValue> ParseInternal(string values)
        {
            return new HashSet<TValue>(values.Split(' ')
                .Where(v => v != string.Empty)
                .OrderBy(v => v)
                .Select(v => _valueParser(v)));
        }

        public IValueSet<TValue> Expand(string with)
        {
            return new BaseValueSet<TValue>(ParseInternal(AsString + " " + with), _valueParser);
        }

        public IValueSet<TValue> Expand(IEnumerable<TValue> with)
        {
            if (with == null)
                throw new ArgumentNullException(nameof(with));

            return new BaseValueSet<TValue>(Values.Concat(with), _valueParser);
        }

        public IValueSet<TValue> Expand(IValueSet<TValue> with)
        {
            if (with == null)
                throw new ArgumentNullException(nameof(with));

            return Expand(with.Values);
        }

        public IValueSet<TValue> Shrink(string with)
        {
            return Shrink(Parse(with, _valueParser));
        }

        public IValueSet<TValue> Shrink(IEnumerable<TValue> with)
        {
            if (with == null)
                throw new ArgumentNullException(nameof(with));

            return new BaseValueSet<TValue>(Values.Except(with), _valueParser);
        }

        public IValueSet<TValue> Shrink(IValueSet<TValue> with)
        {
            if (with == null)
                throw new ArgumentNullException(nameof(with));

            return Shrink(with.Values);
        }
    }
}