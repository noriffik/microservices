﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.Domain
{
    public class AllSpecification<T> : Specification<T> where T : Entity
    {
        private Specification<T> _inner;
        private readonly string _innerDescriptions;

        public AllSpecification(IEnumerable<Specification<T>> inner)
        {
            if (inner == null)
                throw new ArgumentNullException(nameof(inner));

            _inner = ChainOrSpecifications(inner);

            _innerDescriptions = inner.Any()
                ? inner.Select(s => $@"""{s.Description}""").Aggregate((a, b) => $"{a}, {b}") : "";
        }

        public AllSpecification(params Specification<T>[] inner) : this(inner.AsEnumerable())
        {
        }

        private static Specification<T> ChainOrSpecifications(IEnumerable<Specification<T>> inner)
        {
            var chain = inner.Any() ? inner.First() : new EmptySpecification<T>();

            if (inner.Count() == 1)
                return chain;

            foreach (var specification in inner.Skip(1))
                chain = new AndSpecification<T>(chain, specification);

            return chain;
        }
        
        public override Expression<Func<T, bool>> ToExpression()
        {
            return _inner.ToExpression();
        }

        public override string Description => $@"Satisfying all of specifications: {_innerDescriptions}";
    }
}
