﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Domain
{
    public abstract class BaseTwoWayMapper<TSource, TDestination> : BaseMapper<TSource, TDestination>, ITwoWayMapper<TSource, TDestination>
        where TSource : class
        where TDestination : class
    {
        public abstract TSource MapReverse(TDestination destination);

        public IEnumerable<TSource> MapReverse(IEnumerable<TDestination> destination)
            => destination != null 
                ? destination.ToList().Select(MapReverse).Where(d => d != null)
                : throw new ArgumentNullException(nameof(destination));
    }
}
