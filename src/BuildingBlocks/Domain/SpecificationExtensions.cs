﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Domain
{
    public static class SpecificationExtensions
    {
        public static IQueryable<T> Apply<T>(this IQueryable<T> query, Specification<T> specification)
        {
            if (specification == null)
                throw new ArgumentNullException(nameof(specification));

            return query.Where(specification.ToExpression());
        }

        public static IEnumerable<T> Apply<T>(this IEnumerable<T> enumerable, Specification<T> specification)
        {
            return enumerable.AsQueryable().Apply(specification);
        }

        public static bool All<T>(this IEnumerable<T> enumerable, Specification<T> specification)
        {
            var query = enumerable.ToList().AsQueryable();

            return query.Apply(specification).Count() == query.Count();
        }
    }
}
