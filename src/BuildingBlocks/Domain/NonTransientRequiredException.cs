﻿using System;

namespace NexCore.Domain
{
    public class NonTransientRequiredException : Exception
    {
        private const string DefaultMessage = "Inner object is transient";

        public object Object { get; }

        public NonTransientRequiredException() : base(DefaultMessage)
        {
        }

        public NonTransientRequiredException(string message) : base(message)
        {
        }

        public NonTransientRequiredException(string message, Exception inner) : base(message, inner)
        {
        }

        public NonTransientRequiredException(string message, object @object, Exception innerException)
            : this(message, innerException)
        {
            Object = @object;
        }

        public NonTransientRequiredException(string message, object @object)
            : this(message, @object, null)
        {
        }

        public NonTransientRequiredException(object @object)
            : this(DefaultMessage, @object)
        {
        }
    }
}
