﻿using System;

namespace NexCore.Domain
{
    public class ProhibitedByStatusException : Exception
    {
        private const string DefaultMessage = "Command is not applicable to object in current status";
        private const string DefaultExtendedMessage = "Command is not applicable to {0} in {1} status";

        public string StatusDescription { get; }

        public Type ObjectType { get; }

        public ProhibitedByStatusException() : base(DefaultMessage)
        {
        }

        public ProhibitedByStatusException(string message) : base(message)
        {
        }

        public ProhibitedByStatusException(string message, Exception inner) : base(message, inner)
        {
        }

        public ProhibitedByStatusException(string message, string statusDescription, Type objectType, Exception innerException)
            : this(message, innerException)
        {
            StatusDescription = statusDescription;
            ObjectType = objectType;
        }

        public ProhibitedByStatusException(string message, string statusDescription, Type objectType)
            : this(message, statusDescription, objectType, null)
        {
        }

        public ProhibitedByStatusException(string statusDescription, Type objectType)
            : this(string.Format(DefaultExtendedMessage, objectType.Name, statusDescription), statusDescription, objectType, null)
        {
        }
    }

}
