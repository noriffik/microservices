﻿using System.Collections.Generic;

namespace NexCore.Domain
{
    public interface ITwoWayMapper<TSource, TDestination> : IMapper<TSource, TDestination>
        where TSource : class
        where TDestination : class
    {
        TSource MapReverse(TDestination destination);

        IEnumerable<TSource> MapReverse(IEnumerable<TDestination> destination);
    }
}
