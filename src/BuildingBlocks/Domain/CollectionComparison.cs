﻿using System.Collections;
using System.Linq;

namespace NexCore.Domain
{
    public abstract class CollectionComparison
    {
        public static CollectionComparison Ordered = new OrderedCollectionComparison();
        public static CollectionComparison Unordered = new UnorderedCollectionComparison();

        public abstract bool Compare(IEnumerable itemsA, IEnumerable itemsB, IEqualityComparer comparer);

        protected IList Copy(IEnumerable items)
        {
            var list = new ArrayList();
            var e = items.GetEnumerator();

            while (e.MoveNext())
                list.Add(e.Current);

            return list;
        }
    }

    public class OrderedCollectionComparison : CollectionComparison
    {
        public override bool Compare(IEnumerable itemsA, IEnumerable itemsB, IEqualityComparer comparer)
        {
            if (itemsB == null)
                return false;

            var a = Copy(itemsA);
            var b = Copy(itemsB);

            if (a.Count != b.Count)
                return false;

            for (var i = 0; i < a.Count; i++)
            {
                if (!comparer.Equals(a[i], b[i]))
                    return false;
            }

            return true;
        }
    }

    public class UnorderedCollectionComparison : CollectionComparison
    {
        public override bool Compare(IEnumerable itemsA, IEnumerable itemsB, IEqualityComparer comparer)
        {
            if (itemsB == null)
                return false;

            var a = Copy(itemsA);
            var b = Copy(itemsB);

            if (a.Count != b.Count)
                return false;

            foreach (var itemInA in a)
            {
                if (!b.Cast<object>().Any(itemInB => comparer.Equals(itemInA, itemInB)))
                    return false;
            }

            return true;
        }
    }
}
