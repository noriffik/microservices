﻿using System;

namespace NexCore.Domain
{
    public class WorkConcurrencyException : WorkFailedException
    {
        private const string DefaultMessage = "Failed to commit work due to concurrency issues.";

        public WorkConcurrencyException() : base(DefaultMessage)
        {
        }

        public WorkConcurrencyException(Exception innerException) : base(DefaultMessage, innerException)
        {
        }

        public WorkConcurrencyException(string message) : base(message)
        {
        }

        public WorkConcurrencyException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
