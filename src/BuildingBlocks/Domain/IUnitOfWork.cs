﻿using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Domain
{
    public interface IUnitOfWork
    {
        IEntityRepository EntityRepository { get; }

        Task Commit(CancellationToken cancellationToken = default(CancellationToken));
    }
}