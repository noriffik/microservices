﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.Domain
{
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class Year : ValueObject
    {
        public int Value { get; private set; }

        private Year()
        {
        }

        public Year(int value)
        {
            if (DateTime.MinValue.Year > value || DateTime.MaxValue.Year < value)
                throw new ArgumentOutOfRangeException(nameof(value));

            Value = value;
        }

        public static Year From(DateTime time)
        {
            return new Year(time.Year);
        }

        public static Year From(int year)
        {
            return new Year(year);
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return Value;
        }

        public static implicit operator int(Year year)
        {
            return year?.Value ?? default(int);
        }

        public static implicit operator Year(int value)
        {
            return From(value);
        }
    }
}
