﻿using System;
using System.Linq.Expressions;

namespace NexCore.Domain
{
    public class IdSpecification<TEntity> : Specification<TEntity> where TEntity : IAggregateRoot
    {
        public int Id { get; }

        public IdSpecification(int id)
        {
            Id = id;
        }

        public override Expression<Func<TEntity, bool>> ToExpression()
        {
            return entity => entity.Id == Id;
        }

        public override string Description => $@"{typeof(TEntity).Name} with id ""{Id}""";
    }

    public class IdSpecification<TEntity, TId> : Specification<TEntity>
        where TEntity : IAggregateRoot<TId> where TId : EntityId
    {
        public TId Id { get; }

        public IdSpecification(TId id)
        {
            Id = id ?? throw new ArgumentNullException(nameof(id));
        }

        public override Expression<Func<TEntity, bool>> ToExpression()
        {
            return entity => Convert.ToString(entity.Id) == Convert.ToString(Id);
        }

        public override string Description => $@"{typeof(TEntity).Name} with id ""{Id}""";
    }
}
