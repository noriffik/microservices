﻿using MediatR;

namespace NexCore.Domain
{
    public interface IEntityEvent : INotification
    {
    }

    public class AggregateRootCreatedEvent<TEntity> : IEntityEvent where TEntity : IAggregateRoot
    {
        public int Id { get; }

        public AggregateRootCreatedEvent(int id)
        {
            Id = id;
        }
    }

    public class AggregateRootCreatedEvent<TEntity, TId>
        : IEntityEvent where TEntity : IAggregateRoot<TId> where TId : EntityId
    {
        public TId Id { get; set; }

        public AggregateRootCreatedEvent(TId id)
        {
            Id = id;
        }
    }
}
