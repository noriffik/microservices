﻿using System;

namespace NexCore.Domain
{
    public class SpecificationViolationException : DomainException
    {
        public string Name { get; }

        public SpecificationViolationException()
        {
        }

        public SpecificationViolationException(string message) : base(message)
        {
        }

        public SpecificationViolationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public SpecificationViolationException(ISpecification specification) : base(specification?.Description)
        {
            Name = specification?.Name;
        }

        public SpecificationViolationException(string message, ISpecification specification) : base(message)
        {
            Name = specification?.Name;
        }
    }
}