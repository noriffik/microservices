﻿using System;

namespace NexCore.Domain
{
    public class InvalidEntityIdException : Exception
    {
        private const string DefaultMessage = "Entity with given ID does not exist.";
        private const string DefaultEntityTypeMessage = "Entity {0} with given ID does not exist.";

        public InvalidEntityIdException() : base(DefaultMessage)
        {
        }

        public InvalidEntityIdException(string message) : base(message)
        {
        }

        public InvalidEntityIdException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public InvalidEntityIdException(string message, Type entityType, object id, Exception innerException) : this(message, innerException)
        {
            EntityType = entityType;
            Id = id;
        }

        public InvalidEntityIdException(string message, Type entityType, object id) : this(message, entityType, id, null)
        {
        }

        public InvalidEntityIdException(Type entityType, object id) : this(string.Format(DefaultEntityTypeMessage, entityType.Name), entityType, id)
        {
        }

        public Type EntityType { get; }

        public object Id { get; }
    }
}
