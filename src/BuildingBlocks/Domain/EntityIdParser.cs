﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace NexCore.Domain
{
    public abstract class BaseEntityIdParser
    {
        protected readonly ConcurrentDictionary<Type, ConstructorInfo> ConstructorInfos =
            new ConcurrentDictionary<Type, ConstructorInfo>();

        public virtual TId Parse<TId>(string value, int length = -1) where TId : EntityId
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (!TryParse<TId>(value, out var id, length))
                throw new FormatException();

            return id;
        }

        public virtual bool TryParse<TId>(string value, out TId id, int length = -1) where TId : EntityId
        {
            var type = typeof(TId);
            if (IsValid(value, length))
            {
                if (!ConstructorInfos.ContainsKey(type))
                {
                    ConstructorInfos[type] = typeof(TId).GetConstructor(
                        BindingFlags.Instance | BindingFlags.NonPublic,
                        null, new[] { typeof(string) }, null);
                }

                id = (TId)ConstructorInfos[type].Invoke(new object[] { value });
            }
            else
                id = null;

            return id != null;
        }

        protected abstract bool IsValid(string value, int length);
    }

    public class AlphaNumericEntityIdParser : BaseEntityIdParser
    {
        public static AlphaNumericEntityIdParser Instance = new AlphaNumericEntityIdParser();

        private static readonly Regex Pattern = new Regex("^[a-zA-Z0-9]*$");

        protected override bool IsValid(string value, int length)
        {
            return !string.IsNullOrEmpty(value) && (length < 1 || value.Length == length) && Pattern.IsMatch(value);
        }
    }

    public class NumericEntityIdParser : BaseEntityIdParser
    {
        public static NumericEntityIdParser Instance = new NumericEntityIdParser();

        protected override bool IsValid(string value, int length)
        {
            return !string.IsNullOrEmpty(value) && (length < 1 || value.Length == length) && value.All(char.IsDigit);
        }
    }
}
