﻿using MediatR;

namespace NexCore.Domain
{
    public interface IEntityEventHandler<in TEvent> : INotificationHandler<TEvent> where TEvent : IEntityEvent
    {
    }
}
