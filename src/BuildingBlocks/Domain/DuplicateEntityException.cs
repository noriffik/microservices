﻿using System;

namespace NexCore.Domain
{
    public class DuplicateEntityException : InvalidEntityIdException
    {
        private const string DefaultMessage = "Entity with given ID already exists.";
        private const string DefaultEntityTypeMessage = "Entity {0} with given ID already exists.";

        public DuplicateEntityException() : base(DefaultMessage)
        {
        }

        public DuplicateEntityException(string message) : base(message)
        {
        }

        public DuplicateEntityException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public DuplicateEntityException(string message, Type entityType, object id, Exception innerException) : base(message, entityType, id, innerException)
        {
        }

        public DuplicateEntityException(string message, Type entityType, object id) : base(message, entityType, id)
        {
        }

        public DuplicateEntityException(Type entityType, object id) : base(string.Format(DefaultEntityTypeMessage, entityType.Name), entityType, id)
        {
        }
    }
}
