﻿using System;
using System.Linq.Expressions;

namespace NexCore.Domain
{ 
    public abstract class Specification<T> : ISpecification
    {
        private Func<T, bool> _predicate;

        public virtual bool IsSatisfiedBy(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            if (_predicate == null)
                _predicate = ToExpression().Compile();

            return _predicate(entity);
        }

        public abstract Expression<Func<T, bool>> ToExpression();

        public virtual string Name => GetType().Name;

        public abstract string Description { get; }
    }

    public interface ISpecification
    {
        string Name { get; }

        string Description { get; }
    }
}