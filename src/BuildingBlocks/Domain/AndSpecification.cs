﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.Domain
{
    public class AndSpecification<T> : Specification<T>
    {
        private readonly Specification<T> _left;
        private readonly Specification<T> _right;

        public AndSpecification(Specification<T> left, Specification<T> right)
        {
            _left = left ?? throw new ArgumentNullException(nameof(left));
            _right = right ?? throw new ArgumentNullException(nameof(right));
        }

        public override Expression<Func<T, bool>> ToExpression()
        {
            var left = _left.ToExpression();
            var right = _right.ToExpression();

            var andAlso = Expression.AndAlso(left.Body, Expression.Invoke(right, left.Parameters.Single()));

            return Expression.Lambda<Func<T, bool>>(andAlso, left.Parameters.Single());
        }

        public override string Description => $@"Satisfying specifications ""{_left.Description}"" and ""{_right.Description}""";
    }
}
