﻿using System;
using System.Collections.Generic;

namespace NexCore.Domain
{
    public abstract class Entity : IEventful
    {
        private int? _hashCode;
        private readonly List<IEntityEvent> _entityEvents = new List<IEntityEvent>();
        
        public int Id { get; protected set; }

        public IEnumerable<IEntityEvent> EntityEvents => _entityEvents.AsReadOnly();

        public bool IsTransient()
        {
            return Id.Equals(default(int));
        }

        protected Entity()
        {
        }

        protected Entity(int id)
        {
            Id = id;
        }

        public override bool Equals(object obj)
        {
            return obj is Entity other
                   && GetType() == obj.GetType()
                   && ((!IsTransient() && !other.IsTransient() && Id.Equals(other.Id)) || ReferenceEquals(this, other));
        }

        public override int GetHashCode()
        {
            if (IsTransient())
                return base.GetHashCode();

            if (!_hashCode.HasValue)
                _hashCode = HashCode.Combine(GetType(), Id);

            return _hashCode.Value;
        }

        public virtual void AddEntityEvent(IEntityEvent entityEvent)
        {
            if (entityEvent == null)
                throw new ArgumentNullException(nameof(entityEvent));

            _entityEvents.Add(entityEvent);
        }

        public virtual void RemoveEntityEvent(IEntityEvent entityEvent)
        {
            if (entityEvent == null)
                throw new ArgumentNullException(nameof(entityEvent));

            _entityEvents.Remove(entityEvent);
        }

        public virtual void ClearEntityEvents()
        {
            _entityEvents.Clear();
        }

        public static bool operator ==(Entity left, Entity right)
        {
            return left?.Equals(right) ?? Equals(right, null);
        }

        public static bool operator !=(Entity left, Entity right)
        {
            return !(left == right);
        }
    }

    public abstract class Entity<TId> : IEventful where TId : EntityId
    {
        private int? _hashCode;
        private readonly List<IEntityEvent> _entityEvents = new List<IEntityEvent>();
        
        public TId Id { get; private set; }

        public IEnumerable<IEntityEvent> EntityEvents => _entityEvents.AsReadOnly();

        protected Entity()
        {
        }

        protected Entity(TId id)
        {
            Id = id ?? throw new ArgumentNullException(nameof(id));
            _hashCode = HashCode.Combine(GetType(), Id);
        }

        public override bool Equals(object obj)
        {
            return obj is Entity<TId> other
                && GetType() == obj.GetType()
                && Id.Equals(other.Id);
        }

        public override int GetHashCode()
        {
            if (_hashCode == null)
                _hashCode = HashCode.Combine(GetType(), Id);

            return _hashCode.Value;
        }

        public virtual void AddEntityEvent(IEntityEvent entityEvent)
        {
            if (entityEvent == null)
                throw new ArgumentNullException(nameof(entityEvent));

            _entityEvents.Add(entityEvent);
        }

        public virtual void RemoveEntityEvent(IEntityEvent entityEvent)
        {
            if (entityEvent == null)
                throw new ArgumentNullException(nameof(entityEvent));

            _entityEvents.Remove(entityEvent);
        }

        public virtual void ClearEntityEvents()
        {
            _entityEvents.Clear();
        }

        public static bool operator ==(Entity<TId> left, Entity<TId> right)
        {
            return left?.Equals(right) ?? Equals(right, null);
        }

        public static bool operator !=(Entity<TId> left, Entity<TId> right)
        {
            return !(left == right);
        }
    }
}