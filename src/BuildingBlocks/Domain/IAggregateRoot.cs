﻿namespace NexCore.Domain
{
    public interface IAggregateRoot
    {
        int Id { get; }
    }

    public interface IAggregateRoot<out TId> where TId : EntityId
    {
        TId Id { get; }
    }
}
