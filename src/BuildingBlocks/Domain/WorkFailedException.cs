﻿using System;

namespace NexCore.Domain
{
    public class WorkFailedException : DomainException
    {
        private const string DefaultMessage = "Failed to commit work.";

        public WorkFailedException() : base(DefaultMessage)
        {
        }

        public WorkFailedException(Exception innerException) : base(DefaultMessage, innerException)
        {
        }

        public WorkFailedException(string message) : base(message)
        {
        }

        public WorkFailedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
