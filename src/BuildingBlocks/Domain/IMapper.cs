﻿using System.Collections.Generic;

namespace NexCore.Domain
{
    public interface IMapper<in TSource, out TDestination>
        where TSource : class
        where TDestination : class
    {
        TDestination Map(TSource source);

        IEnumerable<TDestination> MapMany(IEnumerable<TSource> source);
    }
}
