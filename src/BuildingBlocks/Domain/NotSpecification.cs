﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.Domain
{
    public class NotSpecification<T> : Specification<T>
    {
        private readonly Specification<T> _specification;

        public NotSpecification(Specification<T> specification)
        {
            _specification = specification ?? throw new ArgumentNullException(nameof(specification));
        }

        public override Expression<Func<T, bool>> ToExpression()
        {
            var specification = _specification.ToExpression();

            var not = Expression.Not(specification.Body);

            return Expression.Lambda<Func<T, bool>>(not, specification.Parameters.Single());
        }

        public override string Description => $@"Not Satisfying specification ""{_specification.Description}""";
    }
}
