﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Domain
{
    public abstract class ValueObject
    {
        private string _toStringResult;

        public override int GetHashCode()
        {
            var hashCode = new HashCode();
            hashCode.Add(GetType());
            foreach (var value in GetValues())
                hashCode.Add(ValueComparer.Instance.GetHashCode(value));

            return hashCode.ToHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ValueObject valueObject) || GetType() != valueObject.GetType())
                return false;

            return CollectionComparison.Ordered.Compare(GetValues(), valueObject.GetValues(), ValueComparer.Instance);
        }

        protected abstract IEnumerable<object> GetValues();

        public static bool operator ==(ValueObject valueObject1, ValueObject valueObject2)
        {
            return valueObject1?.Equals(valueObject2) ?? Equals(valueObject2, null);
        }

        public static bool operator !=(ValueObject valueObject1, ValueObject valueObject2)
        {
            return !(valueObject1 == valueObject2);
        }

        public static bool IsNullOrEmpty(ValueObject value)
        {
            return value == null || !value.GetValues().Any(v => v != null && (v as string) != string.Empty);
        }

        public override string ToString()
        {
            if (_toStringResult != null) return _toStringResult;

            var values = GetValues()
                .Where(v => v != null)
                .Where(v => v.ToString().Trim() != string.Empty)
                .ToList();

            _toStringResult = values.Any()
                ? values.Aggregate((a, b) => $"{a}, {b}").ToString()
                : string.Empty;

            return _toStringResult;
        }

        private class ValueComparer : IEqualityComparer
        {
            private static readonly Lazy<ValueComparer> LazyInstance = new Lazy<ValueComparer>(() => new ValueComparer());

            public static ValueComparer Instance => LazyInstance.Value;

            private ValueComparer()
            {
            }

            bool IEqualityComparer.Equals(object x, object y)
            {
                if (Equals(x, y))
                    return true;

                if (!(x is IEnumerable) || x is string || y is null)
                    return false;

                return CollectionComparison.Ordered.Compare((IEnumerable)x, (IEnumerable)y, EqualityComparer<object>.Default);
            }

            public int GetHashCode(object obj)
            {
                var hashCode = new HashCode();

                if (obj is IEnumerable enumerableObj)
                    foreach (var item in enumerableObj)
                        hashCode.Add(item);
                else
                    hashCode.Add(obj);

                return hashCode.ToHashCode();
            }
        }
    }
}
