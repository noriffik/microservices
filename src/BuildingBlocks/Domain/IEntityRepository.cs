﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Domain
{
    public interface IEntityRepository
    {
        Task Add<TEntity>(TEntity entity) where TEntity : class, IAggregateRoot;

        Task Add<TEntity, TId>(TEntity entity)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;

        Task Remove<TEntity>(TEntity entity) where TEntity : class, IAggregateRoot;

        Task Remove<TEntity, TId>(TEntity entity)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;
        
        //With cancellationToken

        Task<IEnumerable<TEntity>> Find<TEntity>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot;

        Task<IEnumerable<TEntity>> Find<TEntity, TId>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;

        Task<bool> Has<TEntity>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot;

        Task<bool> Has<TEntity, TId>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;

        Task<bool> Has<TEntity>(int id,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot;

        Task<bool> Has<TEntity, TId>(TId id,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;

        Task<bool> HasMany<TEntity>(IEnumerable<int> ids,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot;

        Task<bool> HasMany<TEntity, TId>(IEnumerable<TId> ids,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;

        Task<int> Count<TEntity>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot;

        Task<int> Count<TEntity, TId>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;

        Task<TEntity> SingleOrDefault<TEntity>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot;

        Task<TEntity> SingleOrDefault<TEntity, TId>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;

        Task<TEntity> SingleOrDefault<TEntity>(int id,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot;

        Task<TEntity> SingleOrDefault<TEntity, TId>(TId id,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;

        //With cancellationToken - Require

        Task HasRequired<TEntity>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot;

        Task HasRequired<TEntity, TId>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId;

        Task HasRequired<TEntity>(int id,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot;

        Task HasRequired<TEntity, TId>(TId id,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId;

        Task HasManyRequired<TEntity>(IEnumerable<int> ids,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot;

        Task HasManyRequired<TEntity, TId>(IEnumerable<TId> ids,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId;

        Task<TEntity> Require<TEntity>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot;

        Task<TEntity> Require<TEntity, TId>(Specification<TEntity> specification,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId;

        Task<TEntity> Require<TEntity>(int id,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot;

        Task<TEntity> Require<TEntity, TId>(TId id,
            CancellationToken cancellationToken)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId;

        //Without cancellationToken

        Task<IEnumerable<TEntity>> Find<TEntity>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot;

        Task<IEnumerable<TEntity>> Find<TEntity, TId>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;

        Task<bool> Has<TEntity>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot;

        Task<bool> Has<TEntity, TId>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;

        Task<bool> Has<TEntity>(int id)
            where TEntity : class, IAggregateRoot;

        Task<bool> Has<TEntity, TId>(TId id)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;

        Task<bool> HasMany<TEntity>(IEnumerable<int> ids)
            where TEntity : class, IAggregateRoot;

        Task<bool> HasMany<TEntity, TId>(IEnumerable<TId> ids)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;

        Task<int> Count<TEntity>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot;

        Task<int> Count<TEntity, TId>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;

        Task<TEntity> SingleOrDefault<TEntity>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot;

        Task<TEntity> SingleOrDefault<TEntity, TId>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;

        Task<TEntity> SingleOrDefault<TEntity>(int id)
            where TEntity : class, IAggregateRoot;

        Task<TEntity> SingleOrDefault<TEntity, TId>(TId id)
            where TEntity : class, IAggregateRoot<TId>
            where TId : EntityId;

        //Without cancellationToken - Require

        Task HasRequired<TEntity>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot;

        Task HasRequired<TEntity, TId>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId;

        Task HasRequired<TEntity>(int id)
            where TEntity : class, IAggregateRoot;

        Task HasRequired<TEntity, TId>(TId id)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId;

        Task HasManyRequired<TEntity>(IEnumerable<int> ids)
            where TEntity : class, IAggregateRoot;

        Task HasManyRequired<TEntity, TId>(IEnumerable<TId> ids)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId;

        Task<TEntity> Require<TEntity>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot;

        Task<TEntity> Require<TEntity, TId>(Specification<TEntity> specification)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId;

        Task<TEntity> Require<TEntity>(int id)
            where TEntity : class, IAggregateRoot;

        Task<TEntity> Require<TEntity, TId>(TId id)
            where TEntity : class, IAggregateRoot<TId> where TId : EntityId;
    }
}