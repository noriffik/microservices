﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Domain
{
    public abstract class BaseMapper<TSource, TDestination> : IMapper<TSource, TDestination>
        where TSource : class
        where TDestination : class
    {
        public abstract TDestination Map(TSource source);

        public virtual IEnumerable<TDestination> MapMany(IEnumerable<TSource> source)
            => source != null 
                ? source.ToList().Select(Map).Where(d => d != null)
                : throw new ArgumentNullException(nameof(source));
    }
}
