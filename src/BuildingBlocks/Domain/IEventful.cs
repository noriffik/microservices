﻿using System.Collections.Generic;

namespace NexCore.Domain
{
    public interface IEventful
    {
        IEnumerable<IEntityEvent> EntityEvents { get; }

        void AddEntityEvent(IEntityEvent entityEvent);
        
        void RemoveEntityEvent(IEntityEvent entityEvent);
        
        void ClearEntityEvents();
    }
}