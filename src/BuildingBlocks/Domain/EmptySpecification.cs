﻿using System;
using System.Linq.Expressions;

namespace NexCore.Domain
{
    public class EmptySpecification<T> : Specification<T>
    {
        public override Expression<Func<T, bool>> ToExpression()
        {
            return e => true;
        }

        public override string Description => "Empty specification";
    }
}
