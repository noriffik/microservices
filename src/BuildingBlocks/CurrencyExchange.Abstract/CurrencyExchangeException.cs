﻿using System;

namespace NexCore.CurrencyExchange.Abstract
{
    public class CurrencyExchangeException : Exception
    {
        public CurrencyExchangeException()
        {
        }

        public CurrencyExchangeException(string message) : base(message)
        {
        }

        public CurrencyExchangeException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}