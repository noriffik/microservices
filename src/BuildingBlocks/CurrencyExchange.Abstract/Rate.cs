﻿using NexCore.Domain;
using System;

namespace NexCore.CurrencyExchange.Abstract
{
    public class Rate : Entity, IAggregateRoot
    {
        public Currency From { get; private set; }

        public Currency To { get; private set; }

        public decimal Value { get; private set; }

        public DateTime Date { get; private set; }

        public Rate(Currency from, Currency to, decimal value, DateTime date)
        {
            if (value <= 0)
                throw new ArgumentOutOfRangeException(nameof(value));

            Value = value;
            From = from ?? throw new ArgumentNullException(nameof(from));
            To = to ?? throw new ArgumentNullException(nameof(to));
            Date = date.Date;
        }

        public Rate(int id, Currency from, Currency to, decimal value, DateTime date)
        : this(from, to, value, date)
        {
            Id = id;
        }
    }
}
