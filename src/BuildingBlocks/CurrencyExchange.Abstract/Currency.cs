﻿using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.CurrencyExchange.Abstract
{
    public class Currency : ValueObject
    {
        private const string Codes = "USD UAH EUR";

        public static Currency Usd => new Currency("USD");
        public static Currency Uah => new Currency("UAH");
        public static Currency Eur => new Currency("EUR");

        public string Code { get; }

        private Currency(string code)
        {
            Code = code ?? throw new ArgumentNullException(nameof(code));
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return Code;
        }

        public static Currency Parse(string code)
        {
            if (code == null)
                throw new ArgumentNullException(nameof(code));

            if (!TryParse(code, out var currency))
                throw new FormatException("Currency code is not recognized.");

            return currency;
        }

        public static bool TryParse(string code, out Currency currency)
        {
            var key = code?.ToUpperInvariant();

            currency = key != null && Codes.Split(' ').Contains(key)
                ? new Currency(key)
                : null;

            return currency != null;
        }

        public static implicit operator string(Currency currency)
        {
            return currency?.Code;
        }

        public static implicit operator Currency(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}