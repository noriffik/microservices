﻿using System;
using System.Threading.Tasks;

namespace NexCore.CurrencyExchange.Abstract
{
    public class StubbedCurrencyExchangeService : ICurrencyExchangeService
    {
        public Task<decimal> Convert(Currency @from, Currency to, decimal sum, DateTime onDate)
        {
            if (@from == null)
                throw new ArgumentNullException(nameof(@from));

            if (to == null)
                throw new ArgumentNullException(nameof(to));

            return Task.FromResult(28 * sum);
        }

        public Task<decimal> Convert(Currency @from, Currency to, decimal sum)
        {
            return Convert(@from, to, sum, DateTime.Today);
        }

        public Task<decimal> GetRate(Currency @from, Currency to, DateTime onDate)
        {
            return Task.FromResult(28M);
        }

        public Task<decimal> GetRate(Currency @from, Currency to)
        {
            return GetRate(from, to, DateTime.Today);
        }
    }
}