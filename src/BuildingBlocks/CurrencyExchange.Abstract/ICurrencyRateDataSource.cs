﻿using System;
using System.Threading.Tasks;

namespace NexCore.CurrencyExchange.Abstract
{
    public interface ICurrencyRateDataSource
    {
        Task<decimal?> TryGet(Currency from, Currency to, DateTime onDate);

        Task<decimal> Get(Currency from, Currency to, DateTime onDate);
    }
}
