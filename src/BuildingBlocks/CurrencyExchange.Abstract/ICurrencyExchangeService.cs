﻿using System;
using System.Threading.Tasks;

namespace NexCore.CurrencyExchange.Abstract
{
    public interface ICurrencyExchangeService
    {
        Task<decimal> Convert(Currency from, Currency to, decimal sum, DateTime onDate);

        Task<decimal> Convert(Currency from, Currency to, decimal sum);

        Task<decimal> GetRate(Currency from, Currency to, DateTime onDate);

        Task<decimal> GetRate(Currency from, Currency to);
    }
}