﻿using NexCore.DocumentProcessor;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace WordTemplateProcessor
{
    class Program
	{
		static void Main(string[] args)
		{
			var parseDictionary = new Dictionary<string, string>
			{
				{"REPLACE ME 1", "<REPLACED ME 1>"},
				{"REPLACE ME 2", "<REPLACED ME 2>"},
				{"Value A", "777777"},
				{"REPLACE ME 3", "<REPLACED ME 3>"},
				{"Value C", "999999"},
				{"Test1", "<Tested1>" },
				{"Test2", "<Tested2>" },
				{"Required", "<REPLACED Require>" }
			};

			var parseList = new Dictionary<string, IList<string>>
			{
				{"Test1", new List<string> {"888", "<Tested1>", "<Tested1_2>" }},
				{"Test2", new List<string> {"000", "<Tested1_1>", "<Tested2_2>" }},
				{"Required", new List<string> {"777", "<Tested1>", "<Tested2>" }}
			};

			var file = Path.Combine(new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName, "Template", "Template.docx");
			Sample(file, parseDictionary);

			var file2 = Path.Combine(new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName, "Template", "TemplateList.docx");
			SampleListValues(file2, parseList);

			var fileMerge = Path.Combine(new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName, "Template", "TemplateMerge.docx");
			SampleMerge(fileMerge, parseDictionary);

			var fileMergeList = Path.Combine(new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName, "Template", "TemplateMergeList.docx");
			SampleMergeList(fileMergeList, parseList);
		}

		private static void Sample(string filePath, IDictionary<string, string> parseDictionary)
		{
			var original = File.ReadAllBytes(filePath);
			using (var stream = new MemoryStream())
            {
	            stream.Write(original, 0, original.Length);

	            new WordDocumentFiller().Transform(stream, parseDictionary);

                stream.Seek(0, SeekOrigin.Begin);
                var data = stream.ToArray();

                File.WriteAllBytes(filePath, data);
			}
        }

		private static void SampleListValues(string filePath, IDictionary<string, IList<string>> parseDictionary)
		{
			var original = File.ReadAllBytes(filePath);
			using (var stream = new MemoryStream())
			{
				stream.Write(original, 0, original.Length);

				new WordDocumentFiller().TransformListValues(stream, parseDictionary);

				stream.Seek(0, SeekOrigin.Begin);
				var data = stream.ToArray();

				File.WriteAllBytes(filePath, data);
			}
		}

		private static void SampleMerge(string filePath, IDictionary<string, string> parseDictionary)
		{
			var original = File.ReadAllBytes(filePath);
			using (var stream = new MemoryStream())
			{
				stream.Write(original, 0, original.Length);

				new WordMergeFieldFiller().Transform(stream, parseDictionary);

				stream.Seek(0, SeekOrigin.Begin);
				var data = stream.ToArray();

				File.WriteAllBytes(filePath, data);
			}
		}

		private static void SampleMergeList(string filePath, IDictionary<string, IList<string>> parseDictionary)
		{
			var original = File.ReadAllBytes(filePath);
			using (var stream = new MemoryStream())
			{
				stream.Write(original, 0, original.Length);

				new WordMergeFieldFiller().TransformListValues(stream, parseDictionary);

				stream.Seek(0, SeekOrigin.Begin);
				var data = stream.ToArray();

				File.WriteAllBytes(filePath, data);
			}
		}
	}
}
