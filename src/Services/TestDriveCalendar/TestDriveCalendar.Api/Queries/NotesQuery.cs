﻿using FluentValidation;
using FluentValidation.Results;
using NexCore.TestDriveCalendar.Api.Domain.Notes;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Queries
{
    public class NotesQuery : INotesQuery
    {
        private readonly INotesRepository _notesRepository;
        private readonly ITestDriveRepository _testDriveRepository;
        
        public NotesQuery(INotesRepository notesRepository, ITestDriveRepository testDriveRepository)
        {
            _notesRepository = notesRepository ?? throw new System.ArgumentNullException(nameof(notesRepository));
            _testDriveRepository = testDriveRepository ?? throw new System.ArgumentNullException(nameof(testDriveRepository));
        }

        public async Task<IEnumerable<dynamic>> GetAllNotesByTestDriveId(int testDriveId)
        {
            await ThrowIfTestDriveDoesNotFound(testDriveId);

            return await _notesRepository.FindAllByTestDriveIdAsync(testDriveId);
        }

        private async Task ThrowIfTestDriveDoesNotFound(int testDriveId)
        {
            var exists = await _testDriveRepository.Has(testDriveId);
            if (exists) return;
            
            throw new ValidationException("Failed to handle command.",
                new[] { new ValidationFailure(nameof(testDriveId), "Test-drive does not exists.", testDriveId) });
        }
    }
}
