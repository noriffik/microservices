﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Queries
{
    public interface INotesQuery
    {
        Task<IEnumerable<dynamic>> GetAllNotesByTestDriveId(int testDriveId);
    }
}
