﻿using NexCore.TestDriveCalendar.Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Queries
{
    public interface ITestDriveQueries
    {
        Task<IEnumerable<TestDriveListItem>> AllAsync();
        
        Task<IEnumerable<TestDriveListItem>> ByDateAndDealerIdAsync(TestDriveByDealerDateItem query);
    }
}