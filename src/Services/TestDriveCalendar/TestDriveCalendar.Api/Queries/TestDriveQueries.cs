﻿using Dapper;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Queries
{
    public class TestDriveQueries : ITestDriveQueries
    {
        private const string AllQueryString = 
            "SELECT " +
                "TestDriveId, " +
                "ClientId, " +
                "VehicleId, " +
                "Since, " +
                "Till, " +
                "SalesManagerId, " +
                "StatusId " +
            "FROM testdrive.TestDrive " +
            "ORDER BY TestDriveId;";

        private const string ByDateAndDealerIdQueryString = 
            "SELECT " +
            "testdrive.TestDrive.TestDriveId, " +
            "testdrive.TestDrive.ClientId, " +
            "testdrive.TestDrive.VehicleId, " +
            "testdrive.TestDrive.Since, " +
            "testdrive.TestDrive.Till, " +
            "testdrive.TestDrive.SalesManagerId, " +
            "testdrive.TestDrive.StatusId " +
            "FROM testdrive.TestDrive " +
               "INNER JOIN testdrive.Vehicle ON testdrive.TestDrive.VehicleId = testdrive.Vehicle.VehicleId " +
               "INNER JOIN testdrive.Fleet ON testdrive.Vehicle.FleetId = testdrive.Fleet.FleetId " +
            "WHERE testdrive.Fleet.DealerId = @DealerId AND CAST(Since as DATE) = @Date " +
            "ORDER BY TestDriveId;";

        private readonly IDbConnection _connection;

        public TestDriveQueries(IDbConnection connection)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
        }
        
        public async Task<IEnumerable<TestDriveListItem>> ByDateAndDealerIdAsync(TestDriveByDealerDateItem query)
        {
            var result = await _connection.QueryAsync(ByDateAndDealerIdQueryString, new {query.DealerId, query.Date.Date});

            return Map(result);
        }
        
        public async Task<IEnumerable<TestDriveListItem>> AllAsync()
        {
            var result = await _connection.QueryAsync(AllQueryString);

            return Map(result);
        }

        private static IEnumerable<TestDriveListItem> Map(IEnumerable<dynamic> result)
        {
            return result.Select(
                r => new TestDriveListItem
                {
                    Id = r.TestDriveId, 
                    ClientId = r.ClientId,
                    VehicleId = r.VehicleId,
                    Since = r.Since,
                    Till = r.Till,
                    SalesManagerId = r.SalesManagerId,
                    StatusId = r.StatusId,
                    StatusName = Status.Get(r.StatusId).Name
                });
        }
    }
}
