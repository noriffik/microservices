﻿using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Queries
{
    public interface IFleetQuery
    {
        Task<IEnumerable<VehicleListItem>> ByDealerIdAsync(DealerCode dealerId);
        Task<VehicleListItem> ByVehicleIdAsync(int vehicleId);
    }
}
