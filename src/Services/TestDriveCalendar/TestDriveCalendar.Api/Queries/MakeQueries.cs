﻿using Dapper;
using NexCore.TestDriveCalendar.Api.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Queries
{
    public class MakeQueries : IMakeQueries
    {
        const string Query = "SELECT MakeId, Name FROM testdrive.Make ORDER BY Name;";

        private readonly IDbConnection _connection;

        public MakeQueries(IDbConnection connection)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
        }
        
        public async Task<IEnumerable<MakeListItem>> AllAsync()
        {
            var result = await _connection.QueryAsync(Query);

            return Map(result);
        }

        private IEnumerable<MakeListItem> Map(IEnumerable<dynamic> result)
        {
            return result.Select(
                r => new MakeListItem { Id = r.MakeId, Name = r.Name });
        }
    }
}
