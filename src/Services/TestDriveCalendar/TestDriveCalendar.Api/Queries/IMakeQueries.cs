﻿using NexCore.TestDriveCalendar.Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Queries
{
    public interface IMakeQueries
    {
        Task<IEnumerable<MakeListItem>> AllAsync();
    }
}
