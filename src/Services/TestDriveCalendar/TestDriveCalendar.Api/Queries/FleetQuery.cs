﻿using System;
using Dapper;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Queries
{
    public class FleetQuery : IFleetQuery
    {
        private const string Sql = "SELECT [DealerId],[FleetId],[VehicleId],[Vin],[ModelId],[Equipment],[Engine],[GearBox],[Options],[Comments],[Availability],[MakeName],[ModelCode],[ModelName] " +
                                   "FROM [testdrive].[VI_Fleet_Vehicles]";

        private const string WithDealerId = "WHERE [DealerId] = @DealerId";

        private const string WithVehicleId= "WHERE [VehicleId] = @VehicleId";

        private readonly IDbConnection _connection;

        public FleetQuery(IDbConnection connection)
        {
            _connection = connection ?? throw new System.ArgumentNullException(nameof(connection));
        }

        public async Task<IEnumerable<VehicleListItem>> ByDealerIdAsync(DealerCode dealerId)
        {
            if (dealerId == null)
                throw new ArgumentNullException(nameof(dealerId));

            return await _connection.QueryAsync<VehicleListItem>(Sql + WithDealerId, new {DealerId = dealerId.Value});
        }

        public async Task<VehicleListItem> ByVehicleIdAsync(int vehicleId)
        {   
            var result = await _connection.QueryAsync<VehicleListItem>(Sql + WithVehicleId, new {VehicleId = vehicleId});

            return result.SingleOrDefault();
        }
    }
}
