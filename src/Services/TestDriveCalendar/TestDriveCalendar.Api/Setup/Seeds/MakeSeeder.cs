﻿using NexCore.TestDriveCalendar.Api.Domain;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Setup.Seeds
{
    public class MakeSeeder : AggregateRootSeeder<Make>
    {
        public const int SkodaId = 1;
        public const string SkodaName = "Skoda";

        private static readonly Dictionary<string, string> models = new Dictionary<string, string>()
        {
            { "NF", "Citigo"},
            { "NJ", "Fabia"},
            { "NH", "Rapid"},
            { "NW", "Scala"},
            { "NU", "Karoq"},
            { "3V", "Superb"},
            { "NS", "Kodiaq"},
            { "5E", "Octavia A7"},
            { "NE", "E-Citigo"},
            { "NX", "Octavia A8"},
        };

        public MakeSeeder(IRepository<Make> repository)
            :base(repository, CreateMakes(models, SkodaName))
        {
        }

        private static IEnumerable<Make> CreateMakes(Dictionary<string, string> models,params string[] names)
            => names.Select(
                name => AppendModels(models, new Make(SkodaId,name)));

        private static Make AppendModels(Dictionary<string, string> models, Make make)
        {
            foreach(var model in models)
            {
                make.AddModel(new Model(model.Key, model.Value));
            }

            return make;
        }

        protected override async Task<bool> IsSeeded()
        {
            var makes = DataSource.Load();
            
            foreach (var make in makes)
            {
                var isSeeded = await Repository.Has(make.Id);
                if (!isSeeded)
                    return false;
            }

            return true;
        }
    }
}
