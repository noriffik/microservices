﻿using NexCore.Domain;
using NexCore.Infrastructure.Seeds;
using NexCore.TestDriveCalendar.Api.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Setup.Seeds
{
    public class AggregateRootSeeder<TAggregateRoot> : ISeeder<TAggregateRoot> where TAggregateRoot : IAggregateRoot
    {
        private const int IsSeededIdCriteria = 1;

        protected readonly IRepository<TAggregateRoot> Repository;
        protected readonly IAggregateRootDataSource<TAggregateRoot> DataSource;

        public AggregateRootSeeder(IRepository<TAggregateRoot> repository, IAggregateRootDataSource<TAggregateRoot> dataSource)
        {
            Repository = repository ?? throw new ArgumentNullException(nameof(repository));
            DataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
        }

        public AggregateRootSeeder(IRepository<TAggregateRoot> repository, IEnumerable<TAggregateRoot> roots)
            : this(repository, new CollectionAggregateRootDataSource<TAggregateRoot>(roots))
        {
        }

        public async Task Seed()
        {
            var roots = DataSource.Load();

            if (!roots.Any() || await IsSeeded())
                return;

            roots.ToList().ForEach(r => Repository.Add(r));

            await Repository.UnitOfWork.Commit(CancellationToken.None);
        }

        protected virtual async Task<bool> IsSeeded()
        {
            return await Repository.Has(IsSeededIdCriteria);
        }
    }

    public interface IAggregateRootDataSource<out TAggregateRoot> where TAggregateRoot : IAggregateRoot
    {
        TAggregateRoot[] Load();
    }

    class CollectionAggregateRootDataSource<TAggregateRoot> : IAggregateRootDataSource<TAggregateRoot>
        where TAggregateRoot : IAggregateRoot
    {
        private readonly IEnumerable<TAggregateRoot> _roots;

        public CollectionAggregateRootDataSource(IEnumerable<TAggregateRoot> roots)
        {
            _roots = roots ?? throw new ArgumentNullException(nameof(roots));
        }

        public TAggregateRoot[] Load()
        {
            return _roots.ToArray();
        }
    }
}