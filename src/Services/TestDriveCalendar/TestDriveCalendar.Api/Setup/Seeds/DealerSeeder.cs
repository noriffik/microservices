﻿using NexCore.Infrastructure.Seeds;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Setup.Seeds
{
    public class DealerSeeder : ISeeder<Dealer>
    {
        private readonly IDealerRepository _repository;
        public static readonly DealerCode Code = DealerCode.Parse("EKK38001");

        public const int TypicalTestDriveDuration = 60;

        public DealerSeeder(IDealerRepository repository)
        {
            _repository = repository;
        }

        public async Task Seed()
        {
            var isSeeded = await _repository.Has(Code);
            if (isSeeded)
                return;

            var dealer = new Dealer(Code, Schedule.Typical, TypicalTestDriveDuration);

            _repository.Add(dealer);

            await _repository.UnitOfWork.Commit();
        }
    }
}
