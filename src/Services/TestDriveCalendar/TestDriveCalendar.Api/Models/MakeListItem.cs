﻿namespace NexCore.TestDriveCalendar.Api.Models
{
    public class MakeListItem
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}