﻿using System.Runtime.Serialization;

namespace NexCore.TestDriveCalendar.Api.Models
{
    [DataContract]
    public class VehicleListItem
    {
        [DataMember]
        public string DealerId { get; set; }

        [DataMember]
        public int FleetId { get; set; }

        [DataMember]
        public int VehicleId { get; set; }

        [DataMember]
        public string Vin { get; set; }

        [DataMember]
        public int ModelId { get; set; }

        [DataMember]
        public string Equipment { get; set; }

        [DataMember]
        public string Engine { get; set; }

        [DataMember]
        public string GearBox { get; set; }

        [DataMember]
        public string Options { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public bool Availability { get; set; }

        [DataMember]
        public string MakeName { get; set; }

        [DataMember]
        public string ModelCode { get; set; }

        [DataMember]
        public string ModelName { get; set; }
    }
}
