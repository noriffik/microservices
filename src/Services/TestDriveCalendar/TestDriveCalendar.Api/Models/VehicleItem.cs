﻿namespace NexCore.TestDriveCalendar.Api.Models
{
    public class VehicleItem
    {
        public int Id { get; set; }
        public int ModelId { get; set; }
        public string Vin { get; set; }
        public string DealerId { get; set; }
        public string Equipment { get; set; }
        public string Type { get; set; }
        public string Engine { get; set; }
        public string GearBox { get; set; }
        public string Options { get; set; }
        public string Comments { get; set; }
    }
}
