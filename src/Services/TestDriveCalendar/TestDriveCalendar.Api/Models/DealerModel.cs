﻿using NexCore.TestDriveCalendar.Api.Domain.Dealers;

namespace NexCore.TestDriveCalendar.Api.Models
{
    public class DealerModel
    {
        public string Id { get; set; }

        public Schedule Schedule { get; set; }

        public int? TestDriveDuration { get; set; }
    }
}
