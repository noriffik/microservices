﻿using System;

namespace NexCore.TestDriveCalendar.Api.Models
{
    public class TestDriveListItem
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int VehicleId { get; set; }
        public DateTime Since { get; set; }
        public DateTime Till { get; set; }
        public int SalesManagerId { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
}
