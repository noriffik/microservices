﻿using System;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;

namespace NexCore.TestDriveCalendar.Api.Models
{
    public class VehicleMapper : IVehicleMapper
    {
        public Vehicle Map(VehicleItem item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            return new Vehicle(item.Id, Vin.Parse(item.Vin), item.ModelId)
            {
                Equipment = item.Equipment,
                Type = item.Type,
                Engine = item.Engine,
                GearBox = item.GearBox,
                Options = item.Options,
                Comments = item.Comments
            };
        }
    }
}
