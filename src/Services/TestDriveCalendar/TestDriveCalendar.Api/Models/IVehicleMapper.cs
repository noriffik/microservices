﻿using NexCore.TestDriveCalendar.Api.Domain.Vehicles;

namespace NexCore.TestDriveCalendar.Api.Models
{
    public interface IVehicleMapper
    {
        Vehicle Map(VehicleItem item);
    }
}
