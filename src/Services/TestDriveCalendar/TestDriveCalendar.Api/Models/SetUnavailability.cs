﻿using System.Runtime.Serialization;

namespace NexCore.TestDriveCalendar.Api.Models
{
    [DataContract]
    public class SetUnavailability
    {
        [DataMember]
        public int VehicleId { get; set; }

        [DataMember]
        public bool Availability { get; set; }
    }
}
