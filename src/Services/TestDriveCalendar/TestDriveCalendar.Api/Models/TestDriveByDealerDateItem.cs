﻿using System;

namespace NexCore.TestDriveCalendar.Api.Models
{
    public class TestDriveByDealerDateItem
    {
        public string DealerId { get; set; }
        public DateTime Date { get; set; }
    }
}
