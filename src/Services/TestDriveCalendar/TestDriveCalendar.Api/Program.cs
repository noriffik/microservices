﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using NexCore.TestDriveCalendar.Api.Extensions;
using NexCore.TestDriveCalendar.Api.Infrastructure;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api
{
    public class Program
    {
	    public static readonly string Namespace = typeof(Program).Namespace;
		public static readonly string AppName = Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

		public static async Task Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            await host.MigrateDbContext<TestDriveContext>();            
            
            await host.RunAsync();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseHealthChecks("/hc");
    }
}
