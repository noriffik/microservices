﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.TestDriveCalendar.Api.Domain.Notes;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.EntityTypes
{
    public class NoteEntityTypeConfiguration : IEntityTypeConfiguration<Note>
    {
        public void Configure(EntityTypeBuilder<Note> builder)
        {
            builder.ToTable(nameof(Note), TestDriveContext.Schema);

            builder.HasKey(m => m.Id);
            builder.Property(m => m.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Note), TestDriveContext.Schema)
                .HasColumnName(nameof(Note) + "Id");

            builder.HasOne<TestDrive>().WithMany().HasForeignKey(n => n.TestDriveId);

            builder.Property(m => m.ManagerId).HasColumnName(nameof(Note.ManagerId)).IsRequired();
            builder.Property(m => m.Content).HasColumnName(nameof(Note.Content)).IsRequired();
            builder.Property(m => m.CreatedDate).HasColumnName(nameof(Note.CreatedDate)).IsRequired();
            builder.Property(m => m.UpdatedDate).HasColumnName(nameof(Note.UpdatedDate)).IsRequired();
        }
    }
}
