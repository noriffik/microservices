﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.EntityTypes
{
    public class StatusChangeEntityTypeConfiguration : IEntityTypeConfiguration<StatusChange>
    {
        public void Configure(EntityTypeBuilder<StatusChange> builder)
        {
            builder.ToTable(nameof(StatusChange), TestDriveContext.Schema);

            builder.HasKey(m => m.Id);

            builder.Property(m => m.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(StatusChange), TestDriveContext.Schema)
                .HasColumnName(nameof(StatusChange) + "Id");
            
            builder.HasIndex("TestDriveId", "Version").IsUnique();

            builder.Ignore(td => td.Status);

            builder.Property("_statusId")
                .HasColumnName("StatusId")
                .IsRequired();
            
            builder.Property(m => m.AuthorId)
                .HasColumnName(nameof(StatusChange.AuthorId))
                .IsRequired();

            builder.Property(m => m.Updated)
                .HasColumnName(nameof(StatusChange.Updated));

            builder.Property(m => m.Version)
                .HasColumnName(nameof(StatusChange.Version))
                .IsRequired();
        }
    }
}
