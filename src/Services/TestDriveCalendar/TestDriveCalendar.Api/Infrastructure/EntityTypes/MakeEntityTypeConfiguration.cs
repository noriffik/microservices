﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.EntityTypes
{
    public class MakeEntityTypeConfiguration : IEntityTypeConfiguration<Make>
    {
        public void Configure(EntityTypeBuilder<Make> builder)
        {
            builder.ToTable(nameof(Make), TestDriveContext.Schema);

            builder.HasKey(m => m.Id);
            builder.Property(m => m.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Make), TestDriveContext.Schema)
                .HasColumnName(nameof(Make) + "Id");
            
            builder.Property(m => m.Name).HasColumnName(nameof(Make.Name));

            builder.Ignore(m => m.Models);
            builder.HasMany<Model>("_models")
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);
            
            var modelNavigation = builder.Metadata.FindNavigation("_models");
            modelNavigation.IsEagerLoaded = true;
        }
    }
}
