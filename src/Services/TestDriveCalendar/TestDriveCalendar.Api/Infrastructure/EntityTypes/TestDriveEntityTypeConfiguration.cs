﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.EntityTypes
{
    public class TestDriveEntityTypeConfiguration : IEntityTypeConfiguration<TestDrive>
    {
        public void Configure(EntityTypeBuilder<TestDrive> builder)
        {
            builder.ToTable(nameof(TestDrive), TestDriveContext.Schema);

            builder.HasKey(m => m.Id);

            builder.Property(p => p.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(TestDrive), TestDriveContext.Schema)
                .HasColumnName(nameof(TestDrive) + "Id");

            builder.Property(p => p.ClientId).HasColumnName(nameof(TestDrive.ClientId));

            builder.Ignore(td => td.StatusChanges);

            builder.HasMany<StatusChange>("_statusChanges")
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);

            builder.Metadata.FindNavigation("_statusChanges").IsEagerLoaded = true;

            builder.Property(p => p.VehicleId).HasColumnName(nameof(TestDrive.VehicleId));
            builder.HasOne<Vehicle>().WithMany().HasForeignKey(td => td.VehicleId);

            builder.Property(p => p.Since).HasColumnName(nameof(TestDrive.Since));
            builder.Property(p => p.Till).HasColumnName(nameof(TestDrive.Till));
            builder.Property(p => p.SalesManagerId).HasColumnName(nameof(TestDrive.SalesManagerId));
            
            builder.Ignore(td => td.Status);

            builder.Property("_statusId").HasColumnName("StatusId");
        }
    }
}
