﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.EntityTypes
{
    public class DealerEntityTypeConfiguration : IEntityTypeConfiguration<Dealer>
    {
        public void Configure(EntityTypeBuilder<Dealer> builder)
        {
            builder.ToTable(nameof(Dealer), TestDriveContext.Schema);

            builder.Property(x => x.Id)
                .HasConversion(x => x.Value, x => x)
                .HasColumnName(typeof(Dealer).Name + "Id")
                .IsRequired();

            builder.Property(d => d.Schedule)
                .HasConversion(v => JsonConvert.SerializeObject(v), v => JsonConvert.DeserializeObject<Schedule>(v))
                .HasColumnName(nameof(Dealer.Schedule));

            builder.Property(d => d.TestDriveDuration)
                .HasColumnName(nameof(Dealer.TestDriveDuration));
        }
    }
}
