﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.EntityTypes
{
    public class VehicleEntityTypeConfiguration : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            builder.ToTable(nameof(Vehicle), TestDriveContext.Schema);

            builder.HasKey(m => m.Id);

            builder.Property(m => m.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Vehicle), TestDriveContext.Schema)
                .HasColumnName(nameof(Vehicle) + "Id");

            builder.Property(p => p.ModelId)
                .HasColumnName(nameof(Vehicle.ModelId))
                .IsRequired();

            builder.HasOne<Model>().WithMany()
                .HasForeignKey(v => v.ModelId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.OwnsOne(p => p.Vin, v => 
            {
                v.Property(p => p.Number).HasColumnName(nameof(Vin));
            });

            builder.Property(p => p.Availability).HasColumnName(nameof(Vehicle.Availability));
            builder.Property(p => p.Equipment).HasColumnName(nameof(Vehicle.Equipment));
            builder.Property(p => p.Type).HasColumnName(nameof(Vehicle.Type));
            builder.Property(p => p.Engine).HasColumnName(nameof(Vehicle.Engine));
            builder.Property(p => p.GearBox).HasColumnName(nameof(Vehicle.GearBox));
            builder.Property(p => p.Options).HasColumnName(nameof(Vehicle.Options));
            builder.Property(p => p.Comments).HasColumnName(nameof(Vehicle.Comments));
        }
    }
}
