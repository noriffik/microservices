﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.EntityTypes
{
    public class FleetEntityTypeConfiguration : IEntityTypeConfiguration<Fleet>
    {
        public void Configure(EntityTypeBuilder<Fleet> builder)
        {
            builder.ToTable(nameof(Fleet), TestDriveContext.Schema);

            builder.HasKey(m => m.Id);
            builder.Property(m => m.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Fleet), TestDriveContext.Schema)
                .HasColumnName(nameof(Fleet) + "Id");

            builder.Property(c => c.DealerId)
                .HasConversion(k => k.Value, k => k);

            builder.Ignore(m => m.Vehicles);

            builder.HasMany<Vehicle>("_vehicles").WithOne().OnDelete(DeleteBehavior.Cascade);
            builder.Metadata.FindNavigation("_vehicles").IsEagerLoaded = true;
        }
    }
}
