﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.EntityTypes
{
    public class ModelEntityTypeConfiguration : IEntityTypeConfiguration<Model>
    {
        public void Configure(EntityTypeBuilder<Model> builder)
        {
            builder.ToTable(nameof(Model), TestDriveContext.Schema);

            builder.HasKey(m => m.Id);
            builder.Property(m => m.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Model), TestDriveContext.Schema)
                .HasColumnName(nameof(Model) + "Id");

            builder.Property(m => m.Code).HasColumnName(nameof(Model.Code)).IsRequired();
            builder.Property(m => m.Name).HasColumnName(nameof(Model.Name)).IsRequired();
        }
    }
}
