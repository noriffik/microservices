﻿using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Notes;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Infrastructure.EntityTypes;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Infrastructure
{
    public class TestDriveContext : DbContext, IUnitOfWork
    {
        public const string Schema = "testdrive";

        public DbSet<Make> Makes { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Fleet> Fleets { get; set; }
        public DbSet<Vehicle> Vehicle { get; set; }
        public DbSet<Dealer> Dealers { get; set; }
        public DbSet<TestDrive> TestDrives { get; set; }
        public DbSet<Note> Notes { get; set; }

        public TestDriveContext(DbContextOptions<TestDriveContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new MakeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ModelEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new VehicleEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new FleetEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DealerEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TestDriveEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new StatusChangeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new NoteEntityTypeConfiguration());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.EnableSensitiveDataLogging();
        }

        public async Task Commit(CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                await SaveChangesAsync(cancellationToken);
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw new WorkConcurrencyException(e);
            }
            catch (DbUpdateException e)
            {
                throw new WorkFailedException(e);
            }
        }

        public IEntityRepository EntityRepository => throw new NotSupportedException();
    }
}
