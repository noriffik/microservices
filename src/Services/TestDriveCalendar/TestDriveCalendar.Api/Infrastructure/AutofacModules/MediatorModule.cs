﻿using System.Reflection;
using Autofac;
using MediatR;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.AutofacModules
{
	public class MediatorModule : Autofac.Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly).AsImplementedInterfaces();

		}
	}
}