﻿using Microsoft.EntityFrameworkCore;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.Repositories
{
    public class TestDriveRepository  : EntityFrameworkRepository<TestDriveContext, TestDrive>, ITestDriveRepository
    {
        public TestDriveRepository(TestDriveContext context) : base(context)
        {
        }
        
        public async Task<bool> IsVehicleReservedAsync(int vehicleId, DateTime since, DateTime till)
        {
            return await Context.TestDrives.AnyAsync(
                td => IsVehicleReservedByTestDrive(td, vehicleId, since, till));
        }
        
        public async Task<bool> CanRescheduleAsync(TestDrive testDrive, DateTime since, DateTime till)
        {
            return await Context.TestDrives
                .Where(td => td.VehicleId == testDrive.VehicleId)
                .Where(td => td.Id != testDrive.Id)
                .AllAsync(td => since < td.Since && till <= td.Since 
                                || since >= td.Till && till > td.Till);
        }

        private static bool IsVehicleReservedByTestDrive(
            TestDrive testDrive, int vehicleId, DateTime since, DateTime till)
            => testDrive.VehicleId == vehicleId && (
                   testDrive.Since >= since && testDrive.Since < till ||
                   testDrive.Till >= since && testDrive.Till < till ||
                   testDrive.Since < since && testDrive.Till > till ||
                   testDrive.Since >= since && testDrive.Till <= till);
    }
}
