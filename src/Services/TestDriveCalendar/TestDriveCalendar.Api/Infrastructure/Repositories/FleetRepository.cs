﻿using Microsoft.EntityFrameworkCore;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.Repositories
{
    public class FleetRepository : EntityFrameworkRepository<TestDriveContext, Fleet>, IFleetRepository
    {
        public FleetRepository(TestDriveContext context) : base(context)
        {
        }
        
        public async Task<Fleet> FindByDealerIdAsync(DealerCode dealerId)
        {
            return await Context.Set<Fleet>()
                .Where(f => Convert.ToString(f.DealerId) == dealerId.Value)
                .SingleOrDefaultAsync();
        }

        public async Task<Fleet> FindByVehicleIdAsync(int vehicleId)
        {
            var fleetId = await Context.Set<Vehicle>()
                .Where(v => v.Id == vehicleId)
                .Select(v => EF.Property<int>(v, "FleetId"))
                .SingleOrDefaultAsync();
            
            return fleetId == default ? null : await Find(fleetId);
        }

        public async Task<Vehicle> FindVehicleByIdAsync(int vehicleId)
        {
            return await Context.Set<Vehicle>()
                .Where(v => v.Id == vehicleId)
                .SingleOrDefaultAsync();
        }
    }
}
