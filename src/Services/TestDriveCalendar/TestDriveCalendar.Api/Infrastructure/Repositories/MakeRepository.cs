﻿using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.Repositories
{
    public class MakeRepository : EntityFrameworkRepository<TestDriveContext, Make>, IMakeRepository
    {
        public MakeRepository(TestDriveContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Make>> List()
        {
            return await Context.Makes
                .OrderBy(m => m.Name)
                .ToAsyncEnumerable()
                .ToList();
        }
    }
}
