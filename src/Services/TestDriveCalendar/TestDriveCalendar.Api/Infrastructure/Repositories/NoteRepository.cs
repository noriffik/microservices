﻿using Microsoft.EntityFrameworkCore;
using NexCore.TestDriveCalendar.Api.Domain.Notes;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.Repositories
{
    public class NoteRepository : EntityFrameworkRepository<TestDriveContext, Note>, INotesRepository
    {
        public NoteRepository(TestDriveContext context) : base(context)
        {
        }
        
        public async Task<IEnumerable<Note>> FindAllByTestDriveIdAsync(int testDriveId)
        {
            return await Context.Notes.Where(n => n.TestDriveId == testDriveId).ToListAsync();
        }
    }
}
