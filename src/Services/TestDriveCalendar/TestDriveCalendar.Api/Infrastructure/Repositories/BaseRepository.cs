﻿using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.Repositories
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity>
        where TEntity : Entity, IAggregateRoot
    {
        public abstract IUnitOfWork UnitOfWork { get; }

        public abstract void Add(TEntity entity);

        public abstract void Remove(TEntity entity);

        public async Task<TEntity> Find(int id)
        {
            var query = await Find(new IdSpecification<TEntity>(id), CancellationToken.None);

            return query.SingleOrDefault();
        }

        public Task<IEnumerable<TEntity>> Find(Specification<TEntity> specification, CancellationToken cancellationToken)
        {
            if (specification == null)
                throw new ArgumentNullException(nameof(specification));

            return ToListAsync(GetSatisfying(specification), cancellationToken);
        }

        public Task<TEntity> SingleOrDefault(Specification<TEntity> specification, CancellationToken cancellationToken)
        {
            if (specification == null) 
                throw new ArgumentNullException(nameof(specification));

            return SingleOrDefaultAsync(GetSatisfying(specification), cancellationToken);
        }

        public Task<bool> Has(int id)
        {
            return Has(new IdSpecification<TEntity>(id), CancellationToken.None);
        }
        
        public Task<bool> Has(Specification<TEntity> specification, CancellationToken cancellationToken)
        {
            if (specification == null)
                throw new ArgumentNullException(nameof(specification));

            return HasAsync(GetSatisfying(specification), cancellationToken);
        }

        public async Task<bool> Has(IEnumerable<int> ids)
        {
            var specifications = ids.Distinct()
                .Select(id => new IdSpecification<TEntity>(id))
                .ToList();

            var found = await Count(new AnySpecification<TEntity>(specifications), CancellationToken.None);

            return found == specifications.Count;
        }

        public Task<int> Count(Specification<TEntity> specification, CancellationToken cancellationToken)
        {
            if (specification == null)
                throw new ArgumentNullException(nameof(specification));

            return CountAsync(GetSatisfying(specification), cancellationToken);
        }

        protected abstract Task<IEnumerable<TEntity>> ToListAsync(IQueryable<TEntity> query, CancellationToken cancellationToken);

        protected abstract Task<TEntity> SingleOrDefaultAsync(IQueryable<TEntity> query, CancellationToken cancellationToken);

        protected abstract Task<bool> HasAsync(IQueryable<TEntity> query, CancellationToken cancellationToken);

        protected abstract Task<int> CountAsync(IQueryable<TEntity> query, CancellationToken cancellationToken);

        protected abstract IQueryable<TEntity> GetDataSource();

        protected IQueryable<TEntity> GetSatisfying(Specification<TEntity> specification)
        {
            return GetDataSource().Apply(specification);
        }
    }
}
