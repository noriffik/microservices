﻿using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.Repositories
{
    public abstract class EntityFrameworkRepository<TContext, TEntity> : BaseRepository<TEntity>
        where TEntity : Entity, IAggregateRoot where TContext : DbContext, IUnitOfWork
    {
        protected readonly TContext Context;

        protected EntityFrameworkRepository(TContext context)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public override IUnitOfWork UnitOfWork => Context;

        public override void Add(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            Context.Set<TEntity>().Add(entity);
        }

        public override void Remove(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
        }

        protected override async Task<IEnumerable<TEntity>> ToListAsync(IQueryable<TEntity> query, CancellationToken cancellationToken)
        {
            return await query.ToListAsync(cancellationToken);
        }

        protected override Task<TEntity> SingleOrDefaultAsync(IQueryable<TEntity> query, CancellationToken cancellationToken)
        {
            return query.SingleOrDefaultAsync(cancellationToken);
        }

        protected override Task<bool> HasAsync(IQueryable<TEntity> query, CancellationToken cancellationToken)
        {
            return query.AnyAsync(cancellationToken);
        }

        protected override Task<int> CountAsync(IQueryable<TEntity> query, CancellationToken cancellationToken)
        {
            return query.CountAsync(cancellationToken);
        }

        protected override IQueryable<TEntity> GetDataSource()
        {
            return Context.Set<TEntity>();
        }
    }
}