﻿using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Infrastructure.Repositories
{
    public class DealerRepository : IDealerRepository
    {
        protected readonly TestDriveContext Context;

        public DealerRepository(TestDriveContext context)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public void Add(Dealer entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            Context.Set<Dealer>().Add(entity);
        }

        public Task<Dealer> Find(DealerCode id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            return Context.Dealers
                .Where(d => Convert.ToString(d.Id) == id.Value)
                .SingleOrDefaultAsync();
        }

        public Task<bool> Has(DealerCode id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            return Context.Dealers
                .Where(d => Convert.ToString(d.Id) == id.Value)
                .AnyAsync();
        }

        public async Task<Dealer> FindByFleetIdAsync(int fleetId)
        {
            var dealerId = await Context.Fleets
                .Where(f => f.Id == fleetId)
                .Select(f => f.DealerId)
                .SingleOrDefaultAsync();

            return dealerId == null ? null : await Find(dealerId);
        }

        public IUnitOfWork UnitOfWork => Context;
    }
}
