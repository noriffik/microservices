﻿using NexCore.Infrastructure.Seeds;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;

namespace NexCore.TestDriveCalendar.Api.Infrastructure
{
    public class TestDriveContextSeeder : CompositeSeeder, IDbContextSeeder<TestDriveContext>
    {
        public TestDriveContextSeeder(ISeeder<Dealer> dealerSeeder, ISeeder<Domain.Vehicles.Make> makesSeeder) : base(dealerSeeder, makesSeeder)
        {
        }
    }
}
