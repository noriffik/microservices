﻿using NexCore.Infrastructure;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.TestDriveCalendar.Api.Infrastructure
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Used by EF tool")]
    public class TestDriveContextDesignTimeFactory : DbContextBaseDesignFactory<TestDriveContext>
    {
        public TestDriveContextDesignTimeFactory() : base("ConnectionString")
        {
        }
    }
}
