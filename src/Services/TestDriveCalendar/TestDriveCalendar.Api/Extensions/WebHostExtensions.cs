﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Infrastructure.Seeds;
using Polly;

namespace NexCore.TestDriveCalendar.Api.Extensions
{
    public static class WebHostExtensions
    {
        public static async Task<IWebHost> MigrateDbContext<TContext>(this IWebHost webHost) where TContext : DbContext
        {
            using (var scope = webHost.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<TContext>();
                var seeder = scope.ServiceProvider.GetService<IDbContextSeeder<TContext>>();
                
                await Policy.Handle<SqlException>()
                    .WaitAndRetry(new[]
                    {
                        TimeSpan.FromSeconds(3),
                        TimeSpan.FromSeconds(5),
                        TimeSpan.FromSeconds(8),
                    })
                    .Execute(async () =>
                    {
                        context.Database.Migrate();
						if(seeder!= null)
							await seeder.Seed();
                    });
            }

            return webHost;
        }
    }
}