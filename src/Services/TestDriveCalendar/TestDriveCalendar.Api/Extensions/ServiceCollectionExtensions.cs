﻿using FluentValidation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.HealthChecks;
using NexCore.Infrastructure.Seeds;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Application.Models;
using NexCore.TestDriveCalendar.Api.Application.Validations;
using NexCore.TestDriveCalendar.Api.Domain;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Notes;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Infrastructure;
using NexCore.TestDriveCalendar.Api.Infrastructure.Repositories;
using NexCore.TestDriveCalendar.Api.Models;
using NexCore.TestDriveCalendar.Api.Queries;
using NexCore.TestDriveCalendar.Api.Setup.Seeds;
using NexCore.WebApi.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace NexCore.TestDriveCalendar.Api.Extensions
{
    static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddHealthChecks(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddHealthChecks(checks =>
            {
                var cacheDurationInMinutes = configuration.GetValue("HealthCheck:CacheDurationInMinutes", 1);

                checks.AddSqlCheck(
                    "ContactManager",
                    configuration["ConnectionString"],
                    TimeSpan.FromMinutes(cacheDurationInMinutes));
            });

            return services;
        }

        public static IServiceCollection AddSwagger(this IServiceCollection services,
            IConfiguration configuration)
        {
            return services.AddSwaggerGen(
                configuration,
                "The Test-Drive Service HTTP API",
                "v1",
                new Dictionary<string, string>
                {
                    { "NexCore.TestDriveCalendar", "Test-Drive Service API" }
                });
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {   
            services.AddTransient<IMakeRepository, MakeRepository>();
            services.AddTransient<IFleetRepository, FleetRepository>();
            services.AddTransient<IDealerRepository, DealerRepository>();
            services.AddTransient<IRepository<Make>, MakeRepository>();
            services.AddTransient<ITestDriveRepository, TestDriveRepository>();
            services.AddTransient<INotesRepository, NoteRepository>();

            services.AddScoped<IDbConnection>(
                f => new SqlConnection(configuration["ConnectionString"]));

            return services;
        }

        public static IServiceCollection AddValidation(this IServiceCollection services)
        {
            services.AddTransient<IValidator<VehicleItem>, VehicleItemValidator>();
            services.AddTransient<IValidator<SetScheduleCommand>, SetScheduleCommandValidator>();
            services.AddTransient<IValidator<DailyScheduleModel>, DailyScheduleValidator>();
            services.AddTransient<IValidator<WorkingHoursModel>, WorkingHoursValidator>();
            services.AddTransient<IValidator<CreateTestDriveCommand>, CreateTestDriveCommandValidator>();
            services.AddTransient<IValidator<TestDriveByDealerDateItem>, TestDriveByDealerDateItemValidator>();
            services.AddTransient<IValidator<SetTestDriveClientCommand>, SetTestDriveClientCommandValidator>();
            services.AddTransient<IValidator<RescheduleTestDriveCommand>, SetTestDriveDurationCommandValidator>();
            services.AddTransient<IValidator<TestDriveStatusCommand>, TestDriveStatusCommandValidator>();
            services.AddTransient<IValidator<CreateNoteCommand>, CreateNoteCommandValidator>();
            services.AddTransient<IValidator<UpdateNoteCommand>, UpdateNoteCommandValidator>();
            services.AddTransient<IValidator<RemoveNoteCommand>, RemoveNoteCommandValidator>();
            services.AddTransient<IValidator<UpdateVehicleCommand>, UpdateVehicleCommandValidator>();

            return services;
        }

        public static IServiceCollection AddQueries(this IServiceCollection services)
        {
            if (services.All(s => s.ServiceType != typeof(IMakeQueries)))
                services.AddTransient<IMakeQueries, MakeQueries>();

            if (services.All(s => s.ServiceType != typeof(IFleetQuery)))
                services.AddTransient<IFleetQuery, FleetQuery>();

            if (services.All(s => s.ServiceType != typeof(ITestDriveQueries)))
                services.AddTransient<ITestDriveQueries, TestDriveQueries>();

            if (services.All(s => s.ServiceType != typeof(INotesQuery)))
                services.AddTransient<INotesQuery, NotesQuery>();

            return services;
        }

        public static IServiceCollection AddCommandHandlers(this IServiceCollection services)
        {
            services.AddTransient<ICreateNoteCommandHandler, CreateNoteCommandHandler>();
            services.AddTransient<IUpdateNoteCommandHandler, UpdateNoteCommandHandler>();
            services.AddTransient<IRemoveNoteCommandHandler, RemoveNoteCommandHandler>();
            services.AddTransient<IUpdateVehicleCommandHandler, UpdateVehicleCommandHandler>();

            return services;
        }

        public static IServiceCollection AddSeeders(this IServiceCollection services)
        {   
            services.AddTransient<ISeeder<Dealer>, DealerSeeder>();
            services.AddTransient<ISeeder<Make>, MakeSeeder>();
            services.AddTransient<IDbContextSeeder<TestDriveContext>, TestDriveContextSeeder>();

            return services;
        }
    }
}
