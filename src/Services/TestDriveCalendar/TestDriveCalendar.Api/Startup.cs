﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexCore.WebApi.Filters;
using NexCore.WebApi.Extensions;
using NexCore.Infrastructure.Extensions;
using NexCore.TestDriveCalendar.Api.Application.Filters;
using NexCore.TestDriveCalendar.Api.Extensions;
using NexCore.TestDriveCalendar.Api.Infrastructure;
using System;

namespace NexCore.TestDriveCalendar.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var application = typeof(Startup).Assembly;

            services
                .AddAutoMapper(application)
                .AddDbContext<TestDriveContext>(Configuration["ConnectionString"], Configuration)
                .AddHealthChecks(Configuration)
                .AddSwagger(Configuration)
                .AddQueries()
                .AddCommandHandlers()
                .AddRepositories(Configuration)
				.AddSeeders()
                .AddValidation()
                .AddMvc(options =>
                {
                    options.Filters.Add(typeof(ValidationActionFilter));
                    options.Filters.Add(typeof(ExceptionFilter));
                })
                .AddFluentValidation()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            ConfigureAuthentication(services);

			var container = new ContainerBuilder();
			container.Populate(services);

			return new AutofacServiceProvider(container.Build());
		}

        // This method is required by testing to change authentication options
        protected virtual void ConfigureAuthentication(IServiceCollection services)
        {
            services.AddJwtBearerAuthentication(Configuration, "NexCore.TestDriveCalendar");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");

                c.OAuthClientId("NexCore.TestDriveCalendar.Swagger");
            });

            app.UseMvc();
        }
	}
}
