﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Controllers
{
    [Authorize]
    [Route("api/test-drives")]
    [ApiController]
    public class SetTestDriveClientController : Controller
    {
        private readonly ITestDriveRepository _testDriveRepository;

        public SetTestDriveClientController(ITestDriveRepository testDriveRepository)
        {
            _testDriveRepository = testDriveRepository ?? throw new ArgumentNullException(nameof(testDriveRepository)); 
        }

        [Route("set-client")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SetClient([FromBody]SetTestDriveClientCommand command)
        {
            if (command == null)
                return BadRequest();

            var commandResult = await Handle(command);

            return commandResult ? Ok() : (IActionResult)BadRequest();
        }

        private async Task<bool> Handle(SetTestDriveClientCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            var testDrive = await FindTestDriveByIdOrThrow(command.TestDriveId);
            testDrive.ClientId = command.ClientId;
           
            await _testDriveRepository.UnitOfWork.Commit(CancellationToken.None);

            return true;
        }

        private async Task<TestDrive> FindTestDriveByIdOrThrow(int testDriveId)
        {
            var testDrive = await _testDriveRepository.Find(testDriveId);
            if (testDrive == null)
                throw ValidationExceptionFor(nameof(testDriveId), "TestDrive not found.", testDriveId);

            return testDrive;
        }

        private static ValidationException ValidationExceptionFor(string propertyName, string message, object attemptedValue)
        {
            return new ValidationException(
                "Failed to handle command.", new[]
                {
                    new ValidationFailure(propertyName, message, attemptedValue)
                });
        }
    }
}
