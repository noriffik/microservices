﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Queries;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MakesController : Controller
    {
        private readonly IMakeQueries _queries;
        private readonly IMakeRepository _repository;

        public MakesController(IMakeQueries queries, IMakeRepository repository)
        {
            _queries = queries ?? throw new ArgumentNullException(nameof(queries));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        [Route("list")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<Make>))]
        public async Task<JsonResult> List()
        {
            return Json(await _queries.AllAsync());
        }

        [Route("{id:int}/models")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<Model>))]
        public async Task<JsonResult> Models([FromRoute]int id)
        {
            var make = await _repository.Find(id);
            
            return Json(make != null ? make.Models : new Model[] { });
        }
    }
}
