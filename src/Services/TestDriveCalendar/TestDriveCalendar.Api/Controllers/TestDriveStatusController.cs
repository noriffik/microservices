﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Controllers
{
    [Authorize]
    [Route("api/test-drive")]
    [ApiController]
    public class TestDriveStatusController : Controller
    {
        private readonly ITestDriveRepository _testDriveRepository;

        public TestDriveStatusController(ITestDriveRepository testDriveRepository)
        {
            _testDriveRepository = testDriveRepository ?? throw new ArgumentNullException(nameof(testDriveRepository));
        }
        
        [Route("begin")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Begin([FromBody] TestDriveStatusCommand command)
        {
            if (command == null)
                return BadRequest();

            var testDrive = await GetTestDrive(command);

            try
            {
                testDrive.Begin(command.SalesManagerId);
            }
            catch (InvalidStatusChangeException e)
            {
                throw ValidationExceptionFor("Status", e.Message);
            }

            await CommitChanges();

            return Ok();
        }

        [Route("complete")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Complete([FromBody] TestDriveStatusCommand command)
        {
            if (command == null)
                return BadRequest();

            var testDrive = await GetTestDrive(command);
            try
            {
                testDrive.Complete(command.SalesManagerId);
            }
            catch (InvalidStatusChangeException e)
            {
                throw ValidationExceptionFor("Status", e.Message);
            }

            await CommitChanges();

            return Ok();
        }

        [Route("cancel")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Cancel([FromBody] TestDriveStatusCommand command)
        {
            if (command == null)
                return BadRequest();

            var testDrive = await GetTestDrive(command);
            try
            {
                testDrive.Cancel(command.SalesManagerId);
            }
            catch (InvalidStatusChangeException e)
            {
                throw ValidationExceptionFor("Status", e.Message);
            }

            await CommitChanges();

            return Ok();
        }

        private async Task<TestDrive> GetTestDrive(TestDriveStatusCommand command)
        {
            var testDrive = await _testDriveRepository.Find(command.TestDriveId);

            if (testDrive == null)
                throw ValidationExceptionFor(nameof(command.TestDriveId), "TestDrive does not exists!");

            return testDrive;
        }

        private async Task CommitChanges()
        {
            await _testDriveRepository
                .UnitOfWork
                .Commit(CancellationToken.None);
        }

        private static ValidationException ValidationExceptionFor(string propertyName, string message)
        {
            return new ValidationException("Failed to handle command.", new []
            {
                new ValidationFailure(propertyName, message)
            });
        }
    }
}
