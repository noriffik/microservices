﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Application.Models;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Controllers
{
    [Authorize]
    [Route("api/dealers")]
    [ApiController]
    public class SetScheduleController : Controller
    {
        private readonly IDealerRepository _dealerRepository;

        public SetScheduleController(IDealerRepository dealerRepository)
        {
            _dealerRepository = dealerRepository ?? throw new ArgumentNullException(nameof(dealerRepository)); 
        }

        [Route("set-schedule")]
        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SetSchedule([FromBody]SetScheduleCommand command)
        {
            if (command == null)
                return BadRequest();
            
            var commandResult = await Handle(command);

            return commandResult ? Ok() : (IActionResult)BadRequest();
        }
        
        private async Task<bool> Handle(SetScheduleCommand request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var dealer = await FindDealerOrThrow(request);
            dealer.Schedule = MapWeeklySchedule(request.WeeklySchedule);

            await _dealerRepository.UnitOfWork.Commit(CancellationToken.None);

            return true;
        }

        private async Task<Dealer> FindDealerOrThrow(SetScheduleCommand request)
        {
            var dealer = await _dealerRepository.Find(request.DealerId);
            if (dealer == null)
                throw ValidationExceptionFor(nameof(request.DealerId), "Dealer not found.", request.DealerId);

            return dealer;
        }

        private static ValidationException ValidationExceptionFor(string propertyName, string message, object attemptedValue)
        {
            return new ValidationException(
                "Failed to handle command.", new[]
                {
                    new ValidationFailure(propertyName, message, attemptedValue)
                });
        }

        private Schedule MapWeeklySchedule(ICollection<DailyScheduleModel> dailyScheduleModels)
        {
            if (dailyScheduleModels == null)
                throw ValidationExceptionFor(
                    nameof(dailyScheduleModels), "Weekly schedule is empty.", dailyScheduleModels);
            
            return new Schedule(dailyScheduleModels.Select(MapDailySchedule).ToList());
        }

        private DailySchedule MapDailySchedule(DailyScheduleModel model)
        {
            if (model.WorkingDay == null)
                throw ValidationExceptionFor(
                    nameof(model.WorkingDay), "Working day is null", model.WorkingDay);

            if (model.WorkingHours == null)
                throw ValidationExceptionFor(
                    nameof(model.WorkingHours), "Working hours is null", model.WorkingHours);
            
            return new DailySchedule(
                (DayOfWeek) model.WorkingDay, new WorkingHours(
                    new Time(model.WorkingHours.Since), new Time(model.WorkingHours.Till)));
        }
    }
}
