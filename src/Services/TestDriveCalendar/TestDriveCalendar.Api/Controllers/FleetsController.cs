﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Models;
using NexCore.TestDriveCalendar.Api.Queries;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FleetsController : Controller
    {
        private readonly IFleetRepository _fleetRepository;
        private readonly IDealerRepository _dealerRepository;
        private readonly IFleetQuery _fleetQuery;
        private readonly IUpdateVehicleCommandHandler _updateVehicleCommandHandler;
        private IVehicleMapper _mapper;
        
        public FleetsController(IFleetRepository fleetRepository, IDealerRepository dealerRepository, 
            IFleetQuery fleetQuery, IUpdateVehicleCommandHandler updateVehicleCommandHandler)
        {
            _fleetRepository = fleetRepository ?? throw new ArgumentNullException(nameof(fleetRepository));
            _dealerRepository = dealerRepository ?? throw new ArgumentNullException(nameof(dealerRepository));
            _fleetQuery = fleetQuery ?? throw new ArgumentNullException(nameof(fleetQuery));
            _updateVehicleCommandHandler = updateVehicleCommandHandler ?? throw new ArgumentNullException(nameof(updateVehicleCommandHandler));
        }

        public IVehicleMapper Mapper => _mapper ?? (_mapper = new VehicleMapper());

        [Route("vehicle/{id:int}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(VehicleListItem), (int) HttpStatusCode.OK)]
        public async Task<IActionResult> Get([FromRoute]int id)
        {
            var vehicle = await _fleetQuery.ByVehicleIdAsync(id);
            return vehicle == null ? (IActionResult)NotFound() : Json(vehicle);
        }

        [Route("vehicle/add")]
        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Add([FromBody] VehicleItem query)
        {
            var vehicle = Mapper.Map(query);

            var fleet = await FindFleet(query);
            
            if (fleet.IsMember(vehicle.Vin))
                throw ValidationExceptionFor(nameof(query.Vin), "Vehicle is already a member of the fleet", query.Vin);

            fleet.Add(vehicle);

            await _fleetRepository.UnitOfWork.Commit(CancellationToken.None);

            return CreatedAtAction(nameof(Add), new {id = vehicle.Id}, null);
        }

        private static ValidationException ValidationExceptionFor(string propertyName, string message, object attemptedValue)
        {
            return new ValidationException(
                "Failed to handle command.", new[]
                {
                    new ValidationFailure(propertyName, message, attemptedValue)
                });
        }

        private async Task<Fleet> FindFleet(VehicleItem query)
        {
            var fleet = await _fleetRepository.FindByDealerIdAsync(query.DealerId);
            if (fleet != null)
                return fleet;
            
            var doesDealerExist = await _dealerRepository.Has(query.DealerId);
            if (!doesDealerExist)
                throw ValidationExceptionFor(nameof(query.DealerId), "Dealer was not found", query.DealerId);

            fleet = new Fleet(query.DealerId);

            _fleetRepository.Add(fleet);

            return fleet;
        }

        [Route("vehicle/update")]
        [HttpPut]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Update([FromBody] UpdateVehicleCommand command)
        {
            if (command == null)
                return BadRequest();

            var result = await _updateVehicleCommandHandler.Handle(command, CancellationToken.None);
            return result ? Ok() : BadRequest() as IActionResult;
        }

        [Route("vehicle/availability")]
        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SetAvailability([FromBody] SetUnavailability query)
        {
            if (query == null)
                return BadRequest();

            var fleet = await FindFleet(query);

            fleet.ChangeAvailability(query.VehicleId, query.Availability);

            await _fleetRepository.UnitOfWork.Commit();

            return Ok();
        }

        [Route("{dealerId}/vehicles")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(IEnumerable<VehicleListItem>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> VehiclesList([FromRoute] string dealerId)
        {
            if (!await _dealerRepository.Has(dealerId))
                return NotFound();

            return Json(await _fleetQuery.ByDealerIdAsync(dealerId));
        }

        private async Task<Fleet> FindFleet(SetUnavailability query)
        {
            var fleet = await _fleetRepository.FindByVehicleIdAsync(query.VehicleId);
            if (fleet == null)
                throw ValidationExceptionFor(nameof(query.VehicleId), "Vehicle was not found", query.VehicleId);

            return fleet;
        }
    }
}