﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.Notes;
using NexCore.TestDriveCalendar.Api.Queries;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Controllers
{
    [Authorize]
    [Route("api/test-drive/[controller]")]
    [ApiController]
    public class NotesController : Controller
    {
        private readonly ICreateNoteCommandHandler _createNoteCommandHandler;
        private readonly INotesQuery _notesQuery;
        private readonly IUpdateNoteCommandHandler _updateNoteCommandHandler;
        private readonly IRemoveNoteCommandHandler _removeNoteCommandHandler;
        
        public NotesController(ICreateNoteCommandHandler createNoteCommandHandler, INotesQuery notesQuery,
            IUpdateNoteCommandHandler updateNoteCommandHandler, IRemoveNoteCommandHandler removeNoteCommandHandler)
        {
            _createNoteCommandHandler = createNoteCommandHandler ?? throw new ArgumentNullException(nameof(createNoteCommandHandler));
            _notesQuery = notesQuery ?? throw new ArgumentNullException(nameof(notesQuery));
            _updateNoteCommandHandler = updateNoteCommandHandler ?? throw new ArgumentNullException(nameof(updateNoteCommandHandler));
            _removeNoteCommandHandler = removeNoteCommandHandler ?? throw new ArgumentNullException(nameof(removeNoteCommandHandler));
        }

        [Route("{id:int}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(Note), (int)HttpStatusCode.OK)]
        public Task<OkResult> Get(int id) //TODO : Implement 
        {
            return Task.FromResult(Ok());
        }

        [Route("create")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Create([FromBody] CreateNoteCommand command)
        {
            if (command == null)
                return BadRequest();

            var result = await _createNoteCommandHandler.Handle(command, CancellationToken.None);

            return result > 0
                ? CreatedAtAction(nameof(Get), new { id = result }, result)
                : BadRequest() as IActionResult;
        }

        [Route("list-by-test-drive/{id:int}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(IEnumerable<dynamic>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetNotesByTestDriveId([FromRoute] int id)
        {
            var listNotes = await _notesQuery.GetAllNotesByTestDriveId(id);
            return Json(listNotes);
        }

        [Route("update")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Update([FromBody] UpdateNoteCommand command)
        {
            if (command == null)
                return BadRequest();

            try
            {
                var result = await _updateNoteCommandHandler.Handle(command, CancellationToken.None);
                return result ? Ok() : BadRequest() as IActionResult;
            }
            catch (OwnerRequiredException e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("remove")]
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Remove([FromQuery] RemoveNoteCommand command)
        {
            if (command == null)
                return BadRequest();

            try
            {
                var result = await _removeNoteCommandHandler.Handle(command, CancellationToken.None);
                return result ? Ok() : BadRequest() as IActionResult;
            }
            catch (OwnerRequiredException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
