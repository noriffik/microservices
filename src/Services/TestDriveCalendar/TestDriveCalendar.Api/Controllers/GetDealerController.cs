﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Models;
using System;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Controllers
{
    [Authorize]
    [Route("api/dealers")]
    [ApiController]
    public class GetDealerController : Controller
    {
        private readonly IDealerRepository _dealerRepository;
        private readonly IMapper _mapper;

        public GetDealerController(IDealerRepository dealerRepository, IMapper mapper)
        {
            _dealerRepository = dealerRepository ?? throw new ArgumentNullException(nameof(dealerRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [Route("{id}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(DealerModel))]
        public async Task<IActionResult> GetDealer([FromRoute]string id)
        {
            var dealer = await _dealerRepository.Find(id);

            return dealer == null
                ? (IActionResult) NotFound()
                : Json(_mapper.Map<DealerModel>(dealer));
        }
    }
}
