﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Controllers
{
    [Authorize]
    [Route("api/test-drives")]
    [ApiController]
    public class RescheduleTestDriveController : Controller
    {
        private readonly ITestDriveRepository _testDriveRepository;
        private readonly IDealerRepository _dealerRepository;
        private readonly IFleetRepository _fleetRepository;

        public RescheduleTestDriveController(ITestDriveRepository testDriveRepository, IDealerRepository dealerRepository, IFleetRepository fleetRepository)
        {
            _testDriveRepository = testDriveRepository ?? throw new ArgumentNullException(nameof(testDriveRepository));
            _dealerRepository = dealerRepository ?? throw new ArgumentNullException(nameof(dealerRepository)); 
            _fleetRepository = fleetRepository ?? throw new ArgumentNullException(nameof(fleetRepository)); 
        }

        [Route("reschedule")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Reschedule([FromBody]RescheduleTestDriveCommand command)
        {
            if (command == null)
                return BadRequest();

            var commandResult = await Handle(command);

            return commandResult ? Ok() : (IActionResult)BadRequest();
        }

        private async Task<bool> Handle(RescheduleTestDriveCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            var testDrive = await FindTestDriveOrThrow(command);

            var canReschedule = await _testDriveRepository.CanRescheduleAsync(testDrive, command.Since, command.Till);
            if (!canReschedule)
                throw ValidationExceptionFor(nameof(command.Since), "Vehicle is not available on this time", command.Since);
            
            var fleet = await FindFleetByVehicleIdOrThrow(testDrive.VehicleId);
            if (!fleet.IsAvailable(testDrive.VehicleId))
                throw ValidationExceptionFor(nameof(command.TestDriveId), "Vehicle is not available in the fleet.", command.TestDriveId);
            
            var dealer = await FindDealerByFleetIdOrThrow(fleet.Id);
            if (!dealer.IsOpenOn(command.Since, command.Till))
                throw ValidationExceptionFor(nameof(command.Since), "Dealer is closed at this time.", command.Since);

            try
            {
                testDrive.Reschedule(command.SalesManagerId, command.Since, command.Till);
            }
            catch (InvalidStatusChangeException e)
            { 
                throw ValidationExceptionFor(nameof(command.TestDriveId), $"Test-drive cannot be rescheduled due to: {e.Message}", command.TestDriveId);
            }
           
            await _testDriveRepository.UnitOfWork.Commit(CancellationToken.None);

            return true;
        }

        private async Task<TestDrive> FindTestDriveOrThrow(RescheduleTestDriveCommand command)
        {
            var testDrive = await _testDriveRepository.Find(command.TestDriveId);
            if (testDrive == null)
                throw ValidationExceptionFor(nameof(command.TestDriveId), "TestDrive not found.", command.TestDriveId);

            return testDrive;
        }

        private async Task<Fleet> FindFleetByVehicleIdOrThrow(int vehicleId)
        {
            var fleet = await _fleetRepository.FindByVehicleIdAsync(vehicleId);
            if (fleet == null)
                throw new InvalidOperationException("Fleet is not found for existing test-drive.");

            return fleet;
        }

        private async Task<Dealer> FindDealerByFleetIdOrThrow(int fleetId)
        {
            var dealer = await _dealerRepository.FindByFleetIdAsync(fleetId);
            if (dealer == null)
                throw new InvalidOperationException("Dealer is not found for existing fleet.");

            return dealer;
        }

        private static ValidationException ValidationExceptionFor(string propertyName, string message, object attemptedValue)
        {
            return new ValidationException(
                "Failed to handle command.", new[]
                {
                    new ValidationFailure(propertyName, message, attemptedValue)
                });
        }
    }
}
