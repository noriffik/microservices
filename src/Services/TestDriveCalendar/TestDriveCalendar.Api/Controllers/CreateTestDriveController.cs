﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Controllers
{
    [Authorize]
    [Route("api/test-drives")]
    [ApiController]
    public class CreateTestDriveController : Controller
    {
        private readonly ITestDriveRepository _testDriveRepository;
        private readonly IDealerRepository _dealerRepository;
        private readonly IFleetRepository _fleetRepository;

        public CreateTestDriveController(ITestDriveRepository testDriveRepository, IDealerRepository dealerRepository, IFleetRepository fleetRepository)
        {
            _testDriveRepository = testDriveRepository ?? throw new ArgumentNullException(nameof(testDriveRepository)); 
            _dealerRepository = dealerRepository ?? throw new ArgumentNullException(nameof(dealerRepository)); 
            _fleetRepository = fleetRepository ?? throw new ArgumentNullException(nameof(fleetRepository)); 
        }

        [Route("create")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Create([FromBody]CreateTestDriveCommand command)
        {
            if (command == null)
                return BadRequest();

            var id = await Handle(command);

            //TODO: Implement 0 check, when Id is 0, return bad request.
            
            //TODO: Insure that Get request url is returned

            return CreatedAtAction("Get", "TestDrives", new {Id = id}, id);
        }

        private async Task<int> Handle(CreateTestDriveCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            var fleet = await FindFleetByVehicleIdOrThrow(command.VehicleId);
            if (!fleet.IsAvailable(command.VehicleId))
                throw ValidationExceptionFor(nameof(command.VehicleId), "Fleet is not available.", command.VehicleId);
            
            var dealer = await FindDealerByFleetIdOrThrow(fleet.Id);
            if (!dealer.IsOpenOn(command.Since, command.Till))
                throw ValidationExceptionFor(nameof(dealer), "Dealer is not available.", dealer);

            var isReserved = await _testDriveRepository.IsVehicleReservedAsync(command.VehicleId, command.Since, command.Till);
            if (isReserved)
                throw ValidationExceptionFor(nameof(command.VehicleId), "Vehicle is not available on this time", command.VehicleId);

            var testDrive = MapTestDrive(command);

            _testDriveRepository.Add(testDrive);

            await _testDriveRepository.UnitOfWork.Commit(CancellationToken.None);

            return testDrive.Id;
        }

        private async Task<Fleet> FindFleetByVehicleIdOrThrow(int vehicleId)
        {
            var fleet = await _fleetRepository.FindByVehicleIdAsync(vehicleId);
            if (fleet == null)
                throw ValidationExceptionFor(nameof(vehicleId), "Fleet not found.", vehicleId);

            return fleet;
        }

        private async Task<Dealer> FindDealerByFleetIdOrThrow(int fleetId)
        {
            var dealer = await _dealerRepository.FindByFleetIdAsync(fleetId);
            if (dealer == null)
                throw ValidationExceptionFor(nameof(fleetId), "Dealer not found.", fleetId);

            return dealer;
        }

        //TODO: Extract this
        private TestDrive MapTestDrive(CreateTestDriveCommand command)
        {
            return new TestDrive(
                command.ClientId,
                command.VehicleId,
                command.Since,
                command.Till,
                command.SalesManagerId);
        }

        private static ValidationException ValidationExceptionFor(string propertyName, string message, object attemptedValue)
        {
            return new ValidationException(
                "Failed to handle command.", new[]
                {
                    new ValidationFailure(propertyName, message, attemptedValue)
                });
        }
    }
}