﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Controllers
{
    [Authorize]
    [Route("api/dealers")]
    [ApiController]
    public class SetTypicalDurationController : Controller
    {
        private readonly IDealerRepository _dealerRepository;

        public SetTypicalDurationController(IDealerRepository dealerRepository)
        {
            _dealerRepository = dealerRepository ?? throw new ArgumentNullException(nameof(dealerRepository));
        }

        [Route("set-typical-duration")]
        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SetTypicalDuration([FromBody]SetTestDriveTypicalDurationCommand command)
        {
            if (command?.DealerId == null)
                return BadRequest();

            var commandResult = await Handle(command);

            return commandResult ? Ok() : (IActionResult)BadRequest();
        }

        private async Task<bool> Handle(SetTestDriveTypicalDurationCommand request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var dealer = await FindDealerOrThrow(request);

            dealer.TestDriveDuration = request.TestDriveDuration;

            await _dealerRepository.UnitOfWork.Commit(CancellationToken.None);

            return true;
        }

        private async Task<Dealer> FindDealerOrThrow(SetTestDriveTypicalDurationCommand request)
        {
            var dealer = await _dealerRepository.Find(request.DealerId);
            if (dealer == null)
                throw new ValidationException(
                    "Failed to handle command.",
                    new[]
                    {
                        new ValidationFailure(
                            nameof(request.DealerId),
                            "Dealer not found.", request.DealerId)
                    });

            return dealer;
        }
    }

}
