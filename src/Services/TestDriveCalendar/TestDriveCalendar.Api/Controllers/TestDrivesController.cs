﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.TestDriveCalendar.Api.Models;
using NexCore.TestDriveCalendar.Api.Queries;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;

namespace NexCore.TestDriveCalendar.Api.Controllers
{
    [Authorize]
    [Route("api/test-drives")]
    [ApiController]
    public class TestDrivesController : Controller
    {
        private readonly ITestDriveQueries _queries;
        private readonly ITestDriveRepository _testDriveRepository;

        public TestDrivesController(ITestDriveQueries queries, ITestDriveRepository testDriveRepository)
        {
            _queries = queries ?? throw new ArgumentNullException(nameof(queries));
            _testDriveRepository = testDriveRepository ?? throw new ArgumentNullException(nameof(testDriveRepository));
        }

        [Route("{id:int}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(TestDriveListItem), (int) HttpStatusCode.OK)]
#pragma warning disable 1998
        public async Task<IActionResult> Get([FromRoute]int id)
#pragma warning restore 1998
        {
            var testDrive = await _testDriveRepository.Find(id); 

            return testDrive == null ? (IActionResult)NotFound() : Json(testDrive);
        }

        [Route("list-all")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<TestDriveListItem>))]
        public async Task<JsonResult> List()
        {
            return Json(await _queries.AllAsync());
        }

        [Route("by-dealer-date/{DealerId}/{Date:DateTime}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<TestDriveListItem>))]
        public async Task<JsonResult> ListByDealerDate([FromRoute] TestDriveByDealerDateItem query)
        {
            return Json(await _queries.ByDateAndDealerIdAsync(query));
        }
    }
}
