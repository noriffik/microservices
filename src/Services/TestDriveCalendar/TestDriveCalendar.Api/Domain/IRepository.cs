﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using NexCore.Domain;

namespace NexCore.TestDriveCalendar.Api.Domain
{
    public interface IRepository<TEntity> where TEntity : IAggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }

        void Add(TEntity entity);

        void Remove(TEntity entity);
        
        Task<TEntity> Find(int id);

        Task<IEnumerable<TEntity>> Find(Specification<TEntity> specification, CancellationToken cancellationToken);

        Task<TEntity> SingleOrDefault(Specification<TEntity> specification, CancellationToken cancellationToken);
        
        Task<bool> Has(int id);

        Task<bool> Has(Specification<TEntity> specification, CancellationToken cancellationToken);

        Task<bool> Has(IEnumerable<int> ids);

        Task<int> Count(Specification<TEntity> specification, CancellationToken cancellationToken);
    }
}