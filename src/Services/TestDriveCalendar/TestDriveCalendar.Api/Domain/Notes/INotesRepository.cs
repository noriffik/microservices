﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Domain.Notes
{
    public interface INotesRepository : IRepository<Note>
    {
        Task<IEnumerable<Note>> FindAllByTestDriveIdAsync(int testDriveId);
    }
}
