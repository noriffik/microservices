﻿using System;
using NexCore.Domain;

namespace NexCore.TestDriveCalendar.Api.Domain.Notes
{
    public class Note : Entity, IAggregateRoot
    {
        private string _content;
        public string Content
        {
            get => _content;
            set
            {
                _content = value;
                UpdatedDate = DateTime.Now;
            }
        }
        public int TestDriveId { get; private set; }
        public int ManagerId { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public DateTime UpdatedDate { get; private set; }

        private Note() { }

        public Note(int testDriveId, int managerId , string content) : this(0, testDriveId, managerId, content)
        {  
        }

        public Note(int id, int testDriveId, int managerId, string content) : base(id)
        {
            if (string.IsNullOrEmpty(content))
                throw new ArgumentException("Content must be not empty.", nameof(content));
            
            TestDriveId = testDriveId;
            ManagerId = managerId;
            Content = content;
            CreatedDate = DateTime.Now;
            UpdatedDate = CreatedDate;
        }
    }
}
