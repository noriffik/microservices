﻿using NexCore.Domain;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Domain.Dealers
{
    public interface IDealerRepository
    {
        void Add(Dealer entity);

        Task<Dealer> Find(DealerCode id);

        Task<bool> Has(DealerCode id);

        Task<Dealer> FindByFleetIdAsync(int fleetId);

        IUnitOfWork UnitOfWork { get; }
    }
}
