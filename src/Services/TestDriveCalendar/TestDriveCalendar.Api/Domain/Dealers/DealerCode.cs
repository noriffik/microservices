﻿using NexCore.Domain;

namespace NexCore.TestDriveCalendar.Api.Domain.Dealers
{
    public class DealerCode : EntityId
    {
        protected DealerCode()
        {
        }

        protected DealerCode(string value) : base(value)
        {
        }

        public static DealerCode Parse(string value)
        {
            return new DealerCode(value);
        }

        public static bool TryParse(string value, out DealerCode dealerCode)
        {
            dealerCode = string.IsNullOrEmpty(value) ? null : new DealerCode(value);

            return dealerCode != null;
        }

        public static implicit operator string(DealerCode dealerCode)
        {
            return dealerCode?.Value;
        }

        public static implicit operator DealerCode(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}