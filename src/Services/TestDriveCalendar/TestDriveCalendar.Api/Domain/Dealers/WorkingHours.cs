﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Domain.Extensions;

namespace NexCore.TestDriveCalendar.Api.Domain.Dealers
{
    public class WorkingHours : ValueObject
    {
        public Time Since { get; private set; }
        public Time Till { get; private set; }

        private WorkingHours()
        {
        }
        
        [JsonConstructor]
        public WorkingHours(Time since, Time till)
        {
            if (since == null)
                throw new ArgumentNullException(nameof(since));

            if (till == null)
                throw new ArgumentNullException(nameof(till));

            if (since >= till)
                throw new InvalidOperationException("Since time must be less that till time");

            Since = since;
            Till = till;
        }
        
        public static WorkingHours Empty => new WorkingHours();

        public static WorkingHours Typical => new WorkingHours(new Time(9, 0, 0), new Time(18, 0, 0));
        
        protected override IEnumerable<object> GetValues()
        {
            yield return Since;
            yield return Till;
        }

        public override string ToString() =>
            Since != null ? $"{Since.Hours}:{Since.Minutes} - {Till.Hours}:{Till.Minutes}" : string.Empty;
    }

    
}
