﻿using NexCore.Domain;
using System;

namespace NexCore.TestDriveCalendar.Api.Domain.Dealers
{
    public class Dealer : Entity<DealerCode>, IAggregateRoot<DealerCode>
    {
        public Schedule Schedule { get; set; }

        public int? TestDriveDuration { get; set; }

        private Dealer()
        {
        }
        
        public Dealer(DealerCode id, Schedule schedule, int testDriveDuration) : base(id)
        {
            Schedule = schedule;
            TestDriveDuration = testDriveDuration;
        }

        public bool IsOpenOn(DateTime since, DateTime till)
        {
            return Schedule.IsIn(since, till);
        }
    }
}
