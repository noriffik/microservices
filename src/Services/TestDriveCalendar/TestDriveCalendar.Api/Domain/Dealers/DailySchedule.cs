﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Domain.Extensions;

namespace NexCore.TestDriveCalendar.Api.Domain.Dealers
{
    public class DailySchedule : ValueObject
    {
        public DayOfWeek? WorkingDay { get; private set; }
        public WorkingHours WorkingHours { get; private set; }

        private DailySchedule()
        {
        }

        public DailySchedule(
            DayOfWeek workingDay,
            Time since,
            Time till)
        {
            WorkingDay = workingDay;
            WorkingHours = new WorkingHours(since, till);
        }

        [JsonConstructor]
        public DailySchedule(
            DayOfWeek workingDay,
            WorkingHours workingHours)
        {
            WorkingDay = workingDay;
            WorkingHours = workingHours;
        }
        
        protected override IEnumerable<object> GetValues()
        {
            yield return WorkingDay.ToString();
            yield return WorkingHours.ToString();
        }

        public interface IDailyScheduleBuilder
        {
            IDailyScheduleBuilder WithDay(DayOfWeek weekDay);
            IDailyScheduleBuilder WithTime(Time since, Time till);

            DailySchedule Create();
        }

        class DailyScheduleBuilder : IDailyScheduleBuilder
        {
            private DayOfWeek? _workingDay = null;
            private WorkingHours _workingHours = WorkingHours.Empty;

            public IDailyScheduleBuilder WithDay(DayOfWeek weekDay)
            {
                _workingDay = weekDay;
                return this;
            }

            public IDailyScheduleBuilder WithTime(Time since, Time till)
            {
                _workingHours = new WorkingHours(since, till);
                return this;
            }

            public DailySchedule Create()
            {
                return new DailySchedule
                {
                    WorkingDay = _workingDay,
                    WorkingHours = _workingHours
                };
            }
        }

        public static IDailyScheduleBuilder Build()
        {
            return new DailyScheduleBuilder();
        }
    }
}
