﻿using System;
using System.Collections.Generic;
using System.Linq;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Domain.Extensions;

namespace NexCore.TestDriveCalendar.Api.Domain.Dealers
{
    public class Schedule  : ValueObject
    {
        private readonly List<DailySchedule> _weekSchedule;

        public ICollection<DailySchedule> WeekSchedule => _weekSchedule.AsReadOnly();

        private Schedule()
        {
            _weekSchedule = new List<DailySchedule>();
        }
        
        public Schedule(ICollection<DailySchedule> weekSchedule)
        {
            if (weekSchedule.GroupBy(ds => ds.WorkingDay).Any(d => d.Count() > 1)) 
                throw new InvalidOperationException("Duplicate working day");

            _weekSchedule = new List<DailySchedule>(weekSchedule);
        }

        public bool IsIn(DateTime since, DateTime till)
        {
            var dailySchedule = WeekSchedule.SingleOrDefault(ds => ds.WorkingDay == since.DayOfWeek);

            return since.DayOfWeek == till.DayOfWeek &&
                   dailySchedule != null &&
                   dailySchedule.WorkingHours.Since <= new Time(since.Hour, since.Minute, since.Second) &&
                   dailySchedule.WorkingHours.Till >= new Time(till.Hour, till.Minute, till.Second);
        }

        public static Schedule Empty => new Schedule();

        public static Schedule Typical => new Schedule(
            Enumerable.Range(0, 7)
                .Select(d => new DailySchedule((DayOfWeek) d, WorkingHours.Typical))
                .ToList());

        protected override IEnumerable<object> GetValues()
        {
            return WeekSchedule;
        }
    }
}
