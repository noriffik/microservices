﻿using System;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Domain.TestDrives
{
    public interface ITestDriveRepository : IRepository<TestDrive>
    {
        Task<bool> IsVehicleReservedAsync(int vehicleId, DateTime since, DateTime till);
        
        Task<bool> CanRescheduleAsync(TestDrive testDrive, DateTime since, DateTime till);
    }
}
