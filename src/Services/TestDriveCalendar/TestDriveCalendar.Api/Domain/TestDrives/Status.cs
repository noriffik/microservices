﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace NexCore.TestDriveCalendar.Api.Domain.TestDrives
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    public class Status
    {
        public static IReadOnlyList<Status> All = new List<Status>
        {
            new ScheduledStatus(),
            new RescheduledStatus(),
            new BegunStatus(),
            new CompletedStatus(),
            new CancelledStatus()
        };

        public static Status Scheduled = All[0];
        public static Status Rescheduled = All[1];
        public static Status Begun = All[2];
        public static Status Completed = All[3];
        public static Status Cancelled = All[4];

        public int StatusId { get; }

        public string Name { get; }

        private Status()
        {
        }
        
        protected Status(int statusId, string name)
        {
            StatusId = statusId;
            Name = name;
        }

        public virtual bool IsChangeInvalid(Status status)
        {
            return true;
        }

        public static Status Get(int id)
        {
            TryGet(id, out var status);

            if (status == null)
                throw new ArgumentException("Invalid id given.", nameof(id));
            
            return status;
        }

        public static bool TryGet(int id, out Status result)
        {
            result = All.SingleOrDefault(s => s.StatusId == id);

            return result != null;
        }
    }

    internal class ScheduledStatus : Status
    {
        public ScheduledStatus() : base(1, "Scheduled")
        {
        }

        public override bool IsChangeInvalid(Status status)
        {
            if (status == null)
                throw new ArgumentNullException(nameof(status));

            return status == Scheduled || status == Completed;
        }
    }

    internal class RescheduledStatus : Status
    {
        public RescheduledStatus() : base(2, "Rescheduled")
        {
        }

        public override bool IsChangeInvalid(Status status)
        {
            if (status == null)
                throw new ArgumentNullException(nameof(status));

            return status == Scheduled || status == Completed;
        }
    }

    internal class BegunStatus : Status
    {
        public BegunStatus() : base(3, "Begun")
        {
        }

        public override bool IsChangeInvalid(Status status)
        {
            if (status == null)
                throw new ArgumentNullException(nameof(status));

            return status == Begun || status == Scheduled;
        }
    }

    internal class CompletedStatus : Status
    {
        public CompletedStatus() : base(4, "Completed")
        {
        }

        public override bool IsChangeInvalid(Status status)
        {
            if (status == null)
                throw new ArgumentNullException(nameof(status));

            return true;
        }
    }

    internal class CancelledStatus : Status
    {
        public CancelledStatus() : base(5, "Cancelled")
        {
        }

        public override bool IsChangeInvalid(Status status)
        {
            if (status == null)
                throw new ArgumentNullException(nameof(status));

            return status != Rescheduled;
        }
    }
}
