﻿using System;
using System.Diagnostics.CodeAnalysis;
using NexCore.Domain;

namespace NexCore.TestDriveCalendar.Api.Domain.TestDrives
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Local", Justification = "Required by EF Core")]
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    public class StatusChange : Entity
    {
#pragma warning disable IDE0044 // Required by EF Core
        private int _statusId;
#pragma warning restore IDE0044 // Required by EF Core
        
        public int AuthorId { get; private set; }

        public Status Status => Status.Get(_statusId);

        public DateTime Updated { get; private set; }

        public int Version { get; private set; }
        
        private StatusChange() {} 

        public StatusChange(Status status, int authorId, int version)
        {
            if (status == null)
                throw new ArgumentException(nameof(status));

            if (version <= 0)
                throw new ArgumentOutOfRangeException(nameof(version));
            
            _statusId = status.StatusId;
            
            AuthorId = authorId;

            Version = version;

            Updated = DateTime.Now;
        }
    }
}
