﻿using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace NexCore.TestDriveCalendar.Api.Domain.TestDrives
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class TestDrive : Entity, IAggregateRoot
    {
        private int _statusId;

        private readonly List<StatusChange> _statusChanges = new List<StatusChange>();

        public int ClientId { get; set; }
        public int VehicleId { get; private set; }
        public DateTime Since { get; private set; }
        public DateTime Till { get; private set; }
        public int SalesManagerId { get; private set; }

        private int Version => !StatusChanges.Any() ? 0 : StatusChanges.First().Version;

        public Status Status
        {
            get => Status.Get(_statusId);
            private set
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(value));

                if (_statusId != default(int) && Status.IsChangeInvalid(value))
                    throw new InvalidStatusChangeException("Attempt to assign invalid status");
                
                _statusId = value.StatusId;
            }
        }

        private TestDrive() : base(0)
        {
        }

        public TestDrive(int clientId, int vehicleId, DateTime since, DateTime till, int salesManagerId)
            : this(0, clientId, vehicleId, since, till, salesManagerId)
        {
        }

        public TestDrive(int id, int clientId, int vehicleId, DateTime since, DateTime till, int salesManagerId) : base(id)
        {
            ClientId = clientId;
            VehicleId = vehicleId;
            Since = since;
            Till = till;
            SalesManagerId = salesManagerId;
            Status = Status.Scheduled;

            LogStatusChange(Status.Scheduled, salesManagerId);
        }

        public IEnumerable<StatusChange> StatusChanges => _statusChanges
            .OrderByDescending(c => c.Version)
            .ToList()
            .AsReadOnly();

        public void Begin(int salesManagerId)
        {
            ChangeStatus(Status.Begun, salesManagerId);
        }

        public void Reschedule(int salesManagerId, DateTime since, DateTime till)
        {
            if (since >= till) throw new ArgumentOutOfRangeException(nameof(since), since, "Since must be less then till");
            Since = since;
            Till = till;
            ChangeStatus(Status.Rescheduled, salesManagerId);
        }
        

        public void Complete(int salesManagerId)
        {
            ChangeStatus(Status.Completed, salesManagerId);
        }

        public void Cancel(int salesManagerId)
        {
            ChangeStatus(Status.Cancelled, salesManagerId);
        }

        private void ChangeStatus(Status status, int salesManagerId)
        {
            Status = status;
            
            LogStatusChange(status, salesManagerId);
        }

        private void LogStatusChange(Status status, int salesManagerId)
        {
            _statusChanges.Add(new StatusChange(status, salesManagerId, Version + 1));
        }
    }

    public class InvalidStatusChangeException : Exception
    {
        public InvalidStatusChangeException()
        {
        }

        public InvalidStatusChangeException(string message)
            : base(message)
        {
        }

        public InvalidStatusChangeException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
