﻿using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace NexCore.TestDriveCalendar.Api.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class Fleet : Entity, IAggregateRoot
    {
        public DealerCode DealerId { get; private set; }

        private readonly List<Vehicle> _vehicles = new List<Vehicle>();

        public IReadOnlyList<Vehicle> Vehicles => _vehicles.AsReadOnly();

        private Fleet()
        {
        }

        public Fleet(DealerCode dealerId) : this(0, dealerId)
        {
        }

        public Fleet(int id, DealerCode dealerId) : base(id)
        {
            DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
        }

        public void Add(Vin vin, int modelId, VehicleItem options = null)
        {
            if (vin == null)
                throw new ArgumentNullException(nameof(vin));

            if (IsMember(vin))
                throw new InvalidOperationException("Vehicle is already added with given VIN");

            _vehicles.Add(new Vehicle(vin, modelId)
            {
                Equipment = options?.Equipment,
                Type = options?.Type,
                Engine = options?.Engine,
                GearBox = options?.GearBox,
                Options = options?.Options,
                Comments = options?.Comments
            });
        }

        public void Add(Vehicle vehicle)
        {
            _vehicles.Add(vehicle);
        }

        public bool IsMember(Vin vin)
        {
            if (vin == null)
                throw new ArgumentNullException(nameof(vin));

            return _vehicles.Any(v => v.Vin == vin);
        }

        public bool IsAvailable(int vehicleId)
        {
            var vehicle = _vehicles.SingleOrDefault(v => v.Id == vehicleId);

            if (vehicle == null || !vehicle.Availability) return false;

            return true;
        }

        public void ChangeAvailability(int vehicleId, bool availability)
        {
            var vehicle = _vehicles.SingleOrDefault(v => v.Id == vehicleId);
            if (vehicle == null)
                throw new ArgumentException("Vehicle with given id was not found.", nameof(vehicleId));

            vehicle.Availability = availability;
        }
    }
}
