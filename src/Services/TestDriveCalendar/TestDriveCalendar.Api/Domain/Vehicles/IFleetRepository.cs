﻿using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Domain.Vehicles
{
    public interface IFleetRepository : IRepository<Fleet>
    {
        Task<Fleet> FindByDealerIdAsync(DealerCode dealerId);

        Task<Fleet> FindByVehicleIdAsync(int vehicleId);

        Task<Vehicle> FindVehicleByIdAsync(int vehicleId);
    }
}
