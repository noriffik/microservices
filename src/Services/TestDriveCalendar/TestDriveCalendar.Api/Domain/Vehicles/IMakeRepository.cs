﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Domain.Vehicles
{
    public interface IMakeRepository : IRepository<Make>
    {
        Task<IEnumerable<Make>> List();
    }
}
