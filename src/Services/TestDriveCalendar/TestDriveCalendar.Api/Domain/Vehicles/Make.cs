﻿using System;
using System.Collections.Generic;
using NexCore.Domain;

namespace NexCore.TestDriveCalendar.Api.Domain.Vehicles
{
    public class Make : Entity, IAggregateRoot
    {
        private readonly List<Model> _models = new List<Model>();

        public string Name { get; set; }

        public IEnumerable<Model> Models => _models.AsReadOnly();

        public Make(string name) : this(0, name)
        {
        }

        public Make(int id, string name) : base(id)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Name cannot be null or empty.", nameof(name));

            Name = name;
        }

        public void AddModel(Model model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (_models.Contains(model))
                throw new InvalidOperationException("Model was already added.");

            _models.Add(model);
        }

        public void AddModel(string code, string name)
        {
            AddModel(new Model(code, name));
        }
    }
}
