﻿using System;
using System.Diagnostics.CodeAnalysis;
using NexCore.Domain;

namespace NexCore.TestDriveCalendar.Api.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class Vehicle : Entity
    {
        public Vin Vin { get; private set; }
        public int ModelId { get; private set; }
        public string Equipment { get; set; }
        public bool Availability { get; set; }
        public string Type { get; set; }
        public string Engine { get; set; }
        public string GearBox { get; set; }
        public string Options { get; set; }
        public string Comments { get; set; }

        private Vehicle()
        {
        }

        public Vehicle(Vin vin, int modelId) : this(0, vin, modelId)
        {
        }

        public Vehicle(int id, Vin vin, int modelId) : base(id) 
        {
            if (vin == null)
                throw new ArgumentNullException(nameof(vin));

            if (Equals(0, modelId))
                throw new ArgumentException("Model must be greater than 0", nameof(modelId));

            Vin = vin;
            ModelId = modelId;
            Availability = true;
        }
    }
}
