﻿using System;
using System.Diagnostics.CodeAnalysis;
using NexCore.Domain;

namespace NexCore.TestDriveCalendar.Api.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class Model : Entity
    {
        public string Name { get; set; }
        public string Code { get; private set; }

        public Model(string code, string name) : this(0, code, name)
        {
        }

        public Model(int id, string code, string name) : base(id)
        {
            if (string.IsNullOrEmpty(code))
                throw new ArgumentException("message", nameof(code));

            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Value cannot be null or empty.", nameof(name));

            Code = code;
            Name = name;
        }
    }
}
