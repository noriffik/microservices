﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using NexCore.Domain;

namespace NexCore.TestDriveCalendar.Api.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class Vin : ValueObject
    {
        const int LengthRequired = 17;

        private static readonly Regex Pattern = new Regex("^[a-zA-Z0-9_]*$");

        [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
        private Vin()
        {
        }

        private Vin(string number)
        {
            if (string.IsNullOrEmpty(number))
                throw new ArgumentException("Value cannot be null or empty.", nameof(number));

            if (number.Length != LengthRequired)
                throw new ArgumentException($"Value must {LengthRequired} characters long.", nameof(number));

            if (!Pattern.IsMatch(number))
                throw new ArgumentException("Value must contain only alphanumeric characters.", nameof(number));

            Number = number.ToUpperInvariant();
        }

        public string Number { get; private set; }

        public static Vin Parse(string number)
        {
            return new Vin(number);
        }

        public static bool TryParse(string number, out Vin result)
        {
            try
            {
                result = new Vin(number);
            }
            catch (ArgumentException)
            {
                result = null;

                return false;
            }

            return true;
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return Number;
        }
    }
}
