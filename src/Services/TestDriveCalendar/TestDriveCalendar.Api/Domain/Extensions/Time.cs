﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using NexCore.Domain;

namespace NexCore.TestDriveCalendar.Api.Domain.Extensions
{
    public class Time : ValueObject
    {
        [JsonProperty]
        private readonly int _timestamp;

        private Time()
        {
            
        }

        public Time(int timestamp)
        {
            if (timestamp < 0 || timestamp > 86399) throw new ArgumentOutOfRangeException(nameof(timestamp));
            _timestamp = timestamp;
        }

        public Time(int hours, int minutes, int seconds)
        {
            if (hours < 0 || hours > 23) throw new ArgumentOutOfRangeException(nameof(hours));
            if (minutes < 0 || minutes > 59) throw new ArgumentOutOfRangeException(nameof(minutes));
            if (seconds < 0 || seconds > 59) throw new ArgumentOutOfRangeException(nameof(seconds));
            _timestamp = hours*3600 + minutes*60 + seconds;
        }

        public int Hours => _timestamp / 3600;

        public int Minutes => _timestamp / 60 % 60;

        public int Seconds => _timestamp % 60;

        public static Time Empty => new Time();

        protected override IEnumerable<object> GetValues()
        {
            yield return _timestamp;
        }

        public static bool operator <= (Time since, Time till)
        {
            return GetTimestamp(since) <= GetTimestamp(till);
        }

        public static bool operator >= (Time since, Time till)
        {
            return GetTimestamp(since) >= GetTimestamp(till);
        }

        public static bool operator < (Time since, Time till)
        {
            return GetTimestamp(since) < GetTimestamp(till);
        }

        public static bool operator > (Time since, Time till)
        {
            return GetTimestamp(since) > GetTimestamp(till);
        }

        private static int GetTimestamp(Time time)
        {
            return time != null ? time._timestamp : 0;
        }
    }
}
