﻿using AutoMapper;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Models;

namespace NexCore.TestDriveCalendar.Api
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Dealer, DealerModel>()
                .ForMember(d => d.Id, _ => _.MapFrom(s => s.Id.Value));
        }
    }
}
