﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.TestDriveCalendar.Api.Migrations
{
    public partial class Add_IV_FleetVehicles : Migration
    {
        private const string UpSql = @"
            SET ANSI_NULLS ON
            GO

                SET QUOTED_IDENTIFIER ON
            GO

                CREATE VIEW [testdrive].[VI_Fleet_Vehicles]
            AS
                SELECT testdrive.Fleet.DealerId, testdrive.Vehicle.FleetId, testdrive.Vehicle.VehicleId, testdrive.Vehicle.Vin, testdrive.Vehicle.ModelId, testdrive.Vehicle.Equipment, testdrive.Vehicle.Engine, testdrive.Vehicle.GearBox, testdrive.Vehicle.Options, testdrive.Vehicle.Comments, testdrive.Vehicle.[Availability], testdrive.Make.[Name] AS MakeName, testdrive.Model.Code AS ModelCode, testdrive.Model.[Name] AS ModelName
                FROM testdrive.Vehicle
                INNER JOIN testdrive.Model ON testdrive.Vehicle.ModelId = testdrive.Model.ModelId
                INNER JOIN testdrive.Make ON testdrive.Model.MakeId = testdrive.Make.MakeId
                INNER JOIN testdrive.Fleet ON testdrive.Vehicle.FleetId = testdrive.Fleet.FleetId
            GO";

        private const string DownSql = "DROP VIEW [testdrive].[VI_Fleet_Vehicles]";

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(UpSql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(DownSql);
        }
    }
}
