﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.TestDriveCalendar.Api.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "testdrive");

            migrationBuilder.CreateSequence(
                name: "sq_Make",
                schema: "testdrive",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Make",
                schema: "testdrive",
                columns: table => new
                {
                    MakeId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Make", x => x.MakeId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Make",
                schema: "testdrive");

            migrationBuilder.DropSequence(
                name: "sq_Make",
                schema: "testdrive");
        }
    }
}
