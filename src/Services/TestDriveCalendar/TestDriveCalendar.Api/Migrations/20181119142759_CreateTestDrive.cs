﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.TestDriveCalendar.Api.Migrations
{
    public partial class CreateTestDrive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_TestDrive",
                schema: "testdrive",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "TestDrive",
                schema: "testdrive",
                columns: table => new
                {
                    TestDriveId = table.Column<int>(nullable: false),
                    Firstname = table.Column<string>(nullable: true),
                    Lastname = table.Column<string>(nullable: true),
                    Telephone = table.Column<string>(nullable: true),
                    VehicleId = table.Column<int>(nullable: false),
                    Since = table.Column<DateTime>(nullable: false),
                    Till = table.Column<DateTime>(nullable: false),
                    SalesManagerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestDrive", x => x.TestDriveId);
                    table.ForeignKey(
                        name: "FK_TestDrive_Vehicle_VehicleId",
                        column: x => x.VehicleId,
                        principalSchema: "testdrive",
                        principalTable: "Vehicle",
                        principalColumn: "VehicleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TestDrive_VehicleId",
                schema: "testdrive",
                table: "TestDrive",
                column: "VehicleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TestDrive",
                schema: "testdrive");

            migrationBuilder.DropSequence(
                name: "sq_TestDrive",
                schema: "testdrive");
        }
    }
}
