﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.TestDriveCalendar.Api.Migrations
{
    public partial class Create_TestDrive_StatusChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_StatusChange",
                schema: "testdrive",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "StatusChange",
                schema: "testdrive",
                columns: table => new
                {
                    StatusChangeId = table.Column<int>(nullable: false),
                    AuthorId = table.Column<int>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    TestDriveId = table.Column<int>(nullable: true),
                    StatusId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatusChange", x => x.StatusChangeId);
                    table.ForeignKey(
                        name: "FK_StatusChange_TestDrive_TestDriveId",
                        column: x => x.TestDriveId,
                        principalSchema: "testdrive",
                        principalTable: "TestDrive",
                        principalColumn: "TestDriveId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StatusChange_TestDriveId_Version",
                schema: "testdrive",
                table: "StatusChange",
                columns: new[] { "TestDriveId", "Version" },
                unique: true,
                filter: "[TestDriveId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StatusChange",
                schema: "testdrive");

            migrationBuilder.DropSequence(
                name: "sq_StatusChange",
                schema: "testdrive");
        }
    }
}
