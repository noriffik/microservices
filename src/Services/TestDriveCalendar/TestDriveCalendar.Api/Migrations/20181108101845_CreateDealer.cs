﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.TestDriveCalendar.Api.Migrations
{
    public partial class CreateDealer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_Dealer",
                schema: "testdrive",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Dealer",
                schema: "testdrive",
                columns: table => new
                {
                    DealerId = table.Column<int>(nullable: false),
                    Schedule = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dealer", x => x.DealerId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Dealer",
                schema: "testdrive");

            migrationBuilder.DropSequence(
                name: "sq_Dealer",
                schema: "testdrive");
        }
    }
}
