﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.TestDriveCalendar.Api.Migrations
{
    public partial class _20181203182234_Add_Notes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_Note",
                schema: "testdrive",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Note",
                schema: "testdrive",
                columns: table => new
                {
                    NoteId = table.Column<int>(nullable: false),
                    Content = table.Column<string>(nullable: false),
                    TestDriveId = table.Column<int>(nullable: false),
                    ManagerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Note", x => x.NoteId);
                    table.ForeignKey(
                        name: "FK_Note_TestDrive_TestDriveId",
                        column: x => x.TestDriveId,
                        principalSchema: "testdrive",
                        principalTable: "TestDrive",
                        principalColumn: "TestDriveId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Note_TestDriveId",
                schema: "testdrive",
                table: "Note",
                column: "TestDriveId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Note",
                schema: "testdrive");

            migrationBuilder.DropSequence(
                name: "sq_Note",
                schema: "testdrive");
        }
    }
}
