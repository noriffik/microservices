﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.TestDriveCalendar.Api.Migrations
{
    public partial class AddTestDriveDuration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TestDriveDuration",
                schema: "testdrive",
                table: "Dealer",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TestDriveDuration",
                schema: "testdrive",
                table: "Dealer");
        }
    }
}
