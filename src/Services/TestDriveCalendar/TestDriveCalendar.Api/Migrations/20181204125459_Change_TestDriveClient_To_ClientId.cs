﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.TestDriveCalendar.Api.Migrations
{
    public partial class Change_TestDriveClient_To_ClientId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Firstname",
                schema: "testdrive",
                table: "TestDrive");

            migrationBuilder.DropColumn(
                name: "Lastname",
                schema: "testdrive",
                table: "TestDrive");

            migrationBuilder.DropColumn(
                name: "Telephone",
                schema: "testdrive",
                table: "TestDrive");

            migrationBuilder.AddColumn<int>(
                name: "ClientId",
                schema: "testdrive",
                table: "TestDrive",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClientId",
                schema: "testdrive",
                table: "TestDrive");

            migrationBuilder.AddColumn<string>(
                name: "Firstname",
                schema: "testdrive",
                table: "TestDrive",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Lastname",
                schema: "testdrive",
                table: "TestDrive",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Telephone",
                schema: "testdrive",
                table: "TestDrive",
                nullable: true);
        }
    }
}
