﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.TestDriveCalendar.Api.Migrations
{
    public partial class Change_Dealer_IdType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropSequence(
                name: "sq_Dealer",
                schema: "testdrive");

            migrationBuilder.DropPrimaryKey(
                "PK_Dealer", "Dealer", "testdrive");

            migrationBuilder.AlterColumn<string>(
                name: "DealerId",
                schema: "testdrive",
                table: "Fleet",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "DealerId",
                schema: "testdrive",
                table: "Dealer",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddPrimaryKey(
                "PK_Dealer", "Dealer", "DealerId", "testdrive");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_Dealer",
                schema: "testdrive",
                incrementBy: 10);

            migrationBuilder.DropPrimaryKey(
                "PK_Dealer", "Dealer", "testdrive");

            migrationBuilder.AlterColumn<int>(
                name: "DealerId",
                schema: "testdrive",
                table: "Fleet",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DealerId",
                schema: "testdrive",
                table: "Dealer",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey(
                "PK_Dealer", "Dealer", "DealerId", "testdrive");
        }
    }
}
