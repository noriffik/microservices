﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.TestDriveCalendar.Api.Migrations
{
    public partial class CreateModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_Model",
                schema: "testdrive",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Model",
                schema: "testdrive",
                columns: table => new
                {
                    ModelId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    MakeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Model", x => x.ModelId);
                    table.ForeignKey(
                        name: "FK_Model_Make_MakeId",
                        column: x => x.MakeId,
                        principalSchema: "testdrive",
                        principalTable: "Make",
                        principalColumn: "MakeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Model_MakeId",
                schema: "testdrive",
                table: "Model",
                column: "MakeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Model",
                schema: "testdrive");

            migrationBuilder.DropSequence(
                name: "sq_Model",
                schema: "testdrive");
        }
    }
}
