﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.TestDriveCalendar.Api.Migrations
{
    public partial class CreateFleets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_Fleet",
                schema: "testdrive",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "sq_Vehicle",
                schema: "testdrive",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Fleet",
                schema: "testdrive",
                columns: table => new
                {
                    FleetId = table.Column<int>(nullable: false),
                    DealerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fleet", x => x.FleetId);
                });

            migrationBuilder.CreateTable(
                name: "Vehicle",
                schema: "testdrive",
                columns: table => new
                {
                    VehicleId = table.Column<int>(nullable: false),
                    Vin = table.Column<string>(nullable: true),
                    ModelId = table.Column<int>(nullable: false),
                    Equipment = table.Column<string>(nullable: true),
                    Engine = table.Column<string>(nullable: true),
                    GearBox = table.Column<string>(nullable: true),
                    Options = table.Column<string>(nullable: true),
                    Comments = table.Column<string>(nullable: true),
                    FleetId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicle", x => x.VehicleId);
                    table.ForeignKey(
                        name: "FK_Vehicle_Fleet_FleetId",
                        column: x => x.FleetId,
                        principalSchema: "testdrive",
                        principalTable: "Fleet",
                        principalColumn: "FleetId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Vehicle_Model_ModelId",
                        column: x => x.ModelId,
                        principalSchema: "testdrive",
                        principalTable: "Model",
                        principalColumn: "ModelId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_FleetId",
                schema: "testdrive",
                table: "Vehicle",
                column: "FleetId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_ModelId",
                schema: "testdrive",
                table: "Vehicle",
                column: "ModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Vehicle",
                schema: "testdrive");

            migrationBuilder.DropTable(
                name: "Fleet",
                schema: "testdrive");

            migrationBuilder.DropSequence(
                name: "sq_Fleet",
                schema: "testdrive");

            migrationBuilder.DropSequence(
                name: "sq_Vehicle",
                schema: "testdrive");
        }
    }
}
