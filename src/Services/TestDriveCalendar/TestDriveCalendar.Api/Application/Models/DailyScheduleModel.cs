﻿namespace NexCore.TestDriveCalendar.Api.Application.Models
{
    public class DailyScheduleModel
    {
        public int? WorkingDay { get; set; }
        public WorkingHoursModel WorkingHours { get; set; }
    }
}
