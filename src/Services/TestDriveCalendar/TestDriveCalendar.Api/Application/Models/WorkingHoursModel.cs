﻿namespace NexCore.TestDriveCalendar.Api.Application.Models
{
    public class WorkingHoursModel
    {
        public int Since { get; set; }
        public int Till { get; set; }
    }
}
