﻿using FluentValidation;
using NexCore.TestDriveCalendar.Api.Application.Models;

namespace NexCore.TestDriveCalendar.Api.Application.Validations
{
    public class WorkingHoursValidator : AbstractValidator<WorkingHoursModel>
    {
        public WorkingHoursValidator()
        {
            RuleFor(wh => wh.Since).GreaterThanOrEqualTo(0).LessThan(86400).LessThan(wh => wh.Till).WithMessage("Since must be less than till");
            RuleFor(wh => wh.Till).GreaterThan(0).LessThan(86400).GreaterThan(wh => wh.Since).WithMessage("Till must be greater than since");
        }
    }
}
