﻿using FluentValidation;
using NexCore.TestDriveCalendar.Api.Application.Commands;

namespace NexCore.TestDriveCalendar.Api.Application.Validations
{
    public class RemoveNoteCommandValidator : AbstractValidator<RemoveNoteCommand>
    {
        public RemoveNoteCommandValidator()
        {
            RuleFor(c => c.NoteId).NotEmpty();
            RuleFor(c => c.ManagerId).NotEmpty();
        }
    }
}
