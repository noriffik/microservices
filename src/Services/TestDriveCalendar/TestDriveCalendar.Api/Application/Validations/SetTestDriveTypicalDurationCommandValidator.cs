﻿using FluentValidation;
using NexCore.TestDriveCalendar.Api.Application.Commands;

namespace NexCore.TestDriveCalendar.Api.Application.Validations
{
    public class SetTestDriveTypicalDurationCommandValidator : AbstractValidator<SetTestDriveTypicalDurationCommand>
    {
        public SetTestDriveTypicalDurationCommandValidator()
        {
            RuleFor(c => c.DealerId).NotNull().NotEmpty();
            RuleFor(c => c.TestDriveDuration).NotEmpty().GreaterThan(0);
        }
    }
}
