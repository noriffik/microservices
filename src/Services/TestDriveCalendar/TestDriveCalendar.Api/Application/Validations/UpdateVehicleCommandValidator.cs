﻿using FluentValidation;
using NexCore.TestDriveCalendar.Api.Application.Commands;

namespace NexCore.TestDriveCalendar.Api.Application.Validations
{
    public class UpdateVehicleCommandValidator : AbstractValidator<UpdateVehicleCommand>
    {
        public UpdateVehicleCommandValidator()
        {
            RuleFor(c => c.VehicleId).NotEmpty();
            RuleFor(c => c.Equipment).NotEmpty();
        }
    }
}
