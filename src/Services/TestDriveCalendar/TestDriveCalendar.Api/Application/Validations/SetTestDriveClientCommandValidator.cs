﻿using FluentValidation;
using NexCore.TestDriveCalendar.Api.Application.Commands;

namespace NexCore.TestDriveCalendar.Api.Application.Validations
{
    public class SetTestDriveClientCommandValidator : AbstractValidator<SetTestDriveClientCommand>
    {
        public SetTestDriveClientCommandValidator()
        {
            RuleFor(td => td.TestDriveId).NotEmpty();
            RuleFor(td => td.ClientId).NotEmpty();
        }
        
    }
}
