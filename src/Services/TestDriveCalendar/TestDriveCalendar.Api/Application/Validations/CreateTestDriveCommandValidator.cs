﻿using FluentValidation;
using NexCore.TestDriveCalendar.Api.Application.Commands;

namespace NexCore.TestDriveCalendar.Api.Application.Validations
{
    public class CreateTestDriveCommandValidator  : AbstractValidator<CreateTestDriveCommand>
    {
        public CreateTestDriveCommandValidator()
        {
            RuleFor(td => td.ClientId).NotEmpty();
            RuleFor(td => td.VehicleId).NotEmpty();
            RuleFor(td => td.Since).NotEmpty().LessThan(td => td.Till);
            RuleFor(td => td.Till).NotEmpty().GreaterThan(td => td.Since);
            RuleFor(td => td.SalesManagerId).NotEmpty();
        }
    }
}
