﻿using FluentValidation;
using NexCore.TestDriveCalendar.Api.Application.Commands;

namespace NexCore.TestDriveCalendar.Api.Application.Validations
{
    public class UpdateNoteCommandValidator : AbstractValidator<UpdateNoteCommand>
    {
        public UpdateNoteCommandValidator()
        {
            RuleFor(c => c.NoteId).NotEmpty();
            RuleFor(c => c.ManagerId).NotEmpty();
            RuleFor(c => c.Content).NotEmpty();
        }
    }
}
