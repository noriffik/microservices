﻿using FluentValidation;
using NexCore.TestDriveCalendar.Api.Models;

namespace NexCore.TestDriveCalendar.Api.Application.Validations
{
    public class VehicleItemValidator : AbstractValidator<VehicleItem>
    {
        public VehicleItemValidator()
        {
            RuleFor(i => i.ModelId).NotEqual(0);
            RuleFor(i => i.Vin).MinimumLength(17).MaximumLength(17).NotEmpty().NotNull();
            RuleFor(i => i.DealerId).NotNull().NotEmpty();
            RuleFor(i => i.Equipment).NotNull().NotEmpty();
        }
    }
}
