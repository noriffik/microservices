﻿using FluentValidation;
using NexCore.TestDriveCalendar.Api.Application.Models;

namespace NexCore.TestDriveCalendar.Api.Application.Validations
{
    public class DailyScheduleValidator : AbstractValidator<DailyScheduleModel>
    {
        public DailyScheduleValidator()
        {
            RuleFor(ds => ds.WorkingDay).GreaterThanOrEqualTo(0).LessThanOrEqualTo(6);
            RuleFor(ds => ds.WorkingHours).SetValidator(new WorkingHoursValidator());
        }
    }
}