﻿using System;
using FluentValidation;
using NexCore.TestDriveCalendar.Api.Models;

namespace NexCore.TestDriveCalendar.Api.Application.Validations
{
    public class TestDriveByDealerDateItemValidator : AbstractValidator<TestDriveByDealerDateItem>
    {
        public TestDriveByDealerDateItemValidator()
        {
            RuleFor(i => i.DealerId).NotEmpty();
            RuleFor(i => i.Date).NotEmpty().Must(BeAValidDate);
        }

        private bool BeAValidDate(DateTime date)
        {
            return !date.Equals(default);
        }
    }
}
