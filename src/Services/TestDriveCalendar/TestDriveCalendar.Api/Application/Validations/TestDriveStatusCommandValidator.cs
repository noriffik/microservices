﻿using FluentValidation;
using NexCore.TestDriveCalendar.Api.Application.Commands;

namespace NexCore.TestDriveCalendar.Api.Application.Validations
{
    public class TestDriveStatusCommandValidator : AbstractValidator<TestDriveStatusCommand>
    {
        public TestDriveStatusCommandValidator()
        {
            RuleFor(m => m.TestDriveId).NotEqual(default(int));
            RuleFor(m => m.SalesManagerId).NotEqual(default(int));
        }
    }
}
