﻿using FluentValidation;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using System;

namespace NexCore.TestDriveCalendar.Api.Application.Validations
{
    public class SetTestDriveDurationCommandValidator : AbstractValidator<RescheduleTestDriveCommand>
    {
        public SetTestDriveDurationCommandValidator()
        {
            RuleFor(c => c.TestDriveId).NotEmpty();
            RuleFor(c => c.Since).NotEmpty().LessThan(td => td.Till).Must(BeAValidDate);
            RuleFor(c => c.Till).NotEmpty().GreaterThan(td => td.Since).Must(BeAValidDate);
            RuleFor(c => c.SalesManagerId).NotEmpty();
        }

        private bool BeAValidDate(DateTime date)
        {
            return !date.Equals(default(DateTime));
        }
    }
}
