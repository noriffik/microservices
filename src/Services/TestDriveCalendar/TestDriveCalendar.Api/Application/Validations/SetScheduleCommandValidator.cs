﻿using FluentValidation;
using NexCore.TestDriveCalendar.Api.Application.Commands;

namespace NexCore.TestDriveCalendar.Api.Application.Validations
{
    public class SetScheduleCommandValidator : AbstractValidator<SetScheduleCommand>
    {
        public SetScheduleCommandValidator()
        {
            RuleFor(d => d.DealerId).NotEmpty().WithMessage("DealerId should not be empty");
            RuleForEach(d => d.WeeklySchedule).SetValidator(new DailyScheduleValidator());
        }
    }
}
