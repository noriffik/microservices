﻿using FluentValidation;
using NexCore.TestDriveCalendar.Api.Application.Commands;

namespace NexCore.TestDriveCalendar.Api.Application.Validations
{
    public class CreateNoteCommandValidator : AbstractValidator<CreateNoteCommand>
    {
        public CreateNoteCommandValidator()
        {
            RuleFor(m => m.TestDriveId).NotEqual(default(int));
            RuleFor(m => m.ManagerId).NotEqual(default(int));
            RuleFor(m => m.Content).NotEmpty().NotNull();
        }
    }
}
