﻿using System.Runtime.Serialization;
using NexCore.TestDriveCalendar.Api.Application.Commands.VehicleCommands;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    [DataContract]
    public class UpdateVehicleCommand : IVehicleCommand
    {

        [DataMember]
        public int VehicleId { get; set; }

        [DataMember]
        public string Equipment { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string Engine { get; set; }

        [DataMember]
        public string GearBox { get; set; }

        [DataMember]
        public string Options { get; set; }

        [DataMember]
        public string Comments { get; set; }
    }
}
