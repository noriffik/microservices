﻿using FluentValidation;
using FluentValidation.Results;
using NexCore.TestDriveCalendar.Api.Domain.Notes;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    public class CreateNoteCommandHandler : ICreateNoteCommandHandler
    {
        private readonly INotesRepository _noteRepository;
        private readonly ITestDriveRepository _testDriveRepository;

        public CreateNoteCommandHandler(INotesRepository noteRepository, ITestDriveRepository testDriveRepository)
        {
            _noteRepository = noteRepository ?? throw new ArgumentNullException(nameof(noteRepository));
            _testDriveRepository = testDriveRepository ?? throw new ArgumentNullException(nameof(testDriveRepository));
        }

        public async Task<int> Handle(CreateNoteCommand command, CancellationToken cancellationToken)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            await ThrowIfTestDriveDoesNotExist(command.TestDriveId);

            var note = new Note(command.TestDriveId, command.ManagerId, command.Content);

            _noteRepository.Add(note);

            await _noteRepository.UnitOfWork.Commit(cancellationToken);

            return note.Id;
        }

        private async Task ThrowIfTestDriveDoesNotExist(int testDriveId)
        {
            if(await _testDriveRepository.Has(testDriveId))
                return;

            throw new ValidationException("Failed to handle command.",
                    new[] { new ValidationFailure(nameof(testDriveId), "Test-drive does not exists.", testDriveId), });
        }
    }
}
