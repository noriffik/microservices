﻿using FluentValidation;
using FluentValidation.Results;
using NexCore.TestDriveCalendar.Api.Application.Commands.VehicleCommands;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    public class UpdateVehicleCommandHandler : IUpdateVehicleCommandHandler
    {
        private readonly IFleetRepository _fleetRepository;
        private IVehicleCommandMapper _vehicleCommandMapper;

        public IVehicleCommandMapper VehicleCommandMapper
        {
            get => _vehicleCommandMapper ?? (_vehicleCommandMapper = new VehicleCommandMapper());
            set => _vehicleCommandMapper = value;
        }

        public UpdateVehicleCommandHandler(IFleetRepository fleetRepository)
        {
            _fleetRepository = fleetRepository ?? throw new ArgumentNullException(nameof(fleetRepository));
        }

        public async Task<bool> Handle(UpdateVehicleCommand command, CancellationToken cancellationToken)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            var vehicle = await FindVehicleOrThrow(command.VehicleId);
            VehicleCommandMapper.Map(command, vehicle);

            await _fleetRepository.UnitOfWork.Commit(cancellationToken);

            return true;
        }

        private async Task<Vehicle> FindVehicleOrThrow(int vehicleId)
        {
            var vehicle = await _fleetRepository.FindVehicleByIdAsync(vehicleId);
            if(vehicle == null)
                throw new ValidationException("Failed to handle command.", 
                    new [] {new ValidationFailure(nameof(vehicleId), "Vehicle not found", vehicleId)});

            return vehicle;
        }
    }
}
