﻿using System.Runtime.Serialization;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    [DataContract]
    public class UpdateNoteCommand
    {
        [DataMember]
        public int NoteId { get; set; }

        [DataMember]
        public int ManagerId { get; set; }

        [DataMember]
        public string Content { get; set; }
    }
}
