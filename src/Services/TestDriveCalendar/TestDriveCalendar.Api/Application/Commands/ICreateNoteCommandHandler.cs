﻿using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    public interface ICreateNoteCommandHandler
    {
        Task<int> Handle(CreateNoteCommand command, CancellationToken cancellationToken);
    }
}
