﻿using System.Runtime.Serialization;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    [DataContract]
    public class RemoveNoteCommand
    {
        [DataMember]
        public int NoteId { get; set; }

        [DataMember]
        public int ManagerId { get; set; }
    }
}
