﻿using System.Runtime.Serialization;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    [DataContract]
    public class TestDriveStatusCommand
    {
        [DataMember]
        public int TestDriveId { get; set; }

        [DataMember]
        public int SalesManagerId { get; set; }
    }
}
