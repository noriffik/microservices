﻿using NexCore.TestDriveCalendar.Api.Domain.Vehicles;

namespace NexCore.TestDriveCalendar.Api.Application.Commands.VehicleCommands
{
    public interface IVehicleCommandMapper
    {
        void Map(IVehicleCommand command, Vehicle vehicle);
    }
}
