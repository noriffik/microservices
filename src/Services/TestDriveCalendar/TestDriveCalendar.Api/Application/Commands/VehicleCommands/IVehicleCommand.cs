﻿namespace NexCore.TestDriveCalendar.Api.Application.Commands.VehicleCommands
{
    public interface IVehicleCommand
    {
        string Equipment { get; set; }
        string Type { get; set; }
        string Engine { get; set; }
        string GearBox { get; set; }
        string Options { get; set; }
        string Comments { get; set; }
    }
}
