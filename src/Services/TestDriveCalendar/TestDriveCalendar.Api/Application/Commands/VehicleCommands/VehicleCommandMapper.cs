﻿using System;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;

namespace NexCore.TestDriveCalendar.Api.Application.Commands.VehicleCommands
{
    public class VehicleCommandMapper : IVehicleCommandMapper
    {
        public void Map(IVehicleCommand command, Vehicle vehicle)
        {
            if (command == null) throw new ArgumentNullException(nameof(command));
            if (vehicle == null) throw new ArgumentNullException(nameof(vehicle));

            vehicle.Equipment = command.Equipment;
            vehicle.Type = command.Type;
            vehicle.Engine = command.Engine;
            vehicle.GearBox = command.GearBox;
            vehicle.Options = command.Options;
            vehicle.Comments = command.Comments;
        }
    }
}
