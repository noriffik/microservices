﻿using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    public interface IUpdateVehicleCommandHandler
    {
        Task<bool> Handle(UpdateVehicleCommand command, CancellationToken cancellationToken);
    }
}
