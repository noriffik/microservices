﻿using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    public interface IUpdateNoteCommandHandler
    {
        Task<bool> Handle(UpdateNoteCommand command, CancellationToken cancellationToken);
    }
}
