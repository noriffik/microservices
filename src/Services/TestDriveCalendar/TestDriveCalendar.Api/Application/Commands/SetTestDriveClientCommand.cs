﻿using NexCore.TestDriveCalendar.Api.Application.Models;
using System.Runtime.Serialization;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    [DataContract]
    public class SetTestDriveClientCommand
    {
        [DataMember]
        public int TestDriveId { get; set; }

        [DataMember]
        public int ClientId { get; set; }
    }
}
