﻿using System.Runtime.Serialization;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    [DataContract]
    public class SetTestDriveTypicalDurationCommand
    {
        [DataMember]
        public string DealerId { get; set; }

        [DataMember]
        public int TestDriveDuration { get; set; }
    }
}
