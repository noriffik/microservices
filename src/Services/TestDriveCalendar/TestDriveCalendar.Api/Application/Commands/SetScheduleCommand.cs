﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using NexCore.TestDriveCalendar.Api.Application.Models;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    [DataContract]
    public class SetScheduleCommand
    {
        [DataMember]
        public string DealerId { get; set; }

        [DataMember]
        public ICollection<DailyScheduleModel> WeeklySchedule { get; set; }
    }
}
