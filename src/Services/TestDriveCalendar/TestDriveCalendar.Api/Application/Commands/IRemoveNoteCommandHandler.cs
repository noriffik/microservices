﻿using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    public interface IRemoveNoteCommandHandler
    {
        Task<bool> Handle(RemoveNoteCommand command, CancellationToken cancellationToken);
    }
}
