﻿using System;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    public class OwnerRequiredException : Exception
    {
        public OwnerRequiredException(string message) : base(message)
        { }
    }
}
