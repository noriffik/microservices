﻿using System;
using System.Runtime.Serialization;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    [DataContract]
    public class RescheduleTestDriveCommand
    {
        [DataMember]
        public int TestDriveId { get; set; }

        [DataMember]
        public DateTime Since { get; set; }

        [DataMember]
        public DateTime Till { get; set; }

        [DataMember]
        public int SalesManagerId { get; set; }
    }
}
