﻿using FluentValidation;
using FluentValidation.Results;
using NexCore.TestDriveCalendar.Api.Domain.Notes;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.Api.Application.Commands
{
    public class RemoveNoteCommandHandler : IRemoveNoteCommandHandler
    {
        private readonly INotesRepository _notesRepository;

        public RemoveNoteCommandHandler(INotesRepository notesRepository)
        {
            _notesRepository = notesRepository ?? throw new ArgumentNullException(nameof(notesRepository));
        }

        public async Task<bool> Handle(RemoveNoteCommand command, CancellationToken cancellationToken)
        {
            var note = await FindNoteOrThrow(command.NoteId);
            if (note.ManagerId != command.ManagerId)
                throw new OwnerRequiredException(
                    $"Manager with Id {command.ManagerId} is not owner of note with id {command.NoteId}");
            _notesRepository.Remove(note);

            await _notesRepository.UnitOfWork.Commit(cancellationToken);

            return true;
        }

        private async Task<Note> FindNoteOrThrow(int noteId)
        {
            var note = await _notesRepository.Find(noteId);
            if (note == null)
                throw new ValidationException("Failed to handle command.",
                    new[] { new ValidationFailure(nameof(noteId), "Note not found", noteId), });

            return note;
        }
    }
}
