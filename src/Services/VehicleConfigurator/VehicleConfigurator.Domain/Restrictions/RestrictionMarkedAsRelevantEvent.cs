﻿using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Domain.Restrictions
{
    public class RestrictionMarkedAsRelevantEvent : IEntityEvent
    {
        public int RestrictionId { get; }

        public RestrictionMarkedAsRelevantEvent(int restrictionId)
        {
            RestrictionId = restrictionId;
        }
    }
}