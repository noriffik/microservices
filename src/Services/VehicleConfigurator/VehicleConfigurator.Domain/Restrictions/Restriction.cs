﻿using NexCore.Common;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Restrictions
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local", Justification = "Required for EF core")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required for EF core")]
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required for EF core")]
    public class Restriction : Entity, IAggregateRoot
    {
        private const string DescriptionTemplate = "Restriction for options {0} on {1} for period {2}";

        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        internal DateTime RelevancePeriodFrom;

        internal DateTime? RelevancePeriodTo;

        private string _description;

        public int CatalogId { get; private set; }

        public ModelKey ModelKey { get; private set; }

        public PrNumberSet PrNumbers { get; private set; }

        public virtual Period RelevancePeriod => new Period(RelevancePeriodFrom, RelevancePeriodTo);

        public bool IsEnabled { get; private set; }

        public string Description
        {
            get => _description ?? string.Format(DescriptionTemplate, PrNumbers.AsString, ModelKey, RelevancePeriod);
            set => _description = value;
        }

        protected Restriction()
        {
        }

        public Restriction(int catalogId, ModelKey modelKey, PrNumberSet prNumbers, Period relevancePeriod)
            : this(default(int), catalogId, modelKey, prNumbers, relevancePeriod)
        {
        }

        public Restriction(int id, int catalogId, ModelKey modelKey, PrNumberSet prNumbers, Period relevancePeriod) : base(id)
        {
            CatalogId = catalogId;
            ModelKey = modelKey != null ? modelKey.Value : throw new ArgumentNullException(nameof(modelKey));
            PrNumbers = prNumbers != null ? prNumbers.AsString : throw new ArgumentNullException(nameof(prNumbers));
            RelevancePeriodFrom = relevancePeriod.From.GetValueOrDefault();
            RelevancePeriodTo = relevancePeriod.To;
        }

        public virtual void ChangeRelevancePeriod(Period period)
        {
            var oldIsRelevant = IsRelevant();

            RelevancePeriodFrom = period.From.GetValueOrDefault();
            RelevancePeriodTo = period.To;

            AddRelevanceEvent(oldIsRelevant);
        }

        private void AddRelevanceEvent(bool previousRelevance)
        {
            if (previousRelevance == IsRelevant())
                return;

            if (IsRelevant())
                AddEntityEvent(new RestrictionMarkedAsRelevantEvent(Id));
            else
                AddEntityEvent(new RestrictionMarkedAsIrrelevantEvent(Id));
        }

        public void Enable()
        {
            if (IsEnabled)
                return;

            IsEnabled = true;

            AddEntityEvent(new RestrictionMarkedAsRelevantEvent(Id));
        }

        public void Disable()
        {
            if (!IsEnabled)
                return;

            IsEnabled = false;

            AddEntityEvent(new RestrictionMarkedAsIrrelevantEvent(Id));
        }

        public virtual bool IsRelevant()
        {
            return RelevancePeriod.IsIn(SystemTimeProvider.Instance.Get()) && IsEnabled;
        }
    }
}
