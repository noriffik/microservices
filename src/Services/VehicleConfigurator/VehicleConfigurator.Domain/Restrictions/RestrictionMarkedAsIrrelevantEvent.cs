﻿using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Domain.Restrictions
{
    public class RestrictionMarkedAsIrrelevantEvent : IEntityEvent
    {
        public int RestrictionId { get; }

        public RestrictionMarkedAsIrrelevantEvent(int restrictionId)
        {
            RestrictionId = restrictionId;
        }
    }
}
