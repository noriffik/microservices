﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Restrictions
{
    public class RestrictOptionOffersWhenRelevanceChangedEventHandler :
        IEntityEventHandler<RestrictionMarkedAsRelevantEvent>,
        IEntityEventHandler<RestrictionMarkedAsIrrelevantEvent>
    {
        private readonly IUnitOfWork _work;

        public RestrictOptionOffersWhenRelevanceChangedEventHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Handle(RestrictionMarkedAsRelevantEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            var restriction = await _work.EntityRepository
                .Require<Restriction>(notification.RestrictionId, cancellationToken);

            await EnforceRestrictions(restriction, cancellationToken);
        }

        public async Task Handle(RestrictionMarkedAsIrrelevantEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            var restriction = await _work.EntityRepository
                .Require<Restriction>(notification.RestrictionId, cancellationToken);

            await LiftRestrictions(restriction, cancellationToken);
        }

        private async Task EnforceRestrictions(Restriction restriction, CancellationToken cancellationToken)
        {
            var vehicleOffer = await _work.EntityRepository
                .SingleOrDefault(new VehicleOfferModelKeySpecification(restriction.ModelKey), cancellationToken);

            if (vehicleOffer == null)
                return;

            vehicleOffer.EnforceOptionRestriction(restriction.PrNumbers);
        }

        private async Task LiftRestrictions(Restriction restriction, CancellationToken cancellationToken)
        {
            var vehicleOffer = await _work.EntityRepository
                .SingleOrDefault(new VehicleOfferModelKeySpecification(restriction.ModelKey), cancellationToken);

            if (vehicleOffer == null)
                return;

            vehicleOffer.LiftOptionRestriction(restriction.PrNumbers);
        }
    }
}
