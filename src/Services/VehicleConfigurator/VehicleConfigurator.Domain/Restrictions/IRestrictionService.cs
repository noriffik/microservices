﻿using NexCore.Domain;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Restrictions
{
    public interface IRestrictionService
    {
        Task<Restriction> Add(Restriction restriction);

        Task ChangeRelevancePeriod(int id, Period relevancePeriod);

        Task Enable(int id);

        Task Disable(int id);
    }
}
