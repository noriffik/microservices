﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Restrictions
{
    public class RestrictionService : IRestrictionService
    {
        private readonly IUnitOfWork _work;

        public RestrictionService(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Restriction> Add(Restriction restriction)
        {
            if (restriction == null)
                throw new ArgumentNullException(nameof(restriction));

            await _work.EntityRepository.HasRequired<Catalog>(restriction.CatalogId);

            await _work.EntityRepository.Add(restriction);

            return restriction;
        }

        public async Task ChangeRelevancePeriod(int restrictionId, Period relevancePeriod)
        {
            var restriction = await _work.EntityRepository.Require<Restriction>(restrictionId);

            restriction.ChangeRelevancePeriod(relevancePeriod);
        }

        public async Task Enable(int restrictionId)
        {
            var restriction = await _work.EntityRepository.Require<Restriction>(restrictionId);

            restriction.Enable();
        }

        public async Task Disable(int restrictionId)
        {
            var restriction = await _work.EntityRepository.Require<Restriction>(restrictionId);

            restriction.Disable();
        }
    }
}
