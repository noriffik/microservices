﻿using System;
using System.Linq.Expressions;
using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Domain.Restrictions.Specifications
{
    public class RestrictionByCatalogIdSpecification : Specification<Restriction>
    {
        public int CatalogId { get; }

        public RestrictionByCatalogIdSpecification(int catalogId)
        {
            CatalogId = catalogId;
        }

        public override Expression<Func<Restriction, bool>> ToExpression()
        {
            return restriction => restriction.CatalogId == CatalogId;
        }

        public override string Description => $"Restriction by catalogId {CatalogId}";
    }
}
