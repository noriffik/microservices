﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    public class ModelKeyValidator : IModelKeyValidator
    {
        private readonly IUnitOfWork _work;

        public ModelKeyValidator(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Validate(ModelKey modelKey, CancellationToken cancellationToken)
        {
            if (modelKey == null)
                throw new ArgumentNullException(nameof(modelKey));

            await _work.EntityRepository.HasRequired<Model, ModelId>(modelKey.ModelId, cancellationToken);
            await _work.EntityRepository.HasRequired<Body, BodyId>(modelKey.BodyId, cancellationToken);
            await _work.EntityRepository.HasRequired<Equipment, EquipmentId>(modelKey.EquipmentId, cancellationToken);
            await _work.EntityRepository.HasRequired<Engine, EngineId>(modelKey.EngineId, cancellationToken);
            await _work.EntityRepository.HasRequired<Gearbox, GearboxId>(modelKey.GearboxId, cancellationToken);
        }

        public Task Validate(ModelKey modelKey) => Validate(modelKey, CancellationToken.None);

        public async Task ValidateMany(IEnumerable<ModelKey> modelKeys, CancellationToken cancellationToken)
        {
            if (modelKeys == null)
                throw new ArgumentNullException(nameof(modelKeys));

            var modelKeysList = modelKeys.ToList();

            await _work.EntityRepository.HasManyRequired<Model, ModelId>(
                modelKeysList.Select(k => k.ModelId).Distinct(), cancellationToken);
            await _work.EntityRepository.HasManyRequired<Body, BodyId>(
                modelKeysList.Select(k => k.BodyId).Distinct(), cancellationToken);
            await _work.EntityRepository.HasManyRequired<Equipment, EquipmentId>(
                modelKeysList.Select(k => k.EquipmentId).Distinct(), cancellationToken);
            await _work.EntityRepository.HasManyRequired<Engine, EngineId>(
                modelKeysList.Select(k => k.EngineId).Distinct(), cancellationToken);
            await _work.EntityRepository.HasManyRequired<Gearbox, GearboxId>(
                modelKeysList.Select(k => k.GearboxId).Distinct(), cancellationToken);
        }

        public Task ValidateMany(IEnumerable<ModelKey> modelKeys) => ValidateMany(modelKeys, CancellationToken.None);
    }


}
