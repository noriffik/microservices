﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    public class GearboxService : ModelComponentService<Gearbox, GearboxId>
    {
        public GearboxService(IUnitOfWork work) : base(work)
        {
        }

        protected override async Task ValidateRemoval(Gearbox component)
        {
            await base.ValidateRemoval(component);

            if (await _work.EntityRepository.Has(new VehicleOfferGearboxIdSpecification(component.Id)))
                throw new ComponentInUseException(component.Id, typeof(Gearbox));
        }
    }
}
