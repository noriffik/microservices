﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    public enum FuelType
    {
        Petrol,
        Diesel
    }

    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public class Engine : ModelComponent<EngineId>
    {
        protected Engine()
        {
        }

        public Engine(EngineId id) : base(id)
        {
        }

        public FuelType? FuelType { get; set; }
    }
}
