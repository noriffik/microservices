﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    public class OptionService : ModelComponentService<Option, OptionId>, IOptionService
    {
        public OptionService(IUnitOfWork work) : base(work)
        {
        }

        public async Task AssignToCategory(OptionId optionId, int optionCategoryId)
        {
            if (optionId == null)
                throw new ArgumentNullException(nameof(optionId));

            await _work.EntityRepository.HasRequired<OptionCategory>(optionCategoryId);

            var option = await _work.EntityRepository.Require<Option, OptionId>(optionId);

            option.OptionCategoryId = optionCategoryId;
        }

        protected override async Task ValidateRemoval(Option component)
        {
            await base.ValidateRemoval(component);

            if (await _work.EntityRepository.Has(new VehicleOfferOptionIdSpecification(component.Id)))
                throw new ComponentInUseException(component.Id, typeof(Option));
        }
    }
}
