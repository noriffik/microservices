﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Vehicles.Specifications
{
    public class OptionWithPrNumberSpecification : Specification<Option>
    {
        public PrNumberSet PrNumbers { get; }

        public ModelId ModelId { get; }

        public OptionWithPrNumberSpecification(PrNumberSet prNumbers, ModelId modelId)
        {
            PrNumbers = prNumbers ?? throw new ArgumentNullException(nameof(prNumbers));

            ModelId = modelId ?? throw new ArgumentNullException(nameof(modelId));
        }

        public override Expression<Func<Option, bool>> ToExpression()
        {
            return o => o.ModelId == ModelId && PrNumbers.Has(o.PrNumber);
        }

        public override string Description { get; }
    }
}
