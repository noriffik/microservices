﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    public enum GearBoxCategory
    {
        Mechanical,
        Mechanical4X4,
        Automatic,
        Automatic4X4
    }

    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class Gearbox : ModelComponent<GearboxId>
    {
        public GearboxType Type { get; set; }

        public GearBoxCategory? GearBoxCategory { get; set; }

        protected Gearbox()
        {
        }

        public Gearbox(GearboxId id, GearboxType type) : base(id)
        {
            Type = type;
        }

        public override string ToString()
        {
            return string.IsNullOrEmpty(Name) ? Type.Value : $"{Type}, {Name}";
        }
    }
}
