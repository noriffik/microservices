﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public class Equipment : ModelComponent<EquipmentId>
    {
        protected Equipment()
        {
        }

        public Equipment(EquipmentId id) : base(id)
        {
        }
    }
}
