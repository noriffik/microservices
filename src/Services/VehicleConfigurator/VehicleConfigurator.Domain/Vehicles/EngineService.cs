﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    public class EngineService : ModelComponentService<Engine, EngineId>
    {
        public EngineService(IUnitOfWork work) : base(work)
        {
        }

        protected override async Task ValidateRemoval(Engine component)
        {
            await base.ValidateRemoval(component);

            if (await _work.EntityRepository.Has(new VehicleOfferEngineIdSpecification(component.Id)))
                throw new ComponentInUseException(component.Id, typeof(Engine));
        }
    }
}
