﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    public class ModelService : ComponentService<Model, ModelId>
    {
        public ModelService(IUnitOfWork work) : base(work)
        {
        }

        protected override async Task ValidateRemoval(Model component)
        {
            await base.ValidateRemoval(component);

            if (await _work.EntityRepository.Has(new VehicleOfferModelIdSpecification(component.Id)))
                throw new ComponentInUseException(component.Id, typeof(Model));
        }
    }
}
