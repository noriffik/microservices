﻿using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    public interface IColorService
    {
        Task Add(Color color);
    }
}
