﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    public class ColorService : IColorService
    {
        private readonly IUnitOfWork _work;

        public ColorService(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Add(Color color)
        {
            if (color == null)
                throw new ArgumentNullException(nameof(color));

            await _work.EntityRepository.HasRequired<ColorType, ColorTypeId>(color.TypeId);

            if (await _work.EntityRepository.Has<Color, ColorId>(color.Id))
                throw new DuplicateEntityException(typeof(Color), color.Id);

            await _work.EntityRepository.Add<Color, ColorId>(color);
        }
    }
}
