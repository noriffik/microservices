﻿using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    public interface IColorTypeService
    {
        Task Add(ColorType colorType);
    }
}
