﻿using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    public class OptionCategory : Entity, IAggregateRoot
    {
        public string Name { get; set; }

        public OptionCategory()
        {
        }

        public OptionCategory(string name)
        {
            Name = name;
        }

        public OptionCategory(int id, string name) : this(name)
        {
            Id = id;
        }
    }
}
