﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    public class ColorTypeService : IColorTypeService
    {
        private readonly IUnitOfWork _work;

        public ColorTypeService(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Add(ColorType colorType)
        {
            if (colorType == null)
                throw new ArgumentNullException(nameof(colorType));

            if (await _work.EntityRepository.Has<ColorType, ColorTypeId>(colorType.Id))
                throw new DuplicateEntityException(typeof(ColorType), colorType.Id);

            await _work.EntityRepository.Add<ColorType, ColorTypeId>(colorType);
        }
    }
}
