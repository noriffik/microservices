﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public class Option : ModelComponent<OptionId>
    {
        public int? OptionCategoryId { get; set; }

        public PrNumber PrNumber { get; protected set; }
        
        protected Option()
        {
        }

        public Option(OptionId id) : base(id)
        {
            PrNumber = id.PrNumber;
        }
    }
}
