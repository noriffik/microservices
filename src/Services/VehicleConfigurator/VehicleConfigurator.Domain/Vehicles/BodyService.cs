﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    public class BodyService : ModelComponentService<Body, BodyId>
    {
        public BodyService(IUnitOfWork work) : base(work)
        {
        }

        protected override async Task ValidateRemoval(Body component)
        {
            await base.ValidateRemoval(component);

            if (await _work.EntityRepository.Has(new VehicleOfferBodyIdSpecification(component.Id)))
                throw new ComponentInUseException(component.Id, typeof(Body));
        }
    }
}
