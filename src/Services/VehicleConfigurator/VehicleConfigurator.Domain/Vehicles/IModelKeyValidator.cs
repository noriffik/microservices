﻿using NexCore.Domain.Vehicles;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    public interface IModelKeyValidator
    {
        Task Validate(ModelKey modelKey, CancellationToken cancellationToken);

        Task Validate(ModelKey modelKey);

        Task ValidateMany(IEnumerable<ModelKey> modelKeys, CancellationToken cancellationToken);

        Task ValidateMany(IEnumerable<ModelKey> modelKeys);
    }
}
