﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    public interface IOptionService : IModelComponentService<Option, OptionId>
    {
        Task AssignToCategory(OptionId optionId, int optionCategoryId);
    }
}
