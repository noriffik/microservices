﻿using NexCore.Domain;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public abstract class Component<TId> : Entity<TId>, IAggregateRoot<TId> where TId : EntityId
    {
        protected Component()
        {
        }

        protected Component(TId id) : base(id)
        {
        }
        
        public string Name { get; set; }

        public override string ToString()
        {
            return string.IsNullOrEmpty(Name) ? Id.Value : $"{Id}, {Name}";
        }
    }
}
