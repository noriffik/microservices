﻿using NexCore.Domain;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks
{
    public abstract class ComponentService<TComponent, TId> : IComponentService<TComponent, TId>
        where TComponent : Component<TId>, IAggregateRoot<TId>
        where TId : EntityId
    {
        protected readonly IUnitOfWork _work;

        protected ComponentService(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Add(TComponent component)
        {
            await ValidateAddition(component);

            await _work.EntityRepository.Add<TComponent, TId>(component);
        }

        protected virtual async Task ValidateAddition(TComponent component)
        {
            if (component == null)
                throw new ArgumentNullException(nameof(component));

            if (await _work.EntityRepository.Has<TComponent, TId>(component.Id))
                throw new DuplicateEntityException(typeof(TComponent), component.Id);
        }

        public async Task Delete(TId id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var component = await _work.EntityRepository.SingleOrDefault<TComponent, TId>(id);

            await ValidateRemoval(component);

            await _work.EntityRepository.Remove<TComponent, TId>(component);
        }

        protected virtual Task ValidateRemoval(TComponent component)
        {
            if (component == null)
                throw new InvalidEntityIdException();

            return Task.CompletedTask;
        }
    }
}
