﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public abstract class ModelComponent<TId> : Component<TId>, IModelComponent where TId : EntityId
    {
        protected ModelComponent()
        {
        }
        
        protected ModelComponent(TId id) : base(id)
        {
            ModelId = ModelId.From(id);
        }

        public ModelId ModelId { get; private set; }
    }

    public interface IModelComponent
    {
        ModelId ModelId { get; }
    }
}
