﻿using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks
{
    public interface IModelComponentService<TComponent, TId> : IComponentService<TComponent, TId>
        where TComponent : ModelComponent<TId> 
        where TId : EntityId
    {
    }
}