﻿using NexCore.Domain;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks
{
    public interface IComponentService<TComponent, TId>  
        where TComponent : Component<TId> 
        where TId : EntityId
    {
        Task Add(TComponent component);
        Task Delete(TId id);
    }
}
