﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks
{
    public abstract class ModelComponentService<TComponent, TId> : ComponentService<TComponent, TId>, IModelComponentService<TComponent, TId>
        where TComponent : ModelComponent<TId>, IAggregateRoot<TId>
        where TId : EntityId
    {
        protected ModelComponentService(IUnitOfWork work) : base(work)
        {
        }

        protected override async Task ValidateAddition(TComponent component)
        {
            await base.ValidateAddition(component);

            await _work.EntityRepository.HasRequired<Model, ModelId>(component.ModelId);
        }
    }
}
