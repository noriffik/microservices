﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks
{
    public class ComponentInUseException : Exception
    {
        private const string DefaultMessage = "Component is in use.";
        private const string DefaultExtendedMessage = "Component {0} with id {1} is in use.";

        public EntityId Id { get; set; }

        public Type ComponentType { get; set; }

        public ComponentInUseException() : base(DefaultMessage)
        {
        }

        public ComponentInUseException(string message) : base(message)
        {
        }

        public ComponentInUseException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public ComponentInUseException(string message, EntityId id, Type type, Exception innerException)
            : this(message, innerException)
        {
            Id = id;
            ComponentType = type;
        }

        public ComponentInUseException(string message, EntityId id, Type type)
            : this(message, id, type, null)
        {
        }

        public ComponentInUseException(EntityId id, Type type)
            : this(string.Format(DefaultExtendedMessage, type.Name, id), id, type, null)
        {
        }

    }
}