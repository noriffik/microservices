﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    public class ColorType : Entity<ColorTypeId>, IAggregateRoot<ColorTypeId>
    {
        public string Name { get; set; }

        private ColorType()
        {
        }

        public ColorType(ColorTypeId id, string name) : base(id)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }
    }
}
