﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class Color : Entity<ColorId>, IAggregateRoot<ColorId>
    {
        public System.Drawing.Color Argb { get; set; }

        public string Name { get; set; }

        public ColorTypeId TypeId { get; set; }

        private Color()
        {
        }

        public Color(ColorId id, System.Drawing.Color argb, ColorTypeId typeId, string name) : base(id)
        {
            Argb = argb;
            TypeId = typeId ?? throw new ArgumentNullException(nameof(typeId));
            Name = name;
        }
    }
}
