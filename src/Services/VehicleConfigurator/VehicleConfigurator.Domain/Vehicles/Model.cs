﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public class Model : Component<ModelId>
    {
        protected Model()
        {
        }

        public Model(ModelId id) : base(id)
        {
        }
    }
}
