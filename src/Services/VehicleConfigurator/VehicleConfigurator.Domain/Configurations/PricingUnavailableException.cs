﻿using System;

namespace NexCore.VehicleConfigurator.Domain.Configurations
{
    public class PricingUnavailableException : Exception
    {
        public PricingUnavailableException()
        {
        }

        public PricingUnavailableException(string message) : base(message)
        {
        }

        public PricingUnavailableException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}