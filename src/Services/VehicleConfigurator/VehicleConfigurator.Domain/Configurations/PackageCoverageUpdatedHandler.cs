﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations
{
    public class PackageCoverageUpdatedHandler : IEntityEventHandler<PackageCoverageUpdatedEvent>
    {
        private readonly IUnitOfWork _work;

        public PackageCoverageUpdatedHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Handle(PackageCoverageUpdatedEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            var package = await _work.EntityRepository.Require<Package, PackageId>(notification.PackageId, cancellationToken);
            var configurations = await _work.EntityRepository.Find(new ByPackageIdSpecification(notification.PackageId), cancellationToken);

            foreach (var configuration in configurations)
                if (!package.ModelKeySet.Values.Contains(configuration.ModelKey))
                    configuration.ChangeRelevance(Relevance.Irrelevant);
        }
    }
}
