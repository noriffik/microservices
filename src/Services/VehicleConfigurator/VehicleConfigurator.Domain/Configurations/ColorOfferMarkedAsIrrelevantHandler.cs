﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations
{
    public class ColorOfferMarkedAsIrrelevantHandler : IEntityEventHandler<ColorOfferMarkedAsIrrelevantEvent>
    {
        private readonly IUnitOfWork _work;

        public ColorOfferMarkedAsIrrelevantHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Handle(ColorOfferMarkedAsIrrelevantEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(notification.VehicleOfferId, cancellationToken);

            var specification = new ByColorOfferSpecification(vehicleOffer.CatalogId, vehicleOffer.ModelKey, notification.ColorId);
            var configurations = await _work.EntityRepository.Find(specification, cancellationToken);

            foreach (var configuration in configurations)
                configuration.ChangeRelevance(Relevance.Irrelevant);
        }
    }
}
