﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations
{
    public class MarkConfigurationAsIrrelevantWhenVehicleOfferIsUnavailableHandler :
        IEntityEventHandler<VehicleOfferMarkedAsIrrelevantEvent>,
        IEntityEventHandler<VehicleOfferUnpublishedEvent>
    {
        private readonly IUnitOfWork _work;

        public MarkConfigurationAsIrrelevantWhenVehicleOfferIsUnavailableHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public Task Handle(VehicleOfferMarkedAsIrrelevantEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            return InnerHandle(notification.VehicleOfferId, cancellationToken);
        }

        public Task Handle(VehicleOfferUnpublishedEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            return InnerHandle(notification.VehicleOfferId, cancellationToken);
        }

        private async Task InnerHandle(int vehicleOfferId, CancellationToken cancellationToken)
        {
            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(vehicleOfferId, cancellationToken);
            var configurations = await _work.EntityRepository.Find(
                new ByVehicleOfferSpecification(vehicleOffer.CatalogId, vehicleOffer.ModelKey), cancellationToken);

            foreach (var configuration in configurations)
                configuration.ChangeRelevance(Relevance.Irrelevant);
        }
    }
}
