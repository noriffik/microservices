﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Rules;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Rules
{
    public interface IConfigurationValidator : IConfigurationValidator<VehicleOffer>
    {
    }

    public interface IConfigurationValidator<TAgainst> where TAgainst : class
    {
        void Validate(PrNumberSet configuration, TAgainst against);

        bool IsValid(PrNumberSet configuration, TAgainst against);

        IEnumerable<RuleViolation> GetErrors(PrNumberSet configuration, TAgainst against);
    }
}