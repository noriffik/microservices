﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Rules;
using NexCore.VehicleConfigurator.Domain.Rules.Expressions;
using System;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Rules
{
    public class RuleViolationMapper : BaseMapper<Tuple<PrNumber, PrNumberSetRule>, RuleViolation>
    {
        private IRuleMessageComposer _messageComposer;

        public IRuleMessageComposer MessageComposer
        {
            get => _messageComposer ?? (_messageComposer = new RuleMessageComposer());
            set => _messageComposer = value ?? throw new ArgumentNullException(nameof(value));
        }

        public override RuleViolation Map(Tuple<PrNumber, PrNumberSetRule> source)
        {
            var (prNumber, expression) = source ?? throw new ArgumentNullException(nameof(source));

            return new RuleViolation
            {
                ForPrNumber = prNumber.Value,
                RuleString = expression.ToString(),
                RuleName = expression.RuleName,
                PrNumbers = expression.PrNumbers.AsString,
                Message = MessageComposer.Compose(expression.RuleName, expression.PrNumbers.AsString)
            };
        }
    }
}