﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Rules;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Rules
{
    public abstract class BaseConfigurationValidator<TAgainst> : IConfigurationValidator<TAgainst> where TAgainst : class
    {
        public virtual void Validate(PrNumberSet configuration, TAgainst against)
        {
            var errors = GetErrors(configuration, against).ToList();
            if (!errors.Any())
                return;

            throw new UnavailableOptionException
            {
                Details = errors
            };
        }

        public virtual bool IsValid(PrNumberSet configuration, TAgainst against)
        {
            var errors = GetErrors(configuration, against).ToList();
            if (!errors.Any())
                return true;

            return false;
        }

        public abstract IEnumerable<RuleViolation> GetErrors(PrNumberSet configuration, TAgainst against);
    }
}