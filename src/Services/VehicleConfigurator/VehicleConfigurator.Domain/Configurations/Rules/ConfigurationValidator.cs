﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Rules;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Rules
{
    public class ConfigurationValidator : BaseConfigurationValidator<VehicleOffer>, IConfigurationValidator
    {   
        private IConfigurationValidator<OptionOffer> _perOptionValidator;

        public IConfigurationValidator<OptionOffer> PerOptionValidator
        {
            get => _perOptionValidator ?? (_perOptionValidator = new ConfigurationPerOptionValidator());
            set => _perOptionValidator = value ?? throw new ArgumentNullException(nameof(value));
        }
        
        public override IEnumerable<RuleViolation> GetErrors(PrNumberSet configuration, VehicleOffer against)
        {
            var results = new List<RuleViolation>();

            var includedInVehicleOffer = against.Options
                .Where(o => o.Availability.Type == AvailabilityType.Included)
                .Select(o => o.PrNumber);

            var prNumbers = configuration.Expand(includedInVehicleOffer);

            foreach (var prNumber in prNumbers.Values)
            {
                var optionOffer = against.FindOptionOffer(prNumber);
                if (optionOffer == null)
                    throw new UnavailableOptionException();
                
                results.AddRange(PerOptionValidator.GetErrors(prNumbers, optionOffer));
            }

            return results;
        }
    }
}
