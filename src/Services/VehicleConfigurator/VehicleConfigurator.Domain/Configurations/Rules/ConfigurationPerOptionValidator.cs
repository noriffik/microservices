﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Rules;
using NexCore.VehicleConfigurator.Domain.Rules.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Rules
{
    public class ConfigurationPerOptionValidator : BaseConfigurationValidator<OptionOffer>
    {
        private IRuleParser _ruleParser;
        private IMapper<Tuple<PrNumber, PrNumberSetRule>, RuleViolation> _mapper;

        public IRuleParser RuleParser
        {
            get => _ruleParser ?? (_ruleParser = new RuleParser());
            set => _ruleParser = value ?? throw new ArgumentNullException(nameof(value));
        }
        
        public IMapper<Tuple<PrNumber, PrNumberSetRule>, RuleViolation> Mapper
        {
            get => _mapper ?? (_mapper = new RuleViolationMapper());
            set => _mapper = value ?? throw new ArgumentNullException(nameof(value));
        }
        
        public override IEnumerable<RuleViolation> GetErrors(PrNumberSet configuration, OptionOffer against)
        {
            return Mapper.MapMany(GetViolatedRules(configuration, against));
        }
        
        private IEnumerable<Tuple<PrNumber, PrNumberSetRule>> GetViolatedRules(PrNumberSet configuration, OptionOffer against)
        {
            if (string.IsNullOrEmpty(against.RuleSet))
                return new List<Tuple<PrNumber, PrNumberSetRule>>();

            var context = new RuleContext(against.RuleSet, configuration.AsString);

            var expressions = RuleParser.Parse(context.RuleSet);

            return expressions.Where(e => !e.Check(context))
                .Select(e => new Tuple<PrNumber, PrNumberSetRule>(against.PrNumber, e));
        }
    }
}