﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations
{
    public interface IConfigurationService
    {
        Task<Configuration> Add(int catalogId, ModelKey modelKey);

        Task<Configuration> Build(Year modelYear, ConfigurationSpecification specification);

        Task<Configuration> Copy(int configurationId);

        Task ChangeStatus(int configurationId, ConfigurationStatus status);

        Task IncludeOption(int configurationId, PrNumber prNumber);

        Task ExcludeOption(int configurationId, PrNumber prNumber);

        Task IncludePackage(int configurationId, PackageCode code);

        Task ExcludePackage(int configurationId);
        
        Task ChangeColor(int configurationId, ColorId colorId);

        Task Remove(int configurationId);
    }
}
