﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Specifications
{
    public class ByColorIdSpecification : Specification<Configuration>
    {
        public ColorId ColorId { get; }

        public ByColorIdSpecification(ColorId colorId)
        {
            ColorId = colorId ?? throw new ArgumentNullException(nameof(colorId));
        }

        public override Expression<Func<Configuration, bool>> ToExpression()
        {
            return configuration => Convert.ToString(configuration.ColorId) == ColorId.Value;
        }

        public override string Description => $"Configuration in color with id {ColorId}";
    }
}
