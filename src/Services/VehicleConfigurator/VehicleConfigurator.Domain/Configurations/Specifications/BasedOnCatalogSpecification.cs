﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Specifications
{
    public class BasedOnCatalogSpecification : Specification<Configuration>
    {
        public int CatalogId { get; }

        public BasedOnCatalogSpecification(int catalogId)
        {
            CatalogId = catalogId;
        }

        public BasedOnCatalogSpecification(Catalog catalog)
        {
            if (catalog == null)
                throw new ArgumentNullException(nameof(catalog));

            CatalogId = catalog.Id;
        }

        public override Expression<Func<Configuration, bool>> ToExpression()
        {
            return configuration => configuration.CatalogId == CatalogId;
        }

        public override string Description => $"Configuration in Catalog {CatalogId}";
    }
}
