﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Specifications
{
    public class IncludedOptionSpecification : Specification<Configuration>
    {
        public PrNumberSet PrNumbers { get; }

        public IncludedOptionSpecification(PrNumber prNumber)
        {
            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));

            PrNumbers = new PrNumberSet(prNumber);
        }

        public IncludedOptionSpecification(PrNumberSet prNumberSet)
        {
            PrNumbers = prNumberSet ?? throw new ArgumentNullException(nameof(prNumberSet));
        }

        public override Expression<Func<Configuration, bool>> ToExpression()
        {
            return configuration => PrNumbers.HasExactlyOne(configuration.Options);
        }

        public override string Description => $"Configuration included one of more options with prNumbers {PrNumbers.Values}";
    }
}
