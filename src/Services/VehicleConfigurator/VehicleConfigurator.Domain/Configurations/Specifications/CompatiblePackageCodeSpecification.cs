﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Packages;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Specifications
{
    public class CompatiblePackageCodeSpecification : Specification<Package>
    {
        public PackageCode Code { get; }

        public Configuration Configuration { get; }

        private readonly Specification<Package> _inner;

        public CompatiblePackageCodeSpecification(PackageCode code, Configuration configuration)
        {
            Code = code ?? throw new ArgumentNullException(nameof(code));
            Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));

            _inner = new AndSpecification<Package>(
                new CompatiblePackageSpecification(Configuration),
                new PackageCodeSpecification(code));
        }

        public override Expression<Func<Package, bool>> ToExpression()
        {
            return _inner.ToExpression();
        }

        public override string Description =>
            $@"Published Package with code ""{Code.Value}"" for vehicle {Configuration.ModelKey.Value} in Catalog {Configuration.CatalogId}";
    }
}
