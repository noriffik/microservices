﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Specifications
{
    public class ByModelKeySpecification : Specification<Configuration>
    {
        public ModelKey ModelKey { get; }

        public ByModelKeySpecification(ModelKey modelKey)
        {
            ModelKey = modelKey ?? throw new ArgumentNullException(nameof(modelKey));
        }

        public override Expression<Func<Configuration, bool>> ToExpression()
        {
            return configuration => Convert.ToString(configuration.ModelKey) == ModelKey.Value;
        }

        public override string Description => $"Configuration for vehicle with modelKey {ModelKey}";
    }
}
