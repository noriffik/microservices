﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Specifications
{
    public class ByVehicleOfferWithIncludedOptionSpecification : Specification<Configuration>
    {
        public int CatalogId { get; }

        public ModelKey ModelKey { get; }

        public PrNumberSet PrNumberSet { get; }

        private readonly Specification<Configuration> _inner;

        public ByVehicleOfferWithIncludedOptionSpecification(int catalogId, ModelKey modelKey, PrNumberSet prNumberSet)
        {
            CatalogId = catalogId;
            ModelKey = modelKey ?? throw new ArgumentNullException(nameof(modelKey));
            PrNumberSet = prNumberSet ?? throw new ArgumentNullException(nameof(prNumberSet));

            _inner = new AndSpecification<Configuration>(
                new ByVehicleOfferSpecification(CatalogId, ModelKey),
                new IncludedOptionSpecification(prNumberSet));
        }

        public override Expression<Func<Configuration, bool>> ToExpression() => _inner.ToExpression();

        public override string Description =>
            $"Configuration for vehicle with modelKey {ModelKey.Value} in catalog with id {CatalogId} " +
            $"included on of more options with prNumbers {PrNumberSet}";
    }
}
