﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Packages;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Specifications
{
    public class CompatiblePackageSpecification : Specification<Package>
    {
        public Configuration Configuration { get; }

        private readonly Specification<Package> _inner;

        public CompatiblePackageSpecification(Configuration configuration)
        {
            Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));

            _inner = new AndSpecification<Package>(
                new AndSpecification<Package>(
                    new PackageFromCatalogSpecification(Configuration.CatalogId),
                    new PackageStatusSpecification(PackageStatus.Published)),
                new PackageApplicableToVehicleSpecification(Configuration.ModelKey));
        }

        public override Expression<Func<Package, bool>> ToExpression()
        {
            return _inner.ToExpression();
        }

        public override string Description =>
            $"Published Package for vehicle {Configuration.ModelKey.Value} in Catalog {Configuration.CatalogId}";
    }
}
