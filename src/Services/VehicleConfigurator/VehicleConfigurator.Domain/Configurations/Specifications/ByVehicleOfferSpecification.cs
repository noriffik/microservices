﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Specifications
{
    public class ByVehicleOfferSpecification : Specification<Configuration>
    {
        public int CatalogId { get; }

        public ModelKey ModelKey { get; }

        private readonly Specification<Configuration> _inner;

        public ByVehicleOfferSpecification(int catalogId, ModelKey modelKey)
        {
            CatalogId = catalogId;
            ModelKey = modelKey ?? throw new ArgumentNullException(nameof(modelKey));

            _inner = new AndSpecification<Configuration>(
                new BasedOnCatalogSpecification(CatalogId),
                new ByModelKeySpecification(ModelKey));
        }

        public override Expression<Func<Configuration, bool>> ToExpression() => _inner.ToExpression();

        public override string Description =>
            $"Configuration for vehicle with modelKey {ModelKey.Value} in catalog with id {CatalogId}";
    }
}
