﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Specifications
{
    public class ByColorOfferSpecification : Specification<Configuration>
    {
        public int CatalogId { get; }

        public ModelKey ModelKey { get; }

        public ColorId ColorId { get; }

        private readonly Specification<Configuration> _inner;

        public ByColorOfferSpecification(int catalogId, ModelKey modelKey, ColorId colorId)
        {
            CatalogId = catalogId;
            ModelKey = modelKey ?? throw new ArgumentNullException(nameof(modelKey));
            ColorId = colorId ?? throw new ArgumentNullException(nameof(colorId));

            _inner = new AndSpecification<Configuration>(
                new ByVehicleOfferSpecification(CatalogId, ModelKey),
                new ByColorIdSpecification(ColorId));
        }

        public override Expression<Func<Configuration, bool>> ToExpression() => _inner.ToExpression();

        public override string Description =>
            $"Configuration for vehicle with modelKey {ModelKey.Value} in catalog with id {CatalogId} " +
            $"in Color with id {ColorId}";
    }
}
