﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Specifications
{
    public class ByPackageIdSpecification : Specification<Configuration>
    {
        public PackageId PackageId { get; }

        public ByPackageIdSpecification(PackageId packageId)
        {
            PackageId = packageId ?? throw new ArgumentNullException(nameof(packageId));
        }

        public override Expression<Func<Configuration, bool>> ToExpression()
        {
            return configuration => Convert.ToString(configuration.PackageId) == PackageId.Value;
        }

        public override string Description => $"Configuration with package {PackageId.Value}";
    }
}
