﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Configurations.Rules;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations
{
    public class ConfigurationValidationService : IConfigurationValidationService
    {
        private readonly IUnitOfWork _work;
        private IConfigurationValidator _validator;

        public IConfigurationValidator Validator
        {
            get => _validator ?? (_validator = new ConfigurationValidator());
            set => _validator = value ?? throw new ArgumentNullException(nameof(value));
        }

        public ConfigurationValidationService(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        private static PublishedVehicleOfferSpecification VehicleOfferFor(Configuration configuration) =>
            VehicleOfferFor(configuration.CatalogId, configuration.ModelKey);

        private static PublishedVehicleOfferSpecification VehicleOfferFor(int catalogId, ModelKey modelKey) =>
            new PublishedVehicleOfferSpecification(catalogId, modelKey);

        private static RelevantCatalogPublishedForModelYearSpecification CatalogFor(Year modelYear) =>
            new RelevantCatalogPublishedForModelYearSpecification(modelYear);

        private static void CheckForOptionAvailability(PrNumber prNumber, VehicleOffer vehicleOffer)
        {
            var optionOffer = vehicleOffer.FindOptionOffer(prNumber);

            if (optionOffer == null)
                throw new UnavailableOptionException();

            if (optionOffer.Relevance == Relevance.Irrelevant)
                throw new ItemOfferIrrelevantException(typeof(OptionOffer));
        }

        public async Task ValidateOptionInclusion(Configuration configuration, PrNumber prNumber)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));

            var vehicleOffer = await _work.EntityRepository.Require(VehicleOfferFor(configuration));

            CheckForOptionAvailability(prNumber, vehicleOffer);

            Validator.Validate(configuration.Options.Expand(prNumber), vehicleOffer);
        }

        public async Task ValidateConfiguration(Year modelYear, ModelKey modelKey, PrNumberSet prNumbers)
        {
            if (modelYear == null)
                throw new ArgumentNullException(nameof(modelYear));

            if (modelKey == null)
                throw new ArgumentNullException(nameof(modelKey));

            if (prNumbers == null)
                throw new ArgumentNullException(nameof(prNumbers));

            var catalog = await _work.EntityRepository.Require(CatalogFor(modelYear));
            
            var vehicleOffer = await _work.EntityRepository.Require(VehicleOfferFor(catalog.Id, modelKey));
            
            Validator.Validate(prNumbers, vehicleOffer);
        }

        public async Task ValidateThatOptionsAreAvailable(Configuration configuration, PrNumberSet prNumbers)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (prNumbers == null)
                throw new ArgumentNullException(nameof(prNumbers));

            var vehicleOffer = await _work.EntityRepository.Require(VehicleOfferFor(configuration));
            
            foreach (var prNumber in prNumbers.Values)
            {
                CheckForOptionAvailability(prNumber, vehicleOffer);
            }

            Validator.Validate(configuration.Options.Expand(prNumbers), vehicleOffer);
        }

        public async Task VerifyThatOptionIsNotInPackage(PrNumber prNumber, Configuration configuration)
        {
            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));

            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (!configuration.IsPackageIncluded)
                return;

            var package = await _work.EntityRepository.Require<Package, PackageId>(configuration.PackageId);

            VerifyThatPackageIsApplicable(package, configuration.Options.Shrink(prNumber));
        }

        public async Task VerifyThatOptionIsNotRequired(Configuration configuration, PrNumber prNumber)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));

            var vehicleOffer = await _work.EntityRepository.Require(VehicleOfferFor(configuration));
            
            if (!vehicleOffer.HasOptionOffer(prNumber))
                throw new UnavailableOptionException();

            Validator.Validate(configuration.Options.Shrink(prNumber), vehicleOffer);
        }
        
        public void VerifyThatPackageIsApplicable(Package package, PrNumberSet prNumbers)
        {
            if (package.IsApplicableTo(prNumbers))
                return;

            throw new PackageRequirementViolationException(package, prNumbers);
        }

        public async Task ValidateColorAvailability(Configuration configuration, ColorId colorId)
        {
            if (configuration == null) throw new ArgumentNullException(nameof(configuration));

            if (colorId == null) throw new ArgumentNullException(nameof(colorId));

            var vehicleOffer = await _work.EntityRepository.Require(VehicleOfferFor(configuration));
            
            var colorOffer = vehicleOffer.FindColorOffer(colorId);

            if (colorOffer == null)
                throw new ColorOfferNotFoundException(vehicleOffer.ModelKey, colorId);

            if (colorOffer.Relevance == Relevance.Irrelevant)
                throw new ItemOfferIrrelevantException(typeof(ColorOffer));
        }
    }
}
