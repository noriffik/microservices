﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleConfigurator.Domain.Configurations
{
    public class ItemOfferIrrelevantException : DomainException
    {
        private const string DefaultMessage = "Adding irrelevant item to configuration is not allowed";
        private const string DefaultExtendedMessage = "Adding irrelevant {0} to configuration is not allowed";

        public Type ComponentType { get; }

        public ItemOfferIrrelevantException() : base(DefaultMessage)
        {
        }

        public ItemOfferIrrelevantException(string message) : base(message)
        {
        }

        public ItemOfferIrrelevantException(string message, Exception inner) : base(message, inner)
        {
        }

        public ItemOfferIrrelevantException(Type componentType)
            : this(string.Format(DefaultExtendedMessage, componentType.Name), componentType)
        {
        }

        public ItemOfferIrrelevantException(string message, Type componentType)
            : this(message, componentType, null)
        {
        }

        public ItemOfferIrrelevantException(string message, Type componentType, Exception inner)
            : base(message, inner)
        {
            ComponentType = componentType;
        }
    }
}
