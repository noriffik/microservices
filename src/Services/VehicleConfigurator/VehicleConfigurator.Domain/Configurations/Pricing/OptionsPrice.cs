﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Pricing;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("NexCore.VehicleConfigurator.UnitTests")]
namespace NexCore.VehicleConfigurator.Domain.Configurations.Pricing
{
    public class OptionsPrice
    {
        public static readonly OptionsPrice Empty = Create(new Dictionary<PrNumber, Price>());

        public IReadOnlyDictionary<PrNumber, Price> PerOption { get; }

        public Price Total { get; internal set; }
        
        public OptionsPrice(IReadOnlyDictionary<PrNumber, Price> perOption, Price total)
        {
            PerOption = perOption;
            Total = total;
        }

        public static OptionsPrice Create(IReadOnlyDictionary<PrNumber, Price> perOption)
        {
            var total = perOption.Any() ? perOption.Values.Aggregate((a, b) => a + b) : Price.Empty;

            return new OptionsPrice(perOption, total);
        }
    }
}