﻿using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Pricing.Packages
{
    public class MostExpensiveOptionFilter : ApplicableOptionFilter
    {
        public override IEnumerable<OptionOffer> Apply(IApplicabilitySpecification specification, IEnumerable<OptionOffer> options)
        {
            return base.Apply(specification, options)
                .OrderByDescending(o => o.Availability.Price)
                .Take(1);
        }
    }
}