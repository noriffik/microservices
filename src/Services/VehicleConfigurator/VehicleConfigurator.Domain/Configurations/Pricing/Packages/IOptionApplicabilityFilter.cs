﻿using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Pricing.Packages
{
    public interface IOptionApplicabilityFilter
    {
        IEnumerable<OptionOffer> Apply(IApplicabilitySpecification specification, IEnumerable<OptionOffer> options);

        IEnumerable<OptionOffer> Apply(Applicability applicability, IEnumerable<OptionOffer> options);
    }
}