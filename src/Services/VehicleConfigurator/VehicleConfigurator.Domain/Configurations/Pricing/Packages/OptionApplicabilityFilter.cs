﻿using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Pricing.Packages
{
    public abstract class OptionApplicabilityFilter : IOptionApplicabilityFilter
    {
        public abstract IEnumerable<OptionOffer> Apply(IApplicabilitySpecification specification, IEnumerable<OptionOffer> options);

        public IEnumerable<OptionOffer> Apply(Applicability applicability, IEnumerable<OptionOffer> options)
        {
            return applicability.SelectMany(s => Apply(s, options))
                .Distinct();
        }
    }
}