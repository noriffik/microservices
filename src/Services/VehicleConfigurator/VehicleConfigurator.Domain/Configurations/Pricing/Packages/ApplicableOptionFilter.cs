﻿using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Pricing.Packages
{
    public class ApplicableOptionFilter : OptionApplicabilityFilter
    {
        public override IEnumerable<OptionOffer> Apply(IApplicabilitySpecification specification, IEnumerable<OptionOffer> options)
            => options.Where(o => specification.PrNumbers.Has(o.PrNumber));
    }
}