﻿using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing.Packages;
using NexCore.VehicleConfigurator.Domain.Pricing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Pricing
{
    public class PackagePricingService : IPackagePricingService
    {
        private readonly IOptionPricingService _pricingService;

        public IOptionApplicabilityFilter ApplicableOptionFilter { get; set; } = new ApplicableOptionFilter();

        public IOptionApplicabilityFilter WithoutPackageOptionFilter { get; set; } = new MostExpensiveOptionFilter();

        public PackagePricingService(IOptionPricingService pricingService)
        {
            _pricingService = pricingService ?? throw new ArgumentNullException(nameof(pricingService));
        }

        public virtual Task<PackagePrice> Calculate(IEnumerable<OptionOffer> optionOffers, Package package, DateTime date)
        {
            if (optionOffers == null)
                throw new ArgumentNullException(nameof(optionOffers));

            return Calculate(optionOffers, package, WithoutPackageOptionFilter, date);
        }

        public virtual async Task<PackagePrice> Calculate(IEnumerable<OptionOffer> optionOffers,
            Package package, IOptionApplicabilityFilter withoutPackageFilter, DateTime date)
        {
            if (optionOffers == null)
                throw new ArgumentNullException(nameof(optionOffers));

            if (package == null)
                return PackagePrice.Empty;

            var price = await _pricingService.CreatePrice(package.Price, date);
            var options = await CalculateApplicableOptionsPrice(optionOffers, package, date);
            var without = await CalculateWithoutPrice(optionOffers, package, withoutPackageFilter, date);

            return new PackagePrice(price, without, options);
        }

        private async Task<Price> CalculateWithoutPrice(IEnumerable<OptionOffer> optionOffers, Package package, IOptionApplicabilityFilter withoutPackageFilter, DateTime date)
        {
            var filtered = withoutPackageFilter.Apply(package.Applicability, optionOffers);

            var prices = await _pricingService.Calculate(filtered, date);

            return prices.Total;
        }

        private async Task<OptionsPrice> CalculateApplicableOptionsPrice(IEnumerable<OptionOffer> optionOffers, Package package, DateTime date)
        {
            var filtered = ApplicableOptionFilter.Apply(package.Applicability, optionOffers);

            return await _pricingService.Calculate(filtered, date);
        }

        public virtual async Task<IDictionary<PackageId, PackagePrice>> CalculateMany(IEnumerable<Package> packages, IEnumerable<OptionOffer> optionOffers, DateTime date)
        {
            if (packages == null)
                throw new ArgumentNullException(nameof(packages));

            if (optionOffers == null)
                throw new ArgumentNullException(nameof(optionOffers));

            var pp = packages.ToArray();
            var calculations = pp.Select(p => Calculate(optionOffers, p, date)).ToList();

            var prices = new List<PackagePrice>();
            foreach (var calculation in calculations)
            {
                prices.Add(await calculation);
            }

            var result = new Dictionary<PackageId, PackagePrice>();
            for (var i = 0; i < pp.Length; i++)
            {
                result[pp[i].Id] = prices[i];
            }

            return result;
        }
    }
}