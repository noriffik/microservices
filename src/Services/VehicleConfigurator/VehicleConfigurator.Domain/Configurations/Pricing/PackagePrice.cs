﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Pricing;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("NexCore.VehicleConfigurator.UnitTests")]
namespace NexCore.VehicleConfigurator.Domain.Configurations.Pricing
{
    public class PackagePrice
    {
        public static readonly PackagePrice Empty = new PackagePrice(Price.Empty, Price.Empty, OptionsPrice.Empty);

        public OptionsPrice Options { get; internal set; }

        public Price Price { get; internal set; }

        public Price Without { get; internal set; }

        public Price Savings => Without - Price;

        public PackagePrice(Price price, Price without, OptionsPrice options)
        {
            Price = price;
            Without = without;
            Options = options;
        }

        public static PackagePrice Create(Price price, Price without, IReadOnlyDictionary<PrNumber, Price> perOption)
        {
            return new PackagePrice(price, without, OptionsPrice.Create(perOption));
        }
    }
}