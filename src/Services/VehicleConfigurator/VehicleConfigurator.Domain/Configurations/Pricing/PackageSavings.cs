﻿using NexCore.VehicleConfigurator.Domain.Pricing;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Pricing
{
    public class PackageSavings
    {
        public Price Without { get; }

        public Price With { get; }

        public Price Savings { get; }

        public PackageSavings(Price without, Price with, Price savings)
        {
            Without = without;
            With = with;
            Savings = savings;
        }

        public static PackageSavings Create(Price basePrice, OptionsPrice options, PackagePrice package)
        {
            var without = basePrice + options.Total;
            var with = without - package.Without + package.Price;

            return new PackageSavings(without, with, package.Savings);
        }
    }
}