﻿using NexCore.VehicleConfigurator.Domain.Pricing;
using System;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Pricing
{
    public class ConfigurationPrice
    {
        public DateTime OnDate { get; }

        public PackageSavings PackageSavings { get; }

        public OptionsPrice Options { get; }

        public PackagePrice Package { get; }

        public Price Vehicle { get; }

        public Price Color { get; }

        public Price Total => PackageSavings.With;

        public ConfigurationPrice(Price vehicle, Price color, OptionsPrice options, PackagePrice package, DateTime onDate)
        {
            var basePrice = vehicle + color;

            PackageSavings = PackageSavings.Create(basePrice, options, package);
            Package = package;
            Vehicle = vehicle;
            Color = color;
            Options = options;
            OnDate = onDate;
        }
    }
}
