﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Pricing;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Pricing
{
    public interface IOptionPricingService : IPricingService
    {
        Task<OptionsPrice> Calculate(IEnumerable<OptionOffer> optionOffers, DateTime date);
        
        Task<Dictionary<PrNumber, Price>> CreatePrice(Dictionary<PrNumber, decimal> basePrices, DateTime date);
        
        Task<Price> CreatePrice(decimal basePrice, DateTime date);
    }
}