﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Pricing
{
    public class ConfigurationPricingService : IConfigurationPricingService
    {
        private readonly IUnitOfWork _work;
        private readonly IOptionPricingService _pricingService;
        private readonly IPackagePricingService _packagePricingService;

        public ConfigurationPricingService(IUnitOfWork work, IOptionPricingService pricingService, IPackagePricingService packagePricingService)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _pricingService = pricingService ?? throw new ArgumentNullException(nameof(pricingService));
            _packagePricingService = packagePricingService ?? throw new ArgumentNullException(nameof(packagePricingService));
        }

        public async Task<ConfigurationPrice> Calculate(int configurationId, DateTime date)
        {
            var configuration = await _work.EntityRepository.Require<Configuration>(configurationId);

            var vehicleOffer = await _work.EntityRepository.SingleOrDefault(new VehicleOfferForVehicleSpecification(configuration.CatalogId, configuration.ModelKey));
            if (vehicleOffer == null)
                throw new Exception("System is in invalid state");

            var package = configuration.IsPackageIncluded
                ? await _work.EntityRepository.SingleOrDefault<Package, PackageId>(configuration.PackageId)
                : null;

            return await Calculate(vehicleOffer, configuration.Options, configuration.ColorId, package, date);
        }

        public async Task<ConfigurationPrice> Calculate(VehicleOffer vehicleOffer, PrNumberSet options, ColorId colorId, Package package, DateTime date)
        {
            if (vehicleOffer == null)
                throw new ArgumentNullException(nameof(vehicleOffer));
            if (options == null)
                throw new ArgumentNullException(nameof(options));
            if (colorId == null)
                throw new ArgumentNullException(nameof(colorId));

            var vehiclePrice = await _pricingService.CreatePrice(vehicleOffer.Price, date);

            var optionOffers = GetOffers(vehicleOffer, options);

            var optionsPrice = await _pricingService.Calculate(optionOffers, date);

            var packagePrice = await _packagePricingService.Calculate(optionOffers, package, date);

            var colorPrice = await _pricingService.CreatePrice(
                vehicleOffer.FindColorOffer(colorId).Price, date);

            return new ConfigurationPrice(vehiclePrice, colorPrice, optionsPrice, packagePrice, date);
        }

        private static IReadOnlyCollection<OptionOffer> GetOffers(VehicleOffer vehicleOffer, PrNumberSet options)
        {
            return vehicleOffer.Options
                .Where(o => options.Has(o.PrNumber))
                .ToList()
                .AsReadOnly();
        }
    }
}