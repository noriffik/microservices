﻿using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing.Packages;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Pricing
{
    public interface IPackagePricingService
    {
        Task<PackagePrice> Calculate(IEnumerable<OptionOffer> optionOffers, Package package, DateTime date);

        Task<PackagePrice> Calculate(IEnumerable<OptionOffer> optionOffers,
            Package package, IOptionApplicabilityFilter withoutPackageFilter, DateTime date);
    }
}