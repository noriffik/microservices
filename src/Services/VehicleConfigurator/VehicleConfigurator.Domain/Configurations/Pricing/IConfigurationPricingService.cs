﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Pricing
{
    public interface IConfigurationPricingService
    {
        Task<ConfigurationPrice> Calculate(int configurationId, DateTime date);

        Task<ConfigurationPrice> Calculate(VehicleOffer vehicleOffer, PrNumberSet options, ColorId colorId, Package package, DateTime date);
    }
}