﻿using NexCore.CurrencyExchange.Abstract;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Pricing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations.Pricing
{
    public class OptionPricingService : PricingService, IOptionPricingService
    {
        public OptionPricingService(ICurrencyExchangeService exchangeService) : base(exchangeService)
        {
        }

        public async Task<OptionsPrice> Calculate(IEnumerable<OptionOffer> optionOffers, DateTime date)
        {
            if (optionOffers == null)
                throw new ArgumentNullException(nameof(optionOffers));

            var basePrices = optionOffers.ToDictionary(k => k.PrNumber, v => v.Availability.Price);

            var perOptionPrice = await CreatePrice(basePrices, date);

            return OptionsPrice.Create(perOptionPrice);
        }

        public async Task<Dictionary<PrNumber, Price>> CreatePrice(Dictionary<PrNumber, decimal> basePrices, DateTime date)
        {
            if (basePrices == null)
                throw new ArgumentNullException(nameof(basePrices));

            var result = new Dictionary<PrNumber, Price>();

            foreach (var option in basePrices)
            {
                result.Add(option.Key, await CreatePrice(option.Value, date));
            }

            return result;
        }

        public async Task<Price> CreatePrice(decimal basePrice, DateTime date)
        {
            var retailPrice = await CalculateRetail(basePrice, date);

            return new Price(basePrice, retailPrice);
        }
    }
}
