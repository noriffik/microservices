﻿using NexCore.Common;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Configurations
{
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core.")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core.")]
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Local", Justification = "Required by EF Core.")]
    public class Configuration : Entity, IAggregateRoot
    {
        public PrNumberSet Options { get; private set; }

        public int CatalogId { get; private set; }

        public ModelKey ModelKey { get; private set; }

        public DateTime CreatedAt { get; private set; }

        public DateTime UpdatedAt { get; private set; }

        public ConfigurationStatus Status { get; private set; }

        public PackageId PackageId { get; private set; }

        public bool IsPackageIncluded => PackageId != null;

        public ColorId ColorId { get; private set; }

        public Relevance Relevance { get; private set; }

        public bool IsRestricted { get; private set; }

        private Configuration()
        {
            Options = new PrNumberSet();
            Relevance = Relevance.Relevant;
        }

        public Configuration(int id, int catalogId, ModelKey modelKey, ColorId colorId) : base(id)
        {
            ModelKey = modelKey ?? throw new ArgumentNullException(nameof(modelKey));
            CatalogId = catalogId;
            ColorId = colorId ?? throw new ArgumentNullException(nameof(colorId));

            Status = ConfigurationStatus.Draft;
            CreatedAt = SystemTimeProvider.Instance.Get();
            UpdatedAt = CreatedAt;
            Options = new PrNumberSet();
            Relevance = Relevance.Relevant;
        }

        public Configuration(int catalogId, ModelKey modelKey, ColorId colorId)
            : this(default(int), catalogId, modelKey, colorId)
        {
        }

        private Configuration(Configuration configuration)
        {
            CatalogId = configuration.CatalogId;
            ModelKey = configuration.ModelKey;
            Options = configuration.Options;
            PackageId = configuration.PackageId;
            ColorId = configuration.ColorId;

            Status = ConfigurationStatus.Draft;
            CreatedAt = SystemTimeProvider.Instance.Get();
            UpdatedAt = CreatedAt;
            Relevance = configuration.Relevance;
        }

        public void ChangeStatus(ConfigurationStatus status)
        {
            if (Status == status)
                return;

            Status = status;

            UpdatedAt = SystemTimeProvider.Instance.Get();
        }

        public void IncludeOption(PrNumber prNumber)
        {
            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));

            if (Options.Has(prNumber))
                return;

            UpdateOptions(Options.Expand(prNumber.Value));
            UpdatedAt = SystemTimeProvider.Instance.Get();
        }

        public void IncludeOptions(PrNumberSet prNumbers)
        {
            if (prNumbers == null)
                throw new ArgumentNullException(nameof(prNumbers));

            prNumbers.Values.ToList().ForEach(IncludeOption);
        }

        public void ExcludeOption(PrNumber prNumber)
        {
            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));

            if (!Options.Has(prNumber))
                return;

            UpdateOptions(Options.Shrink(prNumber.Value));
            UpdatedAt = SystemTimeProvider.Instance.Get();
        }

        public void IncludePackage(PackageId packageId)
        {
            if (PackageId == packageId)
                return;

            PackageId = packageId;
            UpdatedAt = SystemTimeProvider.Instance.Get();
        }

        public void ExcludePackage()
        {
            if (PackageId == null)
                return;

            PackageId = null;
            UpdatedAt = SystemTimeProvider.Instance.Get();
        }

        private void UpdateOptions(PrNumberSet options)
        {
            Options = options;
        }

        public virtual Configuration MakeCopy()
        {
            return new Configuration(this);
        }

        public void ChangeColor(ColorId colorId)
        {
            if (colorId == null)
                throw new ArgumentNullException(nameof(colorId));

            if (ColorId == colorId)
                return;

            ColorId = colorId;
            UpdatedAt = SystemTimeProvider.Instance.Get();
        }

        public void ChangeRelevance(Relevance relevance)
        {
            if (Relevance == relevance)
                return;

            Relevance = relevance;
            UpdatedAt = SystemTimeProvider.Instance.Get();
        }

        public void EnforceRestriction()
        {
            IsRestricted = true;
        }

        public void LiftRestriction()
        {
            IsRestricted = false;
        }

        public void ToggleRestriction(bool isEnforced)
        {
            if (isEnforced)
                EnforceRestriction();
            else
                LiftRestriction();
        }
    }

    public enum ConfigurationStatus
    {
        Draft = 0,
        Completed = 1
    }
}
