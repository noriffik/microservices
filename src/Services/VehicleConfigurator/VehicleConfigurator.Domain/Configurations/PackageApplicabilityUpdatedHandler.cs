﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations
{
    public class PackageApplicabilityUpdatedHandler : IEntityEventHandler<PackageApplicabilityUpdatedEvent>
    {
        private readonly IUnitOfWork _work;

        public PackageApplicabilityUpdatedHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Handle(PackageApplicabilityUpdatedEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            var configurations = await _work.EntityRepository.Find(new ByPackageIdSpecification(notification.PackageId), cancellationToken);
            var package = await _work.EntityRepository.Require<Package, PackageId>(notification.PackageId, cancellationToken);

            foreach (var configuration in configurations)
                if (!package.IsApplicableTo(configuration.Options))
                    configuration.ChangeRelevance(Relevance.Irrelevant);
        }
    }
}
