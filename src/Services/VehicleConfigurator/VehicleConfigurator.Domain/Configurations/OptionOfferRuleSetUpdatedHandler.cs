﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations.Rules;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations
{
    public class OptionOfferRuleSetUpdatedHandler : IEntityEventHandler<OptionOfferRuleSetUpdatedEvent>
    {
        private readonly IUnitOfWork _work;
        private IConfigurationValidator _validator;

        public OptionOfferRuleSetUpdatedHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public IConfigurationValidator Validator
        {
            get => _validator ?? (_validator = new ConfigurationValidator());
            set => _validator = value ?? throw new ArgumentNullException(nameof(value));
        }

        public async Task Handle(OptionOfferRuleSetUpdatedEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(notification.VehicleOfferId, cancellationToken);

            var configurations = await _work.EntityRepository.Find(
                new ByVehicleOfferWithIncludedOptionSpecification(vehicleOffer.CatalogId, vehicleOffer.ModelKey, new PrNumberSet(notification.PrNumber)), cancellationToken);

            foreach (var configuration in configurations)
            {
                var isValid = Validator.IsValid(configuration.Options, vehicleOffer);
                configuration.ChangeRelevance(isValid
                    ? Relevance.Relevant
                    : Relevance.Irrelevant);
            }
        }
    }
}
