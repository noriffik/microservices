﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations
{
    public class ConfigurationService : IConfigurationService
    {
        private readonly IUnitOfWork _work;
        private IConfigurationValidationService _configurationValidationService;

        public ConfigurationService(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }
        
        public IConfigurationValidationService Validator
        {
            get => _configurationValidationService ?? (_configurationValidationService = new ConfigurationValidationService(_work));
            set => _configurationValidationService = value ?? throw new ArgumentNullException(nameof(value));
        }

        public async Task<Configuration> Add(int catalogId, ModelKey modelKey)
        {
            if (modelKey == null)
                throw new ArgumentNullException(nameof(modelKey));

            await _work.EntityRepository.HasRequired<Catalog>(catalogId);

            var vehicleOffer = await _work.EntityRepository.Require(new PublishedVehicleOfferSpecification(catalogId, modelKey));

            var configuration = new Configuration(catalogId, modelKey, vehicleOffer.DefaultColor.ColorId);

            await _work.EntityRepository.Add(configuration);

            return configuration;
        }

        public async Task<Configuration> Build(Year modelYear, ConfigurationSpecification specification)
        {
            var catalog = await _work.EntityRepository.Require(new RelevantCatalogPublishedForModelYearSpecification(modelYear));

            var configuration = await Add(catalog.Id, specification.ModelKey);

            await IncludeOptions(configuration, specification.PrNumbers);

            return configuration;
        }

        public async Task<Configuration> Copy(int configurationId)
        {
            var configuration = await _work.EntityRepository.Require<Configuration>(configurationId);

            var copy = configuration.MakeCopy();

            await _work.EntityRepository.Add(copy);

            return copy;
        }

        public async Task ChangeStatus(int configurationId, ConfigurationStatus status)
        {
            var configuration = await _work.EntityRepository.Require<Configuration>(configurationId);

            configuration.ChangeStatus(status);
        }

        public async Task IncludeOption(int configurationId, PrNumber prNumber)
        {
            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));

            var configuration = await _work.EntityRepository.Require<Configuration>(configurationId);

            await Validator.ValidateOptionInclusion(configuration, prNumber);

            configuration.IncludeOption(prNumber);
        }

        private async Task IncludeOptions(Configuration configuration, PrNumberSet prNumbers)
        {
            await Validator.ValidateThatOptionsAreAvailable(configuration, prNumbers);

            configuration.IncludeOptions(prNumbers);
        }

        public async Task IncludePackage(int configurationId, PackageCode code)
        {
            var configuration = await _work.EntityRepository.Require<Configuration>(configurationId);

            var package = await FindCompatiblePackage(configuration, code);

            configuration.IncludePackage(package.Id);
        }

        private async Task<Package> FindCompatiblePackage(Configuration configuration, PackageCode code)
        {
            var package = await _work.EntityRepository.Require<Package, PackageId>(
                new CompatiblePackageCodeSpecification(code, configuration));

            Validator.VerifyThatPackageIsApplicable(package, configuration.Options);

            return package;
        }

        public async Task ExcludeOption(int configurationId, PrNumber prNumber)
        {
            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));

            var configuration = await _work.EntityRepository.Require<Configuration>(configurationId);

            await Validator.VerifyThatOptionIsNotInPackage(prNumber, configuration);
            await Validator.VerifyThatOptionIsNotRequired(configuration, prNumber);

            configuration.ExcludeOption(prNumber);
        }

        public async Task ExcludePackage(int configurationId)
        {
            var configuration = await _work.EntityRepository.Require<Configuration>(configurationId);

            configuration.ExcludePackage();
        }

        public async Task ChangeColor(int configurationId, ColorId colorId)
        {
            if (colorId == null)
                throw new ArgumentNullException(nameof(colorId));

            var configuration = await _work.EntityRepository.Require<Configuration>(configurationId);

            await Validator.ValidateColorAvailability(configuration, colorId);

            configuration.ChangeColor(colorId);
        }

        public async Task Remove(int configurationId)
        {
            var configuration = await _work.EntityRepository.SingleOrDefault<Configuration>(configurationId);
            if (configuration == null)
                return;

            await _work.EntityRepository.Remove(configuration);
        }
    }
}
