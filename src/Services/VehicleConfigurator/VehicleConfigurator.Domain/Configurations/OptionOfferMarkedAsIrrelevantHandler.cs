﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations
{
    public class OptionOfferMarkedAsIrrelevantHandler : IEntityEventHandler<OptionOfferMarkedAsIrrelevantEvent>
    {
        private readonly IUnitOfWork _work;

        public OptionOfferMarkedAsIrrelevantHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Handle(OptionOfferMarkedAsIrrelevantEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(notification.VehicleOfferId, cancellationToken);

            var specification = new ByVehicleOfferWithIncludedOptionSpecification(vehicleOffer.CatalogId, vehicleOffer.ModelKey, new PrNumberSet(notification.PrNumber));
            var configurations = await _work.EntityRepository.Find(specification, cancellationToken);

            foreach (var configuration in configurations)
                configuration.ChangeRelevance(Relevance.Irrelevant);
        }
    }
}
