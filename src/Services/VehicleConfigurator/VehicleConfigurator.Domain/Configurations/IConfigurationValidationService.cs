﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Threading.Tasks;
using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Domain.Configurations
{
    public interface IConfigurationValidationService
    {
        Task ValidateConfiguration(Year modelYear, ModelKey modelKey, PrNumberSet prNumbers);
        Task ValidateThatOptionsAreAvailable(Configuration configuration, PrNumberSet prNumbers);
        Task ValidateOptionInclusion(Configuration configuration, PrNumber prNumber);
        Task VerifyThatOptionIsNotInPackage(PrNumber option, Configuration configuration);
        Task VerifyThatOptionIsNotRequired(Configuration configuration, PrNumber prNumber);
        void VerifyThatPackageIsApplicable(Package package, PrNumberSet prNumbers);
        Task ValidateColorAvailability(Configuration configuration, ColorId colorId);
    }
}