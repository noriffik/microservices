﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Domain.Configurations
{
    public class ConfigurationSpecification : ValueObject
    {
        public ModelKey ModelKey { get; }

        public PrNumberSet PrNumbers { get; }

        public ConfigurationSpecification(ModelKey modelKey, PrNumberSet prNumbers)
        {
            ModelKey = modelKey;
            PrNumbers = prNumbers;
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return ModelKey;
            yield return PrNumbers;
        }
    }
}