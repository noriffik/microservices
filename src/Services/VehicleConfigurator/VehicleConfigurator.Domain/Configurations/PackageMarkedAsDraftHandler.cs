﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Configurations
{
    public class PackageMarkedAsDraftHandler : IEntityEventHandler<PackageMarkedAsDraftEvent>
    {
        private readonly IUnitOfWork _work;

        public PackageMarkedAsDraftHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Handle(PackageMarkedAsDraftEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            var configurations = await _work.EntityRepository.Find(new ByPackageIdSpecification(notification.PackageId), cancellationToken);

            foreach (var configuration in configurations)
                    configuration.ChangeRelevance(Relevance.Irrelevant);
        }
    }
}
