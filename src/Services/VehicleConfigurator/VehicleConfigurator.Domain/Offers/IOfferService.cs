﻿using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Offers
{
    public interface IOfferService
    {
        Task<int> Add(int configurationId);

        Task<int> Actualize(int offerId);

        Task SetPrivateCustomer(int offerId, int privateCustomerId);

        Task ChangeStatus(int offerId, OfferStatus status);
    }
}