﻿using NexCore.Common;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Offers
{
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core.")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core.")]
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Local", Justification = "Required by EF Core.")]
    public class Offer : Entity, IAggregateRoot
    {
        public int? PrivateCustomerId { get; set; }

        public string DealerId { get; set; } = "38001"; //TODO: fix with claims autocomplete

        public Year ModelYear { get; private set; }

        public ModelKey ModelKey { get; private set; }

        public ModelId ModelId { get; private set; }

        public BodyId BodyId { get; private set; }

        public EquipmentId EquipmentId { get; private set; }

        public EngineId EngineId { get; private set; }

        public GearboxId GearboxId { get; private set; }

        public PackageId PackageId { get; private set; }

        public PrNumberSet Options { get; private set; }

        public PrNumberSet IncludedOptions { get; private set; }

        public ColorId ColorId { get; private set; }

        public ConfigurationPrice Price { get; private set; }

        public DateTime CreatedAt { get; private set; }

        public OfferStatus Status { get; private set; }

        private Offer()
        {
            Options = new PrNumberSet();
        }

        public Offer(Year modelYear, ModelKey modelKey, PrNumberSet options, PrNumberSet includedOptions, ConfigurationPrice price, ColorId colorId)
            : this(default, modelYear, modelKey, options, includedOptions, price, colorId)
        {
        }

        public Offer(int id, Year modelYear, ModelKey modelKey, PrNumberSet options, PrNumberSet includedOptions, 
            ConfigurationPrice price, ColorId colorId)
            : base(id)
        {
            ModelYear = modelYear ?? throw new ArgumentNullException(nameof(modelYear));
            ModelKey = modelKey ?? throw new ArgumentNullException(nameof(modelKey));
            ModelId = modelKey.ModelId;
            BodyId = modelKey.BodyId;
            EquipmentId = modelKey.EquipmentId;
            EngineId = modelKey.EngineId;
            GearboxId = modelKey.GearboxId;
            Options = options ?? throw new ArgumentNullException(nameof(options));
            IncludedOptions = includedOptions ?? throw new ArgumentNullException(nameof(includedOptions));
            ColorId = colorId ?? throw new ArgumentNullException(nameof(colorId));
            Price = price ?? throw new ArgumentNullException(nameof(price));
            CreatedAt = SystemTimeProvider.Instance.Get();
            Status = OfferStatus.None;
        }

        public Offer(int id, Year modelYear, ModelKey modelKey, PackageId packageId, PrNumberSet options, PrNumberSet includedOptions,
            ConfigurationPrice price, ColorId colorId)
            : this(id, modelYear, modelKey, options, includedOptions, price, colorId)
        {
            PackageId = packageId ?? throw new ArgumentNullException(nameof(packageId));
        }

        public Offer(Year modelYear, ModelKey modelKey, PackageId packageId, PrNumberSet options, PrNumberSet includedOptions, 
            ConfigurationPrice price, ColorId colorId)
            : this(default, modelYear, modelKey, packageId, options, includedOptions, price, colorId)
        {
        }

        public static Offer From(Year modelYear, VehicleOffer vehicleOffer, Configuration configuration, ConfigurationPrice price)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (vehicleOffer == null)
                throw new ArgumentNullException(nameof(vehicleOffer));

            var includedOptions = new PrNumberSet(vehicleOffer.Options
                .Where(o => o.Availability.Type == AvailabilityType.Included)
                .Select(o => o.PrNumber));

            return configuration.IsPackageIncluded
                ? new Offer(modelYear, configuration.ModelKey, configuration.PackageId, configuration.Options, includedOptions, price, configuration.ColorId)
                : new Offer(modelYear, configuration.ModelKey, configuration.Options, includedOptions, price, configuration.ColorId);
        }

        public void ChangeStatus(OfferStatus status)
        {
            Status = status;
        }
    }

    public enum OfferStatus
    {
        None = 0,
        Accepted = 1
    }
}
