﻿using NexCore.Common;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Offers
{
    public class OfferService : IOfferService
    {
        private readonly IUnitOfWork _work;
        private readonly IConfigurationPricingService _configurationPricingService;

        public OfferService(IUnitOfWork work, IConfigurationPricingService configurationPricingService)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _configurationPricingService = configurationPricingService ?? throw new ArgumentNullException(nameof(configurationPricingService));
        }

        public async Task<int> Add(int configurationId)
        {
            var configuration = await FindCompletedConfiguration(configurationId);
            
            var date = SystemTimeProvider.Instance.Get();

            var price = await _configurationPricingService.Calculate(configuration.Id, date);

            var catalog = await _work.EntityRepository.Require<Catalog>(configuration.CatalogId);

            var vehicleOffer = await _work.EntityRepository.Require(new VehicleOfferForVehicleSpecification(catalog.Id, configuration.ModelKey));

            var offer = await Create(catalog.ModelYear, vehicleOffer, configuration, price);

            return offer.Id;
        }

        private async Task<Offer> Create(Year modelYear, VehicleOffer vehicleOffer, Configuration configuration, ConfigurationPrice price)
        {
            var offer = Offer.From(modelYear, vehicleOffer, configuration, price);

            await _work.EntityRepository.Add(offer);

            return offer;
        }

        public async Task<int> Actualize(int offerId)
        {
            //Load entities
            var offer = await _work.EntityRepository.Require<Offer>(offerId);
            var catalog = await _work.EntityRepository.Require(new RelevantCatalogPublishedForModelYearSpecification(offer.ModelYear));
            var vehicleOffer = await _work.EntityRepository.Require(new VehicleOfferForVehicleSpecification(catalog.Id, offer.ModelKey));
            var package = offer.PackageId == null
                ? null
                : await _work.EntityRepository.Require<Package, PackageId>(offer.PackageId);

            //Calculate actual price
            var date = SystemTimeProvider.Instance.Get();
            var actualPrice = await _configurationPricingService.Calculate(vehicleOffer, offer.Options, offer.ColorId,
                package, date);

            //Actualize
            var actualizedOffer = await Create(offer.ModelYear, offer.ModelKey, offer.PackageId, offer.Options, offer.IncludedOptions, actualPrice, offer.ColorId);

            return actualizedOffer.Id;
        }

        private async Task<Offer> Create(Year modelYear, ModelKey modelKey, PackageId packageId, PrNumberSet options, PrNumberSet includedOptions,
            ConfigurationPrice price, ColorId colorId)
        {
            var offer = packageId == null
                ? new Offer(modelYear, modelKey, options, includedOptions, price, colorId)
                : new Offer(modelYear, modelKey, packageId, options, includedOptions, price, colorId);

            await _work.EntityRepository.Add(offer);

            return offer;
        }

        public async Task SetPrivateCustomer(int offerId, int privateCustomerId)
        {
            var offer = await _work.EntityRepository.Require<Offer>(offerId);

            offer.PrivateCustomerId = privateCustomerId;
        }

        public async Task ChangeStatus(int offerId, OfferStatus status)
        {
            var offer = await _work.EntityRepository.Require<Offer>(offerId);

            offer.ChangeStatus(status);
        }

        private async Task<Configuration> FindCompletedConfiguration(int configurationId)
        {
            var configuration = await _work.EntityRepository.Require<Configuration>(configurationId);

            if (configuration.Status != ConfigurationStatus.Completed)
                throw new InvalidEntityIdException("Configuration is not completed!", typeof(Configuration), configurationId);

            return configuration;
        }
    }
}
