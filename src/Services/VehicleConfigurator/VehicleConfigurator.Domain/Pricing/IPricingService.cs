﻿using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Pricing
{
    public interface IPricingService
    {
        decimal Margin { get; set; }

        Task<decimal> CalculateRetail(decimal basedOn, DateTime onDate);
    }
}
