﻿using System;

namespace NexCore.VehicleConfigurator.Domain.Pricing
{
    public class RetailPriceAlgorithm : IPricingAlgorithm
    {
        public decimal Rate { get; set; }

        public decimal Margin { get; set; }
        
        public decimal Calculate(decimal basedOn)
        {
            if (basedOn < 0)
                throw new ArgumentOutOfRangeException(nameof(basedOn));

            if (!(Rate > 0))
                throw new InvalidOperationException("Rate is invalid. Rate cannot be less or equal to zero.");

            return WithMargin(InCurrency(basedOn));
        }
        
        private decimal WithMargin(decimal basedOnInHrv)
        {
            return basedOnInHrv / 100 * (100 + Margin);
        }

        private decimal InCurrency(decimal basedOn)
        {
            return basedOn * Rate;
        }
    }
}
