﻿using NexCore.CurrencyExchange.Abstract;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Pricing
{
    public class PricingService : IPricingService
    {
        private readonly ICurrencyExchangeService _exchangeService;

        public PricingService(ICurrencyExchangeService exchangeService)
        {
            _exchangeService = exchangeService ?? throw new ArgumentNullException(nameof(exchangeService));
        }

        public decimal Margin { get; set; }

        public virtual async Task<decimal> CalculateRetail(decimal basedOn, DateTime onDate)
        {
            if (basedOn < 0)
                throw new ArgumentOutOfRangeException(nameof(basedOn));
            
            var algorithm = new RetailPriceAlgorithm
            {
                Rate = await GetRate(onDate),
                Margin = Margin
            };

            return algorithm.Calculate(basedOn);
        }

        private async Task<decimal> GetRate(DateTime onDate)
        {
            try
            {
                return await _exchangeService.GetRate(Currency.Usd, Currency.Uah, onDate);
            }
            catch (CurrencyExchangeException e)
            {
                throw new PricingUnavailableException($"Unavailable due to {e}.", e);
            }
        }
    }
}
