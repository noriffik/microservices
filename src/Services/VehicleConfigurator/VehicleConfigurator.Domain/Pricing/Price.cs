﻿using NexCore.Domain;
using System;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Domain.Pricing
{
    public class Price : ValueObject, IComparable<Price>, IComparable
    {
        public decimal Base { get; }

        public decimal Retail { get; }

        private Price()
        {
        }

        public Price(decimal @base, decimal retail)
        {
            Base = @base;
            Retail = retail;
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return Base;
        }

        public static Price Empty => new Price(0, 0);

        public static Price operator +(Price a, Price b)
        {
            if (a == null) return b;

            return b == null ? a : new Price(a.Base + b.Base, a.Retail + b.Retail);
        }

        public static Price operator -(Price a, Price b)
        {
            if (a == null && b != null)
                return new Price(0 - b.Base, 0 - b.Retail);

            if (b == null && a != null)
                return a;

            if (a == null && b == null)
                return new Price(0, 0);

            return new Price(a.Base - b.Base, a.Retail - b.Retail);
        }

        public static bool operator <(Price a, Price b)
        {
            if (a == null)
                return true;

            return a.Base < b.Base ;
        }

        public static bool operator >(Price a, Price b)
        {
            if (b == null)
                return true;

            return a.Base > b.Base;
        }

        public int CompareTo(Price other)
        {
            if (other == null) return 1;

            return Base > other.Base
                ? 1
                : Base < other.Base
                    ? -1
                    : 0;
        }

        public int CompareTo(object obj) => CompareTo(obj as Price);
    }
}
