﻿namespace NexCore.VehicleConfigurator.Domain.Pricing
{
    public interface IPricingAlgorithm
    {
        decimal Calculate(decimal basedOn);
    }
}