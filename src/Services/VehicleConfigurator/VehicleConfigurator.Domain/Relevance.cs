﻿namespace NexCore.VehicleConfigurator.Domain
{
    public enum Relevance
    {
        Irrelevant = 0,
        Relevant = 1
    }
}
