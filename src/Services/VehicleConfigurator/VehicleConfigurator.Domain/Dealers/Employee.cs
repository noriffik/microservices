﻿using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.Legal;
using System;

namespace NexCore.VehicleConfigurator.Domain.Dealers
{
    public class Employee : Entity, IAggregateRoot
    {
        public DealerId DealerId { get; private set; }

        public PersonName PersonName { get; private set; }

        private Employee()
        {
        }

        public Employee(int employeeId, PersonName personName, DealerId dealerId) : base(employeeId)
        {
            PersonName = personName ?? throw new ArgumentNullException(nameof(personName));
            DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
        }

        public void ChangeName(PersonName name)
        {
            PersonName = name ?? throw new ArgumentNullException(nameof(name));
        }
    }
}
