﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Dealers.Specifications
{
    public class DealerByCompanyIdSpecification : Specification<Dealer>
    {
        public int CompanyId { get; }

        public DealerByCompanyIdSpecification(int companyId)
        {
            CompanyId = companyId;
        }

        public override Expression<Func<Dealer, bool>> ToExpression()
        {
            return dealer => dealer.CompanyId == CompanyId;
        }

        public override string Description => $"Dealer by company id {CompanyId}";
    }
}
