﻿using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.Legal;
using System;

namespace NexCore.VehicleConfigurator.Domain.Dealers
{
    public class Dealer : Entity<DealerId>, IAggregateRoot<DealerId>
    {
        public int CompanyId { get; set; }

        public DistributorId DistributorId { get; private set; }

        public DealerCode DealerCode { get; private set; }

        public OrganizationRequisites Requisites { get; private set; }

        private Dealer()
        {
        }

        public Dealer(DealerId id, int companyId, OrganizationRequisites requisites)
            : base(id)
        {
            CompanyId = companyId;
            DistributorId = id.DistributorId;
            DealerCode = id.DealerCode;
            Requisites = requisites ?? throw new ArgumentNullException(nameof(requisites));
        }

        public void ChangeRequisites(OrganizationRequisites requisites)
        {
            Requisites.Assign(requisites ?? OrganizationRequisites.Empty);
        }
    }
}
