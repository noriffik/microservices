﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Domain.Rules
{
    public class PrNumberCombination : ValueObject
    {
        public PrNumberSet Included { get; private set; }
        public PrNumberSet Excluded { get; private set; }

        public static PrNumberCombination Empty => new PrNumberCombination();

        private PrNumberCombination()
        {
            Included = PrNumberSet.Empty;
            Excluded = PrNumberSet.Empty;
        }
        
        public static PrNumberCombination IncludedOnly(PrNumberSet prNumbers)
        {
            return new PrNumberCombination
            {
                Included = prNumbers
            };
        }

        public static PrNumberCombination IncludedOnly(string prNumbers)
        {
            return IncludedOnly(PrNumberSet.Parse(prNumbers));
        }

        public static PrNumberCombination IncludedOnly(PrNumber prNumber)
        {
            return new PrNumberCombination
            {
                Included = new PrNumberSet(prNumber)
            };
        }

        public static PrNumberCombination ExcludedOnly(PrNumberSet prNumbers)
        {
            return new PrNumberCombination
            {
                Excluded = prNumbers
            };
        }

        public static PrNumberCombination ExcludedOnly(string prNumbers)
        {
            return ExcludedOnly(PrNumberSet.Parse(prNumbers));
        }
        
        public static PrNumberCombination ExcludedOnly(PrNumber prNumber)
        {
            return new PrNumberCombination
            {
                Excluded = new PrNumberSet(prNumber)
            };
        }

        public static PrNumberCombination Both(PrNumberSet included, PrNumberSet excluded)
        {
            return new PrNumberCombination
            {
                Included = included,
                Excluded = excluded
            };
        }

        public static PrNumberCombination Both(string included, string excluded)
        {
            return Both(PrNumberSet.Parse(included), PrNumberSet.Parse(excluded));
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return Included;
            yield return Excluded;
        }
    }
}
