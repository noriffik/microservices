﻿using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Domain.Rules
{
    public interface IRuleDescriber
    {
        bool IsValid(string ruleSet);

        IReadOnlyCollection<string> GetRules(string ruleSet);

        IReadOnlyCollection<IRuleDescription> DescribeMany(string ruleSet);

        IRuleDescription Describe(string rule);
    }
}