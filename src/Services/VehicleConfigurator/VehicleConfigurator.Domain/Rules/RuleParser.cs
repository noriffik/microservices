﻿using NexCore.VehicleConfigurator.Domain.Rules.Expressions;
using System;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Domain.Rules
{
    public class RuleParser : IRuleParser
    {
        public IRuleDescriber RuleSetDecomposer => new RuleDescriber();
        
        public bool IsValid(string ruleSet)
        {
            return RuleSetDecomposer.IsValid(ruleSet);
        }

        public IEnumerable<PrNumberSetRule> Parse(string ruleSet)
        {
            var expressions = new List<PrNumberSetRule>();

            foreach (var rule in RuleSetDecomposer.GetRules(ruleSet))
            {
                expressions.Add(ParseRule(rule));
            }

            return expressions;
        }

        private PrNumberSetRule ParseRule(string rule)
        {
            var description = RuleSetDecomposer.Describe(rule);
            
            var requirement = Parse(description.Requirement);

            switch (description.Name)
            {
                case "ZA":
                {
                    return new OnlyWithAll(description.PrNumbers, requirement);
                }
                case "ZO":
                {
                    return new OnlyWithOne(description.PrNumbers, requirement);
                }
                case "VA":
                {
                    return new ProhibitedAll(description.PrNumbers, requirement);
                }
                case "VO":
                {
                    return new ProhibitedOne(description.PrNumbers, requirement);
                }
                default:
                    throw new FormatException();
            }
        }
    }
}
