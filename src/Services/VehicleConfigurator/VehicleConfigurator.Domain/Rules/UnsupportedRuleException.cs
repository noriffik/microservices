﻿using System;

namespace NexCore.VehicleConfigurator.Domain.Rules
{
    public class UnsupportedRuleException: Exception
    {
        private const string DefaultMessage = "Undefined rule name.";

        public UnsupportedRuleException() : base(DefaultMessage)
        {
        }

        public UnsupportedRuleException(string message) : base(message)
        {
        }

        public UnsupportedRuleException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
