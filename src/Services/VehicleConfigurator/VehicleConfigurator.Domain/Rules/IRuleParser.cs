﻿using NexCore.VehicleConfigurator.Domain.Rules.Expressions;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Domain.Rules
{
    public interface IRuleParser
    {
        bool IsValid(string ruleSet);
        
        IEnumerable<PrNumberSetRule> Parse(string ruleSet);
    }
}
