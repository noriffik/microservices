﻿namespace NexCore.VehicleConfigurator.Domain.Rules
{
    public interface IExpression
    {
        bool Check(RuleContext context);
    }
}