﻿using NexCore.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Domain.Rules
{
    public class RuleContext
    {
        public string RuleSet;
        private readonly string _prNumberSet;

        public PrNumberSet PrNumberSet => PrNumberSet.Parse(_prNumberSet);

        public RuleContext(string ruleSet, string prNumberSet)
        {
            RuleSet = ruleSet ?? throw new ArgumentNullException(nameof(ruleSet));
            _prNumberSet = prNumberSet ?? throw new ArgumentNullException(nameof(prNumberSet));
        }
    }
}
