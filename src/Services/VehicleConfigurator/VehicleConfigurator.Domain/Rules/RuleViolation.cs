﻿namespace NexCore.VehicleConfigurator.Domain.Rules
{
    public class RuleViolation
    {
        public static readonly RuleViolation Empty = new RuleViolation();

        public string ForPrNumber { get; set; }
        public string RuleString { get; set; }
        public string RuleName { get; set; }
        public string PrNumbers { get; set; }
        public string Message { get; set; }
    }
}
