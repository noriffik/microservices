﻿using NexCore.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Domain.Rules
{
    public class RuleDescription : IRuleDescription
    {
        public string Rule { get; }

        public string Name => Rule.Substring(1, 2);

        public PrNumberSet PrNumbers
        {
            get
            {
                var prNumbers = Rule.Substring(4, Rule.IndexOf("+", StringComparison.Ordinal) - 4).Trim();
            
                return PrNumberSet.Parse(prNumbers);
            }
        }

        public string Requirement
        {
            get
            {
                var plusIndex = Rule.IndexOf("+", StringComparison.Ordinal) + 2;

                return Rule.Substring(plusIndex, Rule.Length - plusIndex - 2);
            }
        }

        public RuleDescription(string rule)
        {
            if (string.IsNullOrEmpty(rule))
                throw new ArgumentException("Value cannot be null or empty.", nameof(rule));
            
            Rule = rule;
        }
    }
}