﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.OptionOffers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Rules
{
    public class RuleValidationService : IRuleValidationService
    {
        private IRuleParser _ruleParser;
        
        public IRuleParser RuleParser
        {
            get => _ruleParser ?? (_ruleParser = new RuleParser());
            set => _ruleParser = value ?? throw new ArgumentNullException(nameof(value));
        }
        
        public bool HasConflict(string ruleSet, PrNumber prNumber, VehicleOffer vehicleOffer)
        {
            var expressions = RuleParser.Parse(ruleSet).ToList();

            var combinations = new List<PrNumberCombination>();

            foreach (var expression in expressions)
            {
                var satisfying = expression.GetAllCombinations().Satisfying;

                combinations = combinations.Merge(satisfying)
                    .WithoutConflicts()
                    .WithoutSelfDependency(prNumber)
                    .Distinct()
                    .Where(c => !HasDeepDependency(c, prNumber, vehicleOffer))
                    .ToList();

                if (!combinations.Any())
                    return true;
            }

            return false;
        }

        private bool HasDeepDependency(PrNumberCombination combination, PrNumber prNumber, VehicleOffer vehicleOffer)
        {
            var prNumbers = combination.Included.Expand(combination.Excluded);

            var optionOffers = vehicleOffer.Options.Apply(new OptionOffersWithPrNumberSpecification(prNumbers));

            foreach (var optionOffer in optionOffers)
            {
                if (HasConflict(optionOffer.RuleSet, prNumber, vehicleOffer))
                    return true;
            }

            return false;
        }
    }
}
