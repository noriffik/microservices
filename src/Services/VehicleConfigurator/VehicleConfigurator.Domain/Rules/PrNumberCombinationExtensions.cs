﻿using System;
using System.Collections.Generic;
using System.Linq;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Domain.Rules
{
    public static class PrNumberCombinationExtensions
    {
        public static IEnumerable<PrNumberCombination> Merge(this IEnumerable<PrNumberCombination> against, IEnumerable<PrNumberCombination> subject)
        {
            if (subject == null)
                throw new ArgumentNullException(nameof(subject));

            var firstList = against.ToList();
            var secondList = subject.ToList();

            if (!firstList.Any())
                return secondList;

            if (!secondList.Any())
                return firstList;

            var options = new List<PrNumberCombination>();

            foreach (var firstOptions in firstList)
            {
                foreach (var secondOptions in secondList)
                {
                    var included = firstOptions.Included.Expand(secondOptions.Included);
                    var excluded = firstOptions.Excluded.Expand(secondOptions.Excluded);

                    options.Add(PrNumberCombination.Both(included, excluded));
                }
            }

            return options;
        }

        public static IEnumerable<PrNumberCombination> WithoutConflicts(this IEnumerable<PrNumberCombination> source)
        {
            return source.Distinct()
                .Where(o => !o.Included.HasExactlyOne(o.Excluded))
                .ToList();
        }

        public static IEnumerable<PrNumberCombination> WithoutSelfDependency(this IEnumerable<PrNumberCombination> source, PrNumber prNumber)
        {
            return source.Distinct()
                .Where(o => !o.Included.Has(prNumber))
                .Where(o => !o.Excluded.Has(prNumber))
                .ToList();
        }
    }
}