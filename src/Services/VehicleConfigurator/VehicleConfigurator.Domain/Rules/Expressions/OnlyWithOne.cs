﻿using NexCore.Domain.Vehicles;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Rules.Expressions
{
    public class OnlyWithOne : PrNumberSetRule
    {
        public OnlyWithOne(PrNumberSet prNumbers, IEnumerable<PrNumberSetRule> requirement)
            : base("ZO", prNumbers, requirement)
        {
        }

        protected override bool IsSatisfied(RuleContext context)
        {
            return context.PrNumberSet.HasExactlyOne(PrNumbers);
        }

        protected override PrNumberSetRuleCombinations GetCombinations()
        {
            var satisfying = new List<PrNumberCombination>();
            var unsatisfying = new List<PrNumberCombination>
            {
                PrNumberCombination.IncludedOnly(PrNumbers),
                PrNumberCombination.ExcludedOnly(PrNumbers)
            };
            
            foreach (var prNumber in PrNumbers.Values)
            {
                var required = new PrNumberSet(prNumber);
                var residual = PrNumbers.Shrink(prNumber);

                satisfying.Add(PrNumberCombination.Both(required, residual));

                if (residual.Values.Count() > 1) 
                    unsatisfying.Add(PrNumberCombination.Both(residual, required));
            }

            var combinations = new PrNumberSetRuleCombinations(satisfying, unsatisfying);

            return combinations.WithoutConflicts();
        }
    }
}
