﻿using NexCore.Domain.Vehicles;
using System;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Domain.Rules.Expressions
{
    public abstract class PrNumberSetRule : IExpression
    {
        public readonly string RuleName;

        public readonly PrNumberSet PrNumbers;

        private readonly Requirement _requirement;
        
        protected PrNumberSetRule(string ruleName, PrNumberSet prNumbers, IEnumerable<PrNumberSetRule> requirement)
        {
            RuleName = ruleName ?? throw new ArgumentNullException(nameof(ruleName));
            PrNumbers = prNumbers ?? throw new ArgumentNullException(nameof(prNumbers));
            _requirement = new Requirement(requirement);
        }

        protected abstract bool IsSatisfied(RuleContext context);

        protected abstract PrNumberSetRuleCombinations GetCombinations();

        public PrNumberSetRuleCombinations GetAllCombinations()
        {
            return ResolveCombinations().WithoutConflicts();
        }

        private PrNumberSetRuleCombinations ResolveCombinations()
        {
            var satisfyingOptions = new List<PrNumberCombination>();

            var mainPossibleOptions = GetCombinations();
            var requirementPossibleOptions = _requirement.GetAllPossibleOptions();

            var withoutConflicts = mainPossibleOptions
                .Satisfying
                .Merge(requirementPossibleOptions.Satisfying)
                .WithoutConflicts();

            satisfyingOptions.AddRange(withoutConflicts);
            satisfyingOptions.AddRange(requirementPossibleOptions.Unsatisfying);

            return new PrNumberSetRuleCombinations(satisfyingOptions, mainPossibleOptions.Unsatisfying);
        }

        public bool Check(RuleContext context)
        {
            return !_requirement.Check(context) || IsSatisfied(context);
        }
        
        public override string ToString()
        {
            return $"[{RuleName}: {PrNumbers}+{_requirement}]";
        }
    }
}
