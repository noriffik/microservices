﻿using System;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Domain.Rules.Expressions
{
    public interface IRuleMessageComposer
    {
        string Compose(string ruleName, string prNumbers);
    }

    public class RuleMessageComposer : IRuleMessageComposer
    {
        private readonly IDictionary<string, string> _messageTemplates = new Dictionary<string, string>
        {
            {"ZA", "Опція доступна тільки при опціях {0}"},
            {"ZO", "Опція доступна тільки з однією із опцій {0}"},
            {"VA", "Опція не доступна з опціями {0}"},
            {"VO", "Опція не доступна з однією із опцій {0}"}
        };

        public string Compose(string ruleName, string prNumbers)
        {
            if (ruleName == null) throw new ArgumentNullException(nameof(ruleName));
            if (!_messageTemplates.ContainsKey(ruleName)) throw new UnsupportedRuleException();

            return string.Format(_messageTemplates[ruleName], prNumbers);
        }

        public string Compose(PrNumberSetRule ruleExpression)
        {
            if (ruleExpression == null) throw new ArgumentNullException(nameof(ruleExpression));
            if (!_messageTemplates.ContainsKey(ruleExpression.RuleName)) throw new FormatException();

            return string.Format(_messageTemplates[ruleExpression.RuleName], ruleExpression.PrNumbers.AsString);
        }
    }
}
