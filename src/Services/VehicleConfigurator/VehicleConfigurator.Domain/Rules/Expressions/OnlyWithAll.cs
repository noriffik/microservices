﻿using NexCore.Domain.Vehicles;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Domain.Rules.Expressions
{
    public class OnlyWithAll : PrNumberSetRule
    {
        public OnlyWithAll(PrNumberSet prNumbers, IEnumerable<PrNumberSetRule> requirement)
            : base("ZA", prNumbers, requirement)
        {
        }

        protected override bool IsSatisfied(RuleContext context)
        {
            return context.PrNumberSet.Has(PrNumbers);
        }

        protected override PrNumberSetRuleCombinations GetCombinations()
        {
            var satisfying = new List<PrNumberCombination>
            {
                PrNumberCombination.IncludedOnly(PrNumbers)
            };
            var unsatisfying = new List<PrNumberCombination>();

            foreach (var prNumber in PrNumbers.Values)
            {
                unsatisfying.Add(PrNumberCombination.ExcludedOnly(prNumber));
            }

            var combinations = new PrNumberSetRuleCombinations(satisfying, unsatisfying);

            return combinations.WithoutConflicts();
        }
    }
}
 