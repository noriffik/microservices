﻿using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Rules.Expressions
{
    public class Requirement : IExpression
    {
        private readonly IEnumerable<PrNumberSetRule> _expressions;

        public Requirement(IEnumerable<PrNumberSetRule> expressions)
        {
            _expressions = expressions ?? new List<PrNumberSetRule>();
        }

        public bool Check(RuleContext context)
        {
            foreach (var r in _expressions)
                if (r.Check(context) == false)
                    return false;

            return true;
        }

        public PrNumberSetRuleCombinations GetAllPossibleOptions()
        {
            var satisfying = new List<PrNumberCombination>();
            var unsatisfying = new List<PrNumberCombination>();

            foreach (var r in _expressions)
            {
                var possibleOptions = r.GetAllCombinations();

                satisfying = possibleOptions.Satisfying
                    .Merge(satisfying)
                    .WithoutConflicts()
                    .ToList();

                unsatisfying.AddRange(possibleOptions.Unsatisfying);
            }

            var combinations = new PrNumberSetRuleCombinations(satisfying, unsatisfying);

            return combinations.WithoutConflicts();
        }

        public override string ToString()
        {
            var requirement = "";
            
            foreach (var expression in _expressions)
                requirement = requirement + expression;
           
            return $"[{requirement}]";
        }
    }
}
