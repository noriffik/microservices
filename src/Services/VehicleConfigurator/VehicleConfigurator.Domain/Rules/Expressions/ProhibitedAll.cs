﻿using NexCore.Domain.Vehicles;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Domain.Rules.Expressions
{
    public class ProhibitedAll : PrNumberSetRule
    {
        public ProhibitedAll(PrNumberSet prNumbers, IEnumerable<PrNumberSetRule> requirement)
            : base("VA", prNumbers, requirement)
        {
        }

        protected override bool IsSatisfied(RuleContext context)
        {
            return !context.PrNumberSet.Has(PrNumbers);
        }

        protected override PrNumberSetRuleCombinations GetCombinations()
        {
            var satisfying = new List<PrNumberCombination>
            {
                PrNumberCombination.ExcludedOnly(PrNumbers)
            };

            var unsatisfying = new List<PrNumberCombination>();
            foreach (var prNumber in PrNumbers.Values)
            {
                unsatisfying.Add(PrNumberCombination.IncludedOnly(prNumber));
            }

            var combinations = new PrNumberSetRuleCombinations(satisfying, unsatisfying);

            return combinations.WithoutConflicts();
        }
    }
}
