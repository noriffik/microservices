﻿using NexCore.Domain.Vehicles;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Rules.Expressions
{
    public class ProhibitedOne : PrNumberSetRule
    {
        public ProhibitedOne(PrNumberSet prNumbers, IEnumerable<PrNumberSetRule> requirement)
            : base("VO", prNumbers, requirement)
        {
        }

        protected override bool IsSatisfied(RuleContext context)
        {
            return !context.PrNumberSet.HasExactlyOne(PrNumbers);
        }

        protected override PrNumberSetRuleCombinations GetCombinations()
        {
            var satisfying = new List<PrNumberCombination>
            {
                PrNumberCombination.IncludedOnly(PrNumbers),
                PrNumberCombination.ExcludedOnly(PrNumbers)
            };

            var unsatisfying = new List<PrNumberCombination>();

            foreach (var prNumber in PrNumbers.Values)
            {
                var required = PrNumbers.Shrink(prNumber);
                var residual = new PrNumberSet(prNumber);

                if (required.Values.Count() > 1)
                    satisfying.Add(PrNumberCombination.Both(required, residual));

                unsatisfying.Add(PrNumberCombination.Both(residual, required));
            }

            var combinations = new PrNumberSetRuleCombinations(satisfying, unsatisfying);

            return combinations.WithoutConflicts();
        }
    }
}
