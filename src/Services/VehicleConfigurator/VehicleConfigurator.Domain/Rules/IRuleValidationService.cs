﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.Domain.Rules
{
    public interface IRuleValidationService
    {
        IRuleParser RuleParser { get; set; }
        bool HasConflict(string ruleSet, PrNumber prNumber, VehicleOffer vehicleOffer);
    }
}