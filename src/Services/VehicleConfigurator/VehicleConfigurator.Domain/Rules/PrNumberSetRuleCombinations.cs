﻿using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Rules
{
    public class PrNumberSetRuleCombinations
    {
        public IEnumerable<PrNumberCombination> Satisfying { get; }
        public IEnumerable<PrNumberCombination> Unsatisfying { get; }
        
        public PrNumberSetRuleCombinations(
            IEnumerable<PrNumberCombination> satisfying, IEnumerable<PrNumberCombination> unsatisfying)
        {
            Satisfying = satisfying.ToList();
            Unsatisfying = unsatisfying.ToList();
        }

        public PrNumberSetRuleCombinations WithoutConflicts()
        {
            return new PrNumberSetRuleCombinations(
                Satisfying.WithoutConflicts(), Unsatisfying.WithoutConflicts());
        }
    }
}