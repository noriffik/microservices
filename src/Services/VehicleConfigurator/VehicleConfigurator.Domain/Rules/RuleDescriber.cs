﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace NexCore.VehicleConfigurator.Domain.Rules
{
    public class RuleDescriber : IRuleDescriber
    {
        private readonly char _start;
        private readonly char _end;
        private readonly Regex _validator;

        public RuleDescriber() : this('[', ']', new Regex(@"^[[][ZV][OA][:](([\s][\w\d]{3})+)[+][[].*[\]][\]]"))
        {
        }

        public RuleDescriber(char start, char end, Regex validator)
        {
            _start = start;
            _end = end;
            _validator = validator;
        }
        
        public IReadOnlyCollection<string> GetRules(string ruleSet)
        {
            var rules = Decompose(ruleSet);
            if (rules == null)
                throw new FormatException();

            return rules;
        }

        public IReadOnlyCollection<IRuleDescription> DescribeMany(string ruleSet)
        {
            return GetRules(ruleSet).Select(Describe).ToList();
        }

        public bool IsValid(string ruleSet)
        {
            return Decompose(ruleSet) != null;
        }

        public IRuleDescription Describe(string rule)
        {
            return new RuleDescription(rule);
        }

        private IReadOnlyCollection<string> Decompose(string ruleSet)
        {
            var rules = new List<string>();

            var ruleDepth = 0;
            var startAt = 0;
            
            for (var i = 0; i < ruleSet.Length; i++)
            {
                if (ruleSet[i] == _start)
                {
                    ruleDepth++;

                    if (ruleDepth == 1)
                        startAt = i;

                    continue;
                }

                if (ruleSet[i] == _end)
                {
                    ruleDepth--;

                    if (ruleDepth == 0)
                    {
                        var rule = ruleSet.Substring(startAt, i - startAt + 1);

                        if (!_validator.IsMatch(rule))
                            return null;

                        rules.Add(rule);
                    }
                }
            }

            return ruleDepth != 0 ? null : rules.AsReadOnly();
        }
    }
}