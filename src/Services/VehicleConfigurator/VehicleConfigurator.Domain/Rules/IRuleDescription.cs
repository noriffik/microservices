﻿using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Domain.Rules
{
    public interface IRuleDescription
    {
        string Rule { get; }

        string Name { get; }

        PrNumberSet PrNumbers { get; }

        string Requirement { get; }
    }
}