﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class PackageApplicabilityUpdatedEvent : IEntityEvent
    {
        public PackageId PackageId { get; }

        public PackageApplicabilityUpdatedEvent(PackageId packageId)
        {
            PackageId = packageId ?? throw new ArgumentNullException(nameof(packageId));
        }
    }
}
