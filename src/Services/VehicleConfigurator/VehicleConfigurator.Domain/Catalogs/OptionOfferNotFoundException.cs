﻿using NexCore.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class OptionOfferNotFoundException : OptionOfferException
    {
        private const string DefaultMessage = "Option offer was not found.";
        private const string DefaultExtendedMessage = "Option offer for option {0} for vehicle {1} was not found.";

        public OptionOfferNotFoundException() : base(DefaultMessage)
        {
        }

        public OptionOfferNotFoundException(string message) : base(message)
        {
        }

        public OptionOfferNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public OptionOfferNotFoundException(string message, ModelKey modelKey, PrNumber prNumber, Exception innerException)
            : base(message, modelKey, prNumber, innerException)
        {
        }

        public OptionOfferNotFoundException(string message, ModelKey modelKey, PrNumber prNumber)
            : base(message, modelKey, prNumber)
        {
        }

        public OptionOfferNotFoundException(ModelKey modelKey, PrNumber prNumber)
            : base(string.Format(DefaultExtendedMessage, prNumber, modelKey), modelKey, prNumber)
        {
        }
    }
}
