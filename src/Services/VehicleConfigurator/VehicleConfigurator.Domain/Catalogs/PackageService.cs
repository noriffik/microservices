﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class PackageService : IPackageService
    {
        private readonly IUnitOfWork _work;
        private readonly IModelKeyValidator _modelKeyValidator;

        public PackageService(IUnitOfWork work, IModelKeyValidator modelKeyValidator)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _modelKeyValidator = modelKeyValidator ?? throw new ArgumentNullException(nameof(modelKeyValidator));
        }

        public async Task Add(Package package)
        {
            if (package == null)
                throw new ArgumentNullException(nameof(package));

            await _modelKeyValidator.ValidateMany(package.ModelKeySet.Values);

            var prNumbers = package.Applicability
                .SelectMany(s => s.PrNumbers.Values)
                .Distinct();

            var optionIds = OptionId.Next(package.ModelKeySet.ModelId, prNumbers);

            await _work.EntityRepository.HasManyRequired<Option, OptionId>(optionIds);

            await _work.EntityRepository.HasRequired<Catalog>(package.CatalogId);

            if (await _work.EntityRepository.Has<Package, PackageId>(package.Id))
                throw new DuplicateEntityException(typeof(Package), package.Id);

            await _work.EntityRepository.Add<Package, PackageId>(package);
        }

        private async Task Publish(Package package)
        {
            if (await _work.EntityRepository.Has(new VehicleOffersViolatingPackageSpecification(package), CancellationToken.None))
                throw new PackageRequirementViolationException();

            package.ChangeStatus(PackageStatus.Published);
        }

        public async Task ChangeStatus(PackageId packageId, PackageStatus status)
        {
            var package = await _work.EntityRepository.Require<Package, PackageId>(packageId);

            switch (status)
            {
                case PackageStatus.Published:
                    await Publish(package);
                    break;
                default:
                    package.ChangeStatus(PackageStatus.Draft);
                    break;
            }
        }

        public async Task UpdatePrice(PackageId packageId, decimal price)
        {
            var package = await _work.EntityRepository.Require<Package, PackageId>(packageId);

            package.Price = price;
        }

        public async Task UpdateCoverage(PackageId packageId, IModelKeySet with)
        {
            if (packageId == null)
                throw new ArgumentNullException(nameof(packageId));

            if (with == null)
                throw new ArgumentNullException(nameof(with));

            var package = await _work.EntityRepository.Require<Package, PackageId>(packageId);

            var modelKeysForValidate = with.Values.Except(package.ModelKeySet.Values).ToList();
            if (modelKeysForValidate.Any())
                await _modelKeyValidator.ValidateMany(modelKeysForValidate);

            package.UpdateCoverage(with);
        }

        public async Task UpdateApplicability(PackageId packageId, Applicability applicability)
        {
            if (applicability == null)
                throw new ArgumentNullException(nameof(applicability));

            var package = await _work.EntityRepository.Require<Package, PackageId>(packageId);

            var optionIds = OptionId.Next(package.ModelKeySet.ModelId, applicability.PrNumbers);

            await _work.EntityRepository.HasManyRequired<Option, OptionId>(optionIds);

            package.UpdateApplicability(applicability);
        }
    }
}
