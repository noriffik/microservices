﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class RelevancePeriodExceedsCatalogLifetimeException : DomainException
    {
        private const string DefaultMessage = "Relevance period must be in bounds of catalog period";

        public RelevancePeriodExceedsCatalogLifetimeException() : base(DefaultMessage)
        {
        }

        public RelevancePeriodExceedsCatalogLifetimeException(string message) : base(message)
        {
        }

        public RelevancePeriodExceedsCatalogLifetimeException(string message, Exception inner) : base(message, inner)
        {
        }
    }

}
