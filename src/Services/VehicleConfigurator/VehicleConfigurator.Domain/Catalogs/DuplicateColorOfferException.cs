﻿using NexCore.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class DuplicateColorOfferException : ColorOfferException
    {
        private const string DefaultMessage = "Duplicate color offer.";
        private const string DefaultExtendedMessage = "Duplicate offer of color {0} for vehicle {1}.";

        public DuplicateColorOfferException() : base(DefaultMessage)
        {
        }

        public DuplicateColorOfferException(string message) : base(message)
        {
        }

        public DuplicateColorOfferException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public DuplicateColorOfferException(string message, ModelKey modelKey, ColorId colorId, Exception innerException)
            : base(message, modelKey, colorId, innerException)
        {
        }

        public DuplicateColorOfferException(string message, ModelKey modelKey, ColorId colorId)
            : base(message, modelKey, colorId)
        {
        }

        public DuplicateColorOfferException(ModelKey modelKey, ColorId colorId)
            : this(string.Format(DefaultExtendedMessage, colorId, modelKey), modelKey, colorId)
        {
        }
    }
}
