﻿using NexCore.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class VehicleOfferPublishViolationException : Exception
    {
        private const string DefaultMessage = "For publish vehicle default color offer must be set.";
        private const string DefaultExtendedMessage = "For publish vehicle {0} default color offer must be set.";

        public ModelKey ModelKey { get; }

        public VehicleOfferPublishViolationException() : base(DefaultMessage)
        {
        }

        public VehicleOfferPublishViolationException(string message) : base(message)
        {
        }

        public VehicleOfferPublishViolationException(string message, Exception inner) : base(message, inner)
        {
        }

        public VehicleOfferPublishViolationException(string message, ModelKey modelKey, Exception innerException)
            : this(message, innerException)
        {
            ModelKey = modelKey;
        }

        public VehicleOfferPublishViolationException(string message, ModelKey modelKey)
            : this(message, modelKey, null)
        {
        }

        public VehicleOfferPublishViolationException(ModelKey modelKey)
            : this(string.Format(DefaultExtendedMessage, modelKey), modelKey)
        {
        }
    }
}
