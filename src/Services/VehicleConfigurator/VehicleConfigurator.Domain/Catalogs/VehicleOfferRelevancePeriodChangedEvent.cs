﻿using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class VehicleOfferRelevancePeriodChangedEvent : IEntityEvent
    { 
        public int Id { get; }

        public VehicleOfferRelevancePeriodChangedEvent(int id)
        {
            Id = id;
        }
    }
}
