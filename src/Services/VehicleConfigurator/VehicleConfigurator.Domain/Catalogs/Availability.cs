﻿using System;
using NexCore.Domain;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class Availability : ValueObject
    {
        public AvailabilityType Type { get; private set;}

        public ReasonType? Reason { get; private set;}

        public decimal Price { get; private set;}

        private Availability()
        {
        }
        
        private Availability(AvailabilityType availabilityType, ReasonType? reason, decimal price = 0)
        {
            Type = availabilityType;
            Reason = reason;
            Price = price;
        }

        public static Availability Included(ReasonType reason)
        {
            return new Availability(AvailabilityType.Included, reason);
        }

        public static Availability Purchasable(decimal price)
        {
            return price >= 0 
                ? new Availability(AvailabilityType.Purchasable, null, price) 
                : throw new ArgumentOutOfRangeException(nameof(price));
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return Type;
            yield return Reason;
        }
    }

    public enum AvailabilityType
    {
        Included,
        Purchasable
    }

    public enum ReasonType
    {
        SerialEquipment,
        NationalStandard
    }
}
