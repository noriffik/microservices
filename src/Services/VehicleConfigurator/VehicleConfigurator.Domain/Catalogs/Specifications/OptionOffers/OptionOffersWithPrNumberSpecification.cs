﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.OptionOffers
{
    public class OptionOffersWithPrNumberSpecification : Specification<OptionOffer>
    {
        public PrNumberSet PrNumbers { get; }

        public OptionOffersWithPrNumberSpecification(PrNumberSet prNumbers)
        {
            PrNumbers = prNumbers ?? throw new ArgumentNullException(nameof(prNumbers));
        }

        public override Expression<Func<OptionOffer, bool>> ToExpression()
        {
            return o => PrNumbers.Has(o.PrNumber);
        }

        public override string Description => $"Option offers with PR numbers {PrNumbers}.";
    }
}