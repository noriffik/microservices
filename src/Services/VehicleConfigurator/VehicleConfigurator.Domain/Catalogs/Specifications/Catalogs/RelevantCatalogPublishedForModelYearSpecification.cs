﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs
{
    public class RelevantCatalogPublishedForModelYearSpecification : Specification<Catalog>
    {
        private readonly Specification<Catalog> _inner;

        public Year ModelYear { get; }
        
        public RelevantCatalogPublishedForModelYearSpecification(Year modelYear)
        {
            ModelYear = modelYear;

            _inner = new AllSpecification<Catalog>(
                new CatalogForModelYearSpecification(modelYear),
                new RelevantCatalogSpecification(),
                new PublishedCatalogSpecification());
        }

        public override Expression<Func<Catalog, bool>> ToExpression()
        {
            return _inner.ToExpression();
        }

        public override string Description => _inner.Description;
    }
}
