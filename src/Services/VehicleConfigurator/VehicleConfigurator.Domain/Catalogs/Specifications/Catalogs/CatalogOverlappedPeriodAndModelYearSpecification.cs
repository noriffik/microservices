﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs
{
    public class CatalogOverlappedPeriodAndModelYearSpecification : Specification<Catalog>
    {
        public Year ModelYear { get; }

        public Period Period { get; }

        private readonly Specification<Catalog> _inner;

        public CatalogOverlappedPeriodAndModelYearSpecification(Year modelYear, Period period)
        {
            ModelYear = modelYear ?? throw new ArgumentNullException(nameof(modelYear));
            Period = period ?? throw new ArgumentNullException(nameof(period));

            _inner = new AndSpecification<Catalog>(new CatalogForModelYearSpecification(modelYear),
                new CatalogOverlappedPeriodSpecification(period));
        }

        public override Expression<Func<Catalog, bool>> ToExpression() => _inner.ToExpression();

        public override string Description =>
            $"Catalog for model year {ModelYear.Value} overlapped period from {Period.From} to {Period.To}";
    }
}
