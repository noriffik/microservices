﻿using NexCore.Common;
using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs
{
    public class RelevantCatalogSpecification : Specification<Catalog>
    {
        public override Expression<Func<Catalog, bool>> ToExpression()
        {
            //return catalog => true;
            return catalog => catalog.RelevancePeriod.IsIn(SystemTimeProvider.Instance.Get());
        }

        public override string Description => "Catalog is relevant.";
    }
}
