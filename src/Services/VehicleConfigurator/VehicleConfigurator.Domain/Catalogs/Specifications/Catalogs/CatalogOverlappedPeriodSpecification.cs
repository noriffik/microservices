﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs
{
    public class CatalogOverlappedPeriodSpecification : Specification<Catalog>
    {
        public Period Period { get; }

        public CatalogOverlappedPeriodSpecification(Period period)
        {
            Period = period ?? throw new ArgumentNullException(nameof(period));
        }

        public override Expression<Func<Catalog, bool>> ToExpression()
        {
            return catalog =>
                !(catalog.RelevancePeriodFrom < Period.From && catalog.RelevancePeriodTo < Period.From ||
                  catalog.RelevancePeriodFrom > Period.From && catalog.RelevancePeriodFrom > Period.To);
        }

        public override string Description => $"Catalog overlapped period from {Period.From} to {Period.To}";
    }
}
