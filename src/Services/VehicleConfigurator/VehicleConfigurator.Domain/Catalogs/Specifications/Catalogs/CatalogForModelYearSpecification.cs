﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs
{
    public class CatalogForModelYearSpecification : Specification<Catalog>
    {
        public Year ModelYear { get; }

        public CatalogForModelYearSpecification(Year modelYear)
        {
            ModelYear = modelYear ?? throw new ArgumentNullException(nameof(modelYear));
        }

        public override Expression<Func<Catalog, bool>> ToExpression()
        {
            return catalog => Convert.ToInt32(catalog.ModelYear) == ModelYear.Value;
        }

        public override string Description => $"Catalog for model year {ModelYear}.";
    }
}
