﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs
{
    public class RelevantPublishedCatalogSpecification : Specification<Catalog>
    {
        private readonly Specification<Catalog> _inner;

        public RelevantPublishedCatalogSpecification()
        {
            _inner = new AllSpecification<Catalog>(
                new RelevantCatalogSpecification(), 
                new PublishedCatalogSpecification());
        }

        public override Expression<Func<Catalog, bool>> ToExpression()
        {
            return _inner.ToExpression();
        }

        public override string Description => _inner.Description;
    }
}
