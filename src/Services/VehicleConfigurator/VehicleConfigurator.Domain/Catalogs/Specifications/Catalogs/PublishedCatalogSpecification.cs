﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs
{
    public class PublishedCatalogSpecification : Specification<Catalog>
    {
        public override Expression<Func<Catalog, bool>> ToExpression()
        {
            return catalog => catalog.Status == CatalogStatus.Published;
        }

        public override string Description => "Catalog is published.";
    }
}
