﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferEngineIdSpecification : Specification<VehicleOffer>
    {
        public EngineId EngineId { get; }

        public VehicleOfferEngineIdSpecification(EngineId engineId)
        {
            EngineId = engineId ?? throw new ArgumentNullException(nameof(engineId));
        }

        public override Expression<Func<VehicleOffer, bool>> ToExpression()
        {
            return vehicleOffer => Convert.ToString(vehicleOffer.EngineId) == EngineId.Value;
        }

        public override string Description => $"Vehicle offer with engine with id {EngineId}";
    }
}
