﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferApplicableToPackageSpecification : Specification<VehicleOffer>
    {
        public Applicability Applicability { get; }

        public VehicleOfferApplicableToPackageSpecification(Applicability applicability)
        {
            Applicability = applicability;
        }

        public override Expression<Func<VehicleOffer, bool>> ToExpression()
        {
            return offer => offer.PrNumbers.Has(Applicability.PrNumbers);
        }

        public override string Description => $"Vehicle offer satisfy package applicability: {Applicability.AsString}";
    }
}
