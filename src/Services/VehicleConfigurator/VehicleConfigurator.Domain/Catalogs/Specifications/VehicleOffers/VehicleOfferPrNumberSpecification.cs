﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferPrNumberSpecification : Specification<VehicleOffer>
    {
        public PrNumber PrNumber { get; }

        public VehicleOfferPrNumberSpecification(PrNumber prNumber)
        {
            PrNumber = prNumber ?? throw new ArgumentNullException(nameof(prNumber));
        }

        public override Expression<Func<VehicleOffer, bool>> ToExpression()
        {
            return vehicleOffer => Convert.ToString(vehicleOffer.PrNumbers).Contains(PrNumber.Value);
        }

        public override string Description => $"Vehicle offer included option offer with prNumber {PrNumber}";
    }
}
