﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferCatalogIdSpecification : Specification<VehicleOffer>
    {
        public int CatalogId { get; }

        public VehicleOfferCatalogIdSpecification(int catalogId)
        {
            CatalogId = catalogId;
        }

        public override Expression<Func<VehicleOffer, bool>> ToExpression()
        {
            return vehicleOffer => vehicleOffer.CatalogId == CatalogId;
        }

        public override string Description => $"VehicleOffer in Catalog {CatalogId}";
    }
}
