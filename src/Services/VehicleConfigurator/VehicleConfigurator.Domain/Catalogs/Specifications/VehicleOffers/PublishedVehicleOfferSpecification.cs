﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class PublishedVehicleOfferSpecification : Specification<VehicleOffer>
    {
        public int CatalogId { get; }

        public ModelKey ModelKey { get; }

        private readonly Specification<VehicleOffer> _inner;

        public PublishedVehicleOfferSpecification(int catalogId, ModelKey modelKey)
        {
            CatalogId = catalogId;
            ModelKey = modelKey ?? throw new ArgumentNullException( nameof(modelKey));

            _inner = new AndSpecification<VehicleOffer>(
                new VehicleOfferForVehicleSpecification(catalogId, modelKey),
                new VehicleOfferStatusSpecification(VehicleOfferStatus.Published));
        }
        
        public override Expression<Func<VehicleOffer, bool>> ToExpression()
        {
            return _inner.ToExpression();
        }

        public override string Description =>
            $@"Published vehicle offer for vehicle with model key ""{ModelKey}"" in catalog with id ""{CatalogId}""";
    }
}
