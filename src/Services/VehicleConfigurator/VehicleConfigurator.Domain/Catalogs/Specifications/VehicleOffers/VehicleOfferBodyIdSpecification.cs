﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferBodyIdSpecification : Specification<VehicleOffer>
    {
        public BodyId BodyId { get; }

        public VehicleOfferBodyIdSpecification(BodyId bodyId)
        {
            BodyId = bodyId ?? throw new ArgumentNullException(nameof(bodyId));
        }

        public override Expression<Func<VehicleOffer, bool>> ToExpression()
        {
            return vehicleOffer => Convert.ToString(vehicleOffer.BodyId) == BodyId.Value;
        }

        public override string Description => $"Vehicle offer with body with id {BodyId}";
    }
}
