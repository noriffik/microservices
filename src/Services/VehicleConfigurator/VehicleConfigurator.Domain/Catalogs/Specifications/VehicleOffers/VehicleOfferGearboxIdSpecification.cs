﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferGearboxIdSpecification : Specification<VehicleOffer>
    {
        public GearboxId GearboxId { get; }

        public VehicleOfferGearboxIdSpecification(GearboxId gearboxId)
        {
            GearboxId = gearboxId ?? throw new ArgumentNullException(nameof(gearboxId));
        }

        public override Expression<Func<VehicleOffer, bool>> ToExpression()
        {
            return vehicleOffer => Convert.ToString(vehicleOffer.GearboxId) == GearboxId.Value;
        }

        public override string Description => $"Vehicle offer for vehicle with gearbox with id {GearboxId}";
    }
}
