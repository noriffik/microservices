﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferModelIdSpecification : Specification<VehicleOffer>
    {
        public ModelId ModelId { get; }

        public VehicleOfferModelIdSpecification(ModelId modelId)
        {
            ModelId = modelId;
        }

        public override Expression<Func<VehicleOffer, bool>> ToExpression()
        {
            return vehicleOffer => Convert.ToString(vehicleOffer.ModelId) == ModelId.Value;
        }

        public override string Description => $@"Vehicle offer for vehicle with model id ""{ModelId.Value}""";
    }
}
