﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferModelKeySpecification : Specification<VehicleOffer>
    {
        public ModelKeySet ModelKeySet { get; }

        public VehicleOfferModelKeySpecification(ModelKey modelKey)
        {
            if (modelKey == null)
                throw new ArgumentNullException(nameof(modelKey));

            ModelKeySet = new ModelKeySet(new []{ modelKey });
        }

        public VehicleOfferModelKeySpecification(ModelKeySet modelKeySet)
        {
            ModelKeySet = modelKeySet ?? throw new ArgumentNullException(nameof(modelKeySet));
        }

        public override Expression<Func<VehicleOffer, bool>> ToExpression()
        {
            return vehicleOffer => ModelKeySet.AsString.Contains(Convert.ToString(vehicleOffer.ModelKey));
        }

        public override string Description => $@"Vehicle offer for vehicle with one of model key ""{ModelKeySet}""";
    }
}
