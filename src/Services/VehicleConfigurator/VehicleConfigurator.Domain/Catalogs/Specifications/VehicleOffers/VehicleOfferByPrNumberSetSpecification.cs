﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferByPrNumberSetSpecification : Specification<VehicleOffer>
    {
        public PrNumberSet PrNumberSet { get; }

        public VehicleOfferByPrNumberSetSpecification(PrNumberSet prNumberSet)
        {
            PrNumberSet = prNumberSet ?? throw new ArgumentNullException(nameof(prNumberSet));
        }

        public override Expression<Func<VehicleOffer, bool>> ToExpression()
        {
            return vehicleOffer => vehicleOffer.PrNumbers.Has(PrNumberSet);
        }

        public override string Description => $@"VehicleOffer with options ""{PrNumberSet.Values}""";
    }
}
