﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOffersViolatingPackageSpecification : Specification<VehicleOffer>
    {
        public Package Package { get; }

        private readonly Specification<VehicleOffer> _inner;

        public VehicleOffersViolatingPackageSpecification(Package package)
        {
            Package = package ?? throw new ArgumentNullException(nameof(package));
            
            _inner = new AndSpecification<VehicleOffer>(
                new VehicleOfferCatalogIdSpecification(package.CatalogId),
                new AndSpecification<VehicleOffer>(
                    new VehicleOfferCoveredByPackageSpecification(Package.ModelKeySet),
                    new NotSpecification<VehicleOffer>(
                        new VehicleOfferApplicableToPackageSpecification(Package.Applicability))));
        }

        public override Expression<Func<VehicleOffer, bool>> ToExpression()
        {
            return _inner.ToExpression();
        }

        public override string Description => _inner.Description;
    }
}
