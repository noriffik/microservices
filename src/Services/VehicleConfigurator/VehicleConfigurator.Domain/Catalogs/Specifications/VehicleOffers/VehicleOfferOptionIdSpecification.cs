﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferOptionIdSpecification : Specification<VehicleOffer>
    {
        public OptionId OptionId { get; }

        private readonly Specification<VehicleOffer> _inner;

        public VehicleOfferOptionIdSpecification(OptionId optionId)
        {
            OptionId = optionId ?? throw new ArgumentNullException(nameof(optionId));

            _inner = new AndSpecification<VehicleOffer>(
                new VehicleOfferModelIdSpecification(optionId.ModelId),
                new VehicleOfferPrNumberSpecification(optionId.PrNumber));
        }

        public override Expression<Func<VehicleOffer, bool>> ToExpression() => _inner.ToExpression();

        public override string Description => $"Vehicle offer for vehicle with model {OptionId.ModelId.Value} " +
                                              $"included option with prNumber {OptionId.PrNumber}";
    }
}
