﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferEquipmentIdSpecification : Specification<VehicleOffer>
    {
        public EquipmentId EquipmentId { get; }

        public VehicleOfferEquipmentIdSpecification(EquipmentId equipmentId)
        {
            EquipmentId = equipmentId ?? throw new ArgumentNullException(nameof(equipmentId));
        }

        public override Expression<Func<VehicleOffer, bool>> ToExpression()
        {
            return vehicleOffer => Convert.ToString(vehicleOffer.EquipmentId) == EquipmentId.Value;
        }

        public override string Description => $"Vehicle offer with equipment with id {EquipmentId}";
    }
}
