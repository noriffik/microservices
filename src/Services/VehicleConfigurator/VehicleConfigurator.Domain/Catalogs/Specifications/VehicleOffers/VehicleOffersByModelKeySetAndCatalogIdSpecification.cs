﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOffersByModelKeySetAndCatalogIdSpecification : Specification<VehicleOffer>
    {
        private readonly Specification<VehicleOffer> _inner;

        public int CatalogId { get; }

        public ModelKeySet ModelKeySet { get; }

        public VehicleOffersByModelKeySetAndCatalogIdSpecification(int catalogId, ModelKeySet modelKeySet)
        {
            _inner = new AndSpecification<VehicleOffer>(new VehicleOfferCatalogIdSpecification(catalogId),
                new VehicleOfferModelKeySpecification(modelKeySet));

            CatalogId = catalogId;
            ModelKeySet = modelKeySet;
        }

        public override Expression<Func<VehicleOffer, bool>> ToExpression()
        {
            return _inner.ToExpression();
        }

        public override string Description => $"VehicleOffer in CatalogId {CatalogId} and in {ModelKeySet}";
    }
}
