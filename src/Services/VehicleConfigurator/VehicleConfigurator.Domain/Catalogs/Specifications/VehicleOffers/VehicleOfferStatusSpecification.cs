﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferStatusSpecification : Specification<VehicleOffer>
    {
        private readonly VehicleOfferStatus _status;

        public VehicleOfferStatusSpecification(VehicleOfferStatus status)
        {
            _status = status;
        }

        public override Expression<Func<VehicleOffer, bool>> ToExpression()
        {
            return vehicleOffer => vehicleOffer.Status == _status;
        }

        public override string Description => $"Vehicle offer in {_status} status";
    }
}
