﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferCoveredByPackageSpecification : Specification<VehicleOffer>
    {
        public BoundModelKeySet ModelKeySet { get; }

        public VehicleOfferCoveredByPackageSpecification(BoundModelKeySet modelKeySet)
        {
            ModelKeySet = modelKeySet;
        }

        public override Expression<Func<VehicleOffer, bool>> ToExpression()
        {
            return vehicleOffer => Convert.ToString(ModelKeySet).Contains(vehicleOffer.ModelKey);
        }

        public override string Description => $"Vehicle offer is for one of vehicle: {ModelKeySet.AsString}";
    }
}
