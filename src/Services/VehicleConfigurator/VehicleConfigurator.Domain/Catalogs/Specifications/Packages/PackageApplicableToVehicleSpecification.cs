﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Packages
{
    public class PackageApplicableToVehicleSpecification : Specification<Package>
    {
        private readonly string _modelKey;

        public PackageApplicableToVehicleSpecification(ModelKey modelKey)
        {
            if (modelKey == null)
                throw new ArgumentNullException(nameof(modelKey));

            _modelKey = modelKey.Value;
        }

        public override Expression<Func<Package, bool>> ToExpression()
        {
            return p => Convert.ToString(p.ModelKeySet).Contains(_modelKey);
        }

        public override string Description => $@"Package cover vehicle with model key ""{_modelKey}""";
    }
}
