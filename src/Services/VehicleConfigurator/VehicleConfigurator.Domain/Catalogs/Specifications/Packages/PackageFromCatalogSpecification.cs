﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Packages
{
    public class PackageFromCatalogSpecification : Specification<Package>
    {
        public int CatalogId { get; }

        public PackageFromCatalogSpecification(int catalogId)
        {
            CatalogId = catalogId;
        }

        public PackageFromCatalogSpecification(Catalog catalog)
        {
            if (catalog == null)
                throw new ArgumentNullException(nameof(catalog));

            CatalogId = catalog.Id;
        }

        public override Expression<Func<Package, bool>> ToExpression()
        {
            return package => package.CatalogId == CatalogId;
        }

        public override string Description => $"Package in Catalog {CatalogId}";
    }
}
