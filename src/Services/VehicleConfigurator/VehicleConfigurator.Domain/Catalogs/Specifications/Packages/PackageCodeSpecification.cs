﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Packages
{
    public class PackageCodeSpecification : Specification<Package>
    {
        public PackageCode Code { get; }

        public PackageCodeSpecification(PackageCode code)
        {
            Code = code ?? throw new ArgumentNullException(nameof(code));
        }

        public override Expression<Func<Package, bool>> ToExpression()
        {
            return package => Convert.ToString(package.Id).Substring(2) == Code.Value;
        }

        public override string Description => $"Package with code {Code.Value}";
    }
}
