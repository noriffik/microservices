﻿using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Packages
{
    public class PublishedPackageIdSpecification : AndSpecification<Package>
    {
        public PublishedPackageIdSpecification(PackageId id) : base(
            new IdSpecification<Package, PackageId>(id),
            new PackageStatusSpecification(PackageStatus.Published))
        {
        }
    }
}
