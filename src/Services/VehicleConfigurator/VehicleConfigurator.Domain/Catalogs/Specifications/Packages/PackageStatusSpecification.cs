﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Packages
{
    public class PackageStatusSpecification : Specification<Package>
    {
        private readonly PackageStatus _status;

        public PackageStatusSpecification(PackageStatus status)
        {
            _status = status;
        }

        public override Expression<Func<Package, bool>> ToExpression()
        {
            return package => package.Status == _status;
        }

        public override string Description => $"Package in {_status} status.";
    }
}
