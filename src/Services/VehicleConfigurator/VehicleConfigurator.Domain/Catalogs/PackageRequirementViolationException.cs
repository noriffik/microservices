﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class PackageRequirementViolationException : Exception
    {
        private const string DefaultMessage = "Package option requirement was violated.";
        private const string DefaultExtendedMessage = "Package {0} requires following options: {1}";

        public Package Package { get; }

        public PrNumberSet Included { get; }

        [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Provided as per Microsoft recommendations")]
        public PackageRequirementViolationException() : base(DefaultMessage)
        {
        }

        public PackageRequirementViolationException(string message) : base(message)
        {
        }

        public PackageRequirementViolationException(string message, Exception inner) : base(message, inner)
        {
        }

        public PackageRequirementViolationException(string message, Package package, PrNumberSet included, Exception innerException)
            : this(message, innerException)
        {
            Package = package;
            Included = included;
        }

        public PackageRequirementViolationException(string message, Package package, PrNumberSet included)
            : this(message, package, included, null)
        {
        }

        public PackageRequirementViolationException(Package package, PrNumberSet included)
            : this(string.Format(DefaultExtendedMessage, package.Id.Value,
                CreateBasedOnViolated(package.Applicability, included)), package, included)
        {
        }

        public PackageRequirementViolationException(string packageId, PrNumber missing)
            : this(string.Format(DefaultExtendedMessage, packageId, missing))
        {
        }

        private static Applicability CreateBasedOnViolated(Applicability applicability, PrNumberSet prNumbers)
        {
            applicability.IsSatisfied(prNumbers, out var violated);

            return new Applicability(violated);
        }
    }
}
