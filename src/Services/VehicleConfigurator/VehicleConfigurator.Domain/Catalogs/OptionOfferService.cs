﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Rules;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class OptionOfferService : IOptionOfferService
    {
        private readonly IModelKeyValidator _modelKeyValidator;
        private readonly IUnitOfWork _work;

        private IRuleParser _parser;

        public IRuleParser Parser
        {
            get => _parser ?? (_parser = new RuleParser());
            set => _parser = value ?? throw new ArgumentNullException(nameof(value));
        }

        private IRuleValidationService _ruleValidationService;

        public IRuleValidationService RuleValidationService
        {
            get => _ruleValidationService ?? (_ruleValidationService = new RuleValidationService());
            set => _ruleValidationService = value ?? throw new ArgumentNullException(nameof(value));
        }

        public OptionOfferService(IUnitOfWork work, IModelKeyValidator modelKeyValidator)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _modelKeyValidator = modelKeyValidator ?? throw new ArgumentNullException(nameof(modelKeyValidator));
        }

        public async Task Add(int vehicleOfferId, PrNumber prNumber, Availability availability, Inclusion inclusion)
        {
            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(vehicleOfferId);

            await _work.EntityRepository.HasRequired<Option, OptionId>(OptionId.Next(vehicleOffer.ModelId, prNumber));

            vehicleOffer.AddOptionOffer(prNumber, availability, inclusion);
        }

        public async Task UpdateAvailability(int vehicleOfferId, PrNumber prNumber, Availability availability)
        {
            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(vehicleOfferId);

            vehicleOffer.UpdateOptionOfferAvailability(prNumber, availability);
        }

        public async Task UpdateInclusion(int vehicleOfferId, PrNumber prNumber, Inclusion inclusion)
        {
            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(vehicleOfferId);

            vehicleOffer.UpdateOptionOfferInclusion(prNumber, inclusion);
        }

        public async Task Delete(int vehicleOfferId, PrNumber prNumber)
        {
            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(vehicleOfferId);

            vehicleOffer.DeleteOptionOffer(prNumber);
        }

        public async Task UpdateAvailabilities(PrNumber prNumber, BoundModelKeyDictionary<Availability> optionOffers)
        {
            await _work.EntityRepository.HasRequired<Option, OptionId>(OptionId.Next(optionOffers.ModelId, prNumber));
            await _modelKeyValidator.ValidateMany(optionOffers.Keys);

            var vehicleOffers = (await _work.EntityRepository.Find(
                    new VehicleOfferModelIdSpecification(optionOffers.ModelId)))
                .ToList();
            if (!vehicleOffers.Any())
                throw new InvalidEntityIdException(typeof(VehicleOffer), optionOffers.ModelId);

            var toUpdate = vehicleOffers
                .Where(v => v.HasOptionOffer(prNumber))
                .Where(v => optionOffers.ContainsKey(v.ModelKey))
                .ToList();

            var toDelete = vehicleOffers
                .Where(v => v.HasOptionOffer(prNumber))
                .Where(v => !optionOffers.ContainsKey(v.ModelKey))
                .ToList();

            var toAdd = vehicleOffers
                .Except(toUpdate)
                .Except(toDelete)
                .Where(v => optionOffers.ContainsKey(v.ModelKey))
                .ToList();

            toUpdate.ForEach(v => v.UpdateOptionOfferAvailability(prNumber, optionOffers[v.ModelKey]));
            toDelete.ForEach(v => v.DeleteOptionOffer(prNumber));
            toAdd.ForEach(v => v.AddOptionOffer(prNumber, optionOffers[v.ModelKey]));
        }

        public async Task RemoveRule(int vehicleOfferId, PrNumber prNumber)
        {
            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(vehicleOfferId);

            vehicleOffer.AddOptionOfferRuleSet(prNumber, "");
        }

        public async Task AddRuleSet(int vehicleOfferId, PrNumber prNumber, string ruleSet)
        {

            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(vehicleOfferId);

            ValidateRuleSet(ruleSet, prNumber, vehicleOffer);

            vehicleOffer.AddOptionOfferRuleSet(prNumber, ruleSet);
        }

        public async Task AddRuleSet(int catalogId, ModelKeySet set, OptionId optionId, string ruleSet)
        {
            if (set == null)
                throw new ArgumentNullException(nameof(set));

            if (optionId == null)
                throw new ArgumentNullException(nameof(optionId));

            var vehicleOffers = await _work.EntityRepository.Find(new VehicleOffersByModelKeySetAndCatalogIdSpecification(catalogId, set));

            foreach (var vehicleOffer in vehicleOffers)
            {
                ValidateRuleSet(ruleSet, optionId.PrNumber, vehicleOffer);

                vehicleOffer.AddOptionOfferRuleSet(optionId.PrNumber, ruleSet);
            }
        }

        private void ValidateRuleSet(string ruleSet, PrNumber prNumber, VehicleOffer vehicleOffer)
        {
            if (ruleSet == null)
                throw new ArgumentNullException(nameof(ruleSet));

            if (!Parser.IsValid(ruleSet))
                throw new FormatException();

            if (RuleValidationService.HasConflict(ruleSet, prNumber, vehicleOffer))
                throw new RuleConflictException();
        }

        public async Task ChangeRelevancePeriod(int vehicleOfferId, PrNumber prNumber, RelevancePeriod relevancePeriod)
        {
            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));
            if (relevancePeriod == null)
                throw new ArgumentNullException(nameof(relevancePeriod));

            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(vehicleOfferId);
            var catalog = await _work.EntityRepository.Require<Catalog>(vehicleOffer.CatalogId);

            relevancePeriod.VerifyThatIsInBounds(catalog);

            vehicleOffer.ChangeOptionOfferRelevancePeriod(prNumber, relevancePeriod);
        }
    }
}