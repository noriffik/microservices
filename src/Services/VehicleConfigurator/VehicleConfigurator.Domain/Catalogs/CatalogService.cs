﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class CatalogService : ICatalogService
    {
        private readonly IUnitOfWork _work;

        public CatalogService(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Add(Catalog catalog)
        {
            if (await _work.EntityRepository.Has(new CatalogOverlappedPeriodAndModelYearSpecification(catalog.ModelYear, catalog.RelevancePeriod)))
                throw new DuplicateCatalogException();

            await _work.EntityRepository.Add(catalog);
        }

        public async Task ChangeStatus(int id, CatalogStatus status)
        {
            var catalog = await _work.EntityRepository.Require<Catalog>(id);

            catalog.Status = status;
        }

        public async Task ChangeRelevancePeriodEnding(int id, DateTime periodTo)
        {
            var catalog = await _work.EntityRepository.Require<Catalog>(id);

            if (catalog.RelevancePeriodTo.HasValue && await _work.EntityRepository.Has(
                    new CatalogOverlappedPeriodAndModelYearSpecification(
                        catalog.ModelYear, new Period(catalog.RelevancePeriodTo.Value.AddDays(1), periodTo))))
                throw new DuplicateCatalogException();

            catalog.ChangeRelevancePeriodEnding(periodTo);
        }

        public async Task ChangeRelevancePeriod(int id, RelevancePeriod relevancePeriod)
        {
            if (relevancePeriod == null) throw new ArgumentNullException(nameof(relevancePeriod));

            var catalog = await _work.EntityRepository.Require<Catalog>(id);

            if (catalog.RelevancePeriodTo.HasValue && await _work.EntityRepository.Has(
                    new CatalogOverlappedPeriodAndModelYearSpecification(
                        catalog.ModelYear, new Period(catalog.RelevancePeriodTo.Value.AddDays(1), relevancePeriod.To))))
                throw new DuplicateCatalogException();

            catalog.ChangeRelevancePeriod(new Period(relevancePeriod.From.GetValueOrDefault(), relevancePeriod.To));
        }
    }
}
