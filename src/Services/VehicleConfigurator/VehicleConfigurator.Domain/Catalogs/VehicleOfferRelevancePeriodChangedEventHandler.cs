﻿using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using NexCore.Common;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class VehicleOfferRelevancePeriodChangedEventHandler : IEntityEventHandler<VehicleOfferRelevancePeriodChangedEvent>
    {
        private readonly IUnitOfWork _work;

        public VehicleOfferRelevancePeriodChangedEventHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Handle(VehicleOfferRelevancePeriodChangedEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(notification.Id, cancellationToken);

            var date = SystemTimeProvider.Instance.Get();

            var relevance = vehicleOffer.RelevancePeriod.IsIn(date)
                ? Relevance.Relevant
                : Relevance.Irrelevant;

            vehicleOffer.ChangeRelevance(relevance);
        }
    }
}
