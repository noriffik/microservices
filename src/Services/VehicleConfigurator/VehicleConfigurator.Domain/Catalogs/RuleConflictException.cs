﻿using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class RuleConflictException : Exception
    {
        private const string DefaultMessage = "Rule conflict detected!";

        public RuleConflictException() : base(DefaultMessage)
        {
        }

        public RuleConflictException(string message) : base(message)
        {
        }

        public RuleConflictException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
