﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class PackageMarkedAsDraftEvent : IEntityEvent
    {
        public PackageId PackageId;

        public PackageMarkedAsDraftEvent(PackageId packageId)
        {
            PackageId = packageId ?? throw new ArgumentNullException(nameof(packageId));
        }
    }
}
