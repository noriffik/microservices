﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class OptionOffer : Entity, IPotentiallyIrrelevant
    {
        public OptionId OptionId { get; private set; }

        public PrNumber PrNumber => OptionId.PrNumber;

        public Availability Availability { get; set; }

        public string RuleSet { get; private set; }

        public Inclusion Inclusion { get; set; }

        public Relevance Relevance { get; private set; }

        public RelevancePeriod RelevancePeriod => RelevancePeriodFrom.HasValue
            ? new RelevancePeriod(RelevancePeriodFrom.Value, RelevancePeriodTo)
            : RelevancePeriod.Empty;

        public DateTime? RelevancePeriodFrom { get; private set; }

        public DateTime? RelevancePeriodTo { get; private set; }

        public bool IsRestricted { get; private set; }

        [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
        private OptionOffer()
        {
        }

        public OptionOffer(OptionId optionId, Availability availability, Inclusion inclusion = default(Inclusion))
        {
            OptionId = optionId ?? throw new ArgumentNullException(nameof(optionId));
            Availability = availability ?? throw new ArgumentNullException(nameof(availability));
            RuleSet = string.Empty;
            Inclusion = inclusion;
            Relevance = Relevance.Relevant;
        }

        internal void AssignRules(string ruleSet)
        {
            if (ruleSet == null)
                throw new ArgumentNullException(nameof(ruleSet));

            if (ruleSet.Contains(PrNumber.Value))
                throw new SelfReferenceRuleException();

            RuleSet = ruleSet;

           
        }

        public void ChangeRelevance(Relevance relevance)
        {
            Relevance = relevance;
        }

        public void ChangeRelevancePeriod(RelevancePeriod relevancePeriod)
        {
            if (relevancePeriod == null)
                throw new ArgumentNullException(nameof(relevancePeriod));

            RelevancePeriodFrom = relevancePeriod.From;
            RelevancePeriodTo = relevancePeriod.To;
        }

        internal void EnforceRestriction()
        {
            IsRestricted = true;
        }

        internal void LiftRestriction()
        {
            IsRestricted = false;
        }
    }

    public enum Inclusion
    {
        Manual = default(int),
        Automatic = 1
    }
}
