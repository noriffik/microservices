﻿using NexCore.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class ColorOfferException : Exception
    {
        public ModelKey ModelKey { get; }

        public ColorId ColorId { get; }

        protected ColorOfferException() : base()
        {
        }

        protected ColorOfferException(string message) : base(message)
        {
        }

        protected ColorOfferException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ColorOfferException(string message, ModelKey modelKey, ColorId colorId, Exception innerException)
            : this(message, innerException)
        {
            ModelKey = modelKey;
            ColorId = colorId;
        }

        protected ColorOfferException(string message, ModelKey modelKey, ColorId colorId)
            : this(message, modelKey, colorId, null)
        {
        }
    }
}
