﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class ColorOfferRelevancePeriodChangedEvent : IEntityEvent
    {
        public int VehicleOfferId { get; }

        public ColorId ColorId { get; }

        public ColorOfferRelevancePeriodChangedEvent(int vehicleOfferId, ColorId colorId)
        {
            VehicleOfferId = vehicleOfferId;
            ColorId = colorId ?? throw new ArgumentNullException(nameof(colorId));
        }
    }
}
