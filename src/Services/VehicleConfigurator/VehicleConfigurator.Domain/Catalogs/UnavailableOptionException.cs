﻿using NexCore.VehicleConfigurator.Domain.Rules;
using System;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class UnavailableOptionException : Exception
    {
        private const string DefaultMessage = "Given option is unavailable for this configuration.";

        public UnavailableOptionException() : base(DefaultMessage)
        {
        }

        public UnavailableOptionException(string message) : base(message)
        {
        }

        public IEnumerable<RuleViolation> Details { get; set; } = new List<RuleViolation>();
    }
}
