﻿using NexCore.Domain.Vehicles;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public interface IOptionOfferService
    {
        Task Add(int vehicleOfferId, PrNumber prNumber, Availability availability, Inclusion inclusion = Inclusion.Manual);

        Task UpdateAvailabilities(PrNumber prNumber, BoundModelKeyDictionary<Availability> modelOptionOffers);

        Task UpdateAvailability(int vehicleOfferId, PrNumber prNumber, Availability availability);

        Task UpdateInclusion(int vehicleOfferId, PrNumber prNumber, Inclusion inclusion);

        Task Delete(int vehicleOfferId, PrNumber prNumber);

        Task RemoveRule(int vehicleOfferId, PrNumber prNumber);

        Task AddRuleSet(int vehicleOfferId, PrNumber prNumber, string ruleSet);

        Task AddRuleSet(int catalogId, ModelKeySet set, OptionId optionId, string ruleSet);

        Task ChangeRelevancePeriod(int vehicleOfferId, PrNumber prNumber, RelevancePeriod relevancePeriod);
    }
}