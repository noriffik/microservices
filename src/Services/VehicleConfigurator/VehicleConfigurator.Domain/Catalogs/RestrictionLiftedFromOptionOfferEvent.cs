﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class RestrictionLiftedFromOptionOfferEvent : IEntityEvent
    {
        public OptionOffersAffectedByRestriction Affected { get; }

        public RestrictionLiftedFromOptionOfferEvent(OptionOffersAffectedByRestriction affected)
        {
            Affected = affected ?? throw new ArgumentNullException(nameof(affected));
        }
    }
}
