﻿using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class SelfReferenceRuleException : Exception
    {
        private const string DefaultMessage = "Rule for given option cannot contain option itself";

        public SelfReferenceRuleException() : base(DefaultMessage)
        {
        }

        public SelfReferenceRuleException(string message) : base(message)
        {
        }

        public SelfReferenceRuleException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
