﻿using NexCore.Domain.Vehicles;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public interface IVehicleOfferService
    {
        Task Add(VehicleOffer vehicleOffer);

        Task AddColorOffer(int vehicleOfferId, ColorId colorId, decimal price);

        Task ChangeStatus(int vehicleOfferId, VehicleOfferStatus status);

        Task SetDefaultColor(int vehicleOfferId, ColorId colorId);

        Task ChangeColorOfferRelevancePeriod(int vehicleOfferId, ColorId colorId, RelevancePeriod relevancePeriod);

        Task ChangeColorOfferPrice(int vehicleOfferId, ColorId colorId, decimal price);

        Task ChangeRelevancePeriod(int vehicleOfferId, RelevancePeriod period);
    }
}
