﻿using NexCore.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class OptionOffersAffectedByRestriction
    {
        public int CatalogId { get; }

        public ModelKey ModelKey { get; }

        public PrNumberSet PrNumbers { get; }

        public OptionOffersAffectedByRestriction(int catalogId, ModelKey modelKey, PrNumberSet prNumbers)
        {
            CatalogId = catalogId;
            ModelKey = modelKey ?? throw new ArgumentNullException(nameof(modelKey));
            PrNumbers = prNumbers ?? throw new ArgumentNullException(nameof(prNumbers));
        }
    }
}
