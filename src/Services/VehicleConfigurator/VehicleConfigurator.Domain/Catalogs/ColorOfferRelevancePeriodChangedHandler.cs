﻿using NexCore.Common;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class ColorOfferRelevancePeriodChangedHandler : IEntityEventHandler<ColorOfferRelevancePeriodChangedEvent>
    {
        private readonly IUnitOfWork _work;

        public ColorOfferRelevancePeriodChangedHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Handle(ColorOfferRelevancePeriodChangedEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(notification.VehicleOfferId, cancellationToken);

            var colorOffer = vehicleOffer.FindColorOfferOrThrow(notification.ColorId);

            var nowDate = SystemTimeProvider.Instance.Get();

            var relevance = colorOffer.RelevancePeriod.IsIn(nowDate) ? Relevance.Relevant : Relevance.Irrelevant;

            vehicleOffer.ChangeColorOfferRelevance(notification.ColorId, relevance);
        }
    }
}
