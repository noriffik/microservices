﻿using System.Diagnostics.CodeAnalysis;
using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public class PackageCode : EntityId
    {
        private const int Length = 3;

        protected PackageCode()
        {
        }

        protected PackageCode(string value) : base(value)
        {
        }

        public static PackageCode Parse(string value)
        {
            return AlphaNumericEntityIdParser.Instance.Parse<PackageCode>(value, Length);
        }

        public static bool TryParse(string value, out PackageCode prNumber)
        {
            return AlphaNumericEntityIdParser.Instance.TryParse(value, out prNumber, Length);
        }

        public static implicit operator string(PackageCode code)
        {
            return code?.Value;
        }

        public static implicit operator PackageCode(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
