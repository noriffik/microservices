﻿using NexCore.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public abstract class OptionOfferException : Exception
    {
        public ModelKey ModelKey { get; }

        public PrNumber PrNumber { get; }

        protected OptionOfferException() : base()
        {
        }

        protected OptionOfferException(string message) : base(message)
        {
        }

        protected OptionOfferException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected OptionOfferException(string message, ModelKey modelKey, PrNumber prNumber, Exception innerException)
            : this(message, innerException)
        {
            ModelKey = modelKey;
            PrNumber = prNumber;
        }

        protected OptionOfferException(string message, ModelKey modelKey, PrNumber prNumber)
            : this(message, modelKey, prNumber, null)
        {
        }
    }
}
