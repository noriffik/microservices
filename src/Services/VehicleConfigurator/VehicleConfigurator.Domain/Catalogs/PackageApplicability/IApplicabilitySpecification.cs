﻿using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability
{
    public interface IApplicabilitySpecification
    {
        string AsString { get; }

        ApplicabilitySpecificationType Type { get; }

        PrNumberSet PrNumbers { get; }

        bool IsSatisfied(PrNumberSet prNumbers);
    }

    public enum ApplicabilitySpecificationType
    {
        Required,
        Choice
    }
}