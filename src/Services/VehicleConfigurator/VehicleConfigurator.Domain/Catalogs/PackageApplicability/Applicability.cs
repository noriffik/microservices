﻿using NexCore.Domain.Vehicles;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability
{
    public class Applicability : IEnumerable<IApplicabilitySpecification>
    {
        private readonly IEnumerable<IApplicabilitySpecification> _specifications;

        private string _asString;
        private PrNumberSet _prNumbers;

        public string AsString
        {
            get
            {
                if (_asString == null)
                {
                    _asString = _specifications.Any()
                        ? _specifications.Select(s => s.AsString).Aggregate((a, b) => $"{a} {b}")
                        : string.Empty;
                }

                return _asString;
            }
        }

        public PrNumberSet PrNumbers
        {
            get
            {
                if (_prNumbers == null)
                    _prNumbers = new PrNumberSet(_specifications.SelectMany(s => s.PrNumbers.Values));

                return _prNumbers;
            }
        }

        public Applicability(params IApplicabilitySpecification[] specifications)
        {
            _specifications = specifications.ToList().Distinct().OrderBy(s => s.AsString);
        }

        public Applicability(IEnumerable<IApplicabilitySpecification> specifications)
        {
            _specifications = specifications.ToList().Distinct().OrderBy(s => s.AsString);
        }

        public bool IsSatisfied(PrNumberSet prNumbers)
        {
            return IsSatisfied(prNumbers, out _);
        }

        public bool IsSatisfied(PrNumberSet prNumbers, out IEnumerable<IApplicabilitySpecification> violated)
        {
            violated = _specifications.Where(s => !s.IsSatisfied(prNumbers));

            return !violated.Any();
        }
        
        public IEnumerator<IApplicabilitySpecification> GetEnumerator()
        {
            return _specifications.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) _specifications).GetEnumerator();
        }

        public override string ToString()
        {
            return AsString;
        }
    }
}