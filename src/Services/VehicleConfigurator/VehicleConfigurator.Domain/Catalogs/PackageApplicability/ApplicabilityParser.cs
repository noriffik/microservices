﻿using NexCore.Domain.Vehicles;
using System;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability
{
    public class ApplicabilityParser : IApplicabilityParser
    {
        public Applicability Parse(string s)
        {
            if (s == null)
                throw new ArgumentNullException(nameof(s));
            
            if (TryParse(s, out var result))
                return result;

            throw new FormatException();
        }

        public bool TryParse(string s, out Applicability result)
        {
            if (s == null)
                throw new ArgumentNullException(nameof(s));

            if (s == string.Empty)
            {
                result = new Applicability();
                return true;
            }
            
            var patterns = s.Split(' ');

            var specifications = new List<IApplicabilitySpecification>(patterns.Length);
            foreach (var pattern in patterns)
            {
                if (TryParseSpecification(pattern, out var specification))
                {
                    specifications.Add(specification);
                }
                else
                {
                    result = null;
                    return false;
                }
            }

            result = new Applicability(specifications);
            return true;
        }

        private static bool TryParseSpecification(string s, out IApplicabilitySpecification result)
        {
            if (s.Contains('/') && !s.StartsWith('/') && !s.EndsWith('/'))
            {
                if (PrNumberSet.TryParse(s.Replace('/', ' '), out var prNumbers))
                {
                    result = new ChoiceSpecification(prNumbers);
                    return true;
                }
            }

            if (PrNumber.TryParse(s, out var prNumber))
            {
                result = new RequiredSpecification(prNumber);
                return true;
            }
            
            result = null;
            return false;
        }
    }
}
