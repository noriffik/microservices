﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability
{
    public class RequiredSpecification : ValueObject, IApplicabilitySpecification
    {
        private readonly PrNumber _prNumber;

        public string AsString => _prNumber.Value;

        public ApplicabilitySpecificationType Type => ApplicabilitySpecificationType.Required;

        public PrNumberSet PrNumbers => new PrNumberSet(_prNumber);

        public RequiredSpecification(PrNumber prNumber)
        {
            _prNumber = prNumber ?? throw new ArgumentNullException(nameof(prNumber));
        }

        public bool IsSatisfied(PrNumberSet prNumbers)
        {
            return prNumbers.Values.Contains(_prNumber);
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return AsString;
        }
    }
}