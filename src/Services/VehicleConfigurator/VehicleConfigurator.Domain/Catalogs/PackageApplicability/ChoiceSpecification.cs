﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability
{
    public class ChoiceSpecification : ValueObject, IApplicabilitySpecification
    {
        public PrNumberSet PrNumbers { get; }

        private string _asString;

        public string AsString => _asString ??
            (_asString = PrNumbers.Values.Select(n => n.ToString()).Aggregate((a, b) => a + "/" + b));

        public ApplicabilitySpecificationType Type => ApplicabilitySpecificationType.Choice;

        public ChoiceSpecification(PrNumberSet prNumbers)
        {
            PrNumbers = prNumbers ?? throw new ArgumentNullException(nameof(prNumbers));
        }

        public bool IsSatisfied(PrNumberSet prNumbers)
        {
            return prNumbers.Values.Count(p => PrNumbers.Values.Contains(p)) == 1;
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return AsString;
        }
    }
}
