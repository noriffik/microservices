﻿namespace NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability
{
    public interface IApplicabilityParser
    {
        Applicability Parse(string s);

        bool TryParse(string s, out Applicability result);
    }
}
