﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class RestrictionEnforcedUponOptionOfferEvent : IEntityEvent
    {
        public OptionOffersAffectedByRestriction Affected { get; }

        public RestrictionEnforcedUponOptionOfferEvent(OptionOffersAffectedByRestriction affected)
        {
            Affected = affected ?? throw new ArgumentNullException(nameof(affected));
        }
    }
}
