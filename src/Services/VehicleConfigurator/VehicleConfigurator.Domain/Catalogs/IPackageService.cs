﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public interface IPackageService
    {
        Task Add(Package package);

        Task ChangeStatus(PackageId packageId, PackageStatus status);

        Task UpdatePrice(PackageId packageId, decimal price);

        Task UpdateCoverage(PackageId packageId, IModelKeySet with);

        Task UpdateApplicability(PackageId packageId, Applicability applicability);
    }
}
