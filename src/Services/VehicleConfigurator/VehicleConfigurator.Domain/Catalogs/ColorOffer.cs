﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Local", Justification = "Required by EF Core")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class ColorOffer : Entity, IPotentiallyIrrelevant
    {
        public decimal Price { get; private set; }

        public ColorId ColorId { get; private set; }

        public Relevance Relevance { get; private set; }

        public RelevancePeriod RelevancePeriod => RelevancePeriodFrom.HasValue
            ? new RelevancePeriod(RelevancePeriodFrom.Value, RelevancePeriodTo)
            : RelevancePeriod.Empty;

        public DateTime? RelevancePeriodFrom { get; private set; }

        public DateTime? RelevancePeriodTo { get; private set; }

        [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
        private ColorOffer()
        {
        }

        public ColorOffer(ColorId colorId, decimal price)
        {
            ColorId = colorId;
            Price = price;
            Relevance = Relevance.Relevant;
        }

        public void ChangeRelevance(Relevance relevance)
        {
            Relevance = relevance;
        }

        public void ChangeRelevancePeriod(RelevancePeriod relevancePeriod)
        {
            if (relevancePeriod == null)
                throw new ArgumentNullException(nameof(relevancePeriod));

            RelevancePeriodFrom = relevancePeriod.From;
            RelevancePeriodTo = relevancePeriod.To;
        }

        public void ChangePrice(decimal price)
        {
            Price = price;
        }
    }
}
