﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.OptionOffers;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core.")]
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Local", Justification = "Required by EF Core.")]
    public class VehicleOffer : Entity, IAggregateRoot
    {
        private ColorOffer _defaultColor;

        public ColorOffer DefaultColor
        {
            get
            {
                if (DefaultColorId != null && (_defaultColor == null || _defaultColor.ColorId != DefaultColorId))
                {
                    _defaultColor = _colors.Single(c => c.ColorId == DefaultColorId);
                }

                return _defaultColor;
            }
        }

        public ColorId DefaultColorId { get; private set; }

        private readonly List<OptionOffer> _options;

        private readonly List<ColorOffer> _colors;

        public IEnumerable<OptionOffer> Options => _options.AsReadOnly();

        public PrNumberSet PrNumbers { get; private set; }

        public IEnumerable<ColorOffer> Colors => _colors.AsReadOnly();

        public IEnumerable<ColorOffer> RelevantColors =>
            _colors.Where(c => c.Relevance == Relevance.Relevant).ToList().AsReadOnly();

        public ModelKey ModelKey { get; private set; }

        public ModelId ModelId { get; private set; }

        public BodyId BodyId { get; private set; }

        public EquipmentId EquipmentId { get; private set; }

        public EngineId EngineId { get; private set; }

        public GearboxId GearboxId { get; private set; }

        [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core.")]
        public int CatalogId { get; private set; }

        [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core.")]
        public VehicleOfferStatus Status { get; private set; }

        public decimal Price { get; set; }

        public DateTime? RelevancePeriodFrom { get; private set; }

        public DateTime? RelevancePeriodTo { get; private set; }

        public RelevancePeriod RelevancePeriod => RelevancePeriodFrom.HasValue
            ? new RelevancePeriod(RelevancePeriodFrom.Value, RelevancePeriodTo)
            : RelevancePeriod.Empty;

        public Relevance Relevance { get; private set; }

        protected VehicleOffer()
        {
            _options = new List<OptionOffer>();
            _colors = new List<ColorOffer>();
            PrNumbers = new PrNumberSet();
            Relevance = Relevance.Relevant;
        }

        public VehicleOffer(int id, ModelKey modelKey, int catalogId) : this(modelKey, catalogId)
        {
            Id = id;
        }

        public VehicleOffer(ModelKey modelKey, int catalogId) : this()
        {
            AssignModelKey(modelKey ?? throw new ArgumentNullException(nameof(modelKey)));
            CatalogId = catalogId;
            Status = VehicleOfferStatus.Draft;
        }

        private void AssignModelKey(ModelKey modelKey)
        {
            ModelKey = modelKey;
            ModelId = modelKey.ModelId;
            BodyId = modelKey.BodyId;
            EquipmentId = modelKey.EquipmentId;
            EngineId = modelKey.EngineId;
            GearboxId = modelKey.GearboxId;
        }

        public virtual OptionOffer AddOptionOffer(PrNumber prNumber, Availability availability, Inclusion inclusion = default(Inclusion))
        {
            if (HasOptionOffer(prNumber))
                throw new DuplicateOptionOfferException(ModelKey, prNumber);

            var option = new OptionOffer(OptionId.Next(ModelId, prNumber), availability, inclusion);

            _options.Add(option);

            UpdatePrNumbers();

            return option;
        }

        private void UpdatePrNumbers()
        {
            var prNumbers = new PrNumberSet(_options.Select(o => o.PrNumber));

            PrNumbers = prNumbers;
        }

        public virtual OptionOffer FindOptionOffer(PrNumber prNumber)
        {
            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));

            return _options.SingleOrDefault(o => o.OptionId == OptionId.Next(ModelId, prNumber));
        }

        public virtual bool HasOptionOffer(PrNumber prNumber)
        {
            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));

            return _options.Any(o => o.OptionId == OptionId.Next(ModelId, prNumber));
        }

        public virtual OptionOffer UpdateOptionOfferAvailability(PrNumber prNumber, Availability availability)
        {
            if (availability == null)
                throw new ArgumentNullException(nameof(availability));

            var option = FindOptionOfferOrThrow(prNumber);

            option.Availability = availability;

            return option;
        }

        public virtual OptionOffer UpdateOptionOfferInclusion(PrNumber prNumber, Inclusion inclusion)
        {
            var option = FindOptionOfferOrThrow(prNumber);

            option.Inclusion = inclusion;

            return option;
        }

        public virtual OptionOffer AddOptionOfferRuleSet(PrNumber prNumber, string ruleSet)
        {
            if (prNumber == null) throw new ArgumentNullException(nameof(prNumber));

            var option = FindOptionOfferOrThrow(prNumber);

            option.AssignRules(ruleSet);

            AddEntityEvent(new OptionOfferRuleSetUpdatedEvent(Id, prNumber));

            return option;
        }

        public virtual OptionOffer FindOptionOfferOrThrow(PrNumber prNumber)
        {
            var option = FindOptionOffer(prNumber);
            if (option != null)
                return option;

            throw new OptionOfferNotFoundException(ModelKey, prNumber);
        }

        public virtual void DeleteOptionOffer(PrNumber prNumber)
        {
            _options.Remove(FindOptionOfferOrThrow(prNumber));

            UpdatePrNumbers();
        }

        public ColorOffer AddColorOffer(ColorId colorId, decimal price)
        {
            if (colorId == null)
                throw new ArgumentNullException(nameof(colorId));

            if (HasColorOffer(colorId))
                throw new DuplicateColorOfferException(ModelKey, colorId);

            var colorOffer = new ColorOffer(colorId, price);

            _colors.Add(colorOffer);

            return colorOffer;
        }

        public bool HasColorOffer(ColorId colorId)
        {
            if (colorId == null)
                throw new ArgumentNullException(nameof(colorId));

            return _colors.Any(c => c.ColorId == colorId);
        }

        public bool HasRelevantColorOffer(ColorId colorId)
        {
            if (colorId == null)
                throw new ArgumentNullException(nameof(colorId));

            return _colors.Where(c => c.Relevance == Relevance.Relevant)
                .Any(c => c.ColorId == colorId);
        }

        public virtual void Publish()
        {
            if (DefaultColorId == null)
                throw new VehicleOfferPublishViolationException(ModelKey);

            Status = VehicleOfferStatus.Published;
        }

        public virtual void Unpublish()
        {
            if (Status == VehicleOfferStatus.Draft)
                return;

            Status = VehicleOfferStatus.Draft;
            AddEntityEvent(new VehicleOfferUnpublishedEvent(Id));
        }

        public virtual void SetDefaultColor(ColorId colorId)
        {
            if (!HasRelevantColorOffer(colorId))
                throw new DefaultColorViolationException(ModelKey, colorId);

            DefaultColorId = colorId;
        }

        public ColorOffer FindColorOffer(ColorId colorId)
        {
            if (colorId == null)
                throw new ArgumentNullException(nameof(colorId));

            return _colors.Find(c => c.ColorId == colorId);
        }

        public virtual ColorOffer FindColorOfferOrThrow(ColorId colorId)
        {
            var color = FindColorOffer(colorId);
            if (color != null)
                return color;

            throw new ColorOfferNotFoundException(ModelKey, colorId);
        }

        public virtual void ChangeColorOfferRelevancePeriod(ColorId colorId, RelevancePeriod relevancePeriod)
        {
            if (colorId == null)
                throw new ArgumentNullException(nameof(colorId));
            if (relevancePeriod == null)
                throw new ArgumentNullException(nameof(relevancePeriod));

            var colorOffer = FindColorOfferOrThrow(colorId);

            if (colorOffer == DefaultColor)
                throw new InvalidDomainOperationException(
                    "Relevance period of Color offer cannot be changed due to it being a default color choice.");

            colorOffer.ChangeRelevancePeriod(relevancePeriod);
            AddEntityEvent(new ColorOfferRelevancePeriodChangedEvent(Id, colorId));
        }

        public virtual void ChangeColorOfferRelevance(ColorId colorId, Relevance relevance)
        {
            if (colorId == null)
                throw new ArgumentNullException(nameof(colorId));

            var colorOffer = FindColorOfferOrThrow(colorId);

            if (relevance == colorOffer.Relevance)
                return;

            if (colorOffer == DefaultColor)
                throw new InvalidDomainOperationException(
                    "Relevance of Color offer cannot be changed due to it being a default color choice.");

            colorOffer.ChangeRelevance(relevance);

            if (relevance == Relevance.Irrelevant)
                AddEntityEvent(new ColorOfferMarkedAsIrrelevantEvent(Id, colorId));
        }

        public virtual void ChangeColorOfferPrice(ColorId colorId, decimal price)
        {
            if (colorId == null)
                throw new ArgumentNullException(nameof(colorId));

            var colorOffer = FindColorOfferOrThrow(colorId);

            colorOffer.ChangePrice(price);
        }

        public virtual void ChangeOptionOfferRelevancePeriod(PrNumber prNumber, RelevancePeriod relevancePeriod)
        {
            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));
            if (relevancePeriod == null)
                throw new ArgumentNullException(nameof(relevancePeriod));

            var optionOffer = FindOptionOfferOrThrow(prNumber);

            optionOffer.ChangeRelevancePeriod(relevancePeriod);
            AddEntityEvent(new OptionOfferRelevancePeriodChangedEvent(Id, prNumber));
        }

        public virtual void ChangeOptionOfferRelevance(PrNumber prNumber, Relevance relevance)
        {
            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));

            var optionOffer = FindOptionOfferOrThrow(prNumber);

            if (relevance == optionOffer.Relevance)
                return;

            optionOffer.ChangeRelevance(relevance);

            if (relevance == Relevance.Irrelevant)
                AddEntityEvent(new OptionOfferMarkedAsIrrelevantEvent(Id, prNumber));
        }

        public virtual void EnforceOptionRestriction(PrNumberSet prNumbers)
        {
            GetOptionsByPrNumbers(prNumbers).ForEach(o => o.EnforceRestriction());

            var affected = new OptionOffersAffectedByRestriction(CatalogId, ModelKey, prNumbers);

            AddEntityEvent(new RestrictionEnforcedUponOptionOfferEvent(affected));
        }

        public virtual void LiftOptionRestriction(PrNumberSet prNumbers)
        {
            GetOptionsByPrNumbers(prNumbers).ForEach(o => o.LiftRestriction());

            var affected = new OptionOffersAffectedByRestriction(CatalogId, ModelKey, prNumbers);

            AddEntityEvent(new RestrictionLiftedFromOptionOfferEvent(affected));
        }

        public void ChangeRelevancePeriod(RelevancePeriod relevancePeriod)
        {
            if (relevancePeriod == null)
                throw new ArgumentNullException(nameof(relevancePeriod));

            RelevancePeriodFrom = relevancePeriod.From;
            RelevancePeriodTo = relevancePeriod.To;

            AddEntityEvent(new VehicleOfferRelevancePeriodChangedEvent(Id));
        }

        private List<OptionOffer> GetOptionsByPrNumbers(PrNumberSet prNumbers)
        {
            return Options.Apply(new OptionOffersWithPrNumberSpecification(prNumbers))
                .ToList();
        }

        public void ChangeRelevance(Relevance relevance)
        {
            if (Relevance == relevance)
                return;

            Relevance = relevance;

            if (Relevance == Relevance.Irrelevant)
                AddEntityEvent(new VehicleOfferMarkedAsIrrelevantEvent(Id));
        }
    }

    public enum VehicleOfferStatus
    {
        Draft = 0,
        Published = 1
    }
}
