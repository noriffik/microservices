﻿using NexCore.Common;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class OptionOfferRelevancePeriodChangedHandler : IEntityEventHandler<OptionOfferRelevancePeriodChangedEvent>
    {
        private readonly IUnitOfWork _work;

        public OptionOfferRelevancePeriodChangedHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Handle(OptionOfferRelevancePeriodChangedEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(notification.VehicleOfferId, cancellationToken);

            var optionOffer = vehicleOffer.FindOptionOfferOrThrow(notification.PrNumber);

            var nowDate = SystemTimeProvider.Instance.Get();

            var relevance = optionOffer.RelevancePeriod.IsIn(nowDate) ? Relevance.Relevant : Relevance.Irrelevant;

            vehicleOffer.ChangeOptionOfferRelevance(notification.PrNumber, relevance);
        }
    }
}
