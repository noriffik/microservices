﻿using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class VehicleOfferUnpublishedEvent : IEntityEvent
    {
        public int VehicleOfferId { get; }

        public VehicleOfferUnpublishedEvent(int vehicleOfferId)
        {
            VehicleOfferId = vehicleOfferId;
        }
    }
}
