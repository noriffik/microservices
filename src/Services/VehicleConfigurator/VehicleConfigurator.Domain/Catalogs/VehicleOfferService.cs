﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using System.Threading.Tasks;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class VehicleOfferService : IVehicleOfferService
    {
        private readonly IModelKeyValidator _modelKeyValidator;
        private readonly IUnitOfWork _work;

        public VehicleOfferService(IUnitOfWork work, IModelKeyValidator modelKeyValidator)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _modelKeyValidator = modelKeyValidator ?? throw new ArgumentNullException(nameof(modelKeyValidator));
        }

        public async Task Add(VehicleOffer vehicleOffer)
        {
            if (vehicleOffer == null)
                throw new ArgumentNullException(nameof(vehicleOffer));

            if (await _work.EntityRepository.Has(new VehicleOfferForVehicleSpecification(vehicleOffer.CatalogId, vehicleOffer.ModelKey)))
                throw new DuplicateVehicleOfferException(vehicleOffer.CatalogId, vehicleOffer.ModelKey);

            await _work.EntityRepository.HasRequired<Catalog>(vehicleOffer.CatalogId);
            await _modelKeyValidator.Validate(vehicleOffer.ModelKey);

            await _work.EntityRepository.Add(vehicleOffer);
        }

        public async Task AddColorOffer(int vehicleOfferId, ColorId colorId, decimal price)
        {
            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(vehicleOfferId);

            vehicleOffer.AddColorOffer(colorId, price);
        }

        public async Task ChangeStatus(int vehicleOfferId, VehicleOfferStatus status)
        {
            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(vehicleOfferId);

            switch (status)
            {
                case VehicleOfferStatus.Draft:
                    vehicleOffer.Unpublish();
                    break;
                case VehicleOfferStatus.Published:
                    vehicleOffer.Publish();
                    break;
            }
        }

        public async Task SetDefaultColor(int vehicleOfferId, ColorId colorId)
        {
            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(vehicleOfferId);

            vehicleOffer.SetDefaultColor(colorId);
        }

        public async Task ChangeColorOfferRelevancePeriod(int vehicleOfferId, ColorId colorId, RelevancePeriod relevancePeriod)
        {
            if (colorId == null)
                throw new ArgumentNullException(nameof(colorId));
            if (relevancePeriod == null)
                throw new ArgumentNullException(nameof(relevancePeriod));

            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(vehicleOfferId);
            var catalog = await _work.EntityRepository.Require<Catalog>(vehicleOffer.CatalogId);

            relevancePeriod.VerifyThatIsInBounds(catalog);

            vehicleOffer.ChangeColorOfferRelevancePeriod(colorId, relevancePeriod);
        }

        public async Task ChangeColorOfferPrice(int vehicleOfferId, ColorId colorId, decimal price)
        {
            if (colorId == null)
                throw new ArgumentNullException(nameof(colorId));

            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(vehicleOfferId);

            vehicleOffer.ChangeColorOfferPrice(colorId, price);
        }

        public async Task ChangeRelevancePeriod(int vehicleOfferId, RelevancePeriod period)
        {
            var vehicleOffer = await _work.EntityRepository.Require<VehicleOffer>(vehicleOfferId);

            var catalog = await _work.EntityRepository.Require(new RelevantPublishedCatalogSpecification());

            period.VerifyThatIsInBounds(catalog);
            vehicleOffer.ChangeRelevancePeriod(period);
        }
    }
}
