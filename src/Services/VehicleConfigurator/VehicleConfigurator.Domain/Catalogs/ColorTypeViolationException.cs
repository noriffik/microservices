﻿using NexCore.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class ColorTypeViolationException : Exception
    {
        private const string DefaultMessage = "Color offer require option offer for color type in vehicle.";
        private const string DefaultExtendedMessage = "Color offer {0} require option offer for color type in vehicle {1}.";

        public ColorId ColorId { get; }

        public ModelKey ModelKey { get; }

        public ColorTypeViolationException() : base(DefaultMessage)
        {
        }

        public ColorTypeViolationException(string message) : base(message)
        {
        }

        public ColorTypeViolationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public ColorTypeViolationException(string message, ModelKey modelKey, ColorId colorId, Exception innerException)
            : this(message, innerException)
        {
            ModelKey = modelKey;
            ColorId = colorId;
        }

        public ColorTypeViolationException(string message, ModelKey modelKey, ColorId colorId)
            : this(message, modelKey, colorId, null)
        {
        }

        public ColorTypeViolationException(ModelKey modelKey, ColorId colorId)
            : this(string.Format(DefaultExtendedMessage, colorId, modelKey), modelKey, colorId)
        {
        }
    }
}
