﻿using NexCore.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class ColorOfferNotFoundException : ColorOfferException
    {
        private const string DefaultMessage = "Color offer was not found.";
        private const string DefaultExtendedMessage = "Color offer for color {0} for vehicle {1} was not found.";


        public ColorOfferNotFoundException() : base(DefaultMessage)
        {
        }

        public ColorOfferNotFoundException(string message) : base(message)
        {
        }

        public ColorOfferNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public ColorOfferNotFoundException(string message, ModelKey modelKey, ColorId colorId, Exception innerException)
            : base(message, modelKey, colorId, innerException)
        {
        }

        public ColorOfferNotFoundException(string message, ModelKey modelKey, ColorId colorId)
            : base(message, modelKey, colorId)
        {
        }

        public ColorOfferNotFoundException(ModelKey modelKey, ColorId colorId)
            : this(string.Format(DefaultExtendedMessage, colorId.Value, modelKey.Value), modelKey, colorId)
        {
        }
    }
}
