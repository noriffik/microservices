﻿using NexCore.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class DuplicateOptionOfferException : OptionOfferException
    {
        private const string DefaultMessage = "Duplicate option offer.";
        private const string DefaultExtendedMessage = "Duplicate offer of option {0} for vehicle {1}.";
        
        public DuplicateOptionOfferException() : base(DefaultMessage)
        {
        }

        public DuplicateOptionOfferException(string message) : base(message)
        {
        }

        public DuplicateOptionOfferException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public DuplicateOptionOfferException(string message, ModelKey modelKey, PrNumber prNumber, Exception innerException)
            : base(message, modelKey, prNumber, innerException)
        {
        }

        public DuplicateOptionOfferException(string message, ModelKey modelKey, PrNumber prNumber)
            : base(message, modelKey, prNumber)
        {
        }

        public DuplicateOptionOfferException(ModelKey modelKey, PrNumber prNumber)
            : base(string.Format(DefaultExtendedMessage, prNumber, modelKey), modelKey, prNumber)
        {
        }
    }
}
