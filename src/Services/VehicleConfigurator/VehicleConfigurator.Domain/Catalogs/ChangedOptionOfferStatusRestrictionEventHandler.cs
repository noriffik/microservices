﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class ChangedOptionOfferStatusRestrictionEventHandler : IEntityEventHandler<RestrictionEnforcedUponOptionOfferEvent>,
        IEntityEventHandler<RestrictionLiftedFromOptionOfferEvent>
    {
        private readonly IUnitOfWork _work;

        public ChangedOptionOfferStatusRestrictionEventHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Handle(RestrictionEnforcedUponOptionOfferEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            await ToggleRestriction(notification.Affected, true, cancellationToken);
        }

        public async Task Handle(RestrictionLiftedFromOptionOfferEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            await ToggleRestriction(notification.Affected, false, cancellationToken);
        }

        private async Task ToggleRestriction(OptionOffersAffectedByRestriction affected, bool isEnforced, CancellationToken cancellationToken)
        {
            var configurations = (await _work.EntityRepository.Find(
                new ByVehicleOfferWithIncludedOptionSpecification(affected.CatalogId,
                    affected.ModelKey,
                    affected.PrNumbers), cancellationToken)).ToList();

            configurations.ForEach(c => c.ToggleRestriction(isEnforced));
        }
    }
}
