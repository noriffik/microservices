﻿using NexCore.Domain;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public enum CatalogStatus
    {
        Draft = 0,
        Published = 1
    }

    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    public class Catalog : Entity, IAggregateRoot
    {
        internal DateTime RelevancePeriodFrom;

        internal DateTime? RelevancePeriodTo;

        public Year ModelYear { get; set; }

        public Period RelevancePeriod => new Period(RelevancePeriodFrom, RelevancePeriodTo);
        
        public CatalogStatus Status { get; set; }

        private Catalog()
        {
        }

        public Catalog(Year modelYear, Period period)
        {
            if (period == null)
                throw new ArgumentNullException(nameof(period));

            ModelYear = modelYear ?? throw new ArgumentNullException(nameof(modelYear));
            RelevancePeriodFrom = period.From.GetValueOrDefault();
            RelevancePeriodTo = period.To;
            Status = CatalogStatus.Draft;
        }

        public void ChangeRelevancePeriod(Period period)
        {
            if (period == null)
                throw new ArgumentNullException(nameof(period));

            RelevancePeriodFrom = period.From.GetValueOrDefault();
            RelevancePeriodTo = period.To;
        }

        public Catalog(int id, Year modelYear, Period period) : this(modelYear, period)
        {
            Id = id;
        }

        public void ChangeRelevancePeriodEnding(DateTime to)
        {
            ChangeRelevancePeriod(RelevancePeriod.WithAnotherEnding(to));
        }
    }
}
