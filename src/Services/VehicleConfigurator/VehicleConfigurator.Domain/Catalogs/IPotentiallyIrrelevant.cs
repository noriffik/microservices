﻿namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public interface IPotentiallyIrrelevant
    {
        Relevance Relevance { get; }

        RelevancePeriod RelevancePeriod { get; }

        void ChangeRelevance(Relevance relevance);

        void ChangeRelevancePeriod(RelevancePeriod relevancePeriod);
    }
}
