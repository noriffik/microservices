﻿using NexCore.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class DuplicateVehicleOfferException : Exception
    {
        private const string DefaultMessage = "Duplicate vehicle offer.";
        private const string DefaultExtendedMessage = "Duplicate offer of vehicle {0} for catalog {1}.";

        public int CatalogId { get; }

        public ModelKey ModelKey { get; }

        public DuplicateVehicleOfferException() : base(DefaultMessage)
        {
        }

        public DuplicateVehicleOfferException(string message) : base(message)
        {
        }

        public DuplicateVehicleOfferException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public DuplicateVehicleOfferException(string message, int catalogId, ModelKey modelKey, Exception innerException)
            : this(message, innerException)
        {
            CatalogId = catalogId;
            ModelKey = modelKey;
        }

        public DuplicateVehicleOfferException(string message, int catalogId, ModelKey modelKey)
            : this(message, catalogId, modelKey, null)
        {
        }

        public DuplicateVehicleOfferException(int catalogId, ModelKey modelKey)
            : this(string.Format(DefaultExtendedMessage, modelKey, catalogId), catalogId, modelKey, null)
        {
        }
    }
}
