﻿using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public interface ICatalogService
    {
        Task Add(Catalog catalog);

        Task ChangeStatus(int id, CatalogStatus status);

        Task ChangeRelevancePeriodEnding(int id, DateTime periodTo);

        Task ChangeRelevancePeriod(int id, RelevancePeriod relevancePeriod);
    }
}
