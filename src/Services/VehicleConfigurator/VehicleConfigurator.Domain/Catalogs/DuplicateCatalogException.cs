﻿using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class DuplicateCatalogException : Exception
    {
        private const string DefaultMessage = "Catalog for given model year and relevance period already exists.";

        public DuplicateCatalogException() : base(DefaultMessage)
        {
        }

        public DuplicateCatalogException(string message) : base(message)
        {
        }

        public DuplicateCatalogException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
