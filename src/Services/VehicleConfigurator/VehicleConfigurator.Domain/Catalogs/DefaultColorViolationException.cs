﻿using NexCore.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class DefaultColorViolationException : ColorOfferException
    {
        private const string DefaultMessage = "To set color as default firstly add offer for this color to vehicle.";
        private const string DefaultExtendedMessage = "To set color {0} as default firstly add offer for this color to vehicle {1}.";

        public DefaultColorViolationException() : base(DefaultMessage)
        {
        }

        public DefaultColorViolationException(string message) : base(message)
        {
        }

        public DefaultColorViolationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public DefaultColorViolationException(string message, ModelKey modelKey, ColorId colorId, Exception innerException)
            : base(message, modelKey, colorId, innerException)
        {
        }

        public DefaultColorViolationException(string message, ModelKey modelKey, ColorId colorId)
            : base(message, modelKey, colorId)
        {
        }

        public DefaultColorViolationException(ModelKey modelKey, ColorId colorId)
            : this(string.Format(DefaultExtendedMessage, colorId, modelKey), modelKey, colorId)
        {
        }
    }
}
