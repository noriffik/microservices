﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class OptionOfferRelevancePeriodChangedEvent : IEntityEvent
    {
        public int VehicleOfferId { get; }

        public PrNumber PrNumber { get; }

        public OptionOfferRelevancePeriodChangedEvent(int vehicleOfferId, PrNumber prNumber)
        {
            VehicleOfferId = vehicleOfferId;
            PrNumber = prNumber ?? throw new ArgumentNullException(nameof(prNumber));
        }
    }
}
