﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class Package : Entity<PackageId>, IAggregateRoot<PackageId>
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public PackageStatus Status { get; private set; }

        public int CatalogId { get; private set; }

        public Applicability Applicability { get; private set; }

        public BoundModelKeySet ModelKeySet { get; private set; }

        private Package()
        {
        }

        public Package(PackageId id, int catalogId, string name, BoundModelKeySet modelKeySet,
            Applicability applicability) : base(id)
        {
            if (modelKeySet == null)
                throw new ArgumentNullException(nameof(modelKeySet));

            if (id.ModelId != modelKeySet.ModelId)
                throw new SingleModelViolationException("Model keys are in conflict with package's id.");

            CatalogId = catalogId;
            Name = name;
            Applicability = applicability ?? throw new ArgumentNullException(nameof(applicability));
            ModelKeySet = modelKeySet;
            Status = PackageStatus.Draft;
        }

        public Package(PackageId id, int catalogId, string name) : base(id)
        {
            CatalogId = catalogId;
            Name = name;
            Applicability = new Applicability();
            ModelKeySet = new BoundModelKeySet(id.ModelId);
        }

        //ModelKeySet
        public void UpdateCoverage(IModelKeySet with)
        {
            if (!with.Values.Any())
                throw new SingleModelViolationException("At least one model key is required");

            var actualModelId = ModelKeySet.Values.First().ModelId;
            if (with.Values.Any(m => m.ModelId != actualModelId))
                throw new SingleModelViolationException($"All model keys must be for {actualModelId} model");

            ModelKeySet = new BoundModelKeySet(with);

            AddEntityEvent(new PackageCoverageUpdatedEvent(Id));
        }

        public void UpdateCoverage(string with)
        {
            UpdateCoverage(BoundModelKeySet.Parse(with));
        }

        public void UpdateCoverage(IEnumerable<ModelKey> with)
        {
            UpdateCoverage(new BoundModelKeySet(with));
        }

        public bool IsApplicableTo(PrNumberSet prNumbers)
        {
            return Applicability.IsSatisfied(prNumbers);
        }

        public void UpdateApplicability(Applicability applicability)
        {
            Applicability = applicability ?? throw new ArgumentNullException(nameof(applicability));

            AddEntityEvent(new PackageApplicabilityUpdatedEvent(Id));
        }

        public void ChangeStatus(PackageStatus status)
        {
            if (Status == status)
                return;

            Status = status;

            if (status == PackageStatus.Draft)
                AddEntityEvent(new PackageMarkedAsDraftEvent(Id));
        }
    }

    public enum PackageStatus
    {
        Draft = 0,
        Published = 1
    }
}
