﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class RelevancePeriod : PeriodBase
    {
        private RelevancePeriod() : base(PeriodValidator.FromRequired.IsOptional())
        {
        }

        public RelevancePeriod(DateTime from) : this(from, null)
        {
        }

        public RelevancePeriod(DateTime from, DateTime? to)
            : base(PeriodValidator.FromRequired.IsOptional(), from, to)
        {
        }

        public RelevancePeriod(PeriodBase period)
            : base(PeriodValidator.FromRequired.IsOptional(), ThrowWhenNull(period).From, period.To)
        {
        }

        private static PeriodBase ThrowWhenNull(PeriodBase period)
        {
            if (period == null)
                throw new ArgumentNullException(nameof(period));
            
            return period;
        }

        public static RelevancePeriod Empty => new RelevancePeriod();

        public bool IsInBounds(PeriodBase period)
        {
            var isFromInBounds = !From.HasValue || !period.From.HasValue || From >= period.From;
            var isToInBounds = !To.HasValue || !period.To.HasValue || To <= period.To;

            return isFromInBounds && isToInBounds;
        }

        public void VerifyThatIsInBounds(Catalog catalog)
        {
            if (catalog == null) throw new ArgumentNullException(nameof(catalog));

            if (!IsInBounds(catalog.RelevancePeriod))
                throw new RelevancePeriodExceedsCatalogLifetimeException();
        }
    }
}
