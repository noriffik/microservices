﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    [TypeConverter(typeof(PackageIdConverter))]
    public class PackageId : EntityId
    {
        private const int Length = 5;

        protected PackageId()
        {
        }

        protected PackageId(string value) : base(value)
        {
        }

        protected PackageId(ModelId modelId, PackageCode packageCode) : base(modelId.Value + packageCode.Value)
        {
        }

        public static PackageId Next(ModelId modelId, PackageCode packageCode)
        {
            if (modelId == null)
                throw new ArgumentNullException(nameof(modelId));

            if (packageCode == null)
                throw new ArgumentNullException(nameof(packageCode));

            return Parse(modelId.Value + packageCode.Value);
        }

        public static PackageId Parse(string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (TryParse(value, out var packageId))
                return packageId;

            throw new FormatException();
        }

        public static bool TryParse(string value, out PackageId packageId)
        {
            if (value == null || value.Length != Length ||
                !ModelId.TryParse(value.Substring(0, 2), out var modelId) ||
                !PackageCode.TryParse(value.Substring(2, 3), out var packageCode))
            {
                packageId = null;
                return false;
            }

            packageId = new PackageId(modelId, packageCode);
            return true;
        }

        public PackageCode PackageCode => PackageCode.Parse(Value.Substring(2, 3));

        public ModelId ModelId => ModelId.Parse(Value.Substring(0, 2));

        public static implicit operator string(PackageId id)
        {
            return id?.Value;
        }

        public static implicit operator PackageId(string value)
        {
            return value == null ? null : Parse(value);
        }
    }

    public class PackageIdConverter : TypeConverter
    {
        public override bool CanConvertFrom(
            ITypeDescriptorContext context,
            Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context,
            CultureInfo culture, object value)
        {
            return value is string ? PackageId.Parse(value.ToString()) : base.ConvertFrom(context, culture, value);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context,
            Type destinationType)
        {
            return destinationType == typeof(PackageId) || base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context,
            CultureInfo culture, object value, Type destinationType)
        {
            return destinationType == typeof(PackageId) && value is string
                ? PackageId.Parse(value.ToString())
                : base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
