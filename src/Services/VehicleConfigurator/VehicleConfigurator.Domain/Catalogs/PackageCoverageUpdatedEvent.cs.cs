﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class PackageCoverageUpdatedEvent : IEntityEvent
    {
        public PackageId PackageId { get; }

        public PackageCoverageUpdatedEvent(PackageId packageId)
        {
            PackageId = packageId ?? throw new ArgumentNullException(nameof(packageId));
        }
    }
}
