﻿using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Domain.Catalogs
{
    public class VehicleOfferMarkedAsIrrelevantEvent : IEntityEvent
    {
        public int VehicleOfferId { get; }

        public VehicleOfferMarkedAsIrrelevantEvent(int vehicleOfferId)
        {
            VehicleOfferId = vehicleOfferId;
        }
    }
}
