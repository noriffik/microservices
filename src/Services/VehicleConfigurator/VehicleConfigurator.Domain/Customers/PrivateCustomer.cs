﻿using NexCore.Domain;
using NexCore.Legal;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Customers
{
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class PrivateCustomer : Entity, IAggregateRoot
    {
        public PersonName PersonName { get; private set; }

        public string PhysicalAddress { get; set; }

        public Passport Passport { get; private set; }

        public TaxIdentificationNumber TaxIdentificationNumber { get; private set; }

        public string Telephone { get; set; }

        private PrivateCustomer()
        {
        }

        public PrivateCustomer(int privateCustomerId, PersonName personName, string physicalAddress, Passport passport,
            TaxIdentificationNumber taxIdentificationNumber, string telephone)
            : base(privateCustomerId)
        {
            PersonName = personName ?? throw new ArgumentNullException(nameof(personName));
            PhysicalAddress = physicalAddress ?? throw new ArgumentNullException(nameof(physicalAddress));
            Passport = passport ?? throw new ArgumentNullException(nameof(passport));
            TaxIdentificationNumber = taxIdentificationNumber ?? throw new ArgumentNullException(nameof(taxIdentificationNumber));
            Telephone = telephone ?? throw new ArgumentNullException(nameof(telephone));
        }

        public void AssignName(PersonName personName)
        {
            PersonName.Assign(personName ?? PersonName.Empty);
        }

        public void AssignPassport(Passport passport)
        {
            Passport.Assign(passport ?? Passport.Empty);
        }

        public void AssignTaxIdentificationNumber(TaxIdentificationNumber taxNumber)
        {
            TaxIdentificationNumber.Assign(taxNumber ?? TaxIdentificationNumber.Empty);
        }
    }
}
