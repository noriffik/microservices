﻿using NexCore.Legal;
using NexCore.VehicleConfigurator.Domain.Customers;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.Domain.Pricing;
using System;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales
{
    public class VehicleSaleContent : Content
    {
        public TotalPrice Price { get; }

        public IEnumerable<Price> Payment { get; }

        public VehicleSpecification Vehicle { get; }

        public string VehicleModelName { get; }

        public string VehicleColorName { get; }

        public string VehicleStockAddress { get; }

        public int VehicleDeliveryDays { get; }

        public LegalPersonName SalesManagerName { get; }

        public VehicleSaleContent(string number, PrivateCustomer privateCustomer, Dealer dealer, string dealersCity, string dealersHeadPosition,
            TotalPrice price, IEnumerable<Price> payment,
            VehicleSpecification vehicle, string vehicleModelName, string vehicleColorName, string vehicleStockAddress, int vehicleDeliveryDays,
            LegalPersonName salesManagerName)
            : base(number, privateCustomer, dealer, dealersCity, dealersHeadPosition)
        {
            Price = price ?? throw new ArgumentNullException(nameof(price));
            Payment = payment ?? throw new ArgumentNullException(nameof(payment));
            Vehicle = vehicle ?? throw new ArgumentNullException(nameof(vehicle));
            SalesManagerName = salesManagerName ?? throw new ArgumentNullException(nameof(salesManagerName));
            VehicleModelName = vehicleModelName ?? throw new ArgumentNullException(nameof(vehicleModelName));
            VehicleColorName = vehicleColorName ?? throw new ArgumentNullException(nameof(vehicleColorName));
            VehicleStockAddress = vehicleStockAddress ?? throw new ArgumentNullException(nameof(vehicleStockAddress));
            VehicleDeliveryDays = vehicleDeliveryDays;
        }
    }
}