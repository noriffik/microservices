﻿namespace NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales
{
    public class VehicleSaleContract : Contract<VehicleSaleContent>
    {
        public VehicleSaleContract(ContractTypeId typeId, VehicleSaleContent content, int salesManagerId)
            : base(typeId, content, salesManagerId)
        {
        }
    }
}
