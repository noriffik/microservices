﻿using NexCore.Common;
using NexCore.VehicleConfigurator.Domain.Customers;
using NexCore.VehicleConfigurator.Domain.Dealers;
using System;

namespace NexCore.VehicleConfigurator.Domain.Contracts
{
    public abstract class Content
    {
        public string Number { get; private set; }

        public DateTime Date { get; private set; } = SystemTimeProvider.Instance.Get().Date;

        public PrivateCustomer PrivateCustomer { get; private set; } 

        public Dealer Dealer { get; private set; }

        public string DealersCity { get; private set; }

        public string DealersHeadPosition { get; private set; }

        protected Content(string number, PrivateCustomer privateCustomer, Dealer dealer, string dealerCity, string dealersHeadPosition)
        {
            Number = number ?? throw new ArgumentNullException(nameof(number));
            PrivateCustomer = privateCustomer ?? throw new ArgumentNullException(nameof(privateCustomer));
            Dealer = dealer ?? throw new ArgumentNullException(nameof(dealer));
            DealersHeadPosition = dealersHeadPosition ?? throw new ArgumentNullException(nameof(dealersHeadPosition));
            DealersCity = dealerCity ?? throw new ArgumentNullException(nameof(dealerCity));
        }
    }
}