﻿using NexCore.Domain;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Contracts
{
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    public class ContractType: Entity<ContractTypeId>, IAggregateRoot<ContractTypeId>
    {
        public string Name { get; private set; }
        
        private ContractType()
        {
        }

        public ContractType(ContractTypeId id, string name) : base(id)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }
    }
}
