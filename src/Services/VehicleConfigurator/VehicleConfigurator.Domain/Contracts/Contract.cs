﻿using NexCore.Common;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;

namespace NexCore.VehicleConfigurator.Domain.Contracts
{
    public abstract class Contract<TContent> : Entity, IAggregateRoot where TContent : Content
    {
        private string _number;
        private DateTime _date;
        private DealerId _dealerId;
        private int _privateCustomerId;

        public string Number
        {
            get => _number ?? (_number = Content.Number);
            private set => _number = value;
        }

        public ContractTypeId TypeId { get; private set; }

        public DateTime Date
        {
            get
            {
                if (_date == default)
                    _date = Content.Date;

                return _date;
            }
            private set => _date = value;
        }

        public DealerId DealerId
        {
            get => _dealerId ?? (_dealerId = Content.Dealer.Id);
            private set => _dealerId = value;
        }

        public int PrivateCustomerId
        {
            get
            {
                if (_privateCustomerId == default)
                    _privateCustomerId = Content.PrivateCustomer.Id;

                return _privateCustomerId;
            }
            private set => _privateCustomerId = value;
        }

        public int SalesManagerId { get; private set;} 

        public TContent Content { get; private set; }

        public DateTime CreatedAt { get; private set; } = SystemTimeProvider.Instance.Get().Date;

        public DateTime UpdatedAt { get; private set; } = SystemTimeProvider.Instance.Get().Date;

        protected Contract(ContractTypeId typeId, TContent content, int salesManagerId)
        {
            SalesManagerId = salesManagerId;
            TypeId = typeId ?? throw new ArgumentNullException(nameof(typeId));
            Content = content ?? throw new ArgumentNullException(nameof(content));
        }
    }
}