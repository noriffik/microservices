﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Offers;
using System;

namespace NexCore.VehicleConfigurator.Domain.Contracts
{
    public class VehicleSpecification
    {
        public Year ModelYear { get; private set; }

        public ModelKey ModelKey { get; private set; }

        public ColorId ColorId { get; private set; }

        public PackageId PackageId { get; set; }

        public PrNumberSet AdditionalOptions { get; set; }

        public PrNumberSet IncludedOptions { get; set; }

        private VehicleSpecification()
        {
            AdditionalOptions = PrNumberSet.Empty;
            IncludedOptions = PrNumberSet.Empty;
        }

        public VehicleSpecification(Year modelYear, ModelKey modelKey, ColorId colorId) : this()
        {
            ModelYear = modelYear ?? throw new ArgumentNullException(nameof(modelYear));
            ModelKey = modelKey ?? throw new ArgumentNullException(nameof(modelKey));
            ColorId = colorId ?? throw new ArgumentNullException(nameof(colorId));
        }

        public VehicleSpecification(Offer offer)
        {
            if (offer == null)
                throw new ArgumentNullException(nameof(offer));

            ModelYear = offer.ModelYear;
            ModelKey = offer.ModelKey;
            ColorId = offer.ColorId;
            PackageId = offer.PackageId;
            AdditionalOptions = offer.Options;
            IncludedOptions = offer.IncludedOptions;
        }
    }
}