﻿using NexCore.Domain;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Domain.Contracts
{
    public class ContractTemplate : Entity, IAggregateRoot
    {
        public ContractTypeId TypeId { get; private set; }

        public string DealerId { get; private set; }

        public byte[] Content { get; private set; }

        [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
        private ContractTemplate()
        {
        }

        public ContractTemplate(int id, ContractTypeId typeId, string dealerId, byte[] content)
            : base(id)
        {
            TypeId = typeId ?? throw new ArgumentNullException(nameof(typeId));
            DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
            Content = content ?? throw new ArgumentNullException(nameof(content));
        }

        public ContractTemplate(ContractTypeId contractTypeId, string dealerId, byte[] content)
            : this(default, contractTypeId, dealerId, content)
        {
        }
    }
}
