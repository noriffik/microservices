﻿using NexCore.VehicleConfigurator.Domain.Pricing;
using System;

namespace NexCore.VehicleConfigurator.Domain.Contracts
{
    public class TotalPrice
    {
        private const int TaxRate = 20;

        public Price WithoutTax { get; }

        public Price WithTax { get; }

        public Price Tax { get;}

        private TotalPrice()
        {
        }

        public TotalPrice(Price withoutTax, Price withTax, Price tax)
        {
            WithoutTax = withoutTax ?? throw new ArgumentNullException(nameof(withoutTax));
            WithTax = withTax ?? throw new ArgumentNullException(nameof(withTax));
            Tax = tax ?? throw new ArgumentNullException(nameof(tax));
        }

        public TotalPrice(decimal priceWithoutTaxBase, decimal priceWithTaxBase, decimal taxBase,
            decimal priceWithoutTaxRetail, decimal priceWithTaxRetail, decimal taxRetail)
        {
            WithoutTax = new Price(priceWithoutTaxBase, priceWithoutTaxRetail);
            WithTax = new Price(priceWithTaxBase, priceWithTaxRetail);
            Tax = new Price(taxBase, taxRetail);
        }

        public TotalPrice(Price withTax)
        {
            if (withTax == null) throw new ArgumentNullException(nameof(withTax));

            var taxBase = withTax.Base * TaxRate / 100;
            var taxRetail = withTax.Retail * TaxRate / 100;

            var priceWithoutTaxBase = withTax.Base - taxBase;
            var priceWithoutTaxRetail = withTax.Retail - taxRetail;

            WithoutTax = new Price(priceWithoutTaxBase, priceWithoutTaxRetail);
            WithTax = withTax;
            Tax = new Price(taxBase, taxRetail);
        }
    }
}