﻿using System.Diagnostics.CodeAnalysis;
using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Domain.Contracts
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core")]
    public class ContractTypeId: EntityId
    {
        private const int Length = 4;

        protected ContractTypeId()
        {
        }

        protected ContractTypeId(string value) : base(value)
        {
        }

        public static ContractTypeId Parse(string value)
        {
            return AlphaNumericEntityIdParser.Instance.Parse<ContractTypeId>(value, Length);
        }

        public static bool TryParse(string value, out ContractTypeId colorId)
        {
            return AlphaNumericEntityIdParser.Instance.TryParse(value, out colorId, Length);
        }

        public static implicit operator string(ContractTypeId id)
        {
            return id?.Value;
        }

        public static implicit operator ContractTypeId(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
