﻿using AutoMapper;
using NexCore.Domain.Vehicles;
using System.Globalization;
using Color = NexCore.VehicleConfigurator.Domain.Vehicles.Color;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Colors
{
    public class CommandAutoMapperProfile : Profile
    {
        public CommandAutoMapperProfile()
        {
            CreateMap<AddCommand, Color>();

            CreateMap<string, ColorId>()
                .ConstructUsing(c => ColorId.Parse(c));

            CreateMap<string, ColorTypeId>()
                .ConstructUsing(c => ColorTypeId.Parse(c));

            CreateMap<string, System.Drawing.Color>()
               .ConstructUsing(c => System.Drawing.Color.FromArgb(int.Parse(c, NumberStyles.HexNumber)));
        }
    }
}
