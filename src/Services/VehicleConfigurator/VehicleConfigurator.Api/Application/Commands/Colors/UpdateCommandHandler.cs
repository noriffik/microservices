﻿using AutoMapper;
using MediatR;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Colors
{
    public class UpdateCommandHandler : IRequestHandler<UpdateCommand>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public UpdateCommandHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<Unit> Handle(UpdateCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var colorTypeId = ColorTypeId.Parse(request.TypeId);
            await _work.EntityRepository.HasRequired<ColorType, ColorTypeId>(colorTypeId, cancellationToken);

            var color = await _work.EntityRepository.Require<Color, ColorId>(ColorId.Parse(request.Id), cancellationToken);

            color.Name = request.Name;
            color.TypeId = colorTypeId;
            color.Argb = _mapper.Map<System.Drawing.Color>(request.Argb);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
