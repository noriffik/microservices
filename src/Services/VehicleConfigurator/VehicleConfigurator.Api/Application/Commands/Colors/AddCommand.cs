﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Colors
{
    [DataContract]
    public class AddCommand : IRequest<string>
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Argb { get; set; }

        [DataMember]
        public string TypeId { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
