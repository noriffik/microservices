﻿using AutoMapper;
using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Colors
{
    public class AddCommandHandler : IRequestHandler<AddCommand, string>
    {
        private readonly IColorService _service;
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public AddCommandHandler(IColorService service, IUnitOfWork work, IMapper mapper)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<string> Handle(AddCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var color = _mapper.Map<Color>(request);

            try
            {
                await _service.Add(color);
            }
            catch (InvalidEntityIdException e)
            {
                throw ValidationExtensions.ExceptionForInvalidEntityId(e);
            }

            await _work.Commit(cancellationToken);

            return color.Id.Value;
        }
    }
}
