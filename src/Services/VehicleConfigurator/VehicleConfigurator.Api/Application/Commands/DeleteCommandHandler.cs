﻿using MediatR;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands
{
    public abstract class DeleteCommandHandler<TCommand, TEntity> : IRequestHandler<TCommand, Unit>
        where TCommand : IDeleteCommand<TEntity>
        where TEntity : Entity, IAggregateRoot
    {
        protected readonly IUnitOfWork Work;
        protected readonly IDeleteCommandMapper<TCommand> CommandMapper;

        protected DeleteCommandHandler(IUnitOfWork work,
            IDeleteCommandMapper<TCommand> commandMapper)
        {
            Work = work ?? throw new ArgumentNullException(nameof(work));
            CommandMapper = commandMapper ?? throw new ArgumentNullException(nameof(commandMapper));
        }

        public virtual async Task<Unit> Handle(TCommand request, CancellationToken cancellationToken)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            var entity = await Work.EntityRepository.SingleOrDefault<TEntity>(CommandMapper.Map(request), cancellationToken);
            if (entity == null)
                return Unit.Value;

            await Work.EntityRepository.Remove(entity);

            await Work.Commit(cancellationToken);

            return Unit.Value;
        }
    }

    public abstract class DeleteCommandHandler<TCommand, TEntity, TId> : IRequestHandler<TCommand, Unit>
        where TCommand : IDeleteCommand<TEntity, TId>
        where TEntity : Entity<TId>, IAggregateRoot<TId>
        where TId : EntityId
    {
        protected readonly IUnitOfWork Work;
        protected readonly IDeleteCommandMapper<TCommand, TId> CommandMapper;

        protected DeleteCommandHandler(IUnitOfWork work,
            IDeleteCommandMapper<TCommand, TId> commandMapper)
        {
            Work = work ?? throw new ArgumentNullException(nameof(work));
            CommandMapper = commandMapper ?? throw new ArgumentNullException(nameof(commandMapper));
        }

        public virtual async Task<Unit> Handle(TCommand request, CancellationToken cancellationToken)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            var entity = await Work.EntityRepository.SingleOrDefault<TEntity, TId>(CommandMapper.Map(request), cancellationToken);
            if (entity == null)
                return Unit.Value;

            await Work.EntityRepository.Remove<TEntity, TId>(entity);

            await Work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
