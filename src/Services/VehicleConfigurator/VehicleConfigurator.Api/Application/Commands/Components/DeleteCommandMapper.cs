﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Components
{
    public class DeleteCommandMapper<TComponent, TId> : ICommandMapper<DeleteCommand<TComponent, TId>, TId>
        where TComponent : Component<TId>
        where TId : EntityId
    {
        private readonly Func<string, TId> _idFactory;

        [SuppressMessage("ReSharper", "UnusedMember.Global", Justification =  "Used for testing")]
        protected DeleteCommandMapper()
        {
        }
        
        public DeleteCommandMapper(Func<string, TId> idFactory)
        {
            _idFactory = idFactory;
        }

        public virtual TId Map(DeleteCommand<TComponent, TId> command)
        {
            return command != null
                ? _idFactory(command.Id)
                : throw new ArgumentNullException(nameof(command));
        }
    }
}
