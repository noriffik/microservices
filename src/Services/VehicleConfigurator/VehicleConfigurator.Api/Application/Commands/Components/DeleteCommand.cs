﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Components
{
    [DataContract]
    public class DeleteCommand<TComponent, TId> : IRequest
        where TComponent : Component<TId>
        where TId : EntityId
    {
        [DataMember]
        public string Id { get; set; }
    }
}
