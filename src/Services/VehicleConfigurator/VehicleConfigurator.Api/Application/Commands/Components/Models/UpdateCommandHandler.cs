﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Components.Models
{
    public class UpdateCommandHandler : UpdateCommandHandler<UpdateCommand, Model, ModelId>
    {
        public UpdateCommandHandler(IUnitOfWork work)
            : base(work, new UpdateCommandMapper<UpdateCommand>())
        {
        }
    }
}
