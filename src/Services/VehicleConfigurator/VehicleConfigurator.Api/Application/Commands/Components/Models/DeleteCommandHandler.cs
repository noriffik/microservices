﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Components.Models
{
    public class DeleteCommandHandler : DeleteCommandHandler<Model, ModelId>
    {
        public DeleteCommandHandler(IUnitOfWork work,
            IComponentService<Model, ModelId> componentService)
            : base(work, componentService, new DeleteCommandMapper<Model, ModelId>(ModelId.Parse))
        {
        }

        public DeleteCommandHandler(IUnitOfWork work,
            IComponentService<Model, ModelId> componentService,
            DeleteCommandMapper<Model, ModelId> commandMapper)
            : base(work, componentService, commandMapper)
        {
        }
    }
}
