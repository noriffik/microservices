﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Components.Models
{
    public class UpdateCommandMapper<TCommand> : IUpdateCommandMapper<TCommand , Model, ModelId>
        where TCommand : UpdateCommand<Model, ModelId>
    {
        public ModelId MapId(TCommand command)
        {
            return command != null
                ? ModelId.Parse(command.Id)
                : throw new ArgumentNullException();
        }

        public void Map(TCommand command, Model entity)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            entity.Name = command.Name;
        }
    }
}
