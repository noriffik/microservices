﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Components.Models
{
    public class AddCommandHandler : AddCommandHandler<AddCommand, Model, ModelId>
    {
        public AddCommandHandler(IUnitOfWork work, IComponentService<Model, ModelId> componentService)
            : this(work, componentService, new AddCommandMapper())
        {
        }

        public AddCommandHandler(IUnitOfWork work, IComponentService<Model, ModelId> componentService,
            ICommandMapper<AddCommand, Model> commandMapper)
            : base(work, componentService, commandMapper)
        {
        }
    }
}
