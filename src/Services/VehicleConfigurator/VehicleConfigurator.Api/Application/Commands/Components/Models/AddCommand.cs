﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Components.Models
{
    [DataContract]
    public class AddCommand : AddCommand<Model, ModelId>
    {
    }
}
