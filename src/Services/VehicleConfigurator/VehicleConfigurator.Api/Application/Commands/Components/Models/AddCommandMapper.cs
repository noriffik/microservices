﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Components.Models
{
    public class AddCommandMapper : ICommandMapper<AddCommand, Model>
    {
        public Model Map(AddCommand command)
        {
            return command != null
                ? new Model(ModelId.Parse(command.Id))
                {
                    Name = command.Name
                }
                : throw new ArgumentNullException();
        }
    }
}
