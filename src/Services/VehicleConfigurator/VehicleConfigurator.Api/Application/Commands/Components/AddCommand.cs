﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Components
{
    [DataContract]
    public class AddCommand<TComponent, TId> : IRequest<string>
        where TComponent : Component<TId>
        where TId : EntityId
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
