﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Components
{
    public abstract class AddCommandHandler<TCommand, TComponent, TId> : IRequestHandler<TCommand, string>
        where TCommand : AddCommand<TComponent, TId>
        where TComponent : Component<TId>, IAggregateRoot<TId>
        where TId : EntityId
    {
        protected readonly IUnitOfWork Work;
        protected readonly IComponentService<TComponent, TId> Service;
        protected readonly ICommandMapper<TCommand, TComponent> CommandMapper;

        protected AddCommandHandler(IUnitOfWork work,
            IComponentService<TComponent, TId> componentService,
            ICommandMapper<TCommand, TComponent> commandMapper)
        {
            Work = work ?? throw new ArgumentNullException(nameof(work));
            Service = componentService ?? throw new ArgumentNullException(nameof(componentService));
            CommandMapper = commandMapper ?? throw new ArgumentNullException(nameof(commandMapper));
        }

        public virtual async Task<string> Handle(TCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var component = CommandMapper.Map(request);

            try
            {
                await Service.Add(component);
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            await Work.Commit(cancellationToken);

            return component.Id.Value;
        }

        protected virtual void HandleException(Exception e)
        {
            if (e is DuplicateEntityException ex)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(
                    ex.EntityType.Name + "Id", $"{ex.EntityType.Name} with id {ex.Id} already exists.", ex.Id);
            }

            throw e;
        }
    }
}
