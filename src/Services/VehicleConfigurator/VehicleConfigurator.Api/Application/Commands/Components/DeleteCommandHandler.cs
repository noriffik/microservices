﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Components
{
    public class DeleteCommandHandler<TComponent, TId> : IRequestHandler<DeleteCommand<TComponent, TId>, Unit>
        where TComponent : Component<TId>, IAggregateRoot<TId>
        where TId : EntityId
    {
        protected readonly IUnitOfWork Work;
        protected readonly IComponentService<TComponent, TId> Service;
        protected readonly DeleteCommandMapper<TComponent, TId> CommandMapper;

        protected DeleteCommandHandler(IUnitOfWork work,
            IComponentService<TComponent, TId> componentService,
            DeleteCommandMapper<TComponent, TId> commandMapper)
        {
            Work = work ?? throw new ArgumentNullException(nameof(work));
            Service = componentService ?? throw new ArgumentNullException(nameof(componentService));
            CommandMapper = commandMapper ?? throw new ArgumentNullException(nameof(commandMapper));
        }

        public virtual async Task<Unit> Handle(DeleteCommand<TComponent, TId> request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            try
            {
                await Service.Delete(CommandMapper.Map(request));
            }
            catch (InvalidEntityIdException)
            {
                return Unit.Value;
            }
            catch (ComponentInUseException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(nameof(request.Id), e.Message, e.Id);
            }

            await Work.Commit(cancellationToken);
            
            return Unit.Value;
        }
    }
}
