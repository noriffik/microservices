﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Validations;
using NexCore.VehicleConfigurator.Domain.Offers;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Offers
{
    public class ActualizeCommandHandler : IRequestHandler<ActualizeCommand, int>
    {
        private int _offerId;
        private readonly IUnitOfWork _work;
        private readonly IOfferService _service;

        public ActualizeCommandHandler(IUnitOfWork work, IOfferService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<int> Handle(ActualizeCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            try
            {
                _offerId = await _service.Actualize(request.OfferId);
            }
            catch (InvalidEntityIdException e)
            {
                throw ValidationExtensions.ExceptionForInvalidEntityId(e);
            }

            await _work.Commit(cancellationToken);

            return _offerId;
        }
    }
}
