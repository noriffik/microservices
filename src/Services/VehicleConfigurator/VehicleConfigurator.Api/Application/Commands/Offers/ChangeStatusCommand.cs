﻿using MediatR;
using NexCore.VehicleConfigurator.Domain.Offers;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Offers
{
    [DataContract]
    public class ChangeStatusCommand : IRequest
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public OfferStatus Status { get; set; }
    }
}
