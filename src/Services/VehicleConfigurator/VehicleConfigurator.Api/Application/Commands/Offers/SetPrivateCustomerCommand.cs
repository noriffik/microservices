﻿using System.Runtime.Serialization;
using MediatR;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Offers
{
    [DataContract]
    public class SetPrivateCustomerCommand : IRequest
    {
        [DataMember]
        public int OfferId { get; set; }

        [DataMember]
        public int PrivateCustomerId { get; set; }
    }
}
