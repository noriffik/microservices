﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Offers
{
    [DataContract]
    public class AddCommand : IRequest<int>
    {
        [DataMember]
        public int ConfigurationId { get; set; }
    }
}
