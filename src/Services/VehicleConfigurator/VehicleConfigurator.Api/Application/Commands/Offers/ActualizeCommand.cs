﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Offers
{
    [DataContract]
    public class ActualizeCommand : IRequest<int>
    {
        [DataMember]
        public int OfferId { get; set; }
    }
}
