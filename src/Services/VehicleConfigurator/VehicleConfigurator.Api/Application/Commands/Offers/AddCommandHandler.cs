﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Validations;
using NexCore.VehicleConfigurator.Domain.Offers;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Offers
{
    public class AddCommandHandler : IRequestHandler<AddCommand, int>
    {
        private int _offerId;
        private readonly IUnitOfWork _work;
        private readonly IOfferService _service;

        public AddCommandHandler(IUnitOfWork work, IOfferService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<int> Handle(AddCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            try
            {
                _offerId = await _service.Add(request.ConfigurationId);
            }
            catch (InvalidEntityIdException e)
            {
                throw ValidationExtensions.ExceptionForInvalidEntityId(e);
            }

            await _work.Commit(cancellationToken);

            return _offerId;
        }
    }
}
