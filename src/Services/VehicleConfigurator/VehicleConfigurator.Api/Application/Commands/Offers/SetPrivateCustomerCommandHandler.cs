﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Offers;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Offers
{
    public class SetPrivateCustomerCommandHandler : IRequestHandler<SetPrivateCustomerCommand>
    {
        private readonly IUnitOfWork _work;
        private readonly IOfferService _service;

        public SetPrivateCustomerCommandHandler(IUnitOfWork work, IOfferService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<Unit> Handle(SetPrivateCustomerCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            await _service.SetPrivateCustomer(request.OfferId, request.PrivateCustomerId);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
