﻿using MediatR;
using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Api.Application.Commands
{
    public interface IDeleteCommand<TEntity> : IRequest
        where TEntity : Entity
    {
        int Id { get; set; }
    }

    public interface IDeleteCommand<TEntity, TId> : IRequest
        where TEntity : Entity<TId>
        where TId : EntityId
    {
        string Id { get; set; }
    }
}
