﻿using System.Runtime.Serialization;
using MediatR;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories
{
    [DataContract]
    public class AssignCommand : IRequest
    {
        [DataMember]
        public string OptionId { get; set; }

        [DataMember]
        public int CategoryId { get; set; }
    }
}
