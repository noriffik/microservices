﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories
{
    public class UnassignCommandHandler : UpdateCommandHandler<UnassignCommand, Option, OptionId>
    {
        public UnassignCommandHandler(IUnitOfWork work) : base(work, new UnassignCommandMapper())
        {
        }
    }
}
