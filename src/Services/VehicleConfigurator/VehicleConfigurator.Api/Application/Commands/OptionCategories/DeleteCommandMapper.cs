﻿using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories
{
    public class DeleteCommandMapper : IDeleteCommandMapper<DeleteCommand>
    {
        public int Map(DeleteCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            return command.Id;
        }
    }
}
