﻿using System.Runtime.Serialization;
using MediatR;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories
{
    [DataContract]
    public class AddCommand : IRequest<int>
    {
        [DataMember]
        public string Name { get; set; }
    }
}
