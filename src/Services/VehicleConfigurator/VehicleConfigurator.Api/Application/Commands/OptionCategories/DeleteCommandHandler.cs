﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories
{
    public class DeleteCommandHandler : DeleteCommandHandler<DeleteCommand, OptionCategory>
    {
        public DeleteCommandHandler(IUnitOfWork work) : base(work, new DeleteCommandMapper())
        {
        }
    }
}
