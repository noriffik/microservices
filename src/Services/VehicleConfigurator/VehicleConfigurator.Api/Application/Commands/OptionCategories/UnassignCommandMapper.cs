﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories
{
    public class UnassignCommandMapper : IUpdateCommandMapper<UnassignCommand, Option, OptionId>
    {
        public OptionId MapId(UnassignCommand command)
        {
            if(command == null)
                throw new ArgumentNullException(nameof(command));

            return OptionId.Parse(command.Id);
        }

        public void Map(UnassignCommand command, Option option)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            if (option == null)
                throw new ArgumentNullException(nameof(option));

            option.OptionCategoryId = null;
        }
    }
}
