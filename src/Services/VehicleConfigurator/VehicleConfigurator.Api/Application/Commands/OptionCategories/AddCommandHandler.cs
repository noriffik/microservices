﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories
{
    public class AddCommandHandler : IRequestHandler<AddCommand, int>
    {
        private readonly IUnitOfWork _work;
        private ICommandMapper<AddCommand, OptionCategory> _commandMapper;

        public ICommandMapper<AddCommand, OptionCategory> CommandMapper
        {
            get => _commandMapper ?? (_commandMapper = new AddCommandMapper());
            set => _commandMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public AddCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<int> Handle(AddCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var optionCategory = CommandMapper.Map(request);

            await _work.EntityRepository.Add(optionCategory);

            await _work.Commit(cancellationToken);

            return optionCategory.Id;
        }
    }
}
