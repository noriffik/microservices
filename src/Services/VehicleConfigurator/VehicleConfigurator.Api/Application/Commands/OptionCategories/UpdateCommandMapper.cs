﻿using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories
{
    public class UpdateCommandMapper : IUpdateCommandMapper<UpdateCommand, OptionCategory>
    {
        public int MapId(UpdateCommand command)
        {
            if(command == null)
                throw new ArgumentNullException(nameof(command));

            return command.Id;
        }

        public void Map(UpdateCommand command, OptionCategory category)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            if (category == null)
                throw new ArgumentNullException(nameof(category));

            category.Name = command.Name;
        }
    }
}
