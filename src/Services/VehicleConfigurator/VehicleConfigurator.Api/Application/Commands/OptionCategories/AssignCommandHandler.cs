﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories
{
    public class AssignCommandHandler : IRequestHandler<AssignCommand, Unit>
    {
        private readonly IUnitOfWork _work;
        private readonly IOptionService _optionService;

        public AssignCommandHandler(IUnitOfWork work, IOptionService optionService)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _optionService = optionService ?? throw new ArgumentNullException(nameof(optionService));
        }

        public async Task<Unit> Handle(AssignCommand request, CancellationToken cancellationToken)
        {
            try
            {
                await _optionService.AssignToCategory(OptionId.Parse(request.OptionId), request.CategoryId);
            }
            catch (InvalidEntityIdException e)
            {
                throw ValidationExtensions.ExceptionForInvalidEntityId(e);
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
