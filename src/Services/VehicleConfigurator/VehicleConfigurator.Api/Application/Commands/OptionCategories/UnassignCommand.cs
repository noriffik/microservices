﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories
{
    [DataContract]
    public class UnassignCommand : IUpdateCommand<Option, OptionId>
    {
        [DataMember(Name = "OptionId")]
        public string Id { get; set; }
    }
}
