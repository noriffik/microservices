﻿using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories
{
    public class AddCommandMapper : ICommandMapper<AddCommand, OptionCategory>
    {
        public OptionCategory Map(AddCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            return new OptionCategory(command.Name);
        }
    }
}
