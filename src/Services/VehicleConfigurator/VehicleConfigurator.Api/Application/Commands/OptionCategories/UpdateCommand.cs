﻿using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories
{
    [DataContract]
    public class UpdateCommand : IUpdateCommand<OptionCategory>
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
