﻿using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories
{
    [DataContract]
    public class DeleteCommand : IDeleteCommand<OptionCategory>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
