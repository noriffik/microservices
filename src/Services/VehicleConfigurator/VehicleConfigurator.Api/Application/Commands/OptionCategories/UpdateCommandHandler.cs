﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories
{
    public class UpdateCommandHandler : UpdateCommandHandler<UpdateCommand, OptionCategory>
    {
        public UpdateCommandHandler(IUnitOfWork work) : base(work, new UpdateCommandMapper())
        {
        }
    }
}
