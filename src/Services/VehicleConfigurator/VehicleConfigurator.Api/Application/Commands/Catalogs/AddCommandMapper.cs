﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs
{
    public class AddCommandMapper : ICatalogCommandMapper
    {   
        private IPeriodModelMapper _periodMapper;

        public IPeriodModelMapper PeriodMapper
        {
            get => _periodMapper ?? (_periodMapper = new PeriodModelMapper());
            set => _periodMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public Catalog Map(AddCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            return new Catalog(new Year(command.Year), PeriodMapper.MapReverse(command.RelevancePeriod));
        }
    }
}
