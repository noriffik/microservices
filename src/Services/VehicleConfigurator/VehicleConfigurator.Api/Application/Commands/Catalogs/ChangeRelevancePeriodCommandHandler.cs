﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs
{
    public class ChangeRelevancePeriodCommandHandler : IRequestHandler<ChangeRelevancePeriodCommand, Unit>
    {
        private readonly IUnitOfWork _work;
        private readonly ICatalogService _service;

        public ChangeRelevancePeriodCommandHandler(IUnitOfWork work, ICatalogService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public Task<Unit> Handle(ChangeRelevancePeriodCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            return ChangeRelevancePeriod(request.CatalogId,
                new RelevancePeriod(request.RelevancePeriod.From, request.RelevancePeriod.To),
                cancellationToken);
        }

        private async Task<Unit> ChangeRelevancePeriod(int catalogId, RelevancePeriod period,
            CancellationToken cancellationToken)
        {
            try
            {
                await _service.ChangeRelevancePeriod(catalogId, period);
            }
            catch (InvalidPeriodException)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(nameof(period), "Invalid period",
                    period);
            }
            catch (DuplicateCatalogException)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(nameof(period), "Period is overlapping",
                    period);
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
