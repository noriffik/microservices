﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs
{
    public class AddCommandHandler : IRequestHandler<AddCommand, int>
    {   
        private ICommandMapper<AddCommand, Catalog> _mapper;
        private readonly IUnitOfWork _work;
        private readonly ICatalogService _service;

        public ICommandMapper<AddCommand, Catalog> Mapper
        {
            get => _mapper ?? (_mapper = new AddCommandMapper());
            set => _mapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public AddCommandHandler(IUnitOfWork work, ICatalogService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<int> Handle(AddCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var catalog = Mapper.Map(request);

            try
            {
                await _service.Add(catalog);
            }
            catch (DuplicateCatalogException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(
                    nameof(request.RelevancePeriod), e.Message);
            }

            await _work.Commit(cancellationToken);
            
            return catalog.Id;
        }
    }
}
