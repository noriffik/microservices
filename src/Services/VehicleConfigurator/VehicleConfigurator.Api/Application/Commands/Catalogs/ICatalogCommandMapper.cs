﻿using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs
{
    public interface ICatalogCommandMapper : ICommandMapper<AddCommand, Catalog>
    {
        IPeriodModelMapper PeriodMapper { get; set; }
    }
}