﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs
{
    public class ChangeStatusCommandHandler : IRequestHandler<ChangeStatusCommand>
    {
        private readonly IUnitOfWork _work;
        private readonly ICatalogService _service;

        public ChangeStatusCommandHandler(IUnitOfWork work, ICatalogService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<Unit> Handle(ChangeStatusCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            await _service.ChangeStatus(request.Id, request.Status);

            await _work.Commit(CancellationToken.None);

            return Unit.Value;
        }
    }
}
