﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Models;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs
{
    [DataContract]
    public class ChangeRelevancePeriodCommand : IRequest
    {
        [FromRoute]
        [DataMember]
        public int CatalogId { get; set; }

        [FromBody]
        [DataMember]
        public PeriodModel RelevancePeriod { get; set; }
    }
}
