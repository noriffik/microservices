﻿using MediatR;
using NexCore.VehicleConfigurator.Api.Application.Models;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs
{
    [DataContract]
    public class AddCommand : IRequest<int>
    {
       [DataMember]
       public int Year { get; set; }

       [DataMember]
       public PeriodModel RelevancePeriod { get; set; }
    }
}
