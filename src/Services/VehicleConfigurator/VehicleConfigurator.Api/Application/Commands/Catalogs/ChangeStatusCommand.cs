﻿using MediatR;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs
{
    [DataContract]
    public class ChangeStatusCommand : IRequest
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public CatalogStatus Status { get; set; }
    }
}
