﻿using MediatR;

namespace NexCore.VehicleConfigurator.Api.Application.Commands
{
    public interface ICommandMapper<in TFrom, out TTo>
        where TFrom : IBaseRequest
        where TTo : class
    {
        TTo Map(TFrom command);
    }
}