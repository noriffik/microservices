﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Gearboxes
{
    public class DeleteCommandHandler : DeleteCommandHandler<Gearbox, GearboxId>
    {
        public DeleteCommandHandler(
            IUnitOfWork work, 
            IModelComponentService<Gearbox, GearboxId> componentService) 
            : base(work, componentService,  new DeleteCommandMapper<Gearbox, GearboxId>(GearboxId.Parse))
        {
        }

        public DeleteCommandHandler(
            IUnitOfWork work, 
            IModelComponentService<Gearbox, GearboxId> componentService, 
            DeleteCommandMapper<Gearbox, GearboxId> commandMapper) 
            : base(work, componentService, commandMapper)
        {
        }
    }
}
