﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Gearboxes
{
    public class UpdateCommandMapper<TCommand> : IUpdateCommandMapper<TCommand , Gearbox, GearboxId>
        where TCommand : UpdateCommand
    {
        public GearboxId MapId(TCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            return GearboxId.Parse(command.Id);
        }

        public void Map(TCommand command, Gearbox entity)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            entity.Name = command.Name;
            entity.Type = GearboxType.Parse(command.Type);
            entity.GearBoxCategory = command.GearBoxCategory;
        }
    }
}
