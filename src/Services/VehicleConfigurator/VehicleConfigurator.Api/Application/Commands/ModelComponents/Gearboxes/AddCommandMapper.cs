﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Gearboxes
{
    public class AddCommandMapper : ICommandMapper<AddCommand, Gearbox>
    {
        public Gearbox Map(AddCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            return new Gearbox(GearboxId.Parse(command.Id), GearboxType.Parse(command.Type))
            {
                Name = command.Name,
                GearBoxCategory = command.GearBoxCategory
            };
        }
    }
}
