﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Gearboxes
{
    public class UpdateCommandHandler : UpdateCommandHandler<UpdateCommand, Gearbox, GearboxId>
    {
        public UpdateCommandHandler(IUnitOfWork work)
            : base(work, new UpdateCommandMapper<UpdateCommand>())
        {
        }
    }
}
