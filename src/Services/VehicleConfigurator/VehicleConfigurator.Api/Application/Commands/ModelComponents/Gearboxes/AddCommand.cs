﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Gearboxes
{
    [DataContract]
    public class AddCommand : AddCommand<Gearbox, GearboxId>
    {
        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public GearBoxCategory? GearBoxCategory {get; set;}
    }
}
