﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Gearboxes
{
    public class AddCommandHandler  : AddCommandHandler<AddCommand, Gearbox, GearboxId>
    {
        public AddCommandHandler(
            IUnitOfWork work, 
            IModelComponentService<Gearbox, GearboxId> componentService) 
            : base(work, componentService, new AddCommandMapper())
        {
        }

        public AddCommandHandler(
            IUnitOfWork work, 
            IModelComponentService<Gearbox, GearboxId> componentService, 
            ICommandMapper<AddCommand, Gearbox> commandMapper) 
            : base(work, componentService, commandMapper)
        {
        }
    }
}
