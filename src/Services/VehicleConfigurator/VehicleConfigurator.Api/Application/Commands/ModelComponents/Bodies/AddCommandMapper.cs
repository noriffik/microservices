﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Bodies
{
    public class AddCommandMapper : ICommandMapper<AddCommand, Body>
    {
        public Body Map(AddCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            return new Body(BodyId.Parse(command.Id))
            {
                Name = command.Name
            };
        }
    }
}