﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Bodies
{
    public class UpdateCommandMapper<TCommand> : IUpdateCommandMapper<TCommand , Body, BodyId>
        where TCommand : UpdateCommand<Body, BodyId>
    {
        public BodyId MapId(TCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            return BodyId.Parse(command.Id);
        }

        public void Map(TCommand command, Body entity)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            entity.Name = command.Name;
        }
    }
}
