﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Bodies
{
    [DataContract]
    public class AddCommand : AddCommand<Body, BodyId>
    {
    }
}
