﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Bodies
{
    public class AddCommandHandler : AddCommandHandler<AddCommand, Body, BodyId>
    {
        public AddCommandHandler(IUnitOfWork work, IModelComponentService<Body, BodyId> componentService)
            : this(work, componentService, new AddCommandMapper())
        {
        }

        public AddCommandHandler(IUnitOfWork work, IModelComponentService<Body, BodyId> componentService, ICommandMapper<AddCommand, Body> commandMapper)
            : base(work, componentService, commandMapper)
        {
        }
    }
}
