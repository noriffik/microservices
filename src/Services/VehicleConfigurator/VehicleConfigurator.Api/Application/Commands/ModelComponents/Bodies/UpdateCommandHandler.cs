﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Bodies
{
    public class UpdateCommandHandler : UpdateCommandHandler<UpdateCommand, Body, BodyId>
    {
        public UpdateCommandHandler(IUnitOfWork work)
            : base(work, new UpdateCommandMapper<UpdateCommand>())
        {
        }
    }
}
