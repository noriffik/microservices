﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Bodies
{
    public class DeleteCommandHandler : DeleteCommandHandler<Body, BodyId>
    {
        public DeleteCommandHandler(
            IUnitOfWork work,
            IModelComponentService<Body, BodyId> componentService)
            : base(work, componentService, new DeleteCommandMapper<Body, BodyId>(BodyId.Parse))
        {
        }

        public DeleteCommandHandler(
            IUnitOfWork work, 
            IModelComponentService<Body, BodyId> componentService, 
            DeleteCommandMapper<Body, BodyId> commandMapper) 
            : base(work, componentService, commandMapper)
        {
        }
    }
}
