﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Bodies
{
    public class UpdateCommand : UpdateCommand<Body, BodyId>
    {
    }
}
