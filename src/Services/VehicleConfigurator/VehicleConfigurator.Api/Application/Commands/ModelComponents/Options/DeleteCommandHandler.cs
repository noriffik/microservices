﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Options
{
    public class DeleteCommandHandler : DeleteCommandHandler<Option, OptionId>
    {
        public DeleteCommandHandler(IUnitOfWork work,
            IModelComponentService<Option, OptionId> componentService)
            : base(work, componentService, new DeleteCommandMapper<Option, OptionId>(OptionId.Parse))
        {
        }

        public DeleteCommandHandler(IUnitOfWork work,
            IModelComponentService<Option, OptionId> componentService,
            DeleteCommandMapper<Option, OptionId> commandMapper)
            : base(work, componentService, commandMapper)
        {
        }
    }
}
