﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Options
{
    public class UpdateCommandHandler : UpdateCommandHandler<UpdateCommand, Option, OptionId>
    {
        public UpdateCommandHandler(IUnitOfWork work)
            : base(work, new UpdateCommandMapper<UpdateCommand>())
        {
        }
    }
}
