﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Options
{
    public class UpdateCommandMapper<TCommand> : IUpdateCommandMapper<TCommand, Option, OptionId>
        where TCommand : UpdateCommand<Option, OptionId>
    {
        public OptionId MapId(TCommand command)
        {
            return command != null
                ? OptionId.Parse(command.Id)
                : throw new ArgumentNullException();
        }

        public void Map(TCommand command, Option entity)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            entity.Name = command.Name;
        }
    }
}
