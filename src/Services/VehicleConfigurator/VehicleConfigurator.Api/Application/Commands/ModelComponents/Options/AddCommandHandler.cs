﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Options
{
    public class AddCommandHandler : AddCommandHandler<AddCommand, Option, OptionId>
    {
        public AddCommandHandler(IUnitOfWork work, IModelComponentService<Option, OptionId> componentService)
            : this(work, componentService, new AddCommandMapper())
        {
        }

        public AddCommandHandler(IUnitOfWork work,
            IModelComponentService<Option, OptionId> componentService, ICommandMapper<AddCommand, Option> commandMapper)
            : base(work, componentService, commandMapper)
        {
        }
    }
}
