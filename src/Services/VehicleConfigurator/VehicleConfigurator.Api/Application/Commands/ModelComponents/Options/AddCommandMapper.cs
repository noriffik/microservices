﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Options
{
    public class AddCommandMapper : ICommandMapper<AddCommand, Option>
    {
        public Option Map(AddCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            return new Option(OptionId.Parse(command.Id))
            {
                Name = command.Name
            };
        }
    }
}
