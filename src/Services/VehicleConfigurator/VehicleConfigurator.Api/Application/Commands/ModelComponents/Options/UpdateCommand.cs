﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Options
{
    [DataContract]
    public class UpdateCommand : UpdateCommand<Option, OptionId>
    {
    }
}
