﻿using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents
{
    public abstract class AddCommandHandler<TCommand, TComponent, TId> : Components.AddCommandHandler<TCommand, TComponent, TId>
        where TCommand : AddCommand<TComponent, TId>
        where TComponent : ModelComponent<TId>, IAggregateRoot<TId>
        where TId : EntityId
    {
        protected AddCommandHandler(IUnitOfWork work,
            IComponentService<TComponent, TId> componentService,
            ICommandMapper<TCommand, TComponent> commandMapper)
            : base(work, componentService, commandMapper)
        {
        }

        protected override void HandleException(Exception e)
        {
            if (e is InvalidEntityIdException ex)
            {
                throw ValidationExtensions.ExceptionForInvalidEntityId(ex);
            }

            base.HandleException(e);
        }
    }
}
