﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents
{
    public class DeleteCommandMapper<TComponent, TId> : Components.DeleteCommandMapper<TComponent, TId>
        where TComponent : ModelComponent<TId>
        where TId : EntityId
    {
        [SuppressMessage("ReSharper", "UnusedMember.Global", Justification =  "Used for testing")]
        protected DeleteCommandMapper()
        {
        }

        public DeleteCommandMapper(Func<string, TId> idFactory) : base(idFactory)
        {
        }
    }
}
