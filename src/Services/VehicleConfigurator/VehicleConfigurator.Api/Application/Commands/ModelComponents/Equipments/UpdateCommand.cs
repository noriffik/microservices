﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Equipments
{
    public class UpdateCommand : UpdateCommand<Equipment, EquipmentId>
    {
    }
}
