﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Equipments
{
    public class AddCommandMapper : ICommandMapper<AddCommand, Equipment>
    {
        public Equipment Map(AddCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            return new Equipment(EquipmentId.Parse(command.Id))
            {
                Name = command.Name
            };
        }
    }
}
