﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Equipments
{
    [DataContract]
    public class AddCommand : AddCommand<Equipment, EquipmentId>
    {
    }
}
