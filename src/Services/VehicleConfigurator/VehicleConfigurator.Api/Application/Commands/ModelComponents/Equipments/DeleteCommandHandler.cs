﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Equipments
{
    public class DeleteCommandHandler : DeleteCommandHandler<Equipment, EquipmentId>
    {
        public DeleteCommandHandler(
            IUnitOfWork work, 
            IModelComponentService<Equipment, EquipmentId> componentService)
            : base(work, componentService, new DeleteCommandMapper<Equipment, EquipmentId>(EquipmentId.Parse))
        {
        }

        public DeleteCommandHandler(
            IUnitOfWork work, 
            IModelComponentService<Equipment, EquipmentId> componentService, 
            DeleteCommandMapper<Equipment, EquipmentId> commandMapper)
            : base(work, componentService, commandMapper)
        {
        }
    }
}
