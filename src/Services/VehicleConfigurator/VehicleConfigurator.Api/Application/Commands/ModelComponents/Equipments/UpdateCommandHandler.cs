﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Equipments
{
    public class UpdateCommandHandler : UpdateCommandHandler<UpdateCommand, Equipment, EquipmentId>
    {
        public UpdateCommandHandler(IUnitOfWork work)
            : base(work, new UpdateCommandMapper<UpdateCommand>())
        {
        }
    }
}
