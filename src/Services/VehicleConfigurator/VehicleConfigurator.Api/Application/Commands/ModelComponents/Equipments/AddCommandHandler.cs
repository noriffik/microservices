﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Equipments
{
    public class AddCommandHandler : AddCommandHandler<AddCommand, Equipment, EquipmentId>
    {
        public AddCommandHandler(IUnitOfWork work, IModelComponentService<Equipment, EquipmentId> componentService) 
            : base(work, componentService, new AddCommandMapper())
        {
        }

        public AddCommandHandler(IUnitOfWork work, IModelComponentService<Equipment, EquipmentId> componentService, ICommandMapper<AddCommand, Equipment> commandMapper) 
            : base(work, componentService, commandMapper)
        {
        }
    }
}
