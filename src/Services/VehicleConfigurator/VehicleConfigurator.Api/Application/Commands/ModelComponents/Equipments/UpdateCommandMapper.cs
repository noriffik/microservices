﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Equipments
{
    public class UpdateCommandMapper<TCommand> : IUpdateCommandMapper<TCommand , Equipment, EquipmentId>
        where TCommand : UpdateCommand<Equipment, EquipmentId>
    {
        public EquipmentId MapId(TCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            return EquipmentId.Parse(command.Id);
        }

        public void Map(TCommand command, Equipment entity)
        {
            if (command == null) throw new ArgumentNullException(nameof(command));

            if (entity == null) throw new ArgumentNullException(nameof(entity));

            entity.Name = command.Name;
        }
    }
}
