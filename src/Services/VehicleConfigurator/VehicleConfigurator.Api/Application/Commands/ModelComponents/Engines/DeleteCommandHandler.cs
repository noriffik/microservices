﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Engines
{
    public class DeleteCommandHandler : DeleteCommandHandler<Engine, EngineId>
    {
        public DeleteCommandHandler(
            IUnitOfWork work, 
            IModelComponentService<Engine, EngineId> componentService) 
            : base(work, componentService, new DeleteCommandMapper<Engine, EngineId>(EngineId.Parse))
        {
        }

        public DeleteCommandHandler(
            IUnitOfWork work, 
            IModelComponentService<Engine, EngineId> componentService, 
            DeleteCommandMapper<Engine, EngineId> commandMapper) 
            : base(work, componentService, commandMapper)
        {
        }
    }
}
