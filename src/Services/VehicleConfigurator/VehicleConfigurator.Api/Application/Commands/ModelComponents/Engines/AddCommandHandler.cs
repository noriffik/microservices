﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Engines
{
    public class AddCommandHandler : AddCommandHandler<AddCommand, Engine, EngineId>
    {
        public AddCommandHandler(IUnitOfWork work, IModelComponentService<Engine, EngineId> componentService)
            : this(work, componentService, new AddCommandMapper())
        {
        }

        public AddCommandHandler(IUnitOfWork work, IModelComponentService<Engine, EngineId> componentService, ICommandMapper<AddCommand, Engine> commandMapper)
            : base(work, componentService, commandMapper)
        {
        }
    }
}
