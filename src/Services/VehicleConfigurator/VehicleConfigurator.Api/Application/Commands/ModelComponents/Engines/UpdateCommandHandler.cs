﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Engines
{
    public class UpdateCommandHandler : UpdateCommandHandler<UpdateCommand, Engine, EngineId>
    {
        public UpdateCommandHandler(IUnitOfWork work) 
            : base(work, new UpdateCommandMapper<UpdateCommand>())
        {
        }
    }
}
