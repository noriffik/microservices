﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Engines
{
    public class AddCommandMapper : ICommandMapper<AddCommand, Engine>
    {
        public Engine Map(AddCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            return new Engine(EngineId.Parse(command.Id))
            {
                Name = command.Name,
                FuelType = command.FuelType
            };
        }
    }
}