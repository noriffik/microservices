﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Engines
{
    public class UpdateCommand : UpdateCommand<Engine, EngineId>
    {
        [DataMember]
        public FuelType? FuelType { get; set; }
    }
}
