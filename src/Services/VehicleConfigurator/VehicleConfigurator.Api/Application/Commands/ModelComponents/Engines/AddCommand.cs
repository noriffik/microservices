﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Engines
{
    [DataContract]
    public class AddCommand : AddCommand<Engine, EngineId>
    {
        [DataMember]
        public FuelType? FuelType { get; set; }
    }
}
