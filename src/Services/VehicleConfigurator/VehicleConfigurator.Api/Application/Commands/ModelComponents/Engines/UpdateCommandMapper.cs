﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Engines
{
    public class UpdateCommandMapper<TCommand> : IUpdateCommandMapper<TCommand , Engine, EngineId>
        where TCommand : UpdateCommand
    {
        public EngineId MapId(TCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            return EngineId.Parse(command.Id);
        }

        public void Map(TCommand command, Engine entity)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            entity.Name = command.Name;
            entity.FuelType = command.FuelType;
        }
    }
}
