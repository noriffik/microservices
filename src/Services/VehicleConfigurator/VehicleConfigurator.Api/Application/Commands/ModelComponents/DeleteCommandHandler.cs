﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents
{
    public abstract class DeleteCommandHandler<TComponent, TId> : Components.DeleteCommandHandler<TComponent, TId>,
        IRequestHandler<DeleteCommand<TComponent, TId>, Unit>
        where TComponent : ModelComponent<TId>, IAggregateRoot<TId>
        where TId : EntityId
    {
        protected DeleteCommandHandler(IUnitOfWork work,
            IComponentService<TComponent, TId> componentService,
            Components.DeleteCommandMapper<TComponent, TId> commandMapper)
            : base(work, componentService, commandMapper)
        {
        }

        public Task<Unit> Handle(DeleteCommand<TComponent, TId> request, CancellationToken cancellationToken)
        {
            return base.Handle(request, cancellationToken);
        }
    }
}
