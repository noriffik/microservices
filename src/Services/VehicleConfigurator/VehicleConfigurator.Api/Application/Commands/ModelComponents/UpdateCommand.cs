﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents
{
    [DataContract]
    public class UpdateCommand<TComponent, TId> : Components.UpdateCommand<TComponent, TId>
        where TComponent : ModelComponent<TId>
        where TId : EntityId
    {
    }
}
