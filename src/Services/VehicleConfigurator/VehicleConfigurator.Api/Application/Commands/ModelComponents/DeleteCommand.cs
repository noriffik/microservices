﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents
{
    [DataContract]
    public class DeleteCommand<TComponent, TId> : Components.DeleteCommand<TComponent, TId>
        where TComponent : ModelComponent<TId>
        where TId : EntityId
    {
    }
}
