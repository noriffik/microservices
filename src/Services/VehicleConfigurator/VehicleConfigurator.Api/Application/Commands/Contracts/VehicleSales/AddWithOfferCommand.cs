﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MediatR;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Contracts.VehicleSales
{
    [DataContract]
    public class AddWithOfferCommand : IRequest<int>
    {
        [DataMember]
        public string Number { get; set; }

        [DataMember]
        public string ContractTypeId { get; set; }

        [DataMember]
        public IEnumerable<PriceModel> Payment { get; set; }

        [DataMember]
        public int OfferId { get; set; }

        [DataMember]
        public string VehicleStockAddress { get; set; }

        [DataMember]
        public int VehicleDeliveryDays { get; set; }

        [DataMember]
        public int PrivateCustomerId { get; set; }

        [DataMember]
        public int SalesManagerId { get; set; }

        [DataMember]
        public string DealerId { get; set; }

        [DataMember]
        public string DealersCity { get; set; }

        [DataMember]
        public string DealersHeadPosition { get; set; }
    }
}
