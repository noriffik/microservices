﻿using AutoMapper;
using MediatR;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Domain.Contracts;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Domain.Customers;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.Domain.Pricing;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Contracts.VehicleSales
{
    public class AddCommandHandler : IRequestHandler<AddCommand, int>, IRequestHandler<AddWithOfferCommand, int>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public AddCommandHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<int> Handle(AddCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            await _work.EntityRepository.HasRequired<ContractType, ContractTypeId>(ContractTypeId.Parse(request.ContractTypeId),
                cancellationToken);

            var content = await CreateContent(request, cancellationToken);

            var contract = new VehicleSaleContract(ContractTypeId.Parse(request.ContractTypeId), content, request.SalesManagerId);

            await _work.EntityRepository.Add(contract);

            await _work.Commit(cancellationToken);

            return contract.Id;
        }

        public async Task<int> Handle(AddWithOfferCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            await _work.EntityRepository.HasRequired<ContractType, ContractTypeId>(ContractTypeId.Parse(request.ContractTypeId),
                cancellationToken);

            var content = await CreateContentWithOffer(request, cancellationToken);

            var contract = new VehicleSaleContract(ContractTypeId.Parse(request.ContractTypeId), content, request.SalesManagerId);

            await _work.EntityRepository.Add(contract);

            await _work.Commit(cancellationToken);

            return contract.Id;
        }

        private async Task<VehicleSaleContent> CreateContent(AddCommand request,  CancellationToken cancellationToken)
        {
            var dealer = await _work.EntityRepository.Require<Dealer, DealerId>(DealerId.Parse(request.DealerId), cancellationToken);

            var privateCustomer = await _work.EntityRepository.Require<PrivateCustomer>(request.PrivateCustomerId, cancellationToken);

            var model = await _work.EntityRepository.Require<Model, ModelId>(ModelKey.Parse(request.Vehicle.ModelKey).ModelId, cancellationToken);

            var color = await _work.EntityRepository.Require<Color, ColorId>(ColorId.Parse(request.Vehicle.ColorId), cancellationToken);

            var totalPrice = _mapper.Map<TotalPrice>(request.TotalPrice);

            var vehicle = _mapper.Map<VehicleSpecification>(request.Vehicle);

            var payment = _mapper.Map<IEnumerable<Price>>(request.Payment);

            var salesManagerName = new LegalPersonName("John", "Doe", "Michelson"); //TODO: replace with claims

            return new VehicleSaleContent(request.Number, privateCustomer, dealer, request.DealersCity, request.DealersHeadPosition, 
                totalPrice, payment, vehicle, model.Name, color.Name, request.VehicleStockAddress, request.VehicleDeliveryDays, salesManagerName);
        }

        private async Task<VehicleSaleContent> CreateContentWithOffer(AddWithOfferCommand request, CancellationToken cancellationToken)
        {
            var dealer = await _work.EntityRepository.Require<Dealer, DealerId>(DealerId.Parse(request.DealerId), cancellationToken);

            var privateCustomer = await _work.EntityRepository.Require<PrivateCustomer>(request.PrivateCustomerId, cancellationToken);

            var offer = await _work.EntityRepository.Require<Offer>(request.OfferId, cancellationToken);

            var model = await _work.EntityRepository.Require<Model, ModelId>(offer.ModelId, cancellationToken);

            var color = await _work.EntityRepository.Require<Color, ColorId>(offer.ColorId, cancellationToken);

            var totalPrice = new TotalPrice(_mapper.Map<Price>(offer.Price.Total));

            var vehicle = new VehicleSpecification(offer);

            var payment = _mapper.Map<IEnumerable<Price>>(request.Payment);

            var salesManagerName = new LegalPersonName("John", "Doe", "Michelson"); //TODO: replace with claims

            return new VehicleSaleContent(request.Number, privateCustomer, dealer, request.DealersCity, request.DealersHeadPosition,
                totalPrice, payment, vehicle, model.Name, color.Name, request.VehicleStockAddress, request.VehicleDeliveryDays, salesManagerName);
        }
    }
}
