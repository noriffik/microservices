﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers
{
    [DataContract]
    public class AddCommand : IRequest
    {
        [DataMember]
        [FromRoute]
        public int VehicleOfferId { get; set; }

        [DataMember]
        [FromRoute]
        public string ColorId { get; set; }

        [DataMember]
        [FromBody]
        public decimal Price { get; set; }
    }
}
