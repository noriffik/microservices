﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Models;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers
{
    [DataContract]
    public class ChangeColorOfferRelevancePeriodCommand : IRequest
    {
        [FromRoute]
        [DataMember]
        public int VehicleOfferId { get; set; }

        [FromRoute]
        [DataMember]
        public string ColorId { get; set; }

        [FromBody]
        [DataMember]
        public PeriodModel RelevancePeriod { get; set; }
    }
}
