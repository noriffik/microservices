﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers
{
    public class SetDefaultColorCommandHandler : IRequestHandler<SetDefaultColorCommand>
    {
        private readonly IVehicleOfferService _service;
        private readonly IUnitOfWork _work;

        public SetDefaultColorCommandHandler(IVehicleOfferService service, IUnitOfWork work)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(SetDefaultColorCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            try
            {
                await _service.SetDefaultColor(request.VehicleOfferId, ColorId.Parse(request.ColorId));
            }
            catch (DefaultColorViolationException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(nameof(request.ColorId), e.Message, e.ColorId);
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
