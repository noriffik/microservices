﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers
{
    public class ChangePriceCommandHandler : IRequestHandler<ChangePriceCommand>
    {
        private readonly IUnitOfWork _work;
        private readonly IVehicleOfferService _service;

        public ChangePriceCommandHandler(IUnitOfWork work, IVehicleOfferService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<Unit> Handle(ChangePriceCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            try
            {
                await _service.ChangeColorOfferPrice(request.VehicleOfferId, ColorId.Parse(request.ColorId), request.Price);
            }
            catch (ColorOfferNotFoundException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(nameof(request.ColorId), e.Message, e.ColorId);
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
