﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers
{
    public class AddCommandHandler : IRequestHandler<AddCommand>
    {
        private readonly IVehicleOfferService _service;
        private readonly IUnitOfWork _work;

        public AddCommandHandler(IVehicleOfferService service, IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<Unit> Handle(AddCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            try
            {
                await _service.AddColorOffer(request.VehicleOfferId, ColorId.Parse(request.ColorId), request.Price);
            }
            catch (InvalidEntityIdException e)
            {
                throw ValidationExtensions.ExceptionForInvalidEntityId(e);
            }
            catch (DuplicateColorOfferException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(nameof(request.ColorId), e.Message, e.ColorId);
            }
            catch (ColorTypeViolationException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(string.Empty, e.Message);
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
