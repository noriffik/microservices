﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers
{
    [DataContract]
    public class SetDefaultColorCommand : IRequest
    {
        [DataMember]
        public int VehicleOfferId { get; set; }

        [DataMember]
        public string ColorId { get; set; }
    }
}
