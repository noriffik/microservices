﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers
{
    public class ChangeRelevancePeriodCommandHandler : IRequestHandler<ChangeColorOfferRelevancePeriodCommand>,
        IRequestHandler<ResetColorOfferRelevancePeriodCommand>
    {
        private readonly IUnitOfWork _work;
        private readonly IVehicleOfferService _service;

        public ChangeRelevancePeriodCommandHandler(IUnitOfWork work, IVehicleOfferService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public Task<Unit> Handle(ChangeColorOfferRelevancePeriodCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return ChangeRelevancePeriod(request.VehicleOfferId,
                ColorId.Parse(request.ColorId),
                new RelevancePeriod(request.RelevancePeriod.From, request.RelevancePeriod.To),
                cancellationToken);
        }

        public Task<Unit> Handle(ResetColorOfferRelevancePeriodCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return ChangeRelevancePeriod(request.VehicleOfferId,
                ColorId.Parse(request.ColorId),
                RelevancePeriod.Empty,
                cancellationToken);
        }

        private async Task<Unit> ChangeRelevancePeriod(int vehicleOfferId, ColorId colorId, RelevancePeriod period,
            CancellationToken cancellationToken)
        {
            try
            {
                await _service.ChangeColorOfferRelevancePeriod(vehicleOfferId, colorId, period);
            }
            catch (ColorOfferNotFoundException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler("ColorId", e.Message, e.ColorId);
            }
            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
