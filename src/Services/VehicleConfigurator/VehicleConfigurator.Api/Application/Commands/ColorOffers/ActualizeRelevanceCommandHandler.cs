﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers
{
    public class ActualizeRelevanceCommandHandler : ActualizeRelevanceBaseCommandHandler<ColorOffer>
    {
        public ActualizeRelevanceCommandHandler(IUnitOfWork work) : base(work)
        {
        }

        protected override void ActualizePotentiallyIrrelevant(VehicleOffer vehicleOffer, DateTime date)
        {
            foreach (var colorOffer in vehicleOffer.Colors)
            {
                var relevance = colorOffer.RelevancePeriod.IsIn(date)
                    ? Relevance.Relevant
                    : Relevance.Irrelevant;

                vehicleOffer.ChangeColorOfferRelevance(colorOffer.ColorId, relevance);
            }
        }
    }
}
