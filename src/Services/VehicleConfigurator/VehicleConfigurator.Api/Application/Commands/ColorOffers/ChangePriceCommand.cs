﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers
{
    [DataContract]
    public class ChangePriceCommand : IRequest
    {
        [FromRoute]
        [DataMember]
        public int VehicleOfferId { get; set; }

        [FromRoute]
        [DataMember]
        public string ColorId { get; set; }

        [FromBody]
        [DataMember]
        public decimal Price { get; set; }
    }
}
