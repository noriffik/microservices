﻿using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Api.Application.Commands
{
    public interface IUpdateCommandMapper<TCommand, TEntity>
        where TEntity : Entity
    {
        int MapId(TCommand command);

        void Map(TCommand command, TEntity entity);
    }

    public interface IUpdateCommandMapper<TCommand, TEntity, TId>
        where TEntity : Entity<TId>
        where TId : EntityId
    {
        TId MapId(TCommand command);

        void Map(TCommand command, TEntity entity);
    }
}