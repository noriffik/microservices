﻿using MediatR;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Packages
{
    [DataContract]
    public class ChangeStatusCommand : IRequest
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public PackageStatus Status { get; set; }
    }
}
