﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Packages
{
    public class AddCommandHandler : IRequestHandler<AddCommand, string>
    {
        private readonly IPackageService _service;
        private readonly IUnitOfWork _work;
        private readonly ICommandMapper<AddCommand, Package> _commandMapper;

        public AddCommandHandler(IPackageService service, IUnitOfWork work, ICommandMapper<AddCommand, Package> commandMapper)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _commandMapper = commandMapper ?? throw new ArgumentNullException(nameof(commandMapper));
        }

        public async Task<string> Handle(AddCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var package = _commandMapper.Map(request);

            try
            {
                await _service.Add(package);
            }
            catch (InvalidEntityIdException e)
            {
                throw ValidationExtensions.ExceptionForInvalidEntityId(e);
            }

            await _work.Commit(cancellationToken);

            return package.Id.Value;
        }
    }
}
