﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Packages
{
    public class ChangeStatusCommandHandler : IRequestHandler<ChangeStatusCommand>
    {
        private readonly IPackageService _service;
        private readonly IUnitOfWork _work;

        public ChangeStatusCommandHandler(IPackageService service, IUnitOfWork work)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(ChangeStatusCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            try
            {
                await _service.ChangeStatus(PackageId.Parse(request.Id), request.Status);
            }
            catch (PackageRequirementViolationException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler("Status", e.Message);
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
