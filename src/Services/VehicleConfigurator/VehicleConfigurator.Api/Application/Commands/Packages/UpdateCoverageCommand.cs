﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Packages
{
    [DataContract]
    public class UpdateCoverageCommand : IRequest
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string ModelKeys { get; set; }
    }
}
