﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Packages
{
    [DataContract]
    public class UpdatePriceCommand : IRequest
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public decimal Price { get; set; }
    }
}
