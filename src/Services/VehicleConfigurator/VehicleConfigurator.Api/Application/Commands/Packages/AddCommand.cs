﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Packages
{
    [DataContract]
    public class AddCommand : IRequest<string>
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int CatalogId { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public string ModelKeys { get; set; }

        [DataMember]
        public string Applicability { get; set; }
    }
}
