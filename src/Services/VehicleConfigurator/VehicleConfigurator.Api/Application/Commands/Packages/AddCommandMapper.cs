﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Packages
{
    public class AddCommandMapper : ICommandMapper<AddCommand, Package>
    {
        private readonly IApplicabilityParser _parser;

        public AddCommandMapper(IApplicabilityParser parser)
        {
            _parser = parser ?? throw new ArgumentNullException(nameof(parser));
        }

        public Package Map(AddCommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            return new Package(
                PackageId.Parse(command.Id),
                command.CatalogId,
                command.Name,
                BoundModelKeySet.Parse(command.ModelKeys),
                _parser.Parse(command.Applicability))
            {
                Price = command.Price
            };
        }
    }
}
