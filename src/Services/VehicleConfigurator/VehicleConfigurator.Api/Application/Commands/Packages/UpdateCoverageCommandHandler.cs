﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Packages
{
    public class UpdateCoverageCommandHandler : IRequestHandler<UpdateCoverageCommand>
    {
        private readonly IPackageService _service;
        private readonly IUnitOfWork _work;

        public UpdateCoverageCommandHandler(IPackageService service, IUnitOfWork work)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(UpdateCoverageCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var packageId = PackageId.Parse(request.Id);
            var modelKeys = BoundModelKeySet.Parse(request.ModelKeys);

            try
            {
                await _service.UpdateCoverage(packageId, modelKeys);
            }
            catch (InvalidEntityIdException e)
            {
                throw ValidationExtensions.ExceptionForInvalidEntityId(e);
            }
            catch (SingleModelViolationException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(nameof(request.ModelKeys), e.Message, request.ModelKeys);
            }
            catch (ProhibitedByStatusException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(nameof(request.Id), e.Message, request.Id);
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
