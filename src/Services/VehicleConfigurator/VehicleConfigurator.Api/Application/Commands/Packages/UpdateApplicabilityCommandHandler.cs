﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Packages
{
    public class UpdateApplicabilityCommandHandler : IRequestHandler<UpdateApplicabilityCommand>
    {
        private readonly IPackageService _service;
        private readonly IUnitOfWork _work;
        private readonly IApplicabilityParser _parser;

        public UpdateApplicabilityCommandHandler(IPackageService service, IUnitOfWork work, IApplicabilityParser parser)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _parser = parser ?? throw new ArgumentNullException(nameof(parser));
        }

        public async Task<Unit> Handle(UpdateApplicabilityCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var applicability = _parser.Parse(request.Applicability);
            var packageId = PackageId.Parse(request.Id);

            await _service.UpdateApplicability(packageId, applicability);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
