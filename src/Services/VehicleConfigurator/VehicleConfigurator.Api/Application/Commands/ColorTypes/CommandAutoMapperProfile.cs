﻿using AutoMapper;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ColorTypes
{
    public class CommandAutoMapperProfile : Profile
    {
        public CommandAutoMapperProfile()
        {
            CreateMap<AddCommand, ColorType>();

            CreateMap<string, ColorTypeId>()
                .ConstructUsing(c => ColorTypeId.Parse(c));
        }
    }
}
