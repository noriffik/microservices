﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ColorTypes
{
    [DataContract]
    public class RenameCommand : IRequest
    {
        [FromRoute]
        [DataMember]
        public string Id { get; set; }

        [FromBody]
        [DataMember]
        public string Name { get; set; }
    }
}
