﻿using AutoMapper;
using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ColorTypes
{
    public class AddCommandHandler : IRequestHandler<AddCommand, string>
    {
        private readonly IColorTypeService _service;
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public AddCommandHandler(IColorTypeService service, IUnitOfWork work, IMapper mapper)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<string> Handle(AddCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var colorType = _mapper.Map<ColorType>(request);

            try
            {
                await _service.Add(colorType);
            }
            catch (DuplicateEntityException e)
            {
                throw ValidationExtensions.ExceptionForInvalidEntityId(e);
            }

            await _work.Commit(cancellationToken);

            return colorType.Id.Value;
        }
    }
}
