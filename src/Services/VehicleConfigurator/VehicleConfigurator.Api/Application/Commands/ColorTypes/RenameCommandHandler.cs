﻿using MediatR;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.ColorTypes
{
    public class RenameCommandHandler : IRequestHandler<RenameCommand>
    {
        private readonly IUnitOfWork _work;

        public RenameCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(RenameCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var colorType = await _work.EntityRepository.Require<ColorType, ColorTypeId>(ColorTypeId.Parse(request.Id), cancellationToken);

            colorType.Name = request.Name;

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
