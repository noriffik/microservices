﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions
{
    [DataContract]
    public class ChangeRelevancePeriodCommand : IRequest
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public RelevancePeriodModel RelevancePeriod { get; set; }
    }
}
