﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions
{
    [DataContract]
    public class AddCommand : IRequest<int>
    {
        [DataMember]
        public int CatalogId { get; set; }

        [DataMember]
        public string ModelKey { get; set; }

        [DataMember]
        public string PrNumbers { get; set; }

        [DataMember]
        public RelevancePeriodModel RelevancePeriod { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}
