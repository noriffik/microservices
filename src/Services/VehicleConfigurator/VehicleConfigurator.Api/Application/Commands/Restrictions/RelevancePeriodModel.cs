﻿using NexCore.Domain;
using System;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions
{
    public class RelevancePeriodModel
    {
        public static RelevancePeriodModel Create(Period period)
        {
            if (period == null)
                throw new ArgumentNullException(nameof(period));

            return new RelevancePeriodModel
            {
                From = period.From.GetValueOrDefault(),
                To = period.To
            };
        }

        [DataMember]
        public DateTime From { get; set; }

        [DataMember]
        public DateTime? To { get; set; }
    }
}
