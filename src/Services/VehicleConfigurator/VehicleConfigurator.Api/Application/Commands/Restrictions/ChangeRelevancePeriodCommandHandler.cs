﻿using AutoMapper;
using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions
{
    public class ChangeRelevancePeriodCommandHandler : IRequestHandler<ChangeRelevancePeriodCommand, Unit>
    {
        private readonly IUnitOfWork _work;
        private readonly IRestrictionService _service;
        private readonly IMapper _mapper;

        public ChangeRelevancePeriodCommandHandler(IUnitOfWork work, IRestrictionService service, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<Unit> Handle(ChangeRelevancePeriodCommand request, CancellationToken cancellationToken)
        {
            var period = _mapper.Map<Period>(request.RelevancePeriod);

            await _service.ChangeRelevancePeriod(request.Id, period);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
