﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Unit = MediatR.Unit;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions
{
    public class ChangeStateCommandHandler : IRequestHandler<EnableCommand, Unit>, IRequestHandler<DisableCommand, Unit>
    {
        private readonly IUnitOfWork _work;
        private readonly IRestrictionService _service;

        public ChangeStateCommandHandler(IUnitOfWork work, IRestrictionService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<Unit> Handle(EnableCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            await _service.Enable(request.Id);

            await Commit(cancellationToken);

            return Unit.Value;
        }

        public async Task<Unit> Handle(DisableCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            await _service.Disable(request.Id);

            await Commit(cancellationToken);

            return Unit.Value;
        }

        private async Task Commit(CancellationToken cancellationToken)
        {
            await _work.Commit(cancellationToken);
        }
    }
}
