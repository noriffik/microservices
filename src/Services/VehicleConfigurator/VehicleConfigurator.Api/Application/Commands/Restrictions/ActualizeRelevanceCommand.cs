﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions
{
    [DataContract]
    public class ActualizeRelevanceCommand : IRequest
    {
        [DataMember]
        public int? CatalogId { get; set; }
    }
}
