﻿using AutoMapper;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Restrictions;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions
{
    public class CommandAutoMapperProfile : Profile
    {
        public CommandAutoMapperProfile()
        {
            CreateMap<AddCommand, Restriction>()
                .ConvertUsing((c, restriction, context) => new Restriction(
                    c.CatalogId, c.ModelKey, c.PrNumbers, context.Mapper.Map<Period>(c.RelevancePeriod))
                {
                    Description = c.Description
                });
        }
    }
}
