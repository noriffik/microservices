﻿using MediatR;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.Domain.Restrictions.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions
{
    public class ActualizeRelevanceCommandHandler : IRequestHandler<ActualizeRelevanceCommand>
    {
        private readonly IUnitOfWork _work;

        public ActualizeRelevanceCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(ActualizeRelevanceCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var catalogs = (await GetCatalogs(request, cancellationToken)).ToList();

            var restrictions = catalogs.Select(async c =>
                    await _work.EntityRepository.Find(new RestrictionByCatalogIdSpecification(c.Id), cancellationToken))
                .SelectMany(t => t.Result).ToList();

            var vehicleOffers = catalogs.Select(async c =>
                    await GetVehicleOffers(c, restrictions, cancellationToken))
                .SelectMany(t => t.Result).ToList();

            ToggleOptions(restrictions, vehicleOffers);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }

        private async Task<IEnumerable<Catalog>> GetCatalogs(ActualizeRelevanceCommand request, CancellationToken cancellationToken)
        {
            if (request.CatalogId.HasValue)
                return new[] { await _work.EntityRepository.Require<Catalog>(request.CatalogId.Value, cancellationToken) };

            return await _work.EntityRepository.Find(new RelevantPublishedCatalogSpecification(), cancellationToken);
        }

        private static void ToggleOptions(IEnumerable<Restriction> restrictions, ICollection<VehicleOffer> vehicleOffers)
        {
            foreach (var restriction in restrictions)
            {
                var applicableOffers = vehicleOffers
                    .Where(v => v.ModelKey == restriction.ModelKey)
                    .Where(v => v.CatalogId == restriction.CatalogId)
                    .ToList();

                if (restriction.IsRelevant())
                    applicableOffers.ForEach(v => v.EnforceOptionRestriction(restriction.PrNumbers));

                else
                    applicableOffers.ForEach(v => v.LiftOptionRestriction(restriction.PrNumbers));
            }
        }

        private async Task<IEnumerable<VehicleOffer>> GetVehicleOffers(Catalog catalog, IEnumerable<Restriction> restrictions, CancellationToken cancellationToken)
        {
            var modelKeySet = new ModelKeySet(restrictions.Select(r => r.ModelKey));

            return await _work.EntityRepository.Find(new VehicleOffersByModelKeySetAndCatalogIdSpecification(catalog.Id, modelKeySet), cancellationToken);
        }
    }
}
