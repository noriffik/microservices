﻿using AutoMapper;
using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions
{
    public class AddCommandHandler : IRequestHandler<AddCommand, int>
    {
        private readonly IUnitOfWork _work;
        private readonly IRestrictionService _restrictionService;
        private readonly IMapper _mapper;

        public AddCommandHandler(IUnitOfWork work, IRestrictionService restrictionService, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _restrictionService = restrictionService ?? throw new ArgumentNullException(nameof(restrictionService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<int> Handle(AddCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var restriction = await _restrictionService.Add(_mapper.Map<Restriction>(request));

            await _work.Commit(cancellationToken);

            return restriction.Id;
        }
    }
}
