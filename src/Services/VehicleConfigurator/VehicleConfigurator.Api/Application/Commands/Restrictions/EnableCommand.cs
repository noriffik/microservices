﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions
{
    [DataContract]
    public class EnableCommand : IRequest
    {
        [DataMember]
        public int Id { get; set; }
    }
}
