﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using NexCore.Common;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers
{
    public class ActualizeRelevanceCommandHandler : ActualizeRelevanceBaseCommandHandler<VehicleOffer>
    {
        public ActualizeRelevanceCommandHandler(IUnitOfWork work) : base(work)
        {
        }

        protected override void ActualizePotentiallyIrrelevant(VehicleOffer vehicleOffer, DateTime date)
        {
            var relevance = vehicleOffer.RelevancePeriod.IsIn(SystemTimeProvider.Instance.Get())
                ? Relevance.Relevant
                : Relevance.Irrelevant;

            vehicleOffer.ChangeRelevance(relevance);
        }
    }
}
