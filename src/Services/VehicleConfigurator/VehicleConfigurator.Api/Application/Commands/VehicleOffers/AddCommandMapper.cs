﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers
{
    public class AddCommandMapper : ICommandMapper<AddCommand, VehicleOffer>
    {
        public VehicleOffer Map(AddCommand command)
        {
            if(command == null)
                throw new ArgumentNullException(nameof(command));

            return new VehicleOffer(ModelKey.Parse(command.ModelKey), command.CatalogId)
            {
                Price = command.Price
            };
        }
    }
}
