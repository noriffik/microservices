﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers
{
    public class ChangeVehicleOfferRelevancePeriodCommandHandler : IRequestHandler<ChangeVehicleOfferRelevancePeriodCommand>, IRequestHandler<ResetVehicleOfferRelevancePeriodCommand>
    {
        private readonly IUnitOfWork _work;
        private readonly IVehicleOfferService _service;

        public ChangeVehicleOfferRelevancePeriodCommandHandler(IUnitOfWork work, IVehicleOfferService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<Unit> Handle(ChangeVehicleOfferRelevancePeriodCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            await _service.ChangeRelevancePeriod(request.Id,
                new RelevancePeriod(request.RelevancePeriod.From, request.RelevancePeriod.To));

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }

        public async Task<Unit> Handle(ResetVehicleOfferRelevancePeriodCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            await _service.ChangeRelevancePeriod(request.Id, RelevancePeriod.Empty);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
