﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers
{
    [DataContract]
    public class ActualizeRelevanceCommand<TEntity> : IRequest
    {
        [DataMember]
        public int? CatalogId { get; set; }
    }
}
