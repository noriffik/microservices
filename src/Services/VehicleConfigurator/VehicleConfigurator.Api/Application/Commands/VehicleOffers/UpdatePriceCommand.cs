﻿using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers
{
    [DataContract]
    public class UpdatePriceCommand : IUpdateCommand<VehicleOffer>
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public decimal Price { get; set; }
    }
}
