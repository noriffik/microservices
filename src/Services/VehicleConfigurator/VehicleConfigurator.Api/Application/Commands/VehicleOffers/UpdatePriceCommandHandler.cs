﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers
{
    public class UpdatePriceCommandHandler : UpdateCommandHandler<UpdatePriceCommand,VehicleOffer>
    {
        public UpdatePriceCommandHandler(IUnitOfWork work) 
            : base(work, new UpdatePriceCommandMapper())
        {
        }
    }
}
