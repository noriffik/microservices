﻿using MediatR;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers
{
    [DataContract]
    public class ChangeStatusCommand : IRequest
    {
        [DataMember]
        public int VehicleOfferId { get; set; }

        [DataMember]
        public VehicleOfferStatus Status { get; set; }
    }
}
