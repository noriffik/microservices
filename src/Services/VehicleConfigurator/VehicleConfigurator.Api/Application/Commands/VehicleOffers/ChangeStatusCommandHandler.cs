﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Validations;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers
{
    public class ChangeStatusCommandHandler : IRequestHandler<ChangeStatusCommand>
    {
        private readonly IVehicleOfferService _service;
        private readonly IUnitOfWork _work;

        public ChangeStatusCommandHandler(IVehicleOfferService service, IUnitOfWork work)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(ChangeStatusCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            try
            {
                await _service.ChangeStatus(request.VehicleOfferId, request.Status);
            }
            catch (VehicleOfferPublishViolationException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(string.Empty, e.Message);
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
