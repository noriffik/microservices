﻿using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers
{
    public class UpdatePriceCommandMapper : IUpdateCommandMapper<UpdatePriceCommand, VehicleOffer>
    {
        public int MapId(UpdatePriceCommand command)
        {
            if(command == null)
                throw new ArgumentNullException(nameof(command));

            return command.Id;
        }

        public void Map(UpdatePriceCommand command, VehicleOffer vehicleOffer)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            if (vehicleOffer == null)
                throw new ArgumentNullException(nameof(vehicleOffer));

            vehicleOffer.Price = command.Price;
        }
    }
}
