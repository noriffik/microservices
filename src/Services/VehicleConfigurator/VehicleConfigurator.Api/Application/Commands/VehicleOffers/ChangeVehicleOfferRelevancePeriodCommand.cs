﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Models;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers
{
    [DataContract]
    public class ChangeVehicleOfferRelevancePeriodCommand : IRequest
    {
        [FromRoute]
        [DataMember]
        public int Id { get; set; }

        [FromBody]
        [DataMember]
        public PeriodModel RelevancePeriod { get; set; }
    }
}
