﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers
{
    public class AddCommandHandler : IRequestHandler<AddCommand, int>
    {
        private readonly IVehicleOfferService _service;
        private readonly IUnitOfWork _work;
        private ICommandMapper<AddCommand, VehicleOffer> _commandMapper;

        public ICommandMapper<AddCommand, VehicleOffer> CommandMapper
        {
            get => _commandMapper ?? (_commandMapper = new AddCommandMapper());
            set => _commandMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public AddCommandHandler(IVehicleOfferService service, IUnitOfWork work)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<int> Handle(AddCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var vehicle = CommandMapper.Map(request);

            try
            {
                await _service.Add(vehicle);
            }
            catch (InvalidEntityIdException e)
            {
                throw ValidationExtensions.ExceptionForInvalidEntityId(e);
            }
            catch (DuplicateVehicleOfferException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(nameof(request.ModelKey), e.Message, e.ModelKey);
            }

            await _work.Commit(cancellationToken);
            
            return vehicle.Id;
        }
    }
}
