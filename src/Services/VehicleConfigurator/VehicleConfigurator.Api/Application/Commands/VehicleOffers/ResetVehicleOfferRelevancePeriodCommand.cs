﻿using Microsoft.AspNetCore.Mvc;
using System.Runtime.Serialization;
using MediatR;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers
{
    [DataContract]
    public class ResetVehicleOfferRelevancePeriodCommand : IRequest
    {
        [FromRoute]
        [DataMember]
        public int Id { get; set; }
    }
}
