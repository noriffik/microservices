﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using NexCore.Common;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers
{
    public abstract class ActualizeRelevanceBaseCommandHandler<TEntity> : IRequestHandler<ActualizeRelevanceCommand<TEntity>>
    {
        private readonly IUnitOfWork _work;

        protected ActualizeRelevanceBaseCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(ActualizeRelevanceCommand<TEntity> request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (request.CatalogId.HasValue)
                await _work.EntityRepository.HasRequired<Catalog>(request.CatalogId.Value, cancellationToken);

            var vehicleOffers = request.CatalogId.HasValue
                ? await _work.EntityRepository.Find(new VehicleOfferCatalogIdSpecification(request.CatalogId.Value), cancellationToken)
                : await GetActualVehicleOffers(cancellationToken);

            var date = SystemTimeProvider.Instance.Get();

            foreach (var vehicleOffer in vehicleOffers)
                ActualizePotentiallyIrrelevant(vehicleOffer, date);

            await _work.Commit(cancellationToken);
            return Unit.Value;
        }

        private async Task<IEnumerable<VehicleOffer>> GetActualVehicleOffers(CancellationToken cancellationToken)
        {
            var catalogs = await _work.EntityRepository.Find(new RelevantPublishedCatalogSpecification(), cancellationToken);

            var vehicleOffers = new List<VehicleOffer>();
            foreach (var catalog in catalogs)
            {
                var vehicles = await _work.EntityRepository.Find(new VehicleOfferCatalogIdSpecification(catalog.Id), cancellationToken);
                vehicleOffers.AddRange(vehicles);
            }

            return vehicleOffers;
        }

        protected abstract void ActualizePotentiallyIrrelevant(VehicleOffer vehicleOffer, DateTime date);


    }
}
