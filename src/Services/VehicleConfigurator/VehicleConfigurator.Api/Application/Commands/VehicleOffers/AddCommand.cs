﻿using System.Runtime.Serialization;
using MediatR;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers
{
    [DataContract]
    public class AddCommand : IRequest<int>
    {
        [DataMember]
        public int CatalogId { get; set; }

        [DataMember]
        public string ModelKey { get; set; }

        [DataMember]
        public decimal Price { get; set; }
    }
}
