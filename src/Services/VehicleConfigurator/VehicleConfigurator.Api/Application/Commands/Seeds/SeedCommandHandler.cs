﻿using MediatR;
using NexCore.Infrastructure.Seeds;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Seeds
{
    public class SeedCommandHandler : IRequestHandler<SeedCommand>
    {
        private readonly IDbContextSeeder<VehicleConfiguratorContext> _context;

        public SeedCommandHandler(IDbContextSeeder<VehicleConfiguratorContext> context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<Unit> Handle(SeedCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            await _context.Seed();

            return Unit.Value;
        }
    }
}
