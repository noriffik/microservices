﻿using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Api.Application.Commands
{
    public interface IDeleteCommandMapper<TCommand>
    {
        int Map(TCommand command);
    }

    public interface IDeleteCommandMapper<TCommand, TId>
        where TId : EntityId
    {
        TId Map(TCommand command);
    }
}
