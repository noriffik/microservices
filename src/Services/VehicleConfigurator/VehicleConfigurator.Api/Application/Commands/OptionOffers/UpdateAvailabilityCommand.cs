﻿using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using System.Runtime.Serialization;
using MediatR;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    [DataContract]
    public class UpdateAvailabilityCommand : IRequest
    {
        [DataMember]
        [FromRoute]
        public int VehicleOfferId { get; set; }

        [DataMember]
        [FromRoute]
        public string PrNumber { get; set; }

        [DataMember]
        [FromBody]
        public AvailabilityModel Availability { get; set; }
    }
}
