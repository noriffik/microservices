﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    public class UpdateAvailabilitiesCommandHandler : IRequestHandler<UpdateAvailabilitiesCommand>
    {
        private readonly IUnitOfWork _work;
        private readonly IOptionOfferService _service;
        private ITwoWayMapper<Availability, AvailabilityModel> _availabilityMapper;

        public UpdateAvailabilitiesCommandHandler(IUnitOfWork work, IOptionOfferService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public ITwoWayMapper<Availability, AvailabilityModel> AvailabilityMapper
        {
            get => _availabilityMapper ?? (_availabilityMapper = new AvailabilityModelMapper());
            set => _availabilityMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public async Task<Unit> Handle(UpdateAvailabilitiesCommand request, CancellationToken cancellationToken)
        {
            if(request == null)
                throw new ArgumentNullException(nameof(request));

            var prNumber = PrNumber.Parse(request.PrNumber);

            var optionOffers = new BoundModelKeyDictionary<Availability>(
                request.Offers.Select(MapOfferModel));
            
            try
            {
                await _service.UpdateAvailabilities(prNumber, optionOffers);
            }
            catch (InvalidEntityIdException e)
            {
                throw ValidationExtensions.ExceptionForInvalidEntityId(e);
            }
            catch (SingleModelViolationException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(nameof(request.Offers), e.Message);
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }

        private KeyValuePair<ModelKey, Availability> MapOfferModel(AvailabilityPerModelKeyModel offer)
        {
            return new KeyValuePair<ModelKey, Availability>(
                ModelKey.Parse(offer.ModelKey),
                AvailabilityMapper.MapReverse(offer.Availability));
        }
    }
}
