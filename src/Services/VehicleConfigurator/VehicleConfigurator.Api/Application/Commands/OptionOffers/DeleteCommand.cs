﻿using System.Runtime.Serialization;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    [DataContract]
    public class DeleteCommand : IRequest
    {
        [DataMember]
        [FromRoute]
        public int VehicleOfferId { get; set; }

        [DataMember]
        [FromRoute]
        public string PrNumber { get; set; }
    }
}
