﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    public class AddCommandHandler : IRequestHandler<AddCommand>
    {
        private readonly IUnitOfWork _work;
        private readonly IOptionOfferService _service;
        private ITwoWayMapper<Availability, AvailabilityModel> _availabilityMapper;

        public AddCommandHandler(IUnitOfWork work, IOptionOfferService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public ITwoWayMapper<Availability, AvailabilityModel> AvailabilityMapper
        {
            get => _availabilityMapper ?? (_availabilityMapper = new AvailabilityModelMapper());
            set => _availabilityMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public async Task<Unit> Handle(AddCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var prNumber = PrNumber.Parse(request.PrNumber);
            var availability = AvailabilityMapper.MapReverse(request.Availability);

            try
            {
                await _service.Add(request.VehicleOfferId, prNumber, availability);
            }
            catch (InvalidEntityIdException e)
            {
                throw ValidationExtensions.ExceptionForInvalidEntityId(e);
            }
            catch (DuplicateOptionOfferException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(nameof(request.PrNumber), e.Message, e.PrNumber);
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
