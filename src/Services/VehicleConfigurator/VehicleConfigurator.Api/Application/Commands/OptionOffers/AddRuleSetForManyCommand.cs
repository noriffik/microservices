﻿using MediatR;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    [DataContract]
    public class AddRuleSetForManyCommand : IRequest
    {
        [DataMember]
        public int CatalogId { get; set; }

        [DataMember]
        public string ModelKeySet { get; set; }

        [DataMember]
        public string OptionId { get; set; }

        [DataMember]
        public RuleSetModel RuleSet { get; set; }
    }
}
