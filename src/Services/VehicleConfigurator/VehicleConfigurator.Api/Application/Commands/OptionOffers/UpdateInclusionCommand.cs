﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    [DataContract]
    public class UpdateInclusionCommand : IRequest
    {
        [DataMember]
        [FromRoute]
        public int VehicleOfferId { get; set; }

        [DataMember]
        [FromRoute]
        public string PrNumber { get; set; }

        [DataMember]
        [FromBody]
        public Inclusion Inclusion { get; set; }
    }
}
