﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    public class DeleteCommandHandler : IRequestHandler<DeleteCommand>
    {
        private readonly IUnitOfWork _work;
        private readonly IOptionOfferService _service;

        public DeleteCommandHandler(IUnitOfWork work, IOptionOfferService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<Unit> Handle(DeleteCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var prNumber = PrNumber.Parse(request.PrNumber);

            try
            {
                await _service.Delete(request.VehicleOfferId, prNumber);
            }
            catch (InvalidEntityIdException e)
            {
                throw ValidationExtensions.ExceptionForInvalidEntityId(e);
            }
            catch (OptionOfferNotFoundException)
            {
                return Unit.Value;
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
