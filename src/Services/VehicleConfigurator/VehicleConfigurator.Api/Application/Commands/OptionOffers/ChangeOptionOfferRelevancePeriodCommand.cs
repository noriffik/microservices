﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Models;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    [DataContract]
    public class ChangeOptionOfferRelevancePeriodCommand : IRequest
    {
        [FromRoute]
        [DataMember]
        public int VehicleOfferId { get; set; }

        [FromRoute]
        [DataMember]
        public string PrNumber { get; set; }

        [FromBody]
        [DataMember]
        public PeriodModel RelevancePeriod { get; set; }
    }
}
