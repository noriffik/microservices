using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    public class ActualizeRelevanceCommandHandler : ActualizeRelevanceBaseCommandHandler<OptionOffer>
    {
        public ActualizeRelevanceCommandHandler(IUnitOfWork work) : base(work)
        {
        }

        protected override void ActualizePotentiallyIrrelevant(VehicleOffer vehicleOffer, DateTime date)
        {
            foreach (var optionOffer in vehicleOffer.Options)
            {
                var relevance = optionOffer.RelevancePeriod.IsIn(date)
                    ? Relevance.Relevant
                    : Relevance.Irrelevant;

                vehicleOffer.ChangeOptionOfferRelevance(optionOffer.PrNumber, relevance);
            }
        }
    }
}
