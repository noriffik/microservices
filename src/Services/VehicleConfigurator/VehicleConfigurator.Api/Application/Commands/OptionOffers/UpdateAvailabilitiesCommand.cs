﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    [DataContract]
    public class UpdateAvailabilitiesCommand : IRequest
    {
        [DataMember]
        [FromRoute]
        public string PrNumber { get; set; }

        [DataMember]
        [FromBody]
        public IEnumerable<AvailabilityPerModelKeyModel> Offers { get; set; }
    }
}
