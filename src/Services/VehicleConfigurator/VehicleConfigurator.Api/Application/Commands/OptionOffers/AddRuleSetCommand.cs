﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    [DataContract]
    public class AddRuleSetCommand : IRequest
    {
        [DataMember]
        [FromRoute]
        public int VehicleOfferId { get; set; }

        [DataMember]
        [FromRoute]
        public string PrNumber { get; set; }

        [DataMember]
        [FromBody]
        public RuleSetModel RuleSet { get; set; }
    }
}
