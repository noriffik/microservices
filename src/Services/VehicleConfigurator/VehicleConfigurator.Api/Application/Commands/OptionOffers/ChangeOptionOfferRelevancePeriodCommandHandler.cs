﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    public class ChangeOptionOfferRelevancePeriodCommandHandler : IRequestHandler<ChangeOptionOfferRelevancePeriodCommand>,
        IRequestHandler<ResetOptionOfferRelevancePeriodCommand>
    {
        private readonly IUnitOfWork _work;
        private readonly IOptionOfferService _service;

        public ChangeOptionOfferRelevancePeriodCommandHandler(IUnitOfWork work, IOptionOfferService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public Task<Unit> Handle(ChangeOptionOfferRelevancePeriodCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return ChangeRelevancePeriod(request.VehicleOfferId,
                PrNumber.Parse(request.PrNumber),
                new RelevancePeriod(request.RelevancePeriod.From, request.RelevancePeriod.To),
                cancellationToken);
        }

        public Task<Unit> Handle(ResetOptionOfferRelevancePeriodCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return ChangeRelevancePeriod(request.VehicleOfferId,
                PrNumber.Parse(request.PrNumber),
                RelevancePeriod.Empty,
                cancellationToken);
        }

        private async Task<Unit> ChangeRelevancePeriod(int vehicleOfferId, PrNumber prNumber, RelevancePeriod period,
            CancellationToken cancellationToken)
        {
            try
            {
                await _service.ChangeRelevancePeriod(vehicleOfferId, prNumber, period);
            }
            catch (OptionOfferNotFoundException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler("PrNumber", e.Message, e.PrNumber);
            }
            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
