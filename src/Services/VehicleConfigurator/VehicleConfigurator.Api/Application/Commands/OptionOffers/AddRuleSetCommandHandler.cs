﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    public class AddRuleSetCommandHandler : IRequestHandler<AddRuleSetCommand>, IRequestHandler<AddRuleSetForManyCommand>
    {
        private readonly IUnitOfWork _work;
        private readonly IOptionOfferService _service;
        private ITwoWayMapper<string, RuleSetModel> _ruleSetMapper;

        public AddRuleSetCommandHandler(IUnitOfWork work, IOptionOfferService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public ITwoWayMapper<string, RuleSetModel> RuleSetMapper
        {
            get => _ruleSetMapper ?? (_ruleSetMapper = new RuleSetModelMapper());
            set => _ruleSetMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public async Task<Unit> Handle(AddRuleSetCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var prNumber = PrNumber.Parse(request.PrNumber);
            var rules = RuleSetMapper.MapReverse(request.RuleSet);

            try
            {
                await _service.AddRuleSet(request.VehicleOfferId, prNumber, rules);
            }
            catch (InvalidEntityIdException e)
            {
                throw ValidationExtensions.ExceptionForInvalidEntityId(e);
            }
            catch (OptionOfferNotFoundException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(
                    nameof(request.PrNumber), e.Message, request.PrNumber);
            }
            catch (SelfReferenceRuleException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(nameof(request.PrNumber), e.Message);
            }
            catch (RuleConflictException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(nameof(request.PrNumber), e.Message);
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }

        public async Task<Unit> Handle(AddRuleSetForManyCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var modelKeySet = ModelKeySet.Parse(request.ModelKeySet);
            var rules = RuleSetMapper.MapReverse(request.RuleSet);
            var optionId = OptionId.Parse(request.OptionId);

            try
            {
                await _service.AddRuleSet(request.CatalogId, modelKeySet, optionId, rules);
            }
            catch (OptionOfferNotFoundException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(
                    nameof(request.OptionId), e.Message, request.OptionId);
            }
            catch (SelfReferenceRuleException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(
                    nameof(request.OptionId), e.Message, request.OptionId);
            }
            catch (RuleConflictException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(
                    nameof(request.OptionId), e.Message, request.OptionId);
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
