﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    [DataContract]
    public class ResetOptionOfferRelevancePeriodCommand : IRequest
    {
        [DataMember]
        public int VehicleOfferId { get; set; }

        [DataMember]
        public string PrNumber { get; set; }
    }
}
