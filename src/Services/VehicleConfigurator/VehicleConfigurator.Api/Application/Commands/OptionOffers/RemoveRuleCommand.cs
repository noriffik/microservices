﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    public class RemoveRuleCommand: IRequest
    {
        [DataMember]
        [FromRoute]
        public int VehicleOfferId { get; set; }

        [DataMember]
        [FromRoute]
        public string PrNumber { get; set; }
    }
}
