﻿using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers
{
    [DataContract]
    public class AvailabilityPerModelKeyModel
    {
        [DataMember]
        public string ModelKey { get; set; }

        [DataMember]
        public AvailabilityModel Availability { get; set; }
    }
}
