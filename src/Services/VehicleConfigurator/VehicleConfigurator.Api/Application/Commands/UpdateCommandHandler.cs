﻿using MediatR;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands
{
    public abstract class UpdateCommandHandler<TCommand, TEntity> : IRequestHandler<TCommand, Unit>
        where TCommand : IUpdateCommand<TEntity>
        where TEntity : Entity, IAggregateRoot
    {
        protected readonly IUnitOfWork Work;
        protected readonly IUpdateCommandMapper<TCommand, TEntity> CommandMapper;

        protected UpdateCommandHandler(IUnitOfWork work,
            IUpdateCommandMapper<TCommand, TEntity> commandMapper)
        {
            Work = work ?? throw new ArgumentNullException(nameof(work));
            CommandMapper = commandMapper ?? throw new ArgumentNullException(nameof(commandMapper));
        }

        public virtual async Task<Unit> Handle(TCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var entity = await Work.EntityRepository.Require<TEntity>(CommandMapper.MapId(request), cancellationToken);

            CommandMapper.Map(request, entity);

            await Work.Commit(cancellationToken);

            return Unit.Value;
        }
    }

    public abstract class UpdateCommandHandler<TCommand, TEntity, TId> : IRequestHandler<TCommand, Unit>
        where TCommand : IUpdateCommand<TEntity, TId>
        where TEntity : Entity<TId>, IAggregateRoot<TId>
        where TId : EntityId
    {
        protected readonly IUnitOfWork Work;
        protected readonly IUpdateCommandMapper<TCommand, TEntity, TId> CommandMapper;

        protected UpdateCommandHandler(IUnitOfWork work,
            IUpdateCommandMapper<TCommand, TEntity, TId> commandMapper)
        {
            Work = work ?? throw new ArgumentNullException(nameof(work));
            CommandMapper = commandMapper ?? throw new ArgumentNullException(nameof(commandMapper));
        }

        public virtual async Task<Unit> Handle(TCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var entity = await Work.EntityRepository.Require<TEntity, TId>(CommandMapper.MapId(request), cancellationToken);

            CommandMapper.Map(request, entity);

            await Work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
