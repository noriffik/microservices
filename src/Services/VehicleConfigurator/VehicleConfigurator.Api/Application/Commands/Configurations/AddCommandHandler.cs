﻿using MediatR;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    public class AddCommandHandler : IRequestHandler<AddCommand, int>
    {
        private readonly IUnitOfWork _work;
        private readonly IConfigurationService _service;

        public AddCommandHandler(IUnitOfWork work, IConfigurationService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<int> Handle(AddCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var configuration = await _service.Add(request.CatalogId, ModelKey.Parse(request.ModelKey));

            await _work.Commit(CancellationToken.None);

            return configuration.Id;
        }
    }
}
