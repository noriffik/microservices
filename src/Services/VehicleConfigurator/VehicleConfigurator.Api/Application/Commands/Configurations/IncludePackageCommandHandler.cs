﻿using MediatR;
using NexCore.Domain;
using NexCore.Application.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    public class IncludePackageCommandHandler : IRequestHandler<IncludePackageCommand>
    {
        private readonly IUnitOfWork _work;
        private readonly IConfigurationService _service;

        public IncludePackageCommandHandler(IUnitOfWork work, IConfigurationService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<Unit> Handle(IncludePackageCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            try
            {
                await _service.IncludePackage(request.ConfigurationId, PackageCode.Parse(request.PackageCode));
            }
            catch (PackageRequirementViolationException e)
            {
                ValidationExtensions.ExceptionForCommandHandler(nameof(request.ConfigurationId), e.Message);
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
