﻿using System;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Configurations;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    public class DeleteCommandHandler:  IRequestHandler<DeleteCommand, Unit>
    {
        private readonly IUnitOfWork _work;
        private readonly IConfigurationService _service;

        public DeleteCommandHandler(IUnitOfWork work, IConfigurationService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }


        public async Task<Unit> Handle(DeleteCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            await _service.Remove(request.ConfigurationId);

            await _work.Commit(CancellationToken.None);

            return Unit.Value;
        }
    }
}
