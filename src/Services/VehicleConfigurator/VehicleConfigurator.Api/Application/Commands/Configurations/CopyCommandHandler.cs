﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    public class CopyCommandHandler : IRequestHandler<CopyCommand, int>
    {
        private readonly IUnitOfWork _work;
        private readonly IConfigurationService _service;

        public CopyCommandHandler(IUnitOfWork work, IConfigurationService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<int> Handle(CopyCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var configuration = await _service.Copy(request.ConfigurationId);

            await _work.Commit(CancellationToken.None);

            return configuration.Id;
        }
    }
}
