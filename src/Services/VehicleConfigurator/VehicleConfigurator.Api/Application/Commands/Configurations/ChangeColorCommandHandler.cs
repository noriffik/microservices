﻿using MediatR;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Validations;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    public class ChangeColorCommandHandler : IRequestHandler<ChangeColorCommand>
    {
        private readonly IUnitOfWork _work;

        private readonly IConfigurationService _service;

        public ChangeColorCommandHandler(IUnitOfWork work, IConfigurationService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<Unit> Handle(ChangeColorCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            try
            {
                await _service.ChangeColor(request.ConfigurationId, ColorId.Parse(request.ColorId));
            }
            catch (ColorOfferNotFoundException e)
            {
                ValidationExtensions.ExceptionForCommandHandler(nameof(request.ColorId), e.Message, request.ColorId);
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
