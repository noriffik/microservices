﻿using System.Collections.Generic;
using NexCore.VehicleConfigurator.Domain.Rules;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    public class ValidationResponse
    {
        public bool Result { get; private set; }
        public string Message { get; private set; }
        public IEnumerable<RuleViolation> Details { get; private set; } = new List<RuleViolation>();

        public static ValidationResponse Positive()
        {
            return new ValidationResponse
            {
                Result = true,
                Message = "Configuration is valid."
            };
        }

        public static ValidationResponse Negative(string message, IEnumerable<RuleViolation> details)
        {
            return new ValidationResponse
            {
                Result = false,
                Message = message,
                Details = details ?? new List<RuleViolation>()
            };
        }
    }
}
