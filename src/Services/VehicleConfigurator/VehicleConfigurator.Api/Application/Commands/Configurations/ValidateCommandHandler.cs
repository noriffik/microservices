﻿using MediatR;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    public class ValidateCommandHandler : IRequestHandler<ValidateCommand, ValidationResponse>
    {
        private readonly IConfigurationValidationService _validator;

        public ValidateCommandHandler(IConfigurationValidationService validator)
        {
            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
        }

        public async Task<ValidationResponse> Handle(ValidateCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            try
            {
                await _validator.ValidateConfiguration(Year.From(request.ModelYear), ModelKey.Parse(request.ModelKey), PrNumberSet.Parse(request.PrNumberSet));
            }
            catch (UnavailableOptionException e)
            {
                return ValidationResponse.Negative(e.Message, e.Details);
            }
            
            return ValidationResponse.Positive();
        }
    }
}
