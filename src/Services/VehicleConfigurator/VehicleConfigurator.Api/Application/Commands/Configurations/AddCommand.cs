﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    [DataContract]
    public class AddCommand : IRequest<int>
    {
        [DataMember]
        public int CatalogId { get; set; }

        [DataMember]
        public string ModelKey { get; set; }
    }
}
