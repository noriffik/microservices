﻿using MediatR;
using NexCore.Application.Extensions;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Validations.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    public class ToggleOptionCommandHandler : IRequestHandler<IncludeOptionCommand, Unit>,
        IRequestHandler<ExcludeOptionCommand, Unit>
    {
        private readonly IUnitOfWork _work;
        private readonly IConfigurationService _service;

        public ToggleOptionCommandHandler(IUnitOfWork work, IConfigurationService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<Unit> Handle(IncludeOptionCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            try
            {
                await _service.IncludeOption(request.ConfigurationId, PrNumber.Parse(request.PrNumber));
            }
            catch (UnavailableOptionException e)
            {
                OptionValidationExtensions.Throw(e, nameof(request.PrNumber),
                    PrNumber.Parse(request.PrNumber));
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }

        public async Task<Unit> Handle(ExcludeOptionCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            try
            {
                await _service.ExcludeOption(request.ConfigurationId, PrNumber.Parse(request.PrNumber));
            }
            catch (PackageRequirementViolationException e)
            {
                ValidationExtensions.ExceptionForCommandHandler(string.Empty, e.Message);
            }
            catch (UnavailableOptionException e)
            {
                OptionValidationExtensions.Throw(e, nameof(request.PrNumber),
                    PrNumber.Parse(request.PrNumber));
            }

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
