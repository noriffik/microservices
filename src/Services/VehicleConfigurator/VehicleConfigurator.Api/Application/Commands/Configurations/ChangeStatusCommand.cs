﻿using MediatR;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    [DataContract]
    public class ChangeStatusCommand : IRequest
    {
        [DataMember]
        public int ConfigurationId { get; set; }

        [DataMember]
        public ConfigurationStatus Status { get; set; }
    }
}
