﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    [DataContract]
    public class CopyCommand : IRequest<int>
    {
        [DataMember]
        public int ConfigurationId { get; set; }
    }
}
