﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    [DataContract]
    public class ExcludePackageCommand : IRequest
    {
        [DataMember]
        public int ConfigurationId { get; set; }
    }
}