﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    public class ChangeColorCommand : IRequest
    {
        [DataMember]
        public int ConfigurationId { get; set; }

        [DataMember]
        public string ColorId { get; set; }
    }
}
