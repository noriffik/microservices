﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    [DataContract]
    public class ValidateCommand : IRequest<ValidationResponse>
    {
        [DataMember]
        public int ModelYear { get; set; }

        [DataMember]
        public string ModelKey { get; set; }

        [DataMember]
        public string PrNumberSet { get; set; }
    }
}
