﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    [DataContract]
    public class IncludeOptionCommand : IRequest
    {
        [DataMember]
        public int ConfigurationId { get; set; }

        [DataMember]
        public string PrNumber { get; set; }
    }
}
