﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    public class ExcludePackageCommandHandler : IRequestHandler<ExcludePackageCommand>
    {
        private readonly IUnitOfWork _work;
        private readonly IConfigurationService _service;

        public ExcludePackageCommandHandler(IUnitOfWork work, IConfigurationService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<Unit> Handle(ExcludePackageCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            await _service.ExcludePackage(request.ConfigurationId);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
