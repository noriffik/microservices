﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    public class ChangeStatusCommandHandler : IRequestHandler<ChangeStatusCommand, Unit>
    {
        private readonly IUnitOfWork _work;

        private readonly IConfigurationService _service;

        public ChangeStatusCommandHandler(IUnitOfWork work, IConfigurationService service)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
        }

        public async Task<Unit> Handle(ChangeStatusCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            await _service.ChangeStatus(request.ConfigurationId, request.Status);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
