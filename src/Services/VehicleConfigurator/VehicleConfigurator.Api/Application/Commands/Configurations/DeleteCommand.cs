﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Commands.Configurations
{
    public class DeleteCommand: IRequest
    {
        [DataMember]
        public int ConfigurationId { get; set; }
    }
}