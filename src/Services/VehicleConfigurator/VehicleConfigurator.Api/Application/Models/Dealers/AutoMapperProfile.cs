﻿using AutoMapper;
using NexCore.Legal;

namespace NexCore.VehicleConfigurator.Api.Application.Models.Dealers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<OrganizationRequisites, CompanyRequisitesDto>()
                .ForMember(v => v.ShortName, m => m.MapFrom(s => s.Name.ShortName))
                .ForMember(v => v.FullName, m => m.MapFrom(s => s.Name.FullName));
        }
    }
}
