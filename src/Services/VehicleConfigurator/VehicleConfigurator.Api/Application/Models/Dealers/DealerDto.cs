﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Models.Dealers
{
    [DataContract]
    public class DealerDto
    {
        [DataMember] 
        public int CompanyId { get; set; }

        [DataMember] 
        public string DistributorId { get; set; }

        [DataMember] 
        public string DealerCode { get; set; }

        [DataMember] 
        public CompanyRequisitesDto Requisites { get; set; }
    }
}
