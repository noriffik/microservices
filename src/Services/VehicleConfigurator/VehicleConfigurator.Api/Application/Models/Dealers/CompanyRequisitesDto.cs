﻿using NexCore.VehicleConfigurator.Api.Application.Models.Customers;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Models.Dealers
{
    [DataContract]
    public class CompanyRequisitesDto
    {
        [DataMember]
        public string ShortName { get; set; }

        [DataMember]
        public string FullName { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public IEnumerable<string> Telephones { get; set; }

        [DataMember]
        public string BankRequisites { get; set; }

        [DataMember]
        public PersonNameDto Director { get; set; }
    }
}
