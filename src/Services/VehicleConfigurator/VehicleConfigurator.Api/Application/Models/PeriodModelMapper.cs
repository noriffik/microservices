﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Models
{
    public class PeriodModelMapper : BaseTwoWayMapper<Period, PeriodModel>, IPeriodModelMapper
    {
        public override PeriodModel Map(Period source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            return new PeriodModel
            {
                From = source.From.Value,
                To = source.To
            };
        }

        public override Period MapReverse(PeriodModel destination)
        {
            if (destination == null)
                throw new ArgumentNullException(nameof(destination));

            return new Period(destination.From, destination.To);
        }
    }
}
