﻿using NexCore.Domain;

namespace NexCore.VehicleConfigurator.Api.Application.Models
{
    public interface IPeriodModelMapper : ITwoWayMapper<Period, PeriodModel>
    {
    }
}
