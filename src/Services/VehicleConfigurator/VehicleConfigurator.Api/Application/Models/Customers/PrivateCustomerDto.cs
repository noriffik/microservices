﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Models.Customers
{
    [DataContract]
    public class PrivateCustomerDto
    {
        [DataMember] 
        public PersonNameDto PersonName { get; set; }

        [DataMember] 
        public string PhysicalAddress { get; set; }

        [DataMember] 
        public PassportDto Passport { get; set; }

        [DataMember] 
        public TaxIdentificationNumberDto TaxIdentificationNumber { get; set; }

        [DataMember] 
        public string Telephone { get; set; }
    }
}
