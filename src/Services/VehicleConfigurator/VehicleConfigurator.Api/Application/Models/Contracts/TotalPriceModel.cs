﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Models.Contracts
{
    [DataContract]
    public class TotalPriceModel
    {
        [DataMember]
        public PriceModel WithoutTax { get; set; }

        [DataMember]
        public PriceModel WithTax { get; set; }

        [DataMember]
        public PriceModel Tax { get; set; }
    }
}
