﻿using System;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Models.Contracts.VehicleSales
{
    [DataContract]
    public class VehicleSaleContractModel
    {
        [DataMember]
        public string Number { get; set; }

        [DataMember] 
        public string TypeId { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string DealerId { get; set; }

        [DataMember]
        public int PrivateCustomerId { get; set; }

        [DataMember]
        public int SalesManagerId { get; set; }

        [DataMember]
        public VehicleSaleContentModel Content { get; set; }

        [DataMember]
        public DateTime CreatedAt { get; set; }

        [DataMember]
        public DateTime UpdatedAt { get; set; }
    }
}
