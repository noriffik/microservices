﻿using NexCore.VehicleConfigurator.Api.Application.Models.Customers;
using NexCore.VehicleConfigurator.Api.Application.Models.Dealers;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Models.Contracts.VehicleSales
{
    [DataContract]
    public class VehicleSaleContentModel
    {
        [DataMember]
        public TotalPriceModel Price { get; set; }

        [DataMember]
        public IEnumerable<PriceModel> Payment { get; set; }

        [DataMember]
        public VehicleSpecificationModel Vehicle { get; set; }

        [DataMember]
        public PrivateCustomerDto PrivateCustomer { get; set; }

        [DataMember]
        public DealerDto Dealer { get; set; }

        [DataMember]
        public PersonNameDto SalesManagerName { get; set; }
    }
}
