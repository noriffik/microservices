﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Models.Contracts
{
    [DataContract]
    public class ContractTypeModel
    {
        [DataMember] 
        public string Id { get; set; }

        [DataMember] 
        public string Name { get; set; }
    }
}
