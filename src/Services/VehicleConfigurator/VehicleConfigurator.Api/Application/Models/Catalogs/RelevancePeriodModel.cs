﻿using System;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Models.Catalogs
{
    [DataContract]
    public class RelevancePeriodModel
    {
        [DataMember]
        public DateTime? From { get; set; }

        [DataMember]
        public DateTime? To { get; set; }
    }
}
