﻿using System;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Models
{
    [DataContract]
    public class PeriodModel
    {
        [DataMember]
        public DateTime From { get; set; }

        [DataMember]
        public DateTime? To { get; set; }
    }
}
