﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers
{
    [DataContract]
    public class RuleModel
    {
        [DataMember] 
        public string RuleName { get; set; }

        [DataMember] 
        public string PrNumbers { get; set; }

        [DataMember] 
        public RuleSetModel NestedRuleSet { get; set; }

        public override string ToString()
        {
            var requirement = "";
            if (NestedRuleSet != null)
                foreach (var nestedRule in NestedRuleSet.RuleSet)
                    requirement = requirement + nestedRule;

            return $"{RuleName}: {PrNumbers}+[{(NestedRuleSet == null ? "" : NestedRuleSet.ToString())}]";
        }
    }
}
