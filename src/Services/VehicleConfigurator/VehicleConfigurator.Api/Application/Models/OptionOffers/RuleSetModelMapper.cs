﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Rules;
using System;
using System.Linq;

namespace NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers
{
    public class RuleSetModelMapper : BaseTwoWayMapper<string, RuleSetModel>
    {
        private IRuleDescriber _ruleDescriber;

        public IRuleDescriber RuleDescriber
        {
            get => _ruleDescriber ?? (_ruleDescriber = new RuleDescriber());
            set => _ruleDescriber = value ?? throw new ArgumentNullException(nameof(value));
        }

        public override RuleSetModel Map(string source)
        {
            if (string.IsNullOrEmpty(source))
                return null;
            
            return new RuleSetModel
            {
                RuleSet = RuleDescriber
                    .DescribeMany(source)
                    .Select(MapRuleModel)
            };
        }

        public override string MapReverse(RuleSetModel destination)
        {
            var result = "";

            foreach (var ruleModel in destination.RuleSet)
            {
                result += "[" + ruleModel + "]";
            }

            return result;
        }

        private RuleModel MapRuleModel(IRuleDescription description)
        {
            return new RuleModel
            {
                RuleName = description.Name,
                PrNumbers = description.PrNumbers.AsString,
                NestedRuleSet = Map(description.Requirement)
            };
        }
    }
}
