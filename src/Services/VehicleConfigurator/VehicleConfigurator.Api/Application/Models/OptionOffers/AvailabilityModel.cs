﻿using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers
{
    [DataContract]
    public class AvailabilityModel
    {
        [DataMember]
        public AvailabilityType Type { get; set; }

        [DataMember]
        public ReasonType? Reason { get; set; }

        [DataMember]
        public decimal Price { get; set; }
    }
}