﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers
{
    public class AvailabilityModelMapper : BaseTwoWayMapper<Availability, AvailabilityModel>
    {
        public override AvailabilityModel Map(Availability source)
        {
            return source != null
                ? new AvailabilityModel
                {
                    Type = source.Type,
                    Reason = source.Reason,
                    Price = source.Price
                }
                : null;
        }

        public override Availability MapReverse(AvailabilityModel destination)
        {
            if (destination == null)
                throw new ArgumentNullException(nameof(destination));

            switch (destination.Type)
            {
                case AvailabilityType.Included:
                    return Availability.Included(destination.Reason ?? ReasonType.SerialEquipment);
                case AvailabilityType.Purchasable:
                    return Availability.Purchasable(destination.Price);
            }

            throw new Exception($"Unknown {nameof(AvailabilityType)} given.");
        }
    }
}