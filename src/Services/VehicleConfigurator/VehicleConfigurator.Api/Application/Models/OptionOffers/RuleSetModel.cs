﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers
{
    [DataContract]
    public class RuleSetModel
    {
        [DataMember]
        public IEnumerable<RuleModel> RuleSet { get; set; }

        public override string ToString()
        {
            var requirement = "";
            if (RuleSet != null)
                foreach (var nestedRule in RuleSet)
                    requirement = $"{requirement}[{nestedRule}]";

            return requirement;
        }
    }
    
}
