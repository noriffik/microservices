﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Gearboxes
{
    public class GearboxModelMapper : BaseMapper<Gearbox, GearboxModel>
    {
        public override GearboxModel Map(Gearbox source)
        {
            return source != null
                ? new GearboxModel
                {
                    Id = source.Id.Value,
                    Name = source.Name,
                    ModelId = source.ModelId.Value,
                    Type = source.Type.Value,
                    GearBoxCategory = source.GearBoxCategory
                }
                : null;
        }
    }
}
