﻿using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Gearboxes
{
    [DataContract]
    public class GearboxModel : ModelComponentModel
    {
        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public GearBoxCategory? GearBoxCategory { get; set; }
    }
}
