﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents
{
    [DataContract]
    public class ModelComponentModel
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string ModelId { get; set; }
    }
}
