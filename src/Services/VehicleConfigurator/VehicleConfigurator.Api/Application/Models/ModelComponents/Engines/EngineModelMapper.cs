﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Engines
{
    public class EngineModelMapper : BaseMapper<Engine, EngineModel>
    {
        public override EngineModel Map(Engine source)
        {
            return source != null
                ? new EngineModel
                {
                    Id = source.Id.Value,
                    Name = source.Name,
                    ModelId = source.ModelId.Value,
                    FuelType = source.FuelType
                }
                : null;
        }
    }
}
