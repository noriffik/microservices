﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;

namespace NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Engines
{
    public class FindQueryHandler : FindQueryHandler<Engine, EngineId>
    {
        public FindQueryHandler(VehicleConfiguratorContext context) : base(context)
        {
            ModelMapper = new EngineModelMapper();
        }
    }
}
