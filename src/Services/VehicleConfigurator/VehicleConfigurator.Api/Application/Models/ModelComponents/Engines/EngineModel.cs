﻿using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Engines
{
    [DataContract]
    public class EngineModel : ModelComponentModel
    {
        [DataMember]
        public FuelType? FuelType { get; set; }
    }
}
