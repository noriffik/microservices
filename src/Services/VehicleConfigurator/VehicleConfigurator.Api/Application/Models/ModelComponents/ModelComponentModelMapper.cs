﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents
{
    public class ModelComponentModelMapper<TComponent, TId> : BaseMapper<TComponent, ModelComponentModel>
        where TComponent : ModelComponent<TId>
        where TId : EntityId
    {
        public override ModelComponentModel Map(TComponent source)
        {
            return source != null
                ? new ModelComponentModel
                {
                    Id = source.Id.Value,
                    Name = source.Name,
                    ModelId = source.ModelId.Value
                }
                : null;
        }
    }
}
