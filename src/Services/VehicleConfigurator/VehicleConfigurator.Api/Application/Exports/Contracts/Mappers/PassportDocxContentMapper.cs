﻿using NexCore.Legal;
using System.Collections.Generic;
using TemplateEngine.Docx;

namespace NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.Mappers
{
    public class PassportDocxContentMapper
    {
        public virtual IEnumerable<IContentItem> Map(Passport passport, string prefix = "")
        {
            yield return new FieldContent($"{prefix}PassportSeries", passport.Series);
            yield return new FieldContent($"{prefix}PassportNumber", passport.Number);
            yield return new FieldContent($"{prefix}PassportIssuer", passport.Issue.Issuer);
            yield return new FieldContent($"{prefix}PassportIssuedOn", passport.Issue.IssuedOn.ToString());
        }
    }
}