﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.Specifications;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using TemplateEngine.Docx;

namespace NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.Mappers
{
    public class OptionDocxListContentMapper
    {
        private readonly IUnitOfWork _work;

        [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Used for testing purposes.")]
        protected OptionDocxListContentMapper()
        {
        }

        public OptionDocxListContentMapper(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public virtual async Task<IContentItem> Map(string listName, string fieldName, PrNumberSet prNumbers, ModelId modelId)
        {
            if (listName == null)
                throw new ArgumentNullException(nameof(listName));

            if (fieldName == null)
                throw new ArgumentNullException(nameof(fieldName));

            if (prNumbers == null)
                throw new ArgumentNullException(nameof(prNumbers));

            if (modelId == null)
                throw new ArgumentNullException(nameof(modelId));

            if (prNumbers == PrNumberSet.Empty)
                return CreateEmptyList(listName, fieldName);

            var options = await FindOptions(prNumbers, modelId);

            var listContent = new ListContent(listName);

            foreach (var prNumber in prNumbers.Values)
            {
                var contentItem = options.ContainsKey(prNumber)
                    ? MapOption(fieldName, options[prNumber])
                    : MapOption(fieldName, prNumber);

                listContent.AddItem(contentItem);
            }

            return listContent;
        }

        private static ListContent CreateEmptyList(string listName, string fieldName)
        {
            return new ListContent(listName).AddItem(new FieldContent(fieldName, string.Empty));
        }

        private async Task<Dictionary<PrNumber, Option>> FindOptions(PrNumberSet prNumbers, ModelId modelId)
        {
            var options = await _work.EntityRepository.Find<Option, OptionId>(new OptionWithPrNumberSpecification(prNumbers, modelId));

            return options.ToDictionary(o => o.PrNumber);
        }

        private static IContentItem MapOption(string fieldName, Option option)
        {
            return new FieldContent(fieldName, $"{option.PrNumber}, {option.Name}");
        }

        private static IContentItem MapOption(string fieldName, PrNumber prNumber)
        {
            return new FieldContent(fieldName, prNumber.Value);
        }
    }
}