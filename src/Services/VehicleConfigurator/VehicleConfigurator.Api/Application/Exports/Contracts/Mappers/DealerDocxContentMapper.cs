﻿using NexCore.Legal;
using System;
using System.Collections.Generic;
using TemplateEngine.Docx;

namespace NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.Mappers
{
    public class DealerDocxContentMapper
    {
        public virtual IEnumerable<IContentItem> Map(OrganizationRequisites requisites, string city, string headPosition, string prefix = "")
        {
            if (requisites == null)
                throw new ArgumentNullException(nameof(requisites));

            if (city == null)
                throw new ArgumentNullException(nameof(city));

            if (headPosition == null)
                throw new ArgumentNullException(nameof(headPosition));

            return new[]
            {
                new FieldContent($"{prefix}DealersCity", city),
                new FieldContent($"{prefix}DealerFullName", requisites.Name.FullName),
                new FieldContent($"{prefix}DealersHeadPosition", headPosition),
                new FieldContent($"{prefix}DirectorFullName", requisites.Director.ToString()),
                new FieldContent($"{prefix}DealerShortName", requisites.Name.ShortName),
                new FieldContent($"{prefix}DealerAddress", requisites.Address)
            };
        }
    }
}