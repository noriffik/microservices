﻿using NexCore.VehicleConfigurator.Domain.Customers;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using TemplateEngine.Docx;

namespace NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.Mappers
{
    public class PrivateCustomerDocxContentMapper
    {
        private readonly PassportDocxContentMapper _passportMapper;

        [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Used for testing purposes.")]
        protected PrivateCustomerDocxContentMapper()
        {
        }

        public PrivateCustomerDocxContentMapper(PassportDocxContentMapper passportMapper)
        {
            _passportMapper = passportMapper ?? throw new ArgumentNullException(nameof(passportMapper));
        }

        public virtual IEnumerable<IContentItem> Map(PrivateCustomer customer, string prefix = "")
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            if (prefix == null)
                throw new ArgumentNullException(nameof(prefix));

            return new[]
            {
                new FieldContent($"{prefix}PCFullName", customer.PersonName.ToString()),
                new FieldContent($"{prefix}PCTelephone", customer.Telephone),
                new FieldContent($"{prefix}PCPhysicalAddress", customer.PhysicalAddress),
                new FieldContent($"{prefix}TaxIdentificationNumber", customer.TaxIdentificationNumber.Number),
                new FieldContent($"{prefix}PCShortName", customer.PersonName.Lastname)
            }.Concat(_passportMapper.Map(customer.Passport, prefix));
        }
    }
}
