﻿using NexCore.VehicleConfigurator.Domain.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using TemplateEngine.Docx;

namespace NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.Mappers
{
    public class VehicleDocxContentMapper
    {
        private readonly OptionDocxListContentMapper _optionsMapper;

        [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Used for testing purposes.")]
        protected VehicleDocxContentMapper()
        {
        }

        public VehicleDocxContentMapper(OptionDocxListContentMapper optionsMapper)
        {
            _optionsMapper = optionsMapper ?? throw new ArgumentNullException(nameof(optionsMapper));
        }

        public virtual async Task<IEnumerable<IContentItem>> Map(VehicleSpecification specification, string modelName, string colorName, string prefix = "")
        {
            if (specification == null)
                throw new ArgumentNullException(nameof(specification));

            if (modelName == null)
                throw new ArgumentNullException(nameof(modelName));

            if (colorName == null)
                throw new ArgumentNullException(nameof(colorName));

            var modelId = specification.ModelKey.ModelId;

            return new[]
            {
                new FieldContent($"{prefix}ModelName", modelName),
                new FieldContent($"{prefix}ModelYear", specification.ModelYear.Value.ToString()),
                new FieldContent($"{prefix}ColorName", colorName),
                new FieldContent($"{prefix}ColorId", specification.ColorId.Value),
                await _optionsMapper.Map($"{prefix}AdditionalOptions", $"{prefix}AdditionalPrNumbers", specification.AdditionalOptions, modelId),
                await _optionsMapper.Map($"{prefix}IncludedOptions", $"{prefix}IncludedPrNumbers", specification.IncludedOptions, modelId)
            };
        }
    }
}