﻿using NexCore.VehicleConfigurator.Domain.Contracts;
using System;
using System.Collections.Generic;
using System.Globalization;
using TemplateEngine.Docx;

namespace NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.Mappers
{
    public class TotalPriceDocxContentMapper
    {
        private static readonly CultureInfo Culture = CultureInfo.CurrentCulture;

        public virtual IEnumerable<IContentItem> Map(TotalPrice price, int count, string prefix = "")
        {
            if (price == null)
                throw new ArgumentNullException(nameof(price));

            return new[]
            {
                new FieldContent($"{prefix}TotalPriceWithoutTaxRetail", price.WithoutTax.Retail.ToString(Culture)),
                new FieldContent($"{prefix}TotalPriceTaxRetail", price.Tax.Retail.ToString(Culture)),
                new FieldContent($"{prefix}TotalPriceWithTaxRetail", price.WithTax.Retail.ToString(Culture)),
                new FieldContent($"{prefix}Count", count.ToString()),
                new FieldContent($"{prefix}TotalCount", count.ToString()),
                new FieldContent($"{prefix}TotalPriceWithTaxBase", price.WithTax.Base.ToString(Culture))
            };
        }
    }
}