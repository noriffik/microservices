﻿using NexCore.VehicleConfigurator.Domain.Contracts;
using System.IO;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Exports.Contracts
{
    public interface IContractDocxWriter<T> where T : Content
    {
        Task<Stream> Write(Stream template, T contractContent);
        Task<Stream> Write(byte[] template, T contractContent);
    }
}