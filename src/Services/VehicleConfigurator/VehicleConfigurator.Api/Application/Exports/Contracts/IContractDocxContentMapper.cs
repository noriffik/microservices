﻿using System.Threading.Tasks;
using TemplateEngine.Docx;

namespace NexCore.VehicleConfigurator.Api.Application.Exports.Contracts
{
    public interface IContractDocxContentMapper<TContractContent> where TContractContent : Domain.Contracts.Content
    {
        Task<Content> Map(TContractContent contract);
    }
}