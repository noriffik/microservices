﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Exports.Contracts
{
    public class ContractTemplateNotFoundException : DomainException
    {
        private const string DefaultMessage = "Contract template not found.";

        public ContractTemplateNotFoundException() : base(DefaultMessage)
        {
        }

        public ContractTemplateNotFoundException(string message) : base(message)
        {
        }

        public ContractTemplateNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}