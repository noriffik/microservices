﻿using NexCore.VehicleConfigurator.Domain.Contracts;
using System;
using System.IO;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Exports.Contracts
{
    public class DocxWriter<TContractContent> : IContractDocxWriter<TContractContent> where TContractContent : Content
    {
        private readonly IContractDocxContentMapper<TContractContent> _mapper;

        public DocxTemplateProcessorFactory TemplateProcessorFactory { get; set; } = new DocxTemplateProcessorFactory();

        public DocxWriter(IContractDocxContentMapper<TContractContent> mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<Stream> Write(Stream template, TContractContent contractContent)
        {
            if (template == null)
                throw new ArgumentNullException(nameof(template));

            if (contractContent == null)
                throw new ArgumentNullException(nameof(contractContent));

            var documentContent = await _mapper.Map(contractContent);

            using (var document = TemplateProcessorFactory.Create(template))
            {
                document.FillContent(documentContent);
                document.SaveChanges();

                template.Seek(0, SeekOrigin.Begin);

                return template;
            }
        }

        public async Task<Stream> Write(byte[] template, TContractContent contractContent)
        {
            if (template == null)
                throw new ArgumentNullException(nameof(template));

            return await Write(new MemoryStream(template), contractContent);
        }
    }
}
