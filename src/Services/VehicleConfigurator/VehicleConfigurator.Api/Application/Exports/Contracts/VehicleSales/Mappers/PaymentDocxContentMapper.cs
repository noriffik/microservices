﻿using NexCore.VehicleConfigurator.Api.Localization;
using NexCore.VehicleConfigurator.Domain.Pricing;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using TemplateEngine.Docx;

namespace NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.VehicleSales.Mappers
{
    public class PaymentDocxContentMapper
    {
        private const string ExactlyPaymentsAreExpected = "Exactly 2 payments are expected.";

        private static readonly CultureInfo Culture = CultureInfo.CurrentCulture;

        private readonly ICurrencyWriter _currencyWriter;

        [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Used for testing purposes.")]
        protected PaymentDocxContentMapper()
        {
        }

        public PaymentDocxContentMapper(ICurrencyWriter currencyWriter)
        {
            _currencyWriter = currencyWriter ?? throw new ArgumentNullException(nameof(currencyWriter));
        }

        public virtual IEnumerable<IContentItem> Map(IEnumerable<Price> payments, string prefix = "")
        {
            if (payments == null)
                throw new ArgumentNullException(nameof(payments));

            if (prefix == null)
                throw new ArgumentNullException(nameof(prefix));

            var prices = payments.ToArray();
            if (prices.Length < 2)
                throw new ArgumentException(ExactlyPaymentsAreExpected, nameof(payments));

            return new[]
            {
                new FieldContent($"{prefix}FirstPaymentRetail", prices[0].Retail.ToString(Culture)),
                new FieldContent($"{prefix}FirstPaymentRetailString", _currencyWriter.Write(prices[0].Retail)),
                new FieldContent($"{prefix}FirstPaymentBase", prices[0].Base.ToString(Culture)),
                new FieldContent($"{prefix}SecondPaymentBase", prices[1].Base.ToString(Culture)),
            };
        }
    }
}