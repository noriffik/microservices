﻿using MediatR;
using System.IO;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.VehicleSales
{
    [DataContract]
    public class DocxExportQuery : IRequest<Stream>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
