﻿using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.Mappers;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TemplateEngine.Docx;

namespace NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.VehicleSales.Mappers
{
    public class ContractDocxContentMapper : IContractDocxContentMapper<VehicleSaleContent>
    {
        private readonly PaymentDocxContentMapper _paymentMapper;
        private readonly TotalPriceDocxContentMapper _priceMapper;
        private readonly DealerDocxContentMapper _dealerMapper;
        private readonly VehicleDocxContentMapper _vehicleMapper;
        private readonly PrivateCustomerDocxContentMapper _customerMapper;

        public ContractDocxContentMapper(PaymentDocxContentMapper paymentMapper, TotalPriceDocxContentMapper priceMapper,
            DealerDocxContentMapper dealerMapper, VehicleDocxContentMapper vehicleMapper,
            PrivateCustomerDocxContentMapper customerMapper)
        {
            _paymentMapper = paymentMapper ?? throw new ArgumentNullException(nameof(paymentMapper));
            _priceMapper = priceMapper ?? throw new ArgumentNullException(nameof(priceMapper));
            _dealerMapper = dealerMapper ?? throw new ArgumentNullException(nameof(dealerMapper));
            _vehicleMapper = vehicleMapper ?? throw new ArgumentNullException(nameof(vehicleMapper));
            _customerMapper = customerMapper ?? throw new ArgumentNullException(nameof(customerMapper));
        }

        public async Task<Content> Map(VehicleSaleContent contract)
        {
            if (contract == null)
                throw new ArgumentNullException(nameof(contract));

            return new Content(MapContract(contract)
                .Concat(_priceMapper.Map(contract.Price, 1))
                .Concat(_paymentMapper.Map(contract.Payment))
                .Concat(_dealerMapper.Map(contract.Dealer.Requisites, contract.DealersCity, contract.DealersHeadPosition))
                .Concat(_customerMapper.Map(contract.PrivateCustomer))
                .Concat(await _vehicleMapper.Map(contract.Vehicle, contract.VehicleModelName, contract.VehicleColorName))
                .ToArray());
        }

        private static IEnumerable<IContentItem> MapContract(VehicleSaleContent contractContent)
        {
            yield return new FieldContent("Number", contractContent.Number);
            yield return new FieldContent("Date", contractContent.Date.ToShortDateString());
            yield return new FieldContent("VehicleStockAddress", contractContent.VehicleStockAddress);
            yield return new FieldContent("VehicleDeliveryDays", contractContent.VehicleDeliveryDays.ToString());
            yield return new FieldContent("SalesManagerShortName", contractContent.SalesManagerName.ShortName);
        }
    }
}
