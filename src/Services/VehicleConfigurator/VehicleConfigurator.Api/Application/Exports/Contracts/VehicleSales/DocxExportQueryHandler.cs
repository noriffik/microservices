﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.VehicleSales
{
    public class DocxExportQueryHandler : IRequestHandler<DocxExportQuery, Stream>
    {
        private readonly VehicleConfiguratorContext _context;
        private readonly IContractDocxWriter<VehicleSaleContent> _contractWriter;

        public DocxExportQueryHandler(VehicleConfiguratorContext context, IContractDocxWriter<VehicleSaleContent> contractWriter)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _contractWriter = contractWriter ?? throw new ArgumentNullException(nameof(contractWriter));
        }

        public async Task<Stream> Handle(DocxExportQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var contract = await _context.VehicleSaleContracts
                .SingleOrDefaultAsync(o => o.Id == request.Id, cancellationToken);

            if (contract == null)
                return null;

            var template = await _context.ContractTemplate
                .Where(r => r.TypeId == contract.TypeId)
                .Where(r => r.DealerId == contract.DealerId)
                .SingleOrDefaultAsync(cancellationToken: cancellationToken);

            if (template == null)
                throw new ContractTemplateNotFoundException();

            return await _contractWriter.Write(template.Content, contract.Content);
        }
    }
}