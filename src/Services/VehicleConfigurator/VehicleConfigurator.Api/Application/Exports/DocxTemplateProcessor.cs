﻿using System;
using System.IO;
using System.Xml.Linq;
using TemplateEngine.Docx;

namespace NexCore.VehicleConfigurator.Api.Application.Exports
{
    public class DocxTemplateProcessor : TemplateProcessor, IDocxTemplateProcessor
    {
        public DocxTemplateProcessor(string fileName) : base(fileName)
        {
        }

        public DocxTemplateProcessor(Stream stream) : base(stream)
        {
        }

        public DocxTemplateProcessor(XDocument templateSource, XDocument stylesPart = null, XDocument numberingPart = null) : base(templateSource, stylesPart, numberingPart)
        {
        }
    }

    public interface IDocxTemplateProcessor : IDisposable
    {
        TemplateProcessor SetRemoveContentControls(bool isNeedToRemove);

        TemplateProcessor FillContent(Content content);

        void SaveChanges();
    }

    public class DocxTemplateProcessorFactory
    {
        public virtual IDocxTemplateProcessor Create(Stream template)
        {
            var processor = new DocxTemplateProcessor(template);

            processor.SetRemoveContentControls(true);

            return processor;
        }
    }
}
