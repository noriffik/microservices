﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Queries.ModelKeys;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelKeys
{
    public class FindByOptionIdValidator : AbstractValidator<FindByOptionIdQuery>
    {
        public FindByOptionIdValidator()
        {
            RuleFor(q => q.CatalogId).GreaterThan(default(int));
            RuleFor(q => q.OptionId).NotEmpty().SetValidator(new OptionIdValidator());
        }
    }
}
