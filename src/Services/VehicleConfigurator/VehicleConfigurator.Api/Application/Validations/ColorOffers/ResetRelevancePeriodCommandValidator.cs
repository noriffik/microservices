﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ColorOffers
{
    public class ResetRelevancePeriodCommandValidator : AbstractValidator<ResetColorOfferRelevancePeriodCommand>
    {
        public ResetRelevancePeriodCommandValidator()
        {
            RuleFor(c => c.VehicleOfferId).GreaterThan(0);
            RuleFor(c => c.ColorId).NotEmpty().SetValidator(new ColorIdValidator());
        }
    }
}
