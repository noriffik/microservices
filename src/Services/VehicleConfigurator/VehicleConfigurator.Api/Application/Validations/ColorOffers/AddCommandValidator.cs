﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ColorOffers
{
    public class AddCommandValidator : AbstractValidator<AddCommand>
    {
        public AddCommandValidator()
        {
            RuleFor(c => c.VehicleOfferId).GreaterThan(0);
            RuleFor(c => c.ColorId).NotEmpty().SetValidator(new ColorIdValidator());
            RuleFor(c => c.Price).GreaterThanOrEqualTo(0);
        }
    }
}
