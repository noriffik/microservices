﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ColorOffers
{
    public class FindByVehicleOfferIdAndColorIdQueryValidator : AbstractValidator<FindByVehicleOfferIdAndColorIdQuery>
    {
        public FindByVehicleOfferIdAndColorIdQueryValidator()
        {
            RuleFor(c => c.VehicleOfferId).GreaterThan(0);
            RuleFor(c => c.ColorId).NotEmpty().SetValidator(new ColorIdValidator());
        }
    }
}
