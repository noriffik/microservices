﻿using FluentValidation;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.Components;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Components.Models
{
    public class DeleteCommandValidator : AbstractValidator<DeleteCommand<Model, ModelId>>
    {
        public DeleteCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new ModelIdValidator());
        }
    }
}
