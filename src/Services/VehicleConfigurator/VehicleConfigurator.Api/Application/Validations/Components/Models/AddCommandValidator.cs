﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Components.Models;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Components.Models
{
    public class AddCommandValidator : AbstractValidator<AddCommand>
    {
        public AddCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new ModelIdValidator());
        }
    }
}
