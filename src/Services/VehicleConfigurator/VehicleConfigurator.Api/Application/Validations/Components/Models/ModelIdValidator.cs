﻿using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Components.Models
{
    public class ModelIdValidator : AbstractValidator<string>
    {
        public ModelIdValidator()
        {
            RuleFor(v => v).NotEmpty().NotNull()
                .Must(v => ModelId.TryParse(v, out _))
                .WithMessage("Invalid format.");
        }
    }
}
