﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.VehicleOffers
{
    public class ChangeVehicleOfferRelevancePeriodCommandValidator : AbstractValidator<ChangeVehicleOfferRelevancePeriodCommand>
    {
        public ChangeVehicleOfferRelevancePeriodCommandValidator()
        {
            RuleFor(c => c.Id).GreaterThan(0);
            RuleFor(c => c.RelevancePeriod).NotNull().SetValidator(new PeriodModelValidator());
        }
    }
}
