﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.VehicleOffers
{
    public class AddCommandValidator : AbstractValidator<AddCommand>
    {
        public AddCommandValidator()
        {
            RuleFor(c => c.ModelKey).NotEmpty().SetValidator(new ModelKeyValidator());
            RuleFor(c => c.CatalogId).GreaterThan(0);
            RuleFor(c => c.Price).GreaterThanOrEqualTo(0);
        }
    }
}
