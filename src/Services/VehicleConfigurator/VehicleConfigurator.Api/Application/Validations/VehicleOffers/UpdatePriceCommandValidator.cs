﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.VehicleOffers
{
    public class UpdatePriceCommandValidator : AbstractValidator<UpdatePriceCommand>
    {
        public UpdatePriceCommandValidator()
        {
            RuleFor(c => c.Id).GreaterThan(0);
            RuleFor(c => c.Price).GreaterThanOrEqualTo(0);
        }
    }
}
