﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.VehicleOffers
{
    public class ResetVehicleOfferRelevancePeriodCommandValidator : AbstractValidator<ResetVehicleOfferRelevancePeriodCommand>
    {
        public ResetVehicleOfferRelevancePeriodCommandValidator()
        {
            RuleFor(c => c.Id).GreaterThan(0);
        }
    }
}
