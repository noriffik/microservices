﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Configuration
{
    public class AddCommandValidator : AbstractValidator<AddCommand>
    {
        public AddCommandValidator()
        {
            RuleFor(c => c.CatalogId).GreaterThan(default(int));
            RuleFor(c => c.ModelKey).SetValidator(new ModelKeyValidator());
        }
    }
}
