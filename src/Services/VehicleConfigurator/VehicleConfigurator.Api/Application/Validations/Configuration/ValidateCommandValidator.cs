﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Configuration
{
    public class ValidateCommandValidator : AbstractValidator<ValidateCommand>
    {
        public ValidateCommandValidator()
        {
            RuleFor(c => c.ModelYear).GreaterThan(0);
            RuleFor(c => c.ModelKey).SetValidator(new ModelKeyValidator());
            RuleFor(c => c.PrNumberSet).SetValidator(new PrNumberSetValidator());
        }
    }
}
