﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Configuration
{
    public class CopyCommandValidator : AbstractValidator<CopyCommand>
    {
        public CopyCommandValidator()
        {
            RuleFor(c => c.ConfigurationId).GreaterThan(0);
        }
    }
}
