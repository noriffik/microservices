﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Configuration
{
    public class ExcludePackageCommandValidator : AbstractValidator<ExcludePackageCommand>
    {
        public ExcludePackageCommandValidator()
        {
            RuleFor(c => c.ConfigurationId).GreaterThan(0);
        }
    }
}