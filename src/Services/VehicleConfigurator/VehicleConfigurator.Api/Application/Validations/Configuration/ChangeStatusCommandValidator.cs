﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Configuration
{
    public class ChangeStatusCommandValidator : AbstractValidator<ChangeStatusCommand>
    {
        public ChangeStatusCommandValidator()
        {
            RuleFor(c => c.ConfigurationId).GreaterThan(default(int));
            RuleFor(c => c.Status).IsInEnum();
        }
    }
}
