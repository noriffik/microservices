﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Configuration
{
    public class ExcludeOptionCommandValidator : AbstractValidator<ExcludeOptionCommand>
    {
        public ExcludeOptionCommandValidator()
        {
            RuleFor(c => c.ConfigurationId).GreaterThan(default(int));
            RuleFor(c => c.PrNumber).NotEmpty().SetValidator(new PrNumberValidator());
        }
    }
}
