﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Api.Application.Validations.Packages;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Configuration
{
    public class IncludePackageCommandValidator : AbstractValidator<IncludePackageCommand>
    {
        public IncludePackageCommandValidator()
        {
            RuleFor(c => c.ConfigurationId).GreaterThan(0);
            RuleFor(c => c.PackageCode).NotEmpty().SetValidator(new PackageCodeValidator());
        }
    }
}
