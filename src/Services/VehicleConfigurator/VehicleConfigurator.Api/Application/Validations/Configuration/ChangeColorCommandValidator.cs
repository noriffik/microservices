﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Configuration
{
    public class ChangeColorCommandValidator : AbstractValidator<ChangeColorCommand>
    {
        public ChangeColorCommandValidator()
        {
            RuleFor(c => c.ConfigurationId).GreaterThan(0);
            RuleFor(c => c.ColorId).SetValidator(new ColorIdValidator());
        }
    }
}
