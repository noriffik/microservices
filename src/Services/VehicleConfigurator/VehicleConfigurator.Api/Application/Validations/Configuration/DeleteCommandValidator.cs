﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Configuration
{
    public class DeleteCommandValidator : AbstractValidator<DeleteCommand>
    {
        public DeleteCommandValidator()
        {
            RuleFor(c => c.ConfigurationId).GreaterThan(0);
        }
    }
}
