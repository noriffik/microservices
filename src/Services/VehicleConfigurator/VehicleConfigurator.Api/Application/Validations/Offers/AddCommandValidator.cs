﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Offers;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Offers
{
    public class AddCommandValidator : AbstractValidator<AddCommand>
    {
        public AddCommandValidator()
        {
            RuleFor(c => c.ConfigurationId).GreaterThanOrEqualTo(default(int));
        }
    }
}
