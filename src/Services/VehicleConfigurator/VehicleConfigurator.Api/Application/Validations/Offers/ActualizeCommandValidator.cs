﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Offers;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Offers
{
    public class ActualizeCommandValidator : AbstractValidator<ActualizeCommand>
    {
        public ActualizeCommandValidator()
        {
            RuleFor(c => c.OfferId).GreaterThanOrEqualTo(default(int));
        }
    }
}
