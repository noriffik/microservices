﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Offers;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Offers
{
    public class SetPrivateCustomerCommandValidator : AbstractValidator<SetPrivateCustomerCommand>
    {
        public SetPrivateCustomerCommandValidator()
        {
            RuleFor(c => c.OfferId).GreaterThanOrEqualTo(default(int));
            RuleFor(c => c.PrivateCustomerId).GreaterThanOrEqualTo(default(int));
        }
    }
}
