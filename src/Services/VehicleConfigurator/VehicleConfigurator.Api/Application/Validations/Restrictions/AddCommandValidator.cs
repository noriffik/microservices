﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Restrictions
{
    public class AddCommandValidator : AbstractValidator<AddCommand>
    {
        public AddCommandValidator()
        {
            RuleFor(c => c.CatalogId).GreaterThan(0);
            RuleFor(c => c.ModelKey).NotNull().SetValidator(new ModelKeyValidator());
            RuleFor(c => c.PrNumbers).NotNull().SetValidator(new PrNumberSetValidator());
            RuleFor(c => c.RelevancePeriod).NotNull().SetValidator(new RelevancePeriodModelValidator());
        }
    }
}
