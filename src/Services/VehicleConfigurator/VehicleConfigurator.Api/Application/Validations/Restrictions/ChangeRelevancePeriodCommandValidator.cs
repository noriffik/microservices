﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Restrictions
{
    public class ChangeRelevancePeriodCommandValidator : AbstractValidator<ChangeRelevancePeriodCommand>
    {
        public ChangeRelevancePeriodCommandValidator()
        {
            RuleFor(c => c.Id).GreaterThan(0);
            RuleFor(c => c.RelevancePeriod).NotNull().SetValidator(new RelevancePeriodModelValidator());
        }
    }
}
