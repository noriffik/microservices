﻿using FluentValidation;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Restrictions
{
    public class RelevancePeriodModelValidator : AbstractValidator<RelevancePeriodModel>
    {
        public RelevancePeriodModelValidator()
        {
            RuleFor(v => v).NotNull()
                .Must(v => PeriodValidator.FromRequired.IsValid(v.From, v.To))
                .WithMessage("Invalid period. ToDate must be null ar after FromDate.");
        }
    }
}
