﻿using FluentValidation;
using FluentValidation.Results;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Rules;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Catalogs
{
    public static class OptionValidationExtensions
    {
        public static UnavailableOptionValidationException Throw(UnavailableOptionException e, string propertyName, PrNumber attemptedValue)
        {
            throw new UnavailableOptionValidationException(propertyName, attemptedValue, e.Details);
        }

        public static UnavailableOptionValidationException Throw(UnavailableOptionException e, string propertyName)
        {
            var forPrNumber = e.Details.FirstOrDefault()?.ForPrNumber;
            var attemptedValue = forPrNumber != null ? PrNumber.Parse(forPrNumber) : null;

            throw new UnavailableOptionValidationException(propertyName, attemptedValue, e.Details);
        }
    }

    [Serializable]
    public class UnavailableOptionValidationException : ValidationException
    {
        private const string DefaultMessage = "Unacceptable option operation";
        
        public UnavailableOptionValidationException(string message, IEnumerable<OptionValidationFailure> errors) : base(message, errors.ToList())
        {
        }

        public UnavailableOptionValidationException(string propertyName, PrNumber attemptedValue, IEnumerable<RuleViolation> details) 
            : this(DefaultMessage, ToValidationFailure(propertyName, attemptedValue, details))
        {
        }

        private static IEnumerable<OptionValidationFailure> ToValidationFailure(string propertyName, PrNumber attemptedValue, IEnumerable<RuleViolation> details)
        {
            return details.Select(r => new OptionValidationFailure(propertyName, r, attemptedValue));
        }
    }
    
    [Serializable]
    public class OptionValidationFailure : ValidationFailure
    {
        private OptionValidationFailure() : base(string.Empty, null)
        {
        }

        public OptionValidationFailure(string propertyName, string errorMessage, object attemptedValue) 
            : base(propertyName, errorMessage, attemptedValue)
        {
        }

        public OptionValidationFailure(string propertyName, RuleViolation details, PrNumber attemptedValue) 
            : this(propertyName, details.Message, attemptedValue.Value)
        {
            FormattedMessagePlaceholderValues = new Dictionary<string, object> {{propertyName, details}};
        }
    }
}
