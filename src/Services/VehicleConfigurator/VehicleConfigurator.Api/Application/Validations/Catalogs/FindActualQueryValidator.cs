﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Catalogs
{
    public class FindActualQueryValidator : AbstractValidator<FindActualQuery>
    {
        public FindActualQueryValidator()
        {   
            RuleFor(c => c.Status).IsInEnum();
        }
    }
}
