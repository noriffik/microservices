﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Catalogs
{
    public class AddCommandValidator : AbstractValidator<AddCommand>
    {
        public AddCommandValidator()
        {
            RuleFor(c => c.RelevancePeriod)
                .NotEmpty()
                .SetValidator(new PeriodModelValidator());
            RuleFor(c => c.Year).MustBeYear();
        }
    }
}
