﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Catalogs
{
    public class ChangeStatusCommandValidator : AbstractValidator<ChangeStatusCommand>
    {
        public ChangeStatusCommandValidator()
        {
            RuleFor(c => c.Id).GreaterThan(default(int));
            RuleFor(c => c.Status).IsInEnum();
        }
    }
}
