﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Catalogs
{
    public class ChangeRelevancePeriodCommandValidator : AbstractValidator<ChangeRelevancePeriodCommand>
    {
        public ChangeRelevancePeriodCommandValidator()
        {
            RuleFor(c => c.CatalogId).GreaterThan(default(int));
            RuleFor(c => c.RelevancePeriod).NotEmpty()
                .NotNull().SetValidator(new PeriodModelValidator());
        }
    }
}
