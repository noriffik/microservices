﻿using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ColorTypes
{
    public class ColorTypeIdValidator : AbstractValidator<string>
    {
        public ColorTypeIdValidator()
        {
            RuleFor(v => v).NotEmpty().NotNull()
                .Must(v => ColorTypeId.TryParse(v, out _))
                .WithMessage("Invalid Format.");
        }
    }
}
