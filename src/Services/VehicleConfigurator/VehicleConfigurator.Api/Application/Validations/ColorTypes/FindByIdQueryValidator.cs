﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ColorTypes
{
    public class FindByIdQueryValidator : AbstractValidator<FindByIdQuery>
    {
        public FindByIdQueryValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new ColorTypeIdValidator());
        }
    }
}
