﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.ColorTypes;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ColorTypes
{
    public class AddCommandValidator : AbstractValidator<AddCommand>
    {
        public AddCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new ColorTypeIdValidator());
            RuleFor(c => c.Name).NotEmpty();
        }
    }
}
