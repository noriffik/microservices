﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Queries.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles
{
    public class VehicleDescriptionQueryValidator : AbstractValidator<VehicleDescriptionQuery>
    {
        public VehicleDescriptionQueryValidator()
        {
            RuleFor(c => c.ModelKey).NotNull().NotEmpty().SetValidator(new ModelKeyValidator());
            RuleFor(c => c.ColorId).NotNull().NotEmpty().SetValidator(new ColorIdValidator());
            RuleFor(c => c.PrNumberSet).NotNull().NotEmpty().SetValidator(new PrNumberSetValidator());
        }
    }
}
