﻿using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles
{
    public class BoundModelKeySetValidator : AbstractValidator<string>
    {
        public BoundModelKeySetValidator()
        {
            RuleFor(s => s).NotEmpty()
                .Must(s => ModelKeySet.TryParse(s, out _))
                .WithMessage("Invalid format.");

            RuleFor(s => s)
                .Must(s => BoundModelKeySet.TryParse(s, out _))
                .WithMessage("All model keys must be for the same model")
                .When(s => ModelKeySet.TryParse(s, out _) && !string.IsNullOrWhiteSpace(s));
        }
    }
}
