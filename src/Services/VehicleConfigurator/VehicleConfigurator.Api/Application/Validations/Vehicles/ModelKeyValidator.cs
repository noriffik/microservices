﻿
using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles
{
    public class ModelKeyValidator : AbstractValidator<string>
    {
        public ModelKeyValidator()
        {
            RuleFor(v => v).NotEmpty().NotNull()
                .Must(v => ModelKey.TryParse(v, out _))
                .WithMessage("Invalid format.");
        }
    }
}
