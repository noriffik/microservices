﻿using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles
{
    public class ModelKeySetValidator : AbstractValidator<string>
    {
        public ModelKeySetValidator()
        {
            RuleFor(s => s).NotNull()
                .Must(s => ModelKeySet.TryParse(s, out _))
                .WithMessage("Invalid format.");
        }
    }
}
