﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.OptionCategories
{
    public class UpdateCommandValidator : AbstractValidator<UpdateCommand>
    {
        public UpdateCommandValidator()
        {
            RuleFor(c => c.Id).GreaterThan(0);
            RuleFor(c => c.Name).NotEmpty();
        }
    }
}
