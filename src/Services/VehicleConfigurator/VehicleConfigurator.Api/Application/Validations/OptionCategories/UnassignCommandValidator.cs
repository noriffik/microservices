﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.OptionCategories
{
    public class UnassignCommandValidator : AbstractValidator<UnassignCommand>
    {
        public UnassignCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new OptionIdValidator());
        }
    }
}