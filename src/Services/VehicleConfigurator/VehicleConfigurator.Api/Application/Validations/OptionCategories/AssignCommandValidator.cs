﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.OptionCategories
{
    public class AssignCommandValidator : AbstractValidator<AssignCommand>
    {
        public AssignCommandValidator()
        {
            RuleFor(c => c.OptionId).NotEmpty().SetValidator(new OptionIdValidator());
            RuleFor(c => c.CategoryId).GreaterThan(0);
        }
    }
}
