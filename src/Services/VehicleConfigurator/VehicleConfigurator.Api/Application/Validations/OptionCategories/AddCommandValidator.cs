﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.OptionCategories
{
    public class AddCommandValidator : AbstractValidator<AddCommand>
    {
        public AddCommandValidator()
        {
            RuleFor(c => c.Name).NotEmpty();
        }
    }
}
