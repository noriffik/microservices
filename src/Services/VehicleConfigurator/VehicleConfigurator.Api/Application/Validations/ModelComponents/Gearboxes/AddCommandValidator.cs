﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Gearboxes;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Gearboxes
{
    public class AddCommandValidator : AbstractValidator<AddCommand>
    {
        public AddCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new GearboxIdValidator());
            RuleFor(v => v.Type).NotEmpty().SetValidator(new GearboxTypeValidator());
            RuleFor(v => v.GearBoxCategory).IsInEnum();
        }
    }
}
