﻿using FluentValidation;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Gearboxes
{
    public class FindByIdQueryValidator : AbstractValidator<FindByIdQuery<Gearbox, GearboxId>>
    {
        public FindByIdQueryValidator()
        {
            RuleFor(q => q.Id).NotEmpty().SetValidator(new GearboxIdValidator());
        }
    }
}
