﻿using FluentValidation;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using ModelIdValidator = NexCore.VehicleConfigurator.Api.Application.Validations.Components.Models.ModelIdValidator;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Gearboxes
{
    public class FindByModelIdQueryValidator : AbstractValidator<FindByModelIdQuery<Gearbox, GearboxId>>
    {
        public FindByModelIdQueryValidator()
        {
            RuleFor(q => q.ModelId).NotEmpty().SetValidator(new ModelIdValidator());
        }
    }
}
