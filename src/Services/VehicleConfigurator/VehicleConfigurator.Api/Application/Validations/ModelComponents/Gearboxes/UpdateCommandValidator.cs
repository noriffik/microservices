﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Gearboxes;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Gearboxes
{
    public class UpdateCommandValidator : AbstractValidator<UpdateCommand>
    {
        public UpdateCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new GearboxIdValidator());
            RuleFor(v => v.Type).NotEmpty().SetValidator(new GearboxTypeValidator());
            RuleFor(v => v.GearBoxCategory).IsInEnum();
        }
    }
}
