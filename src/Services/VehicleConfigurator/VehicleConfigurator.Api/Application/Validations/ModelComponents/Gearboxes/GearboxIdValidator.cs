﻿using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Gearboxes
{
    public class GearboxIdValidator: AbstractValidator<string>
    {
        public GearboxIdValidator()
        {
            RuleFor(v => v).NotEmpty().NotNull()
                .Must(v =>  GearboxId.TryParse(v, out _))
                .WithMessage("Invalid format.");
        }
    }
}
