﻿using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Gearboxes
{
    public class GearboxTypeValidator: AbstractValidator<string>
    {
        public GearboxTypeValidator()
        {
            RuleFor(v => v).NotEmpty().NotNull()
                .Must(v =>  GearboxType.TryParse(v, out _))
                .WithMessage("Invalid format.");
        }
    }
}
