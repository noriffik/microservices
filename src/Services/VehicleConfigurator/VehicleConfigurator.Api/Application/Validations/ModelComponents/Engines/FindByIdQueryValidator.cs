﻿using FluentValidation;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Engines
{
    public class FindByIdQueryValidator : AbstractValidator<FindByIdQuery<Engine, EngineId>>
    {
        public FindByIdQueryValidator()
        {
            RuleFor(q => q.Id).NotEmpty().SetValidator(new EngineIdValidator());
        }
    }
}
