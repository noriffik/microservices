﻿using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Engines
{
    public class EngineIdValidator : AbstractValidator<string>
    {
        public EngineIdValidator()
        {
            RuleFor(v => v).NotEmpty().NotNull()
                .Must(v => EngineId.TryParse(v, out _))
                .WithMessage("Invalid format.");
        }
    }
}
