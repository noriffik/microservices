﻿using FluentValidation;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Engines
{
    public class DeleteCommandValidator : AbstractValidator<DeleteCommand<Engine, EngineId>>
    {
        public DeleteCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new EngineIdValidator());
        }
    }
}
