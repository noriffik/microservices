﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Engines;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Engines
{
    public class UpdateCommandValidator : AbstractValidator<UpdateCommand>
    {
        public UpdateCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new EngineIdValidator());
            RuleFor(c => c.FuelType).IsInEnum();
        }
    }
}
