﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Bodies;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Bodies
{
    public class UpdateCommandValidator : AbstractValidator<UpdateCommand>
    {
        public UpdateCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new BodyIdValidator());
        }
    }
}
