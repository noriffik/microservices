﻿using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Bodies
{
    public class BodyIdValidator : AbstractValidator<string>
    {
        public BodyIdValidator()
        {
            RuleFor(v => v).NotEmpty().NotNull()
                .Must(v => BodyId.TryParse(v, out _))
                .WithMessage("Invalid format.");
        }
    }
}
