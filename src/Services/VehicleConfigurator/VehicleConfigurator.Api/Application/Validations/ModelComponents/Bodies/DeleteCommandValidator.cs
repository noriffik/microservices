﻿using FluentValidation;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Bodies
{
    public class DeleteCommandValidator : AbstractValidator<DeleteCommand<Body, BodyId>>
    {
        public DeleteCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new BodyIdValidator());
        }
    }
}
