﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Options;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options
{
    public class UpdateCommandValidator : AbstractValidator<UpdateCommand>
    {
        public UpdateCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new OptionIdValidator());
        }
    }
}
