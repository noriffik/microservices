﻿using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options
{
    public class PrNumberSetValidator : AbstractValidator<string>
    {
        public PrNumberSetValidator()
        {
            RuleFor(v => v).NotEmpty().NotNull()
                .Must(v => PrNumberSet.TryParse(v, out _))
                .WithMessage("Invalid format.");
        }
    }
}
