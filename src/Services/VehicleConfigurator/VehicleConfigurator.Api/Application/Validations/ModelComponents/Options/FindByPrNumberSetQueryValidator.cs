﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.Components.Models;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options
{
    public class FindByPrNumberSetQueryValidator : AbstractValidator<FindByPrNumberSetQuery>
    {
        public FindByPrNumberSetQueryValidator()
        {
            RuleFor(q => q.ModelId).NotEmpty().SetValidator(new ModelIdValidator());
            RuleFor(q => q.PrNumberSet).NotEmpty().SetValidator(new PrNumberSetValidator());
        }
    }
}
