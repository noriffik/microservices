﻿using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options
{
    public class OptionIdValidator : AbstractValidator<string>
    {
        public OptionIdValidator()
        {
            RuleFor(v => v).NotEmpty().NotNull()
                .Must(v => OptionId.TryParse(v, out _))
                .WithMessage("Invalid format.");
        }
    }
}
