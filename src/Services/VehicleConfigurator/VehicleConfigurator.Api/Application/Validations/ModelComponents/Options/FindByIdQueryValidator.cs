﻿using FluentValidation;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options
{
    public class FindByIdQueryValidator : AbstractValidator<FindByIdQuery<Option, OptionId>>
    {
        public FindByIdQueryValidator()
        {
            RuleFor(q => q.Id).NotEmpty().SetValidator(new OptionIdValidator());
        }
    }
}
