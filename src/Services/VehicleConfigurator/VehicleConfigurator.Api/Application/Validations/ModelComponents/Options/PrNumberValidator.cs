﻿using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options
{
    public class PrNumberValidator : AbstractValidator<string>
    {
        public PrNumberValidator()
        {
            RuleFor(v => v).NotEmpty().NotNull()
                .Must(v => PrNumber.TryParse(v, out _))
                .WithMessage("Invalid format.");
        }
    }
}
