﻿using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Equipments
{
    public class EquipmentIdValidator : AbstractValidator<string>
    {
        public EquipmentIdValidator()
        {
            RuleFor(v => v).NotEmpty().NotNull()
                .Must(v => EquipmentId.TryParse(v, out _))
                .WithMessage("Invalid format.");
        }
    }
}
