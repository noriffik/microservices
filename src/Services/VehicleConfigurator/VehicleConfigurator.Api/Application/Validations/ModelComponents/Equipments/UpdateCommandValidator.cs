﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Equipments;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Equipments
{
    public class UpdateCommandValidator : AbstractValidator<UpdateCommand>
    {
        public UpdateCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new EquipmentIdValidator());
        }
    }
}
