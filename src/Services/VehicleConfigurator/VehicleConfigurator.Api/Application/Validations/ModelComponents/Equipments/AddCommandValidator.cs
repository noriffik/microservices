﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Equipments;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Equipments
{
    public class AddCommandValidator : AbstractValidator<AddCommand>
    {
        public AddCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new EquipmentIdValidator());
        }
    }
}
