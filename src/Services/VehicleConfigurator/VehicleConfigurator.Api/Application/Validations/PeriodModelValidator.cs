﻿using FluentValidation;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Models;

namespace NexCore.VehicleConfigurator.Api.Application.Validations
{
    public class PeriodModelValidator : AbstractValidator<PeriodModel>
    {
        public PeriodModelValidator()
        {
            RuleFor(v => v).NotEmpty()
                .NotNull()
                .Must(v => PeriodValidator.FromRequired.IsValid(v.From, v.To))
                .WithMessage("Invalid period.");
        }
    }
}
