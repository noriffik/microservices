﻿using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Colors
{
    public class ColorIdValidator : AbstractValidator<string>
    {
        public ColorIdValidator()
        {
            RuleFor(v => v).NotEmpty().NotNull()
                .Must(v => ColorId.TryParse(v, out _))
                .WithMessage("Invalid Format");
        }
    }
}
