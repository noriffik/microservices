﻿using FluentValidation;
using System.Globalization;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Colors
{
    public class ArgbValidator : AbstractValidator<string>
    {
        public ArgbValidator()
        {
            RuleFor(v => v).Length(8)
                .Must(v => int.TryParse(v, NumberStyles.HexNumber, null, out _))
                .WithMessage("Invalid Format");
        }
    }
}
