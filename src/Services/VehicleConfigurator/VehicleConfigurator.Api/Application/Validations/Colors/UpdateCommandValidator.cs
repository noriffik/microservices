﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Colors;
using NexCore.VehicleConfigurator.Api.Application.Validations.ColorTypes;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Colors
{
    public class UpdateCommandValidator : AbstractValidator<UpdateCommand>
    {
        public UpdateCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new ColorIdValidator());
            RuleFor(c => c.TypeId).NotEmpty().SetValidator(new ColorTypeIdValidator());
            RuleFor(c => c.Argb).NotEmpty().SetValidator(new ArgbValidator());
            RuleFor(c => c.Name).NotEmpty();
        }
    }
}
