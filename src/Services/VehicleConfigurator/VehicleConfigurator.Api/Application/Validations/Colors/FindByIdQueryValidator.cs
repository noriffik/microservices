﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Colors
{
    public class FindByIdQueryValidator : AbstractValidator<FindByIdQuery>
    {
        public FindByIdQueryValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new ColorIdValidator());
        }
    }
}
