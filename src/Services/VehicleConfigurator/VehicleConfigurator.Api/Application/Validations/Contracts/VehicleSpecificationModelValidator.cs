﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.Packages;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Contracts
{
    public class VehicleSpecificationModelValidator: AbstractValidator<VehicleSpecificationModel>
    {
        public VehicleSpecificationModelValidator()
        {
            RuleFor(c => c.ModelYear).NotNull().NotEmpty().GreaterThan(default(int));
            RuleFor(c => c.ModelKey).NotNull().NotEmpty().SetValidator(new ModelKeyValidator());
            RuleFor(c => c.ColorId).NotNull().NotEmpty().SetValidator(new ColorIdValidator());
            RuleFor(c => c.AdditionalOptions).NotNull().NotEmpty().SetValidator(new PrNumberSetValidator());
            RuleFor(c => c.IncludedOptions).NotNull().NotEmpty().SetValidator(new PrNumberSetValidator());
            RuleFor(c => c.PackageId).NotNull().NotEmpty().SetValidator(new PackageIdValidator());
        }
    }
}
