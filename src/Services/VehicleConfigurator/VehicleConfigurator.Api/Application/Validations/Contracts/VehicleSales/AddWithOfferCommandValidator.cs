﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Contracts.VehicleSales;
using System.Linq;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Contracts.VehicleSales
{
    public class AddWithOfferCommandValidator : AbstractValidator<AddWithOfferCommand>
    {
        public AddWithOfferCommandValidator()
        {
            RuleFor(c => c.Number).NotNull().NotEmpty();
            RuleFor(c => c.ContractTypeId).NotNull().NotEmpty().SetValidator(new ContractTypeIdValidator());
            RuleFor(c => c.Payment).NotNull().Must(c => c == null || c.Count() > 1).WithMessage("At least two payments expected.");
            RuleForEach(c => c.Payment).NotNull().NotEmpty().SetValidator(new PriceModelValidator());
            RuleFor(c => c.OfferId).GreaterThan(default(int));
            RuleFor(c => c.VehicleStockAddress).NotNull().NotEmpty();
            RuleFor(c => c.VehicleDeliveryDays).GreaterThan(default(int));
            RuleFor(c => c.PrivateCustomerId).GreaterThan(default(int));
            RuleFor(c => c.SalesManagerId).GreaterThan(default(int));
            RuleFor(c => c.DealerId).NotNull().NotEmpty().SetValidator(new DealerIdValidator());
            RuleFor(c => c.DealersCity).NotNull().NotEmpty();
            RuleFor(c => c.DealersHeadPosition).NotNull().NotEmpty();
        }
    }
}
