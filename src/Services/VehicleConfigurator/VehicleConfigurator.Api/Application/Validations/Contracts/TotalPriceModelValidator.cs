﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Contracts
{
    public class TotalPriceModelValidator : AbstractValidator<TotalPriceModel>
    {
        public TotalPriceModelValidator()
        {
            RuleFor(c => c.WithoutTax).NotNull().NotEmpty().SetValidator(new PriceModelValidator());
            RuleFor(c => c.WithTax).NotNull().NotEmpty().SetValidator(new PriceModelValidator());
            RuleFor(c => c.Tax).NotNull().NotEmpty().SetValidator(new PriceModelValidator());
        }
    }
}
