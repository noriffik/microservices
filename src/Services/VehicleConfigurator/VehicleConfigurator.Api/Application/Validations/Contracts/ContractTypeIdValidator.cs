﻿using FluentValidation;
using NexCore.VehicleConfigurator.Domain.Contracts;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Contracts
{
    public class ContractTypeIdValidator : AbstractValidator<string>
    {
        public ContractTypeIdValidator()
        {
            RuleFor(v => v).NotEmpty().NotNull()
                .Must(v => ContractTypeId.TryParse(v, out _))
                .WithMessage("Invalid ContractTypeId format.");
        }
    }
}
