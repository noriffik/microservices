﻿using FluentValidation;
using NexCore.DealerNetwork;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Contracts
{
    public class DealerIdValidator : AbstractValidator<string>
    {
        public DealerIdValidator()
        {
            RuleFor(v => v).NotEmpty().NotNull()
                .Must(v => DealerId.TryParse(v, out _))
                .WithMessage("Invalid DealerId format.");
        }
    }
}
