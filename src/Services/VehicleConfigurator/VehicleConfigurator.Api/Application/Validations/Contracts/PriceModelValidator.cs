﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Contracts
{
    public class PriceModelValidator : AbstractValidator<PriceModel>
    {
        public PriceModelValidator()
        {
            RuleFor(v => v.Base).NotEmpty().NotNull()
                .WithMessage("Invalid base price format.");

            RuleFor(v => v.Retail).NotEmpty().NotNull()
                .WithMessage("Invalid retail price format.");
        }
    }
}
