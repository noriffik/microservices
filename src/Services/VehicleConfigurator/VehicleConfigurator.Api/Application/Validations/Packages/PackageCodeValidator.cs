﻿using FluentValidation;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Packages
{
    public class PackageCodeValidator : AbstractValidator<string>
    {
        public PackageCodeValidator()
        {
            RuleFor(p => p).NotEmpty().NotNull()
                .Must(p => PackageCode.TryParse(p, out _))
                .WithMessage("Invalid format.");
        }
    }
}
