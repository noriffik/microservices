﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Packages;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Packages
{
    public class ChangeStatusCommandValidator : AbstractValidator<ChangeStatusCommand>
    {
        public ChangeStatusCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new PackageIdValidator());
            RuleFor(c => c.Status).IsInEnum();
        }
    }
}
