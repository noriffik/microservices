﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Packages;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Packages
{
    public class UpdateCoverageCommandValidator : AbstractValidator<UpdateCoverageCommand>
    {
        public UpdateCoverageCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new PackageIdValidator());
            RuleFor(c => c.ModelKeys).NotEmpty().SetValidator(new BoundModelKeySetValidator());
        }
    }
}