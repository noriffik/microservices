﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Packages;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Packages
{
    public class AddCommandValidator : AbstractValidator<AddCommand>
    {
        public AddCommandValidator(IApplicabilityParser parser)
        {
            RuleFor(c => c.Id).NotNull().SetValidator(new PackageIdValidator());
            RuleFor(c => c.CatalogId).GreaterThan(0);
            RuleFor(c => c.Name).NotEmpty();
            RuleFor(c => c.Price).GreaterThanOrEqualTo(0);
            RuleFor(c => c.ModelKeys).NotEmpty().SetValidator(new BoundModelKeySetValidator());
            RuleFor(c => c.Applicability).NotEmpty().SetValidator(new PackageApplicabilitySpecificationValidator(parser));
        }
    }
}
