﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Packages;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Packages
{
    public class UpdatePriceCommandValidator : AbstractValidator<UpdatePriceCommand>
    {
        public UpdatePriceCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new PackageIdValidator());
            RuleFor(c => c.Price).GreaterThanOrEqualTo(0);
        }
    }
}
