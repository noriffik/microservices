﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.Packages;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Packages
{
    public class UpdateApplicabilityCommandValidator : AbstractValidator<UpdateApplicabilityCommand>
    {
        public UpdateApplicabilityCommandValidator(IApplicabilityParser parser)
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new PackageIdValidator());
            RuleFor(c => c.Applicability).NotEmpty().SetValidator(new PackageApplicabilitySpecificationValidator(parser));
        }
    }
}
