﻿using FluentValidation;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Packages
{
    public class PackageIdValidator : AbstractValidator<string>
    {
        public PackageIdValidator()
        {
            RuleFor(p => p).NotEmpty().NotNull()
                .Must(p => PackageId.TryParse(p, out _))
                .WithMessage("Invalid format.");
        }
    }
}
