﻿using FluentValidation;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Packages
{
    public class PackageApplicabilitySpecificationValidator : AbstractValidator<string>
    {
        public PackageApplicabilitySpecificationValidator(IApplicabilityParser parser)
        {
            RuleFor(s => s).NotNull().NotEmpty()
                .Must(s => parser.TryParse(s, out _))
                .WithMessage("InValidFormat");
        }
    }
}