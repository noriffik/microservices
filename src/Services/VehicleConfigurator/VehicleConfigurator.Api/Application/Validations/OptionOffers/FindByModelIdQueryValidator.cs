﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers.ByComponents;
using NexCore.VehicleConfigurator.Api.Application.Validations.Components.Models;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers
{
    public class FindByModelIdQueryValidator : AbstractValidator<FindByModelIdQuery>
    {
        public FindByModelIdQueryValidator()
        {
            RuleFor(q => q.CatalogId).GreaterThan(0);
            RuleFor(q => q.ModelId).NotEmpty().SetValidator(new ModelIdValidator());
            RuleFor(q => q.PrNumber).NotEmpty().SetValidator(new PrNumberValidator());
        }
    }
}
