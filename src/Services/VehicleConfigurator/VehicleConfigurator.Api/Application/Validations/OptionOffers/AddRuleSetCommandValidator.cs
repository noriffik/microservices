﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers
{
    public class AddRuleSetCommandValidator : AbstractValidator<AddRuleSetCommand>
    {
        public AddRuleSetCommandValidator()
        {
            RuleFor(c => c.VehicleOfferId).GreaterThan(0);
            RuleFor(c => c.PrNumber).NotEmpty().SetValidator(new PrNumberValidator());
            RuleFor(c => c.RuleSet).NotEmpty().SetValidator(new RuleSetModelValidator());
        }
    }
}
