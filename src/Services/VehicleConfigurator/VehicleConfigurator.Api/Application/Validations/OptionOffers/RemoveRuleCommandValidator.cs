﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers
{
    public class RemoveRuleCommandValidator: AbstractValidator<RemoveRuleCommand>
    {
        public RemoveRuleCommandValidator()
        {
            RuleFor(c => c.VehicleOfferId).GreaterThan(0);
            RuleFor(c => c.PrNumber).NotEmpty().SetValidator(new PrNumberValidator());
        }
    }
}
