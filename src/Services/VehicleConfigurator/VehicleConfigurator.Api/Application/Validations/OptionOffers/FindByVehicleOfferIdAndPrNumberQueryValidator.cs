﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers
{
    public class FindByVehicleOfferIdAndPrNumberQueryValidator : AbstractValidator<FindByVehicleOfferIdAndPrNumberQuery>
    {
        public FindByVehicleOfferIdAndPrNumberQueryValidator()
        {
            RuleFor(q => q.VehicleOfferId).GreaterThan(0);
            RuleFor(q => q.PrNumber).NotEmpty().SetValidator(new PrNumberValidator());
        }
    }
}
