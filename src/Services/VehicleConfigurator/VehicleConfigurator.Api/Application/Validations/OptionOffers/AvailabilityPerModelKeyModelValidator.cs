﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers
{
    public class AvailabilityPerModelKeyModelValidator : AbstractValidator<AvailabilityPerModelKeyModel>
    {
        public AvailabilityPerModelKeyModelValidator()
        {
            RuleFor(c => c).NotEmpty().NotNull();
            RuleFor(c => c.ModelKey).NotEmpty().SetValidator(new ModelKeyValidator());
            RuleFor(c => c.Availability).NotEmpty().SetValidator(new AvailabilityModelValidator());
        }
    }
}
