﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers
{
    public class RuleModelValidator : AbstractValidator<RuleModel>
    {
        public RuleModelValidator()
        {
            RuleFor(r => r.RuleName).NotEmpty().Matches(@"^[ZV][OA]$");
            RuleFor(r => r.PrNumbers).NotEmpty().SetValidator(new PrNumberSetValidator());
            RuleForEach(r => r.NestedRuleSet.RuleSet).NotEmpty().SetValidator(this).When(r => r.NestedRuleSet != null);
        }
    }
}
