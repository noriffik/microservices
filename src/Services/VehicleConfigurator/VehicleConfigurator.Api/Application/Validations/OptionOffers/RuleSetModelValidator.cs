﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers
{
    public class RuleSetModelValidator : AbstractValidator<RuleSetModel>
    {
        public RuleSetModelValidator()
        {
            RuleForEach(a => a.RuleSet).NotEmpty().SetValidator(new RuleModelValidator());
        }
    }
}
