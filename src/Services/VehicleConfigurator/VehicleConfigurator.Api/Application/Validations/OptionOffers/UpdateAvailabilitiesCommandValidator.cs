﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers
{
    public class UpdateAvailabilitiesCommandValidator : AbstractValidator<UpdateAvailabilitiesCommand>
    {
        public UpdateAvailabilitiesCommandValidator()
        {
            RuleFor(c => c.PrNumber).NotEmpty().SetValidator(new PrNumberValidator());
            RuleFor(c => c.Offers).NotEmpty();
            RuleForEach(c => c.Offers).NotEmpty().SetValidator(new AvailabilityPerModelKeyModelValidator());
        }
    }
}
