﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers
{
    public class AvailabilityModelValidator : AbstractValidator<AvailabilityModel>
    {
        public AvailabilityModelValidator()
        {
            RuleFor(a => a).NotEmpty().NotNull();
            RuleFor(c => c.Type).IsInEnum();
            RuleFor(c => c.Reason).IsInEnum();
            RuleFor(c => c.Price).GreaterThanOrEqualTo(0);
        }
    }
}
