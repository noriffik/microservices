﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers
{
    public class AddRuleSetForManyCommandValidator : AbstractValidator<AddRuleSetForManyCommand>
    {
        public AddRuleSetForManyCommandValidator()
        {
            RuleFor(c => c.CatalogId).GreaterThan(default(int));
            RuleFor(c => c.ModelKeySet).NotEmpty().SetValidator(new ModelKeySetValidator());
            RuleFor(c => c.OptionId).NotEmpty().SetValidator(new OptionIdValidator());
            RuleFor(c => c.RuleSet).NotEmpty().SetValidator(new RuleSetModelValidator());
        }
    }
}
