﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers
{
    public class ChangeRelevancePeriodCommandValidator : AbstractValidator<ChangeOptionOfferRelevancePeriodCommand>
    {
        public ChangeRelevancePeriodCommandValidator()
        {
            RuleFor(c => c.VehicleOfferId).GreaterThan(0);
            RuleFor(c => c.PrNumber).SetValidator(new PrNumberValidator());
            RuleFor(c => c.RelevancePeriod).NotNull().SetValidator(new PeriodModelValidator());
        }
    }
}
