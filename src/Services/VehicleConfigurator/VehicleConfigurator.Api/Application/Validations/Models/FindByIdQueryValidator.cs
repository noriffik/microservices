﻿using FluentValidation;
using NexCore.VehicleConfigurator.Api.Application.Queries.Models;
using NexCore.VehicleConfigurator.Api.Application.Validations.Components.Models;

namespace NexCore.VehicleConfigurator.Api.Application.Validations.Models
{
    public class FindByIdQueryValidator : AbstractValidator<FindByIdQuery>
    {
        public FindByIdQueryValidator()
        {
            RuleFor(c => c.Id).NotEmpty().SetValidator(new ModelIdValidator());
        }
    }
}
