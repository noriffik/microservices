﻿using NexCore.VehicleConfigurator.Api.Application.Models;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Restrictions
{
    [DataContract]
    public class RestrictionModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int CatalogId { get; set; }

        [DataMember]
        public string PrNumbers { get; set; }

        [DataMember]
        public string ModelKey { get; set; }

        [DataMember]
        public PeriodModel RelevancePeriod { get; set; }

        [DataMember]
        public bool IsEnabled { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}
