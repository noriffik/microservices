﻿using System.Runtime.Serialization;
using MediatR;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Restrictions
{
    [DataContract]
    public class FindByIdQuery : IRequest<RestrictionModel>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
