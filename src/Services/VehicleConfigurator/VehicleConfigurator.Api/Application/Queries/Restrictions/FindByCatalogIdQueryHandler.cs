﻿using AutoMapper;
using MediatR;
using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using NexCore.VehicleConfigurator.Domain.Restrictions.Specifications;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Restrictions
{
    public class FindByCatalogIdQueryHandler : IRequestHandler<FindByCatalogIdQuery, IEnumerable<RestrictionModel>>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public FindByCatalogIdQueryHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<RestrictionModel>> Handle(FindByCatalogIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var restrictions =
                await _work.EntityRepository.Find(new RestrictionByCatalogIdSpecification(request.CatalogId),
                    cancellationToken);

            return _mapper.Map<IEnumerable<RestrictionModel>>(restrictions);
        }
    }
}
