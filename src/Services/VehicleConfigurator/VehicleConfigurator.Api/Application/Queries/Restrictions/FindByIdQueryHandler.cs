﻿using AutoMapper;
using MediatR;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Restrictions
{
    public class FindByIdQueryHandler : IRequestHandler<FindByIdQuery, RestrictionModel>
    {
        private readonly VehicleConfiguratorContext _context;
        private readonly IMapper _mapper;
        public FindByIdQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<RestrictionModel> Handle(FindByIdQuery request, CancellationToken cancellationToken)
        {
            var restriction = await _context.EntityRepository.SingleOrDefault<Restriction>(request.Id, cancellationToken);

            return _mapper.Map<RestrictionModel>(restriction);
        }
    }
}
