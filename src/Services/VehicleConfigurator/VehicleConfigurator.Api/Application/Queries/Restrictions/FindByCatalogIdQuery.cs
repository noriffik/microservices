﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Restrictions
{
    [DataContract]
    public class FindByCatalogIdQuery : IRequest<IEnumerable<RestrictionModel>>
    {
        [DataMember]
        public int CatalogId { get; set; }
    }
}
