﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    public class ByIdQueryHandler : IRequestHandler<ByIdQuery, QueryResponse>
    {
        private readonly VehicleConfiguratorContext _context;
        private readonly IMapper _mapper;

        public ByIdQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<QueryResponse> Handle(ByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var configurations = from configuration in _context.Configurations
                where configuration.Id == request.Id
                select configuration;
            
            var record = await configurations.JoinRelated(_context)
                .SingleOrDefaultAsync(cancellationToken);

            return _mapper.Map<QueryResponse>(record);
        }
    }
}
