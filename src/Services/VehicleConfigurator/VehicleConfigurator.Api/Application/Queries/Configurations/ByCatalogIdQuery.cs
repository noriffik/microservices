﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class ByCatalogIdQuery : IRequest<IEnumerable<QueryResponse>>
    {
        [DataMember]
        public int CatalogId { get; set; }
    }
}
