﻿using NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories;
using NexCore.VehicleConfigurator.Api.Application.Queries.Restrictions;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class AvailableOptionModel
    {
        [DataMember]
        public OptionModel Option { get; set; }

        [DataMember]
        public OptionOfferModel Offer { get; set; }

        [DataMember]
        public OptionCategoryModel Category { get; set; }

        [DataMember]
        public bool IsIncluded { get; set; }

        [DataMember]
        public RestrictionPeriodModel RestrictionPeriod { get; set; }
    }
}
