﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class BySpecificationQuery : IRequest<DetailedQueryResponse>
    {
        [DataMember]
        public int ModelYear { get; set; }

        [DataMember]
        public string ModelKey { get; set; }

        [DataMember]
        public string PrNumbers { get; set; }
    }
}
