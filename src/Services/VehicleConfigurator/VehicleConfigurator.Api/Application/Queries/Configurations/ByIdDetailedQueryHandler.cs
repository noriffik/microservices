﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Common;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.Domain.Pricing;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    public class ByIdDetailedQueryHandler : IRequestHandler<ByIdDetailedQuery, DetailedQueryResponse>
    {
        private readonly ByIdQueryHandler _inner;
        private readonly VehicleConfiguratorContext _context;
        private readonly IMapper _mapper;
        private readonly IConfigurationPricingService _configurationPricingService;
        private readonly IPackagePricingService _packagePricingService;
        private readonly IPricingService _pricingService;

        public ByIdDetailedQueryHandler(
            VehicleConfiguratorContext context,
            IMapper mapper,
            IConfigurationPricingService configurationPricingService,
            IPackagePricingService packagePricingService,
            IPricingService pricingService)
        {
            _inner = new ByIdQueryHandler(context, mapper);
            _context = context;
            _mapper = mapper;
            _configurationPricingService = configurationPricingService ?? throw new ArgumentNullException(nameof(configurationPricingService));
            _packagePricingService = packagePricingService;
            _pricingService = pricingService ?? throw new ArgumentNullException(nameof(pricingService));
        }

        public async Task<DetailedQueryResponse> Handle(ByIdDetailedQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var innerResponse = await QueryConfiguration(request, cancellationToken);
            if (innerResponse == null)
                return null;

            var response = await QueryOptions(innerResponse);

            response.Packages = await GetPackageModels(innerResponse, cancellationToken);

            response.VehicleOffer.Price.Retail = await _pricingService.CalculateRetail(response.VehicleOffer.Price.Base,
                SystemTimeProvider.Instance.Get());

            var prices = await _configurationPricingService.Calculate(request.Id, SystemTimeProvider.Instance.Get());

            await FillColors(response, cancellationToken);

            return _mapper.Map(prices, response);
        }

        private Task<QueryResponse> QueryConfiguration(ByIdDetailedQuery request, CancellationToken cancellationToken)
        {
            return _inner.Handle(new ByIdQuery { Id = request.Id }, cancellationToken);
        }

        private async Task<DetailedQueryResponse> QueryOptions(QueryResponse innerResponse)
        {
            var response = _mapper.Map<QueryResponse, DetailedQueryResponse>(innerResponse);

            response.Options = _mapper.Map<IEnumerable<AvailableOptionModel>>(LoadOptions(innerResponse));

            return await CalculateOptionRetailPrices(response);
        }

        private async Task<DetailedQueryResponse> CalculateOptionRetailPrices(DetailedQueryResponse response)
        {
            var availabilities = response.Options.Where(o => o.Offer != null)
                .Select(o => o.Offer.Availability)
                .Where(o => o.Type == AvailabilityType.Purchasable);

            foreach (var availability in availabilities)
            {
                var retailPrice = await _pricingService.CalculateRetail(
                    availability.Price.Base, SystemTimeProvider.Instance.Get());

                availability.Price = new PriceModel
                {
                    Base = availability.Price.Base,
                    Retail = retailPrice
                };
            }

            return response;
        }

        private IQueryable<dynamic> LoadOptions(QueryResponse response)
        {
            var restrictions = (from restriction in _context.Restrictions
                               where restriction.IsEnabled && restriction.CatalogId == response.VehicleOffer.CatalogId 
                                                           && restriction.ModelKey == response.VehicleOffer.ModelKey
                               select restriction).ToList();

            return (from option in _context.Options
                    where Convert.ToString(option.ModelId) == response.Vehicle.Model.Id
                    join configuration in _context.Configurations.Select(c => new { c.Id, c.Options }) on response.Configuration.Id
                        equals configuration.Id
                    join vehicleOffers in _context.VehicleOffers on response.VehicleOffer.Id
                        equals vehicleOffers.Id
                    join optionOffers in _context.OptionOffers on new { VehicleOfferId = vehicleOffers.Id, OptionId = option.Id }
                        equals new { VehicleOfferId = EF.Property<int?>(optionOffers, "VehicleOfferId").Value, optionOffers.OptionId } into offerJoin
                    from optionOffer in offerJoin.DefaultIfEmpty()
                    join optionCategories in _context.OptionCategories on option.OptionCategoryId
                        equals optionCategories.Id into categoryJoin
                    from optionCategory in categoryJoin.DefaultIfEmpty()
                    select new
                    {
                        Option = option,
                        Offer = optionOffer,
                        Category = optionCategory,
                        IsIncluded = IsIncluded(configuration.Options, optionOffer),
                        RestrictionPeriod = optionOffer != null ? restrictions.ApplicableTo(vehicleOffers.ModelKey, optionOffer.PrNumber).CompositePeriod() : null
                    });
        }
        private bool IsIncluded(PrNumberSet included, OptionOffer optionOffer)
        {
            if (optionOffer == null)
                return false;

            return optionOffer.Availability.Type == AvailabilityType.Included
                   || included.Has(optionOffer.PrNumber);
        }

        private async Task<IEnumerable<PackageModel>> GetPackageModels(QueryResponse response, CancellationToken cancellationToken)
        {
            var configuration = await _context.Configurations.FindAsync(response.Configuration.Id);
            if (configuration == null)
                return null;

            var vehicleOffer = await _context.VehicleOffers.FindAsync(response.VehicleOffer.Id);
            if (vehicleOffer == null)
                return null;

            var packages = await _context.Packages
                .Apply(new CompatiblePackageSpecification(configuration))
                .ToListAsync(cancellationToken);

            var packageModels = new List<PackageModel>(packages.Count);
            foreach (var package in packages)
            {
                var packageModel = await GetPackageModel(response, package, cancellationToken);

                packageModel.Price = _mapper.Map<PackagePriceModel>(
                    await _packagePricingService.Calculate(vehicleOffer.Options, package, SystemTimeProvider.Instance.Get()));

                packageModels.Add(packageModel);
            }

            return packageModels;
        }

        private async Task<PackageModel> GetPackageModel(QueryResponse response, Package package, CancellationToken cancellationToken)
        {
            var specificationPrNumbers = package.Applicability.SelectMany(s => s.PrNumbers.Values);
            var optionIdValues = OptionId.Next(package.ModelKeySet.ModelId, specificationPrNumbers)
                .Select(v => v.Value);

            var options = from option in _context.Options
                          where optionIdValues.Contains(Convert.ToString(option.Id))
                          join categories in _context.OptionCategories on option.OptionCategoryId
                              equals categories.Id into categoryJoin
                          from category in categoryJoin.DefaultIfEmpty()
                          select new
                          {
                              Category = category,
                              Option = option
                          };

            var packageModel = new PackageModel
            {
                Id = package.Id.Value,
                Name = package.Name,
                Applicability = package.Applicability
                    .Select(s => new ApplicabilitySpecificationModel
                    {
                        Type = s.Type,
                        Options = options.Where(o => s.PrNumbers.Values.Contains(o.Option.PrNumber))
                            .ToList()
                            .Select(_mapper.Map<ExtendedByCategoryOptionModel>)
                    }).ToList()
            };

            if (response.Configuration.PackageId == packageModel.Id)
            {
                packageModel.Inclusion = PackageInclusion.Included;

                return packageModel;
            }

            var prNumbers = await (from configuration in _context.Configurations
                                   where configuration.Id == response.Configuration.Id
                                   select configuration.Options.AsString).SingleOrDefaultAsync(cancellationToken);

            packageModel.Inclusion = package.IsApplicableTo(PrNumberSet.Parse(prNumbers))
                ? PackageInclusion.Available
                : PackageInclusion.Unavailable;

            return packageModel;
        }

        private async Task FillColors(DetailedQueryResponse response, CancellationToken cancellationToken)
        {
            var query = from colorOffer in _context.ColorOffers
                        where EF.Property<int?>(colorOffer, "VehicleOfferId") == response.VehicleOffer.Id
                        join color in _context.Colors on colorOffer.ColorId equals color.Id
                        join type in (_context.ColorTypes) on color.TypeId equals type.Id
                        select new ColorResponseRecord
                        {
                            ColorOffer = colorOffer,
                            Color = color,
                            Type = type
                        };
            var colors = await query.ToListAsync(cancellationToken);

            response.AvailableColors = _mapper.Map<IEnumerable<ColorResponseModel>>(colors).ToList();
            await CalculateColorRetailPrices(response.AvailableColors);

            response.Color = response.AvailableColors.Single(c => c.Color.Id == response.Configuration.ColorId);
        }

        private async Task CalculateColorRetailPrices(IEnumerable<ColorResponseModel> colorModels)
        {
            var colorOffers = colorModels.Select(c => c.ColorOffer);

            foreach (var colorOffer in colorOffers)
            {
                var retailPrice = await _pricingService.CalculateRetail(
                    colorOffer.Price.Base, SystemTimeProvider.Instance.Get());

                colorOffer.Price.Retail = retailPrice;
            }
        }
        
    }
}