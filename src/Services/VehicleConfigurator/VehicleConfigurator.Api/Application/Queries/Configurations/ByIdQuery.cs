﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class ByIdQuery : IRequest<QueryResponse>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
