﻿using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class PackageModel
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public IEnumerable<ApplicabilitySpecificationModel> Applicability { get; set; }

        [DataMember]
        public PackageInclusion Inclusion { get; set; }

        [DataMember]
        public PackagePriceModel Price { get; set; }
    }

    public enum PackageInclusion
    {
        Unavailable = 0,
        Available = 1,
        Included = 2,
    }
}
