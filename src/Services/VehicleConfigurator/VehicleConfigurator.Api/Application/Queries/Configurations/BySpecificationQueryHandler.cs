﻿using MediatR;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Validations.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    public class BySpecificationQueryHandler : IRequestHandler<BySpecificationQuery, DetailedQueryResponse>
    {
        private readonly IMediator _mediator;
        private readonly IUnitOfWork _work;
        private readonly IConfigurationService _configurationService;

        public BySpecificationQueryHandler(IMediator mediator, IUnitOfWork work, IConfigurationService configurationService)
        {
            _mediator = mediator;
            _work = work;
            _configurationService = configurationService;
        }

        public async Task<DetailedQueryResponse> Handle(BySpecificationQuery request, CancellationToken cancellationToken)
        {
            var modelYear = Year.From(request.ModelYear);
            var specification = new ConfigurationSpecification(ModelKey.Parse(request.ModelKey),
                PrNumberSet.Parse(request.PrNumbers ?? string.Empty));

            Configuration configuration = null;
            try
            {
                configuration = await Create(modelYear, specification);
            }
            catch (UnavailableOptionException e)
            {
                OptionValidationExtensions.Throw(e, nameof(request.PrNumbers));
            }

            var response = await Query(configuration, cancellationToken);

            await Delete(configuration);

            return response;
        }

        private async Task Delete(Configuration configuration)
        {
            await _configurationService.Remove(configuration.Id);

            await _work.Commit();
        }

        private async Task<DetailedQueryResponse> Query(Configuration configuration, CancellationToken cancellationToken)
        {
            return await _mediator.Send(new ByIdDetailedQuery { Id = configuration.Id }, cancellationToken);
        }

        private async Task<Configuration> Create(Year modelYear, ConfigurationSpecification specification)
        {
            var configuration = await _configurationService.Build(modelYear, specification);

            await _work.Commit();

            return configuration;
        }
    }
}
