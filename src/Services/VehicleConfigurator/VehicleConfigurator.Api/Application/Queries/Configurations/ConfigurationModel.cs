﻿using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class ConfigurationModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string ModelKey { get; set; }

        [DataMember]
        public ConfigurationStatus Status { get; set; }

        [DataMember]
        public DateTime CreatedAt { get; set; }

        [DataMember]
        public DateTime UpdatedAt { get; set; }

        [DataMember]
        public string PackageId { get; set; }

        [DataMember]
        public string ColorId { get; set; }

        [DataMember]
        public Relevance Relevance { get; set; }

        [DataMember]
        public bool IsRestricted { get; set; }
    }
}