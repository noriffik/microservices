﻿using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class DetailedQueryResponse : QueryResponse
    {
        [DataMember]
        public IEnumerable<AvailableOptionModel> Options { get; set; }

        [DataMember]
        public IEnumerable<PackageModel> Packages { get; set; }

        [DataMember]
        public ConfigurationPricingModel Prices { get; set; }

        [DataMember]
        public ColorResponseModel Color { get; set; }

        [DataMember]
        public IEnumerable<ColorResponseModel> AvailableColors { get; set; }
    }
}
