﻿using System;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.Infrastructure.Views;
using System.Linq;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    public static class QueryExtensions
    {
        public static IQueryable<object> JoinRelated(this IQueryable<Configuration> source, VehicleConfiguratorContext context)
        {
            return from record in from configuration in source
                    join vehicleOffer in context.VehicleOffers on new
                            { configuration.ModelKey, configuration.CatalogId }
                        equals new { vehicleOffer.ModelKey, vehicleOffer.CatalogId }
                    join vehicle in context.Query<OfferedVehicleView>()
                        on new { ModelKey = Convert.ToString(vehicleOffer.ModelKey), configuration.CatalogId }
                        equals new { vehicle.ModelKey, vehicle.CatalogId }
                    select new
                    {
                        configuration,
                        vehicleOffer,
                        vehicle
                    }
                group record by record.configuration.Id
                into distinct
                select distinct.FirstOrDefault();
        }
    }
}
