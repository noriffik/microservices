﻿using NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    public class ExtendedByCategoryOptionModel
    {
        [DataMember]
        public OptionModel Option { get; set; }

        [DataMember]
        public OptionCategoryModel Category { get; set; }
    }
}
