﻿using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class VehicleModel
    {
        [DataContract]
        public class ComponentModel
        {
            [DataMember]
            public string Id { get; set; }

            [DataMember]
            public string Name { get; set; }
        }

        [DataContract]
        public class ModelModel : ComponentModel
        {
        }

        [DataContract]
        public class BodyModel : ComponentModel
        {
        }

        [DataContract]
        public class EquipmentModel : ComponentModel
        {
        }

        [DataContract]
        public class EngineModel : ComponentModel
        {
            [DataMember]
            public FuelType? FuelType { get; set; }
        }

        [DataContract]
        public class GearboxModel : ComponentModel
        {
            [DataMember]
            public string Type { get; set; }

            [DataMember]
            public GearBoxCategory? GearBoxCategory { get; set; }
        }

        [DataMember]
        public ModelModel Model { get; set; }

        [DataMember]
        public BodyModel Body { get; set; }

        [DataMember]
        public EquipmentModel Equipment { get; set; }

        [DataMember]
        public EngineModel Engine { get; set; }

        [DataMember]
        public GearboxModel Gearbox { get; set; }
    }
}