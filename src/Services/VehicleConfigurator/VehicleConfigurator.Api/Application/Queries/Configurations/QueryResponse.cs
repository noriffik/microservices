﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class QueryResponse
    {
        [DataMember]
        public ConfigurationModel Configuration { get; set; }

        [DataMember]
        public VehicleOfferModel VehicleOffer { get; set; }

        [DataMember]
        public VehicleModel Vehicle { get; set; }
    }
}