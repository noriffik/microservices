﻿using MediatR;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    public class UnboundQuery : IRequest<IEnumerable<QueryResponse>>
    {
    }
}
