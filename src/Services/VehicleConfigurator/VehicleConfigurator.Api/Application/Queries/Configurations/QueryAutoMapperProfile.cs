﻿using AutoMapper;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    public class QueryAutoMapperProfile : Profile
    {
        public QueryAutoMapperProfile()
        {
            CreateMap<Option, OptionModel>();
            CreateMap<OptionOffer, VehicleOfferModel>();
            CreateMap<OptionCategory, OptionCategoryModel>();
            CreateMap<Availability, AvailabilityModel>()
                .ForMember(d => d.Price, s => s.MapFrom(src => new PriceModel { Base = src.Price }));

            CreateMap<QueryResponse, DetailedQueryResponse>()
                .ForMember(d => d.Options, o => o.AllowNull());

            CreateMap<VehicleOffer, VehicleOfferModel>()
                .ForMember(d => d.Price, s => s.MapFrom(src => new PriceModel { Base = src.Price }));

            CreateMap<ColorType, ColorTypeModel>();
            CreateMap<Color, ColorModel>();
            CreateMap<ColorOffer, ColorOfferModel>()
                .ForMember(d => d.Price, s => s.MapFrom(o => new PriceModel { Base = o.Price }));
            CreateMap<ColorResponseRecord, ColorResponseModel>();
        }
    }
}
