﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class ByIdDetailedQuery : IRequest<DetailedQueryResponse>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
