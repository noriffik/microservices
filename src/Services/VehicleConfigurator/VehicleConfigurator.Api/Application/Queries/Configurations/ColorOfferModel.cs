﻿using NexCore.VehicleConfigurator.Api.Application.Models.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class ColorOfferModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string ColorId { get; set; }

        [DataMember]
        public PriceModel Price { get; set; }

        [DataMember]
        public Relevance Relevance { get; set; }

        [DataMember]
        public RelevancePeriodModel RelevancePeriod { get; set; }
    }
}
