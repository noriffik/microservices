﻿using System;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing
{
    [DataContract]
    public class ConfigurationPricingModel
    {
        [DataMember]
        public DateTime OnDate { get; set; }

        [DataMember]
        public PriceModel Vehicle { get; set; }

        [DataMember]
        public PriceModel Color { get; set; }

        [DataMember]
        public PackageSavingsModel PackageSavings { get; set; }

        [DataMember]
        public OptionsPriceModel Options { get; set; }

        [DataMember]
        public PackagePriceModel Package { get; set; }

        [DataMember]
        public PriceModel Total { get; set; }
    }
}
