﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing
{
    [DataContract]
    public class CalculationOptionModel
    {
        [DataMember]
        public string PrNumber { get; set; }

        [DataMember]
        public PriceModel Price { get; set; }
    }
}