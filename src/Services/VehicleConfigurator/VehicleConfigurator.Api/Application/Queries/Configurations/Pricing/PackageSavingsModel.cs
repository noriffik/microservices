﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing
{
    [DataContract]
    public class PackageSavingsModel
    {
        [DataMember]
        public PriceModel WithOut { get; set; }

        [DataMember]
        public PriceModel With { get; set; }

        [DataMember]
        public PriceModel Savings { get; set; }
    }
}