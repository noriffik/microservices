﻿using AutoMapper;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Pricing;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing
{
    public class CalculationOptionModelConverter : ITypeConverter<IReadOnlyDictionary<PrNumber, Price>, IEnumerable<CalculationOptionModel>>
    {
        public IEnumerable<CalculationOptionModel> Convert(
            IReadOnlyDictionary<PrNumber, Price> source, IEnumerable<CalculationOptionModel> destination, ResolutionContext context)
        {
            var mapped = new List<CalculationOptionModel>();

            source.ToList().ForEach(
                o => mapped.Add(new CalculationOptionModel
                {
                    PrNumber = o.Key.Value,
                    Price = context.Mapper.Map<PriceModel>(o.Value)
                }));

            return mapped;
        }
    }
}