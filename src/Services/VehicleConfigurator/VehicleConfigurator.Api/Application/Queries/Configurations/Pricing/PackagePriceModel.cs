﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing
{
    [DataContract]
    public class PackagePriceModel
    {
        [DataMember]
        public IEnumerable<CalculationOptionModel> Options { get; set; }

        [DataMember]
        public PriceModel Price { get; set; }

        [DataMember]
        public PriceModel Savings { get; set; }
    }
}