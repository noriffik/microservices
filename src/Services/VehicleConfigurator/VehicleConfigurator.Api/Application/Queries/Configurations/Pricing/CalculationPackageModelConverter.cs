﻿using AutoMapper;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing
{
    public class CalculationPackageModelConverter : ITypeConverter<PackagePrice, PackagePriceModel>
    {
        public PackagePriceModel Convert(
            PackagePrice source, PackagePriceModel destination, ResolutionContext context)
        {
            return new PackagePriceModel
            {
                Price = context.Mapper.Map<PriceModel>(source.Price),
                Options = context.Mapper.Map<IEnumerable<CalculationOptionModel>>(source.Options.PerOption),
                Savings = context.Mapper.Map<PriceModel>(source.Savings)
            };
        }
    }
}