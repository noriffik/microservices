﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing
{
    [DataContract]
    public class OptionsPriceModel
    {
        [DataMember]
        public IEnumerable<CalculationOptionModel> PerOption { get; set; }

        [DataMember]
        public PriceModel Total { get; set; }
    }
}