﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing
{
    [DataContract]
    public class PriceModel
    {
        [DataMember]
        public decimal Base { get; set; }

        [DataMember]
        public decimal Retail { get; set; }
    }
}