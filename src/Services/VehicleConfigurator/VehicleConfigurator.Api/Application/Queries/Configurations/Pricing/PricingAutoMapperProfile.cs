﻿using AutoMapper;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Pricing;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing
{
    public class PricingAutoMapperProfile : Profile
    {
        public PricingAutoMapperProfile()
        {
            CreateMap<IReadOnlyDictionary<PrNumber, Price>, IEnumerable<CalculationOptionModel>>()
                .ConvertUsing<CalculationOptionModelConverter>();

            CreateMap<PackagePrice, PackagePriceModel>()
                .ConvertUsing<CalculationPackageModelConverter>();

            CreateMap<ConfigurationPrice, DetailedQueryResponse>()
                .ConvertUsing<DetailedQueryResponseConverter>();
        }
    }
}
