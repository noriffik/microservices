﻿using AutoMapper;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing
{
    public class DetailedQueryResponseConverter : ITypeConverter<ConfigurationPrice, DetailedQueryResponse>
    {
        public DetailedQueryResponse Convert(ConfigurationPrice source, DetailedQueryResponse destination, ResolutionContext context)
        {
            if (destination == null)
                destination = new DetailedQueryResponse();

            destination.Prices = context.Mapper.Map<ConfigurationPricingModel>(source);

            return destination;
        }
    }
}
