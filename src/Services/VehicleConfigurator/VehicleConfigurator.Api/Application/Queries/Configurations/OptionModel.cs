﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class OptionModel
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string ModelId { get; set; }

        [DataMember]
        public string PrNumber { get; set; }
    }
}
