﻿using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class AvailabilityModel
    {
        [DataMember]
        public AvailabilityType Type { get; set; }

        [DataMember]
        public ReasonType? Reason { get; set; }

        [DataMember]
        public PriceModel Price { get; set; }
    }
}
