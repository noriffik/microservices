﻿using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    public class ColorResponseRecord
    {
        public ColorOffer ColorOffer { get; set; }

        public Color Color { get; set; }

        public ColorType Type { get; set; }
    }
}
