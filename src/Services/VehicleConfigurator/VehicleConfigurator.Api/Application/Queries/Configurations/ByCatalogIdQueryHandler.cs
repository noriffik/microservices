﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    public class ByCatalogIdQueryHandler : IRequestHandler<ByCatalogIdQuery, IEnumerable<QueryResponse>>
    {
        private readonly VehicleConfiguratorContext _context;
        private readonly IMapper _mapper;

        public ByCatalogIdQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<QueryResponse>> Handle(ByCatalogIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            var configurations = _context.Configurations
                .Apply(new BasedOnCatalogSpecification(request.CatalogId));

            var records = await configurations.JoinRelated(_context)
                .ToListAsync(cancellationToken);

            return _mapper.Map<IEnumerable<QueryResponse>>(records);
        }
    }
}