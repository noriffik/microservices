﻿using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class VehicleOfferModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string ModelKey { get; set; }

        [DataMember]
        public PriceModel Price { get; set; }

        [DataMember]
        public int CatalogId { get; set; }
    }
}