﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class ColorModel
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Argb { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string TypeId { get; set; }
    }
}
