﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Configurations
{
    [DataContract]
    public class ColorResponseModel
    {
        [DataMember]
        public ColorOfferModel ColorOffer { get; set; }

        [DataMember]
        public ColorModel Color { get; set; }

        [DataMember]
        public ColorTypeModel Type { get; set; }
    }
}
