﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Extensions;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Vehicles
{
    public class VehicleDescriptionQueryHandler : IRequestHandler<VehicleDescriptionQuery, VehicleDescriptionModel>
    {
        private readonly VehicleConfiguratorContext _context;
        private readonly IMapper _mapper;

        public VehicleDescriptionQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<VehicleDescriptionModel> Handle(VehicleDescriptionQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var modelKey = ModelKey.Parse(request.ModelKey);
            
            var options = await FindOptions(modelKey.ModelId, PrNumberSet.Parse(request.PrNumberSet), cancellationToken);
            var color = await FindColor(ColorId.Parse(request.ColorId), cancellationToken);
            var vehicle = await DescribeVehicle(modelKey, options, color, cancellationToken);

            return _mapper.Map<VehicleDescriptionModel>(vehicle);
        }

        private async Task<VehicleDescriptionModelRecord> DescribeVehicle(ModelKey modelKey, List<OptionModelRecord> options, ColorModelRecord color, CancellationToken cancellationToken)
        {
            var query = from model in _context.Models.Where(m => m.Id == modelKey.ModelId)
                join body in _context.Bodies.Where(b => b.Id == modelKey.BodyId)
                    on modelKey.ModelId equals body.ModelId
                join equipment in _context.Equipments.Where(e => e.Id == modelKey.EquipmentId)
                    on modelKey.ModelId equals equipment.ModelId
                join engine in _context.Engines.Where(e => e.Id == modelKey.EngineId)
                    on modelKey.ModelId equals engine.ModelId
                join gearbox in _context.Gearboxes.Where(g => g.Id == modelKey.GearboxId)
                    on modelKey.ModelId equals gearbox.ModelId
                select new VehicleDescriptionModelRecord
                {
                    Model = model,
                    Body = body,
                    Equipment = equipment,
                    Engine = engine,
                    Gearbox = gearbox,
                    Options = options,
                    Color = color
                };

            var vehicle = await query.SingleOrDefaultAsync(cancellationToken);
            if (vehicle != null)
                return vehicle;

            throw ValidationExtensions.ExceptionForCommandHandler(nameof(VehicleDescriptionQuery.ModelKey),
                "Components not found for given ModelKey.", modelKey.Value);
        }

        private async Task<ColorModelRecord> FindColor(ColorId colorId, CancellationToken cancellationToken)
        {
            var query = from color in _context.Colors
                    .Where(r => Convert.ToString(r.Id) == colorId.Value)
                join colorType in _context.ColorTypes on color.TypeId equals colorType.Id
                select new ColorModelRecord
                {
                    Color = color,
                    Type = colorType
                };

            var record = await query.SingleOrDefaultAsync(cancellationToken);

            if (record != null)
                return record;
            
            throw ValidationExtensions.ExceptionForCommandHandler(nameof(VehicleDescriptionQuery.ColorId),
                "Color not found.", colorId.Value);
        }

        private async Task<List<OptionModelRecord>> FindOptions(ModelId modelId, PrNumberSet prNumberSet,
            CancellationToken cancellationToken)
        {
            var query = _context.Options
                .Where(o => Convert.ToString(o.ModelId) == modelId)
                .Where(o => prNumberSet.Has(o.PrNumber))
                .LeftJoin(_context.OptionCategories,
                option => option.OptionCategoryId,
                category => category.Id,
                (join, category) => new OptionModelRecord
                {
                    Category = category,
                    Option = join.Outer
                });

            var records = await query.ToListAsync(cancellationToken);

            if (records.Count == prNumberSet.Values.Count())
                return records;

            throw ValidationExtensions.ExceptionForCommandHandler(nameof(VehicleDescriptionQuery.PrNumberSet), 
                "Options not found.", prNumberSet.AsString);
        }
    }
}
