﻿using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Engines;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Gearboxes;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Api.Application.Queries.Models;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Vehicles
{
    [DataContract]
    public class VehicleDescriptionModel
    {
        [DataMember]
        public ModelModel Model { get; set; }

        [DataMember]
        public ModelComponentModel Body { get; set; }

        [DataMember]
        public ModelComponentModel Equipment { get; set; }

        [DataMember]
        public EngineModel Engine { get; set; }

        [DataMember]
        public GearboxModel Gearbox { get; set; }

        [DataMember]
        public ColorModel Color { get; set; }

        [DataMember]
        public IEnumerable<OptionModel> Options { get; set; }
    }
}
