﻿using System.Collections.Generic;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Vehicles
{
    public class VehicleDescriptionModelRecord
    {
        public Model Model { get; set; }

        public Body Body { get; set; }

        public Equipment Equipment { get; set; }

        public Engine Engine { get; set; }

        public Gearbox Gearbox { get; set; }

        public IEnumerable<OptionModelRecord> Options { get; set; }

        public ColorModelRecord Color { get; set; }
    }
}
