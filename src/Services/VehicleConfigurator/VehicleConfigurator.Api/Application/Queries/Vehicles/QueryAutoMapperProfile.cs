﻿using AutoMapper;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Vehicles
{
    public class QueryAutoMapperProfile : Profile
    {
        public QueryAutoMapperProfile()
        {
            CreateMap<Option, OptionModel>();
        }
    }
}
