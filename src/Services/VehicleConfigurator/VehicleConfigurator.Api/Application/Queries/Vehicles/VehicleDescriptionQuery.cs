﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Vehicles
{
    public class VehicleDescriptionQuery : IRequest<VehicleDescriptionModel>
    {
        [DataMember]
        public string ModelKey { get; set; }

        [DataMember]
        public string ColorId { get; set; }

        [DataMember]
        public string PrNumberSet { get; set; }
    }
}
