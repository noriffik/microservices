﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.ModelKeys
{
    [DataContract]
    public class FindByOptionIdQuery : IRequest<IEnumerable<string>>
    {
        [DataMember]
        public int CatalogId { get; set; }

        [DataMember]
        public string OptionId { get; set; }
    }
}
