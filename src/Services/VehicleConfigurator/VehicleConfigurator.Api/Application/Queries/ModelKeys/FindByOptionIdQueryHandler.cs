﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.ModelKeys
{
    public class FindByOptionIdQueryHandler : IRequestHandler<FindByOptionIdQuery, IEnumerable<string>>
    {
        private readonly VehicleConfiguratorContext _context;

        public FindByOptionIdQueryHandler(VehicleConfiguratorContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<IEnumerable<string>> Handle(FindByOptionIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var optionId = OptionId.Parse(request.OptionId);

            return await _context.VehicleOffers
                .Where(v => Convert.ToString(v.ModelId) == optionId.ModelId)
                .Where(v => Convert.ToString(v.PrNumbers).Contains(optionId.PrNumber.Value))
                .Where(v => v.CatalogId == request.CatalogId)
                .Select(v => Convert.ToString(v.ModelKey))
                .ToListAsync(cancellationToken);
        }
    }
}
