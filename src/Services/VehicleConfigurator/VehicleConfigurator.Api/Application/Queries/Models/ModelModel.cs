﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Models
{
    [DataContract]
    public class ModelModel
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
