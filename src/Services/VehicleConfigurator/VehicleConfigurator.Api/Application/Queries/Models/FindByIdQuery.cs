﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Models
{
    [DataContract]
    public class FindByIdQuery : IRequest<ModelModel>
    {
        [DataMember]
        public string Id { get; set; }
    }
}
