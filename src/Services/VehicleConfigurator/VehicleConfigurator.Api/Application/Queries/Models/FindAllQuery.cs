﻿using MediatR;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Models
{
    public class FindAllQuery : IRequest<IEnumerable<ModelModel>>
    {
    }
}
