﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Models
{
    public class ModelModelMapper : BaseMapper<Model, ModelModel>
    {
        public override ModelModel Map(Model source)
        {
            return source != null
                ? new ModelModel
                {
                    Id = source.Id.Value,
                    Name = source.Name
                }
                : null;
        }
    }
}
