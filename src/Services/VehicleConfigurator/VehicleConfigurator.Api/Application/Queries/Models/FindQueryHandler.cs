﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Models
{
    public class FindQueryHandler : 
        IRequestHandler<FindByIdQuery, ModelModel>,
        IRequestHandler<FindAllQuery, IEnumerable<ModelModel>>
    {
        private IMapper<Model, ModelModel> _mapper;
        private readonly VehicleConfiguratorContext _context;

        public IMapper<Model, ModelModel> ModelMapper
        {
            get => _mapper ?? (_mapper = new ModelModelMapper());
            set => _mapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public FindQueryHandler(VehicleConfiguratorContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<ModelModel> Handle(FindByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            var specification = new IdSpecification<Model, ModelId>(ModelId.Parse(request.Id));

            var models = await _context.EntityRepository.Find<Model, ModelId>(specification, cancellationToken);
            
            return ModelMapper.Map(models.SingleOrDefault());
        }

        public async Task<IEnumerable<ModelModel>> Handle(FindAllQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var options = await _context.Models.ToListAsync(cancellationToken);

            return ModelMapper.MapMany(options);
        }
    }
}
