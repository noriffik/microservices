﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories
{
    [DataContract]
    public class FindByIdQuery : IRequest<OptionCategoryModel>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
