﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories
{
    public class OptionCategoryModelMapper : BaseMapper<OptionCategory,OptionCategoryModel>
    {
        public override OptionCategoryModel Map(OptionCategory source)
        {
            return source != null
                ? new OptionCategoryModel
                {
                    Id = source.Id,
                    Name = source.Name
                }
                : null;
        }
    }
}
