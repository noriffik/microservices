﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories
{
    [DataContract]
    public class FindAllQuery : IRequest<IEnumerable<OptionCategoryModel>>
    {
    }
}
