﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories
{
    public class FindQueryHandler : 
        IRequestHandler<FindByIdQuery, OptionCategoryModel>,
        IRequestHandler<FindAllQuery, IEnumerable<OptionCategoryModel>>
    {
        private IMapper<OptionCategory, OptionCategoryModel> _mapper;
        private readonly VehicleConfiguratorContext _context;

        public FindQueryHandler(VehicleConfiguratorContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IMapper<OptionCategory, OptionCategoryModel> ModelMapper
        {
            get => _mapper ?? (_mapper = new OptionCategoryModelMapper());
            set => _mapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public async Task<IEnumerable<OptionCategoryModel>> Handle(FindAllQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var categories = await _context.OptionCategories
                .ToListAsync(cancellationToken);

            return ModelMapper.MapMany(categories);
        }

        public async Task<OptionCategoryModel> Handle(FindByIdQuery request, CancellationToken cancellationToken)
        {
            if(request == null)
                throw new ArgumentNullException(nameof(request));

            var category = await _context.OptionCategories
                .SingleOrDefaultAsync(o => o.Id == request.Id, cancellationToken);

            return ModelMapper.Map(category);
        }
    }
}
