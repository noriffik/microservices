﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.ModelComponents
{
    public class FindQueryHandler<TComponent, TId> : 
        IRequestHandler<FindByIdQuery<TComponent, TId>, ModelComponentModel>,
        IRequestHandler<FindAllQuery<TComponent, TId>, IEnumerable<ModelComponentModel>>,
        IRequestHandler<FindByModelIdQuery<TComponent, TId>, IEnumerable<ModelComponentModel>>
        where TComponent : ModelComponent<TId>
        where TId : EntityId
    {
        protected IMapper<TComponent, ModelComponentModel> Mapper;
        protected readonly VehicleConfiguratorContext Context;

        public IMapper<TComponent, ModelComponentModel> ModelMapper
        {
            get => Mapper ?? (Mapper = new ModelComponentModelMapper<TComponent, TId>());
            set => Mapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public FindQueryHandler(VehicleConfiguratorContext context)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<ModelComponentModel> Handle(FindByIdQuery<TComponent, TId> request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var component = await Context.Set<TComponent>()
                .Where(c => Convert.ToString(c.Id) == request.Id)
                .SingleOrDefaultAsync(cancellationToken);

            return ModelMapper.Map(component);
        }

        public async Task<IEnumerable<ModelComponentModel>> Handle(FindAllQuery<TComponent, TId> request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var options = await Context.Set<TComponent>().ToListAsync(cancellationToken);

            return ModelMapper.MapMany(options);
        }

        public async Task<IEnumerable<ModelComponentModel>> Handle(FindByModelIdQuery<TComponent, TId> request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var options = await Context.Set<TComponent>()
                .Where(c => Convert.ToString(c.ModelId) == request.ModelId)
                .ToListAsync(cancellationToken);

            return ModelMapper.MapMany(options);
        }
    }
}
