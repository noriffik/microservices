﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.ModelComponents
{
    [DataContract]
    public class FindByIdQuery<TComponent, TId> : IRequest<ModelComponentModel>
        where TComponent : ModelComponent<TId>
        where TId : EntityId
    {
        [DataMember]
        public string Id { get; set; }
    }
}