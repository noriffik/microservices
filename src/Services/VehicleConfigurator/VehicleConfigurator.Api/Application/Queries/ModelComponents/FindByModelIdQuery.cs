﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.ModelComponents
{
    [DataContract]
    public class FindByModelIdQuery<TComponent, TId> : IRequest<IEnumerable<ModelComponentModel>>
        where TComponent : ModelComponent<TId>
        where TId : EntityId
    {
        [DataMember]
        public string ModelId { get; set; }
    }
}