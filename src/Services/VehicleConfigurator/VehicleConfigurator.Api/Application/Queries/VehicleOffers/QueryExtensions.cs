﻿using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.Infrastructure.Views;
using System;
using System.Linq;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers
{
    public static class QueryExtensions
    {
        public static IQueryable<VehicleOfferModelRecord> JoinRelatedDefaultColor(this IQueryable<OfferedVehicleView> source, VehicleConfiguratorContext context)
        {
            return from vehicleOffer in source
                   join colorOffers in context.ColorOffers on new { vehicleOfferId = (int?)vehicleOffer.Id, colorId = vehicleOffer.DefaultColorId }
                       equals new { vehicleOfferId = EF.Property<int?>(colorOffers, "VehicleOfferId"), colorId = Convert.ToString(colorOffers.ColorId) } into colorOfferJoin
                   from colorOffer in colorOfferJoin.DefaultIfEmpty()
                   join colors in context.Colors on Convert.ToString(colorOffer.ColorId) equals Convert.ToString(colors.Id) into colorJoin
                   from color in colorJoin.DefaultIfEmpty()
                   join colorTypes in context.ColorTypes on Convert.ToString(color.TypeId) equals Convert.ToString(colorTypes.Id) into colorTypeJoin
                   from colorType in colorTypeJoin.DefaultIfEmpty()
                   select new VehicleOfferModelRecord
                   {
                       VehicleView = vehicleOffer,
                       DefaultColor = colorOffer == null ? null : new ColorOfferModelRecord
                       {
                           ColorOffer = colorOffer,
                           Color = new ColorModelRecord
                           {
                               Color = color,
                               Type = colorType
                           }
                       }
                   };
        }
    }
}
