﻿using MediatR;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers
{
    public class FindByModelKeyQuery : IRequest<VehicleOfferModel>
    {
        public FindByModelKeyQuery()
        {
        }

        public FindByModelKeyQuery(int catalogId, string modelKey)
        {
            CatalogId = catalogId;
            ModelKey = modelKey;
        }

        public int CatalogId { get; set; }

        public string ModelKey { get; set; }
    }
}
