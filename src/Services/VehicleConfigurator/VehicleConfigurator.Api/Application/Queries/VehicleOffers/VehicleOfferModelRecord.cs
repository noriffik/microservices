﻿using NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers;
using NexCore.VehicleConfigurator.Infrastructure.Views;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers
{
    public class VehicleOfferModelRecord
    {
        public OfferedVehicleView VehicleView { get; set; }

        public ColorOfferModelRecord DefaultColor { get; set; }
    }
}
