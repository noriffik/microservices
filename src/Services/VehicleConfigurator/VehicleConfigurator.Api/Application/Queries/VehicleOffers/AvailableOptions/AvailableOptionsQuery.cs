﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.AvailableOptions
{
    [DataContract]
    public class AvailableOptionsQuery : IRequest<IEnumerable<AvailableOptionModel>>
    {
        [DataMember]
        public int VehicleOfferId { get; set; }
    }
}
