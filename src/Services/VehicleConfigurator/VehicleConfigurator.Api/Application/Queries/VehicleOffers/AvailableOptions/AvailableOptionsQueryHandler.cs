﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.AvailableOptions
{
    public class AvailableOptionsQueryHandler : IRequestHandler<AvailableOptionsQuery, IEnumerable<AvailableOptionModel>>
    {
        private readonly VehicleConfiguratorContext _context;
        private readonly IMapper _mapper;

        public AvailableOptionsQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<AvailableOptionModel>> Handle(AvailableOptionsQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var modelIdQuery = from vehicleOffer in _context.VehicleOffers
                where vehicleOffer.Id == request.VehicleOfferId
                select vehicleOffer.ModelId;

            var modelId = await modelIdQuery.SingleOrDefaultAsync(cancellationToken);
            if (modelId == null)
                return new AvailableOptionModel[] {};
            
            var query = from option in _context.Options
                where Convert.ToString(option.ModelId) == modelId
                join optionOffers in _context.OptionOffers on new { OptionId = option.Id, VehicleOfferId = new int?(request.VehicleOfferId) }
                    equals new { optionOffers.OptionId, VehicleOfferId = EF.Property<int?>(optionOffers, "VehicleOfferId") } into offerJoin
                from optionOffer in offerJoin.DefaultIfEmpty()
                join optionCategories in _context.OptionCategories on option.OptionCategoryId
                    equals optionCategories.Id into categoryJoin
                from optionCategory in categoryJoin.DefaultIfEmpty()
                select new
                {
                    Option = option,
                    Category = optionCategory,
                    Offer = optionOffer
                };

            var records = await query.ToListAsync(cancellationToken);
            
            return _mapper.Map<IEnumerable<AvailableOptionModel>>(records);
        }
    }
}
