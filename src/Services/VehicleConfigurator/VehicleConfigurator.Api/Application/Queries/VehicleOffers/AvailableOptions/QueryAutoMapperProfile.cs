﻿using AutoMapper;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.AvailableOptions
{
    public class QueryAutoMapperProfile : Profile
    {
        private class ExtendedRuleSetConverter : IValueConverter<string, RuleSetExtendedModel>
        {
            private readonly IMapper<string, RuleSetExtendedModel> _mapper;

            public ExtendedRuleSetConverter(IMapper<string, RuleSetExtendedModel> mapper)
            {
                _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            }

            public RuleSetExtendedModel Convert(string sourceMember, ResolutionContext context)
            {
                return _mapper.Map(sourceMember);
            }
        }

        private static readonly ExtendedRuleSetConverter RuleSetConverter = new ExtendedRuleSetConverter(new RuleSetExtendedModelMapper());

        public QueryAutoMapperProfile()
        {
            CreateMap<Option, AvailableOptionModel.OptionModel>();
            CreateMap<OptionOffer, AvailableOptionModel.OfferModel>()
                .ForMember(d => d.RuleSet, expression => expression.ConvertUsing(RuleSetConverter));

            CreateMap<OptionCategory, AvailableOptionModel.CategoryModel>();
            CreateMap<Availability, AvailabilityModel>();
        }
    }
}
