﻿using NexCore.VehicleConfigurator.Api.Application.Models.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.AvailableOptions
{
    [DataContract]
    public class AvailableOptionModel
    {
        [DataMember]
        public OptionModel Option { get; set; }

        [DataMember]
        public CategoryModel Category { get; set; }

        [DataMember]
        public OfferModel Offer { get; set; }

        [DataContract]
        public class OptionModel
        {
            [DataMember]
            public string Id { get; set; }

            [DataMember]
            public string Name { get; set; }

            [DataMember]
            public string ModelId { get; set; }

            [DataMember]
            public string PrNumber { get; set; }
        }

        [DataContract]
        public class OfferModel
        {
            [DataMember]
            public int Id { get; set; }

            [DataMember]
            public string OptionId { get; set; }

            [DataMember]
            public AvailabilityModel Availability { get; set; }

            [DataMember]
            public Relevance Relevance { get; set; }

            [DataMember]
            public RelevancePeriodModel RelevancePeriod { get; set; }

            [DataMember]
            public Inclusion Inclusion { get; set; }

            [DataMember]
            public RuleSetExtendedModel RuleSet { get; set; }

            [DataMember]
            public bool IsRestricted { get; set; }
        }

        [DataContract]
        public class CategoryModel
        {
            [DataMember]
            public int Id { get; set; }

            [DataMember]
            public string Name { get; set; }
        }
    }
}
