﻿using AutoMapper;
using NexCore.VehicleConfigurator.Api.Application.Models.Catalogs;
    
namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers
{
   public class AutoMapperProfile : Profile
   {
       public AutoMapperProfile()
       {
            CreateMap<VehicleOfferModelRecord, VehicleOfferModel>()
                .ForMember(d => d.Id, m => m.MapFrom(s => s.VehicleView.Id))
                .ForMember(d => d.ModelKey, m => m.MapFrom(s => s.VehicleView.ModelKey))
                .ForMember(d => d.Price, m => m.MapFrom(s => s.VehicleView.Price))
                .ForMember(d => d.CatalogId, m => m.MapFrom(s => s.VehicleView.CatalogId))
                .ForMember(d => d.Relevance, m => m.MapFrom(s => s.VehicleView.Relevance))
                .ForMember(d => d.RelevancePeriod, m => m.MapFrom(s => new RelevancePeriodModel
                {
                    From = s.VehicleView.RelevancePeriodFrom,
                    To = s.VehicleView.RelevancePeriodTo
                }))
                .ForMember(d => d.Model, m => m.MapFrom(s => s.VehicleView.Model))
                .ForMember(d => d.Body, m => m.MapFrom(s => s.VehicleView.Body))
                .ForMember(d => d.Equipment, m => m.MapFrom(s => s.VehicleView.Equipment))
                .ForMember(d => d.Engine, m => m.MapFrom(s => s.VehicleView.Engine))
                .ForMember(d => d.Gearbox, m => m.MapFrom(s => s.VehicleView.Gearbox))
                .ForMember(d => d.Status, m => m.MapFrom(s => s.VehicleView.Status))
                .ForMember(d => d.DefaultColor, m => m.MapFrom(s => s.DefaultColor));
       }
   }
}