﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers
{
    [DataContract]
    public class FindByIdQuery : IRequest<VehicleOfferModel>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
