﻿using AutoMapper;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.AvailableColors
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ColorOffer, AvailableColorModel.ColorOfferModel>();

            CreateMap<AvailableColorModelRecord, AvailableColorModel>();
        }
    }
}
