﻿using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.AvailableColors
{
    public class AvailableColorModelRecord
    {
        public ColorModelRecord Color { get; set; }

        public ColorOffer ColorOffer { get; set; }
    }
}
