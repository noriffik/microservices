﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.AvailableColors
{
    public class AvailableColorsQueryHandler : IRequestHandler<AvailableColorsQuery, IEnumerable<AvailableColorModel>>
    {
        private readonly VehicleConfiguratorContext _context;
        private readonly IMapper _mapper;

        public AvailableColorsQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<AvailableColorModel>> Handle(AvailableColorsQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var query = from color in _context.Colors
                        join colorType in _context.ColorTypes on Convert.ToString(color.TypeId) equals Convert.ToString(
                            colorType.Id)
                        join colorOffers in _context.ColorOffers on new { ColorId = Convert.ToString(color.Id), VehicleOfferId = (int?)request.VehicleOfferId }
                            equals new { ColorId = Convert.ToString(colorOffers.ColorId), VehicleOfferId = EF.Property<int?>(colorOffers, "VehicleOfferId") } into offerJoin
                        from colorOffer in offerJoin.DefaultIfEmpty()
                        select new AvailableColorModelRecord
                        {
                            Color = new ColorModelRecord
                            {
                                Color = color,
                                Type = colorType
                            },
                            ColorOffer = colorOffer
                        };

            var records = await query.ToListAsync(cancellationToken);

            return _mapper.Map<IEnumerable<AvailableColorModel>>(records);
        }
    }
}
