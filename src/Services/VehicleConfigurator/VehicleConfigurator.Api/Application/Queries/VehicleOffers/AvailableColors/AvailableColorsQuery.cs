﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.AvailableColors
{
    [DataContract]
    public class AvailableColorsQuery : IRequest<IEnumerable<AvailableColorModel>>
    {
        [DataMember]
        public int VehicleOfferId { get; set; }
    }
}
