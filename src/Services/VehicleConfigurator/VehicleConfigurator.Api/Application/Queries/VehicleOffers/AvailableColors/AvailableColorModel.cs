﻿using NexCore.VehicleConfigurator.Api.Application.Models.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Domain;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.AvailableColors
{
    [DataContract]
    public class AvailableColorModel
    {
        [DataMember]
        public ColorModel Color { get; set; }

        [DataMember]
        public ColorOfferModel ColorOffer { get; set; }

        [DataContract]
        public class ColorOfferModel
        {
            [DataMember]
            public int Id { get; set; }

            [DataMember]
            public decimal Price { get; set; }

            [DataMember]
            public Relevance Relevance { get; set; }

            [DataMember]
            public RelevancePeriodModel RelevancePeriod { get; set; }
        }
    }
}
