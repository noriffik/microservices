﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Extensions;
using NexCore.Application.Queries;
using NexCore.Infrastructure.QueryHandlers;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.Sort;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.Infrastructure.Views;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers
{
    public class FindQueryHandler : IRequestHandler<FindByIdQuery, VehicleOfferModel>,
        IRequestHandler<FindAllQuery, PagedResponse<VehicleOfferModel>>,
        IRequestHandler<FindByCatalogIdQuery, PagedResponse<VehicleOfferModel>>,
        IRequestHandler<FindByModelKeyQuery, VehicleOfferModel>
    {
        private readonly VehicleConfiguratorContext _context;
        private readonly IMapper _mapper;

        public FindQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<VehicleOfferModel> Handle(FindByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await _context.Query<OfferedVehicleView>()
                .JoinRelatedDefaultColor(_context)
                .SingleOrDefaultAsync(r => r.VehicleView.Id == request.Id, cancellationToken);

            return _mapper.Map<VehicleOfferModel>(result);
        }

        public async Task<PagedResponse<VehicleOfferModel>> Handle(FindAllQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await _context.Query<OfferedVehicleView>()
                .JoinRelatedDefaultColor(_context)
                .SortBy(request.SortBy, request.SortDirection)
                .Paginate(request, cancellationToken);

            return _mapper.MapPagedResponse<VehicleOfferModelRecord, VehicleOfferModel>(result);
        }

        public async Task<PagedResponse<VehicleOfferModel>> Handle(FindByCatalogIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await _context.Query<OfferedVehicleView>()
                .JoinRelatedDefaultColor(_context)
                .Where(r => r.VehicleView.CatalogId == request.CatalogId)
                .SortBy(request.SortBy, request.SortDirection)
                .Paginate(request, cancellationToken);

            return _mapper.MapPagedResponse<VehicleOfferModelRecord, VehicleOfferModel>(result);
        }

        public async Task<VehicleOfferModel> Handle(FindByModelKeyQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await _context.Query<OfferedVehicleView>()
                .JoinRelatedDefaultColor(_context)
                .Where(r => r.VehicleView.CatalogId == request.CatalogId)
                .SingleOrDefaultAsync(r => r.VehicleView.ModelKey == request.ModelKey, cancellationToken);

            return _mapper.Map<VehicleOfferModel>(result);
        }
    }
}
