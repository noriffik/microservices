﻿using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.Sort;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers
{
    [DataContract]
    public class FindAllQuery : PagedRequest<VehicleOfferModel>
    {
        [FromQuery]
        [DataMember]
        public SortCriterion? SortBy { get; set; }

        [FromQuery]
        [DataMember]
        public SortDirection? SortDirection { get; set; }
    }
}
