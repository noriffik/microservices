﻿using NexCore.VehicleConfigurator.Api.Application.Models.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Engines;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Gearboxes;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.Models;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers
{
    [DataContract]
    public class VehicleOfferModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string ModelKey { get; set; }

        [DataMember]
        public ModelModel Model { get; set; }

        [DataMember]
        public ModelComponentModel Body { get; set; }

        [DataMember]
        public ModelComponentModel Equipment { get; set; }

        [DataMember]
        public EngineModel Engine { get; set; }

        [DataMember]
        public GearboxModel Gearbox { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public int CatalogId { get; set; }

        [DataMember]
        public VehicleOfferStatus Status { get; set; }

        [DataMember]
        public Relevance Relevance { get; set; }

        [DataMember]
        public RelevancePeriodModel RelevancePeriod { get; set; }

        [DataMember]
        public ColorOfferModel DefaultColor { get; set; }
    }
}
