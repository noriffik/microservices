﻿using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.Sort;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers
{
    [DataContract]
    public class FindByCatalogIdQuery : PagedRequest<VehicleOfferModel>
    {
        [FromRoute]
        [DataMember]
        public int CatalogId { get; set; }

        [FromQuery]
        [DataMember]
        public SortCriterion? SortBy { get; set; }

        [FromQuery]
        [DataMember]
        public SortDirection? SortDirection { get; set; }
    }
}
