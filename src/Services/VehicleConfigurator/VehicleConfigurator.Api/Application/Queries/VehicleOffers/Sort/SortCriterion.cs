﻿namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.Sort
{
    public enum SortCriterion
    {
        Id = 0,
        ModelKey = 1,
        Body = 2,
        Equipment = 3,
        Engine = 4,
        Gearbox = 5,
        Price = 6,
        Status = 7
    }
}