﻿using NexCore.Application.Queries;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.Sort
{
    public static class QueryableSortExtensions
    {
        public static IQueryable<VehicleOfferModelRecord> SortBy(this IQueryable<VehicleOfferModelRecord> query, SortCriterion? sortCriterion, SortDirection? sortDirection)
        {
            switch (sortCriterion)
            {
                case SortCriterion.ModelKey:
                    return query.InnerSort(m => m.VehicleView.ModelKey, sortDirection);

                case SortCriterion.Body:
                    return query.InnerSort(m => m.VehicleView.Body.Name, sortDirection);

                case SortCriterion.Equipment:
                    return query.InnerSort(m => m.VehicleView.Equipment.Name, sortDirection);

                case SortCriterion.Engine:
                    return query.InnerSort(m => m.VehicleView.Engine.Name, sortDirection);

                case SortCriterion.Gearbox:
                    return query.InnerSort(m => m.VehicleView.Gearbox.Name, sortDirection);

                case SortCriterion.Price:
                    return query.InnerSort(m => m.VehicleView.Price, sortDirection);

                case SortCriterion.Status:
                    return query.InnerSort(m => m.VehicleView.Status, sortDirection);

                case SortCriterion.Id:
                    return query.InnerSort(m => m.VehicleView.Id, sortDirection);

                default:
                    return query.OrderBy(m => m.VehicleView.Id);
            }
        }

        private static IQueryable<VehicleOfferModelRecord> InnerSort(
            this IQueryable<VehicleOfferModelRecord> query, Expression<Func<VehicleOfferModelRecord, object>> keySelector, SortDirection? sortDirection)
        {
            return sortDirection == SortDirection.Descending ? query.OrderByDescending(keySelector) : query.OrderBy(keySelector);
        }
    }
}
