﻿using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers.ByComponents
{
    [DataContract]
    public class OptionOfferByComponentModel : OptionOfferModel
    {
        [DataMember]
        public VehicleOfferModel VehicleOffer { get; set; }
    }
}
