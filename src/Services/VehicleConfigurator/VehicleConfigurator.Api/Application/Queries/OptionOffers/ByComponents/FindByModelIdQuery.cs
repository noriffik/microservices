﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers.ByComponents
{
    [DataContract]
    public class FindByModelIdQuery : IRequest<IEnumerable<OptionOfferByComponentModel>>
    {
        [DataMember]
        public int CatalogId { get; set; }

        [DataMember]
        public string ModelId { get; set; }

        [DataMember]
        public string PrNumber { get; set; }
    }
}
