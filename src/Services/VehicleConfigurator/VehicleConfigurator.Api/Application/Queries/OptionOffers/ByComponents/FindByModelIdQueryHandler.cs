﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.Infrastructure.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers.ByComponents
{
    public class FindByModelIdQueryHandler : IRequestHandler<FindByModelIdQuery, IEnumerable<OptionOfferByComponentModel>>
    {
        private readonly VehicleConfiguratorContext _context;
        private readonly IMapper _mapper;
        private IMapper<OptionOfferByComponentRecord, OptionOfferByComponentModel> _modelMapper;

        public IMapper<OptionOfferByComponentRecord, OptionOfferByComponentModel> ModelMapper
        {
            get => _modelMapper ?? (_modelMapper = new OptionOfferByComponentModelMapper(_mapper));
            set => _modelMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public FindByModelIdQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<OptionOfferByComponentModel>> Handle(FindByModelIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var restrictions = await QueryRestrictionsForVehicleOffer(request.CatalogId, request.ModelId);

            var query = from vehicleOffer in _context.Query<OfferedVehicleView>()
                .JoinRelatedDefaultColor(_context)
                join optionOffer in _context.OptionOffers on vehicleOffer.VehicleView.Id
                    equals EF.Property<int>(optionOffer, "VehicleOfferId")
                join option in _context.Options on optionOffer.OptionId
                    equals option.Id
                join categories in _context.OptionCategories on option.OptionCategoryId
                    equals categories.Id into categoryJoin
                from category in categoryJoin.DefaultIfEmpty()
                where vehicleOffer.VehicleView.CatalogId == request.CatalogId
                where vehicleOffer.VehicleView.ModelKey.Substring(0, 2) == request.ModelId
                where Convert.ToString(optionOffer.PrNumber) == request.PrNumber
                select new OptionOfferByComponentRecord
                {
                    VehicleOfferRecord = vehicleOffer,
                    OptionOffer = optionOffer,
                    Option = new OptionModelRecord
                    {
                       Category = category,
                       Option = option
                    },
                    RestrictionPeriod = restrictions
                       .ApplicableTo(vehicleOffer.VehicleView.ModelKey, optionOffer.PrNumber)
                       .CompositePeriod()
                };

            var results = await query.ToListAsync(cancellationToken);

            return ModelMapper.MapMany(results);
        }

        private async Task<IEnumerable<Restriction>> QueryRestrictionsForVehicleOffer(int catalogId, string modelId)
        {
            var query = from restriction in _context.Restrictions
                where restriction.CatalogId == catalogId
                where Convert.ToString(restriction.ModelKey.ModelId) == modelId
                where restriction.IsEnabled
                select restriction;

            return await query.ToListAsync();
        }
    }
}
