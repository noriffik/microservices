﻿using AutoMapper;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Models.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Api.Application.Queries.Restrictions;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers.ByComponents
{
    public class OptionOfferByComponentModelMapper : BaseMapper<OptionOfferByComponentRecord, OptionOfferByComponentModel>
    {
        private IMapper<OptionModelRecord, OptionModel> _optionMapper;
        private IMapper<Availability, AvailabilityModel> _availabilityMapper;
        private IMapper<string, RuleSetExtendedModel> _ruleSetExtendedModelMapper;
        private readonly IMapper _mapper;

        public OptionOfferByComponentModelMapper(IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public IMapper<OptionModelRecord, OptionModel> OptionMapper
        {
            get => _optionMapper ?? (_optionMapper = new OptionModelMapper());
            set => _optionMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public IMapper<Availability, AvailabilityModel> AvailabilityMapper
        {
            get => _availabilityMapper ?? (_availabilityMapper = new AvailabilityModelMapper());
            set => _availabilityMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public IMapper<string, RuleSetExtendedModel> RuleSetExtendedModelMapper
        {
            get => _ruleSetExtendedModelMapper ?? (_ruleSetExtendedModelMapper = new RuleSetExtendedModelMapper());
            set => _ruleSetExtendedModelMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public override OptionOfferByComponentModel Map(OptionOfferByComponentRecord source)
        {
            return source != null
                ? new OptionOfferByComponentModel
                {
                    Id = source.OptionOffer.Id,
                    OptionId = source.OptionOffer.OptionId.Value,
                    Availability = AvailabilityMapper.Map(source.OptionOffer.Availability),
                    Inclusion = source.OptionOffer.Inclusion,
                    Relevance = source.OptionOffer.Relevance,
                    RelevancePeriod = new RelevancePeriodModel
                    {
                        From = source.OptionOffer.RelevancePeriodFrom,
                        To = source.OptionOffer.RelevancePeriodTo
                    },
                    IsRestricted = source.OptionOffer.IsRestricted,
                    RestrictionPeriod = new RestrictionPeriodModel
                    {
                        From = source.RestrictionPeriod != null ? source.RestrictionPeriod.From : null,
                        To = source.RestrictionPeriod?.To
                    },
                    Option = OptionMapper.Map(source.Option),
                    VehicleOffer = _mapper.Map<VehicleOfferModel>(source.VehicleOfferRecord),
                    RuleSet = RuleSetExtendedModelMapper.Map(source.OptionOffer.RuleSet)
                }
                : null;
        }
    }
}
