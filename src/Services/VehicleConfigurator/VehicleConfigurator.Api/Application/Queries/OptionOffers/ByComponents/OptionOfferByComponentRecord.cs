﻿using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers.ByComponents
{
    public class OptionOfferByComponentRecord : OptionOfferModelRecord
    {
        public VehicleOfferModelRecord VehicleOfferRecord { get; set; }
    }
}
