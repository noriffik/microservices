﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers
{
    [DataContract]
    public class FindByVehicleOfferIdAndPrNumberQuery : IRequest<OptionOfferModel>
    {
        [DataMember]
        public int VehicleOfferId { get; set; }

        [DataMember]
        public string PrNumber { get; set; }
    }
}
