﻿using NexCore.VehicleConfigurator.Api.Application.Models.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Api.Application.Queries.Restrictions;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers
{
    [DataContract]
    public class OptionOfferModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string OptionId { get; set; }

        [DataMember]
        public AvailabilityModel Availability { get; set; }

        [DataMember]
        public Inclusion Inclusion { get; set; }

        [DataMember]
        public Relevance Relevance { get; set; }

        [DataMember]
        public RelevancePeriodModel RelevancePeriod { get; set; }

        [DataMember]
        public OptionModel Option { get; set; }

        [DataMember]
        public RuleSetExtendedModel RuleSet { get; set; }

        [DataMember]
        public bool IsRestricted { get; set; }

        [DataMember]
        public RestrictionPeriodModel RestrictionPeriod { get; set; }
    }
}
