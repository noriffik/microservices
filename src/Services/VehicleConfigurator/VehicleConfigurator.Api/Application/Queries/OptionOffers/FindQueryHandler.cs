﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers
{
    public class FindQueryHandler : IRequestHandler<FindByVehicleOfferIdAndPrNumberQuery, OptionOfferModel>,
        IRequestHandler<FindByVehicleOfferIdQuery, IEnumerable<OptionOfferModel>>
    {
        private readonly VehicleConfiguratorContext _context;
        private IMapper<OptionOfferModelRecord, OptionOfferModel> _modelMapper;

        public IMapper<OptionOfferModelRecord, OptionOfferModel> ModelMapper
        {
            get => _modelMapper ?? (_modelMapper = new OptionOfferModelMapper());
            set => _modelMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public FindQueryHandler(VehicleConfiguratorContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<IEnumerable<OptionOfferModel>> Handle(FindByVehicleOfferIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var restrictions = await QueryRestrictionsForVehicleOffer(request.VehicleOfferId);

            var query = from vehicleOffer in _context.VehicleOffers
                join optionOffer in _context.OptionOffers on vehicleOffer.Id
                    equals EF.Property<int>(optionOffer, "VehicleOfferId")
                join option in _context.Options on optionOffer.OptionId
                    equals option.Id
                join categories in _context.OptionCategories on option.OptionCategoryId
                    equals categories.Id into categoryJoin
                from category in categoryJoin.DefaultIfEmpty()
                where vehicleOffer.Id == request.VehicleOfferId
                select new OptionOfferModelRecord
                {
                    OptionOffer = optionOffer,
                    Option = new OptionModelRecord
                    {
                        Category = category,
                        Option = option
                    },
                    RestrictionPeriod = restrictions
                        .ApplicableTo(vehicleOffer.ModelKey, optionOffer.PrNumber)
                        .CompositePeriod()
                };

            var results = await query.ToListAsync(cancellationToken);
            
            return ModelMapper.MapMany(results);
        }

        private async Task<IEnumerable<Restriction>> QueryRestrictionsForVehicleOffer(int vehicleOfferId)
        {
            var query = from restriction in _context.Restrictions
                join vehicleOffer in _context.VehicleOffers on vehicleOfferId equals vehicleOffer.Id
                where restriction.CatalogId == vehicleOffer.CatalogId
                where Convert.ToString(restriction.ModelKey) == Convert.ToString(vehicleOffer.ModelKey)
                where restriction.IsEnabled
                select restriction;

            return await query.ToListAsync();
        }

        public async Task<OptionOfferModel> Handle(FindByVehicleOfferIdAndPrNumberQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var restrictions = await QueryRestrictionsForVehicleOffer(request.VehicleOfferId);

            var query = from vehicleOffer in _context.VehicleOffers
                join optionOffer in _context.OptionOffers on vehicleOffer.Id
                    equals EF.Property<int>(optionOffer, "VehicleOfferId")
                join option in _context.Options on optionOffer.OptionId
                    equals option.Id
                join categories in _context.OptionCategories on option.OptionCategoryId
                    equals categories.Id into categoryJoin
                from category in categoryJoin.DefaultIfEmpty()
                where EF.Property<int>(optionOffer, "VehicleOfferId") == request.VehicleOfferId
                where Convert.ToString(optionOffer.OptionId).EndsWith(request.PrNumber)
                select new OptionOfferModelRecord
                {
                    OptionOffer = optionOffer,
                    Option = new OptionModelRecord
                    {
                        Category = category,
                        Option = option
                    },
                    RestrictionPeriod = restrictions
                        .ApplicableTo(vehicleOffer.ModelKey, optionOffer.PrNumber)
                        .CompositePeriod()
                };

            var result = await query.SingleOrDefaultAsync(cancellationToken);

            return ModelMapper.Map(result);
        }
    }
}
