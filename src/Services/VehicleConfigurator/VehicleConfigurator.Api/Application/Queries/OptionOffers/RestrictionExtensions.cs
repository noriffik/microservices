﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers
{
    public static class RestrictionExtensions
    {
        public static IEnumerable<Restriction> ApplicableTo(this IEnumerable<Restriction> restrictions, ModelKey modelKey, 
            PrNumber prNumber)
        {
            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));

            return restrictions
                .Where(r => r.ModelKey == modelKey)
                .Where(r => r.PrNumbers.Has(prNumber))
                .Where(r => r.IsRelevant());
        }

        public static IEnumerable<Restriction> ApplicableTo(this IEnumerable<Restriction> restrictions,
            PrNumber prNumber)
        {
            if (prNumber == null)
                throw new ArgumentNullException(nameof(prNumber));

            return restrictions
                .Where(r => r.PrNumbers.Has(prNumber))
                .Where(r => r.IsRelevant());
        }

        public static Period CompositePeriod(this IEnumerable<Restriction> restrictions)
        {
            return restrictions.Select(r => r.RelevancePeriod)
                .CompositePeriod();
        }
        
        public static Period CompositePeriod(this IEnumerable<Period> periods)
        {
            return periods.Aggregate<Period, Period>(null, (current, period) => period.Extend(current));
        }

        private static Period Extend(this Period candidate, Period against)
        {
            if (against == null)
                return candidate;

            var from = candidate.From < against.From ? candidate.From : against.From;
            var to = candidate.To == null || candidate.To > against.To ? candidate.To : against.To;
            
            return new Period(from.GetValueOrDefault(), to);
        }
    }
}
