﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers
{
    [DataContract]
    public class FindByVehicleOfferIdQuery : IRequest<IEnumerable<OptionOfferModel>>
    {
        [DataMember]
        public int VehicleOfferId { get; set; }
    }
}
