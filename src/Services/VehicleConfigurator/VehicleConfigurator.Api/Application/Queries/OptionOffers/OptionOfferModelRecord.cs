﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers
{
    public class OptionOfferModelRecord
    {
        public OptionOffer OptionOffer { get; set; }

        public OptionModelRecord Option { get; set; }

        public Period RestrictionPeriod { get; set; }
    }
}
