﻿using System.Runtime.Serialization;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers
{
    [DataContract]
    public class RuleExtendedModel
    {
        [DataMember] 
        public RuleModel Rule { get; set; }

        [DataMember] 
        public RuleDetailsModel Details { get; set; }
    }
}
