﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers
{
    [DataContract]
    public class RuleDetailsModel
    {
        [DataMember] 
        public string RuleString { get; set; }

        [DataMember] 
        public string Message { get; set; }
    }
}
