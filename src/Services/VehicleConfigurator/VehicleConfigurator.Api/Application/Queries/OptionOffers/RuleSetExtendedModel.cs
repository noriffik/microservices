﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers
{
    [DataContract]
    public class RuleSetExtendedModel
    {
        [DataMember]
        public IEnumerable<RuleExtendedModel> RuleSet { get; set; }
    }
}
