﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Models.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Api.Application.Queries.Restrictions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers
{
    public class OptionOfferModelMapper : BaseMapper<OptionOfferModelRecord, OptionOfferModel>
    {
        private IMapper<OptionModelRecord, OptionModel> _optionMapper;
        private IMapper<Availability, AvailabilityModel> _availabilityMapper;
        private IMapper<string, RuleSetExtendedModel> _ruleSetExtendedModelMapper;

        public IMapper<OptionModelRecord, OptionModel> OptionMapper
        {
            get => _optionMapper ?? (_optionMapper = new OptionModelMapper());
            set => _optionMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public IMapper<Availability, AvailabilityModel> AvailabilityMapper
        {
            get => _availabilityMapper ?? (_availabilityMapper = new AvailabilityModelMapper());
            set => _availabilityMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public IMapper<string, RuleSetExtendedModel> RuleSetExtendedModelMapper
        {
            get => _ruleSetExtendedModelMapper ?? (_ruleSetExtendedModelMapper = new RuleSetExtendedModelMapper());
            set => _ruleSetExtendedModelMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public override OptionOfferModel Map(OptionOfferModelRecord source)
        {
            return source != null
                ? new OptionOfferModel
                {
                    Id = source.OptionOffer.Id,
                    OptionId = source.OptionOffer.OptionId.Value,
                    Availability = AvailabilityMapper.Map(source.OptionOffer.Availability),
                    Inclusion = source.OptionOffer.Inclusion,
                    Relevance = source.OptionOffer.Relevance,
                    RelevancePeriod = new RelevancePeriodModel
                    {
                        From = source.OptionOffer.RelevancePeriodFrom,
                        To = source.OptionOffer.RelevancePeriodTo
                    },
                    IsRestricted = source.OptionOffer.IsRestricted,
                    RestrictionPeriod = new RestrictionPeriodModel
                    {
                        From = source.RestrictionPeriod != null ? source.RestrictionPeriod.From : null,
                        To = source.RestrictionPeriod?.To
                    },
                    Option = OptionMapper.Map(source.Option),
                    RuleSet = RuleSetExtendedModelMapper.Map(source.OptionOffer.RuleSet)
                }
                : null;
        }
    }
}
