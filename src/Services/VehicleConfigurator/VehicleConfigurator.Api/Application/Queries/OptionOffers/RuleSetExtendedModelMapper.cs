﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Rules;
using NexCore.VehicleConfigurator.Domain.Rules.Expressions;
using System;
using System.Linq;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers
{
    public class RuleSetExtendedModelMapper : BaseMapper<string, RuleSetExtendedModel>
    {
        private IRuleDescriber _ruleDescriber;
        private IRuleMessageComposer _messageComposer;

        public IRuleDescriber RuleDescriber
        {
            get => _ruleDescriber ?? (_ruleDescriber = new RuleDescriber());
            set => _ruleDescriber = value ?? throw new ArgumentNullException(nameof(value));
        }
        
        public IRuleMessageComposer MessageComposer
        {
            get => _messageComposer ?? (_messageComposer = new RuleMessageComposer());
            set => _messageComposer = value ?? throw new ArgumentNullException(nameof(value));
        }

        private IMapper<string, RuleSetModel> _ruleSetModelMapper;
        
        public IMapper<string, RuleSetModel> RuleSetModelMapper
        {
            get => _ruleSetModelMapper ?? (_ruleSetModelMapper = new RuleSetModelMapper());
            set => _ruleSetModelMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public override RuleSetExtendedModel Map(string source)
        {
            if (string.IsNullOrEmpty(source))
                return null;
            
            return new RuleSetExtendedModel
            {
                RuleSet = RuleDescriber
                    .DescribeMany(source)
                    .Select(MapRuleExtendedModel)
            };
        }
        
        private RuleExtendedModel MapRuleExtendedModel(IRuleDescription description)
        {
            return new RuleExtendedModel
            {
                Rule = MapRuleModel(description),
                Details = new RuleDetailsModel
                {
                    RuleString = description.Rule,
                    Message = MessageComposer.Compose(description.Name, description.PrNumbers.AsString)
                }
            };
        }

        private RuleModel MapRuleModel(IRuleDescription description)
        {
            return new RuleModel
            {
                RuleName = description.Name,
                PrNumbers = description.PrNumbers.AsString,
                NestedRuleSet = RuleSetModelMapper.Map(description.Requirement)
            };
        }
    }
}
