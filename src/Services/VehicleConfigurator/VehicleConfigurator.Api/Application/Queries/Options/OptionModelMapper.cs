﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Options
{
    public class OptionModelMapper : BaseMapper<OptionModelRecord, OptionModel>
    {
        private IMapper<OptionCategory, OptionCategoryModel> _categoryMapper;

        public override OptionModel Map(OptionModelRecord source)
        {
            return source != null
                ? new OptionModel
                {
                    Id = source.Option.Id.Value,
                    Name = source.Option.Name,
                    ModelId = source.Option.ModelId.Value,
                    PrNumber = source.Option.PrNumber.Value,
                    Category = CategoryMapper.Map(source.Category)
                }
                : null;
        }

        public IMapper<OptionCategory, OptionCategoryModel> CategoryMapper
        {
            get => _categoryMapper ?? (_categoryMapper = new OptionCategoryModelMapper());
            set => _categoryMapper = value ?? throw new ArgumentNullException(nameof(value));
        }
    }
}
