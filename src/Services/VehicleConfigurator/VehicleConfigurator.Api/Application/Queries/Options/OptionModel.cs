﻿using NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Options
{
    [DataContract]
    public class OptionModel
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string ModelId { get; set; }

        [DataMember]
        public string PrNumber { get; set; }

        [DataMember]
        public OptionCategoryModel Category { get; set; }
    }
}
