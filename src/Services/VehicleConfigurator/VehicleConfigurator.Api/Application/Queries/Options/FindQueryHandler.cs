﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Options
{
    public class FindQueryHandler :
        IRequestHandler<FindByIdQuery, OptionModel>,
        IRequestHandler<FindByModelIdQuery, IEnumerable<OptionModel>>,
        IRequestHandler<FindByPrNumberSetQuery, IEnumerable<OptionModel>>,
        IRequestHandler<FindAllQuery, IEnumerable<OptionModel>>
    {
        private readonly VehicleConfiguratorContext _context;
        private IMapper<OptionModelRecord, OptionModel> _modelMapper;

        public IMapper<OptionModelRecord, OptionModel> ModelMapper
        {
            get => _modelMapper ?? (_modelMapper = new OptionModelMapper());
            set => _modelMapper = value ?? throw new ArgumentNullException(nameof(value));
        }
        
        public FindQueryHandler(VehicleConfiguratorContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        
        public async Task<IEnumerable<OptionModel>> Handle(FindByPrNumberSetQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var query = AsRecord(_context.Options
                .Where(o => Convert.ToString(o.ModelId) == request.ModelId)
                .Where(o => request.PrNumberSet.Contains(Convert.ToString(o.PrNumber))));

            var records = await query.ToListAsync(cancellationToken);
            
            return ModelMapper.MapMany(records);
        }

        private IQueryable<OptionModelRecord> AsRecord(IQueryable<Option> query)
        {
            return query.LeftJoin(_context.OptionCategories,
                option => option.OptionCategoryId,
                category => category.Id,
                (join, category) => new OptionModelRecord
                {
                    Category = category,
                    Option = join.Outer
                });
        }

        public async Task<IEnumerable<OptionModel>> Handle(FindAllQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var records = await AsRecord(_context.Options).ToListAsync(cancellationToken);
            
            return ModelMapper.MapMany(records);
        }

        public async Task<OptionModel> Handle(FindByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var query = AsRecord(_context.Options
                .Where(o => Convert.ToString(o.Id) == request.Id));

            var record = await query.SingleOrDefaultAsync(cancellationToken);

            return ModelMapper.Map(record);
        }

        public async Task<IEnumerable<OptionModel>> Handle(FindByModelIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var query = AsRecord(_context.Options
                .Where(o => Convert.ToString(o.ModelId) == request.ModelId));

            var records = await query.ToListAsync(cancellationToken);
            
            return ModelMapper.MapMany(records);
        }
    }
}
