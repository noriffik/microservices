﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Options
{
    public class FindByIdQuery : IRequest<OptionModel>
    {
        [DataMember]
        public string Id { get; set; }
    }
}
