﻿using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Options
{
    public class OptionModelRecord
    {
        public Option Option { get; set; }

        public OptionCategory Category { get; set; }
    }
}
