﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Options
{
    [DataContract]
    public class FindByPrNumberSetQuery : IRequest<IEnumerable<OptionModel>>
    {
        [DataMember]
        public string ModelId { get; set; }

        [DataMember]
        public string PrNumberSet { get; set; }
    }
}
