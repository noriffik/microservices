﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Options
{
    public class FindByModelIdQuery : IRequest<IEnumerable<OptionModel>>
    {
        [DataMember]
        public string ModelId { get; set; }
    }
}
