﻿using MediatR;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Options
{
    public class FindAllQuery : IRequest<IEnumerable<OptionModel>>
    {
    }
}
