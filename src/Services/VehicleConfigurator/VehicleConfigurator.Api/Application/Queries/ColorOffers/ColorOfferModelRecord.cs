﻿using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers
{
    public class ColorOfferModelRecord
    {
        public ColorOffer ColorOffer { get; set; }

        public ColorModelRecord Color { get; set; }
    }
}
