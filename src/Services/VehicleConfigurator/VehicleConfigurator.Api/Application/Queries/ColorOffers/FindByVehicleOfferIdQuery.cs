﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers
{
    [DataContract]
    public class FindByVehicleOfferIdQuery : IRequest<IEnumerable<ColorOfferModel>>
    {
        [DataMember]
        public int VehicleOfferId { get; set; }
    }
}
