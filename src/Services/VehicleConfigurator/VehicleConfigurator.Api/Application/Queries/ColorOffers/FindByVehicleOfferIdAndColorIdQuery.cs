﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers
{
    [DataContract]
    public class FindByVehicleOfferIdAndColorIdQuery : IRequest<ColorOfferModel>
    {
        [DataMember]
        public int VehicleOfferId { get; set; }

        [DataMember]
        public string ColorId { get; set; }
    }
}
