﻿using AutoMapper;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ColorOfferModelRecord, ColorOfferModel>()
                .ForMember(d => d.Id, s => s.MapFrom(c => c.ColorOffer.Id))
                .ForMember(d => d.Relevance, s => s.MapFrom(c => c.ColorOffer.Relevance))
                .ForMember(d => d.RelevancePeriod, s => s.MapFrom(c => c.ColorOffer.RelevancePeriod))
                .ForMember(d => d.Price, s => s.MapFrom(c => c.ColorOffer.Price));
        }
    }
}
