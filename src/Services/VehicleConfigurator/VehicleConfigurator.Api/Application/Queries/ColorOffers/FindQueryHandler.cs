﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers
{
    public class FindQueryHandler : IRequestHandler<FindByVehicleOfferIdQuery, IEnumerable<ColorOfferModel>>,
        IRequestHandler<FindByVehicleOfferIdAndColorIdQuery, ColorOfferModel>
    {
        private readonly VehicleConfiguratorContext _context;
        private readonly IMapper _mapper;

        public FindQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        private IQueryable<ColorOfferModelRecord> GetRecords =>
            from colorOffer in _context.ColorOffers
            join color in _context.Colors on Convert.ToString(colorOffer.ColorId) equals Convert.ToString(color.Id)
            join colorType in _context.ColorTypes on Convert.ToString(color.TypeId) equals Convert.ToString(colorType.Id)
            select new ColorOfferModelRecord
            {
                ColorOffer = colorOffer,
                Color = new ColorModelRecord
                {
                    Color = color,
                    Type = colorType
                }
            };

        public async Task<IEnumerable<ColorOfferModel>> Handle(FindByVehicleOfferIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await GetRecords
                .Where(c => EF.Property<int>(c.ColorOffer, "VehicleOfferId") == request.VehicleOfferId)
                .ToListAsync(cancellationToken);

            return _mapper.Map<IEnumerable<ColorOfferModel>>(result);
        }


        public async Task<ColorOfferModel> Handle(FindByVehicleOfferIdAndColorIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await GetRecords
                .Where(c => EF.Property<int>(c.ColorOffer, "VehicleOfferId") == request.VehicleOfferId)
                .SingleOrDefaultAsync(c => Convert.ToString(c.ColorOffer.ColorId) == request.ColorId, cancellationToken);

            return _mapper.Map<ColorOfferModel>(result);
        }
    }
}
