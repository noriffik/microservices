﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Packages
{
    [DataContract]
    public class ByCatalogIdQuery : IRequest<IEnumerable<PackageModel>>
    {
        [DataMember]
        public int CatalogId { get; set; }
    }
}
