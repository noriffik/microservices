﻿using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Packages
{
    public class PackageModelRecord
    {
        public Package Package { get; set; }

        public IEnumerable<ApplicabilitySpecificationModelRecord> Applicability { get; set; }
    }
}
