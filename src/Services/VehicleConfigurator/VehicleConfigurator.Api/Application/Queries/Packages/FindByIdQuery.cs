﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Packages
{
    [DataContract]
    public class FindByIdQuery : IRequest<PackageModel>
    {
        [DataMember]
        public string Id { get; set; }
    }
}
