﻿using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Packages
{
    [DataContract]
    public class ApplicabilitySpecificationModel
    {
        [DataMember]
        public ApplicabilitySpecificationType Type { get; set; }

        [DataMember]
        public IEnumerable<OptionModel> Options { get; set; }
    }
}
