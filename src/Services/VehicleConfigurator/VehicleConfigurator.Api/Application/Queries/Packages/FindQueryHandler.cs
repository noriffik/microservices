﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Packages
{
    public class FindQueryHandler : IRequestHandler<ByCatalogIdQuery, IEnumerable<PackageModel>>,
        IRequestHandler<FindByIdQuery, PackageModel>
    {
        private readonly VehicleConfiguratorContext _context;
        private readonly IMapper _mapper;

        public FindQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<PackageModel> Handle(FindByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            var package = await _context.Packages
                .Where(p => Convert.ToString(p.Id) == request.Id)
                .SingleOrDefaultAsync(cancellationToken);

            if (package == null)
                return null;

            var options = await GetOptions(new [] {package}, cancellationToken);

            return _mapper.Map<PackageModel>(CreateRecord(package, options));
        }

        public async Task<IEnumerable<PackageModel>> Handle(ByCatalogIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var packages = await GetPackages(request, cancellationToken);
            var options = await GetOptions(packages, cancellationToken);

            return packages.Select(package => CreateRecord(package, options)).Select(_mapper.Map<PackageModel>);
        }

        private async Task<List<Package>> GetPackages(ByCatalogIdQuery request, CancellationToken cancellationToken)
        {
            var query = from package in _context.Packages
                        where package.CatalogId == request.CatalogId
                        select package;

            return await query.ToListAsync(cancellationToken);
        }

        private async Task<List<OptionModelRecord>> GetOptions(IReadOnlyCollection<Package> packages,
            CancellationToken cancellationToken)
        {
            var ids = GetOptionIdsForPackageList(packages);

            var query = from option in _context.Options
                        where ids.Contains(option.Id.Value)
                        join categories in _context.OptionCategories on option.OptionCategoryId
                            equals categories.Id into categoryJoin
                        from category in categoryJoin.DefaultIfEmpty()
                        select new OptionModelRecord
                        {
                            Option = option,
                            Category = category
                        };

            return await query.ToListAsync(cancellationToken);
        }

        private static IEnumerable<string> GetOptionIdsForPackageList(IReadOnlyCollection<Package> packages)
        {
            return packages
                .SelectMany(p => OptionId.Next(p.Id.ModelId, p.Applicability.SelectMany(s => s.PrNumbers.Values)))
                .Select(id => id.Value);
        }

        private static PackageModelRecord CreateRecord(Package package, List<OptionModelRecord> options)
        {
            return new PackageModelRecord
            {
                Package = package,
                Applicability = package.Applicability.Select(s => new ApplicabilitySpecificationModelRecord
                {
                    Specification = s,
                    Options = options
                        .Where(o => o.Option.ModelId == package.Id.ModelId)
                        .Where(o => s.PrNumbers.Values.Contains(o.Option.PrNumber))
                })
            };
        }
    }
}
