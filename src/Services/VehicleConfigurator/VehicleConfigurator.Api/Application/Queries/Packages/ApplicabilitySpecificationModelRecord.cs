﻿using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Packages
{
    public class ApplicabilitySpecificationModelRecord
    {
        public IApplicabilitySpecification Specification { get; set; }

        public IEnumerable<OptionModelRecord> Options { get; set; }
    }
}