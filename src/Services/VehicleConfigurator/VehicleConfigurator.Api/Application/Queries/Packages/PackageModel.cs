﻿using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Packages
{
    [DataContract]
    public class PackageModel
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public PackageStatus Status { get; set; }

        [DataMember]
        public int CatalogId { get; set; }

        [DataMember]
        public string ModelKeys { get; set; }

        [DataMember]
        public IEnumerable<ApplicabilitySpecificationModel> Applicability { get; set; }
    }
}
