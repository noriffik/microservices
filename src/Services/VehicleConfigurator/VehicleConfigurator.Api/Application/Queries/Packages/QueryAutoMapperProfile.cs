﻿using AutoMapper;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Packages
{
    public class QueryAutoMapperProfile : Profile
    {
        public QueryAutoMapperProfile()
        {
            CreateMap<PackageModelRecord, PackageModel>()
                .ForMember(d => d.Id, s => s.MapFrom(p => p.Package.Id.Value))
                .ForMember(d => d.CatalogId, s => s.MapFrom(p => p.Package.CatalogId))
                .ForMember(d => d.ModelKeys, s => s.MapFrom(p => p.Package.ModelKeySet.AsString))
                .ForMember(d => d.Name, s => s.MapFrom(p => p.Package.Name))
                .ForMember(d => d.Price, s => s.MapFrom(p => p.Package.Price))
                .ForMember(d => d.Status, s => s.MapFrom(p => p.Package.Status));

            CreateMap<ApplicabilitySpecificationModelRecord, ApplicabilitySpecificationModel>()
                .ForMember(d => d.Type, s => s.MapFrom(p => p.Specification.Type));

            CreateMap<OptionModelRecord, OptionModel>()
                .ForMember(d => d.PrNumber, s => s.MapFrom(p => p.Option.PrNumber))
                .ForMember(d => d.Id, s => s.MapFrom(p => p.Option.Id))
                .ForMember(d => d.ModelId, s => s.MapFrom(p => p.Option.ModelId))
                .ForMember(d => d.Name, s => s.MapFrom(p => p.Option.Name));
        }
    }
}
