﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing;
using System;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Offers
{
    [DataContract]
    public class OfferModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int ModelYear { get; set; }

        [DataMember]
        public string PackageId { get; set; }

        [DataMember]
        public int? PrivateCustomerId { get; set; }

        [DataMember]
        public PrNumberSet Options { get; set; }

        [DataMember]
        public PrNumberSet IncludedOptions { get; set; }

        [DataMember]
        public ConfigurationPricingModel Price { get; set; }

        [DataMember]
        public DateTime CreatedAt { get; set; }

        [DataMember]
        public string ModelKey { get; set; }

        [DataMember]
        public string ColorId { get; set; }
    }
}
