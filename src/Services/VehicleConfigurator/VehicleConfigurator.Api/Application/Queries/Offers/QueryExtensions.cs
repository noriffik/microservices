﻿using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.Infrastructure.Views;
using System.Linq;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Offers
{
    public static class QueryExtensions
    {
        public static IQueryable<object> JoinRelated(this IQueryable<Offer> source, VehicleConfiguratorContext context)
        {
            return from offer in source
                   join vehicle in context.Query<OfferVehicleView>() on offer.Id equals vehicle.OfferId
                   select new
                   {
                       offer,
                       vehicle
                   };
        }
    }
}
