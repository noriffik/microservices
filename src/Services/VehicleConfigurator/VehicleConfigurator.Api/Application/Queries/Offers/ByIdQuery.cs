﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Offers
{
    [DataContract]
    public class ByIdQuery : IRequest<OfferVehicleModel>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
