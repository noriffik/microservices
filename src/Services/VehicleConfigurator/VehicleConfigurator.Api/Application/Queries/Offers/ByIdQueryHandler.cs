﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Offers
{
    public class ByIdQueryHandler : IRequestHandler<ByIdQuery, OfferVehicleModel>
    {
        private readonly IMapper _mapper;
        private readonly VehicleConfiguratorContext _context;

        public ByIdQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<OfferVehicleModel> Handle(ByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var offer = await _context.Offers
                .Where(o => o.Id == request.Id)
                .JoinRelated(_context)
                .SingleOrDefaultAsync(cancellationToken);

            return _mapper.Map<OfferVehicleModel>(offer);
        }
    }
}
