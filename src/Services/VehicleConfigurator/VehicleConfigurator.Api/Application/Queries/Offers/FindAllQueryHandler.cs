﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Offers
{
    public class FindAllQueryHandler : IRequestHandler<FindAllQuery, IEnumerable<OfferVehicleModel>>
    {
        private readonly VehicleConfiguratorContext _context;
        private readonly IMapper _mapper;

        public FindAllQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<OfferVehicleModel>> Handle(FindAllQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var records = await _context.Offers
                .JoinRelated(_context)
                .ToListAsync(cancellationToken);

            return _mapper.Map<IEnumerable<OfferVehicleModel>>(records);
        }
    }
}
