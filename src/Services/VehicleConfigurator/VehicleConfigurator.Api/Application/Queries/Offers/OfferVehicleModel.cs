﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Offers
{
    [DataContract]
    public class OfferVehicleModel
    {
        [DataMember]
        public OfferModel Offer { get; set; }

        [DataMember]
        public VehicleModel Vehicle { get; set; }
    }
}
