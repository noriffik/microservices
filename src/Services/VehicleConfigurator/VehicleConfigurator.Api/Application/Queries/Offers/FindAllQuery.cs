﻿using MediatR;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Offers
{
    public class FindAllQuery : IRequest<IEnumerable<OfferVehicleModel>>
    {
    }
}
