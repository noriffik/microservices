﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs
{
    public class CatalogModelMapper : BaseMapper<Catalog, CatalogModel>
    {
        private IPeriodModelMapper _periodMapper;

        public IPeriodModelMapper PeriodMapper
        {
            get => _periodMapper ?? new PeriodModelMapper();
            set => _periodMapper = value ?? throw new ArgumentNullException();
        }

        public override CatalogModel Map(Catalog source)
        {
            return source != null
                ? new CatalogModel
                {
                    Id = source.Id,
                    Status = source.Status,
                    RelevancePeriod = PeriodMapper.Map(source.RelevancePeriod),
                    Year = source.ModelYear.Value
                }
                : null;
        }
    }
}
