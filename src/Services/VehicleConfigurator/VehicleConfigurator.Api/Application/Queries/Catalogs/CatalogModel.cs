﻿using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs
{
    [DataContract]
    public class CatalogModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public PeriodModel RelevancePeriod { get; set; }

        [DataMember]
        public CatalogStatus Status { get; set; }

        [DataMember]
        public int Year { get; set; }
    }
}
