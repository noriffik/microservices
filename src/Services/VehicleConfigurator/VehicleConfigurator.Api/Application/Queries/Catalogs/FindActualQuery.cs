﻿using MediatR;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs
{
    [DataContract]
    public class FindActualQuery : IRequest<CatalogModel>
    {
        [DataMember]
        public int ModelYear { get; set; }

        [DataMember]
        public CatalogStatus Status { get; set; }
    }
}
