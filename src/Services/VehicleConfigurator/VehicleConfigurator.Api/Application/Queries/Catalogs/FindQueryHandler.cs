﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Common;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs
{
    public class FindQueryHandler : IRequestHandler<FindByIdQuery, CatalogModel>,
        IRequestHandler<FindActualQuery, CatalogModel>,
        IRequestHandler<FindAllQuery, IEnumerable<CatalogModel>>,
        IRequestHandler<FindOutdatedQuery, IEnumerable<CatalogModel>>
    {
        private readonly VehicleConfiguratorContext _context;
        private IMapper<Catalog, CatalogModel> _mapper;

        public IMapper<Catalog, CatalogModel> ModelMapper
        {
            get => _mapper ?? (_mapper = new CatalogModelMapper());
            set => _mapper = value ?? throw new ArgumentNullException();
        }

        public FindQueryHandler(VehicleConfiguratorContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<CatalogModel> Handle(FindByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var catalog = await _context.Catalogs
                .FindAsync(new object[] {request.Id}, cancellationToken);

            return ModelMapper.Map(catalog);
        }

        public async Task<CatalogModel> Handle(FindActualQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var now = SystemTimeProvider.Instance.Get();

            var catalog = await _context.Catalogs
                .Where(c => Convert.ToInt32(c.ModelYear) == request.ModelYear)
                .Where(c => c.Status == request.Status)
                .Where(c => c.RelevancePeriod.From <= now && c.RelevancePeriod.To >= now)
                .FirstOrDefaultAsync(cancellationToken);

            return ModelMapper.Map(catalog);
        }

        public async Task<IEnumerable<CatalogModel>> Handle(FindAllQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return ModelMapper.MapMany(
                await _context.Catalogs
                    .OrderBy(c => c.RelevancePeriod.From)
                    .ThenBy(c => Convert.ToInt32(c.ModelYear))
                    .ToListAsync(cancellationToken));
        }

        public async Task<IEnumerable<CatalogModel>> Handle(FindOutdatedQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return ModelMapper.MapMany(
                await _context.Catalogs
                    .Where(c => c.RelevancePeriod.To < SystemTimeProvider.Instance.Get())
                    .OrderBy(c => Convert.ToInt32(c.ModelYear))
                    .ToListAsync(cancellationToken));
        }
    }
}
