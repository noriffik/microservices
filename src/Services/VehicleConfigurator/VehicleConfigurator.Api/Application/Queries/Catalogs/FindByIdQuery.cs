﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs
{
    [DataContract]
    public class FindByIdQuery : IRequest<CatalogModel>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
