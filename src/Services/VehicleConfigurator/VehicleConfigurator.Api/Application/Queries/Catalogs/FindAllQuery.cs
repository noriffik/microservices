﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs
{
    [DataContract]
    public class FindAllQuery : IRequest<IEnumerable<CatalogModel>>
    {
    }
}
