﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Colors
{
    [DataContract]
    public class FindByIdQuery : IRequest<ColorModel>
    {
        [DataMember]
        public string Id { get; set; }
    }
}
