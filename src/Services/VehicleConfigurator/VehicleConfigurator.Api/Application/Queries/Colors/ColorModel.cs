﻿using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Colors
{
    [DataContract]
    public class ColorModel
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Argb { get; set; }

        [DataMember]
        public ColorTypeModel Type { get; set; }
    }
}
