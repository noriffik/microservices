﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Colors
{
    public class FindQueryHandler : IRequestHandler<FindByIdQuery, ColorModel>,
        IRequestHandler<FindAllQuery, IEnumerable<ColorModel>>
    {
        private readonly VehicleConfiguratorContext _context;
        private readonly IMapper _mapper;

        public FindQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<ColorModel> Handle(FindByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await GetRecords
                .Where(r => Convert.ToString(r.Color.Id) == request.Id)
                .SingleOrDefaultAsync(cancellationToken);

            return _mapper.Map<ColorModel>(result);
        }

        private IQueryable<ColorModelRecord> GetRecords => from color in _context.Colors
            join colorType in _context.ColorTypes on color.TypeId equals colorType.Id
            select new ColorModelRecord
            {
                Color = color,
                Type = colorType
            };

        public async Task<IEnumerable<ColorModel>> Handle(FindAllQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await GetRecords.ToListAsync(cancellationToken);

            return _mapper.Map<IEnumerable<ColorModel>>(result);
        }
    }
}
