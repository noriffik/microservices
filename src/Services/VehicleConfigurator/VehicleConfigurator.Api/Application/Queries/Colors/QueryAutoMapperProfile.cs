﻿using AutoMapper;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Colors
{
    public class QueryAutoMapperProfile : Profile
    {
        public QueryAutoMapperProfile()
        {
            CreateMap<ColorModelRecord, ColorModel>()
                .ForMember(d => d.Id, s => s.MapFrom(c => c.Color.Id))
                .ForMember(d => d.Name, s => s.MapFrom(c => c.Color.Name))
                .ForMember(d => d.Argb, s => s.MapFrom(c => c.Color.Argb));

            CreateMap<ColorId, string>()
                .ConstructUsing(id => id.Value);

            CreateMap<System.Drawing.Color, string>()
                .ConstructUsing(c => c.ToArgb().ToString("X8"));
        }
    }
}
