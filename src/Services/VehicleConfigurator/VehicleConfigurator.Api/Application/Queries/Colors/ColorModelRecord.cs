﻿using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Colors
{
    public class ColorModelRecord
    {
        public Color Color { get; set; }

        public ColorType Type { get; set; }
    }
}
