﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.VehicleConfigurator.Api.Application.Queries
{
    public static class QueryableExtensions
    {
        public class Join<TOuter, TInner>
        {
            public TOuter Outer { get; set; }
            public TInner Inner { get; set; }
        }

        public class GroupJoin<TOuter, TInner>
        {
            public TOuter Outer { get; set; }
            public IEnumerable<TInner> Inner { get; set; }
        }

        public static IQueryable<TResult> LeftJoin<TOuter, TInner, TKey, TResult>(this IQueryable<TOuter> query,
            IEnumerable<TInner> inner,
            Expression<Func<TOuter, TKey>> outerKeySelector,
            Expression<Func<TInner, TKey>> innerKeySelector,
            Expression<Func<GroupJoin<TOuter, TInner>, TInner, TResult>> resultSelector)
        {
            return query.GroupJoin(inner,
                    outerKeySelector,
                    innerKeySelector,
                    (o, i) => new GroupJoin<TOuter, TInner> { Outer = o, Inner = i })
                .SelectMany(_ => _.Inner.DefaultIfEmpty(),
                    resultSelector);
        }
    }
}
