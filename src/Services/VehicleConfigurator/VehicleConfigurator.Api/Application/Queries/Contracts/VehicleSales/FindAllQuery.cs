﻿using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts.VehicleSales;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Contracts.VehicleSales
{
    [DataContract]
    public class FindAllQuery : PagedRequest<VehicleSaleContractModel>
    {
        [FromQuery]
        [DataMember]
        public string DealerId { get; set; }

        [FromQuery]
        [DataMember]
        public int? PrivateCustomerId { get; set; }
    }
}
