﻿using AutoMapper;
using MediatR;
using NexCore.Application.Extensions;
using NexCore.Application.Queries;
using NexCore.Infrastructure.QueryHandlers;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Contracts.VehicleSales
{
    public class FindQueryHandler: IRequestHandler<FindAllQuery, PagedResponse<VehicleSaleContractModel>>
    {
        private readonly IMapper _mapper;
        private readonly VehicleConfiguratorContext _context;

        public FindQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<PagedResponse<VehicleSaleContractModel>> Handle(FindAllQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await _context.VehicleSaleContracts
                .Where(r => request.DealerId == null || request.DealerId == string.Empty || Convert.ToString(r.DealerId) == request.DealerId)
                .Where(r => request.PrivateCustomerId == null || r.PrivateCustomerId == request.PrivateCustomerId)
                .Paginate(request, cancellationToken);

            return _mapper.MapPagedResponse<VehicleSaleContract, VehicleSaleContractModel>(result);
        }
    }
}
