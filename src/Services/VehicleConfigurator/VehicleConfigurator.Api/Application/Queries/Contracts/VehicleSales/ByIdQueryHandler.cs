﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Contracts.VehicleSales
{
    public class ByIdQueryHandler : IRequestHandler<ByIdQuery, VehicleSaleContractModel>
    {
        private readonly IMapper _mapper;
        private readonly VehicleConfiguratorContext _context;

        public ByIdQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<VehicleSaleContractModel> Handle(ByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var contract = await _context.VehicleSaleContracts
                .SingleOrDefaultAsync(o => o.Id == request.Id, cancellationToken);

            return _mapper.Map<VehicleSaleContractModel>(contract);
        }
    }
}
