﻿using MediatR;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts.VehicleSales;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Contracts.VehicleSales
{
    [DataContract]
    public class ByIdQuery : IRequest<VehicleSaleContractModel>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
