﻿namespace NexCore.VehicleConfigurator.Api.Application.Queries.Sort
{
    public abstract class SortStrategyFactory<TEntityType> where TEntityType : class
    {
        public abstract ISortStrategy<TEntityType> Create(int? criterion);
    }
}
