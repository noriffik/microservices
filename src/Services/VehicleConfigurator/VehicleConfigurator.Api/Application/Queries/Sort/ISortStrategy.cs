﻿using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.Sort
{
    public interface ISortStrategy<TEntityModel> where TEntityModel : class
    {
        IEnumerable<TEntityModel> Sort(IEnumerable<TEntityModel> sequence);
    }
}