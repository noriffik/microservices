﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes
{
    [DataContract]
    public class ColorTypeModel
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
