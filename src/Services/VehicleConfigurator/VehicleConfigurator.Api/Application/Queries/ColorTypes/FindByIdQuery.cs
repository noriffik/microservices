﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes
{
    [DataContract]
    public class FindByIdQuery : IRequest<ColorTypeModel>
    {
        [DataMember]
        public string Id { get; set; }
    }
}
