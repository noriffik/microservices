﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes
{
    public class FindQueryHandler : IRequestHandler<FindByIdQuery, ColorTypeModel>,
        IRequestHandler<FindAllQuery, IEnumerable<ColorTypeModel>>
    {
        private readonly VehicleConfiguratorContext _context;
        private readonly IMapper _mapper;

        public FindQueryHandler(VehicleConfiguratorContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<ColorTypeModel> Handle(FindByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var colorType = await _context.ColorTypes
                .SingleOrDefaultAsync(c => Convert.ToString(c.Id) == request.Id, cancellationToken);

            return _mapper.Map<ColorTypeModel>(colorType);
        }

        public async Task<IEnumerable<ColorTypeModel>> Handle(FindAllQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var colorTypes = await _context.ColorTypes
                .ToListAsync(cancellationToken);

            return _mapper.Map<IEnumerable<ColorTypeModel>>(colorTypes);
        }
    }
}
