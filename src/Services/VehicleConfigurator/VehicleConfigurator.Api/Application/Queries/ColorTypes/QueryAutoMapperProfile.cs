﻿using AutoMapper;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes
{
    public class QueryAutoMapperProfile : Profile
    {
        public QueryAutoMapperProfile()
        {
            CreateMap<ColorType, ColorTypeModel>();
        }
    }
}
