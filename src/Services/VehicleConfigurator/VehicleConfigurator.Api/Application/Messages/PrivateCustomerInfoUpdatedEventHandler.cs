﻿using AutoMapper;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Domain.Customers;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Messages
{
    public class PrivateCustomerInfoUpdatedEventHandler : IIntegrationEventHandler<DealerPrivateCustomerInfoUpdatedEvent>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public PrivateCustomerInfoUpdatedEventHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task Handle(DealerPrivateCustomerInfoUpdatedEvent @event)
        {
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));

            var privateCustomer = await _work.EntityRepository.SingleOrDefault<PrivateCustomer>(
                @event.DealerPrivateCustomerId);

            if (privateCustomer == null)
                return;

            var name = _mapper.Map<PersonName>(@event.PersonName);
            var passport = @event.Passport == null
                ? Passport.Empty
                : _mapper.Map<Passport>(@event.Passport);
            var taxNumber = @event.TaxNumber == null
                ? TaxIdentificationNumber.Empty
                : _mapper.Map<TaxIdentificationNumber>(@event.TaxNumber);

            privateCustomer.AssignName(name);
            privateCustomer.AssignPassport(passport);
            privateCustomer.AssignTaxIdentificationNumber(taxNumber);

            await _work.Commit();
        }
    }
}
