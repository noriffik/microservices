﻿namespace NexCore.VehicleConfigurator.Api.Application.Messages.Dtos
{
    public class PersonNameDto
    {
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Middlename { get; set; }
    }
}
