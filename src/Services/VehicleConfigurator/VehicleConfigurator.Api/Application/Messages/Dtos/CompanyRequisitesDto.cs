﻿using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Api.Application.Messages.Dtos
{
    public class CompanyRequisitesDto
    {
        public string ShortName { get; set; }

        public string FullName { get; set; }

        public string Address { get; set; }

        public IEnumerable<string> Telephones { get; set; }

        public string BankRequisites { get; set; }

        public PersonNameDto Director { get; set; }
    }
}
