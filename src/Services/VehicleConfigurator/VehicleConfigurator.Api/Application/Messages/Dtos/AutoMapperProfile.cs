﻿using AutoMapper;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Domain.Customers;

namespace NexCore.VehicleConfigurator.Api.Application.Messages.Dtos
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<PassportDto, Passport>()
                .ConvertUsing(s => PassportMap(s));

            CreateMap<TaxIdentificationNumberDto, TaxIdentificationNumber>()
                .ConvertUsing(s => TaxMap(s));

            CreateMap<DealerPrivateCustomerAddedEvent, PrivateCustomer>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.DealerPrivateCustomerId))
                .ForMember(d => d.PersonName, s => s.MapFrom(src => new PersonName(src.FirstName, src.LastName, src.MiddleName)));
        }

        private static TaxIdentificationNumber TaxMap(TaxIdentificationNumberDto source)
        {
            return source == null
                ? TaxIdentificationNumber.Empty
                : new TaxIdentificationNumber(source.Number, source.RegisteredOn, new Issue(source.Issue.Issuer, source.Issue.IssuedOn));
        }

        private static Passport PassportMap(PassportDto source)
        {
            return source == null
                ? Passport.Empty
                : new Passport(source.Series, source.Number, new Issue(source.Issuer, source.IssuedOn));
        }
    }
}
