﻿using AutoMapper;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using NexCore.VehicleConfigurator.Domain.Dealers;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Messages
{
    public class DealerAddedEventHandler : IIntegrationEventHandler<DealerAddedEvent>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public DealerAddedEventHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task Handle(DealerAddedEvent @event)
        {
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));

            var dealer = _mapper.Map<Dealer>(@event);

            var existDealer = await _work.EntityRepository.SingleOrDefault<Dealer, DealerId>(dealer.Id);

            if (existDealer == null)
                await _work.EntityRepository.Add<Dealer, DealerId>(dealer);
            else
            {
                existDealer.CompanyId = dealer.CompanyId;
                existDealer.ChangeRequisites(dealer.Requisites);
            }

            await _work.Commit(CancellationToken.None);
        }
    }
}
