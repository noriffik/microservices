﻿using AutoMapper;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Domain.Dealers;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Messages
{
    public class DealerEmployeeNameUpdatedEventHandler : IIntegrationEventHandler<DealerEmployeeNameUpdatedEvent>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public DealerEmployeeNameUpdatedEventHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task Handle(DealerEmployeeNameUpdatedEvent @event)
        {
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));

            var employee = await _work.EntityRepository.SingleOrDefault<Employee>(@event.DealerEmployeeId);

            if (employee == null)
                return;

            employee.ChangeName(_mapper.Map<PersonName>(@event.PersonName));

            await _work.Commit(CancellationToken.None);
        }
    }
}
