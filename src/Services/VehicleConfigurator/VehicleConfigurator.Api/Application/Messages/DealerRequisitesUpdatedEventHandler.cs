﻿using AutoMapper;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Domain.Dealers;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Messages
{
    public class DealerRequisitesUpdatedEventHandler : IIntegrationEventHandler<DealerRequisitesUpdatedEvent>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public DealerRequisitesUpdatedEventHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task Handle(DealerRequisitesUpdatedEvent @event)
        {
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));

            var dealer = await _work.EntityRepository.SingleOrDefault<Dealer, DealerId>(DealerId.Parse(@event.DealerId));

            if (dealer == null)
                return;

            dealer.ChangeRequisites(_mapper.Map<OrganizationRequisites>(@event.Requisites));

            await _work.Commit(CancellationToken.None);
        }
    }
}
