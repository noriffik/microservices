﻿using AutoMapper;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using NexCore.VehicleConfigurator.Domain.Customers;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.Domain.Dealers.Specifications;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Messages
{
    public class DealerPrivateCustomerAddedEventHandler : IIntegrationEventHandler<DealerPrivateCustomerAddedEvent>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public DealerPrivateCustomerAddedEventHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task Handle(DealerPrivateCustomerAddedEvent @event)
        {
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));

            if (!await _work.EntityRepository.Has<Dealer, DealerId>(new DealerByCompanyIdSpecification(@event.OrganizationId), CancellationToken.None))
                return;

            var privateCustomer = await
                _work.EntityRepository.SingleOrDefault<PrivateCustomer>(@event.DealerPrivateCustomerId,
                    CancellationToken.None);

            var mapped = _mapper.Map<PrivateCustomer>(@event);

            if (privateCustomer == null)
                await _work.EntityRepository.Add(mapped);
            else
            {
                privateCustomer.AssignName(mapped.PersonName);
                privateCustomer.AssignPassport(mapped.Passport);
                privateCustomer.AssignTaxIdentificationNumber(mapped.TaxIdentificationNumber);
                privateCustomer.PhysicalAddress = mapped.PhysicalAddress;
                privateCustomer.Telephone = mapped.Telephone;
            }

            await _work.Commit(CancellationToken.None);
        }
    }
}
