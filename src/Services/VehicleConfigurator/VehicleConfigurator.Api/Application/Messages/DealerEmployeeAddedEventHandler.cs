﻿using AutoMapper;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using NexCore.VehicleConfigurator.Domain.Dealers;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Application.Messages
{
    public class DealerEmployeeAddedEventHandler : IIntegrationEventHandler<DealerEmployeeAddedEvent>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public DealerEmployeeAddedEventHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task Handle(DealerEmployeeAddedEvent @event)
        {
            if (@event == null) throw new ArgumentNullException(nameof(@event));

            var employee = _mapper.Map<Employee>(@event);

            var isDealerExists = await _work.EntityRepository.Has<Dealer, DealerId>(employee.DealerId);
            if (!isDealerExists) return;

            var isEmployeeExists = await _work.EntityRepository.Has<Employee>(employee.Id);

            if (!isEmployeeExists)
            {
                await _work.EntityRepository.Add<Employee>(employee);

                await _work.Commit(CancellationToken.None);
            }
                
        }
    }
}
