﻿using NexCore.EventBus.Abstract;
using NexCore.VehicleConfigurator.Api.Application.Messages.Dtos;

namespace NexCore.VehicleConfigurator.Api.Application.Messages
{
    public class DealerPrivateCustomerInfoUpdatedEvent : IntegrationEvent
    {
        public int DealerPrivateCustomerId { get; set; }

        public PersonNameDto PersonName { get; set; }

        public PassportDto Passport { get; set; }

        public TaxIdentificationNumberDto TaxNumber { get; set; }
    }
}
