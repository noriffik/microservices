﻿using NexCore.EventBus.Abstract;
using NexCore.VehicleConfigurator.Api.Application.Messages.Dtos;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Messages
{
    public class DealerRequisitesUpdatedEvent : IntegrationEvent
    {
        public DealerRequisitesUpdatedEvent()
        {
        }

        public DealerRequisitesUpdatedEvent(Guid id, DateTime createdAt) : base(id, createdAt)
        {
        }

        public string DealerId { get; set; }

        public CompanyRequisitesDto Requisites { get; set; }
    }
}
