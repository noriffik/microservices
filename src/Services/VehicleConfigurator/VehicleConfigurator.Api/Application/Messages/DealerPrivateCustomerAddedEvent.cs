﻿using NexCore.EventBus.Abstract;
using NexCore.VehicleConfigurator.Api.Application.Messages.Dtos;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Api.Application.Messages
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Required for messages broker")]
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global", Justification = "Required for message broker")]
    public class DealerPrivateCustomerAddedEvent : IntegrationEvent
    {
        public int DealerPrivateCustomerId { get; set; }

        public int OrganizationId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string PhysicalAddress { get; set; }

        public string Telephone { get; set; }

        public PassportDto Passport { get; set; }

        public TaxIdentificationNumberDto TaxIdentificationNumber { get; set; }
    }
}
