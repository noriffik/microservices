﻿using NexCore.EventBus.Abstract;
using NexCore.VehicleConfigurator.Api.Application.Messages.Dtos;
using System;

namespace NexCore.VehicleConfigurator.Api.Application.Messages
{
    public class DealerEmployeeNameUpdatedEvent : IntegrationEvent
    {
        public DealerEmployeeNameUpdatedEvent()
        {
        }

        public DealerEmployeeNameUpdatedEvent(Guid id, DateTime createdAt) : base(id, createdAt)
        {
        }

        public int DealerEmployeeId { get; set; }

        public PersonNameDto PersonName { get; set; }
    }
}
