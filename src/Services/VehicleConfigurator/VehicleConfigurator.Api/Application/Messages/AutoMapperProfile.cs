﻿using AutoMapper;
using NexCore.DealerNetwork;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Api.Application.Messages.Dtos;
using NexCore.VehicleConfigurator.Domain.Dealers;

namespace NexCore.VehicleConfigurator.Api.Application.Messages
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<DealerAddedEvent, Dealer>()
                .ConvertUsing(s => new Dealer(DealerId.Parse(s.DealerId), s.OrganizationId,
                    s.Requisites == null ? OrganizationRequisites.Empty : MapCompanyRequisites(s.Requisites)));

            CreateMap<CompanyRequisitesDto, OrganizationRequisites>()
                .ConvertUsing(s => MapCompanyRequisites(s));

            CreateMap<DealerEmployeeAddedEvent, Employee>()
                .ConvertUsing(s => new Employee(s.EmployeeId, new PersonName(s.FirstName, s.LastName, s.MiddleName), DealerId.Parse(s.DealerId)));
        }

        private static OrganizationRequisites MapCompanyRequisites(CompanyRequisitesDto source)
        {
            if (source == null)
                return null;

            var name = new LegalOrganizationName(source.ShortName, source.FullName);
            var director = new LegalPersonName(source.Director.Firstname, source.Director.Lastname, source.Director.Middlename);

            return new OrganizationRequisites(name, source.Address, source.Telephones, source.BankRequisites, director);
        }
    }
}
