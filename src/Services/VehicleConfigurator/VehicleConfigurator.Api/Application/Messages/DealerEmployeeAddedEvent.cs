﻿using NexCore.EventBus.Abstract;

namespace NexCore.VehicleConfigurator.Api.Application.Messages
{
    public class DealerEmployeeAddedEvent : IntegrationEvent
    {
        public string DealerId { get; set; }

        public int EmployeeId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }
    }
}
