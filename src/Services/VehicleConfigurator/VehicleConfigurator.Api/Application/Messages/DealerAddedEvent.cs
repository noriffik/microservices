﻿using NexCore.EventBus.Abstract;
using NexCore.VehicleConfigurator.Api.Application.Messages.Dtos;

namespace NexCore.VehicleConfigurator.Api.Application.Messages
{
    public class DealerAddedEvent : IntegrationEvent
    {
        public string DealerId { get; set; }

        public int OrganizationId { get; set; }

        public CompanyRequisitesDto Requisites { get; set; }
    }
}
