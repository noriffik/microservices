﻿using NexCore.Infrastructure.Seeds;
using NexCore.VehicleConfigurator.Domain.Contracts;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds
{
    public class ContractTemplateSeeder : ISeeder<ContractTemplate>
    {
        private const string DealerId = "EKK38001";
        private readonly ContractTypeId _contractTypeId = ContractTypeId.Parse("TEST");

        private readonly VehicleConfiguratorContext _dbContext;
        private readonly IBinaryReaderProvider _readerProvider;

        public ContractTemplateSeeder(VehicleConfiguratorContext dbContext, IBinaryReaderProvider readerProvider)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _readerProvider = readerProvider ?? throw new ArgumentNullException(nameof(readerProvider));
        }

        public async Task Seed()
        {
            var content = await _readerProvider.Get();

            var contractTemplate = new ContractTemplate(_contractTypeId, DealerId, content);

            if (IsNotSeeded(contractTemplate))
                await _dbContext.AddAsync(contractTemplate);

            await _dbContext.SaveChangesAsync();
        }

        private bool IsNotSeeded(ContractTemplate contractTemplate)
        {
            return !_dbContext.ContractTemplate
                .Where(v => v.TypeId == contractTemplate.TypeId)
                .Any(v => v.DealerId == contractTemplate.DealerId);
        }
    }
}
