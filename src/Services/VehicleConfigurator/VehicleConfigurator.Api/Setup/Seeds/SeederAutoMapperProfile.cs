﻿using AutoMapper;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Globalization;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds
{
    public class SeederAutoMapperProfile : Profile
    {
        private readonly IApplicabilityParser _applicabilityParser;

        public SeederAutoMapperProfile()
        {
            _applicabilityParser = new ApplicabilityParser();

            CreateMap<ModelSeederDataSource.Record, Model>().ConvertUsing(
                record => new Model(ModelId.Parse(record.Id))
                {
                    Name = record.Name
                });

            CreateMap<BodySeederDataSource.Record, Body>().ConvertUsing(
                record => new Body(BodyId.Parse(record.ModelId + record.Key))
                {
                    Name = record.Name
                });

            CreateMap<EquipmentSeederDataSource.Record, Equipment>().ConvertUsing(
                record => new Equipment(EquipmentId.Parse(record.ModelId + record.Key))
                {
                    Name = record.Name
                });

            CreateMap<EngineSeederDataSource.Record, Engine>().ConvertUsing(
                record => new Engine(EngineId.Parse(record.ModelId + record.Key))
                {
                    Name = record.Name,
                    FuelType = (FuelType)record.FuelType
                });

            CreateMap<GearboxSeederDataSource.Record, Gearbox>().ConvertUsing(
                record => new Gearbox(
                    GearboxId.Parse(record.ModelId + record.Key),
                    GearboxType.Parse(record.Type))
                {
                    Name = record.Name,
                    GearBoxCategory = (GearBoxCategory)record.Category
                });

            CreateMap<CatalogSeederDataSource.Record, Catalog>().ConvertUsing(
                record => new Catalog(new Year(record.Year), new Period(record.From, record.To))
                {
                    Status = record.Status
                });

            CreateMap<VehicleOfferSeederDataSource.Record, VehicleOffer>().ConvertUsing(
                record => new VehicleOffer(ModelKey.Parse(record.ModelKey), record.CatalogId)
                {
                    Price = record.Price
                });

            CreateMap<OptionSeederDataSource.Record, Option>().ConvertUsing(
                record => new Option(OptionId.Parse(record.OptionId))
                {
                    Name = record.Name,
                    OptionCategoryId = record.CategoryId
                });

            CreateMap<OptionCategoryDataSource.Record, OptionCategory>().ConvertUsing(
                record => new OptionCategory(record.Name));

            CreateMap<OptionOfferSeederDataSource.Record, OptionOfferSeederModel>().ConvertUsing(
                record => new OptionOfferSeederModel
                {
                    Availability = record.Type == AvailabilityType.Included
                        ? Availability.Included(record.Reason.Value)
                        : Availability.Purchasable(record.Price.GetValueOrDefault()),
                    ModelKey = ModelKey.Parse(record.ModelKey),
                    CatalogId = record.CatalogId,
                    PrNumber = PrNumber.Parse(record.PrNumber)
                });

            CreateMap<ColorTypeSeederDataSource.Record, ColorType>().ConvertUsing(
                record => new ColorType(ColorTypeId.Parse(record.ColorTypeId), record.Name));

            CreateMap<ColorSeederDataSource.Record, Color>().ConvertUsing(
                    record => new Color(ColorId.Parse(record.ColorId),
                        System.Drawing.Color.FromArgb(int.Parse(record.Argb, NumberStyles.HexNumber)),
                        ColorTypeId.Parse(record.TypeId), record.Name));

            CreateMap<DefaultColorSeederDataSource.Record, DefaultColorRecord>()
                .ConvertUsing(record => new DefaultColorRecord
                {
                    CatalogId = record.CatalogId,
                    ModelKey = ModelKey.Parse(record.ModelKey),
                    ColorId = ColorId.Parse(record.ColorId)
                });

            CreateMap<CategoryPerOptionSeederDataSource.Record, CategoryPerOptionRecord>()
                .ConvertUsing(record => new CategoryPerOptionRecord
                {
                    CategoryId = record.CategoryId,
                    OptionId = OptionId.Parse(record.OptionId)
                });

            CreateMap<PackageSeederDataSource.Record, Package>()
                .ConvertUsing(record => ConvertPackage(record));
        }

        private Package ConvertPackage(PackageSeederDataSource.Record record)
        {
            var package = new Package(PackageId.Parse(record.Id), record.CatalogId, record.Name,
                BoundModelKeySet.Parse(record.ModelKeySet), _applicabilityParser.Parse(record.Applicability))
            {
                Price = record.Price
            };
            package.ChangeStatus(PackageStatus.Published);

            return package;
        }
    }
}
