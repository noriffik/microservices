﻿using NexCore.Infrastructure.Seeds;
using NexCore.VehicleConfigurator.Domain.Contracts;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds
{
    public class ContractTypeSeeder : ISeeder<ContractType>
    {
        private readonly VehicleConfiguratorContext _dbContext;

        public ContractTypeSeeder(VehicleConfiguratorContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public Task Seed()
        {
            var contractType = new ContractType(ContractTypeId.Parse("TEST"), "Test Contract Type");

            if (IsNotSeeded(contractType))
                _dbContext.Add(contractType);

            return _dbContext.SaveChangesAsync();
        }

        private bool IsNotSeeded(ContractType contractType)
        {
            return !_dbContext.ContractType
                .Any(v => v.Id == contractType.Id);
        }
    }
}
