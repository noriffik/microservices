﻿using NexCore.Infrastructure.Seeds;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds
{
    public class PackageSeeder : ISeeder<Package>
    {
        private readonly VehicleConfiguratorContext _dbContext;
        private readonly ISeederDataSource<Package> _dataSource;

        public PackageSeeder(VehicleConfiguratorContext dbContext, ISeederDataSource<Package> dataSource)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _dataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
        }

        public Task Seed()
        {
            _dataSource.Get()
                .Where(IsNotSeeded)
                .ToList()
                .ForEach(m => _dbContext.Add(m));

            return _dbContext.SaveChangesAsync();
        }

        private bool IsNotSeeded(Package package)
        {
            return !_dbContext.Packages
                .Where(v => v.CatalogId == package.CatalogId)
                .Any(v => v.Id == package.Id);
        }
    }
}
