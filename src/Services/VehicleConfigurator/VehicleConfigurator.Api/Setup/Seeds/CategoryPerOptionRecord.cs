﻿using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds
{
    public class CategoryPerOptionRecord
    {
        public OptionId OptionId { get; set; }

        public int CategoryId { get; set; }
    }
}