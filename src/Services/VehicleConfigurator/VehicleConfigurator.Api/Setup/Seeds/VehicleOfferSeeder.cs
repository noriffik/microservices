﻿using NexCore.Infrastructure.Seeds;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds
{
    public class VehicleOfferSeeder : ISeeder<VehicleOffer>
    {
        private readonly VehicleConfiguratorContext _dbContext;
        private readonly ISeederDataSource<VehicleOffer> _dataSource;

        public VehicleOfferSeeder(VehicleConfiguratorContext dbContext, ISeederDataSource<VehicleOffer> dataSource)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _dataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
        }

        public Task Seed()
        {
            _dataSource.Get()
                .Where(IsNotSeeded)
                .ToList()
                .ForEach(m => _dbContext.Add(m));

            return _dbContext.SaveChangesAsync();
        }

        private bool IsNotSeeded(VehicleOffer vehicleOffer)
        {
            return !_dbContext.VehicleOffers
                .Where(v => v.CatalogId == vehicleOffer.CatalogId)
                .Any(v => Convert.ToString(v.ModelKey) == Convert.ToString(vehicleOffer.ModelKey));
        }
    }
}
