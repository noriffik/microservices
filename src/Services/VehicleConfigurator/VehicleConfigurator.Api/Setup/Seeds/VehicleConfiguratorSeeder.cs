﻿using NexCore.Infrastructure.Seeds;
using NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Contracts;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds
{
    public class VehicleConfiguratorSeeder : CompositeSeeder, IDbContextSeeder<VehicleConfiguratorContext>
    {
        [SuppressMessage("ReSharper", "SuggestBaseTypeForParameter", Justification = "Required by Autofac registration")]
        public VehicleConfiguratorSeeder(ISeeder<Model> modelSeeder,
            ISeeder<Body> bodySeeder,
            ISeeder<Equipment> equipmentSeeder,
            ISeeder<Engine> engineSeeder,
            ISeeder<Gearbox> gearboxSeeder,
            ISeeder<Catalog> catalogSeeder,
            ISeeder<VehicleOffer> vehicleOfferSeeder,
            ISeeder<OptionCategory> optionCategorySeeder,
            ISeeder<Option> optionSeeder,
            ISeeder<OptionOffer> optionOfferSeeder,
            ISeeder<ColorType> colorTypeSeeder,
            ISeeder<Color> colorSeeder,
            ISeeder<DefaultColorRecord> defaultColorSeeder,
            ISeeder<CategoryPerOptionRecord> categoryPerOptionSeeder,
            ISeeder<Package> packageSeeder,
            ISeeder<ContractType> contractTypeSeeder,
            ISeeder<ContractTemplate> contractTemplateSeeder)
            : base(modelSeeder,
                bodySeeder,
                equipmentSeeder,
                engineSeeder,
                gearboxSeeder,
                catalogSeeder,
                vehicleOfferSeeder,
                optionCategorySeeder,
                optionSeeder,
                optionOfferSeeder,
                colorTypeSeeder,
                colorSeeder,
                defaultColorSeeder,
                categoryPerOptionSeeder,
                packageSeeder,
                contractTypeSeeder,
                contractTemplateSeeder)
        {
        }
    }
}
