﻿using AutoMapper;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds
{
    public class VehicleOfferSeederDataSource : EntitySeederCsvDataSource<VehicleOffer, VehicleOfferSeederDataSource.Record>
    {
        public class Record
        {
            public int CatalogId { get; set; }

            public string ModelKey { get; set; }

            public decimal Price { get; set; }
        }

        public VehicleOfferSeederDataSource(IMapper mapper, ITextReaderProvider readerProvider)
            : base(mapper, readerProvider)
        {
        }

        public VehicleOfferSeederDataSource(IMapper mapper, string text)
            : base(mapper, new StringReaderProvider(text))
        {
        }
    }
}
