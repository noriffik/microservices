﻿using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Infrastructure.Seeds;
using NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds
{
    public class OptionOfferSeeder : ISeeder<OptionOffer>
    {
        private readonly VehicleConfiguratorContext _dbContext;
        private readonly ISeederDataSource<OptionOfferSeederModel> _dataSource;

        public OptionOfferSeeder(VehicleConfiguratorContext dbContext, ISeederDataSource<OptionOfferSeederModel> dataSource)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _dataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
        }

        public async Task Seed()
        {
            var optionOffers = _dataSource.Get()
                .ToList();
            var vehicleOffers = await _dbContext.VehicleOffers
                .ToListAsync();
            var options = await _dbContext.Options
                .ToListAsync();

            foreach (var optionOffer in optionOffers)
            {
                var vehicleOffer = vehicleOffers
                    .Where(v => v.CatalogId == optionOffer.CatalogId)
                    .SingleOrDefault(v => v.ModelKey == optionOffer.ModelKey);

                if (vehicleOffer == null)
                    throw new InvalidEntityIdException(typeof(VehicleOffer), optionOffer.ModelKey);

                var optionId = OptionId.Next(vehicleOffer.ModelId, optionOffer.PrNumber);
                if (options.All(o => o.Id != optionId))
                    throw new InvalidEntityIdException(typeof(Option), optionId);

                if (!vehicleOffer.HasOptionOffer(optionOffer.PrNumber))
                    vehicleOffer.AddOptionOffer(optionOffer.PrNumber, optionOffer.Availability);
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}
