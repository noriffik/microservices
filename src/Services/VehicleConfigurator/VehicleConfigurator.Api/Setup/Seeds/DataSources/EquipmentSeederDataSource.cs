﻿using AutoMapper;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources
{
    public class EquipmentSeederDataSource : EntitySeederCsvDataSource<Equipment, EquipmentSeederDataSource.Record>
    {
        public class Record
        {
            public string ModelId { get; set; }

            public string Key { get; set; }

            public string Name { get; set; }
        }
        
        public EquipmentSeederDataSource(IMapper mapper, ITextReaderProvider readerProvider)
            : base(mapper, readerProvider)
        {
        }

        public EquipmentSeederDataSource(IMapper mapper, string text)
            : base(mapper, new StringReaderProvider(text))
        {
        }
    }
}
