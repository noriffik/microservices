﻿using AutoMapper;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Seeds.DataSources;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources
{
    public class CategoryPerOptionSeederDataSource : EntitySeederCsvDataSource<CategoryPerOptionRecord, CategoryPerOptionSeederDataSource.Record>
    {
        public class Record
        {
            public string OptionId { get; set; }

            public int CategoryId { get; set; }
        }

        public CategoryPerOptionSeederDataSource(IMapper mapper, ITextReaderProvider readerProvider) 
            : base(mapper, readerProvider)
        {
        }

        public CategoryPerOptionSeederDataSource(IMapper mapper, string text)
            : base(mapper, new StringReaderProvider(text))
        {
        }
    }
}
