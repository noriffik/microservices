﻿using AutoMapper;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources
{
    public class ColorTypeSeederDataSource : EntitySeederCsvDataSource<ColorType, ColorTypeSeederDataSource.Record>
    {
        public class Record
        {
            public string ColorTypeId { get; set; }

            public string Name { get; set; }
        }

        public ColorTypeSeederDataSource(IMapper mapper, ITextReaderProvider readerProvider) 
            : base(mapper, readerProvider)
        {
        }

        public ColorTypeSeederDataSource(IMapper mapper, string text)
            : base(mapper, new StringReaderProvider(text))
        {
        }
    }
}
