﻿using AutoMapper;
using NexCore.Domain.Vehicles;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources
{
    public class OptionOfferSeederDataSource : CompositeEntitySeederCsvDataSource<OptionOfferSeederModel, OptionOfferSeederDataSource.Record>
    {
        public class Record
        {
            public string PrNumber { get; set; }

            public AvailabilityType Type { get; set; }

            public ReasonType? Reason { get; set; }

            public decimal? Price { get; set; }

            public string ModelKey { get; set; }

            public int CatalogId { get; set; }
        }

        public OptionOfferSeederDataSource(IMapper mapper, params ITextReaderProvider[] readerProviders) 
            : base(mapper, readerProviders)
        {
        }

        public OptionOfferSeederDataSource(IMapper mapper, string text)
            : base(mapper, new StringReaderProvider(text))
        {
        }
    }

    public class OptionOfferSeederModel
    {
        public PrNumber PrNumber { get; set; }

        public Availability Availability { get; set; }

        public ModelKey ModelKey { get; set; }

        public int CatalogId { get; set; }
    }
}
