﻿using AutoMapper;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources
{
    public class ColorSeederDataSource : EntitySeederCsvDataSource<Color, ColorSeederDataSource.Record>
    {
        public class Record
        {
            public string ColorId { get; set; }

            public string Argb { get; set; }

            public string TypeId { get; set; }

            public string Name { get; set; }
        }

        public ColorSeederDataSource(IMapper mapper, ITextReaderProvider readerProvider)
            : base(mapper, readerProvider)
        {
        }

        public ColorSeederDataSource(IMapper mapper, string text)
            : base(mapper, new StringReaderProvider(text))
        {
        }
    }
}
