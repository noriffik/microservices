﻿using AutoMapper;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources
{
    public class PackageSeederDataSource : EntitySeederCsvDataSource<Package, PackageSeederDataSource.Record>
    {
        public class Record
        {
            public string Id { get; set; }

            public int CatalogId { get; set; }

            public string Name { get; set; }

            public string ModelKeySet { get; set; }

            public string Applicability { get; set; }

            public decimal Price { get; set; }
        }

        public PackageSeederDataSource(IMapper mapper, ITextReaderProvider readerProvider)
            : base(mapper, readerProvider)
        {
        }

        public PackageSeederDataSource(IMapper mapper, string text)
            : base(mapper, new StringReaderProvider(text))
        {
        }
    }
}
