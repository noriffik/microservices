﻿using AutoMapper;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources
{
    public class EngineSeederDataSource : EntitySeederCsvDataSource<Engine, EngineSeederDataSource.Record>
    {
        public class Record
        {
            public string ModelId { get; set; }

            public string Key { get; set; }

            public string Name { get; set; }

            public int FuelType { get; set; }
        }
        
        public EngineSeederDataSource(IMapper mapper, ITextReaderProvider readerProvider)
            : base(mapper, readerProvider)
        {
        }

        public EngineSeederDataSource(IMapper mapper, string text)
            : base(mapper, new StringReaderProvider(text))
        {
        }
    }
}
