﻿using AutoMapper;
using CsvHelper.Configuration.Attributes;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources
{
    public class CatalogSeederDataSource : EntitySeederCsvDataSource<Catalog, CatalogSeederDataSource.Record>
    {
        public class Record
        {
            public int Year { get; set; }

            [Format("yyyy-MM-dd")]
            public DateTime From { get; set; }

            [Format("yyyy-MM-dd")]
            public DateTime? To { get; set; }

            public CatalogStatus Status { get; set; }
        }

        public CatalogSeederDataSource(IMapper mapper, ITextReaderProvider readerProvider)
            : base(mapper, readerProvider)
        {
        }

        public CatalogSeederDataSource(IMapper mapper, string text)
            : base(mapper, new StringReaderProvider(text))
        {
        }
    }
}
