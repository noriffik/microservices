﻿using NexCore.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources
{
    public class DefaultColorRecord
    {
        public int CatalogId { get; set; }

        public ModelKey ModelKey { get; set; }

        public ColorId ColorId { get; set; }
    }
}