﻿using AutoMapper;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Seeds.DataSources;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources
{
    public class DefaultColorSeederDataSource : EntitySeederCsvDataSource<DefaultColorRecord, DefaultColorSeederDataSource.Record>
    {
        public class Record
        {
            public int CatalogId { get; set; }

            public string ModelKey { get; set; }

            public string ColorId { get; set; }
        }

        public DefaultColorSeederDataSource(IMapper mapper, ITextReaderProvider readerProvider)
            : base(mapper, readerProvider)
        {
        }

        public DefaultColorSeederDataSource(IMapper mapper, string text)
            : base(mapper, new StringReaderProvider(text))
        {
        }
    }
}
