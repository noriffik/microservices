﻿using AutoMapper;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources
{
    public class ModelSeederDataSource : EntitySeederCsvDataSource<Model, ModelSeederDataSource.Record>
    {
        public class Record
        {
            public string Id { get; set; }

            public string Name { get; set; }
        }

        public ModelSeederDataSource(IMapper mapper, ITextReaderProvider readerProvider)
            : base(mapper, readerProvider)
        {
        }

        public ModelSeederDataSource(IMapper mapper, string text)
            : base(mapper, new StringReaderProvider(text))
        {
        }
    }
}
