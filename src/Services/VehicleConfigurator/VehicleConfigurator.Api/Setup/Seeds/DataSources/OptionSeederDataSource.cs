﻿using AutoMapper;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources
{
    public class OptionSeederDataSource : EntitySeederCsvDataSource<Option, OptionSeederDataSource.Record>
    {
        public class Record
        {
            public string OptionId { get; set; }

            public string Name { get; set; }

            public int? CategoryId { get; set; }
        }

        public OptionSeederDataSource(IMapper mapper, ITextReaderProvider readerProvider)
            : base(mapper, readerProvider)
        {
        }

        public OptionSeederDataSource(IMapper mapper, string text)
            : base(mapper, new StringReaderProvider(text))
        {
        }
    }
}
