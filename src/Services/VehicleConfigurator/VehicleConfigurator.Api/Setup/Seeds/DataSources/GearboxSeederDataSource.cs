﻿using AutoMapper;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources
{
    public class GearboxSeederDataSource : EntitySeederCsvDataSource<Gearbox, GearboxSeederDataSource.Record>
    {
        public class Record
        {
            public string ModelId { get; set; }

            public string Key { get; set; }

            public string Name { get; set; }

            public string Type { get; set; }

            public int Category { get; set; }
        }
        
        public GearboxSeederDataSource(IMapper mapper, ITextReaderProvider readerProvider)
            : base(mapper, readerProvider)
        {
        }

        public GearboxSeederDataSource(IMapper mapper, string text)
            : base(mapper, new StringReaderProvider(text))
        {
        }
    }
}
