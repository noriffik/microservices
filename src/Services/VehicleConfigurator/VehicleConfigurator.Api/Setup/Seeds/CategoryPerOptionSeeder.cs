﻿using Microsoft.EntityFrameworkCore;
using NexCore.Infrastructure.Seeds;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds
{
    public class CategoryPerOptionSeeder : ISeeder<CategoryPerOptionRecord>
    {
        private readonly VehicleConfiguratorContext _dbContext;
        private readonly ISeederDataSource<CategoryPerOptionRecord> _dataSource;

        public CategoryPerOptionSeeder(VehicleConfiguratorContext dbContext, ISeederDataSource<CategoryPerOptionRecord> dataSource)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _dataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
        }

        public async Task Seed()
        {
            var categoriesPerOptions = _dataSource.Get().ToList();
            var options = await _dbContext.Options.ToListAsync();

            foreach (var categoryPerOption in categoriesPerOptions)
            {
                var option = options.SingleOrDefault(o => o.Id.Equals(categoryPerOption.OptionId));
                if (option == null)
                    continue;

                option.OptionCategoryId = categoryPerOption.CategoryId;
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}
