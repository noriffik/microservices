﻿using Microsoft.EntityFrameworkCore;
using NexCore.Infrastructure.Seeds;
using NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Setup.Seeds
{
    public class DefaultColorSeeder : ISeeder<DefaultColorRecord>
    {
        private readonly VehicleConfiguratorContext _dbContext;
        private readonly ISeederDataSource<DefaultColorRecord> _dataSource;

        public DefaultColorSeeder(VehicleConfiguratorContext dbContext, ISeederDataSource<DefaultColorRecord> dataSource)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _dataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
        }

        public async Task Seed()
        {
            var defaultColors = _dataSource.Get()
                .ToList();
            var vehicleOffers = await _dbContext.VehicleOffers
                .ToListAsync();

            foreach (var defaultColor in defaultColors)
            {
                var vehicleOffer = vehicleOffers
                    .Where(v => v.CatalogId == defaultColor.CatalogId)
                    .Single(v => v.ModelKey == defaultColor.ModelKey);

                if (vehicleOffer.DefaultColorId != null)
                    continue;

                if (!vehicleOffer.HasColorOffer(defaultColor.ColorId))
                    vehicleOffer.AddColorOffer(defaultColor.ColorId, 0);

                vehicleOffer.SetDefaultColor(defaultColor.ColorId);

                vehicleOffer.Publish();
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}
