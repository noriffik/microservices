﻿using Microsoft.Extensions.Configuration;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Setup;
using NexCore.VehicleConfigurator.Infrastructure;
using System;

namespace NexCore.VehicleConfigurator.Api.Setup.Tasks
{
    public class VehicleConfiguratorDatabaseSetupTask : DatabaseSetupTask<VehicleConfiguratorContext>
    {
        private const string IsSeedingEnabledKey = "IsSeedingEnabled";

        public VehicleConfiguratorDatabaseSetupTask(VehicleConfiguratorContext context) : base(context)
        {
        }

        public VehicleConfiguratorDatabaseSetupTask(VehicleConfiguratorContext context, IDbContextSeeder<VehicleConfiguratorContext> seeder,
            IConfiguration configuration) : base(context, seeder)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            
            IsSeedingEnabled = configuration.GetValue(IsSeedingEnabledKey, true);
        }
    }
}
