﻿using Autofac;
using MediatR;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Queries.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Module = Autofac.Module;

namespace NexCore.VehicleConfigurator.Api.Infrastructure.AutofacModules
{
    public class VehicleComponentCommandsModule : Module
    {
        private readonly Assembly _vehicleSharedKernel = typeof(PrNumber).Assembly;

        private readonly Type _handler = typeof(FindQueryHandler<,>);

        protected override void Load(ContainerBuilder builder)
        {
            var modelComponents = typeof(IModelComponent)
                .Assembly
                .DefinedTypes
                .Where(t => !t.IsInterface && !t.IsAbstract)
                .Where(t => t.IsAssignableTo<IModelComponent>())
                .ToList();

            modelComponents.ToList().ForEach(m =>
            {
                RegisterQueryHandler(builder, typeof(FindByIdQuery<,>), typeof(ModelComponentModel), m);
                RegisterQueryHandler(builder, typeof(FindAllQuery<,>), typeof(IEnumerable<ModelComponentModel>), m);
                RegisterQueryHandler(builder, typeof(FindByModelIdQuery<,>), typeof(IEnumerable<ModelComponentModel>), m);
            });
        }

        private void RegisterQueryHandler(ContainerBuilder builder, Type command, Type response, Type component)
        {
            var idClassName = component.Name + "Id";
            var id = GetComponentIdType(component, idClassName);

            var commandType = command.MakeGenericType(component, id);
            
            var handlerType = _handler.MakeGenericType(component, id);
            var handlerAbstractType = typeof(IRequestHandler<,>).MakeGenericType(commandType, response);

            builder.RegisterType(handlerType)
                .IfNotRegistered(handlerAbstractType)
                .As(handlerAbstractType);
        }

        private TypeInfo GetComponentIdType(Type component, string className)
        {
            return GetTypeByClassName(component.Assembly, className) ?? GetTypeByClassName(_vehicleSharedKernel, className);
        }

        private static TypeInfo GetTypeByClassName(Assembly assembly, string className)
        {
            return assembly
                .DefinedTypes
                .SingleOrDefault(t => t.Name == className && t.IsAssignableTo<EntityId>());
        }
    }
}
