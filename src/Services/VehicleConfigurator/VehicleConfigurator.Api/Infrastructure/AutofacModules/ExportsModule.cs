﻿using Autofac;
using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts;
using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.Mappers;
using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.VehicleSales.Mappers;
using NexCore.VehicleConfigurator.Api.Localization;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;

namespace NexCore.VehicleConfigurator.Api.Infrastructure.AutofacModules
{
    public class ExportsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<UahCurrencyWriter>()
                .As<ICurrencyWriter>()
                .InstancePerLifetimeScope();

            builder.RegisterType<DocxWriter<VehicleSaleContent>>()
                .As<IContractDocxWriter<VehicleSaleContent>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ContractDocxContentMapper>()
                .As<IContractDocxContentMapper<VehicleSaleContent>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PaymentDocxContentMapper>()
                .InstancePerLifetimeScope();

            builder.RegisterType<OptionDocxListContentMapper>()
                .InstancePerLifetimeScope();

            builder.RegisterType<TotalPriceDocxContentMapper>()
                .SingleInstance();

            builder.RegisterType<DealerDocxContentMapper>()
                .SingleInstance();

            builder.RegisterType<VehicleDocxContentMapper>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PrivateCustomerDocxContentMapper>()
                .SingleInstance();

            builder.RegisterType<PassportDocxContentMapper>()
                .SingleInstance();
        }
    }
}
