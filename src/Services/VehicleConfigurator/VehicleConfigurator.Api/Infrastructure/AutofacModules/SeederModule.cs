﻿using Autofac;
using Microsoft.Extensions.Configuration;
using NexCore.Domain.Vehicles;
using NexCore.Infrastructure.Seeds;
using NexCore.VehicleConfigurator.Api.Setup.Seeds;
using NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Contracts;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;

namespace NexCore.VehicleConfigurator.Api.Infrastructure.AutofacModules
{
    public class SeederModule : Module
    {
        private readonly IConfiguration _configuration;

        public SeederModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VehicleConfiguratorSeeder>()
                .As<IDbContextSeeder<VehicleConfiguratorContext>>()
                .InstancePerLifetimeScope();

            RegisterModelSeeder(builder);
            RegisterBodySeeder(builder);
            RegisterEquipmentSeeder(builder);
            RegisterEngineSeeder(builder);
            RegisterGearboxSeeder(builder);
            RegisterCatalogSeeder(builder);
            RegisterVehicleOfferSeeder(builder);
            RegisterOptionSeeder(builder);
            RegisterOptionCategorySeeder(builder);
            RegisterOptionOfferSeeder(builder);
            RegisterColorTypeSeeder(builder);
            RegisterColorSeeder(builder);
            RegisterDefaultColorSeeder(builder);
            RegisterCategoryPerOptionSeeder(builder);
            RegisterPackageSeeder(builder);
            RegisterContractTypeSeeder(builder);
            RegisterContractTemplateSeeder(builder);
        }

        private void RegisterModelSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<EntitySeeder<VehicleConfiguratorContext, Model, ModelId>>()
                .As<ISeeder<Model>>();

            builder.RegisterType<ModelSeederDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:ModelDataPath"]))
                .As<ISeederDataSource<Model>>();
        }

        private void RegisterBodySeeder(ContainerBuilder builder)
        {
            builder.RegisterType<EntitySeeder<VehicleConfiguratorContext, Body, BodyId>>()
                .As<ISeeder<Body>>();

            builder.RegisterType<BodySeederDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:BodyDataPath"]))
                .As<ISeederDataSource<Body>>();
        }

        private void RegisterEquipmentSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<EntitySeeder<VehicleConfiguratorContext, Equipment, EquipmentId>>()
                .As<ISeeder<Equipment>>();

            builder.RegisterType<EquipmentSeederDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:EquipmentDataPath"]))
                .As<ISeederDataSource<Equipment>>();
        }

        private void RegisterEngineSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<EntitySeeder<VehicleConfiguratorContext, Engine, EngineId>>()
                .As<ISeeder<Engine>>();

            builder.RegisterType<EngineSeederDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:EngineDataPath"]))
                .As<ISeederDataSource<Engine>>();
        }

        private void RegisterGearboxSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<EntitySeeder<VehicleConfiguratorContext, Gearbox, GearboxId>>()
                .As<ISeeder<Gearbox>>();

            builder.RegisterType<GearboxSeederDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:GearboxDataPath"]))
                .As<ISeederDataSource<Gearbox>>();
        }

        private void RegisterCatalogSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<EntitySeeder<VehicleConfiguratorContext, Catalog>>()
                .As<ISeeder<Catalog>>();

            builder.RegisterType<CatalogSeederDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:CatalogDataPath"]))
                .As<ISeederDataSource<Catalog>>();
        }

        private void RegisterVehicleOfferSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<VehicleOfferSeeder>()
                .As<ISeeder<VehicleOffer>>();

            builder.RegisterType<VehicleOfferSeederDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:VehicleOfferDataPath"]))
                .As<ISeederDataSource<VehicleOffer>>();
        }

        private void RegisterOptionSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<EntitySeeder<VehicleConfiguratorContext, Option, OptionId>>()
                .As<ISeeder<Option>>();

            builder.RegisterType<OptionSeederDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:OptionDataPath"]))
                .As<ISeederDataSource<Option>>();
        }

        private void RegisterOptionCategorySeeder(ContainerBuilder builder)
        {
            builder.RegisterType<EntitySeeder<VehicleConfiguratorContext, OptionCategory>>()
                .As<ISeeder<OptionCategory>>();

            builder.RegisterType<OptionCategoryDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:OptionCategoryDataPath"]))
                .As<ISeederDataSource<OptionCategory>>();
        }

        private void RegisterOptionOfferSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<OptionOfferSeeder>()
                .As<ISeeder<OptionOffer>>();

            builder.RegisterType<OptionOfferSeederDataSource>()
                .WithParameter("readerProviders", new[] {
                    new TextFileReaderProvider(_configuration["Setup:Data:OptionOfferNJDataPath"]),
                    new TextFileReaderProvider(_configuration["Setup:Data:OptionOfferNHDataPath"]),
                    new TextFileReaderProvider(_configuration["Setup:Data:OptionOffer5EDataPath"]),
                    new TextFileReaderProvider(_configuration["Setup:Data:OptionOffer3VDataPath"]),
                    new TextFileReaderProvider(_configuration["Setup:Data:OptionOfferNUDataPath"]),
                    new TextFileReaderProvider(_configuration["Setup:Data:OptionOfferNSDataPath"])
                })
                .As<ISeederDataSource<OptionOfferSeederModel>>();
        }

        private void RegisterColorTypeSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<EntitySeeder<VehicleConfiguratorContext, ColorType, ColorTypeId>>()
                .As<ISeeder<ColorType>>();

            builder.RegisterType<ColorTypeSeederDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:ColorTypeDataPath"]))
                .As<ISeederDataSource<ColorType>>();
        }

        private void RegisterColorSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<EntitySeeder<VehicleConfiguratorContext, Color, ColorId>>()
                .As<ISeeder<Color>>();

            builder.RegisterType<ColorSeederDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:ColorDataPath"]))
                .As<ISeederDataSource<Color>>();
        }

        private void RegisterPackageSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<PackageSeeder>()
                .As<ISeeder<Package>>();

            builder.RegisterType<PackageSeederDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:PackageDataPath"]))
                .As<ISeederDataSource<Package>>();
        }

        private void RegisterContractTypeSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<ContractTypeSeeder>()
                .As<ISeeder<ContractType>>();
        }

        private void RegisterContractTemplateSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<ContractTemplateSeeder>()
                .WithParameter("readerProvider", new BinaryReaderProvider(
                    _configuration["Setup:Data:DefaultContractTemplateDataPath"]))
                .As<ISeeder<ContractTemplate>>();
        }

        private void RegisterDefaultColorSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<DefaultColorSeeder>()
                .As<ISeeder<DefaultColorRecord>>();

            builder.RegisterType<DefaultColorSeederDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:DefaultColorDataPath"]))
                .As<ISeederDataSource<DefaultColorRecord>>();
        }

        private void RegisterCategoryPerOptionSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<CategoryPerOptionSeeder>()
                .As<ISeeder<CategoryPerOptionRecord>>();

            builder.RegisterType<CategoryPerOptionSeederDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:CategoryPerOptionDataPath"]))
                .As<ISeederDataSource<CategoryPerOptionRecord>>();
        }
    }
}
