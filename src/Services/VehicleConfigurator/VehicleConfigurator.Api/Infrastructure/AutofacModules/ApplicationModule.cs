﻿using Autofac;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Configurations.Rules;
using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.Domain.Pricing;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Api.Infrastructure.AutofacModules
{
    public class ApplicationModule : Module
    {
        public ApplicationModule()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            RegisterModel(builder);
            RegisterBody(builder);
            RegisterEngine(builder);
            RegisterEquipment(builder);
            RegisterGearbox(builder);
            RegisterOption(builder);
            RegisterVehicle(builder);
            RegisterCatalog(builder);
            RegisterVehicleOffer(builder);
            RegisterPackage(builder);
            RegisterConfiguration(builder);
            RegisterOffer(builder);
            RegisterColorType(builder);
            RegisterColor(builder);
            RegisterRestriction(builder);
        }

        private static void RegisterModel(ContainerBuilder builder)
        {
            builder.RegisterType<ModelService>()
                .As<IComponentService<Model, ModelId>>();
        }

        private static void RegisterBody(ContainerBuilder builder)
        {
            builder.RegisterType<BodyService>()
                .As<IModelComponentService<Body, BodyId>>();
        }

        private static void RegisterEngine(ContainerBuilder builder)
        {
            builder.RegisterType<EngineService>()
                .As<IModelComponentService<Engine, EngineId>>();
        }

        private static void RegisterEquipment(ContainerBuilder builder)
        {
            builder.RegisterType<EquipmentService>()
                .As<IModelComponentService<Equipment, EquipmentId>>();
        }

        private static void RegisterGearbox(ContainerBuilder builder)
        {
            builder.RegisterType<GearboxService>()
                .As<IModelComponentService<Gearbox, GearboxId>>();
        }

        private static void RegisterOption(ContainerBuilder builder)
        {
            builder.RegisterType<OptionService>()
                .As<IModelComponentService<Option, OptionId>>();

            builder.RegisterType<OptionService>()
                .As<IOptionService>();
        }

        private static void RegisterVehicle(ContainerBuilder builder)
        {

            builder.RegisterType<ModelKeyValidator>()
                .As<IModelKeyValidator>()
                .InstancePerLifetimeScope();
        }
        private static void RegisterCatalog(ContainerBuilder builder)
        {
            builder.RegisterType<CatalogService>()
                .As<ICatalogService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ConfigurationValidator>()
                .As<IConfigurationValidator>()
                .InstancePerLifetimeScope();
        }

        private static void RegisterVehicleOffer(ContainerBuilder builder)
        {
            builder.RegisterType<VehicleOfferService>()
                .As<IVehicleOfferService>();

            builder.RegisterType<OptionOfferService>()
                .As<IOptionOfferService>()
                .InstancePerLifetimeScope();
        }

        private static void RegisterPackage(ContainerBuilder builder)
        {
            builder.RegisterType<PackageService>()
                .As<IPackageService>();

            builder.RegisterType<ApplicabilityParser>()
                .As<IApplicabilityParser>();

            builder.RegisterType<Application.Commands.Packages.AddCommandMapper>()
                .As<ICommandMapper<Application.Commands.Packages.AddCommand, Package>>();
        }

        private void RegisterConfiguration(ContainerBuilder builder)
        {
            builder.RegisterType<ConfigurationValidationService>()
                .As<IConfigurationValidationService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ConfigurationService>()
                .As<IConfigurationService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PricingService>()
                .As<IPricingService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ConfigurationPricingService>()
                .As<IConfigurationPricingService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PackagePricingService>()
                .As<IPackagePricingService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<OptionPricingService>()
                .As<IOptionPricingService>()
                .InstancePerLifetimeScope();
        }

        private static void RegisterColorType(ContainerBuilder builder)
        {
            builder.RegisterType<ColorTypeService>()
                .As<IColorTypeService>();
        }

        private static void RegisterColor(ContainerBuilder builder)
        {
            builder.RegisterType<ColorService>()
                .As<IColorService>();
        }

        private void RegisterOffer(ContainerBuilder builder)
        {
            builder.RegisterType<OfferService>()
                .As<IOfferService>()
                .InstancePerLifetimeScope();
        }

        private void RegisterRestriction(ContainerBuilder builder)
        {
            builder.RegisterType<RestrictionService>()
                .As<IRestrictionService>()
                .InstancePerLifetimeScope();
        }
    }
}
