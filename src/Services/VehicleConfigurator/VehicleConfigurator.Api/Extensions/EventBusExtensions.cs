﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using NexCore.EventBus.Abstract;
using NexCore.VehicleConfigurator.Api.Application.Messages;

namespace NexCore.VehicleConfigurator.Api.Extensions
{
    public static class EventBusExtensions
    {
        public static void RunEventBus(this IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
            eventBus.Subscribe<DealerPrivateCustomerAddedEvent, DealerPrivateCustomerAddedEventHandler>();
            eventBus.Subscribe<DealerPrivateCustomerInfoUpdatedEvent, PrivateCustomerInfoUpdatedEventHandler>();
            eventBus.Subscribe<DealerAddedEvent, DealerAddedEventHandler>();
            eventBus.Subscribe<DealerRequisitesUpdatedEvent, DealerRequisitesUpdatedEventHandler>();
            eventBus.Subscribe<DealerEmployeeAddedEvent, DealerEmployeeAddedEventHandler>();
            eventBus.Subscribe<DealerEmployeeNameUpdatedEvent, DealerEmployeeNameUpdatedEventHandler>();
        }
    }
}
