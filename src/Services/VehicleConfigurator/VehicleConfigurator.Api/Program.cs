﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using NexCore.CurrencyExchange.DataSources.MsSql;
using NexCore.Infrastructure.Setup;
using NexCore.VehicleConfigurator.Api.Setup.Tasks;
using NexCore.WebApi.Extensions;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            await host.Setup<DatabaseSetupTask<CurrencyExchangeContext>>();
            await host.Setup<VehicleConfiguratorDatabaseSetupTask>();

            await host.RunAsync();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseHealthChecks("/hc");
    }
}
