﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Commands.Seeds;
using NexCore.VehicleConfigurator.Api.Authorization;
using NexCore.WebApi;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Controllers
{
    [Authorize]
    [Route("api/seed")]
    public class SeedsController : MediatrController
    {
        public SeedsController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Authorize(Roles = "Administrator")]
        [Route("run")]
        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public Task<IActionResult> Run() => ExecuteCommand(new SeedCommand(), AuthorizationRequirements.None);
    }
}
