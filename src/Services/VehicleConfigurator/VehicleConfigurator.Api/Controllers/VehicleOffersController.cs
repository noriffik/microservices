﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.AvailableColors;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.AvailableOptions;
using NexCore.VehicleConfigurator.Api.Authorization;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Controllers
{
    [Authorize]
    [Route("api/catalogs/vehicle")]
    public class VehicleOffersController : MediatrController
    {
        public VehicleOffersController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{Id}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(VehicleOfferModel))]
        public Task<IActionResult> Get([FromRoute] FindByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("many")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(PagedResponse<VehicleOfferModel>))]
        public Task<IActionResult> GetAll(FindAllQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("catalog/{CatalogId}/many")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(PagedResponse<VehicleOfferModel>))]
        public Task<IActionResult> GetByCatalogId(FindByCatalogIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("{VehicleOfferId}/available/options")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<AvailableOptionModel>))]
        public Task<IActionResult> GetAvailableOptions([FromRoute]AvailableOptionsQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("{VehicleOfferId}/available/colors")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<AvailableColorModel>))]
        public Task<IActionResult> GetAvailableColors([FromRoute]AvailableColorsQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("add")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Add([FromBody] AddCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None, nameof(Get));

        [Route("update/price")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> UpdatePrice([FromBody] UpdatePriceCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("change/status")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ChangeStatus([FromBody] ChangeStatusCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("{Id:int}/change/relevance-period")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ChangeRelevancePeriod(ChangeVehicleOfferRelevancePeriodCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("{Id:int}/reset/relevance-period")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ResetRelevancePeriod(ResetVehicleOfferRelevancePeriodCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("actualize/relevance/many")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Actualize(ActualizeRelevanceCommand<VehicleOffer> command) => ExecuteCommand(command, AuthorizationRequirements.None);
    }
}
