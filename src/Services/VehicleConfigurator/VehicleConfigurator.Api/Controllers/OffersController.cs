﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Commands.Offers;
using NexCore.VehicleConfigurator.Api.Application.Queries.Offers;
using NexCore.VehicleConfigurator.Api.Authorization;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Controllers
{
    [Authorize]
    [Route("api/offers")]
    public class OffersController : MediatrController
    {
        public OffersController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{Id}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(OfferVehicleModel))]
        public Task<IActionResult> Get([FromRoute] ByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("add")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Add([FromBody] AddCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None, nameof(Get));

        [Route("actualize")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Actualize([FromBody] ActualizeCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None, nameof(Get));

        [Route("change/customer/private")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> SetPrivateCustomer([FromBody] SetPrivateCustomerCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("change/status")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ChangeStatus([FromBody] ChangeStatusCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("many")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<OfferVehicleModel>))]
        public Task<IActionResult> GetAll(FindAllQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);
    }
}
