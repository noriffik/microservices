﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Queries.ModelKeys;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ModelKeysController : MediatrController
    {
        public ModelKeysController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("many/{CatalogId}/{OptionId}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(IEnumerable<string>))]
        public Task<IActionResult> FindMany([FromRoute] FindByOptionIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);
    }
}

