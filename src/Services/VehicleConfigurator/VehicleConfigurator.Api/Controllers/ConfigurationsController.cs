﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations;
using NexCore.VehicleConfigurator.Api.Authorization;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Controllers
{
    [Authorize]
    [Route("api/configurations")]
    public class ConfigurationsController : MediatrController
    {
        public ConfigurationsController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{Id}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(QueryResponse))]
        public Task<IActionResult> Get([FromRoute] ByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);
        
        [Route("{Id}/details")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(DetailedQueryResponse))]
        public Task<IActionResult> GetDetails([FromRoute] ByIdDetailedQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("preview/{ModelYear}/{ModelKey}/{PrNumbers?}")]
        [HttpGet]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [Produces(typeof(DetailedQueryResponse))]
        public Task<IActionResult> Preview([FromRoute] BySpecificationQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("many")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(IEnumerable<QueryResponse>))]
        public Task<IActionResult> GetAll(UnboundQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("catalog/{CatalogId}/many")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(IEnumerable<QueryResponse>))]
        public Task<IActionResult> GetByCatalogId([FromRoute] ByCatalogIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("add")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Add([FromBody] AddCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None, nameof(Get));

        [Route("copy/{ConfigurationId}")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Copy([FromRoute] CopyCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None, nameof(Get));

        [Route("include/option")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> IncludeOption([FromBody] IncludeOptionCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("exclude/option")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ExcludeOption([FromBody] ExcludeOptionCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);
        
        [Route("change/status")]
        [HttpPut]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public Task<IActionResult> ChangeStatus([FromBody] ChangeStatusCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("include/package")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> IncludePackage([FromBody] IncludePackageCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("exclude/package")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ExcludePackage([FromBody] ExcludePackageCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("change/color")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ChangeColor([FromBody] ChangeColorCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("validate/{ModelYear}/{ModelKey}/{PrNumberSet}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [Produces(typeof(ValidationResponse))]
        public Task<IActionResult> Validate([FromRoute] ValidateCommand command) => ExecuteQuery(command, QueryAuthorizationSettings.None);

        [Route("remove/{ConfigurationId}")]
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Remove(DeleteCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);
    }
}
