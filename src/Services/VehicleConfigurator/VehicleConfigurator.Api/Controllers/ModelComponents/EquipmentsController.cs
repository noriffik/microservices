﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Equipments;
using NexCore.VehicleConfigurator.Api.Authorization;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Controllers.ModelComponents
{
    [Authorize]
    [Route("api/model/components/equipment")]
    public class EquipmentsController : ModelComponentController<Equipment, EquipmentId>
    {
        public EquipmentsController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("add")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Add([FromBody] AddCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None, nameof(Get));

        [Route("update")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Update([FromBody] UpdateCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);
    }
}
