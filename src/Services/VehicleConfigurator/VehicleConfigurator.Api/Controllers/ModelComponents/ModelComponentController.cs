﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Queries.ModelComponents;
using NexCore.VehicleConfigurator.Api.Authorization;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Controllers.ModelComponents
{
    [Authorize]
    public abstract class ModelComponentController<TComponent, TId> : MediatrController
        where TComponent : ModelComponent<TId>
        where TId : EntityId
    {
        protected ModelComponentController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{Id}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(ModelComponentModel))]
        public Task<IActionResult> Get([FromRoute] FindByIdQuery<TComponent, TId> query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("many")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(IEnumerable<ModelComponentModel>))]
        public Task<IActionResult> GetAll() => ExecuteQuery(new FindAllQuery<TComponent, TId>(), QueryAuthorizationSettings.None);

        [Route("many/model/{ModelId}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(IEnumerable<ModelComponentModel>))]
        public Task<IActionResult> GetByModel([FromRoute] FindByModelIdQuery<TComponent, TId> query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("remove/{Id}")]
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Delete([FromRoute] DeleteCommand<TComponent, TId> command) => ExecuteCommand(command, AuthorizationRequirements.None);
    }
}
