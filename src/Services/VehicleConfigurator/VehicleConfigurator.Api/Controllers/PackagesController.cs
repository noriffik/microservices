﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using NexCore.VehicleConfigurator.Api.Application.Commands.Packages;
using NexCore.VehicleConfigurator.Api.Application.Queries.Packages;
using NexCore.VehicleConfigurator.Api.Authorization;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Controllers
{
    [Authorize]
    [Route("api/catalogs/packages")]
    public class PackagesController : MediatrController
    {
        public PackagesController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{Id}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [Produces(typeof(PackageModel))]
        public Task<IActionResult> Get([FromRoute] FindByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("catalog/{CatalogId}/many")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<PackageModel>))]
        public Task<IActionResult> GetByCatalogId([FromRoute] ByCatalogIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("add")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [Produces(typeof(PackageModel))]
        public Task<IActionResult> Add([FromBody] AddCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None, nameof(Get));

        [Route("coverage/update")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ExpandCoverage([FromBody] UpdateCoverageCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("status/change")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ChangeStatus([FromBody] ChangeStatusCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("price/update")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> UpdatePrice([FromBody] UpdatePriceCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);
        [Route("applicability/update")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> UpdateApplicability([FromBody] UpdateApplicabilityCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);
    }
}
