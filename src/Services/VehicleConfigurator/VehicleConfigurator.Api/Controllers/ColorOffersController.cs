﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers;
using NexCore.VehicleConfigurator.Api.Authorization;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AddCommand = NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers.AddCommand;

namespace NexCore.VehicleConfigurator.Api.Controllers
{
    [Authorize]
    [Route("api/catalogs/vehicle")]
    public class ColorOffersController : MediatrController
    {
        public ColorOffersController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{VehicleOfferId}/color/{ColorId}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [Produces(typeof(ColorOfferModel))]
        public Task<IActionResult> GetByVehicleOfferIdAndColorId([FromRoute] FindByVehicleOfferIdAndColorIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("{VehicleOfferId}/color/many")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<ColorOfferModel>))]
        public Task<IActionResult> GetByVehicleOfferId([FromRoute] FindByVehicleOfferIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("{VehicleOfferId}/color/add/{ColorId}")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Add(AddCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("{VehicleOfferId}/color/default/{ColorId}")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> SetDefault([FromRoute]SetDefaultColorCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("{VehicleOfferId}/color/{ColorId}/change/relevance-period")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ChangeRelevancePeriod(ChangeColorOfferRelevancePeriodCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("{VehicleOfferId}/color/{ColorId}/reset/relevance-period")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ResetRelevancePeriod([FromRoute]ResetColorOfferRelevancePeriodCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("color/relevance/actualize/catalog/{CatalogId:int}")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ActualizeRelevanceInCatalog([FromRoute]ActualizeRelevanceCommand<ColorOffer> command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("color/relevance/actualize")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ActualizeRelevanceInRelevantCatalogs() => ExecuteCommand(new ActualizeRelevanceCommand<ColorOffer>(), AuthorizationRequirements.None);

        [Route("{VehicleOfferId}/color/{ColorId}/change/price")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ChangePrice(ChangePriceCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);
    }
}
