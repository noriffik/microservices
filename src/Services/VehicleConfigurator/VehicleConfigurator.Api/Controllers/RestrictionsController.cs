﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions;
using NexCore.VehicleConfigurator.Api.Application.Queries.Restrictions;
using NexCore.VehicleConfigurator.Api.Authorization;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Controllers
{
    [Authorize]
    [Route("api/restrictions")]
    public class RestrictionsController : MediatrController
    {
        public RestrictionsController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{Id}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(RestrictionModel))]
        public Task<IActionResult> Get([FromRoute] FindByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("{CatalogId}/many")]
        [HttpGet]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [Produces(typeof(IEnumerable<RestrictionModel>))]
        public Task<IActionResult> GetByCatalogId([FromRoute] FindByCatalogIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("add")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Add([FromBody] AddCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None, nameof(Get));

        [Route("change/period")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ChangePeriod([FromBody] ChangeRelevancePeriodCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("enable/{Id}")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Enable([FromRoute] EnableCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("disable/{Id}")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Disable([FromRoute] DisableCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("actualize/{CatalogId:int}")]
        [HttpPut]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public Task<IActionResult> Actualize([FromRoute] ActualizeRelevanceCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);
    }
}
