﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.VehicleConfigurator.Api.Application.Commands.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Api.Application.Queries.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Api.Authorization;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Controllers
{
    [Authorize]
    [Route("api/contracts/vehicle-sales/")]
    public class VehicleSalesContractsController : MediatrController
    {
        public VehicleSalesContractsController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }
        
        [Route("{Id}")]
        [HttpGet]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [Produces(typeof(VehicleSaleContractModel))]
        public Task<IActionResult> Get([FromRoute] ByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("{Id}/docx")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(FileStreamResult))]
        public Task<IActionResult> Get([FromRoute] DocxExportQuery query) => Download(query, "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "template.docx", AuthorizationRequirements.None);

        [Route("add")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Add([FromBody] AddCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None, nameof(Get));

        [Route("add-with-offer")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> AddWithOffer([FromBody] AddWithOfferCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None, nameof(Get));

        [Route("")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(PagedResponse<VehicleSaleContractModel>))]
        public Task<IActionResult> GetAll([FromQuery] FindAllQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);
    }
}
