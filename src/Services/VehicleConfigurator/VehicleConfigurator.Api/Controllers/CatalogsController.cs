﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs;
using NexCore.VehicleConfigurator.Api.Authorization;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Controllers
{
    [Authorize]
    [Route("api/catalogs")]
    public class CatalogsController : MediatrController
    {
        public CatalogsController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{Id}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(CatalogModel))]
        public Task<IActionResult> Get([FromRoute] FindByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("single/actual/{ModelYear}/{Status}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(CatalogModel))]
        public Task<IActionResult> GetActual([FromRoute] FindActualQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("many")]
        [HttpGet]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<CatalogModel>))]
        public Task<IActionResult> GetAll(FindAllQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);
        
        [Route("many/outdated")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<CatalogModel>))]
        public Task<IActionResult> Outdated(FindOutdatedQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("add")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Add([FromBody] AddCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None, nameof(Get));

        [Route("change/status")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ChangeStatus([FromBody] ChangeStatusCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("{CatalogId}/change/relevance-period")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ChangePeriod(ChangeRelevancePeriodCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);
    }
}