﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories;
using NexCore.VehicleConfigurator.Api.Authorization;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Controllers
{
    [Authorize]
    [Route("api/components/option/category")]
    public class OptionCategoriesController : MediatrController
    {
        public OptionCategoriesController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("add")]
        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public Task<IActionResult> Add([FromBody] AddCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None, nameof(Get));

        [Route("{Id}")]
        [HttpGet]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [Produces(typeof(OptionCategoryModel))]
        public Task<IActionResult> Get([FromRoute] FindByIdQuery command) => ExecuteQuery(command, QueryAuthorizationSettings.None);

        [Route("many")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<OptionCategoryModel>))]
        public Task<IActionResult> GetAll() => ExecuteQuery(new FindAllQuery(), QueryAuthorizationSettings.None);

        [Route("assign")]
        [HttpPut]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public Task<IActionResult> Assign([FromBody] AssignCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("unassign")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Unassign([FromBody] UnassignCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("update")]
        [HttpPut]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public Task<IActionResult> Update([FromBody] UpdateCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("remove/{Id}")]
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Delete([FromRoute] DeleteCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

    }
}
