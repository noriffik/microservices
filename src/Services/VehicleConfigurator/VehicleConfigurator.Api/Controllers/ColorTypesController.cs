﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Commands.ColorTypes;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;
using NexCore.VehicleConfigurator.Api.Authorization;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Controllers
{
    [Authorize]
    [Route("api/colors/types")]
    public class ColorTypesController : MediatrController
    {
        public ColorTypesController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{Id}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [Produces(typeof(ColorTypeModel))]
        public Task<IActionResult> Get([FromRoute] FindByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("many")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<ColorTypeModel>))]
        public Task<IActionResult> GetAll() => ExecuteQuery(new FindAllQuery(), QueryAuthorizationSettings.None);

        [Route("add")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Add([FromBody] AddCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None, nameof(Get));

        [Route("{Id}/rename")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Rename(RenameCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);
    }
}
