﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers.ByComponents;
using NexCore.VehicleConfigurator.Api.Authorization;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AddCommand = NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers.AddCommand;

namespace NexCore.VehicleConfigurator.Api.Controllers
{
    [Authorize]
    [Route("api/catalogs/vehicle")]
    public class OptionOffersController : MediatrController
    {
        public OptionOffersController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{VehicleOfferId}/option/many")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<OptionOfferModel>))]
        public Task<IActionResult> GetManyByVehicleOffer([FromRoute] FindByVehicleOfferIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("{VehicleOfferId}/option/{PrNumber}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [Produces(typeof(OptionOfferModel))]
        public Task<IActionResult> GetByVehicleOfferIdAndPrNumber([FromRoute] FindByVehicleOfferIdAndPrNumberQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("catalog/{CatalogId}/model/{ModelId}/option/{PrNumber}/many")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<OptionOfferModel>))]
        public Task<IActionResult> GetByModelId([FromRoute] FindByModelIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("{VehicleOfferId}/option/add/{PrNumber}")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Add(AddCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("{VehicleOfferId}/option/update/availability/{PrNumber}")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> UpdateAvailability(UpdateAvailabilityCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("{VehicleOfferId}/option/update/inclusion/{PrNumber}")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> UpdateInclusion(UpdateInclusionCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("{VehicleOfferId}/option/add/rule-set/{PrNumber}")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> AddRuleSet(AddRuleSetCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("option/add/rule-set/for/many")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> AddRuleSetForMany([FromBody] AddRuleSetForManyCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("{VehicleOfferId}/option/remove/rule-set/{PrNumber}")]
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Remove(RemoveRuleCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("option/update/many/availability/{PrNumber}")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> UpdateAvailabilities(UpdateAvailabilitiesCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("{VehicleOfferId}/option/remove/{PrNumber}")]
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Remove(DeleteCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("{VehicleOfferId}/option/{PrNumber}/change/relevance-period")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ChangeRelevancePeriod(ChangeOptionOfferRelevancePeriodCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("{VehicleOfferId}/option/{PrNumber}/reset/relevance-period")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ResetRelevancePeriod([FromRoute] ResetOptionOfferRelevancePeriodCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("option/relevance/actualize/catalog/{CatalogId:int}")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ActualizeRelevanceInCatalog([FromRoute] ActualizeRelevanceCommand<OptionOffer> command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("option/relevance/actualize")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ActualizeRelevanceInRelevantCatalogs() => ExecuteCommand(new ActualizeRelevanceCommand<OptionOffer>(), AuthorizationRequirements.None);
    }
}
