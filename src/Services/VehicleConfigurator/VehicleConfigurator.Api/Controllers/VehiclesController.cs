﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleConfigurator.Api.Application.Queries.Vehicles;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Api.Controllers
{
    [Authorize]
    [Route("api/vehicles")]
    public class VehiclesController : MediatrController
    {
        public VehiclesController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("describe/{ModelKey}/{ColorId}/{PrNumberSet}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [Produces(typeof(VehicleDescriptionModel))]
        public Task<IActionResult> Get([FromRoute] VehicleDescriptionQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);
       
    }
}
