﻿namespace NexCore.VehicleConfigurator.Api.Authorization
{
    public static class Roles
    {
        public const string Administrator = "Administrator";
        public const string DistributorEmployee = "DistributorEmployee";
        public const string DealerEmployee = "DealerEmployee";
    }
}
