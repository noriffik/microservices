﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace NexCore.VehicleConfigurator.Api.Authorization
{
    public static class AuthorizationRequirements
    {
        public static readonly IAuthorizationRequirement None = new AssertionRequirement(c => true);

        public static readonly IAuthorizationRequirement Administrator = new RolesAuthorizationRequirement(new[] {Roles.Administrator});
    }
}
