﻿namespace NexCore.VehicleConfigurator.Api.Localization
{
    public interface ICurrencyWriter
    {
        string Write(decimal amount);
    }
}