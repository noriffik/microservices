﻿using System;
using System.Collections.Generic;

namespace NexCore.VehicleConfigurator.Api.Localization
{
    public class UahCurrencyWriter : ICurrencyWriter
    {
        private static readonly string[] Decade = {"", " одна", " дві", " три", " чотири", " п'ять", " шість", " сім", " вісім", " дев'ять", " десять", " одинадцять", " дванадцять", " тринадцять", " чотирнадцять", " п'ятнадцять", " шістнадцять", " сімнадцять", " вісімнадцять", " дев'ятнадцять" };
        private static readonly string[] Dozens = {"", "", " двадцять", " тридцять", " сорок", " п'ятдесят", " шістдесят", " сімдесят", " вісімдесят", " дев'яносто" };
        private static readonly string[] Hundreds = {"", " cто", " двісті", " триста", " чотириста", " п'ятсот", " шістсот", " сімсот", " вісімсот", " дев'ятсот" };
        private static readonly string[] Degrees = {"", "", "тисяч", "мільйон", "міліард", "триліон", "квадриліон", "квинтиліон" };

        private static readonly Dictionary<int, (string Hryvnas, string Pennies, string Thousands, string OtherDegrees)> Endings = new Dictionary<int, (string, string, string, string)>
        {
            [0] = ("гривень", " копійок", "", "ів"),
            [1] = ("гривня", " копійка", "а", ""),
            [2] = ("гривні", " копійки", "і", "и"),
            [3] = ("гривні", " копійки", "і", "и"),
            [4] = ("гривні", " копійки", "і", "и"),
            [5] = ("гривень", " копійок", "", "ів"),
            [6] = ("гривень", " копійок", "", "ів"),
            [7] = ("гривень", " копійок", "", "ів"),
            [8] = ("гривень", " копійок", "", "ів"),
            [9] = ("гривень", " копійок", "", "ів"),
            [10] = ("гривень", " копійок", "", "ів"),
            [11] = ("гривень", " копійок", "", "ів"),
            [12] = ("гривень", " копійок", "", "ів"),
            [13] = ("гривень", " копійок", "", "ів"),
            [14] = ("гривень", " копійок", "", "ів"),
        };

        public string Write(decimal amount)
        {
            var whole = (int) Math.Truncate(amount);
            var pennies = (int) Math.Truncate((amount - whole) * 100);

            return $"{WriteWhole(whole)}, {WritePennies(pennies)}".Trim();
        }

        private static string WriteWhole(int currency)
        {
            var result = "";
            var degree = 0;
            var currencyEnding = Endings[GetEndingNumber(currency)].Hryvnas;

            if (currency == 0)
                return "нуль " + currencyEnding;

            do
            { 
                degree++; 
                
                var triade = (currency % 1000).ToString("D3"); 
                
                var degreeEnding = string.Empty; 
                if (degree == 2) 
                    degreeEnding = Endings[GetEndingNumber(triade)].Thousands;
                else if (degree > 2) 
                    degreeEnding = Endings[GetEndingNumber(triade)].OtherDegrees; 
                
                result = ConvertTriade(triade) + " " + Degrees[degree] + degreeEnding + result; 
                
                currency /= 1000;
            }
            while (currency != 0);

            return $"{result}" + currencyEnding;
        }

        private static string ConvertTriade(string stringNumber)
        {
            var number = int.Parse(stringNumber);

            var decade = ConvertDecade((number % 100).ToString("D2"));

            return Hundreds[number / 100] + decade;
        }

        private static string ConvertDecade(string stringNumber)
        {
            var number = int.Parse(stringNumber);

            return number % 100 < 20 ? Decade[number] : Dozens[number / 10] + Decade[number % 10];
        }

        private static string WritePennies(int pennies)
        {
            return $"{pennies:D2}{Endings[GetEndingNumber(pennies)].Pennies}";
        }

        private static int GetEndingNumber(int number)
        {
            return number % 100 < 15 ? number % 100 : number % 10;
        }

        private static int GetEndingNumber(string number)
        {
            return GetEndingNumber(int.Parse(number));
        }
    }
}
