﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Application.Extensions;
using NexCore.CurrencyExchange.Configuration;
using NexCore.CurrencyExchange.DataSources.MsSql;
using NexCore.Infrastructure.Extensions;
using NexCore.Infrastructure.Setup;
using NexCore.VehicleConfigurator.Api.Extensions;
using NexCore.VehicleConfigurator.Api.Infrastructure.AutofacModules;
using NexCore.VehicleConfigurator.Api.Setup.Tasks;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.WebApi.Extensions;
using NexCore.WebApi.Filters;
using System;
using System.Reflection;

namespace NexCore.VehicleConfigurator.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var application = typeof(Startup).GetTypeInfo().Assembly;
            var infrastructure = typeof(VehicleConfiguratorContext).Assembly;
            var domain = typeof(Model).GetTypeInfo().Assembly;

            services
                .AddAutoMapper(application)
                .AddTransient<DatabaseSetupTask<CurrencyExchangeContext>>()
                .AddTransient<VehicleConfiguratorDatabaseSetupTask>()
                .AddUnitOfWork<VehicleConfiguratorContext>(Configuration["ConnectionString"], Configuration)
                .AddCurrencyExchange(Configuration["ExchangeRateUrl"], Configuration["CurrencyExchangeConnectionString"])
                .AddHealthChecks(Configuration)
                .AddSwagger(Configuration)
                .AddIntegrationServices(Configuration)
                .AddMvc(options =>
                {
                    options.Filters.Add(typeof(ExceptionFilter));
                })
                .AddControllersAsServices();

            ConfigureAuthentication(services);

            var containerBuilder = services.GetAutofacContainerBuilder(application, domain, infrastructure);

            containerBuilder.RegisterModule(new ApplicationModule());
            containerBuilder.RegisterModule(new VehicleComponentCommandsModule());
            containerBuilder.RegisterModule(new SeederModule(Configuration));
            containerBuilder.RegisterModule(new ExportsModule());

            return new AutofacServiceProvider(containerBuilder.Build());
        }

        // This method is required by testing to change authentication options
        protected virtual void ConfigureAuthentication(IServiceCollection services)
        {
            services.AddJwtBearerAuthentication(Configuration, "NexCore.VehicleConfigurator");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");

                c.OAuthClientId("NexCore.VehicleConfigurator.Swagger");
            });

            app.RunEventBus();

            app.UseMvc();
        }
    }
}
