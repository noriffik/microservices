﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NexCore.VehicleConfigurator.Infrastructure.Dtos.Customers;
using NexCore.VehicleConfigurator.Infrastructure.Dtos.Dealers;

namespace NexCore.VehicleConfigurator.Infrastructure.Dtos.Contracts
{
    [DataContract]
    public class VehicleSaleContentDto
    {
        [DataMember]
        public string Number { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public TotalPriceDto Price { get; set; }

        [DataMember]
        public IEnumerable<PriceDto> Payment { get; set; }

        [DataMember]
        public string VehicleModelName { get; set; }

        [DataMember]
        public string VehicleColorName { get; set; }

        [DataMember]
        public VehicleSpecificationDto Vehicle { get; set; }

        [DataMember]
        public string VehicleStockAddress { get; set; }

        [DataMember]
        public int VehicleDeliveryDays { get; set; }

        [DataMember]
        public PrivateCustomerDto PrivateCustomer { get; set; }

        [DataMember]
        public DealerDto Dealer { get; set; }

        [DataMember]
        public string DealersCity { get; set; }

        [DataMember]
        public string DealersHeadPosition { get; set; }

        [DataMember]
        public PersonNameDto SalesManagerName { get; set; }
    }
}
