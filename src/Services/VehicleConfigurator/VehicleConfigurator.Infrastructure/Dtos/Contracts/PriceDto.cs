﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Infrastructure.Dtos.Contracts
{
    [DataContract]
    public class PriceDto
    {
        [DataMember]
        public decimal Base { get; set; }

        [DataMember]
        public decimal Retail { get; set; }
    }
}
