﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Infrastructure.Dtos.Contracts
{
    [DataContract]
    public class TotalPriceDto
    {
        [DataMember]
        public PriceDto WithoutTax { get; set; }

        [DataMember]
        public PriceDto WithTax { get; set; }

        [DataMember]
        public PriceDto Tax { get; set; }
    }
}
