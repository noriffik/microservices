﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Infrastructure.Dtos.Contracts
{
    [DataContract]
    public class VehicleSpecificationDto
    {
        [DataMember]
        public int ModelYear { get; set; }

        [DataMember]
        public string ModelKey { get; set; }

        [DataMember]
        public string ColorId { get; set; }

        [DataMember]
        public string AdditionalOptions { get; set; }

        [DataMember]
        public string IncludedOptions { get; set; }

        [DataMember]
        public string PackageId { get; set; }
    }
}
