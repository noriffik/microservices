﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using NexCore.VehicleConfigurator.Infrastructure.Dtos.Customers;

namespace NexCore.VehicleConfigurator.Infrastructure.Dtos.Dealers
{
    [DataContract]
    public class CompanyRequisitesDto
    {
        [DataMember]
        public LegalCompanyNameDto Name { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public IEnumerable<string> Telephones { get; set; }

        [DataMember]
        public string BankRequisites { get; set; }

        [DataMember]
        public PersonNameDto Director { get; set; }
    }
}
