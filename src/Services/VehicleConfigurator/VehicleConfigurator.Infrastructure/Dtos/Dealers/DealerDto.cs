﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Infrastructure.Dtos.Dealers
{
    [DataContract]
    public class DealerDto
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember] 
        public int CompanyId { get; set; }

        [DataMember] 
        public string DistributorId { get; set; }

        [DataMember] 
        public string DealerCode { get; set; }

        [DataMember] 
        public CompanyRequisitesDto Requisites { get; set; }
    }
}
