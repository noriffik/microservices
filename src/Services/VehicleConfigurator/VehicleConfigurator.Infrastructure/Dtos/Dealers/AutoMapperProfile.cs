﻿using AutoMapper;
using NexCore.VehicleConfigurator.Domain.Dealers;

namespace NexCore.VehicleConfigurator.Infrastructure.Dtos.Dealers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<DealerDto, Dealer>()
                .ForMember(v => v.DistributorId.Value, m => m.MapFrom(s => s.DistributorId))
                .ForMember(v => v.DealerCode.Value, m => m.MapFrom(s => s.DealerCode))
                .ForMember(v => v.Id.Value, m => m.MapFrom(s => s.Id));
        }
    }
}
