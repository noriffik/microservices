﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Infrastructure.Dtos.Customers
{
    public class PassportDto
    {
        [DataMember]
        public string Series { get; set; }

        [DataMember]
        public string Number { get; set; }

        [DataMember]
        public IssueDto Issue { get; set; }
    }
}
