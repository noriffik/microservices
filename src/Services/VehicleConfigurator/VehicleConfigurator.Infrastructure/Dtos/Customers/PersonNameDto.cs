﻿using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Infrastructure.Dtos.Customers
{
    [DataContract]
    public class PersonNameDto
    {
        [DataMember]
        public string Firstname { get; set; }

        [DataMember]
        public string Lastname { get; set; }

        [DataMember]
        public string Middlename { get; set; }
    }
}
