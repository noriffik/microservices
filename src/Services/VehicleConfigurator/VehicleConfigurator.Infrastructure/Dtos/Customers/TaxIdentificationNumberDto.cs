﻿using System;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Infrastructure.Dtos.Customers
{
    [DataContract]
    public class TaxIdentificationNumberDto
    {
        [DataMember]
        public string Number { get; set; }

        [DataMember]
        public DateTime? RegisteredOn { get; set; }

        [DataMember]
        public IssueDto Issue { get; set; }
    }
}
