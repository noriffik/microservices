﻿using System;
using System.Runtime.Serialization;

namespace NexCore.VehicleConfigurator.Infrastructure.Dtos.Customers
{
    [DataContract]
    public class IssueDto
    {
        [DataMember]
        public string Issuer { get; set; }

        [DataMember]
        public DateTime IssuedOn { get; set; }
    }
}
