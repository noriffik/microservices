﻿using NexCore.Infrastructure;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleConfigurator.Infrastructure
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Used by EF tool")]
    public class VehicleConfiguratorContextDesignTimeFactory : DbContextBaseDesignFactory<VehicleConfiguratorContext>
    {
        public VehicleConfiguratorContextDesignTimeFactory() : base("ConnectionString")
        {
        }
    }
}
