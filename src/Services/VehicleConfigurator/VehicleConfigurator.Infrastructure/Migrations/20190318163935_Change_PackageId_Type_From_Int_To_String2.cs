﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Change_PackageId_Type_From_Int_To_String2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Package",
                schema: "dbo",
                table: "Package");

            migrationBuilder.DropSequence(
                name: "sq_Package",
                schema: "dbo");

            migrationBuilder.DropColumn(
                name: "PackageId",
                schema: "dbo",
                table: "Package");

            migrationBuilder.AddColumn<string>(
                name: "PackageId2",
                schema: "dbo",
                table: "Package",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "PackageId",
                table: "Configurations",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Package",
                schema: "dbo",
                table: "Package",
                column: "PackageId2");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Package",
                schema: "dbo",
                table: "Package");

            migrationBuilder.DropColumn(
                name: "PackageId2",
                schema: "dbo",
                table: "Package");

            migrationBuilder.CreateSequence(
                name: "sq_Package",
                schema: "dbo",
                incrementBy: 10);

            migrationBuilder.AddColumn<int>(
                name: "PackageId",
                schema: "dbo",
                table: "Package",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "PackageId",
                table: "Configurations",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Package",
                schema: "dbo",
                table: "Package",
                column: "PackageId");
        }
    }
}
