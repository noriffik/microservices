﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_PrivateCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PrivateCustomer",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Firstname = table.Column<string>(nullable: false),
                    Lastname = table.Column<string>(nullable: false),
                    Middlename = table.Column<string>(nullable: false),
                    PhysicalAddress = table.Column<string>(nullable: false),
                    PassportSeries = table.Column<string>(nullable: false),
                    PassportNumber = table.Column<string>(nullable: false),
                    PassportIssuer = table.Column<string>(nullable: false),
                    PassportIssuedOn = table.Column<DateTime>(nullable: false),
                    TaxIdentificationNumber = table.Column<string>(nullable: false),
                    TaxIdentificationNumberRegisteredOn = table.Column<DateTime>(nullable: false),
                    TaxIdentificationNumberIssuer = table.Column<string>(nullable: false),
                    TaxIdentificationNumberIssuedOn = table.Column<DateTime>(nullable: false),
                    Telephone = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrivateCustomer", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PrivateCustomer",
                schema: "dbo");
        }
    }
}
