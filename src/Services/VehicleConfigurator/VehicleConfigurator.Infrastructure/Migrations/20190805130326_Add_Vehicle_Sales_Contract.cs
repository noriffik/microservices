﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_Vehicle_Sales_Contract : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_VehicleSaleContract",
                schema: "dbo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "VehicleSaleContracts",
                columns: table => new
                {
                    VehicleSaleContractId = table.Column<int>(nullable: false),
                    Number = table.Column<string>(nullable: true),
                    TypeId = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    DealerId = table.Column<string>(nullable: true),
                    PrivateCustomerId = table.Column<int>(nullable: false),
                    SalesManagerId = table.Column<int>(nullable: false),
                    Content = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleSaleContracts", x => x.VehicleSaleContractId);
                    table.ForeignKey(
                        name: "FK_VehicleSaleContracts_Dealer_DealerId",
                        column: x => x.DealerId,
                        principalSchema: "dbo",
                        principalTable: "Dealer",
                        principalColumn: "DealerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleSaleContracts_PrivateCustomer_PrivateCustomerId",
                        column: x => x.PrivateCustomerId,
                        principalSchema: "dbo",
                        principalTable: "PrivateCustomer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VehicleSaleContracts_DealerId",
                table: "VehicleSaleContracts",
                column: "DealerId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleSaleContracts_PrivateCustomerId",
                table: "VehicleSaleContracts",
                column: "PrivateCustomerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VehicleSaleContracts");

            migrationBuilder.DropSequence(
                name: "sq_VehicleSaleContract",
                schema: "dbo");
        }
    }
}
