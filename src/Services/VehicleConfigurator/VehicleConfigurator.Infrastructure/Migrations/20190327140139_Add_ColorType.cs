﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_ColorType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ColorType",
                schema: "dbo",
                columns: table => new
                {
                    ColorTypeId = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ColorType", x => x.ColorTypeId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ColorType",
                schema: "dbo");
        }
    }
}
