﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using NexCore.VehicleConfigurator.Infrastructure;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    [DbContext(typeof(VehicleConfiguratorContext))]
    [Migration("20190301122921_SetDecimalPrecision")]
    partial class SetDecimalPrecision
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("Relational:Sequence:dbo.sq_Catalog", "'sq_Catalog', 'dbo', '1', '10', '', '', 'Int64', 'False'")
                .HasAnnotation("Relational:Sequence:dbo.sq_Configuration", "'sq_Configuration', 'dbo', '1', '10', '', '', 'Int64', 'False'")
                .HasAnnotation("Relational:Sequence:dbo.sq_ConfigurationVersion", "'sq_ConfigurationVersion', 'dbo', '1', '10', '', '', 'Int64', 'False'")
                .HasAnnotation("Relational:Sequence:dbo.sq_OptionCategory", "'sq_OptionCategory', 'dbo', '1', '10', '', '', 'Int64', 'False'")
                .HasAnnotation("Relational:Sequence:dbo.sq_OptionOffer", "'sq_OptionOffer', 'dbo', '1', '10', '', '', 'Int64', 'False'")
                .HasAnnotation("Relational:Sequence:dbo.sq_Package", "'sq_Package', 'dbo', '1', '10', '', '', 'Int64', 'False'")
                .HasAnnotation("Relational:Sequence:dbo.sq_VehicleOffer", "'sq_VehicleOffer', 'dbo', '1', '10', '', '', 'Int64', 'False'")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Catalogs.Catalog", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("CatalogId")
                        .HasAnnotation("SqlServer:HiLoSequenceName", "sq_Catalog")
                        .HasAnnotation("SqlServer:HiLoSequenceSchema", "dbo")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.SequenceHiLo);

                    b.Property<int>("Status")
                        .HasColumnName("Status");

                    b.HasKey("Id");

                    b.ToTable("Catalogs");
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Catalogs.OptionOffer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("OptionOfferId")
                        .HasAnnotation("SqlServer:HiLoSequenceName", "sq_OptionOffer")
                        .HasAnnotation("SqlServer:HiLoSequenceSchema", "dbo")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.SequenceHiLo);

                    b.Property<string>("RuleSet")
                        .HasColumnName("RuleSet");

                    b.Property<int?>("VehicleOfferId");

                    b.Property<string>("_optionId")
                        .HasColumnName("OptionId");

                    b.HasKey("Id");

                    b.HasIndex("VehicleOfferId");

                    b.HasIndex("_optionId");

                    b.ToTable("OptionOffer","dbo");
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Catalogs.Package", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("PackageId")
                        .HasAnnotation("SqlServer:HiLoSequenceName", "sq_Package")
                        .HasAnnotation("SqlServer:HiLoSequenceSchema", "dbo")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.SequenceHiLo);

                    b.Property<int>("CatalogId");

                    b.Property<string>("Name");

                    b.Property<decimal>("Price")
                        .HasColumnType("decimal(18,2)");

                    b.Property<int>("Status");

                    b.HasKey("Id");

                    b.HasIndex("CatalogId");

                    b.ToTable("Package","dbo");
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Catalogs.VehicleOffer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("VehicleOfferId")
                        .HasAnnotation("SqlServer:HiLoSequenceName", "sq_VehicleOffer")
                        .HasAnnotation("SqlServer:HiLoSequenceSchema", "dbo")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.SequenceHiLo);

                    b.Property<int>("CatalogId");

                    b.Property<decimal>("Price")
                        .HasColumnType("decimal(18,2)");

                    b.Property<string>("_bodyId")
                        .IsRequired()
                        .HasColumnName("BodyId");

                    b.Property<string>("_engineId")
                        .IsRequired()
                        .HasColumnName("EngineId");

                    b.Property<string>("_equipmentId")
                        .IsRequired()
                        .HasColumnName("EquipmentId");

                    b.Property<string>("_gearboxId")
                        .IsRequired()
                        .HasColumnName("GearboxId");

                    b.Property<string>("_modelId")
                        .IsRequired()
                        .HasColumnName("ModelId");

                    b.Property<string>("_modelKey")
                        .IsRequired()
                        .HasColumnName("ModelKey");

                    b.HasKey("Id");

                    b.HasAlternateKey("_modelKey", "CatalogId");

                    b.HasIndex("CatalogId");

                    b.HasIndex("_bodyId");

                    b.HasIndex("_engineId");

                    b.HasIndex("_equipmentId");

                    b.HasIndex("_gearboxId");

                    b.HasIndex("_modelId");

                    b.ToTable("VehicleOffer","dbo");
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Configurations.Configuration", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ConfigurationId")
                        .HasAnnotation("SqlServer:HiLoSequenceName", "sq_Configuration")
                        .HasAnnotation("SqlServer:HiLoSequenceSchema", "dbo")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.SequenceHiLo);

                    b.Property<int>("CatalogId")
                        .HasColumnName("CatalogId");

                    b.Property<int?>("ClientId")
                        .HasColumnName("ClientId");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnName("CreatedAt");

                    b.Property<int?>("PackageId");

                    b.Property<int>("Status")
                        .HasColumnName("Status");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnName("UpdatedAt");

                    b.Property<string>("_modelKey")
                        .IsRequired()
                        .HasColumnName("ModelKey");

                    b.HasKey("Id");

                    b.ToTable("Configurations");
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Configurations.ConfigurationVersion", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ConfigurationVersionId")
                        .HasAnnotation("SqlServer:HiLoSequenceName", "sq_ConfigurationVersion")
                        .HasAnnotation("SqlServer:HiLoSequenceSchema", "dbo")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.SequenceHiLo);

                    b.Property<int>("ConfigurationId")
                        .HasColumnName("ConfigurationId");

                    b.Property<int>("ConfigurationStatus")
                        .HasColumnName("ConfigurationStatus");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnName("CreatedAt");

                    b.HasKey("Id");

                    b.HasIndex("ConfigurationId");

                    b.ToTable("ConfigurationVersion","dbo");
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Vehicles.Body", b =>
                {
                    b.Property<string>("IdValue")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("BodyId");

                    b.Property<string>("Name")
                        .HasColumnName("Name");

                    b.Property<string>("_modelId")
                        .HasColumnName("ModelId");

                    b.HasKey("IdValue");

                    b.HasIndex("_modelId");

                    b.ToTable("Body","dbo");
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Vehicles.Engine", b =>
                {
                    b.Property<string>("IdValue")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("EngineId");

                    b.Property<int?>("FuelType")
                        .HasColumnName("FuelType");

                    b.Property<string>("Name")
                        .HasColumnName("Name");

                    b.Property<string>("_modelId")
                        .HasColumnName("ModelId");

                    b.HasKey("IdValue");

                    b.HasIndex("_modelId");

                    b.ToTable("Engine","dbo");
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Vehicles.Equipment", b =>
                {
                    b.Property<string>("IdValue")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("EquipmentId");

                    b.Property<string>("Name")
                        .HasColumnName("Name");

                    b.Property<string>("_modelId")
                        .HasColumnName("ModelId");

                    b.HasKey("IdValue");

                    b.HasIndex("_modelId");

                    b.ToTable("Equipment","dbo");
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Vehicles.Gearbox", b =>
                {
                    b.Property<string>("IdValue")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("GearboxId");

                    b.Property<int?>("GearBoxCategory")
                        .HasColumnName("GearBoxCategory");

                    b.Property<string>("Name")
                        .HasColumnName("Name");

                    b.Property<string>("Type")
                        .IsRequired();

                    b.Property<string>("_modelId")
                        .HasColumnName("ModelId");

                    b.HasKey("IdValue");

                    b.HasIndex("_modelId");

                    b.ToTable("Gearbox","dbo");
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Vehicles.Model", b =>
                {
                    b.Property<string>("IdValue")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ModelId");

                    b.Property<string>("Name")
                        .HasColumnName("Name");

                    b.HasKey("IdValue");

                    b.ToTable("Model","dbo");
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Vehicles.Option", b =>
                {
                    b.Property<string>("IdValue")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("OptionId");

                    b.Property<string>("Name")
                        .HasColumnName("Name");

                    b.Property<int?>("OptionCategoryId");

                    b.Property<string>("_modelId")
                        .HasColumnName("ModelId");

                    b.Property<string>("_prNumber")
                        .HasColumnName("PrNumber");

                    b.HasKey("IdValue");

                    b.HasIndex("OptionCategoryId");

                    b.HasIndex("_modelId");

                    b.ToTable("Option","dbo");
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Vehicles.OptionCategory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("OptionCategoryId")
                        .HasAnnotation("SqlServer:HiLoSequenceName", "sq_OptionCategory")
                        .HasAnnotation("SqlServer:HiLoSequenceSchema", "dbo")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.SequenceHiLo);

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("OptionCategory","dbo");
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Vehicles.Vehicle", b =>
                {
                    b.Property<string>("IdValue")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("VehicleId");

                    b.Property<string>("Name")
                        .HasColumnName("Name");

                    b.Property<string>("PrNumbers");

                    b.Property<int>("Status")
                        .HasColumnName("Status");

                    b.Property<string>("_bodyId")
                        .HasColumnName("BodyId");

                    b.Property<string>("_engineId")
                        .HasColumnName("EngineId");

                    b.Property<string>("_equipmentId")
                        .HasColumnName("EquipmentId");

                    b.Property<string>("_gearboxId")
                        .HasColumnName("GearboxId");

                    b.Property<string>("_modelId")
                        .HasColumnName("ModelId");

                    b.HasKey("IdValue");

                    b.HasIndex("_bodyId");

                    b.HasIndex("_engineId");

                    b.HasIndex("_equipmentId");

                    b.HasIndex("_gearboxId");

                    b.HasIndex("_modelId");

                    b.ToTable("Vehicle","dbo");
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Catalogs.Catalog", b =>
                {
                    b.OwnsOne("NexCore.Domain.Year", "ModelYear", b1 =>
                        {
                            b1.Property<int?>("CatalogId");

                            b1.Property<int>("Value")
                                .HasColumnName("ModelYear");

                            b1.ToTable("Catalogs");

                            b1.HasOne("NexCore.VehicleConfigurator.Domain.Catalogs.Catalog")
                                .WithOne("ModelYear")
                                .HasForeignKey("NexCore.Domain.Year", "CatalogId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });

                    b.OwnsOne("NexCore.Domain.Period", "RelevancePeriod", b1 =>
                        {
                            b1.Property<int>("CatalogId");

                            b1.Property<DateTime>("From")
                                .HasColumnName("RelevancePeriodFrom");

                            b1.Property<DateTime?>("To")
                                .HasColumnName("RelevancePeriodTo");

                            b1.ToTable("Catalogs");

                            b1.HasOne("NexCore.VehicleConfigurator.Domain.Catalogs.Catalog")
                                .WithOne("RelevancePeriod")
                                .HasForeignKey("NexCore.Domain.Period", "CatalogId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Catalogs.OptionOffer", b =>
                {
                    b.HasOne("NexCore.VehicleConfigurator.Domain.Catalogs.VehicleOffer")
                        .WithMany("_options")
                        .HasForeignKey("VehicleOfferId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Option")
                        .WithMany()
                        .HasForeignKey("_optionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.OwnsOne("NexCore.VehicleConfigurator.Domain.Catalogs.Availability", "Availability", b1 =>
                        {
                            b1.Property<int?>("OptionOfferId");

                            b1.Property<decimal>("Price")
                                .HasColumnName("Price")
                                .HasColumnType("decimal(18,2)");

                            b1.Property<int?>("Reason")
                                .HasColumnName("Reason");

                            b1.Property<int>("Type")
                                .HasColumnName("Type");

                            b1.ToTable("OptionOffer","dbo");

                            b1.HasOne("NexCore.VehicleConfigurator.Domain.Catalogs.OptionOffer")
                                .WithOne("Availability")
                                .HasForeignKey("NexCore.VehicleConfigurator.Domain.Catalogs.Availability", "OptionOfferId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Catalogs.Package", b =>
                {
                    b.HasOne("NexCore.VehicleConfigurator.Domain.Catalogs.Catalog")
                        .WithMany()
                        .HasForeignKey("CatalogId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.OwnsOne("NexCore.VehicleConfigurator.Domain.Vehicles.BoundModelKeySet", "ModelKeySet", b1 =>
                        {
                            b1.Property<int?>("PackageId");

                            b1.Property<string>("AsString")
                                .HasColumnName("ModelKeySet");

                            b1.ToTable("Package","dbo");

                            b1.HasOne("NexCore.VehicleConfigurator.Domain.Catalogs.Package")
                                .WithOne("ModelKeySet")
                                .HasForeignKey("NexCore.VehicleConfigurator.Domain.Vehicles.BoundModelKeySet", "PackageId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });

                    b.OwnsOne("NexCore.VehicleConfigurator.Domain.Vehicles.PrNumberSet", "PrNumberSet", b1 =>
                        {
                            b1.Property<int>("PackageId");

                            b1.Property<string>("AsString")
                                .HasColumnName("PrNumberSet");

                            b1.ToTable("Package","dbo");

                            b1.HasOne("NexCore.VehicleConfigurator.Domain.Catalogs.Package")
                                .WithOne("PrNumberSet")
                                .HasForeignKey("NexCore.VehicleConfigurator.Domain.Vehicles.PrNumberSet", "PackageId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Catalogs.VehicleOffer", b =>
                {
                    b.HasOne("NexCore.VehicleConfigurator.Domain.Catalogs.Catalog")
                        .WithMany()
                        .HasForeignKey("CatalogId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Body")
                        .WithMany()
                        .HasForeignKey("_bodyId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Engine")
                        .WithMany()
                        .HasForeignKey("_engineId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Equipment")
                        .WithMany()
                        .HasForeignKey("_equipmentId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Gearbox")
                        .WithMany()
                        .HasForeignKey("_gearboxId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Model")
                        .WithMany()
                        .HasForeignKey("_modelId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Configurations.Configuration", b =>
                {
                    b.OwnsOne("NexCore.VehicleConfigurator.Domain.Vehicles.PrNumberSet", "Options", b1 =>
                        {
                            b1.Property<int>("ConfigurationId");

                            b1.Property<string>("AsString")
                                .HasColumnName("PrNumberSet");

                            b1.ToTable("Configurations");

                            b1.HasOne("NexCore.VehicleConfigurator.Domain.Configurations.Configuration")
                                .WithOne("Options")
                                .HasForeignKey("NexCore.VehicleConfigurator.Domain.Vehicles.PrNumberSet", "ConfigurationId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Configurations.ConfigurationVersion", b =>
                {
                    b.HasOne("NexCore.VehicleConfigurator.Domain.Configurations.Configuration")
                        .WithMany("_versions")
                        .HasForeignKey("ConfigurationId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Vehicles.Body", b =>
                {
                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Model")
                        .WithMany()
                        .HasForeignKey("_modelId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Vehicles.Engine", b =>
                {
                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Model")
                        .WithMany()
                        .HasForeignKey("_modelId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Vehicles.Equipment", b =>
                {
                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Model")
                        .WithMany()
                        .HasForeignKey("_modelId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Vehicles.Gearbox", b =>
                {
                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Model")
                        .WithMany()
                        .HasForeignKey("_modelId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Vehicles.Option", b =>
                {
                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.OptionCategory")
                        .WithMany()
                        .HasForeignKey("OptionCategoryId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Model")
                        .WithMany()
                        .HasForeignKey("_modelId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("NexCore.VehicleConfigurator.Domain.Vehicles.Vehicle", b =>
                {
                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Body")
                        .WithMany()
                        .HasForeignKey("_bodyId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Engine")
                        .WithMany()
                        .HasForeignKey("_engineId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Equipment")
                        .WithMany()
                        .HasForeignKey("_equipmentId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Gearbox")
                        .WithMany()
                        .HasForeignKey("_gearboxId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Model")
                        .WithMany()
                        .HasForeignKey("_modelId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.OwnsOne("NexCore.Domain.Period", "RelevancePeriod", b1 =>
                        {
                            b1.Property<string>("VehicleIdValue");

                            b1.Property<DateTime>("From")
                                .HasColumnName("Valid_From");

                            b1.Property<DateTime?>("To")
                                .HasColumnName("Valid_To");

                            b1.ToTable("Vehicle","dbo");

                            b1.HasOne("NexCore.VehicleConfigurator.Domain.Vehicles.Vehicle")
                                .WithOne("RelevancePeriod")
                                .HasForeignKey("NexCore.Domain.Period", "VehicleIdValue")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
