﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_Inclusion_To_OptionOffer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Inclusion",
                schema: "dbo",
                table: "OptionOffer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Inclusion",
                schema: "dbo",
                table: "OptionOffer");
        }
    }
}
