﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class RemoveVersions_FromConfiguration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ConfigurationVersion",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "sq_ConfigurationVersion",
                schema: "dbo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_ConfigurationVersion",
                schema: "dbo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "ConfigurationVersion",
                schema: "dbo",
                columns: table => new
                {
                    ConfigurationVersionId = table.Column<int>(nullable: false),
                    ConfigurationId = table.Column<int>(nullable: false),
                    ConfigurationStatus = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConfigurationVersion", x => x.ConfigurationVersionId);
                    table.ForeignKey(
                        name: "FK_ConfigurationVersion_Configurations_ConfigurationId",
                        column: x => x.ConfigurationId,
                        principalTable: "Configurations",
                        principalColumn: "ConfigurationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ConfigurationVersion_ConfigurationId",
                schema: "dbo",
                table: "ConfigurationVersion",
                column: "ConfigurationId");
        }
    }
}
