﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class MakeOptionModelComponent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ModelId",
                schema: "dbo",
                table: "Option",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PrNumber",
                schema: "dbo",
                table: "Option",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ModelId",
                schema: "dbo",
                table: "Option");

            migrationBuilder.DropColumn(
                name: "PrNumber",
                schema: "dbo",
                table: "Option");
        }
    }
}
