﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Drop_Vehicle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Vehicle",
                schema: "dbo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Vehicle",
                schema: "dbo",
                columns: table => new
                {
                    VehicleId = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    PrNumbers = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    BodyId = table.Column<string>(nullable: true),
                    EngineId = table.Column<string>(nullable: true),
                    EquipmentId = table.Column<string>(nullable: true),
                    GearboxId = table.Column<string>(nullable: true),
                    ModelId = table.Column<string>(nullable: true),
                    Valid_From = table.Column<DateTime>(nullable: false),
                    Valid_To = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicle", x => x.VehicleId);
                    table.ForeignKey(
                        name: "FK_Vehicle_Body_BodyId",
                        column: x => x.BodyId,
                        principalSchema: "dbo",
                        principalTable: "Body",
                        principalColumn: "BodyId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vehicle_Engine_EngineId",
                        column: x => x.EngineId,
                        principalSchema: "dbo",
                        principalTable: "Engine",
                        principalColumn: "EngineId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vehicle_Equipment_EquipmentId",
                        column: x => x.EquipmentId,
                        principalSchema: "dbo",
                        principalTable: "Equipment",
                        principalColumn: "EquipmentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vehicle_Gearbox_GearboxId",
                        column: x => x.GearboxId,
                        principalSchema: "dbo",
                        principalTable: "Gearbox",
                        principalColumn: "GearboxId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vehicle_Model_ModelId",
                        column: x => x.ModelId,
                        principalSchema: "dbo",
                        principalTable: "Model",
                        principalColumn: "ModelId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_BodyId",
                schema: "dbo",
                table: "Vehicle",
                column: "BodyId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_EngineId",
                schema: "dbo",
                table: "Vehicle",
                column: "EngineId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_EquipmentId",
                schema: "dbo",
                table: "Vehicle",
                column: "EquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_GearboxId",
                schema: "dbo",
                table: "Vehicle",
                column: "GearboxId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_ModelId",
                schema: "dbo",
                table: "Vehicle",
                column: "ModelId");
        }
    }
}
