﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_Restriction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_Restriction",
                schema: "dbo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Restriction",
                schema: "dbo",
                columns: table => new
                {
                    RestrictionId = table.Column<int>(nullable: false),
                    CatalogId = table.Column<int>(nullable: false),
                    ModelKey = table.Column<string>(nullable: false),
                    PrNumbers = table.Column<string>(nullable: false),
                    State = table.Column<bool>(nullable: false),
                    RelevancePeriodFrom = table.Column<DateTime>(nullable: false),
                    RelevancePeriodTo = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Restriction", x => x.RestrictionId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Restriction",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "sq_Restriction",
                schema: "dbo");
        }
    }
}
