﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class PackageApplicabilityRedesign : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Specification",
                schema: "dbo",
                table: "Package");

            migrationBuilder.AddColumn<string>(
                name: "Applicability",
                schema: "dbo",
                table: "Package",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Applicability",
                schema: "dbo",
                table: "Package");

            migrationBuilder.AddColumn<string>(
                name: "Specification",
                schema: "dbo",
                table: "Package",
                nullable: true);
        }
    }
}
