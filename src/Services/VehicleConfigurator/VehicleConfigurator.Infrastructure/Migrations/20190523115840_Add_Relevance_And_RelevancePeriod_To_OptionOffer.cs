﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_Relevance_And_RelevancePeriod_To_OptionOffer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Relevance",
                schema: "dbo",
                table: "OptionOffer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "RelevancePeriodFrom",
                schema: "dbo",
                table: "OptionOffer",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RelevancePeriodTo",
                schema: "dbo",
                table: "OptionOffer",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Relevance",
                schema: "dbo",
                table: "OptionOffer");

            migrationBuilder.DropColumn(
                name: "RelevancePeriodFrom",
                schema: "dbo",
                table: "OptionOffer");

            migrationBuilder.DropColumn(
                name: "RelevancePeriodTo",
                schema: "dbo",
                table: "OptionOffer");
        }
    }
}
