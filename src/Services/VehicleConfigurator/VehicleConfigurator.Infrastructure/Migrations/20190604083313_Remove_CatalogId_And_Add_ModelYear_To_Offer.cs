﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Remove_CatalogId_And_Add_ModelYear_To_Offer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConfigurationId",
                table: "Offers");

            migrationBuilder.AddColumn<string>(
                name: "BodyId",
                table: "Offers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EngineId",
                table: "Offers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EquipmentId",
                table: "Offers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GearboxId",
                table: "Offers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModelId",
                table: "Offers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ModelYear",
                table: "Offers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Offers_BodyId",
                table: "Offers",
                column: "BodyId");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_EngineId",
                table: "Offers",
                column: "EngineId");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_EquipmentId",
                table: "Offers",
                column: "EquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_GearboxId",
                table: "Offers",
                column: "GearboxId");

            migrationBuilder.CreateIndex(
                name: "IX_Offers_ModelId",
                table: "Offers",
                column: "ModelId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Body_BodyId",
                table: "Offers",
                column: "BodyId",
                principalSchema: "dbo",
                principalTable: "Body",
                principalColumn: "BodyId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Engine_EngineId",
                table: "Offers",
                column: "EngineId",
                principalSchema: "dbo",
                principalTable: "Engine",
                principalColumn: "EngineId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Equipment_EquipmentId",
                table: "Offers",
                column: "EquipmentId",
                principalSchema: "dbo",
                principalTable: "Equipment",
                principalColumn: "EquipmentId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Gearbox_GearboxId",
                table: "Offers",
                column: "GearboxId",
                principalSchema: "dbo",
                principalTable: "Gearbox",
                principalColumn: "GearboxId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Offers_Model_ModelId",
                table: "Offers",
                column: "ModelId",
                principalSchema: "dbo",
                principalTable: "Model",
                principalColumn: "ModelId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Body_BodyId",
                table: "Offers");

            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Engine_EngineId",
                table: "Offers");

            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Equipment_EquipmentId",
                table: "Offers");

            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Gearbox_GearboxId",
                table: "Offers");

            migrationBuilder.DropForeignKey(
                name: "FK_Offers_Model_ModelId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_BodyId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_EngineId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_EquipmentId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_GearboxId",
                table: "Offers");

            migrationBuilder.DropIndex(
                name: "IX_Offers_ModelId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "BodyId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "EngineId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "EquipmentId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "GearboxId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "ModelId",
                table: "Offers");

            migrationBuilder.DropColumn(
                name: "ModelYear",
                table: "Offers");

            migrationBuilder.AddColumn<int>(
                name: "ConfigurationId",
                table: "Offers",
                nullable: false,
                defaultValue: 0);
        }
    }
}
