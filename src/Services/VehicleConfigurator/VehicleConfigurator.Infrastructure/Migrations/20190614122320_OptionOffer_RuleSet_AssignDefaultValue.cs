﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class OptionOffer_RuleSet_AssignDefaultValue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "RuleSet",
                schema: "dbo",
                table: "OptionOffer",
                nullable: true,
                defaultValue: "",
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "RuleSet",
                schema: "dbo",
                table: "OptionOffer",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true,
                oldDefaultValue: "");
        }
    }
}
