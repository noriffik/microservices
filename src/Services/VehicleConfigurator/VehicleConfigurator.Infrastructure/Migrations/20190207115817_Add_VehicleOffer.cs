﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_VehicleOffer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_VehicleOffer",
                schema: "dbo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "VehicleOffer",
                schema: "dbo",
                columns: table => new
                {
                    VehicleOfferId = table.Column<int>(nullable: false),
                    CatalogId = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    BodyId = table.Column<string>(nullable: false),
                    EngineId = table.Column<string>(nullable: false),
                    EquipmentId = table.Column<string>(nullable: false),
                    GearboxId = table.Column<string>(nullable: false),
                    ModelId = table.Column<string>(nullable: false),
                    ModelKey = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleOffer", x => x.VehicleOfferId);
                    table.ForeignKey(
                        name: "FK_VehicleOffer_Catalogs_CatalogId",
                        column: x => x.CatalogId,
                        principalTable: "Catalogs",
                        principalColumn: "CatalogId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleOffer_Body_BodyId",
                        column: x => x.BodyId,
                        principalSchema: "dbo",
                        principalTable: "Body",
                        principalColumn: "BodyId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleOffer_Engine_EngineId",
                        column: x => x.EngineId,
                        principalSchema: "dbo",
                        principalTable: "Engine",
                        principalColumn: "EngineId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleOffer_Equipment_EquipmentId",
                        column: x => x.EquipmentId,
                        principalSchema: "dbo",
                        principalTable: "Equipment",
                        principalColumn: "EquipmentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleOffer_Gearbox_GearboxId",
                        column: x => x.GearboxId,
                        principalSchema: "dbo",
                        principalTable: "Gearbox",
                        principalColumn: "GearboxId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleOffer_Model_ModelId",
                        column: x => x.ModelId,
                        principalSchema: "dbo",
                        principalTable: "Model",
                        principalColumn: "ModelId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VehicleOffer_CatalogId",
                schema: "dbo",
                table: "VehicleOffer",
                column: "CatalogId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleOffer_BodyId",
                schema: "dbo",
                table: "VehicleOffer",
                column: "BodyId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleOffer_EngineId",
                schema: "dbo",
                table: "VehicleOffer",
                column: "EngineId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleOffer_EquipmentId",
                schema: "dbo",
                table: "VehicleOffer",
                column: "EquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleOffer_GearboxId",
                schema: "dbo",
                table: "VehicleOffer",
                column: "GearboxId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleOffer_ModelId",
                schema: "dbo",
                table: "VehicleOffer",
                column: "ModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VehicleOffer",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "sq_VehicleOffer",
                schema: "dbo");
        }
    }
}
