﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_Color : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Color",
                schema: "dbo",
                columns: table => new
                {
                    ColorId = table.Column<string>(nullable: false),
                    Argb = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    TypeId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Color", x => x.ColorId);
                    table.ForeignKey(
                        name: "FK_Color_ColorType_TypeId",
                        column: x => x.TypeId,
                        principalSchema: "dbo",
                        principalTable: "ColorType",
                        principalColumn: "ColorTypeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Color_TypeId",
                schema: "dbo",
                table: "Color",
                column: "TypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Color",
                schema: "dbo");
        }
    }
}
