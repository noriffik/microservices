﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_ForeignKey_ModelId_To_Option : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ModelId",
                schema: "dbo",
                table: "Option",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Option_ModelId",
                schema: "dbo",
                table: "Option",
                column: "ModelId");

            migrationBuilder.AddForeignKey(
                name: "FK_Option_Model_ModelId",
                schema: "dbo",
                table: "Option",
                column: "ModelId",
                principalSchema: "dbo",
                principalTable: "Model",
                principalColumn: "ModelId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Option_Model_ModelId",
                schema: "dbo",
                table: "Option");

            migrationBuilder.DropIndex(
                name: "IX_Option_ModelId",
                schema: "dbo",
                table: "Option");

            migrationBuilder.AlterColumn<string>(
                name: "ModelId",
                schema: "dbo",
                table: "Option",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
