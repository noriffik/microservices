﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateSequence(
                name: "sq_Catalog",
                schema: "dbo",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "sq_OptionCategory",
                schema: "dbo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Catalogs",
                columns: table => new
                {
                    CatalogId = table.Column<int>(nullable: false),
                    ModelYear = table.Column<int>(nullable: false),
                    RelevancePeriodFrom = table.Column<DateTime>(nullable: false),
                    RelevancePeriodTo = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Catalogs", x => x.CatalogId);
                });

            migrationBuilder.CreateTable(
                name: "Model",
                schema: "dbo",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: true),
                    ModelId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Model", x => x.ModelId);
                });

            migrationBuilder.CreateTable(
                name: "OptionCategory",
                schema: "dbo",
                columns: table => new
                {
                    OptionCategoryId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OptionCategory", x => x.OptionCategoryId);
                });

            migrationBuilder.CreateTable(
                name: "Body",
                schema: "dbo",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: true),
                    BodyId = table.Column<string>(nullable: false),
                    ModelId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Body", x => x.BodyId);
                    table.ForeignKey(
                        name: "FK_Body_Model_ModelId",
                        column: x => x.ModelId,
                        principalSchema: "dbo",
                        principalTable: "Model",
                        principalColumn: "ModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Engine",
                schema: "dbo",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: true),
                    FuelType = table.Column<int>(nullable: true),
                    EngineId = table.Column<string>(nullable: false),
                    ModelId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Engine", x => x.EngineId);
                    table.ForeignKey(
                        name: "FK_Engine_Model_ModelId",
                        column: x => x.ModelId,
                        principalSchema: "dbo",
                        principalTable: "Model",
                        principalColumn: "ModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Equipment",
                schema: "dbo",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: true),
                    EquipmentId = table.Column<string>(nullable: false),
                    ModelId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipment", x => x.EquipmentId);
                    table.ForeignKey(
                        name: "FK_Equipment_Model_ModelId",
                        column: x => x.ModelId,
                        principalSchema: "dbo",
                        principalTable: "Model",
                        principalColumn: "ModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Gearbox",
                schema: "dbo",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: false),
                    GearboxId = table.Column<string>(nullable: false),
                    ModelId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gearbox", x => x.GearboxId);
                    table.ForeignKey(
                        name: "FK_Gearbox_Model_ModelId",
                        column: x => x.ModelId,
                        principalSchema: "dbo",
                        principalTable: "Model",
                        principalColumn: "ModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Option",
                schema: "dbo",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: true),
                    OptionCategoryId = table.Column<int>(nullable: true),
                    OptionId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Option", x => x.OptionId);
                    table.ForeignKey(
                        name: "FK_Option_OptionCategory_OptionCategoryId",
                        column: x => x.OptionCategoryId,
                        principalSchema: "dbo",
                        principalTable: "OptionCategory",
                        principalColumn: "OptionCategoryId",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Vehicle",
                schema: "dbo",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: true),
                    PrNumbers = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    Valid_From = table.Column<DateTime>(nullable: false),
                    Valid_To = table.Column<DateTime>(nullable: true),
                    VehicleId = table.Column<string>(nullable: false),
                    BodyId = table.Column<string>(nullable: true),
                    EngineId = table.Column<string>(nullable: true),
                    EquipmentId = table.Column<string>(nullable: true),
                    GearboxId = table.Column<string>(nullable: true),
                    ModelId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicle", x => x.VehicleId);
                    table.ForeignKey(
                        name: "FK_Vehicle_Body_BodyId",
                        column: x => x.BodyId,
                        principalSchema: "dbo",
                        principalTable: "Body",
                        principalColumn: "BodyId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vehicle_Engine_EngineId",
                        column: x => x.EngineId,
                        principalSchema: "dbo",
                        principalTable: "Engine",
                        principalColumn: "EngineId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vehicle_Equipment_EquipmentId",
                        column: x => x.EquipmentId,
                        principalSchema: "dbo",
                        principalTable: "Equipment",
                        principalColumn: "EquipmentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vehicle_Gearbox_GearboxId",
                        column: x => x.GearboxId,
                        principalSchema: "dbo",
                        principalTable: "Gearbox",
                        principalColumn: "GearboxId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vehicle_Model_ModelId",
                        column: x => x.ModelId,
                        principalSchema: "dbo",
                        principalTable: "Model",
                        principalColumn: "ModelId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Body_ModelId",
                schema: "dbo",
                table: "Body",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Engine_ModelId",
                schema: "dbo",
                table: "Engine",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Equipment_ModelId",
                schema: "dbo",
                table: "Equipment",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Gearbox_ModelId",
                schema: "dbo",
                table: "Gearbox",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Option_OptionCategoryId",
                schema: "dbo",
                table: "Option",
                column: "OptionCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_BodyId",
                schema: "dbo",
                table: "Vehicle",
                column: "BodyId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_EngineId",
                schema: "dbo",
                table: "Vehicle",
                column: "EngineId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_EquipmentId",
                schema: "dbo",
                table: "Vehicle",
                column: "EquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_GearboxId",
                schema: "dbo",
                table: "Vehicle",
                column: "GearboxId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_ModelId",
                schema: "dbo",
                table: "Vehicle",
                column: "ModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Catalogs");

            migrationBuilder.DropTable(
                name: "Option",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Vehicle",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "OptionCategory",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Body",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Engine",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Equipment",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Gearbox",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Model",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "sq_Catalog",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "sq_OptionCategory",
                schema: "dbo");
        }
    }
}
