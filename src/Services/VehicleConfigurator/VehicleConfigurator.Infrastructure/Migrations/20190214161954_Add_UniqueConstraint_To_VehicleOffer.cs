﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_UniqueConstraint_To_VehicleOffer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ModelKey",
                schema: "dbo",
                table: "VehicleOffer",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddUniqueConstraint(
                name: "AK_VehicleOffer_ModelKey_CatalogId",
                schema: "dbo",
                table: "VehicleOffer",
                columns: new[] { "ModelKey", "CatalogId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_VehicleOffer_ModelKey_CatalogId",
                schema: "dbo",
                table: "VehicleOffer");

            migrationBuilder.AlterColumn<string>(
                name: "ModelKey",
                schema: "dbo",
                table: "VehicleOffer",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
