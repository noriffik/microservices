﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_Package : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_Package",
                schema: "dbo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Package",
                schema: "dbo",
                columns: table => new
                {
                    PackageId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    CatalogId = table.Column<int>(nullable: false),
                    PrNumberSet = table.Column<string>(nullable: true),
                    ModelKeySet = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Package", x => x.PackageId);
                    table.ForeignKey(
                        name: "FK_Package_Catalogs_CatalogId",
                        column: x => x.CatalogId,
                        principalTable: "Catalogs",
                        principalColumn: "CatalogId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Package_CatalogId",
                schema: "dbo",
                table: "Package",
                column: "CatalogId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Package",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "sq_Package",
                schema: "dbo");
        }
    }
}
