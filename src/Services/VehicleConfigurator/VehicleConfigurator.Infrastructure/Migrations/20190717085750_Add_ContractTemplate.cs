﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_ContractTemplate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_ContractTemplate",
                schema: "dbo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "ContractTemplate",
                schema: "dbo",
                columns: table => new
                {
                    ContractTemplateId = table.Column<int>(nullable: false),
                    TypeId = table.Column<string>(nullable: false),
                    DealerId = table.Column<string>(nullable: true),
                    Content = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractTemplate", x => x.ContractTemplateId);
                    table.ForeignKey(
                        name: "FK_ContractTemplate_ContractType_TypeId",
                        column: x => x.TypeId,
                        principalSchema: "dbo",
                        principalTable: "ContractType",
                        principalColumn: "ContractTypeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ContractTemplate_TypeId",
                schema: "dbo",
                table: "ContractTemplate",
                column: "TypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ContractTemplate",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "sq_ContractTemplate",
                schema: "dbo");
        }
    }
}
