﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_PackageApplicabilitySpecification_To_Package : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PrNumberSet",
                schema: "dbo",
                table: "Package");

            migrationBuilder.RenameColumn(
                name: "PrNumberSet",
                table: "Configurations",
                newName: "Options");

            migrationBuilder.AddColumn<string>(
                name: "Specification",
                schema: "dbo",
                table: "Package",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Specification",
                schema: "dbo",
                table: "Package");

            migrationBuilder.RenameColumn(
                name: "Options",
                table: "Configurations",
                newName: "PrNumberSet");

            migrationBuilder.AddColumn<string>(
                name: "PrNumberSet",
                schema: "dbo",
                table: "Package",
                nullable: true);
        }
    }
}
