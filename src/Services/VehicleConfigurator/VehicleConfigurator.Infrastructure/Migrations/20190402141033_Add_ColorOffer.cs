﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_ColorOffer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_ColorOffer",
                schema: "dbo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "ColorOffer",
                schema: "dbo",
                columns: table => new
                {
                    ColorOfferId = table.Column<int>(nullable: false),
                    VehicleOfferId = table.Column<int>(nullable: true),
                    ColorId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ColorOffer", x => x.ColorOfferId);
                    table.ForeignKey(
                        name: "FK_ColorOffer_VehicleOffer_VehicleOfferId",
                        column: x => x.VehicleOfferId,
                        principalSchema: "dbo",
                        principalTable: "VehicleOffer",
                        principalColumn: "VehicleOfferId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ColorOffer_Color_ColorId",
                        column: x => x.ColorId,
                        principalSchema: "dbo",
                        principalTable: "Color",
                        principalColumn: "ColorId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ColorOffer_VehicleOfferId",
                schema: "dbo",
                table: "ColorOffer",
                column: "VehicleOfferId");

            migrationBuilder.CreateIndex(
                name: "IX_ColorOffer_ColorId",
                schema: "dbo",
                table: "ColorOffer",
                column: "ColorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ColorOffer",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "sq_ColorOffer",
                schema: "dbo");
        }
    }
}
