﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_RelevancePeriod_To_ColorOffer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "RelevancePeriodFrom",
                schema: "dbo",
                table: "ColorOffer",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RelevancePeriodTo",
                schema: "dbo",
                table: "ColorOffer",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RelevancePeriodFrom",
                schema: "dbo",
                table: "ColorOffer");

            migrationBuilder.DropColumn(
                name: "RelevancePeriodTo",
                schema: "dbo",
                table: "ColorOffer");
        }
    }
}
