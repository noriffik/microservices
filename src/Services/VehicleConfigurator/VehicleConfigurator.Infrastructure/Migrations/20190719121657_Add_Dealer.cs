﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_Dealer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Dealer",
                schema: "dbo",
                columns: table => new
                {
                    DealerId = table.Column<string>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    ImporterId = table.Column<string>(nullable: false),
                    DealerCode = table.Column<string>(nullable: false),
                    RequisitesShortName = table.Column<string>(nullable: true),
                    RequisitesFullName = table.Column<string>(nullable: true),
                    RequisitesAddress = table.Column<string>(nullable: true),
                    BankRequisites = table.Column<string>(nullable: true),
                    RequisitesTelephones = table.Column<string>(nullable: true),
                    DirectorFirstname = table.Column<string>(nullable: true),
                    DirectorLastname = table.Column<string>(nullable: true),
                    DirectorMiddlename = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dealer", x => x.DealerId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Dealer",
                schema: "dbo");
        }
    }
}
