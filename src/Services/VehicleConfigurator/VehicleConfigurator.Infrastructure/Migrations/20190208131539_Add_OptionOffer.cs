﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Add_OptionOffer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_OptionOffer",
                schema: "dbo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "OptionOffer",
                schema: "dbo",
                columns: table => new
                {
                    OptionOfferId = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Reason = table.Column<int>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    VehicleOfferId = table.Column<int>(nullable: true),
                    OptionId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OptionOffer", x => x.OptionOfferId);
                    table.ForeignKey(
                        name: "FK_OptionOffer_VehicleOffer_VehicleOfferId",
                        column: x => x.VehicleOfferId,
                        principalSchema: "dbo",
                        principalTable: "VehicleOffer",
                        principalColumn: "VehicleOfferId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OptionOffer_Option_OptionId",
                        column: x => x.OptionId,
                        principalSchema: "dbo",
                        principalTable: "Option",
                        principalColumn: "OptionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OptionOffer_VehicleOfferId",
                schema: "dbo",
                table: "OptionOffer",
                column: "VehicleOfferId");

            migrationBuilder.CreateIndex(
                name: "IX_OptionOffer_OptionId",
                schema: "dbo",
                table: "OptionOffer",
                column: "OptionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OptionOffer",
                schema: "dbo");

            migrationBuilder.DropSequence(
                name: "sq_OptionOffer",
                schema: "dbo");
        }
    }
}
