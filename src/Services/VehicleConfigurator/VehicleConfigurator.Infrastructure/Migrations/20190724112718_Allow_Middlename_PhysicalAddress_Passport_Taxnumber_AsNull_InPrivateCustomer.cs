﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Allow_Middlename_PhysicalAddress_Passport_Taxnumber_AsNull_InPrivateCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "TaxIdentificationNumberIssuer",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<DateTime>(
                name: "TaxIdentificationNumberIssuedOn",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "PassportIssuer",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<DateTime>(
                name: "PassportIssuedOn",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "PhysicalAddress",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<DateTime>(
                name: "TaxIdentificationNumberRegisteredOn",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "TaxIdentificationNumber",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Middlename",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "PassportSeries",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "PassportNumber",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: true,
                oldClrType: typeof(string));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "TaxIdentificationNumberIssuer",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "TaxIdentificationNumberIssuedOn",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PassportIssuer",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "PassportIssuedOn",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PhysicalAddress",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "TaxIdentificationNumberRegisteredOn",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "TaxIdentificationNumber",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Middlename",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PassportSeries",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PassportNumber",
                schema: "dbo",
                table: "PrivateCustomer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
