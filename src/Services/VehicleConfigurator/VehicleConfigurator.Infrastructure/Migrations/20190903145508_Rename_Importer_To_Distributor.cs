﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleConfigurator.Infrastructure.Migrations
{
    public partial class Rename_Importer_To_Distributor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImporterId",
                schema: "dbo",
                table: "Dealer");

            migrationBuilder.AddColumn<string>(
                name: "DistributorId",
                schema: "dbo",
                table: "Dealer",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DistributorId",
                schema: "dbo",
                table: "Dealer");

            migrationBuilder.AddColumn<string>(
                name: "ImporterId",
                schema: "dbo",
                table: "Dealer",
                nullable: false,
                defaultValue: "");
        }
    }
}
