﻿using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Infrastructure.Views
{
    public class OfferVehicleView
    {
        public int OfferId { get; set; }

        public string ModelKey { get; set; }

        public Model Model { get; set; }

        public Body Body { get; set; }

        public Equipment Equipment { get; set; }

        public Engine Engine { get; set; }

        public Gearbox Gearbox { get; set; }
    }
}
