﻿using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;

namespace NexCore.VehicleConfigurator.Infrastructure.Views
{
    public class OfferedVehicleView
    {
        public int Id { get; set; }

        public int CatalogId { get; set; }

        public string ModelKey { get; set; }

        public decimal Price { get; set; }

        public Relevance Relevance { get; set; }

        public DateTime? RelevancePeriodFrom { get; set; }

        public DateTime? RelevancePeriodTo { get; set; }

        public VehicleOfferStatus Status { get; set; }

        public Model Model { get; set; }

        public Body Body { get; set; }

        public Equipment Equipment { get; set; }

        public Engine Engine { get; set; }

        public Gearbox Gearbox { get; set; }

        public string DefaultColorId { get; set; }
    }
}
