﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.Infrastructure;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Customers;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Catalogs;
using NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Configurations;
using NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Contracts;
using NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Customers;
using NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Dealers;
using NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Offers;
using NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Restrictions;
using NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure.Views;
using System;
using System.Linq;
using System.Reflection;
using AutoMapper;
using NexCore.VehicleConfigurator.Domain.Contracts;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;

namespace NexCore.VehicleConfigurator.Infrastructure
{
    public class VehicleConfiguratorContext : UnitOfWorkDbContext
    {
        public const string Schema = "dbo";

        //TODO : This is temporary solution
        private IApplicabilityParser _applicabilityParser;

        public IApplicabilityParser ApplicabilityParser
        {
            get => _applicabilityParser ?? (_applicabilityParser = new ApplicabilityParser());
            set => _applicabilityParser = value ?? throw new ArgumentNullException(nameof(value));
        }

        private IMapper _mapper;
        public IMapper Mapper
        {
            get => _mapper ?? (_mapper = new MapperConfiguration(c => c.AddProfiles(Assembly.GetEntryAssembly())).CreateMapper());
            set => _mapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public DbSet<Model> Models { get; set; }
        public DbSet<Body> Bodies { get; set; }
        public DbSet<Equipment> Equipments { get; set; }
        public DbSet<Engine> Engines { get; set; }
        public DbSet<Gearbox> Gearboxes { get; set; }
        public DbSet<Option> Options { get; set; }
        public DbSet<OptionCategory> OptionCategories { get; set; }
        public DbSet<Catalog> Catalogs { get; set; }
        public DbSet<OptionOffer> OptionOffers { get; set; }
        public DbSet<ColorOffer> ColorOffers { get; set; }
        public DbSet<VehicleOffer> VehicleOffers { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<ColorType> ColorTypes { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<Restriction> Restrictions { get; set; }
        public DbSet<PrivateCustomer> PrivateCustomers { get; set; }
        public DbSet<Dealer> Dealers { get; set; }
        public DbSet<VehicleSaleContract> VehicleSaleContracts { get; set; }
        public DbSet<ContractTemplate> ContractTemplate { get; set; }
        public DbSet<ContractType> ContractType { get; set; }

        public VehicleConfiguratorContext(DbContextOptions<VehicleConfiguratorContext> options)
            : base(options)
        {
        }

        public VehicleConfiguratorContext(DbContextOptions<VehicleConfiguratorContext> options, IMediator mediator)
            : base(options, mediator)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new BodyEntityTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new EquipmentEntityTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new EngineEntityTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new GearBoxEntityTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new OptionEntityTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new ModelEntityTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new OptionCategoryEntityTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new CatalogEntityTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new VehicleOfferTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new OptionOfferEntityTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new PackageEntityTypeConfiguration(ApplicabilityParser, Schema));
            modelBuilder.ApplyConfiguration(new ConfigurationTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new OfferTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new ColorTypeEntityTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new ColorEntityTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new ColorOfferEntityTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new RestrictionTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new PrivateCustomerEntityTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new ContractTypeTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new ContractTemplateTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new VehicleSaleContractTypeConfiguration(Schema, Mapper));
            modelBuilder.ApplyConfiguration(new DealerEntityTypeConfiguration(Schema));
            modelBuilder.ApplyConfiguration(new EmployeeEntityTypeConfiguration(Schema));
            modelBuilder.Query(VehicleOfferVehicleViewQuery);
            modelBuilder.Query(OfferVehicleViewQuery);
        }

        private Action<QueryTypeBuilder<OfferedVehicleView>> VehicleOfferVehicleViewQuery =>
            b => b.ToQuery(() =>
                from offer in VehicleOffers
                join model in Models on Convert.ToString(offer.ModelId)
                    equals Convert.ToString(model.Id)
                join body in Bodies on Convert.ToString(offer.BodyId)
                    equals Convert.ToString(body.Id)
                join equipment in Equipments on Convert.ToString(offer.EquipmentId)
                    equals Convert.ToString(equipment.Id)
                join engine in Engines on Convert.ToString(offer.EngineId)
                    equals Convert.ToString(engine.Id)
                join gearbox in Gearboxes on Convert.ToString(offer.GearboxId)
                    equals Convert.ToString(gearbox.Id)
                select new OfferedVehicleView
                {
                    Id = offer.Id,
                    CatalogId = offer.CatalogId,
                    ModelKey = offer.ModelKey,
                    Price = offer.Price,
                    Relevance = offer.Relevance,
                    RelevancePeriodFrom = offer.RelevancePeriodFrom,
                    RelevancePeriodTo = offer.RelevancePeriodTo,
                    Status = offer.Status,
                    Model = model,
                    Body = body,
                    Equipment = equipment,
                    Engine = engine,
                    Gearbox = gearbox,
                    DefaultColorId = offer.DefaultColorId
                });

        private Action<QueryTypeBuilder<OfferVehicleView>> OfferVehicleViewQuery =>
            b => b.ToQuery(() =>
                from offer in Offers
                join model in Models on Convert.ToString(offer.ModelId)
                    equals Convert.ToString(model.Id)
                join body in Bodies on Convert.ToString(offer.BodyId)
                    equals Convert.ToString(body.Id)
                join equipment in Equipments on Convert.ToString(offer.EquipmentId)
                    equals Convert.ToString(equipment.Id)
                join engine in Engines on Convert.ToString(offer.EngineId)
                    equals Convert.ToString(engine.Id)
                join gearbox in Gearboxes on Convert.ToString(offer.GearboxId)
                    equals Convert.ToString(gearbox.Id)
                select new OfferVehicleView
                {
                    OfferId = offer.Id,
                    ModelKey = offer.ModelKey,
                    Model = model,
                    Body = body,
                    Equipment = equipment,
                    Engine = engine,
                    Gearbox = gearbox
                });
    }
}
