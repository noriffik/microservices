﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Domain.Dealers;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Dealers
{
    public class EmployeeEntityTypeConfiguration : IEntityTypeConfiguration<Employee>
    {
        private readonly string _schema;

        public EmployeeEntityTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.ToTable(nameof(Employee), _schema);

            builder.HasKey(c => c.Id);
            builder.Property(p => p.Id).ValueGeneratedNever();

            builder.OwnsOne(p => p.PersonName, n =>
            {
                n.Property(p => p.Firstname).HasColumnName(nameof(PersonName.Firstname)).IsRequired();
                n.Property(p => p.Lastname).HasColumnName(nameof(PersonName.Lastname)).IsRequired();
                n.Property(p => p.Middlename).HasColumnName(nameof(PersonName.Middlename));
            });

            builder.Property(c => c.DealerId)
                .HasConversion(p => p.Value, p => p)
                .IsRequired();
        }
    }
}
