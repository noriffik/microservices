﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Domain.Dealers;
using System;
using System.Linq;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Dealers
{
    public class DealerEntityTypeConfiguration : IEntityTypeConfiguration<Dealer>
    {
        private readonly string _schema;

        public DealerEntityTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<Dealer> builder)
        {
            builder.ToTable(nameof(Dealer), _schema);

            builder.Property(x => x.Id)
                .HasConversion(x => x.Value, x => x)
                .HasColumnName(nameof(Dealer) + "Id")
                .IsRequired();

            builder.Property(c => c.CompanyId).IsRequired();
            builder.Property(c => c.DistributorId)
                .HasConversion(p => p.Value, p => p)
                .IsRequired();
            builder.Property(c => c.DealerCode)
                .HasConversion(p => p.Value, p => p)
                .IsRequired();

            builder.OwnsOne(c => c.Requisites, r =>
            {
                r.OwnsOne(p => p.Name, n =>
                {
                    n.Property(p => p.ShortName).HasColumnName(nameof(Dealer.Requisites) + nameof(LegalOrganizationName.ShortName));
                    n.Property(p => p.FullName).HasColumnName(nameof(Dealer.Requisites) + nameof(LegalOrganizationName.FullName));
                });
                r.Property(p => p.Address).HasColumnName(nameof(Dealer.Requisites) + nameof(OrganizationRequisites.Address));
                r.Property(p => p.BankRequisites).HasColumnName(nameof(OrganizationRequisites.BankRequisites));
                r.OwnsOne(p => p.Director, d =>
                {
                    d.Property(p => p.Firstname).HasColumnName(nameof(OrganizationRequisites.Director) + nameof(LegalPersonName.Firstname));
                    d.Property(p => p.Lastname).HasColumnName(nameof(OrganizationRequisites.Director) + nameof(LegalPersonName.Lastname));
                    d.Property(p => p.Middlename).HasColumnName(nameof(OrganizationRequisites.Director) + nameof(LegalPersonName.Middlename));
                });
                r.Property(p => p.Telephones).HasColumnName(nameof(Dealer.Requisites) + nameof(OrganizationRequisites.Telephones))
                    .HasConversion(t => t.Any() ? t.Aggregate((a, b) => $"{a},{b}") : "", s => s.Split(',', StringSplitOptions.RemoveEmptyEntries));
            });
        }
    }
}
