﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleConfigurator.Domain.Configurations;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Configurations
{
    public class ConfigurationTypeConfiguration : IEntityTypeConfiguration<Configuration>
    {
        private readonly string _schema;

        public ConfigurationTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<Configuration> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Configuration), _schema)
                .HasColumnName(nameof(Configuration) + "Id");

            builder.Property(c => c.ModelKey)
                .HasConversion(k => k.Value, k => k);

            builder.Property(m => m.CatalogId)
                .HasColumnName(nameof(Configuration.CatalogId));
            builder.Property(m => m.CreatedAt)
                .HasColumnName(nameof(Configuration.CreatedAt));
            builder.Property(m => m.UpdatedAt)
                .HasColumnName(nameof(Configuration.UpdatedAt));
            builder.Property(m => m.Status)
                .HasColumnName(nameof(Configuration.Status));

            builder.Property(c => c.Options)
                .HasConversion(o => o.AsString, o => o);

            //Package
            builder.Ignore(m => m.IsPackageIncluded);

            builder.Property(c => c.PackageId)
                .HasConversion(i => i.Value, i => i)
                .IsRequired(false);

            builder.Property(c => c.ColorId)
                .HasConversion(i => i.Value, i => i)
                .IsRequired();
        }
    }
}
