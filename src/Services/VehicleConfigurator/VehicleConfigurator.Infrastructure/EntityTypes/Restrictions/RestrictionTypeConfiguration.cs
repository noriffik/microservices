﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleConfigurator.Domain.Restrictions;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Restrictions
{
    public class RestrictionTypeConfiguration : IEntityTypeConfiguration<Restriction>
    {
        private readonly string _schema;

        public RestrictionTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<Restriction> builder)
        {
            builder.ToTable(nameof(Restriction), _schema);

            builder.HasKey(r => r.Id);
            builder.Property(v => v.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Restriction), _schema)
                .HasColumnName(nameof(Restriction) + "Id");

            builder.OwnsOne(r => r.ModelKey).Property(vo => vo.Value)
                .HasColumnName(nameof(Restriction.ModelKey))
                .IsRequired();
            builder.OwnsOne(r => r.PrNumbers, prNumbers =>
            {
                prNumbers.Property(n => n.AsString).HasColumnName(nameof(Restriction.PrNumbers)).IsRequired();
                prNumbers.Ignore(n => n.Values);
            });

            builder.Property(r => r.CatalogId).HasColumnName(nameof(Restriction.CatalogId));
            builder.Property("RelevancePeriodFrom").HasColumnName("RelevancePeriodFrom");
            builder.Property("RelevancePeriodTo").HasColumnName("RelevancePeriodTo");
            builder.Property(r => r.IsEnabled).HasColumnName(nameof(Restriction.IsEnabled));
            builder.Property("_description").HasColumnName("Description");

            builder.Ignore(r => r.Description);
        }
    }
}
