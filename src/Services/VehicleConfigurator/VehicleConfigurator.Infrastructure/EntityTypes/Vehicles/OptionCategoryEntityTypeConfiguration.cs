﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles
{
    class OptionCategoryEntityTypeConfiguration : IEntityTypeConfiguration<OptionCategory>
    {
        private readonly string _schema;

        public OptionCategoryEntityTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<OptionCategory> builder)
        {
            builder.ToTable(nameof(OptionCategory), _schema);

            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(OptionCategory), _schema)
                .HasColumnName(nameof(OptionCategory) + "Id");
        }
    }
}
