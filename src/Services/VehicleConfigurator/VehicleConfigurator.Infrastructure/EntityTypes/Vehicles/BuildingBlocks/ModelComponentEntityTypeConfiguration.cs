﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles.BuildingBlocks
{
    class ModelComponentEntityTypeConfiguration<TComponent, TCode> : ComponentEntityTypeConfiguration<TComponent, TCode>
        where TComponent : ModelComponent<TCode>
        where TCode : EntityId
    {
        public ModelComponentEntityTypeConfiguration(string name, Func<string, TCode> parseId, string schema)
            : base(name, parseId, schema)
        {
        }

        public override void Configure(EntityTypeBuilder<TComponent> builder)
        {
            base.Configure(builder);

            builder.Property(c => c.ModelId)
                .HasConversion(i => i.Value, i => i)
                .IsRequired();
        }
    }
}
