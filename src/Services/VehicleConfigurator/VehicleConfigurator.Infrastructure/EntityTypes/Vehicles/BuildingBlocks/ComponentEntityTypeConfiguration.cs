﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using System;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles.BuildingBlocks
{
    abstract class ComponentEntityTypeConfiguration<TComponent, TCode> : IEntityTypeConfiguration<TComponent>
        where TComponent : Component<TCode>
        where TCode : EntityId
    {
        protected readonly string Name;
        protected Func<string, TCode> ParseId;
        private readonly string _schema;

        protected ComponentEntityTypeConfiguration(string name, Func<string, TCode> parseId, string schema)
        {
            Name = name;
            ParseId = parseId;
            _schema = schema;
        }

        public virtual void Configure(EntityTypeBuilder<TComponent> builder)
        {
            builder.ToTable(Name, _schema);

            builder.Property(x => x.Id)
                .HasConversion(x => x.Value, x => ParseId(x))
                .HasColumnName(typeof(TComponent).Name + "Id")
                .IsRequired();
            
            builder.Property(c => c.Name)
                .HasColumnName("Name");
        }
    }
}
