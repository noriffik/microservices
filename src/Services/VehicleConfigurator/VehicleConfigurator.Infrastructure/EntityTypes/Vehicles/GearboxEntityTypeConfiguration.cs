﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles
{
    class GearBoxEntityTypeConfiguration : ModelComponentEntityTypeConfiguration<Gearbox, GearboxId>
    {
        public GearBoxEntityTypeConfiguration(string schema) : base(nameof(Gearbox), GearboxId.Parse, schema)
        {
        }

        public override void Configure(EntityTypeBuilder<Gearbox> builder)
        {
            base.Configure(builder);

            builder.Property(g => g.Type)
                .HasConversion(t => t.Value, v => v)
                .IsRequired();
        }
    }
}
