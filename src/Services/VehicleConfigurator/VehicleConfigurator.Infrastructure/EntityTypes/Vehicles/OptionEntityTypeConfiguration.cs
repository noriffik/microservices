﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles
{
    class OptionEntityTypeConfiguration : ModelComponentEntityTypeConfiguration<Option, OptionId>
    {
        public OptionEntityTypeConfiguration(string schema) : base(nameof(Option), OptionId.Parse, schema)
        {
        }

        public override void Configure(EntityTypeBuilder<Option> builder)
        {
            base.Configure(builder);

            builder.Property(o => o.PrNumber)
                .HasConversion(p => p.Value, p => p);

            builder.HasOne<OptionCategory>()
                .WithMany()
                .HasForeignKey(o => o.OptionCategoryId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Property(o => o.OptionCategoryId)
                .IsRequired(false);
        }
    }
}