﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles
{
    class BodyEntityTypeConfiguration : ModelComponentEntityTypeConfiguration<Body, BodyId>
    {
        public BodyEntityTypeConfiguration(string schema) : base(nameof(Body), BodyId.Parse, schema)
        {
        }
    }
}
