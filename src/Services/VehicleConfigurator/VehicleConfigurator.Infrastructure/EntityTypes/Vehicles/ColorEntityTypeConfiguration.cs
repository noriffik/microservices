﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles
{
    public class ColorEntityTypeConfiguration : IEntityTypeConfiguration<Color>
    {
        private readonly string _schema;

        public ColorEntityTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<Color> builder)
        {
            builder.ToTable(nameof(Color), _schema);

            //Id
            builder.Property(x => x.Id)
                .HasConversion(x => x.Value, x => x)
                .HasColumnName(typeof(Color).Name + "Id")
                .IsRequired();

            //ColorTypeId
            builder.Property(x => x.TypeId)
                .HasConversion(i => i.Value, i => i);

            builder.HasOne<ColorType>()
                .WithMany()
                .HasForeignKey(x => x.TypeId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            //Color
            builder.Property(c => c.Argb)
                .HasConversion(c => c.ToArgb(), v => System.Drawing.Color.FromArgb(v));
        }
    }
}
