﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles
{
    class EngineEntityTypeConfiguration : ModelComponentEntityTypeConfiguration<Engine, EngineId>
    {
        public EngineEntityTypeConfiguration(string schema) : base(nameof(Engine), EngineId.Parse, schema)
        {
        }
    }
}