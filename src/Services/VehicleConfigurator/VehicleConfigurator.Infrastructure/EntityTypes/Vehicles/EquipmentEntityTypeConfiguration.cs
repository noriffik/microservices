﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles
{
    class EquipmentEntityTypeConfiguration : ModelComponentEntityTypeConfiguration<Equipment, EquipmentId>
    {
        public EquipmentEntityTypeConfiguration(string schema) : base(nameof(Equipment), EquipmentId.Parse, schema)
        {
        }
    }
}