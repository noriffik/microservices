﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles
{
    public class ColorTypeEntityTypeConfiguration : IEntityTypeConfiguration<ColorType>
    {
        private readonly string _schema;

        public ColorTypeEntityTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<ColorType> builder)
        {
            builder.ToTable(nameof(ColorType), _schema);

            builder.Property(x => x.Id)
                .HasConversion(x => x.Value, x => x)
                .HasColumnName(typeof(ColorType).Name + "Id")
                .IsRequired();
        }
    }
}
