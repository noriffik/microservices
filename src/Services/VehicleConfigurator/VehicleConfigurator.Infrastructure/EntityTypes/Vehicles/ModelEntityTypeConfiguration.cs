﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Vehicles
{
    class ModelEntityTypeConfiguration : ComponentEntityTypeConfiguration<Model, ModelId>
    {
        public ModelEntityTypeConfiguration(string schema) : base(nameof(Model), ModelId.Parse, schema)
        {
        }

        public override void Configure(EntityTypeBuilder<Model> builder)
        {
            base.Configure(builder);
            
            HasComponents<Body>(builder);
            HasComponents<Equipment>(builder);
            HasComponents<Engine>(builder);
            HasComponents<Gearbox>(builder);
            HasComponents<Option>(builder);
        }

        private static void HasComponents<TComponent>(EntityTypeBuilder<Model> builder)
            where TComponent : class, IModelComponent
        {
            builder.HasMany<TComponent>()
                .WithOne()
                .HasForeignKey(c => c.ModelId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
