﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Domain.Customers;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Customers
{
    public class PrivateCustomerEntityTypeConfiguration : IEntityTypeConfiguration<PrivateCustomer>
    {
        private readonly string _schema;

        public PrivateCustomerEntityTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<PrivateCustomer> builder)
        {
            builder.ToTable(nameof(PrivateCustomer), _schema);

            builder.HasKey(c => c.Id);

            builder.Property(p => p.Id)
                .ValueGeneratedNever();

            builder.OwnsOne(p => p.PersonName, n =>
            {
                n.Property(p => p.Firstname).HasColumnName(nameof(PersonName.Firstname)).IsRequired();
                n.Property(p => p.Lastname).HasColumnName(nameof(PersonName.Lastname)).IsRequired();
                n.Property(p => p.Middlename).HasColumnName(nameof(PersonName.Middlename));
            });

            builder.Property(c => c.PhysicalAddress).HasColumnName(nameof(PrivateCustomer.PhysicalAddress));

            builder.OwnsOne(p => p.Passport, n =>
            {
                n.Property(p => p.Series).HasColumnName(nameof(PrivateCustomer.Passport) + nameof(Passport.Series));
                n.Property(p => p.Number).HasColumnName(nameof(Passport) + nameof(Passport.Number));
                n.OwnsOne(p => p.Issue, i =>
                {
                    i.Property(p => p.Issuer).HasColumnName(nameof(Passport) + nameof(Issue.Issuer));
                    i.Property(p => p.IssuedOn).HasColumnName(nameof(Passport) + nameof(Issue.IssuedOn));
                });
            });

            builder.OwnsOne(p => p.TaxIdentificationNumber, n =>
            {
                n.Property(p => p.Number).HasColumnName(nameof(PrivateCustomer.TaxIdentificationNumber));
                n.Property(p => p.RegisteredOn).HasColumnName(nameof(PrivateCustomer.TaxIdentificationNumber) + nameof(PrivateCustomer.TaxIdentificationNumber.RegisteredOn));
                n.OwnsOne(p => p.Issue, i =>
                {
                    i.Property(p => p.Issuer).HasColumnName(nameof(PrivateCustomer.TaxIdentificationNumber) + nameof(Issue.Issuer));
                    i.Property(p => p.IssuedOn).HasColumnName(nameof(PrivateCustomer.TaxIdentificationNumber) + nameof(Issue.IssuedOn));
                });
            });

            builder.Property(c => c.Telephone).HasColumnName(nameof(PrivateCustomer.Telephone)).IsRequired();
        }
    }
}
