﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Catalogs
{
    class VehicleOfferTypeConfiguration : IEntityTypeConfiguration<VehicleOffer>
    {
        private readonly string _schema;

        public VehicleOfferTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<VehicleOffer> builder)
        {
            builder.ToTable(nameof(VehicleOffer), _schema);

            //Property Id
            builder.HasKey(v => v.Id);
            builder.Property(v => v.Id)
                    .ForSqlServerUseSequenceHiLo("sq_" + nameof(VehicleOffer), _schema)
                    .HasColumnName(nameof(VehicleOffer) + "Id");

            //Properties ModelKey

            builder.Property(v => v.ModelKey)
                .HasConversion(k => k.Value, k => k);

            builder.Property(v => v.ModelId)
                .HasConversion(i => i.Value, i => i);

            builder.HasOne<Model>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(v => v.BodyId)
                .HasConversion(i => i.Value, i => i);

            builder.HasOne<Body>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(v => v.EquipmentId)
                .HasConversion(i => i.Value, i => i);

            builder.HasOne<Equipment>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(v => v.EngineId)
                .HasConversion(i => i.Value, i => i);

            builder.HasOne<Engine>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(v => v.GearboxId)
                .HasConversion(i => i.Value, i => i);

            builder.HasOne<Gearbox>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            //Property CatalogId
            builder.HasOne<Catalog>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(v => v.Price)
                .HasColumnType("decimal(18,2)");

            builder.HasAlternateKey(nameof(VehicleOffer.ModelKey), nameof(VehicleOffer.CatalogId));
            
            //PR numbers
            builder.Property(v => v.PrNumbers)
                .HasConversion(v => v.AsString, v => v)
                .HasColumnName(nameof(VehicleOffer.PrNumbers));

            //Option offers
            builder.Ignore(v => v.Options);
            builder.HasMany<OptionOffer>("_options").WithOne().OnDelete(DeleteBehavior.Cascade);
            builder.Metadata.FindNavigation("_options").IsEagerLoaded = true;

            //Color offers
            builder.Ignore(c => c.Colors);
            builder.Ignore(c => c.RelevantColors);
            builder.HasMany<ColorOffer>("_colors").WithOne().OnDelete(DeleteBehavior.Cascade);
            builder.Metadata.FindNavigation("_colors").IsEagerLoaded = true;

            //Default Color
            builder.Property(c => c.DefaultColorId)
                .HasConversion(i => i.Value, i => i);
        }
    }
}
