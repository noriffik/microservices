﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Catalogs
{
    public class ColorOfferEntityTypeConfiguration : IEntityTypeConfiguration<ColorOffer>
    {
        private readonly string _schema;

        public ColorOfferEntityTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<ColorOffer> builder)
        {
            builder.ToTable(nameof(ColorOffer), _schema);

            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(ColorOffer), _schema)
                .HasColumnName(nameof(ColorOffer) + "Id");

            builder.Property(c => c.ColorId)
                .HasConversion(i => i.Value, i => i);

            builder.HasOne<Color>()
                .WithMany()
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
