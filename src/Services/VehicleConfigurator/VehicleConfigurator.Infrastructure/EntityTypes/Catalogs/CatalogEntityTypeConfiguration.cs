﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Catalogs
{
    public class CatalogEntityTypeConfiguration : IEntityTypeConfiguration<Catalog>
    {
        private readonly string _schema;

        public CatalogEntityTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<Catalog> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Catalog), _schema)
                .HasColumnName(nameof(Catalog) + "Id");

            builder.Property(c => c.Status)
                .HasColumnName(nameof(Catalog.Status));

            builder.Property(c => c.ModelYear)
                .HasConversion(y => y.Value, y => y);

            builder.Ignore(c => c.RelevancePeriod);
            builder.Property("RelevancePeriodFrom")
                .HasColumnName("RelevancePeriodFrom");
            builder.Property("RelevancePeriodTo")
                .HasColumnName("RelevancePeriodTo");
        }
    }
}
