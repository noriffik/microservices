﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using System;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Catalogs
{
    class PackageEntityTypeConfiguration : IEntityTypeConfiguration<Package>
    {
        private readonly IApplicabilityParser _parser;
        private readonly string _schema;

        public PackageEntityTypeConfiguration(IApplicabilityParser parser, string schema)
        {
            _parser = parser ?? throw new ArgumentNullException(nameof(parser));
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<Package> builder)
        {
            builder.ToTable(nameof(Package), _schema);
            
            builder.Property(x => x.Id)
                .HasConversion(x => x.Value, x => x)
                .HasColumnName(typeof(Package).Name + "Id")
                .IsRequired();

            builder.HasOne<Catalog>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(p => p.Price)
                .HasColumnType("decimal(18,2)");

            builder.OwnsOne(p => p.ModelKeySet, m =>
            {
                m.Property(s => s.AsString).HasColumnName(nameof(Package.ModelKeySet));
                m.Ignore(s => s.Values);
                m.Ignore(s => s.ModelId);
            });

            builder.Property(p => p.Applicability)
                .HasConversion(s => s.AsString, s => _parser.Parse(s));
        }
    }
}
