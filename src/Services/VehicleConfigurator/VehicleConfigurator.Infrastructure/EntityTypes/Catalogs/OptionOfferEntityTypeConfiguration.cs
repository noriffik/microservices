﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Catalogs
{
    public class OptionOfferEntityTypeConfiguration : IEntityTypeConfiguration<OptionOffer>
    {
        private readonly string _schema;

        public OptionOfferEntityTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<OptionOffer> builder)
        {
            builder.ToTable(nameof(OptionOffer), _schema);

            builder.HasKey(o => o.Id);
            builder.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(OptionOffer), _schema)
                .HasColumnName(nameof(OptionOffer) + "Id");

            builder.Property(o => o.RuleSet)
                .HasDefaultValue(string.Empty);

            builder.Property(o => o.OptionId)
                .HasConversion(i => i.Value, i => i);

            builder.HasOne<Option>()
                .WithMany()
                .HasForeignKey(o => o.OptionId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.OwnsOne(e => e.Availability, a =>
            {
                a.Property(p => p.Type)
                    .HasColumnName(nameof(Availability.Type));
                a.Property(p => p.Reason)
                    .HasColumnName(nameof(Availability.Reason));
                a.Property(p => p.Price)
                    .HasColumnName(nameof(Availability.Price))
                    .HasColumnType("decimal(18,2)");
            });

            builder.Property(o => o.RuleSet);
        }
    }
}
