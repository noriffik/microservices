﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Offers
{
    public class OfferTypeConfiguration : IEntityTypeConfiguration<Offer>
    {
        private readonly string _schema;

        public OfferTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<Offer> builder)
        {
            builder.HasKey(o => o.Id);

            builder.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Offer), _schema)
                .HasColumnName(nameof(Offer) + "Id");

            builder.Property(o => o.PrivateCustomerId)
                .HasColumnName(nameof(Offer.PrivateCustomerId));

            builder.Property(o => o.DealerId)
                .HasColumnName(nameof(Offer.DealerId))
                .IsRequired();
            
            builder.Property(o => o.ModelYear)
                .HasConversion(y => y.Value, k => k)
                .IsRequired();

            builder.Property(o => o.PackageId)
                .HasConversion(i => i.Value, i => i);

            builder.Property(o => o.Options)
                .HasConversion(o => o.AsString, o => o);

            builder.Property(o => o.IncludedOptions)
                .HasConversion(o => o.AsString, o => o);

            builder.Property(o => o.ColorId)
                .HasConversion(i => i.Value, i => i);

            builder.Property(o => o.Price).HasConversion(
                    v => JsonConvert.SerializeObject(v),
                    v => JsonConvert.DeserializeObject<ConfigurationPrice>(v))
                .HasColumnName(nameof(Offer.Price));

            builder.Property(o => o.CreatedAt)
                .HasColumnName(nameof(Offer.CreatedAt));

            builder.Property(o => o.Status)
                .HasColumnName(nameof(Offer.Status));

            //Properties ModelKey

            builder.Property(o => o.ModelKey)
                .HasConversion(k => k.Value, k => k);

            builder.Property(v => v.ModelId)
                .HasConversion(i => i.Value, i => i);

            builder.HasOne<Model>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(v => v.BodyId)
                .HasConversion(i => i.Value, i => i);

            builder.HasOne<Body>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(v => v.EquipmentId)
                .HasConversion(i => i.Value, i => i);

            builder.HasOne<Equipment>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(v => v.EngineId)
                .HasConversion(i => i.Value, i => i);

            builder.HasOne<Engine>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(v => v.GearboxId)
                .HasConversion(i => i.Value, i => i);

            builder.HasOne<Gearbox>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
