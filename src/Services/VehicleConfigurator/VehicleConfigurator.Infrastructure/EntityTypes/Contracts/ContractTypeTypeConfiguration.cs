﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleConfigurator.Domain.Contracts;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Contracts
{
    public class ContractTypeTypeConfiguration: IEntityTypeConfiguration<ContractType>
    {
        private readonly string _schema;

        public ContractTypeTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<ContractType> builder)
        {
            builder.ToTable(nameof(ContractType), _schema);

            builder.Property(x => x.Id)
                .HasConversion(x => x.Value, x => x)
                .HasColumnName(typeof(ContractType).Name + "Id")
                .IsRequired();
        }
    }
}
