﻿using System;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Domain.Customers;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.Infrastructure.Dtos.Contracts;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Contracts
{
    public class VehicleSaleContractTypeConfiguration: IEntityTypeConfiguration<VehicleSaleContract>
    {
        private readonly string _schema;
        private readonly IMapper _mapper;

        public VehicleSaleContractTypeConfiguration(string schema, IMapper mapper)
        {
            _schema = schema;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public void Configure(EntityTypeBuilder<VehicleSaleContract> builder)
        {
            builder.HasKey(o => o.Id);

            //Id
            builder.Property(o => o.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(VehicleSaleContract), _schema)
                .HasColumnName(nameof(VehicleSaleContract) + "Id");

            //ColorTypeId
            builder.Property(x => x.TypeId)
                .HasConversion(i => i.Value, i => i);

            builder.Property(o => o.Date)
                .HasColumnName(nameof(VehicleSaleContract.Date));

            builder.HasOne<Dealer>().WithMany().HasForeignKey(e => e.DealerId);
            builder.Property(v => v.DealerId)
                .HasConversion(i => i.Value, i => i);
            
            builder.HasOne<PrivateCustomer>().WithMany().HasForeignKey(e => e.PrivateCustomerId);
            builder.Property(o => o.PrivateCustomerId)
                .HasColumnName(nameof(VehicleSaleContract.PrivateCustomerId));

            builder.Property(o => o.SalesManagerId)
                .HasColumnName(nameof(VehicleSaleContract.SalesManagerId));

            builder.Property(o => o.Content).HasConversion(
                    v => JsonConvert.SerializeObject(_mapper.Map<VehicleSaleContentDto>(v)),
                    v => _mapper.Map<VehicleSaleContent>(JsonConvert.DeserializeObject<VehicleSaleContentDto>(v)))
                .HasColumnName(nameof(VehicleSaleContract.Content));
        }
    }
}
