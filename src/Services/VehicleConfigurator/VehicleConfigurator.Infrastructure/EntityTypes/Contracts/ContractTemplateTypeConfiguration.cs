﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleConfigurator.Domain.Contracts;

namespace NexCore.VehicleConfigurator.Infrastructure.EntityTypes.Contracts
{
    public class ContractTemplateTypeConfiguration : IEntityTypeConfiguration<ContractTemplate>
    {
        private readonly string _schema;

        public ContractTemplateTypeConfiguration(string schema)
        {
            _schema = schema;
        }

        public void Configure(EntityTypeBuilder<ContractTemplate> builder)
        {
            builder.ToTable(nameof(ContractTemplate), _schema);

            //Id
            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(ContractTemplate), _schema)
                .HasColumnName(nameof(ContractTemplate) + "Id");

            //ColorTypeId
            builder.Property(x => x.TypeId)
                .HasConversion(i => i.Value, i => i);

            builder.HasOne<ContractType>()
                .WithMany()
                .HasForeignKey(x => x.TypeId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
            
            builder.Property(m => m.DealerId)
                .HasColumnName(nameof(ContractTemplate.DealerId));

            builder.Property(m => m.Content)
                .HasColumnName(nameof(ContractTemplate.Content));
        }
    }
}
