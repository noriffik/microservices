﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Worker.Tasks
{
    public class ServiceProviderTaskActivator : ITaskActivator
    {
        private readonly IServiceProvider _serviceProvider;

        public ServiceProviderTaskActivator(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task Perform<TTask>() where TTask : ITask
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var job = scope.ServiceProvider.GetRequiredService<TTask>();

                await job.Perform();
            }
        }

        public async Task Perform(Type type)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));

            using (var scope = _serviceProvider.CreateScope())
            {
                if (!(scope.ServiceProvider.GetRequiredService(type) is ITask task))
                    throw new ArgumentException("Must be implementing ITask", nameof(type));

                await task.Perform();
            }
        }
    }
}