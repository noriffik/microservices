﻿using MediatR;
using Microsoft.Extensions.Logging;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Worker.Tasks
{
    public class ActualizeRelevanceTask<TEntity> : ITask
    {
        private readonly IMediator _mediator;
        private readonly ILogger<ActualizeRelevanceTask<TEntity>> _logger;

        public ActualizeRelevanceTask(IMediator mediator, ILogger<ActualizeRelevanceTask<TEntity>> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        public async Task Perform()
        {
            _logger.LogInformation($"This is a job for actualize {typeof(TEntity).Name} relevances according relevance period.");

            await _mediator.Send(new ActualizeRelevanceCommand<TEntity>());

            _logger.LogInformation($"{typeof(TEntity).Name} relevances are actualized.");
        }
    }
}
