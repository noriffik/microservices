﻿using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Worker.Tasks
{
    public interface ITaskActivator
    {
        Task Perform<TJob>() where TJob : ITask;

        Task Perform(Type type);
    }
}