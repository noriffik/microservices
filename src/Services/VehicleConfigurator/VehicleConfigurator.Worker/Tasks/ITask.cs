﻿using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Worker.Tasks
{
    public interface ITask
    {
        Task Perform();
    }
}
