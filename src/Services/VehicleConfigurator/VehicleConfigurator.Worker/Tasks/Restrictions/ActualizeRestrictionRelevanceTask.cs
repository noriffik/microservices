﻿using MediatR;
using Microsoft.Extensions.Logging;
using NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Worker.Tasks.Restrictions
{
    public class ActualizeRestrictionRelevanceTask : ITask
    {
        private readonly IMediator _mediator;
        private readonly ILogger<ActualizeRestrictionRelevanceTask> _logger;

        public ActualizeRestrictionRelevanceTask(IMediator mediator, ILogger<ActualizeRestrictionRelevanceTask> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        public async Task Perform()
        {
            _logger.LogInformation($"This is a job for actualize {typeof(Restriction).Name} relevances.");

            await _mediator.Send(new ActualizeRelevanceCommand());

            _logger.LogInformation($"{typeof(Restriction).Name} relevances are actualized.");
        }
    }
}
