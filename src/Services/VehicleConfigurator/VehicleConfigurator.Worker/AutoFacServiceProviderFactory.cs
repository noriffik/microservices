﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Application.AutofacModules;
using NexCore.VehicleConfigurator.Api;
using NexCore.VehicleConfigurator.Api.Infrastructure.AutofacModules;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using System.Reflection;

namespace NexCore.VehicleConfigurator.Worker
{
    public class AutoFacServiceProviderFactory : IServiceProviderFactory<ContainerBuilder>
    {
        public ContainerBuilder CreateBuilder(IServiceCollection services)
        {
            var container = new ContainerBuilder();

            container.Populate(services);

            return container;
        }

        public IServiceProvider CreateServiceProvider(ContainerBuilder containerBuilder)
        {
            var application = typeof(Startup).GetTypeInfo().Assembly;
            var domain = typeof(Model).GetTypeInfo().Assembly;

            containerBuilder.RegisterModule(new MediatorModule(application, domain));
            containerBuilder.RegisterModule(new ApplicationModule());

            return new AutofacServiceProvider(containerBuilder.Build());
        }
    }
}