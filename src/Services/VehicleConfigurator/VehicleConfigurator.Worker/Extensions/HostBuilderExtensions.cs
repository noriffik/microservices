﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System.IO;
using System.Reflection;

namespace NexCore.VehicleConfigurator.Worker.Extensions
{
    public static class HostBuilderExtensions
    {
        public static IHostBuilder ConfigureApp(this IHostBuilder builder, string[] args)
        {
            return builder.ConfigureHostConfiguration(config =>
                {
                    config.SetBasePath(Directory.GetCurrentDirectory())
                        .AddEnvironmentVariables("VEHICLE_CONFIGURATOR_WORKER_")
                        .AddCommandLine(args);
                })
                .ConfigureAppConfiguration((hostContext, config) =>
                {
                    hostContext.HostingEnvironment.ApplicationName = typeof(Program).Assembly.GetName().Name;

                    var env = hostContext.HostingEnvironment;

                    config.AddJsonFile("appsettings.json", true, true)
                        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true);

                    if (env.IsDevelopment())
                    {
                        var appAssembly = Assembly.Load(new AssemblyName(env.ApplicationName));
                        if (appAssembly != null)
                        {
                            config.AddUserSecrets(appAssembly, true);
                        }
                    }

                    config.AddEnvironmentVariables();
                });
        }
    }
}
