﻿using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.Extensions.DependencyInjection;
using NexCore.VehicleConfigurator.Worker.Services;
using NexCore.VehicleConfigurator.Worker.Tasks;
using System.Linq;
using System.Reflection;

namespace NexCore.VehicleConfigurator.Worker.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddRecurringTasks(this IServiceCollection services, Assembly assembly)
        {
            services.AddHostedService<RecurringTasksService>();
            services.AddTransient<ITaskActivator, ServiceProviderTaskActivator>();
            services.AddTasks(assembly);
            
            services.AddHangfire((provider, config) => config.UseMemoryStorage());

            services.AddHangfireServer();
            
            return services;
        }

        public static IServiceCollection AddTasks(this IServiceCollection services, Assembly assembly)
        {
            assembly.GetTypes()
                .Where(t => t.IsClass)
                .Where(t => !t.IsAbstract)
                .Where(t => typeof(ITask).IsAssignableFrom(t))
                .ToList()
                .ForEach(t => services.AddTransient(t));
            
            return services;
        }
    }
}
