﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NexCore.CurrencyExchange.Configuration;
using NexCore.Infrastructure.Extensions;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.Worker.Extensions;
using NexCore.VehicleConfigurator.Worker.Tasks;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Worker
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = new HostBuilder()
                .ConfigureApp(args)
                .UseServiceProviderFactory(new AutoFacServiceProviderFactory())
                .ConfigureServices((hostContext, services) =>
                {
                    var configuration = hostContext.Configuration;

                    services.AddUnitOfWork<VehicleConfiguratorContext>(configuration["ConnectionString"], configuration);
                    services.AddCurrencyExchange(configuration["ExchangeRateUrl"],
                        configuration["CurrencyExchangeConnectionString"]);
                    services.AddRecurringTasks(typeof(ITask).Assembly);
                })
                .ConfigureLogging((hostContext, logging) =>
                {
                    logging.AddConfiguration(hostContext.Configuration.GetSection("Logging"));
                    logging.AddConsole();
                    logging.AddDebug();
                })
                .Build();

            await host.RunAsync();
        }
    }
}
