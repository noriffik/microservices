﻿using Hangfire;
using Microsoft.Extensions.Hosting;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Worker.Tasks;
using NexCore.VehicleConfigurator.Worker.Tasks.Restrictions;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.Worker.Services
{
    public class RecurringTasksService : BackgroundService
    {
        private readonly IRecurringJobManager _recurringJobs;
        private readonly ITaskActivator _taskActivator;

        public RecurringTasksService(IRecurringJobManager recurringJobs, ITaskActivator taskActivator)
        {
            _recurringJobs = recurringJobs ?? throw new ArgumentNullException(nameof(recurringJobs));
            _taskActivator = taskActivator ?? throw new ArgumentNullException(nameof(taskActivator));
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            AddOrUpdateTask<ActualizeRelevanceTask<OptionOffer>>("30 0 * * *");
            AddOrUpdateTask<ActualizeRelevanceTask<ColorOffer>>("25 0 * * *");
            AddOrUpdateTask<ActualizeRelevanceTask<VehicleOffer>>("* 0 * * *");
            AddOrUpdateTask<ActualizeRestrictionRelevanceTask>("* 0 * * *");

            return Task.CompletedTask;
        }

        private void AddOrUpdateTask<TTask>(string cronExpression) where TTask : ITask
        {
            var taskType = typeof(TTask);

            _recurringJobs.AddOrUpdate(taskType.FullName, () => _taskActivator.Perform(taskType), cronExpression);
        }
    }
}
