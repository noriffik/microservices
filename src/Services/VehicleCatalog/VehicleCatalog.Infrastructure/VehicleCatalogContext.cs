﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Infrastructure;

namespace NexCore.VehicleCatalog.Infrastructure
{
    public class VehicleCatalogContext : UnitOfWorkDbContext
    {
        public const string Schema = "catalog";

        public VehicleCatalogContext(DbContextOptions options) : base(options)
        {
        }

        public VehicleCatalogContext(DbContextOptions options, IMediator mediator) : base(options, mediator)
        {
        }
    }
}
