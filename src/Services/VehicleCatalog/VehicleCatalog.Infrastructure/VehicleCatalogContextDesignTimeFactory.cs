﻿using NexCore.Infrastructure;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleCatalog.Infrastructure
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Used by EF tool")]
    public class VehicleCatalogContextDesignTimeFactory : DbContextBaseDesignFactory<VehicleCatalogContext>
    {
        public VehicleCatalogContextDesignTimeFactory() : base("ConnectionString")
        {
        }
    }
}
