﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NexCore.Application.Extensions;
using NexCore.Infrastructure.Extensions;
using NexCore.VehicleCatalog.Api.Extensions;
using NexCore.VehicleCatalog.Api.Setup.AutofacModules;
using NexCore.VehicleCatalog.Api.Setup.Tasks;
using NexCore.VehicleCatalog.Infrastructure;
using NexCore.WebApi;
using NexCore.WebApi.Extensions;
using System;

namespace NexCore.VehicleCatalog.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddControllersAsServices();

            services
                .AddTransient<VehicleCatalogDatabaseSetupTask>()
                .AddTransient<IntegrationEventLogDatabaseSetupTask>()
                .AddTransient<IConfigureOptions<MvcOptions>, MvcDefaultOptionsSetup>()
                .AddUnitOfWork<VehicleCatalogContext>(Configuration["ConnectionString"], Configuration)
                .AddHealthChecks(Configuration)
                .AddSwagger(Configuration)
                .AddIntegrationServices(Configuration);

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            ConfigureAuthentication(services);

            var containerBuilder = services.GetAutofacContainerBuilder();

            containerBuilder.RegisterModule(new EventBusModule());
            containerBuilder.RegisterModule(new IntegrationEventPublishersModule());

            return new AutofacServiceProvider(containerBuilder.Build());
        }

        // This method is required by testing to change authentication options
        protected virtual void ConfigureAuthentication(IServiceCollection services)
        {
            services.AddJwtBearerAuthentication(Configuration, "NexCore.VehicleCatalog");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");

                c.OAuthClientId("NexCore.VehicleCatalog.Swagger");
            });

            app.UseMvc();
        }
    }
}
