﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NexCore.Application.Setup;
using NexCore.EventBus.EventLog;
using Polly;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace NexCore.VehicleCatalog.Api.Setup.Tasks
{
    public class IntegrationEventLogDatabaseSetupTask : IApplicationSetupTask
    {
        private readonly IIntegrationEventLogDbContextFactory _dbContextFactory;
        private readonly IConfiguration _configuration;

        public string ConnectionStringKey { get; set; } = "ConnectionString";

        public IntegrationEventLogDatabaseSetupTask(IIntegrationEventLogDbContextFactory dbContextFactory, IConfiguration configuration)
        {
            _dbContextFactory = dbContextFactory ?? throw new ArgumentNullException(nameof(dbContextFactory));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public async Task Execute()
        {
            var connectionString = _configuration.GetValue<string>(ConnectionStringKey);
            if (connectionString == null)
                throw new InvalidOperationException(
                    $@"Required ""{ConnectionStringKey}"" connection string key in configuration is missing.");

            await Policy.Handle<SqlException>()
                .WaitAndRetry(new[]
                {
                    TimeSpan.FromSeconds(3),
                    TimeSpan.FromSeconds(5),
                    TimeSpan.FromSeconds(8)
                })
                .Execute(async () =>
                {
                    using (var dbContext = _dbContextFactory.Create(connectionString))
                    {
                        await dbContext.Database.MigrateAsync();
                    }
                });
        }
    }
}
