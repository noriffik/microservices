﻿using Microsoft.Extensions.Configuration;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Setup;
using NexCore.VehicleCatalog.Infrastructure;
using System;

namespace NexCore.VehicleCatalog.Api.Setup.Tasks
{
    public class VehicleCatalogDatabaseSetupTask : DatabaseSetupTask<VehicleCatalogContext>
    {
        private const string IsSeedingEnabledKey = "IsSeedingEnabled";

        public VehicleCatalogDatabaseSetupTask(VehicleCatalogContext context) : base(context)
        {
        }

        public VehicleCatalogDatabaseSetupTask(VehicleCatalogContext context, IDbContextSeeder<VehicleCatalogContext> seeder, 
            IConfiguration configuration) : base(context, seeder)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            
            IsSeedingEnabled = configuration.GetValue(IsSeedingEnabledKey, true);
        }
    }
}
