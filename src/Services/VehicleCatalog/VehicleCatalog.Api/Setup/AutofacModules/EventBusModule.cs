﻿using Autofac;
using NexCore.EventBus.Abstract;
using NexCore.EventBus.EventLog;
using NexCore.Infrastructure;

namespace NexCore.VehicleCatalog.Api.Setup.AutofacModules
{
    public class EventBusModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<InMemoryIntegrationEventStorage>()
                .As<IIntegrationEventStorage>()
                .InstancePerLifetimeScope();

            builder.RegisterType<IntegrationEventLogDbContextSqlServerFactory>()
                .As<IIntegrationEventLogDbContextFactory>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UnitOfWorkDbContextPlugin>().As<IUnitOfWorkDbContextPlugin>();
        }
    }
}
