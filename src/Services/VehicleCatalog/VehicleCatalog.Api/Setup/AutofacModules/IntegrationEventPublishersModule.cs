﻿using Autofac;
using MediatR.Pipeline;
using NexCore.Application.Processors;

namespace NexCore.VehicleCatalog.Api.Setup.AutofacModules
{
    public class IntegrationEventPublishersModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(IntegrationEventPublishProcessor<,>))
                .As(typeof(IRequestPostProcessor<,>));
        }
    }
}
