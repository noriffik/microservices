﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using NexCore.VehicleCatalog.Api.Setup.Tasks;
using NexCore.WebApi.Extensions;
using System.Threading.Tasks;

namespace NexCore.VehicleCatalog.Api
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            await host.Setup<IntegrationEventLogDatabaseSetupTask>();
            await host.Setup<VehicleCatalogDatabaseSetupTask>();

            await host.RunAsync();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseHealthChecks("/hc");
    }
}
