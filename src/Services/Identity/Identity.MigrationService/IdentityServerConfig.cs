﻿using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using ApiResource = IdentityServer4.Models.ApiResource;
using Client = IdentityServer4.Models.Client;
using IdentityResource = IdentityServer4.Models.IdentityResource;
using Secret = IdentityServer4.Models.Secret;

namespace NexCore.Identity.MigrationService
{
    public class IdentityServerConfig
    {
        private readonly IConfiguration _configuration;

        public IdentityServerConfig(IConfiguration configuration)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public IEnumerable<ApiResource> GetApiResources()
        {
	        var claimTypes = new[]
	        {
		        "role", "person_info",
		        "employee_info",
		        "importer_employee_info",
		        "dealer_employee_info",
		        "private_customer_info"
	        };

            return new[]
            {
                new ApiResource("NexCore.SalesCore.Web.HttpAggregator", "SalesCore Web Http Aggregator", claimTypes),
                new ApiResource("NexCore.ContactManager", "Управление контактами", claimTypes),
                new ApiResource("NexCore.VehicleCatalog", "Каталог автомобилей", claimTypes),
                new ApiResource("NexCore.VehicleConfigurator", "Конфигуратор автомобилей", claimTypes),
                new ApiResource("NexCore.VehicleStock", "Склад автомобилей", claimTypes),
				new ApiResource("NexCore.Identity.Api","Управление пользователями", claimTypes)
            };
        }

        private ICollection<string> GetScopes()
        {
            var identityResources = GetIdentityResources().Select(r => r.Name);
            var apiResources = GetApiResources().Select(r => r.Name);

            return identityResources.Concat(apiResources).ToList();
        }

        public IEnumerable<Client> GetClients()
        {
            var urls = GetClientUrls();
            var scopes = GetScopes();

            return new[]
                {
                    CreateIdentityAdminClient(urls["NexCore.Identity.Admin"]),
                    CreateIdentityApiClient()
                }
                .Concat(CreateSalesCoreClients(urls["NexCore.SalesCore.Web"], scopes))
                .Concat(CreateSwaggerClients(urls, scopes));
        }

        private IEnumerable<Client> CreateSalesCoreClients(string urls, ICollection<string> scopes)
        {
            var urlList = urls.Split(',').ToList();
            for (var i = 0; i < urlList.Count; i++)
            {
                yield return CreateSalesCoreClient(i, urlList[i], scopes);
            }
        }

        private Client CreateSalesCoreClient(int id, string baseUrl, ICollection<string> scopes)
        {
            return new Client
            {
                ClientId = $"NexCore.SalesCore.Web_{id}",
                ClientName = "NexCore SalesCore",
                ClientUri = baseUrl,
                AllowedGrantTypes = GrantTypes.Implicit,
                AllowAccessTokensViaBrowser = true,
                RequireConsent = false,
                RedirectUris = { $"{baseUrl}/oidc-callback" , $"{baseUrl}/silent-renew-oidc.html" },
                PostLogoutRedirectUris = { $"{baseUrl}/" },
                AllowedCorsOrigins = { baseUrl },
                AllowedScopes = scopes,
                AllowOfflineAccess = _configuration.GetValue<bool>("Clients:NexCore_SalesCore_Web:AllowOfflineAccess"),
                AccessTokenLifetime = _configuration.GetValue("Clients:NexCore_SalesCore_Web:AccessTokenLifetime", 3600), // 1 hour
                AbsoluteRefreshTokenLifetime = 0,
                RefreshTokenUsage = TokenUsage.OneTimeOnly,
                RefreshTokenExpiration = TokenExpiration.Sliding,
                UpdateAccessTokenClaimsOnRefresh = true
            };
        }

        private static Client CreateIdentityApiClient()
        {
            return new Client
            {
                ClientId = "NexCore.Identity.Api",
                AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                ClientSecrets =
                {
                    new Secret("secret".Sha256())
                },
                AllowedScopes = { "NexCore.Identity.Api" }
            };
        }

        private static Client CreateIdentityAdminClient(string baseUrl)
        {
            return new Client
            {
                ClientId = "NexCore.Identity.Admin",
                ClientName = "NexCore Identity Admin",
                ClientSecrets = new List<Secret>
                {
                    new Secret("secret".Sha256())
                },
                ClientUri = baseUrl,
                AllowedGrantTypes = GrantTypes.Hybrid,
                RedirectUris = new List<string>
                {
                    $"{baseUrl}/signin-oidc"
                },
                FrontChannelLogoutUri = $"{baseUrl}/signout-oidc",
                PostLogoutRedirectUris = new List<string>
                {
                    $"{baseUrl}/signout-callback-oidc"
                },
                AlwaysIncludeUserClaimsInIdToken = true,
                AllowAccessTokensViaBrowser = false,
                AllowOfflineAccess = true,

                AlwaysSendClientClaims = true,

                AllowedScopes = new List<string>
                {
                    IdentityServerConstants.StandardScopes.OpenId,
                    IdentityServerConstants.StandardScopes.Profile,
                    IdentityServerConstants.StandardScopes.Email,
                    "roles",
                    "person_info",
                    "importer_employee_info",
                    "dealer_employee_info",
                    "private_customer_info"
                }
            };
        }

        private static IEnumerable<Client> CreateSwaggerClients(IDictionary<string, string> urls, IEnumerable<string> scopes)
        {
            return new[]
            {
                CreateSwaggerClient(
                    "NexCore.SalesCore.Web.HttpAggregator",
                    "SalesCore Web Http Aggregator Swagger UI",
                    urls["NexCore.SalesCore.Web.HttpAggregator"], scopes.ToArray()),
                CreateSwaggerClient(
                    "NexCore.ContactManager.Swagger",
                    "Contact Manager Service Swagger UI",
                    urls["NexCore.ContactManager"], "NexCore.ContactManager"),
                CreateSwaggerClient(
                    "NexCore.VehicleCatalog.Swagger",
                    "Vehicle Catalog Service Swagger UI",
                    urls["NexCore.VehicleCatalog"], "NexCore.VehicleCatalog", "openid", "roles"),
                CreateSwaggerClient(
                    "NexCore.VehicleConfigurator.Swagger",
                    "Vehicle Configurator Service Swagger UI",
                    urls["NexCore.VehicleConfigurator"], "NexCore.VehicleConfigurator", "openid", "roles"),
                CreateSwaggerClient(
                    "NexCore.VehicleStock.Swagger",
                    "Vehicle Stock Service Swagger UI",
                    urls["NexCore.VehicleStock"], "NexCore.VehicleStock", "openid", "roles"),
                CreateSwaggerClient(
                    "NexCore.Identity.Api.Swagger",
                    "User Management Swagger UI",
                    urls["NexCore.Identity.Api"], "NexCore.Identity.Api")
            };
        }

        private static Client CreateSwaggerClient(string id, string name, string url, params string[] scopes)
        {
            return new Client
            {
                ClientId = id,
                ClientName = name,
                ClientUri = url,
                AllowedGrantTypes = GrantTypes.Implicit,
                AllowAccessTokensViaBrowser = true,
                RequireConsent = false,
                RedirectUris =
                {
                    $"{url}/swagger/oauth2-redirect.html",
                },
                PostLogoutRedirectUris = {$"{url}/swagger/"},
                AllowedCorsOrigins = {url},

                AlwaysSendClientClaims = true,
                AccessTokenType = AccessTokenType.Jwt,
				AllowedScopes = scopes
            };
        }

        private IDictionary<string, string> GetClientUrls()
        {
            return new Dictionary<string, string>
            {
                { "NexCore.Identity.Admin", _configuration.GetValue<string>("Clients:NexCore_Identity_Admin:Url") },
                { "NexCore.SalesCore.Web", _configuration.GetValue<string>("Clients:NexCore_SalesCore_Web:Url") },
                { "NexCore.SalesCore.Web.HttpAggregator", _configuration.GetValue<string>("Clients:NexCore_SalesCore_Web_HttpAggregator:Url") },
                { "NexCore.ContactManager", _configuration.GetValue<string>("Clients:NexCore_ContactManager:Url") },
                { "NexCore.VehicleCatalog", _configuration.GetValue<string>("Clients:NexCore_VehicleCatalog:Url") },
                { "NexCore.VehicleConfigurator", _configuration.GetValue<string>("Clients:NexCore_VehicleConfigurator:Url") },
                { "NexCore.VehicleStock", _configuration.GetValue<string>("Clients:NexCore_VehicleStock:Url") },
                { "NexCore.Identity.Api", _configuration.GetValue<string>("Clients:NexCore_Identity_Api:Url") }
            };
        }

        public IEnumerable<IdentityResource> GetIdentityResources()
        {
	        return new List<IdentityResource>
	        {
		        new IdentityResources.OpenId(),
		        new IdentityResources.Profile(),
		        new IdentityResources.Email(),
		        new IdentityResource("roles", "Roles", new[] {"role"}),
		        new IdentityResource("person_info", "Person Info", new[] {"person_id"}),
		        new IdentityResource("importer_employee_info", "Importer Employee Info",
			        new[] {"employee_id", "company_id", "is_importer_employee", "importer_id"}),
		        new IdentityResource("dealer_employee_info", "Dealer Employee Info",
			        new[] {"employee_id", "company_id", "is_dealer_employee", "dealer_id"}),
				new IdentityResource("private_customer_info", "Customer Info", new []{"private_customer_id", "company_id"})
	        };
        }
    }
}
