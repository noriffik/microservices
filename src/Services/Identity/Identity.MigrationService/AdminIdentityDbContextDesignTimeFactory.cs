﻿using NexCore.Identity.Common.Persistence;
using NexCore.Infrastructure;

namespace NexCore.Identity.MigrationService
{
    public class AdminIdentityDbContextDesignTimeFactory : DbContextBaseDesignFactory<AdminIdentityDbContext>
    {
        public AdminIdentityDbContextDesignTimeFactory() : base("ConnectionString", typeof(Startup).Assembly.GetName().Name)
        {
        }
    }
}
