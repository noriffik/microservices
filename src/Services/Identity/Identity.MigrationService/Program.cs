﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using NexCore.Identity.Common.Persistence;
using NexCore.Infrastructure.Setup;
using NexCore.WebApi.Extensions;
using System.Threading.Tasks;

namespace NexCore.Identity.MigrationService
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            await host.Setup<DatabaseSetupTask<AdminIdentityDbContext>>();
            await host.Setup<DatabaseSetupTask<AdminLogDbContext>>();
            await host.Setup<DatabaseSetupTask<IdentityServerConfigurationDbContext>>();
            await host.Setup<DatabaseSetupTask<IdentityServerPersistedGrantDbContext>>();

            await host.RunAsync();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
