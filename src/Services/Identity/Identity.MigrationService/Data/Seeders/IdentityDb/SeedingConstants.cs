﻿using NexCore.Identity.Common;
using System.Collections.Generic;
using System.Security.Claims;

namespace NexCore.Identity.MigrationService.Data.Seeders.IdentityDb
{
    public static class SeedingConstants
	{
		public const string DefaultPassword = "<Strong!Passw0rd>";

		public const string AdministratorUserName = "admin";
        public const string DistributorUserName = "distributor_employee";
		public const string DealerEmployeeUserName = "dealer_employee";

        public const string AdministratorRole = "Administrator";
		public const string DistributorEmployeeRole = "DistributorEmployee";
		public const string DealerEmployeeRole = "DealerEmployee";

		public static IEnumerable<string> Roles { get; } = new[]
		{
			AdministratorRole,
			DistributorEmployeeRole,
			DealerEmployeeRole
		};

		public static IEnumerable<UserSeedingSpecification> Users { get; } = new[]
		{
			new UserSeedingSpecification(AdministratorUserName, DefaultPassword)
			{
				Roles = new[] {AdministratorRole}
			},
            new UserSeedingSpecification(DistributorUserName, DefaultPassword)
            {
                Roles = new[] {DistributorEmployeeRole},
                Claims = new[]
                {
                    new Claim(IdentityClaimTypes.IndividualId, "1", ClaimValueTypes.Integer),
                    new Claim(IdentityClaimTypes.EmployeeId, "1", ClaimValueTypes.Integer),
                    new Claim(IdentityClaimTypes.EmployeeDistributorId, "EKK"),
                    new Claim(IdentityClaimTypes.EmployeeDistributorOrganizationId, "1", ClaimValueTypes.Integer)
                }
            },
			new UserSeedingSpecification(DealerEmployeeUserName, DefaultPassword)
			{
				Roles = new[] {DealerEmployeeRole},
				Claims = new[]
				{
					new Claim(IdentityClaimTypes.IndividualId, "2", ClaimValueTypes.Integer),
                    new Claim(IdentityClaimTypes.EmployeeId, "2", ClaimValueTypes.Integer),
                    new Claim(IdentityClaimTypes.EmployeeDealerId, "EKK38001"),
                    new Claim(IdentityClaimTypes.EmployeeDealerOrganizationId, "2", ClaimValueTypes.Integer),
                    new Claim(IdentityClaimTypes.EmployeeDealerDistributorId, "EKK"),
                    new Claim(IdentityClaimTypes.EmployeeDealerDistributorOrganizationId, "1", ClaimValueTypes.Integer)
                }
			}
        };
	}
}
