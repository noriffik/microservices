﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using NexCore.Identity.Common.Entities;
using NexCore.Infrastructure.Seeds;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.Identity.MigrationService.Data.Seeders.IdentityDb
{
    public class UserIdentitySeeder : ISeeder<UserIdentity>
    {
        private readonly UserManager<UserIdentity> _userManager;
        private IEnumerable<UserSeedingSpecification> _users = SeedingConstants.Users;

        public IEnumerable<UserSeedingSpecification> Users
        {
            get => _users;
            set
            {
                if (value != null)
                    _users = value;
            }
        }

        public UserIdentitySeeder(UserManager<UserIdentity> userManager)
        {
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public async Task Seed()
        {
            foreach (var user in Users)
                await CreateUser(user);
        }

        private async Task CreateUser(UserSeedingSpecification specification)
        {
            var doesExist = await _userManager.Users.AnyAsync(u => u.UserName == specification.UserName);
            if (doesExist)
                return;

            var user = new UserIdentity(specification.UserName);

            await ThrowIfFailed(() => _userManager.CreateAsync(user, specification.Password));
            await ThrowIfFailed(() => _userManager.AddToRolesAsync(user, specification.Roles));
            await ThrowIfFailed(() => _userManager.AddClaimsAsync(user, specification.Claims));
        }

        private static async Task ThrowIfFailed(Func<Task<IdentityResult>> action)
        {
            var result = await action();
            if (result.Succeeded)
                return;

            throw new AggregateException(
                result.Errors.Select(e => new InvalidOperationException(e.Description)));
        }
    }
}