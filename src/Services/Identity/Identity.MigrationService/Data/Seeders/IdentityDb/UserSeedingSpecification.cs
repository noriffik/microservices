﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace NexCore.Identity.MigrationService.Data.Seeders.IdentityDb
{
    public class UserSeedingSpecification
    {
        private IEnumerable<string> _roles = new List<string>();
        private IEnumerable<Claim> _claims = new List<Claim>();

        public string UserName { get; }

        public string Password { get; }

        public IEnumerable<string> Roles
        {
            get => _roles;
            set
            {
                if (value != null)
                    _roles = value;
            }
        }

        public IEnumerable<Claim> Claims
        {
            get => _claims;
            set
            {
                if (value != null)
                    _claims = value;
            }
        }

        public UserSeedingSpecification(string userName, string password)
        {
            UserName = userName ?? throw new ArgumentNullException(nameof(userName));
            Password = password ?? throw new ArgumentNullException(nameof(password));
        }
    }
}