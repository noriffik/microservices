﻿using Microsoft.AspNetCore.Identity;
using NexCore.Identity.Common.Entities;
using NexCore.Infrastructure.Seeds;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexCore.Identity.MigrationService.Data.Seeders.IdentityDb
{
    public class UserIdentityRoleSeeder : ISeeder<UserIdentityRole>
    {
        private readonly RoleManager<UserIdentityRole> _roleManager;
        private IEnumerable<string> _roles = SeedingConstants.Roles;

        public IEnumerable<string> Roles
        {
            get => _roles;
            set
            {
                if (value != null)
                    _roles = value;
            }
        }

        public UserIdentityRoleSeeder(RoleManager<UserIdentityRole> roleManager)
        {
            _roleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
        }

        public async Task Seed()
        {
            foreach (var role in Roles)
                await CreateRole(role);
        }

        private async Task CreateRole(string roleName)
        {
            var doesExist = await _roleManager.RoleExistsAsync(roleName);
            if (doesExist)
                return;

            await _roleManager.CreateAsync(new UserIdentityRole(roleName));
        }
    }
}
