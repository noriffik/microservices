﻿using NexCore.Identity.Common.Entities;
using NexCore.Identity.Common.Persistence;
using NexCore.Infrastructure.Seeds;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.Identity.MigrationService.Data.Seeders.IdentityDb
{
    public class IdentityDbSeeder : CompositeSeeder, IDbContextSeeder<AdminIdentityDbContext>
    {
        [SuppressMessage("ReSharper", "SuggestBaseTypeForParameter", Justification = "Specific interface type is used by Ioc.")]
        public IdentityDbSeeder(ISeeder<UserIdentityRole> roleSeeder, ISeeder<UserIdentity> userSeeder) 
            : base(roleSeeder, userSeeder)
        {
        }
    }
}
