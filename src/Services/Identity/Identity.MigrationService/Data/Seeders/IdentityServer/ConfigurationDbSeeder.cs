﻿using IdentityServer4.EntityFramework.Mappers;
using Microsoft.EntityFrameworkCore;
using NexCore.Identity.Common.Persistence;
using NexCore.Infrastructure.Seeds;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.Identity.MigrationService.Data.Seeders.IdentityServer
{
    public class ConfigurationDbSeeder : IDbContextSeeder<IdentityServerConfigurationDbContext>
    {
        private readonly IdentityServerConfigurationDbContext _context;
        private readonly IdentityServerConfig _config;

        public ConfigurationDbSeeder(IdentityServerConfigurationDbContext context, IdentityServerConfig config)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _config = config ?? throw new ArgumentNullException(nameof(config));
        }
        
        public async Task Seed()
        {
            await AddApiResources();
            await AddClients();
            await AddIdentityResources();

            await _context.SaveChangesAsync();
        }

        private async Task AddClients()
        {
            var isSeeded = await _context.Clients.AnyAsync();
            if (isSeeded) return;
            
            _context.Clients.AddRange(_config.GetClients().Select(c => c.ToEntity()));
        }

        private async Task AddIdentityResources()
        {
            var isSeeded = await _context.IdentityResources.AnyAsync();
            if (isSeeded) return;

            _context.IdentityResources.AddRange(_config.GetIdentityResources().Select(r => r.ToEntity()));
        }

        private async Task AddApiResources()
        {
            var isSeeded = await _context.ApiResources.AnyAsync();
            if (isSeeded) return;

            _context.ApiResources.AddRange(_config.GetApiResources().Select(r => r.ToEntity()));
        }
    }
}
