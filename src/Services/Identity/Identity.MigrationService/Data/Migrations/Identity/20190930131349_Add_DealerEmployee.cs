﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.Identity.MigrationService.Data.Migrations.Identity
{
	public partial class Add_DealerEmployee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DealerEmployees",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DealerId = table.Column<string>(nullable: true),
                    DealerOrganizationId = table.Column<int>(nullable: false),
                    DistributorId = table.Column<string>(nullable: true),
                    DistributorOrganizationId = table.Column<int>(nullable: false),
                    IndividualId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Telephone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DealerEmployees", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DealerEmployees");
        }
    }
}
