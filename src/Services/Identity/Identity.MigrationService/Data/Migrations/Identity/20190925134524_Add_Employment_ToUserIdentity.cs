﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.Identity.MigrationService.Data.Migrations.Identity
{
    public partial class Add_Employment_ToUserIdentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Employment_DistributorId",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Employment_EmployeeId",
                table: "Users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Employment_DistributorId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Employment_EmployeeId",
                table: "Users");
        }
    }
}
