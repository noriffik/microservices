﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.Identity.MigrationService.Data.Migrations.Identity
{
	public partial class AddFieldDisplayName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                table: "Users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DisplayName",
                table: "Users");
        }
    }
}
