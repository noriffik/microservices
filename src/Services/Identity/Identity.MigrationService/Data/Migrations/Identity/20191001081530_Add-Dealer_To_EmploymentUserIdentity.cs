﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.Identity.MigrationService.Data.Migrations.Identity
{
    public partial class AddDealer_To_EmploymentUserIdentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Employment_DealerId",
                table: "Users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Employment_DealerId",
                table: "Users");
        }
    }
}
