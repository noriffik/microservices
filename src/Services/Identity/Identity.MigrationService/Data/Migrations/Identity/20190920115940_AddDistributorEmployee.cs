﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.Identity.MigrationService.Data.Migrations.Identity
{
    public partial class AddDistributorEmployee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DistributorEmployees",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    DistributorId = table.Column<string>(nullable: true),
                    DistributorOrganizationId = table.Column<int>(nullable: false),
                    IndividualId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Telephone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DistributorEmployees", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DistributorEmployees");
        }
    }
}
