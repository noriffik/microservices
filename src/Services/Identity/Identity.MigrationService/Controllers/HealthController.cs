﻿using Microsoft.AspNetCore.Mvc;

namespace NexCore.Identity.MigrationService.Controllers
{
    [Route("api/health")]
    [ApiController]
    public class HealthController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            return Ok();
        }
    }
}
