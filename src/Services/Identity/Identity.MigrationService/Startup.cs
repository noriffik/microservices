﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Identity.Common.Entities;
using NexCore.Identity.Common.Persistence;
using NexCore.Identity.MigrationService.Data.Seeders.IdentityServer;
using NexCore.Infrastructure.Extensions;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Setup;
using System.Reflection;
using IdentityDbSeeder = NexCore.Identity.MigrationService.Data.Seeders.IdentityDb.IdentityDbSeeder;
using UserIdentityRoleSeeder = NexCore.Identity.MigrationService.Data.Seeders.IdentityDb.UserIdentityRoleSeeder;
using UserIdentitySeeder = NexCore.Identity.MigrationService.Data.Seeders.IdentityDb.UserIdentitySeeder;

namespace NexCore.Identity.MigrationService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var app = typeof(Startup).Assembly;

            services
                .AddTransient<DatabaseSetupTask<AdminIdentityDbContext>>()
                .AddDbContext<AdminIdentityDbContext>(Configuration["ConnectionString"], Configuration, app)
                .AddTransient<DatabaseSetupTask<AdminLogDbContext>>()
                .AddDbContext<AdminLogDbContext>(Configuration["ConnectionString"], Configuration, app)
                .AddTransient<DatabaseSetupTask<IdentityServerConfigurationDbContext>>()
                .AddDbContext<IdentityServerConfigurationDbContext>(Configuration["ConnectionString"], Configuration, app)
                .AddTransient<DatabaseSetupTask<IdentityServerPersistedGrantDbContext>>()
                .AddDbContext<IdentityServerPersistedGrantDbContext>(Configuration["ConnectionString"], Configuration, app)
                .AddTransient<IDbContextSeeder<AdminIdentityDbContext>, IdentityDbSeeder>()
                .AddTransient<ISeeder<UserIdentityRole>, UserIdentityRoleSeeder>()
                .AddTransient<ISeeder<UserIdentity>, UserIdentitySeeder>()
                .AddTransient<IDbContextSeeder<IdentityServerConfigurationDbContext>, ConfigurationDbSeeder>()
                .AddTransient<IdentityServerConfig>();

            var migrationsAssembly = typeof(IdentityServerConfigurationDbContext).GetTypeInfo().Assembly.GetName().Name;

            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddConfigurationStore<IdentityServerConfigurationDbContext>(options =>
                {
                    options.ConfigureDbContext = builder =>
                        builder.UseSqlServer(Configuration["ConnectionString"],
                            sql => sql.MigrationsAssembly(migrationsAssembly));
                })
                .AddOperationalStore<IdentityServerPersistedGrantDbContext>(options =>
                {
                    options.ConfigureDbContext = builder =>
                        builder.UseSqlServer(Configuration["ConnectionString"],
                            sql => sql.MigrationsAssembly(migrationsAssembly));

                    // This enables automatic token cleanup. this is optional.
                    options.EnableTokenCleanup = true;
                    options.TokenCleanupInterval = 30;
                });
            
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddIdentity<UserIdentity, UserIdentityRole>(o =>
                    {
                        o.ClaimsIdentity.SecurityStampClaimType = "security_stamp";
                    })
                .AddEntityFrameworkStores<AdminIdentityDbContext>()
                .AddDefaultTokenProviders();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
