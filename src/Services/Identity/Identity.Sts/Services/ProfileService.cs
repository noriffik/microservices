﻿using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using NexCore.Identity.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace NexCore.Identity.Sts.Services
{
    public class ProfileService : IProfileService
    {
        public const string SecurityStampClaimType = "security_stamp";

		private readonly UserManager<UserIdentity> _userManager;

		public ProfileService(UserManager<UserIdentity> userManager)
		{
			_userManager = userManager;
		}

		public async Task GetProfileDataAsync(ProfileDataRequestContext context)
		{
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            var subject = context.Subject ?? throw new ArgumentNullException(nameof(context.Subject));
			
            context.IssuedClaims.AddRange(subject.Claims);
            context.IssuedClaims.AddRange(await GetClaimsFromUser(subject));
        }

        private async Task<IEnumerable<Claim>> GetClaimsFromUser(ClaimsPrincipal subject)
        {
            var user = await FindCurrentUser(subject);
            if (user == null)
                throw new ArgumentException("Invalid subject identifier");

            return await _userManager.GetClaimsAsync(user);
        }

        private async Task<UserIdentity> FindCurrentUser(ClaimsPrincipal subject)
        {
            var subjectId = subject.Claims
                .FirstOrDefault(x => x.Type == "sub")?
                .Value;
            
            return await _userManager.FindByIdAsync(subjectId);
        }

		public async Task IsActiveAsync(IsActiveContext context)
		{
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            var subject = context.Subject ?? throw new ArgumentNullException(nameof(context.Subject));

            var user = await FindCurrentUser(subject);

            context.IsActive = await IsUserActive(user, subject);
        }

        private async Task<bool> IsUserActive(UserIdentity user, ClaimsPrincipal subject)
        {
            if (user == null)
                return false;

            return await IsUserSecurityStampValid(user, subject) && !await _userManager.IsLockedOutAsync(user);
        }

        private async Task<bool> IsUserSecurityStampValid(UserIdentity user, ClaimsPrincipal subject)
        {
            if (!_userManager.SupportsUserSecurityStamp)
                return true;

            var securityStamp = subject.Claims
                .Where(c => c.Type == SecurityStampClaimType)
                .Select(c => c.Value)
                .SingleOrDefault(); 
            
            if (securityStamp == null)
                return false;

            var actualSecurityStamp = await _userManager.GetSecurityStampAsync(user);
            
            return actualSecurityStamp == securityStamp;
        }
    }
}