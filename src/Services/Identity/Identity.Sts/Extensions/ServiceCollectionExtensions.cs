﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Identity.Common.Entities;
using NexCore.Identity.Common.Persistence;
using NexCore.Identity.Sts.Services;
using NexCore.Infrastructure.Setup;
using System.Reflection;

namespace NexCore.Identity.Sts.Extensions
{
	internal static class ServiceCollectionExtensions
    {
        public static void AddApplicationIdentityServer(this IServiceCollection services, IConfiguration configuration)
        {
            var migrationsAssembly = typeof(IdentityServerConfigurationDbContext).GetTypeInfo().Assembly.GetName().Name;
			
			services.AddIdentityServer(o =>
                {
                    var issuerUri = configuration["Identity:IssuerUrl"];
                    if (!string.IsNullOrEmpty(issuerUri))
                        o.IssuerUri = issuerUri;

                    var publicOrigin = configuration["Identity:PublicOrigin"];
                    if (!string.IsNullOrEmpty(publicOrigin))
                        o.PublicOrigin = publicOrigin;
                })
                .AddAspNetIdentity<UserIdentity>()
                .AddDeveloperSigningCredential()
                .AddConfigurationStore<IdentityServerConfigurationDbContext>(options =>
                {
                    options.ConfigureDbContext = builder =>
                        builder.UseSqlServer(configuration["ConnectionString"],
                            sql => sql.MigrationsAssembly(migrationsAssembly));
                })
                .AddOperationalStore<IdentityServerPersistedGrantDbContext>(options =>
                {
                    options.ConfigureDbContext = builder =>
                        builder.UseSqlServer(configuration["ConnectionString"],
                            sql => sql.MigrationsAssembly(migrationsAssembly));

                    // This enables automatic token cleanup. this is optional.
                    options.EnableTokenCleanup = true;
                    options.TokenCleanupInterval = 30;
                });
		}

        public static void AddAspNetIdentity(this IServiceCollection services)
        {
            services.AddIdentity<UserIdentity, UserIdentityRole>(o =>
                    {
                        o.ClaimsIdentity.SecurityStampClaimType = ProfileService.SecurityStampClaimType;
                    })
                .AddEntityFrameworkStores<AdminIdentityDbContext>()
                .AddDefaultTokenProviders();
        }

        public static IServiceCollection AddMigrationServiceWait(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<MigrationServiceWaitTask>()
                .Configure<MigrationServiceWaitOptions>(configuration.GetSection("MigrationServiceWait"))
                .AddOptions<MigrationServiceWaitOptions>()
                .ValidateDataAnnotations();

            return services;
        }
    }
}
