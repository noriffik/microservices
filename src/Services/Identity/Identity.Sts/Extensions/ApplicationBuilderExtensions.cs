﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Primitives;

namespace NexCore.Identity.Sts.Extensions
{
    static class ApplicationBuilderExtensions
    {
        public static void AllowFrames(this IApplicationBuilder app, string hostNameExpr)
        {
            app.Use(async (context, func) =>
            {
                context.Response.Headers.Add(
                    "Content-Security-Policy", new StringValues($"frame-src {hostNameExpr}"));

                await func();
            });
        }
    }
}
