﻿const gulp = require('gulp');
const rimraf = require("rimraf");
const merge = require('merge-stream');

const dependencies = {
    "jquery": {
        "source": "dist/*",
        "destination": ""
    },
    "bootstrap": {
        "source": "dist/**/*",
        "destination": ""
    },
    "jquery-validation": {
        "source": "dist/*",
        "destination": ""
    },
    "jquery-validation-unobtrusive": {
        "source": "dist/*",
        "destination": ""
    }
};

gulp.task("clean", function (cb) {
    return rimraf("wwwroot/lib/", cb);
});

gulp.task("scripts", function () {
    const streams = [];

    for (const [name, dependency] of Object.entries(dependencies)) {
        streams.push(gulp.src("node_modules/" + name + "/" + dependency["source"])
            .pipe(gulp.dest("wwwroot/lib/" + name + "/" + dependency["destination"])));
    }
    
    return merge(streams);
});

gulp.task("default", gulp.series('clean', 'scripts'));