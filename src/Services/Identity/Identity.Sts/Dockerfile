FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-stretch-slim AS base
WORKDIR /app
EXPOSE 80

FROM node:10.15.1-alpine AS js-build
WORKDIR /app
COPY ["src/Services/Identity/Identity.Sts/package.json", "src/Services/Identity/Identity.Sts/package-lock.json", "src/Services/Identity/Identity.Sts/gulpfile.js", "./"]
RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh
RUN npm install && npm run build

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build
WORKDIR /src
COPY ["src/Services/Identity/Identity.Sts/Identity.Sts.csproj", "src/Services/Identity/Identity.Sts/"]
COPY ["src/Services/Identity/Identity.Common/Identity.Common.csproj", "src/Services/Identity/Identity.Common/"]
COPY ["src/BuildingBlocks/WebApi/WebApi.csproj", "src/BuildingBlocks/WebApi/"]
COPY ["src/BuildingBlocks/Infrastructure/Infrastructure.csproj", "src/BuildingBlocks/Infrastructure/"]
COPY ["src/BuildingBlocks/Domain/Domain.csproj", "src/BuildingBlocks/Domain/"]
RUN dotnet restore "src/Services/Identity/Identity.Sts/Identity.Sts.csproj"
COPY . .
WORKDIR "/src/src/Services/Identity/Identity.Sts"
RUN dotnet build "Identity.Sts.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "Identity.Sts.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=js-build /app/wwwroot/lib ./wwwroot/lib
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "NexCore.Identity.Sts.dll"]