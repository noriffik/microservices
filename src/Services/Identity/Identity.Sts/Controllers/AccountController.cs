﻿using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NexCore.Identity.Common.Entities;
using NexCore.Identity.Sts.ViewModels.Account;
using System;
using System.Threading.Tasks;

namespace NexCore.Identity.Sts.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<UserIdentity> _userManager;
        private readonly SignInManager<UserIdentity> _signInManager;
        private readonly IIdentityServerInteractionService _interaction;

        public AccountController(
            UserManager<UserIdentity> userManager,
            SignInManager<UserIdentity> signInManager,
            IIdentityServerInteractionService interaction)
        {
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
            _interaction = interaction ?? throw new ArgumentNullException(nameof(interaction));
        }

        public async Task<IActionResult> Login(string returnUrl)
        {
            var context = await _interaction.GetAuthorizationContextAsync(returnUrl);
            
            return View(new LoginViewModel
            {
                Username = context?.LoginHint,
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = await FindUserByCredentials(model);
            if (user == null)
            {
                ModelState.AddModelError(string.Empty, "Invalid login or password");

                return View(model);
            }

            await _signInManager.SignInAsync(user, true);

            return Redirect(_interaction.IsValidReturnUrl(model.ReturnUrl) ? model.ReturnUrl : "~/");
        }

        private async Task<UserIdentity> FindUserByCredentials(LoginViewModel model)
        {
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user == null)
                return null;

            var isValid = await _userManager.CheckPasswordAsync(user, model.Password);

            return isValid ? user : null;
        }

        [HttpGet]
        public async Task<IActionResult> Logout(string logoutId)
        {
            var model = new LogoutViewModel {LogoutId = logoutId};
            if (!User.Identity.IsAuthenticated)
            {
                return await Logout(model);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout(LogoutViewModel model)
        {
            await _signInManager.SignOutAsync();
            
            var logout = await _interaction.GetLogoutContextAsync(model.LogoutId);

            return Redirect(logout?.PostLogoutRedirectUri ?? "/");
        }
    }
}
