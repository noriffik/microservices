﻿using IdentityServer4.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using NexCore.Identity.Common.Persistence;
using NexCore.Identity.Sts.Extensions;
using NexCore.Identity.Sts.Services;
using NexCore.Infrastructure.Extensions;
using System.IO;
using System.Linq;

namespace NexCore.Identity.Sts
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.WithOrigins(GetCorsOrigins())
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            services.AddMigrationServiceWait(Configuration);
            
	        services.AddDbContext<AdminIdentityDbContext>(Configuration["ConnectionString"], Configuration);
	        services.AddDbContext<AdminLogDbContext>(Configuration["ConnectionString"], Configuration);

            services.AddAspNetIdentity();

	        services.AddMvc();

	        services.AddApplicationIdentityServer(Configuration);
			services.AddTransient<IProfileService, ProfileService>();
		}

        private string[] GetCorsOrigins()
        {
            var origins = Configuration.GetValue("CorsOrigins", string.Empty);

            return origins.Split(',')
                .Select(origin => origin.Trim())
                .ToArray();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.AllowFrames(Configuration["Csp:Host"]);
            app.UseCors("CorsPolicy");
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "lib")),
                RequestPath = "/wwwroot/lib"
            });

            app.UseIdentityServer();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
