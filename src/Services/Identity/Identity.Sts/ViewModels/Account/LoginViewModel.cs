﻿using System.ComponentModel.DataAnnotations;

namespace NexCore.Identity.Sts.ViewModels.Account
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Имя учетной записи")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Запомнить меня?")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}
