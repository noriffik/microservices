﻿using System.Collections.Generic;

namespace NexCore.Identity.Sts.ViewModels.Consent
{
    public class ConsentInputViewModel
    {
        public string Button { get; set; }
        public IEnumerable<string> ScopesConsented { get; set; }
        public bool RememberConsent { get; set; }
        public string ReturnUrl { get; set; }
    }
}
