﻿namespace NexCore.Identity.Common.Entities
{
    public class DistributorEmployee : Employee
	{
		public string DistributorId { get; set; }

		public int DistributorOrganizationId { get; set; }
	}
}