﻿namespace NexCore.Identity.Common.Entities
{
	public class DealerEmployee : Employee
	{
		public string DealerId { get; set; }
		public int DealerOrganizationId { get; set; }
		public string DistributorId { get; set; }
		public int DistributorOrganizationId { get; set; }
	}
}