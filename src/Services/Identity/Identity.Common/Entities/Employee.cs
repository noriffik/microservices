﻿namespace NexCore.Identity.Common.Entities
{
	public abstract class Employee
	{
		public int Id { get; set; }

		public int IndividualId { get; set; }

		public int JobId { get; set; }

		public string Name { get; set; }

		public string Telephone { get; set; }

		public string Email { get; set; }
	}
}