﻿using Microsoft.AspNetCore.Identity;

namespace NexCore.Identity.Common.Entities
{
    public class UserIdentity : IdentityUser
    {
        public UserIdentity()
        {
			Employment = new UserIdentityEmployment();
        }

        public UserIdentity(string userName) : base(userName)
        {
	        Employment = new UserIdentityEmployment();
		}

		public string DisplayName { get; set; }

        public UserIdentityEmployment Employment { get; set; }
    }
}
