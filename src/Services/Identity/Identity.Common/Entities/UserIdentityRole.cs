﻿using Microsoft.AspNetCore.Identity;

namespace NexCore.Identity.Common.Entities
{
	public class UserIdentityRole : IdentityRole
	{
        public UserIdentityRole()
        {
        }

        public UserIdentityRole(string roleName) : base(roleName)
        {
        }
	}
}