﻿namespace NexCore.Identity.Common.Entities
{
    public class UserIdentityEmployment
    {
        public int EmployeeId { get; set; }

        public string DistributorId { get; set; }

		public string DealerId { get; set; }
    }
}
