﻿using NexCore.Infrastructure;

namespace NexCore.Identity.Common.Persistence
{
    public class AdminIdentityDbContextDesignTimeFactory : DbContextBaseDesignFactory<AdminIdentityDbContext>
    {
        public AdminIdentityDbContextDesignTimeFactory() : base("ConnectionString")
        {
        }
    }
}
