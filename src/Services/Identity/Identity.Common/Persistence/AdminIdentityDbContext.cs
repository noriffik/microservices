﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NexCore.Identity.Common.Entities;

namespace NexCore.Identity.Common.Persistence
{
    public class AdminIdentityDbContext : IdentityDbContext<UserIdentity, UserIdentityRole, string, UserIdentityUserClaim, UserIdentityUserRole, UserIdentityUserLogin, UserIdentityRoleClaim, UserIdentityUserToken>
    {
        protected AdminIdentityDbContext() : base()
        {
        }

        public AdminIdentityDbContext(DbContextOptions<AdminIdentityDbContext> options) : base(options)
        {
        }

        public DbSet<DistributorEmployee> DistributorEmployees { get; set; }

		public DbSet<DealerEmployee> DealerEmployees { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            ConfigureIdentityContext(builder);

            builder.Entity<DistributorEmployee>().Property(a => a.Id).ValueGeneratedNever();
        }

        private void ConfigureIdentityContext(ModelBuilder builder)
        {
            builder.Entity<UserIdentityRole>().ToTable("Roles");
            builder.Entity<UserIdentityRoleClaim>().ToTable("RoleClaims");
            builder.Entity<UserIdentityUserRole>().ToTable("UserRoles");

            builder.Entity<UserIdentity>().ToTable("Users");

            builder.Entity<UserIdentity>().OwnsOne(u => u.Employment);
     
			builder.Entity<UserIdentityUserLogin>().ToTable("UserLogins");
            builder.Entity<UserIdentityUserClaim>().ToTable("UserClaims");
            builder.Entity<UserIdentityUserToken>().ToTable("UserTokens");
        }
    }
}