﻿namespace NexCore.Identity.Common
{
    public class IdentityClaimTypes
    {
        public const string IndividualId = nameof(IndividualId);

        public const string EmployeeId = nameof(EmployeeId);

        public const string EmployeeDistributorId = nameof(EmployeeDistributorId);

        public const string EmployeeDistributorOrganizationId = nameof(EmployeeDistributorOrganizationId);

        public const string EmployeeDealerId = nameof(EmployeeDealerId);

        public const string EmployeeDealerOrganizationId = nameof(EmployeeDealerOrganizationId);

        public const string EmployeeDealerDistributorId = nameof(EmployeeDealerDistributorId);

        public const string EmployeeDealerDistributorOrganizationId = nameof(EmployeeDealerDistributorOrganizationId);

        public const string EmployeeJobId = nameof(EmployeeJobId);
    }
}
