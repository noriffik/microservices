﻿using Microsoft.Extensions.Localization;

namespace Skoruba.Identity.Admin.Helpers.Localization
{
    public interface IGenericControllerLocalizer<T> : IStringLocalizer<T>
    {
        
    }
}