﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Infrastructure.Setup;

namespace Skoruba.Identity.Admin.Extensions
{
    internal static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMigrationServiceWait(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<MigrationServiceWaitTask>()
                .Configure<MigrationServiceWaitOptions>(configuration.GetSection("MigrationServiceWait"))
                .AddOptions<MigrationServiceWaitOptions>()
                .ValidateDataAnnotations();

            return services;
        }
    }
}
