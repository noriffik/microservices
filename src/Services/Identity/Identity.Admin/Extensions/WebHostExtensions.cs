﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Infrastructure.Setup;
using System.Threading.Tasks;

namespace Skoruba.Identity.Admin.Extensions
{
    public static class WebHostExtensions
    {
        public static async Task<IWebHost> SetupMigrationServiceWait(this IWebHost webHost)
        {
            using (var scope = webHost.Services.CreateScope())
            {
                var setup = scope.ServiceProvider.GetRequiredService<MigrationServiceWaitTask>();

                await setup.Execute();
            }

            return webHost;
        }
    }
}