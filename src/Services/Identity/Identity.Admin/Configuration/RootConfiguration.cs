﻿using Microsoft.Extensions.Options;
using Skoruba.Identity.Admin.Configuration.Interfaces;

namespace Skoruba.Identity.Admin.Configuration
{
    public class RootConfiguration : IRootConfiguration
    {
        public IAdminConfiguration AdminConfiguration { get; set; }

        public RootConfiguration(IOptions<AdminConfiguration> adminConfiguration)
        {
            AdminConfiguration = adminConfiguration.Value;
        }
    }
}
