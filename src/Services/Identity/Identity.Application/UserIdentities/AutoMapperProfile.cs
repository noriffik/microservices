﻿using AutoMapper;
using NexCore.Identity.Application.UserIdentities.Commands.AddUserIdentity;
using NexCore.Identity.Application.UserIdentities.Commands.DistributorEmployees.AddDistributorEmployeeIdentity;
using NexCore.Identity.Application.UserIdentities.Commands.UpdateUserIdentity;
using NexCore.Identity.Common.Entities;

namespace NexCore.Identity.Application.UserIdentities
{
	public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AddUserIdentityCommand, UserIdentity>();
            CreateMap<UpdateUserIdentityCommand, UserIdentity>();
            CreateMap<AddDistributorEmployeeIdentityCommand, UserIdentity>();
        }
    }
}
