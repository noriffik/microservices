﻿using System.Runtime.Serialization;
using NexCore.Application.Queries;

namespace NexCore.Identity.Application.UserIdentities.Queries.UserIdentity
{
    public class UserIdentityQuery : PagedRequest<UserIdentityDto>
	{
		[DataMember]
		public SortCriterion? SortBy { get; set; }

		[DataMember]
		public SortDirection? SortDirection { get; set; }

		[DataMember]
		public string UserName { get; set; }

		[DataMember]
		public string DisplayName { get; set; }
	}
}