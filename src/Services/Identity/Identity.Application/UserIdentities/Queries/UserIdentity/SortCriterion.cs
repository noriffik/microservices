﻿namespace NexCore.Identity.Application.UserIdentities.Queries.UserIdentity
{
	public enum SortCriterion
	{
		UserName = 0,
		Email = 1
	}
}