﻿using System.Runtime.Serialization;
using MediatR;

namespace NexCore.Identity.Application.UserIdentities.Queries.UserIdentity
{
	[DataContract]
	public class UserIdentityByIdQuery : IRequest<UserIdentityDto>
	{
		[DataMember]
		public string Id { get; set; }
	}
}