﻿namespace NexCore.Identity.Application.UserIdentities.Queries
{
	public enum SortCriterion
	{
		UserName = 0,
		DisplayName = 1
	}
}