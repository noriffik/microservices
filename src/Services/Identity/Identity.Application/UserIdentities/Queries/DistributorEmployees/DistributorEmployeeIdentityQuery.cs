﻿using NexCore.Application.Queries;
using System.Runtime.Serialization;

namespace NexCore.Identity.Application.UserIdentities.Queries.DistributorEmployees
{
	[DataContract]
	public class DistributorEmployeeIdentityQuery : PagedRequest<DistributorEmployeeIdentityDto>
	{
		public string DistributorId { get; set; }

		[DataMember]
		public SortCriterion? SortCriterion { get; set; }

		[DataMember]
		public SortDirection? SortDirection { get; set; }

		[DataMember]
		public string DisplayName { get; set; }

		[DataMember]
		public string UserName { get; set; }
	}
}