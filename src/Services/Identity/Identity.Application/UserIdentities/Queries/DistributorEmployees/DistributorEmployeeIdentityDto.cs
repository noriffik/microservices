﻿using NexCore.Identity.Application.UserIdentities.Queries.UserIdentity;

namespace NexCore.Identity.Application.UserIdentities.Queries.DistributorEmployees
{
	public class DistributorEmployeeIdentityDto : UserIdentityDto
	{
		public int EmployeeId { get; set; }
		public string EmployeeDistributorId { get; set; }
		public int EmployeeDistributorOrganizationId { get; set; }
		public int IndividualId { get; set; }
		public int EmployeeJobId { get; set; }
	}
}