﻿using System.Runtime.Serialization;
using MediatR;

namespace NexCore.Identity.Application.UserIdentities.Queries.DistributorEmployees
{
	[DataContract]
	public class DistributorEmployeeIdentityByIdQuery : IRequest<DistributorEmployeeIdentityDto>
	{
		public string DistributorId { get; set; }

		[DataMember]
		public string Id { get; set; }
	}
}