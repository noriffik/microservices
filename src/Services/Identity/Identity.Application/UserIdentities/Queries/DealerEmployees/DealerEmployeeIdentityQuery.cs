﻿using NexCore.Application.Queries;
using System.Runtime.Serialization;

namespace NexCore.Identity.Application.UserIdentities.Queries.DealerEmployees
{
	[DataContract]
	public class DealerEmployeeIdentityQuery : PagedRequest<DealerEmployeeIdentityDto>
	{
		[DataMember]
		public SortCriterion? SortCriteria { get; set; }

		[DataMember]
		public SortDirection? SortDirection { get; set; }

		[DataMember]
		public string DisplayName { get; set; }

		[DataMember]
		public string UserName { get; set; }
		
		public string DistributorId { get; set; }

		public string DealerId { get; set; }
	}
}