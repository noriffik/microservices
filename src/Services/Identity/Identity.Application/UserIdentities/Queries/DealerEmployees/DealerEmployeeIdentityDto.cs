﻿using NexCore.Identity.Application.UserIdentities.Queries.UserIdentity;

namespace NexCore.Identity.Application.UserIdentities.Queries.DealerEmployees
{
	public class DealerEmployeeIdentityDto : UserIdentityDto
	{
		public int EmployeeId { get; set; }
		public string EmployeeDealerId { get; set; }
		public int EmployeeDealerOrganizationId { get; set; }
		public string EmployeeDistributorId { get; set; }
		public int EmployeeDistributorOrganizationId { get; set; }
		public int IndividualId { get; set; }
		public int EmployeeJobId { get; set; }
	}
}