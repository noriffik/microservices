﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.Identity.Application.UserIdentities.Queries.DealerEmployees
{
	[DataContract]
	public class DealerEmployeeIdentityByIdQuery : IRequest<DealerEmployeeIdentityDto>
	{
		[DataMember]
		public string Id { get; set; }

		public string DistributorId { get; set; }

		public string DealerId { get; set; }
	}
}