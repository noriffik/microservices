﻿using NexCore.Identity.Application.Services;
using NexCore.Identity.Common.Entities;

namespace NexCore.Identity.Application.UserIdentities.Commands.DistributorEmployees.AddDistributorEmployeeIdentity
{
	public class AddDistributorEmployeeIdentityCommandHandler : AddEmployeeIdentityCommandHandler<AddDistributorEmployeeIdentityCommand, DistributorEmployee>
	{
		public AddDistributorEmployeeIdentityCommandHandler(UserIdentityService service,
			DistributorEmployeeIdentityOptionsFactory factory)
			: base(service, factory)
		{
		}
	}
}