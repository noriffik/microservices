﻿using FluentValidation;

namespace NexCore.Identity.Application.UserIdentities.Commands.DistributorEmployees.AddDistributorEmployeeIdentity
{
	public class
		AddDistributorEmployeeIdentityCommandValidator : AbstractValidator<AddDistributorEmployeeIdentityCommand>
	{
		public AddDistributorEmployeeIdentityCommandValidator()
		{
			RuleFor(x => x.UserName).NotEmpty();
			RuleFor(x => x.Password).NotEmpty();
		}
	}
}