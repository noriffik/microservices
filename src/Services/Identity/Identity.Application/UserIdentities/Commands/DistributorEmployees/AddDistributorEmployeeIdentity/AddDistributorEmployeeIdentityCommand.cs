﻿using NexCore.Identity.Common.Entities;
using System.Runtime.Serialization;

namespace NexCore.Identity.Application.UserIdentities.Commands.DistributorEmployees.AddDistributorEmployeeIdentity
{
	[DataContract]
	public class AddDistributorEmployeeIdentityCommand : AddEmployeeIdentityCommand<DistributorEmployee>
	{
		public string DistributorId { get; set; }
	}
}