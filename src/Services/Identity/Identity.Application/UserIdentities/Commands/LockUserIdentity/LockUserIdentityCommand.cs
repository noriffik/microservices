﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.Identity.Application.UserIdentities.Commands.LockUserIdentity
{
    [DataContract]
	public class LockUserIdentityCommand : IRequest<string>
	{
		[DataMember]
		public string UserId { get; set; }

		[DataMember]
		public bool Enabled { get; set; }
	}
}