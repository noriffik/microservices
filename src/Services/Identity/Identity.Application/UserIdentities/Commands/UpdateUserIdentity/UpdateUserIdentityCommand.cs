﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.Identity.Application.UserIdentities.Commands.UpdateUserIdentity
{
    [DataContract]
	public class UpdateUserIdentityCommand : IRequest
	{
		[DataMember]
		public string Id { get; set; }

		[DataMember]
		public string UserName { get; set; }

		[DataMember]
		public string DisplayName { get; set; }

		[DataMember]
		public string PhoneNumber { get; set; }

		[DataMember]
		public string Email { get; set; }

		[DataMember]
		public bool LockoutEnabled { get; set; }
	}
}