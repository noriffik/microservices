﻿using FluentValidation;

namespace NexCore.Identity.Application.UserIdentities.Commands.UpdateUserIdentity
{
	public class UpdateUserIdentityCommandValidator : AbstractValidator<UpdateUserIdentityCommand>
	{
		public UpdateUserIdentityCommandValidator()
		{
			RuleFor(c => c.Id).NotEmpty();
			RuleFor(c => c.UserName).NotEmpty();
			RuleFor(c => c.Email).EmailAddress();
			RuleFor(c => c.PhoneNumber).Length(6, 15).Matches("^[0-9]*$");
		}
	}
}