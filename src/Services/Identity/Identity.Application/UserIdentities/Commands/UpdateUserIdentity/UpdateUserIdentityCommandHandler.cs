﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using NexCore.Identity.Application.Exceptions;
using NexCore.Identity.Common.Entities;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Identity.Application.UserIdentities.Commands.UpdateUserIdentity
{
    public class UpdateUserIdentityCommandHandler : IRequestHandler<UpdateUserIdentityCommand>
	{
        private const string FailedToFindMessage = "Failed to find user identity.";
		private const string FailedToUpdateMessage = "Failed to update user identity.";

		private readonly UserManager<UserIdentity> _userManager;
		private readonly IMapper _mapper;

		public UpdateUserIdentityCommandHandler(UserManager<UserIdentity> userManager, IMapper mapper)
		{
			_userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
			_mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
		}

		public async Task<Unit> Handle(UpdateUserIdentityCommand command, CancellationToken cancellationToken)
		{
			if (command == null)
				throw new ArgumentNullException(nameof(command));

			var user = await _userManager.FindByIdAsync(command.Id);
            if (user == null)
                throw new IdentityOperationFailureException(FailedToFindMessage);

			var result = await _userManager.UpdateAsync(_mapper.Map(command, user));
			if (!result.Succeeded)
                throw new IdentityOperationFailureException(FailedToUpdateMessage, result.Errors.Select(e => e.Description));

            return Unit.Value;
        }
	}
}