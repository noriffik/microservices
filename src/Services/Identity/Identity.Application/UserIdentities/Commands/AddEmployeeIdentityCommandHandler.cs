﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using NexCore.Identity.Application.Services;
using NexCore.Identity.Common.Entities;

namespace NexCore.Identity.Application.UserIdentities.Commands
{
	public abstract class AddEmployeeIdentityCommandHandler<TCommand, TEmployee> : IRequestHandler<TCommand, string>
		where TCommand : AddEmployeeIdentityCommand<TEmployee>
		where TEmployee : Employee
	{
		private readonly UserIdentityService _service;
		private readonly EmployeeIdentityOptionsFactory<TEmployee, TCommand> _factory;

		protected AddEmployeeIdentityCommandHandler(UserIdentityService service, EmployeeIdentityOptionsFactory<TEmployee, TCommand> factory)
		{
			_service = service ?? throw new ArgumentNullException(nameof(service));
			_factory = factory ?? throw new ArgumentNullException(nameof(factory));
		}

		public virtual async Task<string> Handle(TCommand request, CancellationToken cancellationToken)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request));

			var userOptions = await _factory.Create(request);

			var userId = await _service.Create(userOptions);

			await _service.AddRole(userOptions);

			await _service.AddClaims(userOptions);

			return userId;
		}
	}
}