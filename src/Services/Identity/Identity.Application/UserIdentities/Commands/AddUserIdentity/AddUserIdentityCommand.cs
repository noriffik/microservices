﻿using MediatR;
using System.Runtime.Serialization;
using FluentValidation;

namespace NexCore.Identity.Application.UserIdentities.Commands.AddUserIdentity
{
    [DataContract]
	public class AddUserIdentityCommand : IRequest<string>
	{
		[DataMember]
		public string UserName { get; set; }

		[DataMember]
		public string DisplayName { get; set; }

		[DataMember]
		public string Password { get; set; }

		[DataMember]
		public string PhoneNumber { get; set; }

		[DataMember]
		public string Email { get; set; }

		[DataMember]
		public bool LockEnabled { get; set; }
	}
}