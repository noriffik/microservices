﻿using FluentValidation;

namespace NexCore.Identity.Application.UserIdentities.Commands.AddUserIdentity
{
	public class AddUserIdentityCommandValidator : AbstractValidator<AddUserIdentityCommand>
	{
		public AddUserIdentityCommandValidator()
		{
			RuleFor(c => c.UserName).NotEmpty();
			RuleFor(c => c.Password).NotEmpty();
			RuleFor(c => c.Email).EmailAddress();
			RuleFor(c => c.PhoneNumber).Length(6,15).Matches("^[0-9]*$");
		}
	}
}