﻿using AutoMapper;
using MediatR;
using NexCore.Identity.Application.Services;
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using NexCore.Identity.Application.Exceptions;
using NexCore.Identity.Common.Entities;

namespace NexCore.Identity.Application.UserIdentities.Commands.AddUserIdentity
{
	public class AddUserIdentityCommandHandler : IRequestHandler<AddUserIdentityCommand, string>
	{
		private const string FailedToAddUserIdentityMessage = "Failed to add user identity.";
		private const string ExistUserIdentityMessage = "Failed to add user identity. Exist user name";

		private readonly UserManager<UserIdentity> _userManager;
		private readonly IMapper _mapper;

		public AddUserIdentityCommandHandler(UserManager<UserIdentity> userManager, IMapper mapper)
		{
			_userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
			_mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
		}

		public async Task<string> Handle(AddUserIdentityCommand command, CancellationToken cancellationToken)
		{
			if (command == null)
				throw new ArgumentNullException(nameof(command));
			await ThrowIfUserNameIsInUse(command.UserName);

			var user = _mapper.Map<UserIdentity>(command);

			await _userManager.CreateAsync(user, command.Password)
				.ThrowIfFailed(FailedToAddUserIdentityMessage);

			await _userManager.SetLockoutEnabledAsync(user, command.LockEnabled)
				.ThrowIfFailed(FailedToAddUserIdentityMessage);

			return user.Id;
		}

		private async Task ThrowIfUserNameIsInUse(string userName)
		{
			if (await _userManager.FindByNameAsync(userName) != null)
				throw new IdentityOperationFailureException(ExistUserIdentityMessage);
		}
	}
}