﻿using MediatR;
using System.Runtime.Serialization;
using NexCore.Identity.Common.Entities;

namespace NexCore.Identity.Application.UserIdentities.Commands.DealerEmployees.AddDealerEmployeeIdentity
{
	[DataContract]
	public class AddDealerEmployeeIdentityCommand : AddEmployeeIdentityCommand<DealerEmployee>
	{
		public string DistributorId { get; set; }

		public string DealerId { get; set; }
	}
}