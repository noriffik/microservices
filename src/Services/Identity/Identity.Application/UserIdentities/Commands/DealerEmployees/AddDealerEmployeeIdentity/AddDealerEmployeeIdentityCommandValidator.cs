﻿using FluentValidation;

namespace NexCore.Identity.Application.UserIdentities.Commands.DealerEmployees.AddDealerEmployeeIdentity
{
	public class AddDealerEmployeeIdentityCommandValidator :  AbstractValidator<AddDealerEmployeeIdentityCommand>
	{
		public AddDealerEmployeeIdentityCommandValidator()
		{
			RuleFor(x => x.UserName).NotEmpty();
			RuleFor(x => x.Password).NotEmpty();
		}
	}
}