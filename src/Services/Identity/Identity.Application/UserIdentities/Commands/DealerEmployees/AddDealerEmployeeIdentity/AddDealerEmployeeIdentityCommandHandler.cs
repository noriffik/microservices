﻿using NexCore.Identity.Application.Services;
using NexCore.Identity.Common.Entities;

namespace NexCore.Identity.Application.UserIdentities.Commands.DealerEmployees.AddDealerEmployeeIdentity
{
	public class AddDealerEmployeeIdentityCommandHandler : AddEmployeeIdentityCommandHandler<AddDealerEmployeeIdentityCommand, DealerEmployee>
	{
		public AddDealerEmployeeIdentityCommandHandler(UserIdentityService service, 
			DealerEmployeeIdentityOptionsFactory factory) : base(service, factory)
		{
		}
	}
}