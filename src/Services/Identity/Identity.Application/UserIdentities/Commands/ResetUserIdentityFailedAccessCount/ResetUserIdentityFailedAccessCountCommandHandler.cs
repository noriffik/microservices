﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using NexCore.Identity.Application.Exceptions;
using NexCore.Identity.Common.Entities;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NexCore.Common;

namespace NexCore.Identity.Application.UserIdentities.Commands.ResetUserIdentityFailedAccessCount
{
	public class
		ResetUserIdentityFailedAccessCountCommandHandler : IRequestHandler<ResetUserIdentityFailedAccessCountCommand>
	{
		private const string FailedToFindMessage = "Failed to find user identity.";
		private const string FailedResetUserIdentityFailedAccessMessage = "Failed to reset access user identity.";
		private readonly UserManager<UserIdentity> _userManager;

		public ResetUserIdentityFailedAccessCountCommandHandler(UserManager<UserIdentity> userManager)
		{
			_userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
		}

		public async Task<Unit> Handle(ResetUserIdentityFailedAccessCountCommand command,
			CancellationToken cancellationToken)
		{
			if (command == null)
				throw new ArgumentNullException(nameof(command));

			var userIdentity = await _userManager.FindByIdAsync(command.Id);

			if (userIdentity == null)
				throw new IdentityOperationFailureException(FailedToFindMessage);

			await ThrowIfFailed(_userManager.ResetAccessFailedCountAsync(userIdentity));
			await ThrowIfFailed(_userManager.SetLockoutEnabledAsync(userIdentity, true));
			await ThrowIfFailed(_userManager.SetLockoutEndDateAsync(userIdentity, new DateTimeOffset(SystemTimeProvider.Instance.Get())));

			return Unit.Value;
		}

		private static async Task ThrowIfFailed(Task<IdentityResult> result, string message = FailedResetUserIdentityFailedAccessMessage)
		{
			var identityResult = await result;

			if (identityResult.Succeeded)
				return;

			throw new IdentityOperationFailureException(message, identityResult.Errors.Select(e => e.Description));
		}
	}
}