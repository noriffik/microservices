﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.Identity.Application.UserIdentities.Commands.ResetUserIdentityFailedAccessCount
{
    public class ResetUserIdentityFailedAccessCountCommand : IRequest
	{
		[DataMember]
		public string Id { get; set; }
	}
}