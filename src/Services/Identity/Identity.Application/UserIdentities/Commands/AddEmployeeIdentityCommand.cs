﻿using System.Runtime.Serialization;
using MediatR;
using NexCore.Identity.Common.Entities;

namespace NexCore.Identity.Application.UserIdentities.Commands
{
	[DataContract]
	public abstract class AddEmployeeIdentityCommand<TEmployee> : IRequest<string> where TEmployee : Employee
	{
		[DataMember]
		public string UserName { get; set; }

		[DataMember]
		public string Password { get; set; }

		[DataMember]
		public int EmployeeId { get; set; }

		[DataMember]
		public bool LockEnabled { get; set; }
	}
}