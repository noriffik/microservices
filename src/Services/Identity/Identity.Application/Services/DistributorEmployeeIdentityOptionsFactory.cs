﻿using NexCore.Identity.Application.Services.Factories;
using NexCore.Identity.Application.UserIdentities.Commands.DistributorEmployees.AddDistributorEmployeeIdentity;
using NexCore.Identity.Common.Entities;
using NexCore.Identity.Common.Persistence;

namespace NexCore.Identity.Application.Services
{
	public class DistributorEmployeeIdentityOptionsFactory : EmployeeIdentityOptionsFactory<DistributorEmployee, AddDistributorEmployeeIdentityCommand>
	{
		public DistributorEmployeeIdentityOptionsFactory(AdminIdentityDbContext context, EmployeeClaimFactory<DistributorEmployee> claimFactory) : base(context, claimFactory)
		{
		}

		protected override UserIdentityEmployment MapEmployment(AddDistributorEmployeeIdentityCommand request)
		{
			var employment = base.MapEmployment(request);

			employment.DistributorId = request.DistributorId;

			return employment;
		}

		protected override string GetRoleName() => "DistributorEmployee";
	}
}