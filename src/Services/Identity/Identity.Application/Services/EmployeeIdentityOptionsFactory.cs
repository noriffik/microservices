﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using NexCore.Identity.Application.Exceptions;
using NexCore.Identity.Application.Services.Factories;
using NexCore.Identity.Application.UserIdentities.Commands;
using NexCore.Identity.Common.Entities;
using NexCore.Identity.Common.Persistence;

[assembly: InternalsVisibleTo("NexCore.Testing")]
[assembly: InternalsVisibleTo("NexCore.Identity.Api.UnitTests.UnitTests")]
namespace NexCore.Identity.Application.Services
{
	public abstract class EmployeeIdentityOptionsFactory<TEmployee, TCommand>
		where TEmployee : Employee
		where TCommand : AddEmployeeIdentityCommand<TEmployee>
	{
		private const string EmployeeNotFound = "Employee not found";

		private readonly AdminIdentityDbContext _context;
		private readonly EmployeeClaimFactory<TEmployee> _claimFactory;

		protected EmployeeIdentityOptionsFactory(AdminIdentityDbContext context, EmployeeClaimFactory<TEmployee> claimFactory)
		{
			_context = context ?? throw new ArgumentNullException(nameof(context));
			_claimFactory = claimFactory ?? throw new ArgumentNullException(nameof(claimFactory));
		}

		public virtual async Task<UserIdentityOptions> Create(TCommand request)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request));

			var employee = await FindOrThrow(request.EmployeeId);

			return new UserIdentityOptions(new UserIdentity
				{
					UserName = request.UserName,
					PhoneNumber = employee.Telephone,
					Email = employee.Email,
					DisplayName = employee.Name,
					LockoutEnabled = request.LockEnabled,
					Employment = MapEmployment(request)
				},
				request.Password)
			{
				RoleName = GetRoleName(),
				Claims = _claimFactory.Create(employee)
			};
		}

		protected virtual UserIdentityEmployment MapEmployment(TCommand request)
		{
			return new UserIdentityEmployment
			{
				EmployeeId = request.EmployeeId
			};
		}

		protected abstract string GetRoleName();

		private async Task<TEmployee> FindOrThrow(int employeeId)
		{
			var employee = await _context.Set<TEmployee>().FindAsync(employeeId);
			if (employee == null)
				throw new IdentityOperationFailureException(EmployeeNotFound);

			return employee;
		}
	}
}