﻿using Microsoft.AspNetCore.Identity;
using NexCore.Identity.Application.Exceptions;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.Identity.Application.Services
{
	public static class IdentityResultExtensions
	{
		private const string Message = "Failed execute identity operation";

		public static async Task ThrowIfFailed(this Task<IdentityResult> result, string message = Message)
		{
			var identityResult = await result;
			if (identityResult.Succeeded)
				return;

			throw new IdentityOperationFailureException(message, identityResult.Errors.Select(e => e.Description));
		}
	}
}