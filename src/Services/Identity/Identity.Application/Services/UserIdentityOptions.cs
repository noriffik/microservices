﻿using System.Collections.Generic;
using System.Security.Claims;
using NexCore.Identity.Common.Entities;

namespace NexCore.Identity.Application.Services
{
	public class UserIdentityOptions
	{
		public UserIdentity User { get; set; }

		public string Password { get; set; }

		public IEnumerable<Claim> Claims { get; set; }

		public string RoleName { get; set; }

		public UserIdentityOptions(UserIdentity user, string password)
		{
			User = user;
			Password = password;
		}
	}
}