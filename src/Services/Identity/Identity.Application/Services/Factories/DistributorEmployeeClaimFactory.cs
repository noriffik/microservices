﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using NexCore.Identity.Common;
using NexCore.Identity.Common.Entities;

namespace NexCore.Identity.Application.Services.Factories
{
	public class DistributorEmployeeClaimFactory : EmployeeClaimFactory<DistributorEmployee>
	{
		public override IEnumerable<Claim> Create(DistributorEmployee employee)
		{
			return base.Create(employee).Concat(new[]
			{
				new Claim(IdentityClaimTypes.EmployeeDistributorId, employee.DistributorId),
				new Claim(IdentityClaimTypes.EmployeeDistributorOrganizationId, employee.DistributorOrganizationId.ToString())
			});
		}
	}
}