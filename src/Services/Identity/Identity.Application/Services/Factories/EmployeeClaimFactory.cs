﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using NexCore.Identity.Common;
using NexCore.Identity.Common.Entities;

namespace NexCore.Identity.Application.Services.Factories
{
	public abstract class EmployeeClaimFactory<TEmployee> where TEmployee : Employee
	{
		public virtual IEnumerable<Claim> Create(TEmployee employee)
		{
			if (employee is null)
				throw new ArgumentNullException(nameof(employee));

			return new[]
			{
				new Claim(IdentityClaimTypes.IndividualId, employee.IndividualId.ToString()),
				new Claim(IdentityClaimTypes.EmployeeId, employee.Id.ToString()),
				new Claim(IdentityClaimTypes.EmployeeJobId, employee.JobId.ToString())
			};
		}
	}
}