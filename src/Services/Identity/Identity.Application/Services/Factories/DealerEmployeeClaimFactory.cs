﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using NexCore.Identity.Common;
using NexCore.Identity.Common.Entities;

namespace NexCore.Identity.Application.Services.Factories
{
	public class DealerEmployeeClaimFactory : EmployeeClaimFactory<DealerEmployee>
	{
		public override IEnumerable<Claim> Create(DealerEmployee employee)
		{
			return base.Create(employee).Concat(new[]
			{
				new Claim(IdentityClaimTypes.EmployeeDealerId, employee.DealerId),
				new Claim(IdentityClaimTypes.EmployeeDealerOrganizationId, employee.DealerOrganizationId.ToString()),
				new Claim(IdentityClaimTypes.EmployeeDealerDistributorId, employee.DistributorId),
				new Claim(IdentityClaimTypes.EmployeeDealerDistributorOrganizationId, employee.DistributorOrganizationId.ToString())
			});
		}
	}
}