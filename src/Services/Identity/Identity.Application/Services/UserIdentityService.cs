﻿using Microsoft.AspNetCore.Identity;
using NexCore.Identity.Application.Exceptions;
using NexCore.Identity.Common.Entities;
using System;
using System.Threading.Tasks;

namespace NexCore.Identity.Application.Services
{
    public class UserIdentityService
	{
		private const string FailedToAddUserIdentityMessage = "Failed to add user identity.";
		private const string ExistUserIdentityMessage = "Failed to add user identity. Exist user name";

		private readonly UserManager<UserIdentity> _userManager;

        public UserIdentityService(UserManager<UserIdentity> userManager)
		{
			_userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
		}

		public virtual async Task<string> Create(UserIdentityOptions options)
		{
			await ThrowIfUserNameIsInUse(options.User);

			await _userManager.CreateAsync(options.User, options.Password)
				.ThrowIfFailed(FailedToAddUserIdentityMessage);

			await _userManager.SetLockoutEnabledAsync(options.User, options.User.LockoutEnabled)
				.ThrowIfFailed(FailedToAddUserIdentityMessage);

			return options.User.Id;
		}

		public virtual async Task AddRole(UserIdentityOptions options)
		{
			await _userManager.AddToRoleAsync(options.User, options.RoleName)
				.ThrowIfFailed(FailedToAddUserIdentityMessage);
		}

		public virtual async Task AddClaims(UserIdentityOptions options)
		{
			await _userManager.AddClaimsAsync(options.User, options.Claims)
				.ThrowIfFailed(FailedToAddUserIdentityMessage);
		}
		
		private async Task ThrowIfUserNameIsInUse(UserIdentity user)
		{
			if (await _userManager.FindByNameAsync(user.UserName) != null)
				throw new IdentityOperationFailureException(ExistUserIdentityMessage);
		}
	}
}