﻿using NexCore.Identity.Application.Services.Factories;
using NexCore.Identity.Application.UserIdentities.Commands.DealerEmployees.AddDealerEmployeeIdentity;
using NexCore.Identity.Common.Entities;
using NexCore.Identity.Common.Persistence;

namespace NexCore.Identity.Application.Services
{
	public class DealerEmployeeIdentityOptionsFactory : EmployeeIdentityOptionsFactory<DealerEmployee, AddDealerEmployeeIdentityCommand>
	{
		public DealerEmployeeIdentityOptionsFactory(AdminIdentityDbContext context, EmployeeClaimFactory<DealerEmployee> claimFactory) : base(context, claimFactory)
		{
		}

		protected override UserIdentityEmployment MapEmployment(AddDealerEmployeeIdentityCommand request)
		{
			var employment = base.MapEmployment(request);

			employment.DistributorId = request.DistributorId;
			employment.DealerId = request.DealerId;

			return employment;
		}
		protected override string GetRoleName() => "DealerEmployee";
	}
}