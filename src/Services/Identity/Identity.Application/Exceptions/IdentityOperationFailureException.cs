﻿using System;
using System.Collections.Generic;

namespace NexCore.Identity.Application.Exceptions
{
    public class IdentityOperationFailureException : Exception
	{
		public IdentityOperationFailureException()
		{
		}

		public IdentityOperationFailureException(string message) : base(message)
		{
		}

		public IdentityOperationFailureException(string message, IEnumerable<string> errors) : base(message)
        {
            Errors = errors ?? throw new ArgumentNullException(nameof(errors));
        }

		public IdentityOperationFailureException(string message, Exception innerException) : base(message, innerException)
		{
		}

        public IEnumerable<string> Errors { get; } = new List<string>();
    }
}