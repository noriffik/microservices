﻿'use strict';
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', { title: 'Demo Identity API JS Client' });
});

router.get('/callback',
    function(req, res) {
        res.render('callback');
    });

module.exports = router;
