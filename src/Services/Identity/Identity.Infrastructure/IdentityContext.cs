﻿using Microsoft.EntityFrameworkCore;
using NexCore.Identity.Common.Persistence;

namespace NexCore.Identity.Infrastructure
{
	public class IdentityContext : AdminIdentityDbContext
	{
		public IdentityContext(DbContextOptions<AdminIdentityDbContext> options) : base(options)
		{
		}
	}
}