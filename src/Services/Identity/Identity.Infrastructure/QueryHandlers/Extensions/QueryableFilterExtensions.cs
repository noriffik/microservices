﻿using System.Linq;

namespace NexCore.Identity.Infrastructure.QueryHandlers.Extensions
{
	internal static class QueryableFilterExtensions
	{
		public static IQueryable<UserIdentityRecord> ByUserName(this IQueryable<UserIdentityRecord> query,
			string value)
		{
			if (string.IsNullOrEmpty(value)) 
				return query;

			var name = value.ToLowerInvariant().Trim();
			return query.Where(c => c.User.UserName.ToLowerInvariant() == name);
		}

		public static IQueryable<UserIdentityRecord> ByDisplayName(this IQueryable<UserIdentityRecord> query,
			string value)
		{
			if (string.IsNullOrEmpty(value))
				return query;

			var name = value.ToLowerInvariant().Trim();
			return query.Where(c => c.User.DisplayName.ToLowerInvariant() == name);
		}
	}
}