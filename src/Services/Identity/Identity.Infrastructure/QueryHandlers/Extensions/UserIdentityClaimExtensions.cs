﻿using NexCore.Identity.Common.Entities;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Identity.Infrastructure.QueryHandlers.Extensions
{
	public static class UserIdentityClaimExtensions
    {
        public static string FindValue(this IEnumerable<UserIdentityUserClaim> claims, string type)
        {
            var claim = claims.SingleOrDefault(c => c.ClaimType == type);

            return claim?.ClaimValue;
        }
    }
}