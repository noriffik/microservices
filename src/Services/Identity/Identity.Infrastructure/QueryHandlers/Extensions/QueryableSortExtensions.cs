﻿using System;
using System.Linq;
using System.Linq.Expressions;
using NexCore.Application.Queries;
using NexCore.Identity.Application.UserIdentities.Queries;

namespace NexCore.Identity.Infrastructure.QueryHandlers.Extensions
{
	internal static class QueryableSortExtensions
	{
		public static IQueryable<UserIdentityRecord> SortBy(this IQueryable<UserIdentityRecord> query,
			SortCriterion? sortCriterion, SortDirection? sortDirection)
		{
			switch (sortCriterion)
			{
				case SortCriterion.UserName:
					return query.InnerSort(r => r.User.UserName, sortDirection);

				case SortCriterion.DisplayName:
					return query.InnerSort(r => r.User.DisplayName, sortDirection);

				default:
					return query.InnerSort(r => r.User.UserName, sortDirection);
			}
		}

		private static IQueryable<UserIdentityRecord> InnerSort(this IQueryable<UserIdentityRecord> query,
			Expression<Func<UserIdentityRecord, object>> keySelector, SortDirection? sortDirection)
		{
			return sortDirection == SortDirection.Descending
				? query.OrderByDescending(keySelector)
				: query.OrderBy(keySelector);
		}
	}
}