﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Extensions;
using NexCore.Application.Queries;
using NexCore.Identity.Application.UserIdentities.Queries.DealerEmployees;
using NexCore.Identity.Common.Persistence;
using NexCore.Identity.Infrastructure.QueryHandlers.Extensions;
using NexCore.Infrastructure.QueryHandlers;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Identity.Infrastructure.QueryHandlers.DealerEmployees
{
	public class DealerEmployeeIdentityQueryHandler : EmployeeIdentityQueryHandler, IRequestHandler<DealerEmployeeIdentityQuery, PagedResponse<DealerEmployeeIdentityDto>>,
		IRequestHandler<DealerEmployeeIdentityByIdQuery, DealerEmployeeIdentityDto>
	{
		public DealerEmployeeIdentityQueryHandler(AdminIdentityDbContext context, IMapper mapper) : base(context,
			mapper)
		{
		}

		public async Task<PagedResponse<DealerEmployeeIdentityDto>> Handle(
			DealerEmployeeIdentityQuery request, CancellationToken cancellationToken)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request));

			var query = RecordSet(request.DistributorId)
				.Where(x => x.User.Employment.DealerId == request.DealerId)
				.ByUserName(request.UserName)
				.ByDisplayName(request.DisplayName);

			var result = await query
				.SortBy(request.SortCriteria, request.SortDirection)
				.Paginate(request, cancellationToken);

			return Mapper.MapPagedResponse<UserIdentityRecord, DealerEmployeeIdentityDto>(result);
		}

		public async Task<DealerEmployeeIdentityDto> Handle(DealerEmployeeIdentityByIdQuery request, CancellationToken cancellationToken)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request));

			var query = RecordSet(request.DistributorId);

			var result = await query
				.Where(x => x.User.Employment.DealerId == request.DealerId)
				.Where(x => x.User.Id == request.Id)
				.SingleOrDefaultAsync(cancellationToken);

			var mapin = Mapper.Map<DealerEmployeeIdentityDto>(result);
			return mapin;
		}
	}
}