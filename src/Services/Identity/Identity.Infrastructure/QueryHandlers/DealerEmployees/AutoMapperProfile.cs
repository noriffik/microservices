﻿using AutoMapper;
using NexCore.Identity.Application.UserIdentities.Queries.DealerEmployees;
using NexCore.Identity.Common;
using NexCore.Identity.Common.Entities;
using NexCore.Identity.Infrastructure.QueryHandlers.Extensions;

namespace NexCore.Identity.Infrastructure.QueryHandlers.DealerEmployees
{
	public class AutoMapperProfile : Profile
	{
		public AutoMapperProfile()
		{
			CreateMap<UserIdentityRecord, DealerEmployeeIdentityDto>()
				.ForMember(d => d.DisplayName, m => m.MapFrom(s => s.User.DisplayName))
				.ForMember(d => d.UserName, m => m.MapFrom(s => s.User.UserName))
				.ForMember(d => d.Id, m => m.MapFrom(s => s.User.Id))
				.ForMember(d => d.Email, m => m.MapFrom(s => s.User.Email))
				.ForMember(d => d.LockoutEnabled, m => m.MapFrom(s => s.User.LockoutEnabled))
				.ForMember(d => d.LockoutEnd, m => m.MapFrom(s => s.User.LockoutEnd))
				.ForMember(d => d.PhoneNumber, m => m.MapFrom(s => s.User.PhoneNumber))
				.ForMember(d => d.EmployeeId,
					m => m.MapFrom(s => s.Claims.FindValue(IdentityClaimTypes.EmployeeId)))
				.ForMember(d => d.EmployeeDistributorId,
					m => m.MapFrom(s => s.Claims.FindValue(IdentityClaimTypes.EmployeeDistributorId)))
				.ForMember(d => d.EmployeeDistributorOrganizationId,
					m => m.MapFrom(s => s.Claims.FindValue(IdentityClaimTypes.EmployeeDistributorOrganizationId)))
				.ForMember(d => d.EmployeeDealerId,
					m => m.MapFrom(s => s.Claims.FindValue(IdentityClaimTypes.EmployeeDealerId)))
				.ForMember(d => d.EmployeeDealerOrganizationId,
					m => m.MapFrom(s => s.Claims.FindValue(IdentityClaimTypes.EmployeeDealerOrganizationId)))
				.ForMember(d => d.IndividualId,
					m => m.MapFrom(s => s.Claims.FindValue(IdentityClaimTypes.IndividualId)))
				.ForMember(d => d.EmployeeJobId,
					m => m.MapFrom(s => s.Claims.FindValue(IdentityClaimTypes.EmployeeJobId)));
			
			CreateMap<UserIdentity, DealerEmployeeIdentityDto>()
				.ForMember(d => d.IndividualId, m => m.Ignore())
				.ForMember(d => d.EmployeeId, m => m.Ignore())
				.ForMember(d => d.EmployeeDistributorId, m => m.Ignore())
				.ForMember(d => d.EmployeeDealerId, m => m.Ignore())
				.ForMember(d => d.EmployeeDealerOrganizationId, m => m.Ignore())
				.ForMember(d => d.EmployeeDistributorOrganizationId, m => m.Ignore())
				.ForMember(d => d.EmployeeJobId, m => m.Ignore());
		}
	}
}