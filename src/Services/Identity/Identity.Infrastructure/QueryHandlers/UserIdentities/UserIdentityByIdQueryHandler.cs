﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Identity.Application.UserIdentities.Queries;
using NexCore.Identity.Common.Persistence;
using System;
using System.Threading;
using System.Threading.Tasks;
using NexCore.Identity.Application.UserIdentities.Queries.UserIdentity;

namespace NexCore.Identity.Infrastructure.QueryHandlers.UserIdentities
{
    public class UserIdentityByIdQueryHandler : IRequestHandler<UserIdentityByIdQuery, UserIdentityDto>
	{
		private readonly AdminIdentityDbContext _context;
		private readonly IMapper _mapper;

		public UserIdentityByIdQueryHandler(AdminIdentityDbContext context, IMapper mapper)
		{
			_context = context ?? throw new ArgumentNullException(nameof(context));
			_mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
		}

		public async Task<UserIdentityDto> Handle(UserIdentityByIdQuery request, CancellationToken cancellationToken)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request));
			var record = await _context.Users.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
			return _mapper.Map<UserIdentityDto>(record);
		}
	}
}