﻿using AutoMapper;
using NexCore.Identity.Application.UserIdentities.Queries;
using NexCore.Identity.Application.UserIdentities.Queries.UserIdentity;
using NexCore.Identity.Common.Entities;

namespace NexCore.Identity.Infrastructure.QueryHandlers.UserIdentities
{
	public class AutoMapperProfile : Profile
	{
		public AutoMapperProfile()
		{
			CreateMap<UserIdentity, UserIdentityDto>();
		}
	}
}