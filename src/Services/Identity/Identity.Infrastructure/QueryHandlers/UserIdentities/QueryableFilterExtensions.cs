﻿using System.Linq;
using NexCore.Identity.Common.Entities;

namespace NexCore.Identity.Infrastructure.QueryHandlers.UserIdentities
{
	internal static class QueryableFilterExtensions
	{
		public static IQueryable<UserIdentity> ByUserName(this IQueryable<UserIdentity> query,
			string value)
		{
			if (string.IsNullOrEmpty(value))
				return query;

			var name = value.ToLowerInvariant().Trim();
			return query.Where(c => c.UserName.ToLowerInvariant() == name);
		}

		public static IQueryable<UserIdentity> ByDisplayName(this IQueryable<UserIdentity> query,
			string value)
		{
			if (string.IsNullOrEmpty(value))
				return query;

			var name = value.ToLowerInvariant().Trim();
			return query.Where(c => c.DisplayName.ToLowerInvariant() == name);
		}
	}
}