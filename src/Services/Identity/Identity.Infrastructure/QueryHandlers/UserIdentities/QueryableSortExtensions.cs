﻿using NexCore.Application.Queries;
using NexCore.Identity.Application.UserIdentities.Queries.UserIdentity;
using NexCore.Identity.Common.Entities;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.Identity.Infrastructure.QueryHandlers.UserIdentities
{
	public static class QueryableSortExtensions
	{
		public static IQueryable<UserIdentity> SortBy(this IQueryable<UserIdentity> query, SortCriterion? sortCriterion,
			SortDirection? sortDirection)
		{
			switch (sortCriterion)
			{
				case SortCriterion.UserName:
					return query.InnerSort(m => m.UserName, sortDirection);

				case SortCriterion.Email:
					return query.InnerSort(m => m.Email, sortDirection);

				default:
					return query.OrderBy(m => m.UserName);
			}
		}

		private static IQueryable<UserIdentity> InnerSort(this IQueryable<UserIdentity> query,
			Expression<Func<UserIdentity, object>> keySelector, SortDirection? sortDirection)
		{
			return sortDirection == SortDirection.Descending
				? query.OrderByDescending(keySelector)
				: query.OrderBy(keySelector);
		}
	}
}