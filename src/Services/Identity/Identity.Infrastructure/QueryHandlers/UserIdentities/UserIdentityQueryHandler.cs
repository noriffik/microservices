﻿using AutoMapper;
using MediatR;
using NexCore.Application.Extensions;
using NexCore.Application.Queries;
using NexCore.Identity.Application.UserIdentities.Queries.UserIdentity;
using NexCore.Identity.Common.Entities;
using NexCore.Identity.Common.Persistence;
using NexCore.Infrastructure.QueryHandlers;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Identity.Infrastructure.QueryHandlers.UserIdentities
{
	public class UserIdentityQueryHandler : IRequestHandler<UserIdentityQuery, PagedResponse<UserIdentityDto>>
	{
		private readonly AdminIdentityDbContext _context;
		private readonly IMapper _mapper;

		public UserIdentityQueryHandler(AdminIdentityDbContext context, IMapper mapper)
		{
			_context = context ?? throw new ArgumentNullException(nameof(context));
			_mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
		}

		public async Task<PagedResponse<UserIdentityDto>> Handle(UserIdentityQuery request, CancellationToken cancellationToken)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request));

			var result = await _context.Users
				.ByUserName(request.UserName)
				.ByDisplayName(request.DisplayName)
				.SortBy(request.SortBy, request.SortDirection)
				.Paginate(request, cancellationToken);

			return _mapper.MapPagedResponse<UserIdentity, UserIdentityDto>(result);
		}
	}
}