﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Extensions;
using NexCore.Application.Queries;
using NexCore.Identity.Application.UserIdentities.Queries.DistributorEmployees;
using NexCore.Identity.Common.Persistence;
using NexCore.Identity.Infrastructure.QueryHandlers.Extensions;
using NexCore.Infrastructure.QueryHandlers;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Identity.Infrastructure.QueryHandlers.DistributorEmployees
{
	public class DistributorEmployeeIdentityQueryHandler : EmployeeIdentityQueryHandler, IRequestHandler<DistributorEmployeeIdentityQuery, PagedResponse<DistributorEmployeeIdentityDto>>, 
		IRequestHandler<DistributorEmployeeIdentityByIdQuery, DistributorEmployeeIdentityDto>
    {
	    public DistributorEmployeeIdentityQueryHandler(AdminIdentityDbContext context, IMapper mapper) : base(context,
		    mapper)
	    {
	    }

		public async Task<PagedResponse<DistributorEmployeeIdentityDto>> Handle(
			DistributorEmployeeIdentityQuery request, CancellationToken cancellationToken)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request));

			var query = RecordSet(request.DistributorId)
				.ByUserName(request.UserName)
				.ByDisplayName(request.DisplayName);

			var result = await query
				.SortBy(request.SortCriterion, request.SortDirection)
				.Paginate(request, cancellationToken);

			return Mapper.MapPagedResponse<UserIdentityRecord, DistributorEmployeeIdentityDto>(result);
		}

		public async Task<DistributorEmployeeIdentityDto> Handle(DistributorEmployeeIdentityByIdQuery request, CancellationToken cancellationToken)
		{
			if (request == null)
				throw new ArgumentNullException(nameof(request));

			var query = RecordSet(request.DistributorId);
			var result = await query.Where(x => x.User.Id == request.Id).SingleOrDefaultAsync(cancellationToken);

			return Mapper.Map<DistributorEmployeeIdentityDto>(result);
		}
	}
}