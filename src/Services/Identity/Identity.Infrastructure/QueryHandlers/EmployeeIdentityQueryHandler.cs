﻿using AutoMapper;
using NexCore.Identity.Common.Persistence;
using System;
using System.Linq;

namespace NexCore.Identity.Infrastructure.QueryHandlers
{
	public class EmployeeIdentityQueryHandler
	{
		protected readonly AdminIdentityDbContext Context;
		protected readonly IMapper Mapper;

		public EmployeeIdentityQueryHandler(AdminIdentityDbContext context, IMapper mapper)
		{
			Context = context ?? throw new ArgumentNullException(nameof(context));
			Mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
		}

		public IQueryable<UserIdentityRecord> RecordSet(string distributorId)
		{
			return from user in Context.Users
				where user.Employment != null
				where user.Employment.DistributorId.Equals(distributorId, StringComparison.CurrentCultureIgnoreCase)
				join claim in Context.UserClaims on user.Id equals claim.UserId into claims
				select new UserIdentityRecord { User = user, Claims = claims };
		}
	}
}