﻿using NexCore.Identity.Common.Entities;
using System.Collections.Generic;

namespace NexCore.Identity.Infrastructure.QueryHandlers
{
	public class UserIdentityRecord
    {
        public UserIdentity User { get; set; }

        public IEnumerable<UserIdentityUserClaim> Claims { get; set; }
    }
}