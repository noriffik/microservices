﻿using IdentityModel.Client;
using System;
using System.Threading.Tasks;

namespace NexCore.Identity.Client
{
    public class Program
    {
        public static Task Main(string[] args)
        {
            return Run(args);
        }

        private static async Task Run(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Gain access to demo API using:");
                Console.WriteLine("1. Using client's secret");
                Console.WriteLine("2. Using user's credentials");
                Console.WriteLine("0. Quit");
                Console.Write("\n\n");

                var option = ReadUserOption();

                TokenResponse tokenResponse;

                var client = args.Length == 2 ? new IdentityClient(args[0], args[1]) : new IdentityClient();

                try
                {
                    switch (option)
                    {
                        case 0:
                            return;
                        case 1:
                            tokenResponse = await client.RequestWithSecret(ReadClientSecret());
                            break;
                        case 2:
                            tokenResponse = await client.RequestWithUserCredentials(ReadUserCredentials());
                            break;
                        default:
                            throw new InvalidOperationException("Should never happen");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.Write("\n\n");

                    continue;
                }

                Console.WriteLine("Token:");
                Console.WriteLine(tokenResponse.Json);
                Console.Write("\n\n");

                var resourceResponse = await client.QueryResource(tokenResponse.AccessToken);

                Console.WriteLine("API:");
                Console.WriteLine(resourceResponse);
                Console.Write("\n\n");
            }
        }

        private static int ReadUserOption()
        {
            Console.Write("> ");

            var inputMethod = Console.ReadLine();
            if (!int.TryParse(inputMethod, out var method) || (method < 0 && method > 2))
            {
                Console.WriteLine("Unknown option selected");

                ReadUserOption();
            }

            Console.Write("\n\n");

            return method;
        }

        private static string ReadClientSecret()
        {
            Console.Write("Enter secret: ");
            var secret = Console.ReadLine();

            Console.Write("\n\n");
            return secret;
        }

        private static Tuple<string, string> ReadUserCredentials()
        {
            Console.Write("Enter username: ");
            var username = Console.ReadLine();
            Console.Write("Enter password: ");
            var password = Console.ReadLine();

            Console.Write("\n\n");

            return new Tuple<string, string>(username, password);
        }
    }
}
