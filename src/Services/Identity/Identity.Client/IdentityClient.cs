﻿using IdentityModel.Client;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace NexCore.Identity.Client
{
    public class IdentityClient
    {
        private const string ClientId = "NexCore.Identity.Client";
        private const string Scope = "NexCore.Identity.Resource";

        private readonly string _serverUrl;
        private readonly string _resourceUrl;
        
        public IdentityClient() : this("http://localhost:5000", "http://localhost:5001/api/identity")
        {
        }

        public IdentityClient(string serverUrl, string resourceUrl)
        {
            _serverUrl = serverUrl;
            _resourceUrl = resourceUrl;
        }

        public async Task<TokenResponse> RequestWithSecret(string secret)
        {
            var discovery = await DiscoverIdentityService();

            var client = new HttpClient();
            var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = discovery.TokenEndpoint,
                ClientId = ClientId,
                ClientSecret = secret,
                Scope = Scope
            });

            if (tokenResponse.IsError)
                throw new Exception(tokenResponse.Error);
            
            return tokenResponse;
        }

        public async Task<DiscoveryResponse> DiscoverIdentityService()
        {
            var client = new HttpClient();

            var discovery = await client.GetDiscoveryDocumentAsync(_serverUrl);
            if (discovery.IsError)
                throw new Exception(discovery.Error);

            return discovery;
        }

        public async Task<TokenResponse> RequestWithUserCredentials(Tuple<string, string> credentials)
        {
            var discovery = await DiscoverIdentityService();

            var client = new HttpClient();
            var tokenResponse = await client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = discovery.TokenEndpoint,
                ClientId = ClientId,
                ClientSecret = "secret",
                UserName = credentials.Item1,
                Password = credentials.Item2,
                Scope = Scope
            });

            if (tokenResponse.IsError)
                throw new Exception(tokenResponse.Error);

            return tokenResponse;
        }

        public async Task<string> QueryResource(string accessToken)
        {
            var client = new HttpClient();

            client.SetBearerToken(accessToken);

            var response = await client.GetAsync(_resourceUrl);

            return response.IsSuccessStatusCode
                ? await response.Content.ReadAsStringAsync()
                : response.StatusCode.ToString();
        }
    }
}
