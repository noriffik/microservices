﻿using System;

namespace NexCore.Identity.Domain.UserIdentity
{
	public class UserIdentityDto
	{
		public string Id { get; set; }
		public string UserName { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
		public bool LockoutEnabled { get; set; }
		public DateTimeOffset? LockoutEnd { get; set; }
	}
}