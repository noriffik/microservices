﻿using Microsoft.Extensions.Configuration;
using NexCore.Identity.Common.Persistence;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Setup;
using System;

namespace NexCore.Identity.Api.Setup.Tasks
{
	public class AdminIdentityDatabaseSetupTask : DatabaseSetupTask<AdminIdentityDbContext>
	{
		private const string IsSeedingEnabledKey = "IsSeedingEnabled";

		public AdminIdentityDatabaseSetupTask(AdminIdentityDbContext context) : base(context)
		{
		}

		public AdminIdentityDatabaseSetupTask(AdminIdentityDbContext context, IDbContextSeeder<AdminIdentityDbContext> seeder,
			IConfiguration configuration) : base(context, seeder)
		{
			if (configuration == null)
				throw new ArgumentNullException(nameof(configuration));

			IsSeedingEnabled = configuration.GetValue(IsSeedingEnabledKey, true);
		}
	}
}
