﻿using Autofac;
using NexCore.Identity.Application.Services;
using NexCore.Identity.Application.Services.Factories;
using NexCore.Identity.Application.UserIdentities.Commands;
using NexCore.Identity.Application.UserIdentities.Commands.DealerEmployees.AddDealerEmployeeIdentity;
using NexCore.Identity.Application.UserIdentities.Commands.DistributorEmployees.AddDistributorEmployeeIdentity;
using NexCore.Identity.Common.Entities;

namespace NexCore.Identity.Api.Setup.AutofacModules
{
	public class ApplicationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			RegisterUserService(builder);
			RegisterUserIdentityOptionsFactory(builder);
		}

		private void RegisterUserIdentityOptionsFactory(ContainerBuilder builder)
		{
			builder.RegisterType<EmployeeClaimFactory<DealerEmployee>>()
				.As<DealerEmployeeClaimFactory>()
				.InstancePerLifetimeScope();

			builder.RegisterType<DealerEmployeeIdentityOptionsFactory>()
				.InstancePerLifetimeScope();

			builder.RegisterType<EmployeeClaimFactory<DistributorEmployee>>()
				.As<DistributorEmployeeClaimFactory>()
				.InstancePerLifetimeScope();

			builder.RegisterType<DistributorEmployeeIdentityOptionsFactory>()
				.InstancePerLifetimeScope();
		}

		private void RegisterUserService(ContainerBuilder builder)
		{
			builder.RegisterType<UserIdentityService>().As<UserIdentityService>().InstancePerLifetimeScope();
		}
	}
}