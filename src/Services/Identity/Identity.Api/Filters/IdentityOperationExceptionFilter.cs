﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NexCore.Identity.Application.Exceptions;
using NexCore.WebApi.ActionResults;
using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using ValidationProblemDetails = NexCore.WebApi.Filters.ValidationProblemDetails;

namespace NexCore.Identity.Api.Filters
{
    public class IdentityOperationExceptionFilter : IExceptionFilter
	{
		const string ProblemTitle = "An error has occured.";
		const string ProblemValidationDetail = "Please refer to the errors property for additional details.";

		private readonly IHostingEnvironment _env;
		private readonly ILogger<IdentityOperationExceptionFilter> _logger;

		public IdentityOperationExceptionFilter(IHostingEnvironment env, ILogger<IdentityOperationExceptionFilter> logger)
		{
			_env = env ?? throw new ArgumentNullException(nameof(env));
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
		}

		public void OnException(ExceptionContext context)
		{
			LogLevel logError;

			switch (context.Exception)
			{
				case ValidationException validationException:
					context.HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
					context.Result = ActionResultFrom(validationException, context.HttpContext.Request.Path);
					logError = LogLevel.Warning;
					break;
				case IdentityOperationFailureException identityOperationFailureException:
					context.HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
					context.Result = ActionResultFrom(identityOperationFailureException, context.HttpContext.Request.Path);
					logError = LogLevel.Error;
					break;
				default:
					context.HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
					context.Result = ActionResultFrom(context.Exception, context.HttpContext.Request.Path);
					logError = LogLevel.Critical;
					break;
			}
			_logger.Log(logError, new EventId(context.Exception.HResult), context.Exception, context.Exception.Message);

			context.ExceptionHandled = true;
		}

		private IActionResult ActionResultFrom(Exception exception, string instance)
		{
			var problemDetails = new ProblemDetails
			{
				Title = ProblemTitle,
				Instance = instance,
				Status = StatusCodes.Status500InternalServerError,
				Detail = _env.IsDevelopment() ? exception.ToString() : null
			};

			return new ExceptionObjectResult(problemDetails);
		}

		private IActionResult ActionResultFrom(IdentityOperationFailureException exception, string instance)
		{
			var problemDetails = new IdentityProblemDetails
			{
				Title = exception.Message,
				Instance = instance,
				Status = StatusCodes.Status400BadRequest,
                Errors = exception.Errors,
				Detail = _env.IsDevelopment() ? exception.ToString() : null
			};

			return new BadRequestObjectResult(problemDetails);
		}

		private IActionResult ActionResultFrom(ValidationException exception, string instance)
		{
			var problemDetails = new ValidationProblemDetails
			{
				Title = ProblemTitle,
				Instance = instance,
				Status = StatusCodes.Status400BadRequest,
				Message = exception.Message,
				Detail = ProblemValidationDetail
			};

			FillValidationErrors(exception, problemDetails.Errors);

			return new BadRequestObjectResult(problemDetails);
		}

		private static void FillValidationErrors(ValidationException exception, IDictionary<string, object[]> validationErrors)
		{
			var temp = new Dictionary<string, List<object>>();
			foreach (var error in exception.Errors)
			{
				if (!temp.ContainsKey(error.PropertyName))
					temp.Add(error.PropertyName, new List<object>());

				temp[error.PropertyName].Add(
					error.FormattedMessagePlaceholderValues != null && error.FormattedMessagePlaceholderValues.ContainsKey(error.PropertyName)
						? error.FormattedMessagePlaceholderValues[error.PropertyName]
						: new { message = error.ErrorMessage });
			}

			temp.Keys
				.ToList()
				.ForEach(p => validationErrors.Add(p, temp[p].ToArray()));
		}
	}

    public class IdentityProblemDetails : ProblemDetails
    {
        public IEnumerable<string> Errors { get; set; }
    }

    public class ValidationProblemDetails : ProblemDetails
    {
	    public string Message { get; set; }

	    public IDictionary<string, object[]> Errors { get; } = new Dictionary<string, object[]>();

    }
}