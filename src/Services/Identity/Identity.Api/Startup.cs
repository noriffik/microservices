﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NexCore.Application.Extensions;
using NexCore.Identity.Api.Extensions;
using NexCore.Identity.Api.Setup.AutofacModules;
using NexCore.Identity.Application.UserIdentities.Commands.AddUserIdentity;
using NexCore.Identity.Infrastructure;
using NexCore.WebApi.AutofacModules;
using NexCore.WebApi.Extensions;
using System;

namespace NexCore.Identity.Api
{
	public class Startup
	{
		public IConfiguration Configuration { get; }

		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public IServiceProvider ConfigureServices(IServiceCollection services)
		{
			var api = typeof(Startup).Assembly;
			var application = typeof(AddUserIdentityCommand).Assembly;
			var infrastructure = typeof(IdentityContext).Assembly;

            services.AddMvc();
			
			services.AddTransient<IConfigureOptions<MvcOptions>, MvcOptionsSetup>()
                .AddMigrationServiceWait(Configuration)
				.AddAuthorizationService(Configuration)
				.AddAutoMapper(application, infrastructure)
				.AddHealthChecks(Configuration)
				.AddSwagger(Configuration)
				.AddMvc();

			ConfigureAuthentication(services);

			var containerBuilder = services.GetAutofacContainerBuilder(application, infrastructure);
			containerBuilder.RegisterModule(new ApplicationModule());
			containerBuilder.RegisterModule(new AuthorizationRequirementsModule(api));

			return new AutofacServiceProvider(containerBuilder.Build());
		}

		protected virtual void ConfigureAuthentication(IServiceCollection services)
		{
			services.AddJwtBearerAuthentication(Configuration, "NexCore.Identity.Api");
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseAuthentication();

			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");

				c.OAuthClientId("NexCore.Identity.Api.Swagger");
			});

			app.UseMvc();
		}
	}
}
