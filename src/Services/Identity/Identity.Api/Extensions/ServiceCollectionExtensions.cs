﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.HealthChecks;
using NexCore.Identity.Common.Entities;
using NexCore.Identity.Common.Persistence;
using NexCore.Infrastructure.Setup;
using NexCore.WebApi.Extensions;
using System;
using System.Collections.Generic;

namespace NexCore.Identity.Api.Extensions
{
    internal static class ServiceCollectionExtensions
	{
		public static IServiceCollection AddHealthChecks(this IServiceCollection services,
			IConfiguration configuration)
		{
			services.AddHealthChecks(checks =>
			{
				var cacheDurationInMinutes = configuration.GetValue("HealthCheck:CacheDurationInMinutes", 1);

				checks.AddSqlCheck(
					"Identity.Api",
					configuration["ConnectionString"],
					TimeSpan.FromMinutes(cacheDurationInMinutes));
			});

			return services;
		}

		public static IServiceCollection AddSwagger(this IServiceCollection services,
			IConfiguration configuration)
		{
			return services.AddSwaggerGen(configuration,
				"The Identity Api Service HTTP API",
				"v1",
				new Dictionary<string, string>
				{
					{"NexCore.Identity.Api", "Identity Service API"}
				});
		}

		public static IServiceCollection AddAuthorizationService(this IServiceCollection services, IConfiguration configuration)
		{
			services.AddDbContext<IdentityServerConfigurationDbContext>(options =>
				options.UseSqlServer(configuration["ConnectionString"]));
			services.AddDbContext<AdminIdentityDbContext>(options =>
				options.UseSqlServer(configuration["ConnectionString"]));

			services.AddDefaultIdentity<UserIdentity>()
				.AddRoles<UserIdentityRole>()
				.AddUserManager<UserManager<UserIdentity>>()
				.AddDefaultTokenProviders()
				.AddEntityFrameworkStores<AdminIdentityDbContext>();

			services.AddScoped<UserManager<UserIdentity>>();
			
			return services;
		}

        public static IServiceCollection AddMigrationServiceWait(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<MigrationServiceWaitTask>()
                .Configure<MigrationServiceWaitOptions>(configuration.GetSection("MigrationServiceWait"))
                .AddOptions<MigrationServiceWaitOptions>()
                .ValidateDataAnnotations();

            return services;
        }
	}
}