﻿using NexCore.DealerNetwork;
using NexCore.Identity.Api.Authorization.RequirementHandlers;
using NexCore.Identity.Api.Authorization.Requirements;

namespace NexCore.Identity.Api.Authorization.DealerEmployees.Queries.DealerEmployeeIdentityQuery
{
	public class DealerEmployeeHandler : ClaimAuthorizationHandler<DealerEmployeeIdentityRequirement, Application.UserIdentities.Queries.DealerEmployees.DealerEmployeeIdentityQuery>
	{
		public DealerEmployeeHandler() : base(Claims.EmployeeDistributorId, GetClaimValue)
		{ }

		private static string GetClaimValue(Application.UserIdentities.Queries.DealerEmployees.DealerEmployeeIdentityQuery command)
		{
			DealerId.TryParse(command.DealerId, out var dealerId);

			return dealerId;
		}
	}
}