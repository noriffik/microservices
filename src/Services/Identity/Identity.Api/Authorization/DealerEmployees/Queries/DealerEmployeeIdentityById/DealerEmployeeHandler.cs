﻿using NexCore.DealerNetwork;
using NexCore.Identity.Api.Authorization.RequirementHandlers;
using NexCore.Identity.Api.Authorization.Requirements;
using NexCore.Identity.Application.UserIdentities.Queries.DealerEmployees;

namespace NexCore.Identity.Api.Authorization.DealerEmployees.Queries.DealerEmployeeIdentityById
{
	public class DealerEmployeeHandler : ClaimAuthorizationHandler<DealerEmployeeIdentityRequirement, DealerEmployeeIdentityByIdQuery>
	{
		public DealerEmployeeHandler(): base(Claims.EmployeeDistributorId, GetClaimValue)
		{ }

		private static string GetClaimValue(DealerEmployeeIdentityByIdQuery command)
		{
			DealerId.TryParse(command.DealerId, out var dealerId);

			return dealerId;
		}
	}
}