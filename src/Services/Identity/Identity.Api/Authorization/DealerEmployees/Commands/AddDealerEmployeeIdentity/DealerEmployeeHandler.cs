﻿using NexCore.DealerNetwork;
using NexCore.Identity.Api.Authorization.RequirementHandlers;
using NexCore.Identity.Api.Authorization.Requirements;
using NexCore.Identity.Application.UserIdentities.Commands.DealerEmployees.AddDealerEmployeeIdentity;

namespace NexCore.Identity.Api.Authorization.DealerEmployees.Commands.AddDealerEmployeeIdentity
{
	public class DealerEmployeeHandler : ClaimAuthorizationHandler<DealerEmployeeIdentityRequirement, AddDealerEmployeeIdentityCommand>
	{
		public DealerEmployeeHandler() : base(Claims.EmployeeDealerId, GetClaimValue)
		{
		}

		private static string GetClaimValue(AddDealerEmployeeIdentityCommand command)
		{
			DealerId.TryParse(command.DealerId, out var dealerId);

			return dealerId;
		}
	}
}