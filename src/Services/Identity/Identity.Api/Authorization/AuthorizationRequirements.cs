﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using NexCore.Identity.Api.Authorization.Requirements;

namespace NexCore.Identity.Api.Authorization
{
    public static class AuthorizationRequirements
    {
        public static readonly IAuthorizationRequirement None = new AssertionRequirement(c => true);

        public static readonly IAuthorizationRequirement Administrator = new RolesAuthorizationRequirement(new[] { Roles.Administrator });

        public static readonly IAuthorizationRequirement DealerEmployeeIdentity = new DealerEmployeeIdentityRequirement();

        public static readonly IAuthorizationRequirement DistributorEmployeeIdentity = new DistributorEmployeeIdentityRequirement();
	}
}
