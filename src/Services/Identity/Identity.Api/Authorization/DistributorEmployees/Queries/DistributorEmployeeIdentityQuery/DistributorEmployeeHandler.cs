﻿using NexCore.DealerNetwork;
using NexCore.Identity.Api.Authorization.RequirementHandlers;
using NexCore.Identity.Api.Authorization.Requirements;

namespace NexCore.Identity.Api.Authorization.DistributorEmployees.Queries.DistributorEmployeeIdentityQuery
{
	public class DistributorEmployeeHandler : ClaimAuthorizationHandler<DistributorEmployeeIdentityRequirement, Application.UserIdentities.Queries.DistributorEmployees.DistributorEmployeeIdentityQuery>
	{
		public DistributorEmployeeHandler() : base(Claims.EmployeeDistributorId, GetClaimValue)
		{ }

		private static string GetClaimValue(Application.UserIdentities.Queries.DistributorEmployees.DistributorEmployeeIdentityQuery command)
		{
			DistributorId.TryParse(command.DistributorId, out var distributorId);

			return distributorId;
		}
	}
}