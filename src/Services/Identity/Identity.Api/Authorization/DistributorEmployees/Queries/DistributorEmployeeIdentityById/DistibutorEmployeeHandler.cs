﻿using NexCore.DealerNetwork;
using NexCore.Identity.Api.Authorization.RequirementHandlers;
using NexCore.Identity.Api.Authorization.Requirements;
using NexCore.Identity.Application.UserIdentities.Queries.DistributorEmployees;

namespace NexCore.Identity.Api.Authorization.DistributorEmployees.Queries.DistributorEmployeeIdentityById
{
	public class DistributorEmployeeHandler : ClaimAuthorizationHandler<DistributorEmployeeIdentityRequirement, DistributorEmployeeIdentityByIdQuery>
	{
		public DistributorEmployeeHandler(): base(Claims.EmployeeDistributorId, GetClaimValue)
		{ }

		private static string GetClaimValue(DistributorEmployeeIdentityByIdQuery command)
		{
			DistributorId.TryParse(command.DistributorId, out var distributorId);

			return distributorId;
		}
	}
}