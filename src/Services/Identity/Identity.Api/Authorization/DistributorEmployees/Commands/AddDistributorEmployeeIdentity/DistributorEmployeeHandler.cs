﻿using NexCore.DealerNetwork;
using NexCore.Identity.Api.Authorization.RequirementHandlers;
using NexCore.Identity.Api.Authorization.Requirements;
using NexCore.Identity.Application.UserIdentities.Commands.DistributorEmployees.AddDistributorEmployeeIdentity;

namespace NexCore.Identity.Api.Authorization.DistributorEmployees.Commands.AddDistributorEmployeeIdentity
{
	public class DistributorEmployeeHandler : ClaimAuthorizationHandler<DistributorEmployeeIdentityRequirement, AddDistributorEmployeeIdentityCommand>
	{
		public DistributorEmployeeHandler() : base(Claims.EmployeeDistributorId, GetClaimValue)
		{
		}

		private static string GetClaimValue(AddDistributorEmployeeIdentityCommand command)
		{
			DistributorId.TryParse(command.DistributorId, out var distributorId);

			return distributorId;
		}
	}
}