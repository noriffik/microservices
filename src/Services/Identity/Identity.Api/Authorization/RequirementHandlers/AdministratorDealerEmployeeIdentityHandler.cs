﻿using NexCore.Identity.Api.Authorization.Requirements;

namespace NexCore.Identity.Api.Authorization.RequirementHandlers
{
	public class AdministratorDealerEmployeeIdentityHandler : AdministratorHandler<DealerEmployeeIdentityRequirement>
	{
	}
}