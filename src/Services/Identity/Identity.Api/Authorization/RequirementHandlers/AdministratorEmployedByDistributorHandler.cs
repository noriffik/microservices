﻿using NexCore.Identity.Api.Authorization.Requirements;

namespace NexCore.Identity.Api.Authorization.RequirementHandlers
{
	public class AdministratorEmployedByDistributorHandler : AdministratorHandler<DistributorEmployeeIdentityRequirement>
	{
	}
}