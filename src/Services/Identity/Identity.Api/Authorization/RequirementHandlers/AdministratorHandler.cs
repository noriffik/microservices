﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace NexCore.Identity.Api.Authorization.RequirementHandlers
{
	public class AdministratorHandler<T> : AuthorizationHandler<T> where T : IAuthorizationRequirement
	{
		protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, T requirement)
		{
			if (context.User.IsInRole(Roles.Administrator))
				context.Succeed(requirement);

			return Task.CompletedTask;
		}
	}
}