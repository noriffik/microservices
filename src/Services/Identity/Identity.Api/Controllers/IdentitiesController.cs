﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using NexCore.Application.Queries;
using NexCore.Identity.Api.Authorization;
using NexCore.Identity.Application.UserIdentities.Commands.AddUserIdentity;
using NexCore.Identity.Application.UserIdentities.Commands.ResetUserIdentityFailedAccessCount;
using NexCore.Identity.Application.UserIdentities.Commands.UpdateUserIdentity;
using NexCore.Identity.Application.UserIdentities.Queries;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Net;
using System.Threading.Tasks;
using NexCore.Identity.Application.UserIdentities.Queries.UserIdentity;

namespace NexCore.Identity.Api.Controllers
{
    [Authorize]
	[Route("api/users")]
	public class IdentitiesController : MediatrController
    {
	    public IdentitiesController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
	    {
	    }

		[HttpGet]
		[Authorize(Roles = "Administrator")]
		[ProducesResponseType((int)HttpStatusCode.OK)]
		[Produces(typeof(PagedResponse<UserIdentityDto>))]
		public Task<IActionResult> Get(UserIdentityQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);
		
        [Route("{Id:guid}")]
		[HttpGet]
        [Authorize(Roles = "Administrator")]
		[ProducesResponseType((int)HttpStatusCode.OK)]
		[ProducesResponseType((int)HttpStatusCode.NotFound)]
		[ProducesResponseType((int)HttpStatusCode.BadRequest)]
		[Produces(typeof(UserIdentityDto))]
		public Task<IActionResult> Get([FromRoute] UserIdentityByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

		[HttpPost]
        [Authorize(Roles = "Administrator")]
		[ProducesResponseType((int)HttpStatusCode.OK)]
		[ProducesResponseType((int)HttpStatusCode.BadRequest)]
		public Task<IActionResult> Post([FromBody] AddUserIdentityCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None);

		[Route("{Id:guid}")]
		[HttpPut]
		[Authorize(Roles = "Administrator")]
		[ProducesResponseType((int) HttpStatusCode.OK)]
		[ProducesResponseType((int) HttpStatusCode.BadRequest)]
		public Task<IActionResult> Put([FromBody] UpdateUserIdentityCommand command) =>
			ExecuteCommand(command, AuthorizationRequirements.None);

		[Route("{Id:guid}/failed-access-count")]
		[HttpDelete]
		[Authorize(Roles = "Administrator")]
		[ProducesResponseType((int) HttpStatusCode.OK)]
		[ProducesResponseType((int) HttpStatusCode.BadRequest)]
		public Task<IActionResult> ResetFailedAccessCount(ResetUserIdentityFailedAccessCountCommand command) =>
            ExecuteCommand(command, AuthorizationRequirements.None);
    }
}