﻿using System.Collections.Generic;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Identity.Api.Authorization;
using NexCore.Identity.Application.UserIdentities.Commands.DistributorEmployees.AddDistributorEmployeeIdentity;
using NexCore.WebApi;
using System.Net;
using System.Threading.Tasks;
using NexCore.Application.Queries;
using NexCore.Identity.Application.UserIdentities.Queries.DistributorEmployees;
using NexCore.WebApi.Authorization;

namespace NexCore.Identity.Api.Controllers
{
    [Authorize]
	[Route("api/distributors/{DistributorId}")]
    public class DistributorEmployeeIdentityController : MediatrController
	{
		public DistributorEmployeeIdentityController(IMediator mediator, IAuthorizationService authorizationService) 
			: base(mediator, authorizationService)
		{
		}

		[Route("users/{Id}")]
		[HttpGet]
		[ProducesResponseType((int) HttpStatusCode.OK)]
		[ProducesResponseType((int) HttpStatusCode.NotFound)]
		[Produces(typeof(DistributorEmployeeIdentityDto))]
		public Task<IActionResult> Get(DistributorEmployeeIdentityByIdQuery query) =>
			ExecuteQuery(query, new QueryAuthorizationSettings(AuthorizationRequirements.DistributorEmployeeIdentity, AuthorizationQueryScope.Query));

		[Route("users")]
		[HttpGet]
		[ProducesResponseType((int) HttpStatusCode.OK)]
		[Produces(typeof(PagedResponse<DistributorEmployeeIdentityDto>))]
		public Task<IActionResult> Get(DistributorEmployeeIdentityQuery query) =>
			ExecuteQuery(query, new QueryAuthorizationSettings(AuthorizationRequirements.DistributorEmployeeIdentity, AuthorizationQueryScope.Query));

		[Route("users")]
        [HttpPost]
		[ProducesResponseType((int)HttpStatusCode.OK)]
		[ProducesResponseType((int)HttpStatusCode.BadRequest)]
		public Task<IActionResult> Post([FromBody] AddDistributorEmployeeIdentityCommand command) => 
			ExecuteCreateCommand(command,  AuthorizationRequirements.DistributorEmployeeIdentity,
                result => CreatedAtAction(nameof(Get), new {command.DistributorId, Id = result}, result));
	}
}