﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.Identity.Api.Authorization;
using NexCore.Identity.Application.UserIdentities.Commands.DealerEmployees.AddDealerEmployeeIdentity;
using NexCore.Identity.Application.UserIdentities.Queries.DealerEmployees;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.Identity.Api.Controllers
{
	[Authorize]
	[Route("api/distributors/{DistributorId}/dealers/{DealerId}")]
	public class DealerEmployeeIdentityController : MediatrController
	{
		public DealerEmployeeIdentityController(IMediator mediator, IAuthorizationService authorizationService) : base(mediator, authorizationService)
		{
		}
		
		[Route("users/{Id}")]
		[HttpGet]
		[ProducesResponseType((int)HttpStatusCode.OK)]
		[ProducesResponseType((int)HttpStatusCode.NotFound)]
		[Produces(typeof(DealerEmployeeIdentityDto))]
		public Task<IActionResult> Get(DealerEmployeeIdentityByIdQuery query) =>
			ExecuteQuery(query, new QueryAuthorizationSettings(AuthorizationRequirements.DealerEmployeeIdentity, AuthorizationQueryScope.Query));

		[Route("users")]
		[HttpGet]
		[ProducesResponseType((int)HttpStatusCode.OK)]
		[Produces(typeof(PagedResponse<DealerEmployeeIdentityDto>))]
		public Task<IActionResult> Get(DealerEmployeeIdentityQuery query) =>
			ExecuteQuery(query, new QueryAuthorizationSettings(AuthorizationRequirements.DealerEmployeeIdentity, AuthorizationQueryScope.Query));

		[Route("users")]
		[HttpPost]
		[ProducesResponseType((int)HttpStatusCode.OK)]
		[ProducesResponseType((int)HttpStatusCode.BadRequest)]
		public Task<IActionResult> Post([FromBody] AddDealerEmployeeIdentityCommand command) =>
			ExecuteCreateCommand(command, AuthorizationRequirements.DealerEmployeeIdentity,
				result => CreatedAtAction(nameof(Get), new { command.DistributorId, command.DealerId, Id = result }, result));

	}
}