﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Logging;
using NexCore.Identity.Api.Filters;
using NexCore.WebApi;

namespace NexCore.Identity.Api
{
    public class MvcOptionsSetup : MvcDefaultOptionsSetup
    {
        public MvcOptionsSetup(IHttpRequestStreamReaderFactory readerFactory) : base(readerFactory)
        {
        }

        public MvcOptionsSetup(IHttpRequestStreamReaderFactory readerFactory, ILoggerFactory loggerFactory) : base(readerFactory, loggerFactory)
        {
        }

        public override void Configure(MvcOptions options)
        {
            base.Configure(options);

            options.Filters.Clear();
            options.Filters.Add<IdentityOperationExceptionFilter>();
        }
    }
}
