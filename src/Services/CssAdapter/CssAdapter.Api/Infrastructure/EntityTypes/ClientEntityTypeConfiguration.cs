﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.CssAdapter.Api.Model;

namespace NexCore.CssAdapter.Api.Infrastructure.EntityTypes
{
    public class ClientEntityTypeConfiguration : IEntityTypeConfiguration<Client>
    {
        public void Configure(EntityTypeBuilder<Client> builder)
        {
            builder.ToTable(nameof(Client), ApplicationContext.Schema);

            builder.HasKey(m => m.ClientId);

            builder.Property(m => m.ClientId)
                .HasColumnName("client_id")
                .ValueGeneratedNever();

            builder.Property(m => m.Email).HasColumnName("email");
            builder.Property(m => m.Telephone).HasColumnName("phone");
            builder.Property(m => m.Firstname).HasColumnName("firstname");
            builder.Property(m => m.Lastname).HasColumnName("lastname");
            builder.Property(m => m.Middlename).HasColumnName("patronymic");
            builder.Property(m => m.DealerCode).HasColumnName("dealer_id");
            builder.Property(m => m.Gender).HasColumnName("gender");
            builder.Property(m => m.Birthday).HasColumnName("birthday");
        }
    }
}
