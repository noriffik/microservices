﻿using Microsoft.EntityFrameworkCore;
using NexCore.CssAdapter.Api.Infrastructure.EntityTypes;
using NexCore.CssAdapter.Api.Model;

namespace NexCore.CssAdapter.Api.Infrastructure
{
    public class ApplicationContext : DbContext
    {
        public const string Schema = "dbo";

        public DbSet<Client> Clients { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options){ }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new ClientEntityTypeConfiguration());
        }
    }
}
