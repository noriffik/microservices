﻿using System.Runtime.Serialization;

namespace NexCore.CssAdapter.Api.Application.Queries
{
    [DataContract]
    public class ClientQuery
    {
        [DataMember]
        public string DealerCode { get; set; }

        [DataMember]
        public string Telephone { get; set; }
    }
}
