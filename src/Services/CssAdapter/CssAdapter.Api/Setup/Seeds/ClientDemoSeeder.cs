﻿using Microsoft.EntityFrameworkCore;
using NexCore.CssAdapter.Api.Infrastructure;
using NexCore.CssAdapter.Api.Model;
using NexCore.Infrastructure.Seeds;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexCore.CssAdapter.Api.Setup.Seeds
{
    public class ClientDemoSeeder : IDbContextSeeder<ApplicationContext>
    {
        private readonly IEnumerable<Client> _clients = new[]
        {
            new Client
            {
                ClientId = 1,
                Email = "maxim.koostritsa@gmail.com",
                Telephone = "380503330203",
                Firstname = "Maxim",
                Lastname = "Kostritsa",
                Middlename = "Vitalievich",
                Gender = "m",
                DealerCode = "38001"
            },
            new Client
            {
                ClientId = 2,
                Email = "maxim.koostritsa@gmail.com",
                Telephone = "380503330203",
                Firstname = "Maxim",
                Lastname = "Kostritsa",
                Middlename = "Vitalievich",
                Gender = "m",
                DealerCode = "38003"
               
            },
            new Client
            {
                ClientId = 3,
                Email = "yuriy.goncharenko@gmail.com",
                Telephone = "380683542582",
                Firstname = "Yuriy",
                Lastname = "Goncharenko",
                Middlename = "Anatolievich",
                Gender = "m",
                DealerCode = "38001",
                Birthday = new DateTime(1984, 5, 2)
            },
            new Client
            {
                ClientId = 4,
                Email = "pavel.holubchenko@gmail.com",
                Telephone = "380992199324",
                Firstname = "Holubchenko",
                Lastname = "Pavel",
                Middlename = "Sergeevich",
                Gender = "Male",
                DealerCode = "38002"
            },
            new Client
            {
                ClientId = 5,
                Email = "anton.gurov@gmail.com",
                Telephone = "380977206334",
                Firstname = "Gurov",
                Lastname = "Anton",
                Middlename = "Vladimirovich",
                Gender = "m",
                DealerCode = "38003"
            }
        };

        private readonly ApplicationContext _context;

        public ClientDemoSeeder(ApplicationContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task Seed()
        {
            var isSeeded = await _context.Set<Client>().AnyAsync();
            if (isSeeded) return;
            
            await _context.AddRangeAsync(_clients);
            await _context.SaveChangesAsync();
        }
    }
}
