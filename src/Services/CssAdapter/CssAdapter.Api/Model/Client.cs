﻿using System;

namespace NexCore.CssAdapter.Api.Model
{
    public class Client
    {
        public int ClientId { get; set; }

        public string Email { get; set; }

        public string Telephone { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Middlename { get; set; }

        public string Gender { get; set; }

        public DateTime? Birthday { get; set; }

        public string DealerCode { get; set; }
    }
}
