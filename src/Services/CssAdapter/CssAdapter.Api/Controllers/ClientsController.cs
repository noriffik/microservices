﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NexCore.CssAdapter.Api.Application.Queries;
using NexCore.CssAdapter.Api.Infrastructure;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.CssAdapter.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ClientsController : Controller
    {
        private readonly ApplicationContext _context;

        public ClientsController(ApplicationContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        [Route("find")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Find([FromQuery] ClientQuery query)
        {
            if (query == null)
                return BadRequest();

            var result = await _context.Clients
                .Where(c => c.DealerCode == query.DealerCode)
                .Where(c => c.Telephone == query.Telephone)
                .FirstOrDefaultAsync();

            if (result == null)
                return NotFound();

            return Json(result);
        }
    }
}
