﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexCore.CssAdapter.Api.Extensions;
using NexCore.CssAdapter.Api.Infrastructure;
using NexCore.CssAdapter.Api.Setup.Seeds;
using NexCore.Infrastructure.Extensions;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Setup;
using NexCore.WebApi.Extensions;

namespace NexCore.CssAdapter.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Add(
                new ServiceDescriptor(
                    typeof(IDbContextSeeder<ApplicationContext>),
                    typeof(ClientDemoSeeder),
                    ServiceLifetime.Transient));

            services
                .AddDbContext<ApplicationContext>(Configuration["ConnectionString"], Configuration)
                .AddScoped<DatabaseSetupTask<ApplicationContext>>()
                .AddHealthChecks(Configuration)
                .AddSwagger(Configuration)
                .AddMvc();

            ConfigureAuthentication(services);
        }

        // This method is required by testing to change authentication options
        protected virtual void ConfigureAuthentication(IServiceCollection services)
        {
            services.AddJwtBearerAuthentication(Configuration, "NexCore.CssAdapter");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");

                c.OAuthClientId("NexCore.CssAdapter.Swagger");
            });

            app.UseMvc();
        }
	}
}
