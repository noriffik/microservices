﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using NexCore.CssAdapter.Api.Infrastructure;
using NexCore.Infrastructure.Setup;
using NexCore.WebApi.Extensions;
using System.Threading.Tasks;

namespace NexCore.CssAdapter.Api
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            await host.Setup<DatabaseSetupTask<ApplicationContext>>();
            
            await host.RunAsync();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseHealthChecks("/hc");
    }
}
