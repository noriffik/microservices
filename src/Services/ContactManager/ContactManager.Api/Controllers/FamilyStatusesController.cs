﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.ContactManager.Application.FamilyStatuses.Dtos;
using NexCore.ContactManager.Application.FamilyStatuses.Queries;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers
{
    [Authorize]
    [Route("api/family-statuses")]
    public class FamilyStatusesController : MediatrController
    {
        public FamilyStatusesController(IMediator mediator, IAuthorizationService authorizationService) 
            : base(mediator, authorizationService)
        {
        }

        [HttpGet]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<FamilyStatusDto>))]
        public Task<IActionResult> Get(FamilyStatusQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);
    }
}
