﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Authorization;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddCorporateCustomer;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers.Corporate;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers
{
    [Authorize]
    [Route("api/distributors/{DistributorId}/dealers/{DealerCode}/customers/corporate")]
    public class DealerCorporateCustomerController : MediatrController
    {
        public DealerCorporateCustomerController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [HttpGet]
        [Route("{CorporateCustomerId}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(CorporateCustomerDto))]
        public Task<IActionResult> Get([FromRoute] CorporateCustomerByIdQuery query) =>  ExecuteQuery(query,
            new QueryAuthorizationSettings(AuthorizationRequirements.EmployedByOrganization, AuthorizationQueryScope.Query));

        [HttpGet]
        [Produces(typeof(PagedResponse<CorporateCustomerDto>))]
        public Task<IActionResult> Get(CorporateCustomerQuery query) => ExecuteQuery(query,
            new QueryAuthorizationSettings(AuthorizationRequirements.EmployedByOrganization, AuthorizationQueryScope.Query));

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Post([FromBody]AddCorporateCustomerCommand command) =>
            ExecuteCreateCommand(command, AuthorizationRequirements.EmployedByOrganization,
            result => CreatedAtAction(nameof(Get), new
            {   
                command.DistributorId,
                command.DealerCode,
                CorporateCustomerId = result
            }, result));
    }
}
