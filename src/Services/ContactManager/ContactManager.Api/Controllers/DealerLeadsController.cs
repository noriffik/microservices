﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Authorization;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Leads.OpenLead;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Leads;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers
{
    [Authorize]
    [Route("api/distributors/{DistributorId}/dealers/{DealerCode}/leads")]
    public class DealerLeadsController : MediatrController
    {
        public DealerLeadsController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [HttpGet]
        [Route("{Id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(LeadDto))]
        public Task<IActionResult> Get([FromRoute] LeadByIdQuery query) => ExecuteQuery(query,
            new QueryAuthorizationSettings(AuthorizationRequirements.EmployedByOrganization, AuthorizationQueryScope.Query));

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(PagedResponse<LeadDto>))]
        public Task<IActionResult> Get(LeadQuery query) => ExecuteQuery(query,
            new QueryAuthorizationSettings(AuthorizationRequirements.EmployedByOrganization, AuthorizationQueryScope.Query));

        [HttpPost]
        [Authorize(Roles = "Administrator, DealerEmployee")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Post([FromBody] OpenLeadCommand command)
        {
            return ExecuteCreateCommand(command, AuthorizationRequirements.EmployedByOrganization,
                result => CreatedAtAction(nameof(Get), new
                {
                    command.DistributorId,
                    command.DealerCode,
                    Id = result
                }, result));
        }
    }
}
