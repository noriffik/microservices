﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Authorization;
using NexCore.ContactManager.Application.Individuals.Commands.AddIndividual;
using NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Individuals.Queries;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers
{
    [Authorize]
    [Route("api/contacts/individuals")]
    public class IndividualsController : MediatrController
    {
        public IndividualsController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{Id:int}")]
        [Authorize(Roles = "Administrator, DistributorEmployee, DealerEmployee")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(IndividualDto))]
        public Task<IActionResult> Get([FromRoute] IndividualByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Authorize(Roles = "Administrator, DistributorEmployee, DealerEmployee")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(PagedResponse<IndividualDto>))]
        public Task<IActionResult> Get([FromQuery] IndividualQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Authorize(Roles = "Administrator, DistributorEmployee, DealerEmployee")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Post([FromBody] AddIndividualCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None);

        [Route("{Id:int}")]
        [Authorize(Roles = "Administrator, DistributorEmployee, DealerEmployee")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Put([FromBody] UpdateIndividualCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);
    }
}