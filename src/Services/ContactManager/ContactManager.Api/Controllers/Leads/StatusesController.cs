﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.ContactManager.Application.Leads.Queries.Statuses;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers.Leads
{
    [Authorize]
    [Route("api/leads/statuses")]
    public class StatusesController : MediatrController
    {
        public StatusesController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [HttpGet]
        [Authorize(Roles = "Administrator, DistributorEmployee, DealerEmployee")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<StatusDto>))]
        public Task<IActionResult> Get() => ExecuteQuery(new StatusQuery(), QueryAuthorizationSettings.None);
    }
}
