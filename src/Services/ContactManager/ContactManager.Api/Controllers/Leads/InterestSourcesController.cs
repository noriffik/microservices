﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.ContactManager.Api.Authorization;
using NexCore.ContactManager.Application.Leads.Commands.InterestSources.AddInterestSource;
using NexCore.ContactManager.Application.Leads.Commands.InterestSources.RemoveInterestedSource;
using NexCore.ContactManager.Application.Leads.Commands.InterestSources.UpdateInterestSource;
using NexCore.ContactManager.Application.Leads.Queries.InterestSources;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers.Leads
{
    [Authorize]
    [Route("api/leads/interest-sources/")]
    public class InterestSourcesController : MediatrController
    {
        public InterestSourcesController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [HttpGet]
        [Route("{Id}")]
        [Authorize(Roles = "Administrator, DistributorEmployee, DealerEmployee")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(InterestSourceDto))]
        public Task<IActionResult> Get([FromRoute] InterestSourceByIdQuery query) =>
            ExecuteQuery(query, QueryAuthorizationSettings.None);

        [HttpGet]
        [Authorize(Roles = "Administrator, DistributorEmployee, DealerEmployee")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<InterestSourceDto>))]
        public Task<IActionResult> Get(InterestSourceQuery query) =>
            ExecuteQuery(query, QueryAuthorizationSettings.None);

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Post([FromBody]AddInterestSourceCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None);

        [HttpPut]
        [Authorize(Roles = "Administrator")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Put([FromBody] UpdateInterestSourceCommand command) =>
            ExecuteCommand(command, AuthorizationRequirements.None);

        [HttpDelete]
        [Route("{Id}")]
        [Authorize(Roles = "Administrator")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Delete([FromRoute] RemoveInterestSourceCommand command) =>
            ExecuteCommand(command, AuthorizationRequirements.None);
    }
}
