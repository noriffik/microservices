﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Authorization;
using NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.AddDisqualificationReason;
using NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.UpdateDisqualificationReason;
using NexCore.ContactManager.Application.Leads.Queries.DisqualificationReasons;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers.Leads
{
    [Authorize]
    [Route("api/leads/disqualification-reasons")]
    public class DisqualificationReasonsController : MediatrController
    {
        public DisqualificationReasonsController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [HttpGet]
        [Route("{Id}")]
        [Authorize(Roles = "Administrator, DistributorEmployee, DealerEmployee")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(DisqualificationReasonDto))]
        public Task<IActionResult> Get([FromRoute] DisqualificationReasonByIdQuery query) =>
            ExecuteQuery(query, QueryAuthorizationSettings.None);

        [HttpGet]
        [Authorize(Roles = "Administrator, DistributorEmployee, DealerEmployee")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(PagedResponse<DisqualificationReasonDto>))]
        public Task<IActionResult> Get(DisqualificationReasonQuery query) =>
            ExecuteQuery(query, QueryAuthorizationSettings.None);

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Post([FromBody] AddDisqualificationReasonCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None);

        [HttpPut]
        [Authorize(Roles = "Administrator")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Put([FromBody] UpdateDisqualificationReasonCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);
    }
}
