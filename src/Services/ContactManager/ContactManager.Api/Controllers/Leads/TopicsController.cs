﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Authorization;
using NexCore.ContactManager.Application.Leads.Commands.Topics.AddTopic;
using NexCore.ContactManager.Application.Leads.Commands.Topics.UpdateTopic;
using NexCore.ContactManager.Application.Leads.Queries.Topics;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers.Leads
{
    [Authorize]
    [Route("api/leads/topics")]
    public class TopicsController : MediatrController
    {
        public TopicsController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [HttpGet]
        [Authorize(Roles = "Administrator, DistributorEmployee, DealerEmployee")]
        [Route("{Id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(TopicDto))]
        public Task<IActionResult> Get([FromRoute] TopicByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [HttpGet]
        [Authorize(Roles = "Administrator, DistributorEmployee, DealerEmployee")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(PagedResponse<TopicDto>))]
        public Task<IActionResult> Get(TopicQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Post([FromBody] AddTopicCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None);

        [HttpPut]
        [Authorize(Roles = "Administrator")]
        [Route("{Id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Put([FromBody] UpdateTopicCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);
    }
}
