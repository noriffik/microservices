﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Authorization;
using NexCore.ContactManager.Application.Organizations.Commands;
using NexCore.ContactManager.Application.Organizations.Commands.AddOrganization;
using NexCore.ContactManager.Application.Organizations.Queries;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers
{
    [Authorize]
    [Route("api/contacts/organizations")]
    public class OrganizationsController : MediatrController
    {
        public OrganizationsController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Authorize(Roles = "Administrator, DistributorEmployee, DealerEmployee")]
        [Route("{Id:int}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(OrganizationDto))]
        public Task<IActionResult> Get([FromRoute] OrganizationByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Authorize(Roles = "Administrator, DistributorEmployee, DealerEmployee")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(PagedResponse<OrganizationDto>))]
        public Task<IActionResult> Get([FromQuery] OrganizationQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Authorize(Roles = "Administrator, DistributorEmployee, DealerEmployee")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Post([FromBody] AddOrganizationCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None);
       
        [Authorize(Roles = "Administrator, DistributorEmployee, DealerEmployee")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Put([FromBody] UpdateOrganizationCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);
    }
       
}
