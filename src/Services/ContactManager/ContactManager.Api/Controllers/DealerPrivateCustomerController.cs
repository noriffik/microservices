﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Authorization;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers
{
    [Authorize]
    [Route("api/distributors/{DistributorId}/dealers/{DealerCode}/customers/private")]
    public class DealerPrivateCustomerController : MediatrController
    {
        public DealerPrivateCustomerController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{PrivateCustomerId}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(PrivateCustomerDto), (int)HttpStatusCode.OK)]
        public Task<IActionResult> Get([FromRoute] PrivateCustomerByIdQuery query) => ExecuteQuery(query,
            new QueryAuthorizationSettings(AuthorizationRequirements.EmployedByOrganization, AuthorizationQueryScope.Query));

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(PagedResponse<PrivateCustomerDto>))]
        public Task<IActionResult> Get(PrivateCustomerQuery query) => ExecuteQuery(query,
            new QueryAuthorizationSettings(AuthorizationRequirements.EmployedByOrganization, AuthorizationQueryScope.Query));

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Post([FromBody]AddPrivateCustomerCommand command) =>
            ExecuteCreateCommand(command, AuthorizationRequirements.EmployedByOrganization,
                result => CreatedAtAction(nameof(Get), new
                {
                    command.DistributorId,
                    command.DealerCode,
                    PrivateCustomerId = result
                }, result));
    }
}
