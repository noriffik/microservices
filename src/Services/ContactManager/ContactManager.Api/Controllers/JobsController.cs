﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.ContactManager.Api.Authorization;
using NexCore.ContactManager.Application.Jobs.Commands.AddJob;
using NexCore.ContactManager.Application.Jobs.Queries;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers
{
    [Authorize]
    [Route("api/jobs")]
    public class JobsController : MediatrController
    {
        public JobsController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{Id:int}")]
        [HttpGet]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [Produces(typeof(JobDto))]
        public Task<IActionResult> Get([FromRoute] JobByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("create")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Create([FromBody] AddJobCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None);

        [Route("many")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<JobDto>))]
        public Task<IActionResult> List([FromRoute] JobQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);
    }
}
