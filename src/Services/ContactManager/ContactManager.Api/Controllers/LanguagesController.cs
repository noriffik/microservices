﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.ContactManager.Application.Languages.Queries;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers
{
    [Route("api/languages")]
    public class LanguagesController : MediatrController
    {
        public LanguagesController(IMediator mediator, IAuthorizationService authorizationService) : base(mediator, authorizationService)
        {
        }

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<LanguageDto>))]
        public Task<IActionResult> Get(LanguageQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);
    }
}
