﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Authorization;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.AddDealer;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.UpdateDealer;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers
{
    [Authorize]
    [Route("api/distributors/{DistributorId}/dealers/")]
    public class DealersController : MediatrController
    {
        public DealersController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{DealerCode}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(DealerDto))]
        public Task<IActionResult> Get([FromRoute] DealerByCodeQuery query) => ExecuteQuery(query,
            new QueryAuthorizationSettings(AuthorizationRequirements.EmployedByOrganization, AuthorizationQueryScope.Query));

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(PagedResponse<DealerDto>))]
        public Task<IActionResult> Get(DealerQuery query) => ExecuteQuery(query,
            new QueryAuthorizationSettings(AuthorizationRequirements.EmployedByOrganization, AuthorizationQueryScope.Query));

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Post([FromBody]AddDealerCommand command) =>
            ExecuteCreateCommand(command, AuthorizationRequirements.EmployedByOrganization,
                result => CreatedAtAction(nameof(Get), new { command.DistributorId, DealerCode = command.Code }, result));

        [Route("{Code}")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Put([FromBody]UpdateDealerCommand command) =>
            ExecuteCommand(command, AuthorizationRequirements.EmployedByOrganization);
    }
}
