﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Authorization;
using NexCore.ContactManager.Application.Distributors.Commands.AddDistributor;
using NexCore.ContactManager.Application.Distributors.Commands.UpdateDistributor;
using NexCore.ContactManager.Application.Distributors.Queries;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers
{
    [Authorize]
    [Route("api/distributors")]
    public class DistributorsController : MediatrController
    {
        public DistributorsController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{Id}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(DistributorDto))]
        public Task<IActionResult> Get([FromRoute] DistributorByIdQuery query) => ExecuteQuery(query,
            new QueryAuthorizationSettings(AuthorizationRequirements.EmployedByOrganization, AuthorizationQueryScope.Query));

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(PagedResponse<DistributorDto>))]
        public Task<IActionResult> Get([FromQuery] DistributorsQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Post([FromBody] AddDistributorCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None);

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Put([FromBody] UpdateDistributorCommand command) =>
            ExecuteCommand(command, AuthorizationRequirements.EmployedByOrganization);
    }
}
