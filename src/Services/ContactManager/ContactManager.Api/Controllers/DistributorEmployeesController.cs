﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Authorization;
using NexCore.ContactManager.Application.Distributors.Commands.Employees.AddEmployee;
using NexCore.ContactManager.Application.Distributors.Commands.Employees.UpdateEmployee;
using NexCore.ContactManager.Application.Distributors.Queries.Employees;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers
{
    [Authorize]
    [Route("api/distributors/{DistributorId}/employees/")]
    public class DistributorEmployeesController : MediatrController
    {
        public DistributorEmployeesController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{Id}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(EmployeeDto))]
        public Task<IActionResult> Get([FromRoute] EmployeeByIdQuery query) => ExecuteQuery(query,
            new QueryAuthorizationSettings(AuthorizationRequirements.EmployedByOrganization, AuthorizationQueryScope.Query));

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(PagedResponse<EmployeeDto>))]
        public Task<IActionResult> Get(EmployeeQuery query) => ExecuteQuery(query,
            new QueryAuthorizationSettings(AuthorizationRequirements.EmployedByOrganization, AuthorizationQueryScope.Query));

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Post([FromBody] AddEmployeeCommand command) => ExecuteCreateCommand(command,
            AuthorizationRequirements.EmployedByOrganization,
            result => CreatedAtAction(nameof(Get), new
            {
                command.DistributorId,
                Id = result
            }, result));

        [HttpPut]
        [Route("{Id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Put([FromBody]UpdateEmployeeCommand command) =>
            ExecuteCommand(command, AuthorizationRequirements.EmployedByOrganization);
    }
}
