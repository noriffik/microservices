﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.WebApi;
using System.Linq;
using System.Net;

namespace NexCore.ContactManager.Api.Controllers
{
    [Authorize]
    [Route("api/user")]
    public class UserController : MediatrController
    {
	    public UserController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
	    {
	    }

	    [Route("claims")]
	    [HttpGet]
	    [ProducesResponseType((int) HttpStatusCode.OK)]
	    [ProducesResponseType((int) HttpStatusCode.NotFound)]
	    [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public IActionResult GetClaims()
	    {
		    var claims = User.Identities
                .SelectMany(i => i.Claims)
                .Select(c => new { type = c.Type, value = c.Value })
                .ToList();

			return Ok(claims);
	    }
    }
}