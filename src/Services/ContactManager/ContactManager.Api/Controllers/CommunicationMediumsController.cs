﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.ContactManager.Application.Individuals.Queries;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Controllers
{
    [Route("api/communication-mediums/")]
    public class CommunicationMediumsController : MediatrController
    {
        public CommunicationMediumsController(IMediator mediator, IAuthorizationService authorizationService) 
            : base(mediator, authorizationService)
        {
        }

        [HttpGet]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<string>))]
        public Task<IActionResult> Get(CommunicationMediumQuery query) =>
            ExecuteQuery(query, QueryAuthorizationSettings.None);
    }
}
