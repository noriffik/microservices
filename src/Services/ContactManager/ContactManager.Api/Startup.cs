﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NexCore.Application.Extensions;
using NexCore.ContactManager.Api.Extensions;
using NexCore.ContactManager.Api.Setup.AutofacModules;
using NexCore.ContactManager.Api.Setup.Tasks;
using NexCore.ContactManager.Application.Organizations.Commands.AddOrganization;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Infrastructure;
using NexCore.Infrastructure.Extensions;
using NexCore.WebApi;
using NexCore.WebApi.AutofacModules;
using NexCore.WebApi.Extensions;
using System;

namespace NexCore.ContactManager.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var api = typeof(Startup).Assembly;
            var application = typeof(AddOrganizationCommand).Assembly;
            var infrastructure = typeof(ContactContext).Assembly;
            var domain = typeof(ContactInfo).Assembly;

            services.AddMvc()
                .AddControllersAsServices();

            services
                .AddTransient<ContactManagerDatabaseSetupTask>()
                .AddTransient<IntegrationEventLogDatabaseSetupTask>()
                .AddTransient<IConfigureOptions<MvcOptions>, MvcDefaultOptionsSetup>()
                .AddAutoMapper(api, application, infrastructure)
                .AddUnitOfWork<ContactContext>(Configuration["ConnectionString"], Configuration)
                .AddHealthChecks(Configuration)
                .AddSwagger(Configuration)
                .AddIntegrationServices(Configuration);

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            ConfigureAuthentication(services);

            var containerBuilder = services.GetAutofacContainerBuilder(application, domain, infrastructure);

            containerBuilder.RegisterModule(new AuthorizationFiltersModule(api, application));
            containerBuilder.RegisterModule(new ProvidersModule());
            containerBuilder.RegisterModule(new ApplicationModule());
            containerBuilder.RegisterModule(new SeederModule(Configuration));
            containerBuilder.RegisterModule(new EventBusModule());
            containerBuilder.RegisterModule(new IntegrationEventPublishersModule());
            containerBuilder.RegisterModule(new AuthorizationRequirementsModule(api));

            return new AutofacServiceProvider(containerBuilder.Build());
        }

        // This method is required by testing to change authentication options
        protected virtual void ConfigureAuthentication(IServiceCollection services)
        {
            services.AddJwtBearerAuthentication(Configuration, "NexCore.ContactManager");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");

                c.OAuthClientId("NexCore.ContactManager.Swagger");
            });

            app.UseMvc();
        }
    }
}
