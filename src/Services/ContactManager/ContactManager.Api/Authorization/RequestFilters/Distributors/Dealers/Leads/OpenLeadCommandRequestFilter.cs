﻿using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Leads.OpenLead;
using NexCore.WebApi.Authorization;
using System;
using System.Security.Claims;

namespace NexCore.ContactManager.Api.Authorization.RequestFilters.Distributors.Dealers.Leads
{
    public class OpenLeadCommandRequestFilter : RequestAuthorizationFilter<OpenLeadCommand, int>
    {
        public OpenLeadCommandRequestFilter(ICurrentUserProvider userProvider) : base(userProvider)
        {
        }

        protected override void Filter(OpenLeadCommand request, ClaimsPrincipal user)
        {
            if (request.OwnerEmployeeId != null)
                return;

            var employeeIdValue = user.FindFirst(Claims.EmployeeId)?.Value;
            if (employeeIdValue != null)
                request.OwnerEmployeeId = Convert.ToInt32(employeeIdValue);
        }
    }
}
