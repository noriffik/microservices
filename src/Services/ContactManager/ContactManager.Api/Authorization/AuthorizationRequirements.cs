﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using NexCore.ContactManager.Api.Authorization.Requirements;

namespace NexCore.ContactManager.Api.Authorization
{
    public static class AuthorizationRequirements
    {
        public static readonly IAuthorizationRequirement None = new AssertionRequirement(c => true);

        public static readonly IAuthorizationRequirement Administrator = new RolesAuthorizationRequirement(new[] {Roles.Administrator});

        public static readonly IAuthorizationRequirement EmployedByOrganization = new EmployedByOrganizationRequirement();
    }
}
