﻿namespace NexCore.ContactManager.Api.Authorization
{
    public static class Claims
    {
        public const string EmployeeId = "EmployeeId";
        public const string EmployeeDistributorId = "EmployeeDistributorId";
        public const string EmployeeDealerId = "EmployeeDealerId";
        public const string EmployeeDealerDistributorId = "EmployeeDealerDistributorId";
    }
}
