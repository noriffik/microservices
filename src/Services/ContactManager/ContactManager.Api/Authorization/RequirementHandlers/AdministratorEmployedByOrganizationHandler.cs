﻿using NexCore.ContactManager.Api.Authorization.Requirements;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers
{
    public class AdministratorEmployedByOrganizationHandler : AdministratorHandler<EmployedByOrganizationRequirement>
    {
    }
}