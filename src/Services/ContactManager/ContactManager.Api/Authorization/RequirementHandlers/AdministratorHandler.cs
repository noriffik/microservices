﻿using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers
{
    public class AdministratorHandler<T> : AuthorizationHandler<T> where T : IAuthorizationRequirement
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, T requirement)
        {
            if (context.User.IsInRole(Roles.Administrator))
                context.Succeed(requirement);

            return Task.CompletedTask;
        }
    }
}
