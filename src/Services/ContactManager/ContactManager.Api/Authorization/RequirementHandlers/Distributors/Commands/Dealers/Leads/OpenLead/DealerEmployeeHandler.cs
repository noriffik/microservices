﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Leads.OpenLead;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Commands.Dealers.Leads.OpenLead
{
    public class DealerEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, OpenLeadCommand>
    {
        public DealerEmployeeHandler() : base(Claims.EmployeeDealerId, GetClaimValue)
        {
        }

        private static string GetClaimValue(OpenLeadCommand command)
        {
            DealerId.TryParse(command.DistributorId, command.DealerCode, out var dealerId);

            return dealerId.Value;
        }
    }
}