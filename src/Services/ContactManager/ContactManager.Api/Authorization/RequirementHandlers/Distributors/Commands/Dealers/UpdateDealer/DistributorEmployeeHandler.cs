﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.UpdateDealer;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Commands.Dealers.UpdateDealer
{
    public class DistributorEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, UpdateDealerCommand>
    {
        public DistributorEmployeeHandler() : base(Claims.EmployeeDistributorId, r => r.DistributorId?.ToUpperInvariant())
        {
        }
    }
}