﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Commands.Employees.UpdateEmployee;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Commands.Employees.UpdateEmployee
{
    public class DistributorEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, UpdateEmployeeCommand>
    {
        public DistributorEmployeeHandler() : base(Claims.EmployeeDistributorId, GetClaimValue)
        {
        }

        private static string GetClaimValue(UpdateEmployeeCommand command)
        {
            DistributorId.TryParse(command.DistributorId, out var distributorId);

            return distributorId;
        }
    }
}