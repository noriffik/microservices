﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Commands.Dealers.Customers.AddPrivateCustomer
{
    public class DistributorEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, AddPrivateCustomerCommand>
    {
        public DistributorEmployeeHandler() : base(Claims.EmployeeDistributorId, r => r.DistributorId?.ToUpperInvariant())
        {
        }
    }
}