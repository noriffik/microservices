﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.UpdateEmployee;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Commands.Dealers.Employees.UpdateEmployee
{
    public class DistributorEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, UpdateEmployeeCommand>
    {
        public DistributorEmployeeHandler() : base(Claims.EmployeeDistributorId, r => r.DistributorId?.ToUpperInvariant())
        {
        }
    }
}