﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.UpdateDealer;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Commands.Dealers.UpdateDealer
{
    public class DealerEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, UpdateDealerCommand>
    {
        public DealerEmployeeHandler() : base(Claims.EmployeeDealerId, GetClaimValue)
        {
        }

        private static string GetClaimValue(UpdateDealerCommand command)
        {
            DealerId.TryParse(command.DistributorId, command.Code, out var dealerId);

            return dealerId;
        }
    }
}