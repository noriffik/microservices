﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Commands.UpdateDistributor;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Commands.UpdateDistributor
{
    public class DistributorEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, UpdateDistributorCommand>
    {
        public DistributorEmployeeHandler() : base(Claims.EmployeeDistributorId, r => r.DistributorId)
        {
        }
    }
}
