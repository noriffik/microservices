﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Commands.Employees.AddEmployee;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Commands.Employees.AddEmployee
{
    public class DistributorEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, AddEmployeeCommand>
    {
        public DistributorEmployeeHandler() : base(Claims.EmployeeDistributorId, GetClaimValue)
        {
        }

        private static string GetClaimValue(AddEmployeeCommand command)
        {
            DistributorId.TryParse(command.DistributorId, out var distributorId);

            return distributorId;
        }
    }
}