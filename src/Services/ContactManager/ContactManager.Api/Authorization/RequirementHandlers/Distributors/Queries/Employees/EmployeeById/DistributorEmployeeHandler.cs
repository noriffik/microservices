﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Queries.Employees;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Queries.Employees.EmployeeById
{
    public class DistributorEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, EmployeeByIdQuery>
    {
        public DistributorEmployeeHandler() : base(Claims.EmployeeDistributorId, GetClaimValue)
        {
        }

        private static string GetClaimValue(EmployeeByIdQuery command)
        {
            DistributorId.TryParse(command.DistributorId, out var distributorId);

            return distributorId;
        }
    }
}