﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Queries;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Queries.DistributorById
{
    public class DistributorEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, DistributorByIdQuery>
    {
        public DistributorEmployeeHandler() : base(Claims.EmployeeDistributorId, r => r.Id?.ToUpperInvariant())
        {
        }
    }
}