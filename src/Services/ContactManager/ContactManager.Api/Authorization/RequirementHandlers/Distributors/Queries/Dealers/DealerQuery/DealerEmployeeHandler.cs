﻿using NexCore.ContactManager.Api.Authorization.Requirements;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Queries.Dealers.DealerQuery
{
    public class DealerEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, Application.Distributors.Queries.Dealers.DealerQuery>
    {
        public DealerEmployeeHandler() : base(Claims.EmployeeDealerDistributorId, r => r.DistributorId?.ToUpperInvariant())
        {
        }
    }
}