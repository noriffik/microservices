﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers.Corporate;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Queries.Dealers.Customers.CorporateCustomer
{
    public class DealerEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, CorporateCustomerQuery>
    {
        public DealerEmployeeHandler() : base(Claims.EmployeeDealerId, GetClaimValue)
        {
        }

        private static string GetClaimValue(CorporateCustomerQuery command)
        {
            DealerId.TryParse(command.DistributorId, command.DealerCode, out var dealerId);

            return dealerId;
        }
    }
}