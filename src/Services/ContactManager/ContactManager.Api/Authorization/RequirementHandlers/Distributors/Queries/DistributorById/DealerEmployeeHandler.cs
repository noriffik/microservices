﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Queries;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Queries.DistributorById
{
    public class DealerEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, DistributorByIdQuery>
    {
        public DealerEmployeeHandler() : base(Claims.EmployeeDealerDistributorId, r => r.Id?.ToUpperInvariant())
        {
        }
    }
}