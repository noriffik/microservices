﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers.Corporate;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Queries.Dealers.Customers.CorporateCustomerById
{
    public class DealerEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, CorporateCustomerByIdQuery>
    {
        public DealerEmployeeHandler() : base(Claims.EmployeeDealerId, GetClaimValue)
        {
        }

        private static string GetClaimValue(CorporateCustomerByIdQuery command)
        {
            DealerId.TryParse(command.DistributorId, command.DealerCode, out var dealerId);

            return dealerId;
        }
    }
}