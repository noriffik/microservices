﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Employees;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Queries.Dealers.Employees.EmployeeById
{
    public class DealerEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, EmployeeByIdQuery>
    {
        public DealerEmployeeHandler() : base(Claims.EmployeeDealerId, GetClaimValue)
        {
        }

        private static string GetClaimValue(Application.Distributors.Queries.Dealers.Employees.EmployeeByIdQuery command)
        {
            DealerId.TryParse(command.DistributorId, command.DealerCode, out var dealerId);

            return dealerId;
        }
    }
}