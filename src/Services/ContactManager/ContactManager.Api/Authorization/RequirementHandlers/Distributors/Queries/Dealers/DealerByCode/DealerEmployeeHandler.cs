﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Queries.Dealers.DealerByCode
{
    public class DealerEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, DealerByCodeQuery>
    {
        public DealerEmployeeHandler() : base(Claims.EmployeeDealerDistributorId, r => r.DistributorId?.ToUpperInvariant())
        {
        }
    }
}