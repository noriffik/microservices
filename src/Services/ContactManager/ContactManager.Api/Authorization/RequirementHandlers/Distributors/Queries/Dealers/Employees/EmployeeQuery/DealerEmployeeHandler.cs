﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Queries.Dealers.Employees.EmployeeQuery
{
    public class DealerEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, Application.Distributors.Queries.Dealers.Employees.EmployeeQuery>
    {
        public DealerEmployeeHandler() : base(Claims.EmployeeDealerId, GetClaimValue)
        {
        }

        private static string GetClaimValue(Application.Distributors.Queries.Dealers.Employees.EmployeeQuery command)
        {
            DealerId.TryParse(command.DistributorId, command.DealerCode, out var dealerId);

            return dealerId;
        }
    }
}