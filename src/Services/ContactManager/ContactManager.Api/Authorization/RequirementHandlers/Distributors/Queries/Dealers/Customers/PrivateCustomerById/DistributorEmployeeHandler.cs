﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Queries.Dealers.Customers.PrivateCustomerById
{
    public class DistributorEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, PrivateCustomerByIdQuery>
    {
        public DistributorEmployeeHandler() : base(Claims.EmployeeDistributorId, r => r.DistributorId?.ToUpperInvariant())
        {
        }
    }
}