﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Queries.Employees.EmployeeQuery
{
    public class DistributorEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, Application.Distributors.Queries.Employees.EmployeeQuery>
    {
        public DistributorEmployeeHandler() : base(Claims.EmployeeDistributorId, GetClaimValue)
        {
        }

        private static string GetClaimValue(Application.Distributors.Queries.Employees.EmployeeQuery command)
        {
            DistributorId.TryParse(command.DistributorId, out var distributorId);

            return distributorId;
        }
    }
}

