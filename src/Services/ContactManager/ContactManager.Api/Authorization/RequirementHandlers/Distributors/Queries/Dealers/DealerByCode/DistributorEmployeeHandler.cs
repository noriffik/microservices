﻿using NexCore.ContactManager.Api.Authorization.Requirements;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Queries.Dealers.DealerByCode
{
    public class DistributorEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, Application.Distributors.Queries.Dealers.DealerByCodeQuery>
    {
        public DistributorEmployeeHandler() : base(Claims.EmployeeDistributorId, r => r.DistributorId?.ToUpperInvariant())
        {
        }
    }
}