﻿using NexCore.ContactManager.Api.Authorization.Requirements;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Employees;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers.Distributors.Queries.Dealers.Employees.EmployeeById
{
    public class DistributorEmployeeHandler : ClaimAuthorizationHandler<EmployedByOrganizationRequirement, EmployeeByIdQuery>
    {
        public DistributorEmployeeHandler() : base(Claims.EmployeeDistributorId, r => r.DistributorId?.ToUpperInvariant())
        {
        }
    }
}