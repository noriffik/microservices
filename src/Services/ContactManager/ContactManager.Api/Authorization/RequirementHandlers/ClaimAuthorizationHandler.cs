﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Authorization.RequirementHandlers
{
    public class ClaimAuthorizationHandler<TRequirement, TResource> : AuthorizationHandler<TRequirement, TResource> where TRequirement : IAuthorizationRequirement
    {
        private readonly string _claimType;
        private readonly Func<TResource, string> _claimValue;

        public ClaimAuthorizationHandler(string claimType, Func<TResource, string> claimValue)
        {
            _claimType = claimType ?? throw new ArgumentNullException(nameof(claimType));
            _claimValue = claimValue ?? throw new ArgumentNullException(nameof(claimValue));
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, TRequirement requirement, TResource resource)
        {
            var claimValue = _claimValue(resource);
            if (claimValue != null && context.User.HasClaim(_claimType, claimValue))
                context.Succeed(requirement);

            return Task.CompletedTask;
        }
    }
}
