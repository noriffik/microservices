﻿using Newtonsoft.Json;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.DealerNetwork;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.ContactManager.Api.Setup.Seeds
{
    public class DistributorJsonMapper : IMapper<DistributorRecord>
    {
        public IEnumerable<DistributorRecord> Map(string data)
        {
            var items = JsonConvert.DeserializeObject<List<DistributorJsonObject>>(data);

            return items.Select(i => new DistributorRecord
            {
                DistributorId = DistributorId.Parse(i.Id),
                Organization = new Organization(i.Name, Identity.WithEdrpou(i.Edrpou)),
                CountryCode = i.CountryCode
            });
        }
    }
}