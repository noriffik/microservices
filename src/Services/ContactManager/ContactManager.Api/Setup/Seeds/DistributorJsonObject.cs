﻿namespace NexCore.ContactManager.Api.Setup.Seeds
{
    public class DistributorJsonObject
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Edrpou { get; set; }

        public string CountryCode { get; set; }
    }
}