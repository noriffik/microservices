﻿using NexCore.ContactManager.Application.Distributors.Commands.AddDistributor;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Infrastructure;
using NexCore.EventBus.Abstract;
using NexCore.Infrastructure.Seeds;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Setup.Seeds
{
    public class DistributorSeeder : ISeeder<DistributorRecord>
    {
        private readonly ContactContext _context;
        private readonly ISeederDataSource<DistributorRecord> _dataSource;
        private readonly IIntegrationEventStorage _eventStorage;
        private readonly IEventBus _eventBus;

        private IDistributorRegisteredEventFactory _eventFactory;
        public IDistributorRegisteredEventFactory EventFactory
        {
            get => _eventFactory ?? (_eventFactory = new DistributorRegisteredEventFactory());
            set => _eventFactory = value ?? throw new ArgumentNullException(nameof(value));
        }

        public DistributorSeeder(ContactContext context, ISeederDataSource<DistributorRecord> dataSource, 
	        IIntegrationEventStorage eventStorage, IEventBus eventBus)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _dataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
            _eventStorage = eventStorage ?? throw new ArgumentNullException(nameof(eventStorage));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        public async Task Seed()
        {
            var records = _dataSource.Get()
                .Where(IsNotSeeded)
                .ToList();

            var events = await AddEvents(records);

            AddDistributors(records);

            await _context.Commit(CancellationToken.None);

            _eventBus.Publish(events);

            await _eventStorage.Clear();
        }

        private void AddDistributors(IEnumerable<DistributorRecord> records)
        {
            foreach (var record in records)
            {
                _context.Add(record.Organization);
                _context.Add(new Distributor(record.DistributorId, record.Organization.Id, record.CountryCode));
            }
        }

        private async Task<List<IntegrationEvent>> AddEvents(IEnumerable<DistributorRecord> records)
        {
            var events = records
                .Select(r => EventFactory.Create(r.DistributorId, r.Organization))
                .Cast<IntegrationEvent>()
                .ToList();

            await _eventStorage.AddRange(events);

            return events;
        }

        private bool IsNotSeeded(DistributorRecord record)
        {
            return !_context.Set<Distributor>()
                .Any(m => Convert.ToString(m.Id) == record.DistributorId.Value);
        }
    }
}
