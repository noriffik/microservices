﻿using System.Collections.Generic;

namespace NexCore.ContactManager.Api.Setup.Seeds
{
    public interface IMapper<out T> where T : class
    {
        IEnumerable<T> Map(string data);
    }
}