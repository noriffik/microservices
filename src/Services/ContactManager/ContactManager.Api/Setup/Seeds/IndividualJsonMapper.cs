﻿using Newtonsoft.Json;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.Legal;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace NexCore.ContactManager.Api.Setup.Seeds
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Local", Justification = "Required by Newtonsoft.Json")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    internal struct IndividualJsonObject
    {
        #pragma warning disable 649
        public string Firstname;
        public string Lastname;
        public string Middlename;
        public string Birthday;
        public string Gender;
        #pragma warning restore 649

        public Individual ToIndividual()
        {
            return new Individual(
                new PersonName(Firstname, Lastname, Middlename))
            {
                Birthday = DateTime.Parse(Birthday),
                Gender = MapToGender(Gender)
            };
        }

        private Gender? MapToGender(string gender)
        {
            switch (gender)
            {
                case "m":
                    return Domain.Individuals.Gender.Male;
                case "f":
                    return Domain.Individuals.Gender.Female;
                case null:
                    return null;
            }

            throw new InvalidOperationException("Specified gender is unsupported.");
        }
    }

    public class IndividualJsonMapper : IMapper<Individual>
    {
        public IEnumerable<Individual> Map(string data)
        {
            return JsonConvert.DeserializeObject<List<IndividualJsonObject>>(data)
                .Select(i => i.ToIndividual());
        }
    }
}
