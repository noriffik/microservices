﻿using Newtonsoft.Json;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.DealerNetwork;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Api.Setup.Seeds
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Local", Justification = "Required by Newtonsoft.Json")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    struct DealerJsonObject
    {
        #pragma warning disable 649
        public string _id;
        public string _name;
        public string _street;
        public string _city;
        public string _zip;
        public double _latitude;
        public double _longitude;
        #pragma warning restore 649

        public Organization ToOrganization(Identity identity)
        {
            var organization = new Organization(_name, identity);

            organization.Contact.Add(ToAddress());

            return organization;
        }

        private PhysicalAddress ToAddress()
        {
            return new PhysicalAddress(
                _street, null, _city, null, null, "UA", "Україна",
                _latitude, _longitude,
                _zip);
        }

        public Dealer ToDealer(int organizationId) => new Dealer(DealerId.Parse(_id), organizationId);
    }

    public class OrganizationJsonMapper : IMapper<Organization>
    {
        public IEnumerable<Organization> Map(string data)
        {
            var items = JsonConvert.DeserializeObject<List<DealerJsonObject>>(data);
            var organizations = new List<Organization>(items.Count);
            for (int i = 0; i < items.Count; i++)
                organizations.Add(items[i].ToOrganization(Identity.WithEdrpou((i + 1).ToString("D7"))));

            return organizations;
        }
    }
}
