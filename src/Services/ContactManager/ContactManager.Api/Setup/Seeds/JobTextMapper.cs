﻿using NexCore.ContactManager.Domain.Dictionaries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.ContactManager.Api.Setup.Seeds
{
    public class JobTextMapper : IMapper<Job>
    {
        public IEnumerable<Job> Map(string data)
        {
            var lines = data.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            return lines.Select(l => new Job(l));
        }
    }
}
