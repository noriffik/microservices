﻿using Newtonsoft.Json;
using NexCore.ContactManager.Domain.Dealers;
using System.Collections.Generic;

namespace NexCore.ContactManager.Api.Setup.Seeds
{
    public class DealerJsonMapper : IMapper<Dealer>
    {
        public IEnumerable<Dealer> Map(string data)
        {
            var items = JsonConvert.DeserializeObject<List<DealerJsonObject>>(data);
            var dealers = new List<Dealer>(items.Count);
            for (var i = 0; i < items.Count; i++)
                dealers.Add(items[i].ToDealer(i + 1));

            return dealers;
        }
    }
}