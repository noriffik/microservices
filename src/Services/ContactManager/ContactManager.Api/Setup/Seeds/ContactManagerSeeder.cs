﻿using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure;
using NexCore.Infrastructure.Seeds;

namespace NexCore.ContactManager.Api.Setup.Seeds
{
    public class ContactManagerSeeder : CompositeSeeder, IDbContextSeeder<ContactContext>
    {
        public ContactManagerSeeder(
            ISeeder<Job> jobSeeder,
            ISeeder<Organization> organizationSeeder,
            ISeeder<DistributorRecord> distributorSeeder,
            ISeeder<Dealer> dealerSeeder,
            ISeeder<Individual> individualSeeder,
            ISeeder<DealerPrivateCustomer> privateCustomerSeeder)
            : base(jobSeeder, organizationSeeder, distributorSeeder, dealerSeeder, individualSeeder, privateCustomerSeeder)
        {
        }
    }
}
