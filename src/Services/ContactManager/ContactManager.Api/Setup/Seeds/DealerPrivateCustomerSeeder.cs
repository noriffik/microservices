﻿using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Customers.Specifications;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using NexCore.Infrastructure.Seeds;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Setup.Seeds
{
    public class DealerPrivateCustomerSeeder : ISeeder<DealerPrivateCustomer>
    {
        private readonly DealerId _dealerId = DealerId.Parse("EKK38001");
        private const int CustomerIndividualId = 1;
        private const int AtOrganizationId = 5;

        private readonly Telephone _telephone = new Telephone("38", "095", "3488238", TelephoneType.Mobile);
        private readonly EmailAddress _email = new EmailAddress("igor.boyaryn@nexcore.ai");

        private readonly IUnitOfWork _work;
        private readonly IIntegrationEventStorage _eventStorage;
        private readonly IEventBus _eventBus;
        private readonly DealerPrivateCustomerAddedEventFactory _eventFactory;

        public DealerPrivateCustomerSeeder(IUnitOfWork work, IIntegrationEventStorage eventStorage,
            IEventBus eventBus, DealerPrivateCustomerAddedEventFactory eventFactory)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _eventStorage = eventStorage ?? throw new ArgumentNullException(nameof(eventStorage));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
            _eventFactory = eventFactory ?? throw new ArgumentNullException(nameof(eventFactory));
        }

        public async Task Seed()
        {
            await _work.EntityRepository.HasRequired<Organization>(AtOrganizationId);

            var isCustomerAlreadySpecification = new ByOrganizationAndContactableSpecification<DealerPrivateCustomer>(AtOrganizationId, CustomerIndividualId);

            if (await _work.EntityRepository.Has(isCustomerAlreadySpecification))
                return;

            var customer = await SeedCustomer();

            var individual = await _work.EntityRepository.Require<Individual>(CustomerIndividualId);

            SetContactInfo(individual);

            var @event = _eventFactory.Create(individual, customer);

            await _eventStorage.Add(@event);

            await _work.Commit(CancellationToken.None);

            _eventBus.Publish(@event);

            await _eventStorage.Clear();
        }

        private async Task<DealerPrivateCustomer> SeedCustomer()
        {
            var customer = new DealerPrivateCustomer(CustomerIndividualId, AtOrganizationId, _dealerId);

            customer.UpdateTelephone(_telephone);

            await _work.EntityRepository.Add(customer);

            return customer;
        }

        private void SetContactInfo(Individual individual)
        {
            var contact = individual.Contact;

            if (!contact.Has(_telephone))
                contact.Add(_telephone);

            if (!contact.Has(_email))
                contact.Add(_email);

            contact.SetPrimary(_telephone);
            contact.SetPrimary(_email);
        }
    }
}
