﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.AddDealer;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure;
using NexCore.EventBus.Abstract;
using NexCore.Infrastructure.Seeds;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Api.Setup.Seeds
{
    public class DealerSeeder : ISeeder<Dealer>
    {
        private readonly ContactContext _context;
        private readonly ISeederDataSource<Dealer> _dataSource;
        private readonly IMapper _mapper;
		private readonly IIntegrationEventStorage _eventStorage;
		private readonly IEventBus _eventBus;

		private IDealerAddedEventFactory _eventFactory;

        public IDealerAddedEventFactory EventFactory
        {
            get => _eventFactory ?? (_eventFactory = new DealerAddedEventFactory(_mapper));
            set => _eventFactory = value ?? throw new ArgumentNullException(nameof(value));
        }

        public DealerSeeder(ContactContext context, ISeederDataSource<Dealer> dataSource, IMapper mapper,
	        IIntegrationEventStorage eventStorage, IEventBus eventBus)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _dataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
			_eventStorage = eventStorage ?? throw new ArgumentNullException(nameof(eventStorage));
			_eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
		}

        public async Task Seed()
        {
            var records = _dataSource.Get()
                .Where(IsNotSeeded)
                .ToList();

            foreach (var dealer in records)
            {
	            _context.Add(dealer);
            }

			var organizations = await AddOrganizations(records);

            var events = await AddEvents(records, organizations);

            await _context.Commit(CancellationToken.None);
			
            _eventBus.Publish(events);

            await _eventStorage.Clear();
        }

        private async Task<IEnumerable<IntegrationEvent>> AddEvents(IEnumerable<Dealer> records, Dictionary<int, Organization> organizations)
        {
	        var events = records
		        .Select(d => EventFactory.Create(d, organizations[d.OrganizationId]))
		        .Cast<IntegrationEvent>()
		        .ToList();

	        await _eventStorage.AddRange(events);

	        return events;
        }

        private async Task<Dictionary<int, Organization>> AddOrganizations(List<Dealer> records)
        {
	        var organizationIds = records
		        .Select(d => d.OrganizationId)
		        .Distinct()
		        .ToList();

	        var organizations = (await _context.Organizations
			        .Where(o => organizationIds.Contains(o.Id))
			        .ToListAsync())
		        .ToDictionary(o => o.Id, o => o);
			
	        return organizations;
        }

        private bool IsNotSeeded(Dealer entity)
        {
            return !_context.Dealers
                .Any(m => Convert.ToString(m.Id) == entity.Id.Value);
        }
    }
}
