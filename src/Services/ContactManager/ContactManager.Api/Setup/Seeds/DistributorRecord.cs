﻿using NexCore.ContactManager.Domain.Organizations;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Api.Setup.Seeds
{
    public class DistributorRecord
    {
        public DistributorId DistributorId { get; set; }

        public Organization Organization { get; set; }

        public string CountryCode { get; set; }
    }
}
