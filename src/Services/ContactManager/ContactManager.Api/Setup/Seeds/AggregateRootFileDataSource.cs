﻿using NexCore.Infrastructure.Seeds;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NexCore.ContactManager.Api.Setup.Seeds
{
    public class AggregateRootFileDataSource<T> : ISeederDataSource<T>
        where T : class
    {
        private readonly string _path;
        private readonly IMapper<T> _mapper;

        public AggregateRootFileDataSource(string path, IMapper<T> mapper)
        {
            if (string.IsNullOrEmpty(path))
                throw new ArgumentException("Value cannot be null or empty.", nameof(path));

            _path = path;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public IEnumerable<T> Get()
        {
            return _mapper.Map(File.ReadAllText(_path)).ToArray();
        }
    }
}
