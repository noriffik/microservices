﻿using Autofac;
using MediatR.Pipeline;
using NexCore.Application.Processors;
using NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual;

namespace NexCore.ContactManager.Api.Setup.AutofacModules
{
    public class IntegrationEventPublishersModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<IndividualInfoUpdatedEventPublisher>()
                .As<IIndividualInfoUpdatedEventPublisher>();

            builder.RegisterType<DealerPrivateCustomerInfoUpdatedEventFactory>()
                .As<IDealerPrivateCustomerInfoUpdatedEventFactory>()
                .SingleInstance();

            builder.RegisterType<DealerEmployeeNameUpdatedEventFactory>()
                .As<IDealerEmployeeNameUpdatedEventFactory>()
                .SingleInstance();

            builder.RegisterGeneric(typeof(IntegrationEventPublishProcessor<,>))
                .As(typeof(IRequestPostProcessor<,>));
        }
    }
}
