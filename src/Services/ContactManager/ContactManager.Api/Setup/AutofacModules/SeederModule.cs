﻿using Autofac;
using Microsoft.Extensions.Configuration;
using NexCore.ContactManager.Api.Setup.Seeds;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure;
using NexCore.Infrastructure.Seeds;

namespace NexCore.ContactManager.Api.Setup.AutofacModules
{
    public class SeederModule : Module
    {
        private readonly IConfiguration _configuration;

        public SeederModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ContactManagerSeeder>()
                .As<IDbContextSeeder<ContactContext>>()
                .InstancePerLifetimeScope();

            RegisterJobSeeder(builder);
            RegisterOrganizationSeeder(builder);
            RegisterDealerSeeder(builder);
            RegisterIndividualSeeder(builder);
            RegisterPrivateCustomerSeeder(builder);
            RegisterDistributorSeeder(builder);
        }

        private void RegisterJobSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<EntitySeeder<ContactContext, Job>>()
                .As<ISeeder<Job>>();

            builder.Register(c => new AggregateRootFileDataSource<Job>(
                    _configuration["Setup:Data:JobDataPath"], new JobTextMapper()))
                .As<ISeederDataSource<Job>>()
                .InstancePerLifetimeScope();
        }

        private void RegisterOrganizationSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<EntitySeeder<ContactContext, Organization>>()
                .As<ISeeder<Organization>>();

            builder.Register(c => new AggregateRootFileDataSource<Organization>(
                    _configuration["Setup:Data:DealerDataPath"], new OrganizationJsonMapper()))
                .As<ISeederDataSource<Organization>>()
                .InstancePerLifetimeScope();
        }

        private void RegisterDealerSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<DealerSeeder>()
                .As<ISeeder<Dealer>>();

            builder.Register(c => new AggregateRootFileDataSource<Dealer>(
                    _configuration["Setup:Data:DealerDataPath"], new DealerJsonMapper()))
                .As<ISeederDataSource<Dealer>>()
                .InstancePerLifetimeScope();
        }

        private void RegisterIndividualSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<EntitySeeder<ContactContext, Individual>>()
                .As<ISeeder<Individual>>();

            builder.Register(c => new AggregateRootFileDataSource<Individual>(
                    _configuration["Setup:Data:IndividualDataPath"], new IndividualJsonMapper()))
                .As<ISeederDataSource<Individual>>()
                .InstancePerLifetimeScope();
        }

        private void RegisterPrivateCustomerSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<DealerPrivateCustomerSeeder>()
                .As<ISeeder<DealerPrivateCustomer>>()
                .InstancePerLifetimeScope();
        }

        private void RegisterDistributorSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<DistributorSeeder>()
                .As<ISeeder<DistributorRecord>>();

            builder.Register(c => new AggregateRootFileDataSource<DistributorRecord>(
                    _configuration["Setup:Data:DistributorsDataPath"], new DistributorJsonMapper()))
                .As<ISeederDataSource<DistributorRecord>>()
                .InstancePerLifetimeScope();
        }
    }
}
