﻿using Autofac;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddCorporateCustomer;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer;
using NexCore.ContactManager.Application.Individuals.Policies;
using NexCore.ContactManager.Domain.Organizations;

namespace NexCore.ContactManager.Api.Setup.AutofacModules
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            RegisterIndividuals(builder);
            RegisterOrganizations(builder);
            RegisterDealerPrivateCustomers(builder);
            RegisterDealerEmployees(builder);
            RegisterDistributorEmployees(builder);
            RegisterDealerCorporateCustomers(builder);
        }

        private static void RegisterIndividuals(ContainerBuilder builder)
        {
            builder.RegisterType<IgnoreIndividualDeduplicationPolicy>()
                .As<IIndividualDeduplicationRulePolicy>()
                .InstancePerLifetimeScope();
        }

        private static void RegisterOrganizations(ContainerBuilder builder)
        {
            builder.RegisterType<OrganizationRegistrationService>()
                .As<IOrganizationRegistrationService>();
        }

        private static void RegisterDealerPrivateCustomers(ContainerBuilder builder)
        {
            builder.RegisterType<PrivateCustomerFactory>()
                .InstancePerLifetimeScope();

            builder.RegisterType<DealerPrivateCustomerAddedEventFactory>()
                .InstancePerLifetimeScope();
        }

        private static void RegisterDealerCorporateCustomers(ContainerBuilder builder)
        {
            builder.RegisterType<DealerCorporateCustomerFactory>()
                .InstancePerLifetimeScope();

            builder.RegisterType<DealerCorporateCustomerAddedEventFactory>()
                .InstancePerLifetimeScope();
        }

        private static void RegisterDealerEmployees(ContainerBuilder builder)
        {
            builder.RegisterType<Application.Distributors.Commands.Dealers.Employees.AddEmployee.EmployeeFactory>()
                .InstancePerLifetimeScope();
        }

        private static void RegisterDistributorEmployees(ContainerBuilder builder)
        {
            builder.RegisterType<Application.Distributors.Commands.Employees.AddEmployee.EmployeeFactory>()
                .InstancePerLifetimeScope();
        }
    }
}
