﻿using Autofac;
using NexCore.ContactManager.Application.Providers;
using NexCore.ContactManager.Infrastructure.Providers;
using NexCore.ContactManager.Infrastructure.Providers.Leads;

namespace NexCore.ContactManager.Api.Setup.AutofacModules
{
    public class ProvidersModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CultureInfoGeographyDescriptionProvider>()
                .As<IGeographyDescriptionProvider>()
                .InstancePerLifetimeScope();

            builder.RegisterType<CultureInfoLanguageProvider>()
                .As<ILanguageProvider>()
                .InstancePerLifetimeScope();

            builder.RegisterType<StatusDescriptionProvider>()
                .As<IStatusDescriptionProvider>()
                .InstancePerLifetimeScope();
        }
    }
}
