﻿using Microsoft.Extensions.Configuration;
using NexCore.ContactManager.Infrastructure;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Setup;
using System;

namespace NexCore.ContactManager.Api.Setup.Tasks
{
    public class ContactManagerDatabaseSetupTask : DatabaseSetupTask<ContactContext>
    {
        private const string IsSeedingEnabledKey = "IsSeedingEnabled";

        public ContactManagerDatabaseSetupTask(ContactContext context) : base(context)
        {
        }

        public ContactManagerDatabaseSetupTask(ContactContext context, IDbContextSeeder<ContactContext> seeder,
            IConfiguration configuration) : base(context, seeder)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            
            IsSeedingEnabled = configuration.GetValue(IsSeedingEnabledKey, true);
        }
    }
}
