﻿using NexCore.ContactManager.Application.Providers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace NexCore.ContactManager.Infrastructure.Providers
{
    public class CultureInfoGeographyDescriptionProvider : IGeographyDescriptionProvider
    {
        private IEnumerable<RegionInfo> _regions;

        private IEnumerable<RegionInfo> Regions =>
            _regions ?? (_regions = CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                .Select(cultureInfo => new RegionInfo(cultureInfo.Name))
                .Distinct()
                .ToList());

        public static readonly IGeographyDescriptionProvider Instance = new CultureInfoGeographyDescriptionProvider();

        public string GetCountryNameByIso2(string code)
        {
            if (code == null)
                throw new ArgumentNullException(nameof(code));

            var region = Regions
                .SingleOrDefault(r => r.TwoLetterISORegionName.Equals(code, StringComparison.CurrentCultureIgnoreCase));

            return region?.EnglishName ?? throw new InvalidCountryCodeException($"{code} is invalid country code", code);
        }

        public bool IsValidIso2(string code)
        {
            if (code == null)
                throw new ArgumentNullException(nameof(code));

            return Regions
                .Any(r => r.TwoLetterISORegionName.Equals(code, StringComparison.CurrentCultureIgnoreCase));
        }
    }
}