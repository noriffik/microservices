﻿using NexCore.ContactManager.Application.Providers;
using NexCore.ContactManager.Domain.Languages;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.Providers
{
    public class CultureInfoLanguageProvider : ILanguageProvider
    {
        private static readonly CultureInfo[] CultureInfos = CultureInfo.GetCultures(CultureTypes.NeutralCultures);

        private readonly Lazy<ICollection<Language>> _languages =
            new Lazy<ICollection<Language>>(() => CultureInfos.Select(Language.From).ToList());

        public Task<Language> GetByIso2(string code)
        {
            if (code == null)
                throw new ArgumentNullException(nameof(code));

            var culture = CultureInfos.SingleOrDefault(l => l.ThreeLetterISOLanguageName == code);
            if (culture == null)
                throw new InvalidLanguageCodeException { LanguageCode = code };

            return Task.FromResult(Language.From(culture));
        }

        public Task<bool> IsValidIso2(string code)
        {
            if (code == null)
                throw new ArgumentNullException(nameof(code));

            return Task.FromResult(CultureInfos.Any(l => l.ThreeLetterISOLanguageName == code));
        }

        public Task<IEnumerable<Language>> Get()
        {
            return Task.FromResult(_languages.Value.AsEnumerable());
        }
    }
}