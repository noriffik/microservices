﻿using NexCore.ContactManager.Domain.Leads;
using System.Collections.Generic;

namespace NexCore.ContactManager.Infrastructure.Providers.Leads
{
    public interface IStatusDescriptionProvider
    {
        IEnumerable<StatusRecord> Get();
        bool Has(int id);
        StatusRecord SingleOrDefault(int id);
        StatusRecord Single(int id);
        bool Has(Status status);
        StatusRecord SingleOrDefault(Status status);
        StatusRecord Singe(Status status);
    }
}