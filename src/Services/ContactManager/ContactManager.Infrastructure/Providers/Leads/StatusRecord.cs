﻿namespace NexCore.ContactManager.Infrastructure.Providers.Leads
{
    public class StatusRecord
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
