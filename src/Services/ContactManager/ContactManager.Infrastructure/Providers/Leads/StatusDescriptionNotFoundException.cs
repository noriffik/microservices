﻿using System;

namespace NexCore.ContactManager.Infrastructure.Providers.Leads
{
    public class StatusDescriptionNotFoundException : Exception
    {
        private const string DefaultMessage = "Description of status was not found";
        private const string DefaultExtendedMessage = "Description of status with id {0} was not found";

        public int Id { get; }

        public StatusDescriptionNotFoundException() : base(DefaultMessage)
        {
        }

        public StatusDescriptionNotFoundException(string message) : base(message)
        {
        }

        public StatusDescriptionNotFoundException(string message, Exception inner) : base(message, inner)
        {
        }

        public StatusDescriptionNotFoundException(int id)
            : this(string.Format(DefaultExtendedMessage, id), id)
        {
        }

        public StatusDescriptionNotFoundException(string message, int id) : base(message)
        {
            Id = id;
        }

        public StatusDescriptionNotFoundException(string message, int id, Exception inner) : base(message, inner)
        {
            Id = id;
        }
    }
}
