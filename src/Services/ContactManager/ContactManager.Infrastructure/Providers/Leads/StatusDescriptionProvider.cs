﻿using CsvHelper;
using CsvHelper.Configuration;
using NexCore.ContactManager.Domain.Leads;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NexCore.ContactManager.Infrastructure.Providers.Leads
{
    public class StatusDescriptionProvider : IStatusDescriptionProvider
    {
        private const string FileName = "LeadStatuses";

        private readonly Lazy<IReadOnlyCollection<StatusRecord>> _statuses =
            new Lazy<IReadOnlyCollection<StatusRecord>>(Load);

        private static IReadOnlyCollection<StatusRecord> Load()
        {
            using (var reader = new StringReader(Properties.Resources.ResourceManager.GetString(FileName)))
            using (var csv = new CsvReader(reader, new Configuration { Delimiter = ";" }))
            {
                return csv.GetRecords<StatusRecord>().ToList();
            }
        }

        public IEnumerable<StatusRecord> Get() => _statuses.Value;

        public bool Has(int id) => Get().Any(c => c.Id == id);

        public StatusRecord SingleOrDefault(int id) => Get().SingleOrDefault(c => c.Id == id);

        public StatusRecord Single(int id) =>
            Get().SingleOrDefault(c => c.Id == id) ?? throw new StatusDescriptionNotFoundException(id);

        public bool Has(Status status) => Has((int)status);

        public StatusRecord SingleOrDefault(Status status) => SingleOrDefault((int)status);

        public StatusRecord Singe(Status status) => Single((int)status);
    }
}
