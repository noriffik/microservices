﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.DealerCorporateCustomers;
using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.DistributorEmployees;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure.EntityTypes;
using NexCore.ContactManager.Infrastructure.EntityTypes.Leads;
using NexCore.Domain;
using NexCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DuplicateCorporateCustomerException = NexCore.ContactManager.Infrastructure.Exceptions.DuplicateCorporateCustomerException;
using DuplicateEmploymentException = NexCore.ContactManager.Infrastructure.Exceptions.DuplicateEmploymentException;

namespace NexCore.ContactManager.Infrastructure
{
    public class ContactContext : UnitOfWorkDbContext
    {
        public const string Schema = "contacts";

        public DbSet<Individual> Individuals { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Dealer> Dealers { get; set; }
        public DbSet<DealerEmployee> DealerEmployees { get; set; }
        public DbSet<DistributorEmployee> DistributorEmployees { get; set; }
        public DbSet<DealerPrivateCustomer> DealerPrivateCustomers { get; set; }
        public DbSet<DealerCorporateCustomer> DealerCorporateCustomers { get; set; }
        public DbSet<Distributor> Distributors { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<DisqualificationReason> DisqualificationReasons { get; set; }
        public DbSet<Lead> Leads { get; set; }
        public DbSet<InterestSource> InterestSources { get; set; }

        public ContactContext(DbContextOptions<ContactContext> options) : base(options)
        {
            InitEventHandling();
        }

        public ContactContext(DbContextOptions<ContactContext> options, IMediator mediator)
            : base(options, mediator)
        {
            InitEventHandling();
        }

        public ContactContext(DbContextOptions<ContactContext> options, IMediator mediator, IEnumerable<IUnitOfWorkDbContextPlugin> plugins)
            : base(options, mediator, plugins)
        {
            InitEventHandling();
        }

        private void InitEventHandling()
        {
            ChangeTracker.StateChanged += OnStateChanged;
        }

        private void OnStateChanged(object sender, Microsoft.EntityFrameworkCore.ChangeTracking.EntityStateChangedEventArgs e)
        {
            if (e.NewState == EntityState.Deleted && e.Entry.Entity is IContactable contactable)
                Entry(contactable.Contact).State = EntityState.Deleted;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var contactPoint = new ContactPointEntityTypeConfiguration();

            modelBuilder.ApplyConfiguration<ContactPoint<PhysicalAddress>>(contactPoint);
            modelBuilder.ApplyConfiguration<ContactPoint<Telephone>>(contactPoint);
            modelBuilder.ApplyConfiguration<ContactPoint<EmailAddress>>(contactPoint);
            modelBuilder.ApplyConfiguration(new ContactInfoEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new IndividualEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrganizationEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new JobEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DealerEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DealerEmployeeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DealerPrivateCustomerEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DealerCorporateCustomerEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DistributorEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PersonalPreferencesTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CommunicationPreferencesEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DistributorEmployeeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TopicEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DisqualificationReasonTypeConfiguration());
            modelBuilder.ApplyConfiguration(new LeadEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new InterestSourceTypeConfiguration());
        }

        public override async Task Commit(CancellationToken cancellationToken = default)
        {
            try
            {
                await base.Commit(cancellationToken);
            }
            catch (WorkFailedException e)
            {
                ThrowIfDuplicateEmployment(e.InnerException);
                ThrowIfDuplicateCorporateCustomer(e.InnerException);

                throw;
            }
        }

        private static void ThrowIfDuplicateEmployment(Exception e)
        {
            if (e.InnerException != null && e.InnerException.Message.Contains("IX_Employee_OrganizationId_IndividualId_JobId"))
                throw new DuplicateEmploymentException("This job is taken by this employee for given organization.", e);
        }

        private static void ThrowIfDuplicateCorporateCustomer(Exception e)
        {
            if (e.InnerException != null && e.InnerException.Message.Contains("IX_CorporateCustomer_ContactableId_OrganizationId"))
                throw new DuplicateCorporateCustomerException("Given corporate customer is already belongs to given organization.", e);
        }
    }
}
