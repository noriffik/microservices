﻿using System.Linq;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.DealerCorporateCustomers
{
    public static class QueryableFilterExtensions
    {
        public static IQueryable<CorporateCustomerRecord> FilterByEdrpou(this IQueryable<CorporateCustomerRecord> query, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return query;

            return query.Where(r => r.Organization.Identity.Edrpou.StartsWith(value.Trim()));
        }

        public static IQueryable<CorporateCustomerRecord> FilterByIpn(this IQueryable<CorporateCustomerRecord> query,
            string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return query;

            return query.Where(r => r.Organization.Identity.Ipn.StartsWith(value.Trim()));
        }
    }
}
