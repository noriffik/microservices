﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Extensions;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers.Corporate;
using NexCore.DealerNetwork;
using NexCore.Infrastructure.QueryHandlers;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.DealerCorporateCustomers
{
    public class CorporateCustomerQueryHandler : IRequestHandler<CorporateCustomerByIdQuery, CorporateCustomerDto>,
        IRequestHandler<CorporateCustomerQuery, PagedResponse<CorporateCustomerDto>>
    {
        private readonly ContactContext _context;
        private readonly IMapper _mapper;

        private IQueryable<CorporateCustomerRecord> RecordSet =>
            from customer in _context.DealerCorporateCustomers
            join organization in _context.Organizations on customer.ContactableId equals organization.Id
            select new CorporateCustomerRecord
            {
                Customer = customer,
                Organization = organization
            };

        public CorporateCustomerQueryHandler(ContactContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<CorporateCustomerDto> Handle(CorporateCustomerByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (!DealerId.TryParse(request.DistributorId, request.DealerCode, out var dealerId))
                return null;

            var result = await RecordSet
                .Where(r => Convert.ToString(r.Customer.DealerId) == dealerId)
                .SingleOrDefaultAsync(r => r.Customer.Id == request.CorporateCustomerId, cancellationToken);

            return _mapper.Map<CorporateCustomerDto>(result);
        }

        public async Task<PagedResponse<CorporateCustomerDto>> Handle(CorporateCustomerQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (!DealerId.TryParse(request.DistributorId, request.DealerCode, out var dealerId))
                return new PagedResponse<CorporateCustomerDto>(PageOptions.From(request), 0);

            var result = await RecordSet
                .Where(r => Convert.ToString(r.Customer.DealerId) == dealerId.Value)
                .FilterByEdrpou(request.Edrpou)
                .FilterByIpn(request.Ipn)
                .OrderBy(r => r.Organization.Name)
                .Paginate(request, cancellationToken);

            return _mapper.MapPagedResponse<CorporateCustomerRecord, CorporateCustomerDto>(result);
        }
    }
}
