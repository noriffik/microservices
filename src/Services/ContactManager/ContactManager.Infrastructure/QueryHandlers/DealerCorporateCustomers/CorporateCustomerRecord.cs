﻿using NexCore.ContactManager.Domain.DealerCorporateCustomers;
using NexCore.ContactManager.Domain.Organizations;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.DealerCorporateCustomers
{
    public class CorporateCustomerRecord
    {
        public DealerCorporateCustomer Customer { get; set; }

        public Organization Organization { get; set; }
    }
}
