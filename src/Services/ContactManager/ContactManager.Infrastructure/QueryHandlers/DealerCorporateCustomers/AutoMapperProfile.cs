﻿using AutoMapper;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers.Corporate;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.DealerCorporateCustomers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CorporateCustomerRecord, CorporateCustomerDto>()
                .ForMember(d => d.Id, p => p.MapFrom(s => s.Customer.Id))
                .ForMember(d => d.DealerId, p => p.MapFrom(s => s.Customer.DealerId.Value));
        }
    }
}
