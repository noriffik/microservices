﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Extensions;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries.Employees;
using NexCore.DealerNetwork;
using NexCore.Infrastructure.QueryHandlers;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.DistributorEmployees
{
    public class EmployeeQueryHandler : IRequestHandler<EmployeeByIdQuery, EmployeeDto>, IRequestHandler<EmployeeQuery, PagedResponse<EmployeeDto>>
    {
        private readonly ContactContext _context;
        private readonly IMapper _mapper;

        private IQueryable<EmployeeRecord> RecordSet =>
            from employee in _context.DistributorEmployees
            join job in _context.Jobs on employee.JobId equals job.Id
            join individual in _context.Individuals on employee.IndividualId equals individual.Id
            select new EmployeeRecord
            {
                Id = employee.Id,
                DistributorId = employee.DistributorId,
                Job = job,
                Individual = individual
            };

        public EmployeeQueryHandler(ContactContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<EmployeeDto> Handle(EmployeeByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (!DistributorId.TryParse(request.DistributorId, out var distributorId))
                return null;

            var result = await RecordSet
                .Where(e => Convert.ToString(e.DistributorId) == distributorId.Value)
                .SingleOrDefaultAsync(e => e.Id == request.Id, cancellationToken);

            return _mapper.Map<EmployeeDto>(result);
        }

        public async Task<PagedResponse<EmployeeDto>> Handle(EmployeeQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (!DistributorId.TryParse(request.DistributorId, out var distributorId))
                return new PagedResponse<EmployeeDto>(PageOptions.From(request), 0);

            var result = await RecordSet
                .Where(e => Convert.ToString(e.DistributorId) == distributorId.Value)
                .SortBy(request.SortCriterion, request.SortDirection)
                .Paginate(request, cancellationToken);

            return _mapper.MapPagedResponse<EmployeeRecord, EmployeeDto>(result);
        }
    }
}
