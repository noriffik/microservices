﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries.Employees;
using System;
using System.Linq;
using System.Linq.Expressions;


namespace NexCore.ContactManager.Infrastructure.QueryHandlers.DistributorEmployees
{
    public static class QueryableSortExtensions
    {
        public static IQueryable<EmployeeRecord> SortBy(this IQueryable<EmployeeRecord> query,
            SortCriterion? sortCriterion, SortDirection? sortDirection)
        {
            switch (sortCriterion)
            {
                case SortCriterion.Firstname:
                    return query.InnerSort(r => r.Individual.Name.Firstname, sortDirection);

                case SortCriterion.Lastname:
                    return query.InnerSort(r => r.Individual.Name.Lastname, sortDirection);

                case SortCriterion.Middlename:
                    return query.InnerSort(r => r.Individual.Name.Middlename, sortDirection);

                case SortCriterion.JobTitle:
                    return query.InnerSort(r => r.Job.Title, sortDirection);

                default:
                    return query.InnerSort(r => r.Individual.Name.Lastname, sortDirection);
            }
        }

        private static IQueryable<EmployeeRecord> InnerSort(this IQueryable<EmployeeRecord> query,
            Expression<Func<EmployeeRecord, object>> keySelector, SortDirection? sortDirection)
        {
            return sortDirection == SortDirection.Descending
                ? query.OrderByDescending(keySelector)
                : query.OrderBy(keySelector);
        }
    }
}
