﻿using AutoMapper;
using MediatR;
using NexCore.ContactManager.Application.FamilyStatuses.Dtos;
using NexCore.ContactManager.Application.FamilyStatuses.Queries;
using NexCore.Legal;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.FamilyStatuses
{
    public class FamilyStatusQueryHandler : IRequestHandler<FamilyStatusQuery, IEnumerable<FamilyStatusDto>>
    {
        private readonly IMapper _mapper;

        public FamilyStatusQueryHandler(IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<FamilyStatusDto>> Handle(FamilyStatusQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return await Task.FromResult(_mapper.Map<IEnumerable<FamilyStatusDto>>(FamilyStatus.All));
        }
    }
}
