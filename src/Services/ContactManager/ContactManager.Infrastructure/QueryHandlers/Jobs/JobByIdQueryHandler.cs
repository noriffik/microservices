﻿using AutoMapper;
using MediatR;
using NexCore.ContactManager.Application.Jobs.Queries;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Jobs
{
    public class JobByIdQueryHandler : IRequestHandler<JobByIdQuery, JobDto>
    {
        private readonly ContactContext _context;
        private readonly IMapper _mapper;

        public JobByIdQueryHandler(ContactContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<JobDto> Handle(JobByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var record = await _context.Jobs.FindAsync(request.Id);

            return _mapper.Map<JobDto>(record);
        }
    }
}
