﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.ContactManager.Application.Jobs.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Jobs
{
    public class JobQueryHandler : IRequestHandler<JobQuery, IEnumerable<JobDto>>
    {
        private readonly ContactContext _context;
        private readonly IMapper _mapper;

        public JobQueryHandler(ContactContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<JobDto>> Handle(JobQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var records = await _context.Jobs
                .OrderBy(j => j.Title)
                .ToListAsync(cancellationToken);

            return _mapper.Map<IEnumerable<JobDto>>(records);
        }
    }
}
