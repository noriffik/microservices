﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Extensions;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Organizations.Queries;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.Infrastructure.QueryHandlers;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Organizations
{
    public class OrganizationQueryHandler : IRequestHandler<OrganizationByIdQuery, OrganizationDto>,
        IRequestHandler<OrganizationQuery, PagedResponse<OrganizationDto>>
    {
        private readonly ContactContext _context;
        private readonly IMapper _mapper;

        public OrganizationQueryHandler(ContactContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<OrganizationDto> Handle(OrganizationByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var record = await _context.Organizations.FindAsync(request.Id);

            return _mapper.Map<OrganizationDto>(record);
        }

        public async Task<PagedResponse<OrganizationDto>> Handle(OrganizationQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var query = _context.Organizations.AsQueryable();

            var organizations = await ByIpn(ByEdrpou(ByName(query, request.Name), request.Edrpou), request.Ipn)
                .OrderBy(o => o.Name)
                .Paginate(request, cancellationToken);

            return _mapper.MapPagedResponse<Organization, OrganizationDto>(organizations);
        }

        private static IQueryable<Organization> ByName(IQueryable<Organization> query, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return query;

            var sanitizedValue = value.ToLowerInvariant().Trim();

            return query.Where(o => o.Name.ToLowerInvariant().Contains(sanitizedValue));
        }

        private static IQueryable<Organization> ByEdrpou(IQueryable<Organization> query, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return query;

            var sanitizedValue = value.ToLowerInvariant().Trim();

            return query.Where(o => EF.Property<string>(o.Identity, nameof(o.Identity.Edrpou)).ToLowerInvariant().Contains(sanitizedValue));
        }

        private static IQueryable<Organization> ByIpn(IQueryable<Organization> query, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return query;

            var sanitizedValue = value.ToLowerInvariant().Trim();

            return query.Where(o => EF.Property<string>(o.Identity, nameof(o.Identity.Ipn)).ToLowerInvariant().Contains(sanitizedValue));
        }
    }
}
