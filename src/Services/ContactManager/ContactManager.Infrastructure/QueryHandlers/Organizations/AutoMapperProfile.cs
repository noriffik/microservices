﻿using AutoMapper;
using NexCore.ContactManager.Application.Organizations.Dtos;
using NexCore.ContactManager.Application.Organizations.Queries;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.Legal;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Organizations
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Organization, OrganizationDto>()
                .ForMember(d => d.Requisites, s => s.MapFrom(c => c.Requisites == OrganizationRequisites.Empty ? null : c.Requisites));

            CreateMap<Organization, OrganizationInfoDto>();

            CreateMap<OrganizationRequisites, OrganizationRequisitesDto>()
                .ForMember(d => d.ShortName, s => s.MapFrom(c => c.Name.ShortName))
                .ForMember(d => d.FullName, s => s.MapFrom(c => c.Name.FullName));
        }
    }
}
