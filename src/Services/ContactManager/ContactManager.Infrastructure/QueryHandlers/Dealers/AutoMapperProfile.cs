﻿using AutoMapper;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Dealers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<DealerRecord, DealerDto>()
                .ForMember(d => d.Id, p => p.MapFrom(s => s.Dealer.Id.Value))
                .ForMember(d => d.DistributorId, p => p.MapFrom(s => s.Dealer.DistributorId.Value))
                .ForMember(d => d.DealerCode, p => p.MapFrom(s => s.Dealer.DealerCode.Value))
                .ForMember(d => d.CityCode, p => p.MapFrom(s => s.Dealer.CityCode))
                .ForMember(d => d.Organization, p => p.MapFrom(s => s.Organization));
        }
    }
}
