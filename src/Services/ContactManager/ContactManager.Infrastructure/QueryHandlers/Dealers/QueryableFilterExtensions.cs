﻿using System.Linq;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Dealers
{
    public static class QueryableFilterExtensions
    {
        public static IQueryable<DealerRecord> FilterByEdrpou(this IQueryable<DealerRecord> query, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return query;

            var edrpou = value.Trim();

            return query.Where(r => r.Organization.Identity.Edrpou == edrpou);
        }

        public static IQueryable<DealerRecord> FilterByIpn(this IQueryable<DealerRecord> query, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return query;

            var ipn = value.Trim();

            return query.Where(r => r.Organization.Identity.Ipn == ipn);
        }
    }
}
