﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Extensions;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers;
using NexCore.DealerNetwork;
using NexCore.Infrastructure.QueryHandlers;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Dealers
{
    public class DealerQueryHandler : IRequestHandler<DealerQuery, PagedResponse<DealerDto>>,
        IRequestHandler<DealerByCodeQuery, DealerDto>
    {
        private readonly ContactContext _context;
        private readonly IMapper _mapper;

        private IQueryable<DealerRecord> RecordSet => from dealer in _context.Dealers
            join organization in _context.Organizations on dealer.OrganizationId equals organization.Id
            select new DealerRecord
            {
                Dealer = dealer,
                Organization = organization
            };
        
        public DealerQueryHandler(ContactContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<DealerDto> Handle(DealerByCodeQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var dealerId = DealerId.Parse(request.DistributorId, request.DealerCode);

            var result = await RecordSet
                .Where(record => Convert.ToString(record.Dealer.Id) == dealerId.Value)
                .SingleOrDefaultAsync(cancellationToken);

            return _mapper.Map<DealerDto>(result);
        }

        public async Task<PagedResponse<DealerDto>> Handle(DealerQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await RecordSet
                .Where(r => Convert.ToString(r.Dealer.DistributorId) == request.DistributorId)
                .SortBy(request.SortCriterion, request.SortDirection)
                .FilterByEdrpou(request.Edrpou)
                .FilterByIpn(request.Ipn)
                .Paginate(request, cancellationToken);

            return _mapper.MapPagedResponse<DealerRecord, DealerDto>(result);
        }
    }
}
