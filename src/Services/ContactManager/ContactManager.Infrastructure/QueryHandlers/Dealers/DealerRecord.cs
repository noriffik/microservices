﻿using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Organizations;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Dealers
{
    public class DealerRecord
    {
        public Dealer Dealer { get; set; }

        public Organization Organization { get; set; }
    }
}
