﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Dealers
{
    public static class QueryableSortExtensions
    {
        public static IQueryable<DealerRecord> SortBy(this IQueryable<DealerRecord> query, SortCriterion? sortCriterion, SortDirection? sortDirection)
        {
            switch (sortCriterion)
            {
                case SortCriterion.DealerCode:
                    return query.InnerSort(r => r.Dealer.DealerCode, sortDirection);

                case SortCriterion.OrganizationName:
                    return query.InnerSort(r => r.Organization.Name, sortDirection);

                default:
                    return query.InnerSort(r => r.Dealer.DealerCode, sortDirection);
            }
        }

        private static IQueryable<DealerRecord> InnerSort(this IQueryable<DealerRecord> query,
            Expression<Func<DealerRecord, object>> keySelector, SortDirection? sortDirection)
        {
            return sortDirection == SortDirection.Descending
                ? query.OrderByDescending(keySelector)
                : query.OrderBy(keySelector);
        }
    }
}
