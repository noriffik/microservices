﻿using AutoMapper;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Employees;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.DealerEmployees
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<EmployeeRecord, EmployeeDto>();
        }
    }
}
