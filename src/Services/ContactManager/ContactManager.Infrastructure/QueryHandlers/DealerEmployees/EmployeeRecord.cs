﻿using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.DealerEmployees
{
    public class EmployeeRecord
    {
        public int Id { get; set; }

        public DealerId DealerId { get; set; }

        public Job Job { get; set; }

        public Individual Individual { get; set; }
    }
}
