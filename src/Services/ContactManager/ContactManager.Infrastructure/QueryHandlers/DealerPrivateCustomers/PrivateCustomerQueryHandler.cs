﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Extensions;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers;
using NexCore.DealerNetwork;
using NexCore.Infrastructure.QueryHandlers;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.DealerPrivateCustomers
{
    public class PrivateCustomerQueryHandler : IRequestHandler<PrivateCustomerByIdQuery, PrivateCustomerDto>,
        IRequestHandler<PrivateCustomerQuery, PagedResponse<PrivateCustomerDto>>
    {
        private readonly ContactContext _context;
        private readonly IMapper _mapper;

        private IQueryable<PrivateCustomerRecord> RecordSet =>
            from customer in _context.DealerPrivateCustomers
            join individual in _context.Individuals on customer.ContactableId equals individual.Id
            select new PrivateCustomerRecord
            {
                PrivateCustomer = customer,
                Individual = individual
            };

        public PrivateCustomerQueryHandler(ContactContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<PrivateCustomerDto> Handle(PrivateCustomerByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (!DealerId.TryParse(request.DistributorId, request.DealerCode, out var dealerId))
                return null;

            var result = await RecordSet
                .Where(r => Convert.ToString(r.PrivateCustomer.DealerId) == dealerId)
                .SingleOrDefaultAsync(r => r.PrivateCustomer.Id == request.PrivateCustomerId, cancellationToken);

            return _mapper.Map<PrivateCustomerDto>(result);
        }

        public async Task<PagedResponse<PrivateCustomerDto>> Handle(PrivateCustomerQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (!DealerId.TryParse(request.DistributorId, request.DealerCode, out var dealerId))
                return new PagedResponse<PrivateCustomerDto>(PageOptions.From(request), 0);

            var result = await RecordSet
                .Where(r => Convert.ToString(r.PrivateCustomer.DealerId) == dealerId.Value)
                .SortBy(request.SortCriterion, request.SortDirection)
                .FilterByTelephone(request.Telephone)
                .FilterByName(request.Name)
                .Paginate(request, cancellationToken);

            return _mapper.MapPagedResponse<PrivateCustomerRecord, PrivateCustomerDto>(result);
        }
    }
}
