﻿using AutoMapper;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.DealerPrivateCustomers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<PrivateCustomerRecord, PrivateCustomerDto>()
                .ForMember(d => d.Id, p => p.MapFrom(s => s.PrivateCustomer.Id))
                .ForMember(d => d.DealerId, p => p.MapFrom(s => s.PrivateCustomer.DealerId.Value));
        }
    }
}
