﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.DealerPrivateCustomers
{
    public static class QueryableSortExtensions
    {
        public static IQueryable<PrivateCustomerRecord> SortBy(this IQueryable<PrivateCustomerRecord> query,
            SortCriterion? sortCriterion, SortDirection? sortDirection)
        {
            switch (sortCriterion)
            {
                case SortCriterion.Firstname:
                    return query.InnerSort(r => r.Individual.Name.Firstname, sortDirection);

                case SortCriterion.Lastname:
                    return query.InnerSort(r => r.Individual.Name.Lastname, sortDirection);

                case SortCriterion.Middlename:
                    return query.InnerSort(r => r.Individual.Name.Middlename, sortDirection);

                default:
                    return query.InnerSort(r => r.Individual.Name.Lastname, sortDirection);
            }
        }

        private static IQueryable<PrivateCustomerRecord> InnerSort(this IQueryable<PrivateCustomerRecord> query,
            Expression<Func<PrivateCustomerRecord, object>> keySelector, SortDirection? sortDirection)
        {
            return sortDirection == SortDirection.Descending
                ? query.OrderByDescending(keySelector)
                : query.OrderBy(keySelector);
        }
    }
}
