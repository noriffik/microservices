﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.DealerPrivateCustomers
{
    public static class QueryableFilterExtensions
    {
        public static IQueryable<PrivateCustomerRecord> FilterByTelephone(this IQueryable<PrivateCustomerRecord> query,
            string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return query;

            var telephone = Regex.Replace(value, "[^0-9]", "");

            if (telephone.Length < 3)
                return query;

            return query.Where(r => r.PrivateCustomer.Telephone.Number.StartsWith(telephone));
        }

        public static IQueryable<PrivateCustomerRecord> FilterByName(this IQueryable<PrivateCustomerRecord> query,
            string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return query;

            var name = value.Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Aggregate((a, b) => $"{a} {b}");

            return query.Where(r => EF.Property<string>(r.Individual.Name, "FullLastNameFirst").Contains(name)
                                    || EF.Property<string>(r.Individual.Name, "FullFirstNameFirst").Contains(name));
        }
    }
}
