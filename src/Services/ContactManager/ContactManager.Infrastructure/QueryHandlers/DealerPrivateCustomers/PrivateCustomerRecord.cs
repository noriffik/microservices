﻿using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.Individuals;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.DealerPrivateCustomers
{
    public class PrivateCustomerRecord
    {
        public DealerPrivateCustomer PrivateCustomer { get; set; }

        public Individual Individual { get; set; }
    }
}
