﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Leads;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Leads
{
    public static class QueryableSortExtensions
    {
        public static IQueryable<LeadRecord> SortBy(this IQueryable<LeadRecord> query,
            SortCriterion? sortCriterion, SortDirection? sortDirection)
        {
            switch (sortCriterion)
            {
                case SortCriterion.IndividualContactFirstname:
                    return query.InnerSort(r => r.Individual.Name.Firstname, sortDirection);

                case SortCriterion.IndividualContactLastname:
                    return query.InnerSort(r => r.Individual.Name.Lastname, sortDirection);

                case SortCriterion.IndividualContactMiddlename:
                    return query.InnerSort(r => r.Individual.Name.Middlename, sortDirection);

                case SortCriterion.ContactOrganizationName:
                    return query.InnerSort(r => r.Organization == null ? null : r.Organization.Name, sortDirection);

                default:
                    return query.InnerSort(r => r.Individual.Name.Lastname, sortDirection);
            }
        }

        private static IQueryable<LeadRecord> InnerSort(this IQueryable<LeadRecord> query,
            Expression<Func<LeadRecord, object>> keySelector, SortDirection? sortDirection)
        {
            return sortDirection == SortDirection.Descending
                ? query.OrderByDescending(keySelector)
                : query.OrderBy(keySelector);
        }
    }
}
