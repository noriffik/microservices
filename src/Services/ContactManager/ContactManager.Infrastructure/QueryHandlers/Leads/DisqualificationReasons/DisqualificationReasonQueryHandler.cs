﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Extensions;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Leads.Queries.DisqualificationReasons;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Infrastructure.QueryHandlers;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Leads.DisqualificationReasons
{
    public class DisqualificationReasonQueryHandler : IRequestHandler<DisqualificationReasonByIdQuery, DisqualificationReasonDto>,
        IRequestHandler<DisqualificationReasonQuery, PagedResponse<DisqualificationReasonDto>>
    {
        private readonly ContactContext _context;
        private readonly IMapper _mapper;

        public DisqualificationReasonQueryHandler(ContactContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<DisqualificationReasonDto> Handle(DisqualificationReasonByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var reason = await _context.DisqualificationReasons.SingleOrDefaultAsync(r => r.Id == request.Id, cancellationToken);

            return _mapper.Map<DisqualificationReasonDto>(reason);
        }

        public async Task<PagedResponse<DisqualificationReasonDto>> Handle(DisqualificationReasonQuery request, CancellationToken cancellationToken)
        {
            if (request == null) 
                throw new ArgumentNullException(nameof(request));

            var reasons = await _context.DisqualificationReasons
                .FilterByName(request.Name)
                .OrderBy(r => r.Name)
                .Paginate(request, cancellationToken);

            return _mapper.MapPagedResponse<DisqualificationReason, DisqualificationReasonDto>(reasons);
        }
    }
}
