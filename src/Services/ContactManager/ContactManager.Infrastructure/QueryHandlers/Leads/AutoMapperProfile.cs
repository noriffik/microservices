﻿using AutoMapper;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Leads;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Leads
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<EmployeeOwnerRecord, EmployeeOwnerDto>()
                .ForMember(d => d.EmployeeId, p => p.MapFrom(s => s.Employee.Id))
                .ForMember(d => d.EmployeeIndividualId, p => p.MapFrom(s => s.Employee.IndividualId))
                .ForMember(d => d.EmployeeName, p => p.MapFrom(s => s.Individual.Name))
                .ForMember(d => d.DealerId, p => p.MapFrom(s => s.Employee.DealerId))
                .ForMember(d => d.DealerOrganizationId, p => p.MapFrom(s => s.Organization.Id))
                .ForMember(d => d.DealerName, p => p.MapFrom(s => s.Organization.Name));

            CreateMap<LeadRecord, LeadDto>()
                .ForMember(d => d.Id, p => p.MapFrom(s => s.Lead.Id))
                .ForMember(d => d.DealerId, p => p.MapFrom(s => s.Lead.DealerId))
                .ForMember(d => d.Topic, p => p.MapFrom(s => s.Topic))
                .ForMember(d => d.ContactType, p => p.MapFrom(s => s.Lead.Contact.Type))
                .ForMember(d => d.Individual, p => p.MapFrom(s => s.Individual))
                .ForMember(d => d.Organization, p => p.MapFrom(s => s.Organization))
                .ForMember(d => d.EmployeeOwner, p => p.MapFrom(s => s.EmployeeOwner))
                .ForMember(d => d.Status, p => p.MapFrom(s => s.Lead.Status))
                .ForMember(d => d.CreatedAt, p => p.MapFrom(s => s.Lead.CreatedAt))
                .ForMember(d => d.UpdatedAt, p => p.MapFrom(s => s.Lead.UpdatedAt));
        }
    }
}
