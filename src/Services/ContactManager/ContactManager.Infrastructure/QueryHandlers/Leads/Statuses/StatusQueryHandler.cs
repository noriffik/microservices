﻿using AutoMapper;
using MediatR;
using NexCore.ContactManager.Application.Leads.Queries.Statuses;
using NexCore.ContactManager.Infrastructure.Providers.Leads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Leads.Statuses
{
    public class StatusQueryHandler : IRequestHandler<StatusQuery, IEnumerable<StatusDto>>
    {
        private readonly IStatusDescriptionProvider _provider;
        private readonly IMapper _mapper;

        public StatusQueryHandler(IStatusDescriptionProvider provider, IMapper mapper)
        {
            _provider = provider ?? throw new ArgumentNullException(nameof(provider));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Task<IEnumerable<StatusDto>> Handle(StatusQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var records = _provider
                .Get()
                .OrderBy(r => r.Name);

            var result = _mapper.Map<IEnumerable<StatusDto>>(records);

            return Task.FromResult(result);
        }
    }
}
