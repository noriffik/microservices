﻿using Microsoft.EntityFrameworkCore;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Leads;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Leads
{
    public static class QueryableFilterExtensions
    {
        public static IQueryable<LeadRecord> FilterByIndividualName(this IQueryable<LeadRecord> query,
            string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return query;

            var name = value.Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Aggregate((a, b) => $"{a} {b}");

            return query.Where(r => EF.Property<string>(r.Individual.Name, "FullLastNameFirst").Contains(name)
                                    || EF.Property<string>(r.Individual.Name, "FullFirstNameFirst").Contains(name));
        }

        public static IQueryable<LeadRecord> FilterByOrganizationName(this IQueryable<LeadRecord> query,
            string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return query;

            var name = value.Trim();

            return query.Where(r => r.Organization != null && r.Organization.Name.Contains(name));
        }

        public static IQueryable<LeadRecord> FilterByIndividualTelephone(this IQueryable<LeadRecord> query,
            string value, IQueryable<ContactPoint<Telephone>> telephones)
        {
            if (string.IsNullOrWhiteSpace(value))
                return query;

            var telephoneValue = Regex.Replace(value, "[^0-9]", "");

            if (telephoneValue.Length < 3)
                return query;

            return from record in query
                   join telephone in GetMatchingTelephones(telephones, telephoneValue)
                       on record.Individual.Contact.Id equals EF.Property<int>(telephone, "ContactInfoId")
                   select record;
        }

        private static IQueryable<ContactPoint<Telephone>> GetMatchingTelephones(
            IQueryable<ContactPoint<Telephone>> telephones, string telephoneValue)
        {
            return telephones
                .Where(telephone => telephone.IsPrimary)
                .Where(telephone => telephone.Value.Number.StartsWith(telephoneValue));
        }

        public static IQueryable<LeadRecord> FilterByOrganizationTelephone(this IQueryable<LeadRecord> query,
            string value, IQueryable<ContactPoint<Telephone>> telephones)
        {
            if (string.IsNullOrWhiteSpace(value))
                return query;

            var telephoneValue = Regex.Replace(value, "[^0-9]", "");

            if (telephoneValue.Length < 3)
                return query;

            return from record in query
                   join telephone in GetMatchingTelephones(telephones, telephoneValue)
                       on record.Organization.Contact.Id equals EF.Property<int>(telephone, "ContactInfoId")
                   select record;
        }

        public static IQueryable<LeadRecord> FilterByEmployeeOwnerId(this IQueryable<LeadRecord> query, int? id)
        {
            return id.HasValue
                ? query.Where(r => r.Lead.OwnerEmployeeId == id.Value)
                : query;
        }

        public static IQueryable<LeadRecord> FilterByStatus(this IQueryable<LeadRecord> query, int? status)
        {
            if (status.HasValue && Enum.IsDefined(typeof(Status), status))
                return query.Where(r => r.Lead.Status == (Status)status.Value);

            return query;
        }
    }
}
