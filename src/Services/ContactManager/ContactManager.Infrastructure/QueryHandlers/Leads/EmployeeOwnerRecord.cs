﻿using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Organizations;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Leads
{
    public class EmployeeOwnerRecord
    {
        public Individual Individual { get; set; }

        public DealerEmployee Employee { get; set; }

        public Organization Organization { get; set; }
    }
}