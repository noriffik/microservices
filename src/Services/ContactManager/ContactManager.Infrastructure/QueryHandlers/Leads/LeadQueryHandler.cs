﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Extensions;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Leads;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.DealerNetwork;
using NexCore.Infrastructure.QueryHandlers;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Leads
{
    public class LeadQueryHandler : IRequestHandler<LeadByIdQuery, LeadDto>,
        IRequestHandler<LeadQuery, PagedResponse<LeadDto>>
    {
        private readonly ContactContext _context;
        private readonly IMapper _mapper;

        private IQueryable<LeadRecord> RecordSet =>
            from lead in _context.Leads
            join topic in _context.Topics on lead.TopicId equals topic.Id
            join individual in _context.Individuals on lead.Contact.IndividualId equals individual.Id

            join organizations in _context.Organizations on lead.Contact.OrganizationId equals organizations.Id into organizationJoin
            from organization in organizationJoin.DefaultIfEmpty()

            join owners in _context.DealerEmployees on lead.OwnerEmployeeId equals owners.Id into ownerJoin
            from owner in ownerJoin.DefaultIfEmpty()
            join ownerIndividuals in _context.Individuals on owner.IndividualId equals ownerIndividuals.Id into ownerIndividualJoin
            from ownerIndividual in ownerIndividualJoin.DefaultIfEmpty()
            join ownerOrganizations in _context.Organizations on owner.OrganizationId equals ownerOrganizations.Id into ownerOrganizationJoin
            from ownerOrganization in ownerOrganizationJoin.DefaultIfEmpty()

            select new LeadRecord
            {
                Lead = lead,
                Topic = topic,
                Individual = individual,
                Organization = organization,
                EmployeeOwner = owner == null
                    ? null
                    : new EmployeeOwnerRecord
                    {
                        Employee = owner,
                        Individual = ownerIndividual,
                        Organization = ownerOrganization
                    }
            };

        public LeadQueryHandler(ContactContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<LeadDto> Handle(LeadByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (!DealerId.TryParse(request.DistributorId, request.DealerCode, out var dealerId))
                return null;

            var result = await RecordSet
                .Where(r => Convert.ToString(r.Lead.DealerId) == dealerId.Value)
                .SingleOrDefaultAsync(r => r.Lead.Id == request.Id, cancellationToken);

            return _mapper.Map<LeadDto>(result);
        }

        public async Task<PagedResponse<LeadDto>> Handle(LeadQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (!DealerId.TryParse(request.DistributorId, request.DealerCode, out var dealerId))
                return new PagedResponse<LeadDto>(PageOptions.From(request), 0);

            var result = await RecordSet
                .Where(r => Convert.ToString(r.Lead.DealerId) == dealerId.Value)
                .SortBy(request.SortCriterion, request.SortDirection)
                .FilterByIndividualName(request.IndividualName)
                .FilterByOrganizationName(request.OrganizationName)
                .FilterByIndividualTelephone(request.IndividualTelephone, _context.Set<ContactPoint<Telephone>>())
                .FilterByOrganizationTelephone(request.OrganizationTelephone, _context.Set<ContactPoint<Telephone>>())
                .FilterByEmployeeOwnerId(request.EmployeeOwnerId)
                .FilterByStatus(request.Status)
                .Paginate(request, cancellationToken);

            return _mapper.MapPagedResponse<LeadRecord, LeadDto>(result);
        }
    }
}
