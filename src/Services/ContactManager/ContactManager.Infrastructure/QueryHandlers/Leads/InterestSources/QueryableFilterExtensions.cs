﻿using NexCore.ContactManager.Domain.Leads;
using System.Linq;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Leads.InterestSources
{
    public static class QueryableFilterExtensions
    {
        public static IQueryable<InterestSource> FilterByName(this IQueryable<InterestSource> query,
            string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return query;

            var name = value.Trim().ToLowerInvariant();

            return query.Where(t => t.Name.Contains(name));
        }
    }
}
