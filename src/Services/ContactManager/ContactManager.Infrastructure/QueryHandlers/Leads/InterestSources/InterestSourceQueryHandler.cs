﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.ContactManager.Application.Leads.Queries.InterestSources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Leads.InterestSources
{
    public class InterestSourceQueryHandler : IRequestHandler<InterestSourceByIdQuery, InterestSourceDto>, IRequestHandler<InterestSourceQuery, IEnumerable<InterestSourceDto>>
    {
        private readonly ContactContext _context;
        private readonly IMapper _mapper;

        public InterestSourceQueryHandler(ContactContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<InterestSourceDto> Handle(InterestSourceByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var interest = await _context.InterestSources.SingleOrDefaultAsync(s => s.Id == request.Id, cancellationToken);

            return _mapper.Map<InterestSourceDto>(interest);
        }

        public async Task<IEnumerable<InterestSourceDto>> Handle(InterestSourceQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var interests = await _context.InterestSources
                .FilterByName(request.Name)
                .OrderBy(i => i.Name)
                .ToListAsync(cancellationToken);

            return _mapper.Map<IEnumerable<InterestSourceDto>>(interests);
        }
    }
}
