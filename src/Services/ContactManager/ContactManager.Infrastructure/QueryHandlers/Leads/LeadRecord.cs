﻿using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.Domain.Organizations;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Leads
{
    public class LeadRecord
    {
        public Lead Lead { get; set; }

        public Topic Topic { get; set; }

        public Individual Individual { get; set; }

        public Organization Organization { get; set; }

        public EmployeeOwnerRecord EmployeeOwner { get; set; }
    }
}
