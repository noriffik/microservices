﻿using AutoMapper;
using MediatR;
using NexCore.ContactManager.Application.Languages.Queries;
using NexCore.ContactManager.Application.Providers;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Languages
{
    public class LanguageQueryHandler : IRequestHandler<LanguageQuery, IEnumerable<LanguageDto>>
    {
        private readonly ILanguageProvider _languageProvider;
        private readonly IMapper _mapper;

        public LanguageQueryHandler(ILanguageProvider languageProvider, IMapper mapper)
        {
            _languageProvider = languageProvider ?? throw new ArgumentNullException(nameof(languageProvider));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<LanguageDto>> Handle(LanguageQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return _mapper.Map<IEnumerable<LanguageDto>>(await _languageProvider.Get());
        }
    }
}
