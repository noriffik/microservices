﻿using AutoMapper;
using MediatR;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Individuals.Queries;
using NexCore.ContactManager.Domain.Individuals;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Individuals
{
    public class CommunicationMediumQueryHandler : IRequestHandler<CommunicationMediumQuery, IEnumerable<CommunicationMediumDto>>
    {
        private readonly IMapper _mapper;

        public CommunicationMediumQueryHandler(IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<CommunicationMediumDto>> Handle(CommunicationMediumQuery request, CancellationToken cancellationToken)
        {
            if (request == null) 
                throw new ArgumentNullException(nameof(request));

            return await Task.FromResult(_mapper.Map<IEnumerable<CommunicationMediumDto>>(CommunicationMedium.All));
        }
    }
}
