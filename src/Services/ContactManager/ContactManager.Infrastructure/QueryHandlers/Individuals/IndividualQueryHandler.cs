﻿using AutoMapper;
using MediatR;
using NexCore.Application.Extensions;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Individuals.Queries;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Contacts;
using NexCore.Infrastructure.QueryHandlers;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Individuals
{
    public class IndividualQueryHandler :
        IRequestHandler<IndividualByIdQuery, IndividualDto>,
        IRequestHandler<IndividualQuery, PagedResponse<IndividualDto>>
    {
        private readonly ContactContext _context;
        private readonly IMapper _mapper;
        private IContactableQueryBuilder<Individual> _contactableQuery;

        public IContactableQueryBuilder<Individual> ContactableQuery
        {
            get => _contactableQuery ?? (_contactableQuery = new ContactableQueryBuilder<Individual>(_context));
            set => _contactableQuery = value ?? throw new ArgumentNullException(nameof(value));
        }

        public IndividualQueryHandler(ContactContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IndividualDto> Handle(IndividualByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var record = await _context.Individuals.FindAsync(request.Id);

            return _mapper.Map<IndividualDto>(record);
        }

        public async Task<PagedResponse<IndividualDto>> Handle(IndividualQuery request, CancellationToken cancellationToken)
        {
            var query = _context.Individuals.AsQueryable();

            if (request.Name != null)
                query = query.ByName(request);

            if (request.Telephone != null)
                query = query.ByTelephone(_context.Set<ContactPoint<Telephone>>(), request);

            var records = await query.Paginate(request, cancellationToken);

            return _mapper.MapPagedResponse<Individual, IndividualDto>(records);
        }
    }
}
