﻿using Microsoft.EntityFrameworkCore;
using NexCore.ContactManager.Application.Individuals.Queries;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Individuals;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Individuals
{
    public static class IndividualFilterExtensions
    {
        public static IQueryable<Individual> ByName(this IQueryable<Individual> query, IndividualQuery request)
        {
            if (string.IsNullOrWhiteSpace(request.Name))
                return query;

            var name = request.Name.Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Aggregate((a, b) => $"{a} {b}");

            return query.Where(i => EF.Property<string>(i.Name, "FullLastNameFirst").Contains(name)
                                    || EF.Property<string>(i.Name, "FullFirstNameFirst").Contains(name));
        }

        public static IQueryable<Individual> ByTelephone(this IQueryable<Individual> query,
            IQueryable<ContactPoint<Telephone>> telephones, IndividualQuery request)
        {
            var number = Regex.Replace(request.Telephone, "[^0-9]", "");

            var matchingTelephones = from telephone in telephones
                where telephone.IsPrimary && telephone.Value.Number.StartsWith(number)
                select telephone;

            return from individual in query
                join telephone in matchingTelephones on individual.Contact.Id equals EF.Property<int>(telephone, "ContactInfoId")
                select individual;
        }
    }
}