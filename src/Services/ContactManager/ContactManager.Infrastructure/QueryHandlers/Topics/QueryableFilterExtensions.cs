﻿using NexCore.ContactManager.Domain.Leads;
using System.Linq;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Topics
{
    public static class QueryableFilterExtensions
    {
        public static IQueryable<Topic> FilterByName(this IQueryable<Topic> query,
            string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return query;

            var name = value.Trim();

            return query.Where(t => t.Name.Contains(name));
        }
    }
}
