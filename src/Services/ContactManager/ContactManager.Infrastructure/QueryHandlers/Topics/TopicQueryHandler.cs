﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Extensions;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Leads.Queries.Topics;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Infrastructure.QueryHandlers;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Topics
{
    public class TopicQueryHandler : IRequestHandler<TopicByIdQuery, TopicDto>,
        IRequestHandler<TopicQuery, PagedResponse<TopicDto>>
    {
        private readonly ContactContext _context;
        private readonly IMapper _mapper;

        public TopicQueryHandler(ContactContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<TopicDto> Handle(TopicByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await _context.Topics.SingleOrDefaultAsync(t => t.Id == request.Id, cancellationToken);

            return _mapper.Map<TopicDto>(result);
        }

        public async Task<PagedResponse<TopicDto>> Handle(TopicQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await _context.Topics
                .OrderBy(t => t.Name)
                .FilterByName(request.Name)
                .Paginate(request, cancellationToken);

            return _mapper.MapPagedResponse<Topic, TopicDto>(result);
        }
    }
}
