﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Distributors
{
    public static class QueryableSortExtensions
    {
        public static IQueryable<DistributorRecord> SortBy(this IQueryable<DistributorRecord> query, SortCriterion? sortCriterion, SortDirection? sortDirection)
        {
            switch (sortCriterion)
            {
                case SortCriterion.DistributorId:
                    return query.InnerSort(r => r.Distributor.Id, sortDirection);

                case SortCriterion.OrganizationName:
                    return query.InnerSort(r => r.Organization.Name, sortDirection);

                default:
                    return query.InnerSort(r => r.Distributor.Id, sortDirection);
            }
        }

        private static IQueryable<DistributorRecord> InnerSort(this IQueryable<DistributorRecord> query,
            Expression<Func<DistributorRecord, object>> keySelector, SortDirection? sortDirection)
        {
            return sortDirection == SortDirection.Descending
                ? query.OrderByDescending(keySelector)
                : query.OrderBy(keySelector);
        }
    }
}
