﻿using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Organizations;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Distributors
{
    public class DistributorRecord
    {
        public Distributor Distributor { get; set; }

        public Organization Organization { get; set; }
    }
}
