﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Extensions;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries;
using NexCore.Infrastructure.QueryHandlers;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Distributors
{
    public class DistributorQueryHandler : IRequestHandler<DistributorByIdQuery, DistributorDto>,
        IRequestHandler<DistributorsQuery, PagedResponse<DistributorDto>>
    {
        private readonly ContactContext _context;
        private readonly IMapper _mapper;

        private IQueryable<DistributorRecord> RecordSet => from distributor in _context.Distributors
            join organization in _context.Organizations on distributor.OrganizationId equals organization.Id
            select new DistributorRecord
            {
                Distributor = distributor,
                Organization = organization
            };

        public DistributorQueryHandler(ContactContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<DistributorDto> Handle(DistributorByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await RecordSet.SingleOrDefaultAsync(r =>
                Convert.ToString(r.Distributor.Id) == request.Id, cancellationToken);

            return _mapper.Map<DistributorDto>(result);
        }

        public async Task<PagedResponse<DistributorDto>> Handle(DistributorsQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await RecordSet
                .SortBy(request.SortCriterion, request.SortDirection)
                .Paginate(request, cancellationToken);

            return _mapper.MapPagedResponse<DistributorRecord, DistributorDto>(result);
        }
    }
}
