﻿using AutoMapper;
using NexCore.ContactManager.Application.Distributors.Queries;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Distributors
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<DistributorRecord, DistributorDto>()
                .ForMember(d => d.Id, p => p.MapFrom(s => s.Distributor.Id))
                .ForMember(d => d.Organization, p => p.MapFrom(s => s.Organization))
                .ForMember(d => d.CountryCode, p => p.MapFrom(s => s.Distributor.CountryCode));
        }
    }
}
