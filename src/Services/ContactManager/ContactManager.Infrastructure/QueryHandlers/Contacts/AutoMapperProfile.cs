﻿using AutoMapper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Domain.Contacts;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Contacts
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ContactInfo, ContactDto>()
                .ForMember(d => d.Telephones, s => s.MapFrom(_ => _.Telephones))
                .ForMember(d => d.Addresses, s => s.MapFrom(_ => _.PhysicalAddresses))
                .ForMember(d => d.Emails, s => s.MapFrom(_ => _.EmailAddresses));

            CreateMap<IContactPoint<PhysicalAddress>, PhysicalAddressDto>()
                .ForMember(d => d.City, s => s.MapFrom(_ => _.Value.City))
                .ForMember(d => d.Country, s => s.MapFrom(_ => _.Value.Country))
                .ForMember(d => d.Geolocation, s => s.MapFrom(_ => _.Value.Geolocation))
                .ForMember(d => d.PostCode, s => s.MapFrom(_ => _.Value.PostCode))
                .ForMember(d => d.Region, s => s.MapFrom(_ => _.Value.Region))
                .ForMember(d => d.Street, s => s.MapFrom(_ => _.Value.Street));

            CreateMap<PhysicalAddressElement, PhysicalAddressElementDto>()
                .ForMember(d => d.Name, s => s.MapFrom(_ => _.Value));

            CreateMap<PhysicalAddressCodedElement, PhysicalAddressElementDto>()
                .ForMember(d => d.Name, s => s.MapFrom(_ => _.Value));

            CreateMap<IContactPoint<EmailAddress>, EmailAddressDto>()
                .ForMember(d => d.Address, s => s.MapFrom(_ => _.Value.Address));

            CreateMap<IContactPoint<Telephone>, TelephoneDto>()
                .ForMember(d => d.Number, s => s.MapFrom(_ => _.Value))
                .ForMember(d => d.Type, s => s.MapFrom(_ => _.Value.Type));
        }
    }
}
