﻿using Microsoft.EntityFrameworkCore;
using NexCore.ContactManager.Domain.Contacts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.ContactManager.Infrastructure.QueryHandlers.Contacts
{
    public interface IContactableQueryBuilder<out TContactable> where TContactable : class, IContactable
    {
        IQueryable<TContactable> ByTelephone(string number);
    }

    public class ContactableQueryBuilder<TContactable> : IContactableQueryBuilder<TContactable> where TContactable : class, IContactable
    {
        private readonly ContactContext _context;

        public ContactableQueryBuilder(ContactContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IQueryable<TContactable> ByTelephone(string number)
        {
            var telephones = from telephone in _context.Set<ContactPoint<Telephone>>()
                             where telephone.Value.Number == number
                             select telephone;

            return from individual in _context.Set<TContactable>()
                   join telephone in telephones on individual.Contact.Id equals EF.Property<int>(telephone, "ContactInfoId")
                   select individual;
        }
    }
}
