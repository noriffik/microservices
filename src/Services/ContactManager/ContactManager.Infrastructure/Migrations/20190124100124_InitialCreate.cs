﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "contacts");

            migrationBuilder.CreateSequence(
                name: "sq_Company",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "sq_ContactInfo",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "sq_CorporateCustomer",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "sq_Dealer",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "sq_EmailAddress",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "sq_Employee",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "sq_Job",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "sq_Person",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "sq_PhysicalAddress",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "sq_PrivateCustomer",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "sq_Telephone",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "ContactInfo",
                schema: "contacts",
                columns: table => new
                {
                    ContactInfoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactInfo", x => x.ContactInfoId);
                });

            migrationBuilder.CreateTable(
                name: "Job",
                schema: "contacts",
                columns: table => new
                {
                    JobId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Job", x => x.JobId);
                });

            migrationBuilder.CreateTable(
                name: "Company",
                schema: "contacts",
                columns: table => new
                {
                    CompanyId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Edrpou = table.Column<string>(nullable: true),
                    Ipn = table.Column<string>(nullable: true),
                    ContactId = table.Column<int>(nullable: true),
                    LegalAddress = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company", x => x.CompanyId);
                    table.ForeignKey(
                        name: "FK_Company_ContactInfo_ContactId",
                        column: x => x.ContactId,
                        principalSchema: "contacts",
                        principalTable: "ContactInfo",
                        principalColumn: "ContactInfoId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmailAddress",
                schema: "contacts",
                columns: table => new
                {
                    EmailAddressId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    IsPrimary = table.Column<bool>(nullable: false),
                    ContactInfoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailAddress", x => x.EmailAddressId);
                    table.ForeignKey(
                        name: "FK_EmailAddress_ContactInfo_ContactInfoId",
                        column: x => x.ContactInfoId,
                        principalSchema: "contacts",
                        principalTable: "ContactInfo",
                        principalColumn: "ContactInfoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Person",
                schema: "contacts",
                columns: table => new
                {
                    PersonId = table.Column<int>(nullable: false),
                    Firstname = table.Column<string>(nullable: true),
                    Lastname = table.Column<string>(nullable: true),
                    Middlename = table.Column<string>(nullable: true),
                    Birthday = table.Column<DateTime>(nullable: true),
                    Gender = table.Column<int>(nullable: true),
                    ContactId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Person", x => x.PersonId);
                    table.ForeignKey(
                        name: "FK_Person_ContactInfo_ContactId",
                        column: x => x.ContactId,
                        principalSchema: "contacts",
                        principalTable: "ContactInfo",
                        principalColumn: "ContactInfoId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PhysicalAddress",
                schema: "contacts",
                columns: table => new
                {
                    PhysicalAddressId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    CityCode = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    RegionCode = table.Column<string>(nullable: true),
                    Region = table.Column<string>(nullable: true),
                    CountryCode = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    GeolocationLatitude = table.Column<double>(nullable: true),
                    GeolocationLongitude = table.Column<double>(nullable: true),
                    PostCode = table.Column<string>(nullable: true),
                    FullAddress = table.Column<string>(nullable: true),
                    IsPrimary = table.Column<bool>(nullable: false),
                    ContactInfoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhysicalAddress", x => x.PhysicalAddressId);
                    table.ForeignKey(
                        name: "FK_PhysicalAddress_ContactInfo_ContactInfoId",
                        column: x => x.ContactInfoId,
                        principalSchema: "contacts",
                        principalTable: "ContactInfo",
                        principalColumn: "ContactInfoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Telephone",
                schema: "contacts",
                columns: table => new
                {
                    TelephoneId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    CountryCode = table.Column<string>(nullable: true),
                    AreaCode = table.Column<string>(nullable: true),
                    LocalNumber = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: true),
                    E123 = table.Column<string>(nullable: true),
                    Number = table.Column<string>(nullable: true),
                    IsPrimary = table.Column<bool>(nullable: false),
                    ContactInfoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Telephone", x => x.TelephoneId);
                    table.ForeignKey(
                        name: "FK_Telephone_ContactInfo_ContactInfoId",
                        column: x => x.ContactInfoId,
                        principalSchema: "contacts",
                        principalTable: "ContactInfo",
                        principalColumn: "ContactInfoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CorporateCustomer",
                schema: "contacts",
                columns: table => new
                {
                    CorporateCustomerId = table.Column<int>(nullable: false),
                    ContactableId = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CorporateCustomer", x => x.CorporateCustomerId);
                    table.ForeignKey(
                        name: "FK_CorporateCustomer_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "contacts",
                        principalTable: "Company",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CorporateCustomer_Company_ContactableId",
                        column: x => x.ContactableId,
                        principalSchema: "contacts",
                        principalTable: "Company",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Dealer",
                schema: "contacts",
                columns: table => new
                {
                    DealerId = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: false),
                    Sales = table.Column<bool>(nullable: false),
                    Service = table.Column<bool>(nullable: false),
                    Since = table.Column<DateTime>(nullable: false),
                    Till = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dealer", x => x.DealerId);
                    table.ForeignKey(
                        name: "FK_Dealer_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "contacts",
                        principalTable: "Company",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Employee",
                schema: "contacts",
                columns: table => new
                {
                    EmployeeId = table.Column<int>(nullable: false),
                    PersonId = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee", x => x.EmployeeId);
                    table.ForeignKey(
                        name: "FK_Employee_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "contacts",
                        principalTable: "Company",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Employee_Job_JobId",
                        column: x => x.JobId,
                        principalSchema: "contacts",
                        principalTable: "Job",
                        principalColumn: "JobId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Employee_Person_PersonId",
                        column: x => x.PersonId,
                        principalSchema: "contacts",
                        principalTable: "Person",
                        principalColumn: "PersonId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PrivateCustomer",
                schema: "contacts",
                columns: table => new
                {
                    PrivateCustomerId = table.Column<int>(nullable: false),
                    ContactableId = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrivateCustomer", x => x.PrivateCustomerId);
                    table.ForeignKey(
                        name: "FK_PrivateCustomer_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "contacts",
                        principalTable: "Company",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PrivateCustomer_Person_ContactableId",
                        column: x => x.ContactableId,
                        principalSchema: "contacts",
                        principalTable: "Person",
                        principalColumn: "PersonId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Company_ContactId",
                schema: "contacts",
                table: "Company",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_CorporateCustomer_CompanyId",
                schema: "contacts",
                table: "CorporateCustomer",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_CorporateCustomer_ContactableId_CompanyId",
                schema: "contacts",
                table: "CorporateCustomer",
                columns: new[] { "ContactableId", "CompanyId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Dealer_Code",
                schema: "contacts",
                table: "Dealer",
                column: "Code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Dealer_CompanyId",
                schema: "contacts",
                table: "Dealer",
                column: "CompanyId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EmailAddress_ContactInfoId",
                schema: "contacts",
                table: "EmailAddress",
                column: "ContactInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_JobId",
                schema: "contacts",
                table: "Employee",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_PersonId",
                schema: "contacts",
                table: "Employee",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_CompanyId_PersonId_JobId",
                schema: "contacts",
                table: "Employee",
                columns: new[] { "CompanyId", "PersonId", "JobId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Person_ContactId",
                schema: "contacts",
                table: "Person",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_PhysicalAddress_ContactInfoId",
                schema: "contacts",
                table: "PhysicalAddress",
                column: "ContactInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_PrivateCustomer_CompanyId",
                schema: "contacts",
                table: "PrivateCustomer",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_PrivateCustomer_ContactableId_CompanyId",
                schema: "contacts",
                table: "PrivateCustomer",
                columns: new[] { "ContactableId", "CompanyId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Telephone_ContactInfoId",
                schema: "contacts",
                table: "Telephone",
                column: "ContactInfoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CorporateCustomer",
                schema: "contacts");

            migrationBuilder.DropTable(
                name: "Dealer",
                schema: "contacts");

            migrationBuilder.DropTable(
                name: "EmailAddress",
                schema: "contacts");

            migrationBuilder.DropTable(
                name: "Employee",
                schema: "contacts");

            migrationBuilder.DropTable(
                name: "PhysicalAddress",
                schema: "contacts");

            migrationBuilder.DropTable(
                name: "PrivateCustomer",
                schema: "contacts");

            migrationBuilder.DropTable(
                name: "Telephone",
                schema: "contacts");

            migrationBuilder.DropTable(
                name: "Job",
                schema: "contacts");

            migrationBuilder.DropTable(
                name: "Company",
                schema: "contacts");

            migrationBuilder.DropTable(
                name: "Person",
                schema: "contacts");

            migrationBuilder.DropTable(
                name: "ContactInfo",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_Company",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_ContactInfo",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_CorporateCustomer",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_Dealer",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_EmailAddress",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_Employee",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_Job",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_Person",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_PhysicalAddress",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_PrivateCustomer",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_Telephone",
                schema: "contacts");
        }
    }
}
