﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Rename_Company_To_Organization : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company_ContactInfo_ContactId",
                schema: "contacts",
                table: "Company");

            migrationBuilder.DropForeignKey(
                name: "FK_CorporateCustomer_Company_CompanyId",
                schema: "contacts",
                table: "CorporateCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_CorporateCustomer_Company_ContactableId",
                schema: "contacts",
                table: "CorporateCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_Dealer_Company_CompanyId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.DropForeignKey(
                name: "FK_DealerPrivateCustomer_Company_CompanyId",
                schema: "contacts",
                table: "DealerPrivateCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Company_CompanyId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Importer_Company_CompanyId",
                schema: "contacts",
                table: "Importer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Company",
                schema: "contacts",
                table: "Company");

            migrationBuilder.DropSequence(
                name: "sq_Company",
                schema: "contacts");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                schema: "contacts",
                table: "Company");

            migrationBuilder.RenameTable(
                name: "Company",
                schema: "contacts",
                newName: "Organization",
                newSchema: "contacts");

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                schema: "contacts",
                table: "Importer",
                newName: "OrganizationId");

            migrationBuilder.RenameIndex(
                name: "IX_Importer_CompanyId",
                schema: "contacts",
                table: "Importer",
                newName: "IX_Importer_OrganizationId");

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                schema: "contacts",
                table: "Employee",
                newName: "OrganizationId");

            migrationBuilder.RenameIndex(
                name: "IX_Employee_CompanyId_IndividualId_JobId",
                schema: "contacts",
                table: "Employee",
                newName: "IX_Employee_OrganizationId_IndividualId_JobId");

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                newName: "OrganizationId");

            migrationBuilder.RenameIndex(
                name: "IX_DealerPrivateCustomer_ContactableId_CompanyId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                newName: "IX_DealerPrivateCustomer_ContactableId_OrganizationId");

            migrationBuilder.RenameIndex(
                name: "IX_DealerPrivateCustomer_CompanyId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                newName: "IX_DealerPrivateCustomer_OrganizationId");

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                schema: "contacts",
                table: "Dealer",
                newName: "OrganizationId");

            migrationBuilder.RenameIndex(
                name: "IX_Dealer_CompanyId",
                schema: "contacts",
                table: "Dealer",
                newName: "IX_Dealer_OrganizationId");

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                schema: "contacts",
                table: "CorporateCustomer",
                newName: "OrganizationId");

            migrationBuilder.RenameIndex(
                name: "IX_CorporateCustomer_ContactableId_CompanyId",
                schema: "contacts",
                table: "CorporateCustomer",
                newName: "IX_CorporateCustomer_ContactableId_OrganizationId");

            migrationBuilder.RenameIndex(
                name: "IX_CorporateCustomer_CompanyId",
                schema: "contacts",
                table: "CorporateCustomer",
                newName: "IX_CorporateCustomer_OrganizationId");

            migrationBuilder.RenameIndex(
                name: "IX_Company_ContactId",
                schema: "contacts",
                table: "Organization",
                newName: "IX_Organization_ContactId");

            migrationBuilder.CreateSequence(
                name: "sq_Organization",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.AddColumn<int>(
                name: "OrganizationId",
                schema: "contacts",
                table: "Organization",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Organization",
                schema: "contacts",
                table: "Organization",
                column: "OrganizationId");

            migrationBuilder.AddForeignKey(
                name: "FK_CorporateCustomer_Organization_ContactableId",
                schema: "contacts",
                table: "CorporateCustomer",
                column: "ContactableId",
                principalSchema: "contacts",
                principalTable: "Organization",
                principalColumn: "OrganizationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CorporateCustomer_Organization_OrganizationId",
                schema: "contacts",
                table: "CorporateCustomer",
                column: "OrganizationId",
                principalSchema: "contacts",
                principalTable: "Organization",
                principalColumn: "OrganizationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Dealer_Organization_OrganizationId",
                schema: "contacts",
                table: "Dealer",
                column: "OrganizationId",
                principalSchema: "contacts",
                principalTable: "Organization",
                principalColumn: "OrganizationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DealerPrivateCustomer_Organization_OrganizationId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                column: "OrganizationId",
                principalSchema: "contacts",
                principalTable: "Organization",
                principalColumn: "OrganizationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Organization_OrganizationId",
                schema: "contacts",
                table: "Employee",
                column: "OrganizationId",
                principalSchema: "contacts",
                principalTable: "Organization",
                principalColumn: "OrganizationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Importer_Organization_OrganizationId",
                schema: "contacts",
                table: "Importer",
                column: "OrganizationId",
                principalSchema: "contacts",
                principalTable: "Organization",
                principalColumn: "OrganizationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Organization_ContactInfo_ContactId",
                schema: "contacts",
                table: "Organization",
                column: "ContactId",
                principalSchema: "contacts",
                principalTable: "ContactInfo",
                principalColumn: "ContactInfoId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CorporateCustomer_Organization_ContactableId",
                schema: "contacts",
                table: "CorporateCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_CorporateCustomer_Organization_OrganizationId",
                schema: "contacts",
                table: "CorporateCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_Dealer_Organization_OrganizationId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.DropForeignKey(
                name: "FK_DealerPrivateCustomer_Organization_OrganizationId",
                schema: "contacts",
                table: "DealerPrivateCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Organization_OrganizationId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Importer_Organization_OrganizationId",
                schema: "contacts",
                table: "Importer");

            migrationBuilder.DropForeignKey(
                name: "FK_Organization_ContactInfo_ContactId",
                schema: "contacts",
                table: "Organization");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Organization",
                schema: "contacts",
                table: "Organization");

            migrationBuilder.DropSequence(
                name: "sq_Organization",
                schema: "contacts");

            migrationBuilder.DropColumn(
                name: "OrganizationId",
                schema: "contacts",
                table: "Organization");

            migrationBuilder.RenameTable(
                name: "Organization",
                schema: "contacts",
                newName: "Company",
                newSchema: "contacts");

            migrationBuilder.RenameColumn(
                name: "OrganizationId",
                schema: "contacts",
                table: "Importer",
                newName: "CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Importer_OrganizationId",
                schema: "contacts",
                table: "Importer",
                newName: "IX_Importer_CompanyId");

            migrationBuilder.RenameColumn(
                name: "OrganizationId",
                schema: "contacts",
                table: "Employee",
                newName: "CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Employee_OrganizationId_IndividualId_JobId",
                schema: "contacts",
                table: "Employee",
                newName: "IX_Employee_CompanyId_IndividualId_JobId");

            migrationBuilder.RenameColumn(
                name: "OrganizationId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                newName: "CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_DealerPrivateCustomer_ContactableId_OrganizationId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                newName: "IX_DealerPrivateCustomer_ContactableId_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_DealerPrivateCustomer_OrganizationId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                newName: "IX_DealerPrivateCustomer_CompanyId");

            migrationBuilder.RenameColumn(
                name: "OrganizationId",
                schema: "contacts",
                table: "Dealer",
                newName: "CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Dealer_OrganizationId",
                schema: "contacts",
                table: "Dealer",
                newName: "IX_Dealer_CompanyId");

            migrationBuilder.RenameColumn(
                name: "OrganizationId",
                schema: "contacts",
                table: "CorporateCustomer",
                newName: "CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_CorporateCustomer_ContactableId_OrganizationId",
                schema: "contacts",
                table: "CorporateCustomer",
                newName: "IX_CorporateCustomer_ContactableId_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_CorporateCustomer_OrganizationId",
                schema: "contacts",
                table: "CorporateCustomer",
                newName: "IX_CorporateCustomer_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Organization_ContactId",
                schema: "contacts",
                table: "Company",
                newName: "IX_Company_ContactId");

            migrationBuilder.CreateSequence(
                name: "sq_Company",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.AddColumn<int>(
                name: "CompanyId",
                schema: "contacts",
                table: "Company",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Company",
                schema: "contacts",
                table: "Company",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Company_ContactInfo_ContactId",
                schema: "contacts",
                table: "Company",
                column: "ContactId",
                principalSchema: "contacts",
                principalTable: "ContactInfo",
                principalColumn: "ContactInfoId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CorporateCustomer_Company_CompanyId",
                schema: "contacts",
                table: "CorporateCustomer",
                column: "CompanyId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CorporateCustomer_Company_ContactableId",
                schema: "contacts",
                table: "CorporateCustomer",
                column: "ContactableId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Dealer_Company_CompanyId",
                schema: "contacts",
                table: "Dealer",
                column: "CompanyId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DealerPrivateCustomer_Company_CompanyId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                column: "CompanyId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Company_CompanyId",
                schema: "contacts",
                table: "Employee",
                column: "CompanyId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Importer_Company_CompanyId",
                schema: "contacts",
                table: "Importer",
                column: "CompanyId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
