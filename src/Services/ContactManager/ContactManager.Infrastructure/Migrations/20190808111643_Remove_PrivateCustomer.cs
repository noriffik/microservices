﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Remove_PrivateCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PrivateCustomer_Company_CompanyId",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_PrivateCustomer_Person_ContactableId",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_PrivateCustomer_Dealer_DealerId",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PrivateCustomer",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropSequence(
                name: "sq_PrivateCustomer",
                schema: "contacts");

            migrationBuilder.DropColumn(
                name: "PrivateCustomerType",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.RenameTable(
                name: "PrivateCustomer",
                schema: "contacts",
                newName: "DealerPrivateCustomer",
                newSchema: "contacts");

            migrationBuilder.RenameColumn(
                name: "PrivateCustomerId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                newName: "DealerPrivateCustomerId");

            migrationBuilder.RenameIndex(
                name: "IX_PrivateCustomer_DealerId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                newName: "IX_DealerPrivateCustomer_DealerId");

            migrationBuilder.RenameIndex(
                name: "IX_PrivateCustomer_ContactableId_CompanyId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                newName: "IX_DealerPrivateCustomer_ContactableId_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_PrivateCustomer_CompanyId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                newName: "IX_DealerPrivateCustomer_CompanyId");

            migrationBuilder.CreateSequence(
                name: "sq_DealerPrivateCustomer",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.AlterColumn<string>(
                name: "DealerId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_DealerPrivateCustomer",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                column: "DealerPrivateCustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_DealerPrivateCustomer_Company_CompanyId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                column: "CompanyId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DealerPrivateCustomer_Person_ContactableId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                column: "ContactableId",
                principalSchema: "contacts",
                principalTable: "Person",
                principalColumn: "PersonId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DealerPrivateCustomer_Dealer_DealerId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                column: "DealerId",
                principalSchema: "contacts",
                principalTable: "Dealer",
                principalColumn: "DealerId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DealerPrivateCustomer_Company_CompanyId",
                schema: "contacts",
                table: "DealerPrivateCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_DealerPrivateCustomer_Person_ContactableId",
                schema: "contacts",
                table: "DealerPrivateCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_DealerPrivateCustomer_Dealer_DealerId",
                schema: "contacts",
                table: "DealerPrivateCustomer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DealerPrivateCustomer",
                schema: "contacts",
                table: "DealerPrivateCustomer");

            migrationBuilder.DropSequence(
                name: "sq_DealerPrivateCustomer",
                schema: "contacts");

            migrationBuilder.RenameTable(
                name: "DealerPrivateCustomer",
                schema: "contacts",
                newName: "PrivateCustomer",
                newSchema: "contacts");

            migrationBuilder.RenameColumn(
                name: "DealerPrivateCustomerId",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "PrivateCustomerId");

            migrationBuilder.RenameIndex(
                name: "IX_DealerPrivateCustomer_ContactableId_CompanyId",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "IX_PrivateCustomer_ContactableId_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_DealerPrivateCustomer_DealerId",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "IX_PrivateCustomer_DealerId");

            migrationBuilder.RenameIndex(
                name: "IX_DealerPrivateCustomer_CompanyId",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "IX_PrivateCustomer_CompanyId");

            migrationBuilder.CreateSequence(
                name: "sq_PrivateCustomer",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.AlterColumn<string>(
                name: "DealerId",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "PrivateCustomerType",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PrivateCustomer",
                schema: "contacts",
                table: "PrivateCustomer",
                column: "PrivateCustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateCustomer_Company_CompanyId",
                schema: "contacts",
                table: "PrivateCustomer",
                column: "CompanyId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateCustomer_Person_ContactableId",
                schema: "contacts",
                table: "PrivateCustomer",
                column: "ContactableId",
                principalSchema: "contacts",
                principalTable: "Person",
                principalColumn: "PersonId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateCustomer_Dealer_DealerId",
                schema: "contacts",
                table: "PrivateCustomer",
                column: "DealerId",
                principalSchema: "contacts",
                principalTable: "Dealer",
                principalColumn: "DealerId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
