﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Rename_EntityPerson_In_Individual : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DealerPrivateCustomer_Person_ContactableId",
                schema: "contacts",
                table: "DealerPrivateCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Person_PersonId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Person_ContactInfo_ContactId",
                schema: "contacts",
                table: "Person");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Person",
                schema: "contacts",
                table: "Person");

            migrationBuilder.DropSequence(
                name: "sq_Person",
                schema: "contacts");

            migrationBuilder.RenameTable(
                name: "Person",
                schema: "contacts",
                newName: "Individual",
                newSchema: "contacts");

            migrationBuilder.RenameColumn(
                name: "PersonId",
                schema: "contacts",
                table: "Employee",
                newName: "IndividualId");

            migrationBuilder.RenameIndex(
                name: "IX_Employee_CompanyId_PersonId_JobId",
                schema: "contacts",
                table: "Employee",
                newName: "IX_Employee_CompanyId_IndividualId_JobId");

            migrationBuilder.RenameIndex(
                name: "IX_Employee_PersonId",
                schema: "contacts",
                table: "Employee",
                newName: "IX_Employee_IndividualId");

            migrationBuilder.RenameColumn(
                name: "PersonId",
                schema: "contacts",
                table: "Individual",
                newName: "IndividualId");

            migrationBuilder.RenameIndex(
                name: "IX_Person_ContactId",
                schema: "contacts",
                table: "Individual",
                newName: "IX_Individual_ContactId");

            migrationBuilder.CreateSequence(
                name: "sq_Individual",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Individual",
                schema: "contacts",
                table: "Individual",
                column: "IndividualId");

            migrationBuilder.AddForeignKey(
                name: "FK_DealerPrivateCustomer_Individual_ContactableId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                column: "ContactableId",
                principalSchema: "contacts",
                principalTable: "Individual",
                principalColumn: "IndividualId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Individual_IndividualId",
                schema: "contacts",
                table: "Employee",
                column: "IndividualId",
                principalSchema: "contacts",
                principalTable: "Individual",
                principalColumn: "IndividualId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Individual_ContactInfo_ContactId",
                schema: "contacts",
                table: "Individual",
                column: "ContactId",
                principalSchema: "contacts",
                principalTable: "ContactInfo",
                principalColumn: "ContactInfoId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DealerPrivateCustomer_Individual_ContactableId",
                schema: "contacts",
                table: "DealerPrivateCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Individual_IndividualId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Individual_ContactInfo_ContactId",
                schema: "contacts",
                table: "Individual");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Individual",
                schema: "contacts",
                table: "Individual");

            migrationBuilder.DropSequence(
                name: "sq_Individual",
                schema: "contacts");

            migrationBuilder.RenameTable(
                name: "Individual",
                schema: "contacts",
                newName: "Person",
                newSchema: "contacts");

            migrationBuilder.RenameColumn(
                name: "IndividualId",
                schema: "contacts",
                table: "Employee",
                newName: "PersonId");

            migrationBuilder.RenameIndex(
                name: "IX_Employee_CompanyId_IndividualId_JobId",
                schema: "contacts",
                table: "Employee",
                newName: "IX_Employee_CompanyId_PersonId_JobId");

            migrationBuilder.RenameIndex(
                name: "IX_Employee_IndividualId",
                schema: "contacts",
                table: "Employee",
                newName: "IX_Employee_PersonId");

            migrationBuilder.RenameColumn(
                name: "IndividualId",
                schema: "contacts",
                table: "Person",
                newName: "PersonId");

            migrationBuilder.RenameIndex(
                name: "IX_Individual_ContactId",
                schema: "contacts",
                table: "Person",
                newName: "IX_Person_ContactId");

            migrationBuilder.CreateSequence(
                name: "sq_Person",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Person",
                schema: "contacts",
                table: "Person",
                column: "PersonId");

            migrationBuilder.AddForeignKey(
                name: "FK_DealerPrivateCustomer_Person_ContactableId",
                schema: "contacts",
                table: "DealerPrivateCustomer",
                column: "ContactableId",
                principalSchema: "contacts",
                principalTable: "Person",
                principalColumn: "PersonId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Person_PersonId",
                schema: "contacts",
                table: "Employee",
                column: "PersonId",
                principalSchema: "contacts",
                principalTable: "Person",
                principalColumn: "PersonId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Person_ContactInfo_ContactId",
                schema: "contacts",
                table: "Person",
                column: "ContactId",
                principalSchema: "contacts",
                principalTable: "ContactInfo",
                principalColumn: "ContactInfoId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
