﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_Lead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_Lead",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Lead",
                schema: "contacts",
                columns: table => new
                {
                    LeadId = table.Column<int>(nullable: false),
                    DealerId = table.Column<string>(nullable: false),
                    DealerCode = table.Column<string>(nullable: false),
                    DistributorId = table.Column<string>(nullable: false),
                    TopicId = table.Column<int>(nullable: false),
                    IndividualId = table.Column<int>(nullable: false),
                    OrganizationId = table.Column<int>(nullable: true),
                    ContactType = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    OwnerEmployeeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lead", x => x.LeadId);
                    table.ForeignKey(
                        name: "FK_Lead_Individual_IndividualId",
                        column: x => x.IndividualId,
                        principalSchema: "contacts",
                        principalTable: "Individual",
                        principalColumn: "IndividualId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Lead_Organization_OrganizationId",
                        column: x => x.OrganizationId,
                        principalSchema: "contacts",
                        principalTable: "Organization",
                        principalColumn: "OrganizationId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Lead_Dealer_DealerId",
                        column: x => x.DealerId,
                        principalSchema: "contacts",
                        principalTable: "Dealer",
                        principalColumn: "DealerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Lead_Distributor_DistributorId",
                        column: x => x.DistributorId,
                        principalSchema: "contacts",
                        principalTable: "Distributor",
                        principalColumn: "DistributorId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Lead_DealerEmployee_OwnerEmployeeId",
                        column: x => x.OwnerEmployeeId,
                        principalSchema: "contacts",
                        principalTable: "DealerEmployee",
                        principalColumn: "EmployeeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Lead_LeadTopic_TopicId",
                        column: x => x.TopicId,
                        principalSchema: "contacts",
                        principalTable: "LeadTopic",
                        principalColumn: "LeadTopicId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Lead_IndividualId",
                schema: "contacts",
                table: "Lead",
                column: "IndividualId");

            migrationBuilder.CreateIndex(
                name: "IX_Lead_OrganizationId",
                schema: "contacts",
                table: "Lead",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_Lead_DealerId",
                schema: "contacts",
                table: "Lead",
                column: "DealerId");

            migrationBuilder.CreateIndex(
                name: "IX_Lead_DistributorId",
                schema: "contacts",
                table: "Lead",
                column: "DistributorId");

            migrationBuilder.CreateIndex(
                name: "IX_Lead_OwnerEmployeeId",
                schema: "contacts",
                table: "Lead",
                column: "OwnerEmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_Lead_TopicId",
                schema: "contacts",
                table: "Lead",
                column: "TopicId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Lead",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_Lead",
                schema: "contacts");
        }
    }
}
