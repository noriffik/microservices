﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_ExpandPrivateCustomer_By_PhysicalAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Type",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "Telephone_Type");

            migrationBuilder.RenameColumn(
                name: "Number",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "Telephone_Number");

            migrationBuilder.RenameColumn(
                name: "LocalNumber",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "Telephone_LocalNumber");

            migrationBuilder.RenameColumn(
                name: "E123",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "Telephone_E123");

            migrationBuilder.RenameColumn(
                name: "CountryCode",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "Telephone_CountryCode");

            migrationBuilder.RenameColumn(
                name: "AreaCode",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "Telephone_AreaCode");

            migrationBuilder.AddColumn<double>(
                name: "PhysicalAddress_GeolocationLatitude",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "PhysicalAddress_GeolocationLongitude",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalAddress_FullAddress",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalAddress_CityCode",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalAddress_City",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalAddress_CountryCode",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalAddress_Country",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalAddress_RegionCode",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalAddress_Region",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalAddress_PostCode",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Street",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhysicalAddress_GeolocationLatitude",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "PhysicalAddress_GeolocationLongitude",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "PhysicalAddress_FullAddress",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "PhysicalAddress_CityCode",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "PhysicalAddress_City",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "PhysicalAddress_CountryCode",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "PhysicalAddress_Country",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "PhysicalAddress_RegionCode",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "PhysicalAddress_Region",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "PhysicalAddress_PostCode",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "Street",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.RenameColumn(
                name: "Telephone_Type",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "Type");

            migrationBuilder.RenameColumn(
                name: "Telephone_Number",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "Number");

            migrationBuilder.RenameColumn(
                name: "Telephone_LocalNumber",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "LocalNumber");

            migrationBuilder.RenameColumn(
                name: "Telephone_E123",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "E123");

            migrationBuilder.RenameColumn(
                name: "Telephone_CountryCode",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "CountryCode");

            migrationBuilder.RenameColumn(
                name: "Telephone_AreaCode",
                schema: "contacts",
                table: "PrivateCustomer",
                newName: "AreaCode");
        }
    }
}
