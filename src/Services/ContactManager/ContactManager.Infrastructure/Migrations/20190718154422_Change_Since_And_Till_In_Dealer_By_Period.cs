﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Change_Since_And_Till_In_Dealer_By_Period : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Since",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.RenameColumn(
                name: "Till",
                schema: "contacts",
                table: "Dealer",
                newName: "PeriodTo");

            migrationBuilder.AddColumn<DateTime>(
                name: "PeriodFrom",
                schema: "contacts",
                table: "Dealer",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PeriodFrom",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.RenameColumn(
                name: "PeriodTo",
                schema: "contacts",
                table: "Dealer",
                newName: "Till");

            migrationBuilder.AddColumn<DateTime>(
                name: "Since",
                schema: "contacts",
                table: "Dealer",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
