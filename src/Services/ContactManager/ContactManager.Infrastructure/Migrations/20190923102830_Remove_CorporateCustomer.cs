﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Remove_CorporateCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CorporateCustomer",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_CorporateCustomer",
                schema: "contacts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_CorporateCustomer",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "CorporateCustomer",
                schema: "contacts",
                columns: table => new
                {
                    CorporateCustomerId = table.Column<int>(nullable: false),
                    ContactableId = table.Column<int>(nullable: false),
                    OrganizationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CorporateCustomer", x => x.CorporateCustomerId);
                    table.ForeignKey(
                        name: "FK_CorporateCustomer_Organization_ContactableId",
                        column: x => x.ContactableId,
                        principalSchema: "contacts",
                        principalTable: "Organization",
                        principalColumn: "OrganizationId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CorporateCustomer_Organization_OrganizationId",
                        column: x => x.OrganizationId,
                        principalSchema: "contacts",
                        principalTable: "Organization",
                        principalColumn: "OrganizationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CorporateCustomer_OrganizationId",
                schema: "contacts",
                table: "CorporateCustomer",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_CorporateCustomer_ContactableId_OrganizationId",
                schema: "contacts",
                table: "CorporateCustomer",
                columns: new[] { "ContactableId", "OrganizationId" },
                unique: true);
        }
    }
}
