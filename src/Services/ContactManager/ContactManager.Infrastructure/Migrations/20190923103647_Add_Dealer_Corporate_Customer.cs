﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_Dealer_Corporate_Customer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_DealerCorporateCustomer",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "DealerCorporateCustomer",
                schema: "contacts",
                columns: table => new
                {
                    DealerCorporateCustomerId = table.Column<int>(nullable: false),
                    ContactableId = table.Column<int>(nullable: false),
                    OrganizationId = table.Column<int>(nullable: false),
                    DealerId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DealerCorporateCustomer", x => x.DealerCorporateCustomerId);
                    table.ForeignKey(
                        name: "FK_DealerCorporateCustomer_Organization_ContactableId",
                        column: x => x.ContactableId,
                        principalSchema: "contacts",
                        principalTable: "Organization",
                        principalColumn: "OrganizationId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DealerCorporateCustomer_Dealer_DealerId",
                        column: x => x.DealerId,
                        principalSchema: "contacts",
                        principalTable: "Dealer",
                        principalColumn: "DealerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DealerCorporateCustomer_Organization_OrganizationId",
                        column: x => x.OrganizationId,
                        principalSchema: "contacts",
                        principalTable: "Organization",
                        principalColumn: "OrganizationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DealerCorporateCustomer_DealerId",
                schema: "contacts",
                table: "DealerCorporateCustomer",
                column: "DealerId");

            migrationBuilder.CreateIndex(
                name: "IX_DealerCorporateCustomer_OrganizationId",
                schema: "contacts",
                table: "DealerCorporateCustomer",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_DealerCorporateCustomer_ContactableId_OrganizationId",
                schema: "contacts",
                table: "DealerCorporateCustomer",
                columns: new[] { "ContactableId", "OrganizationId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DealerCorporateCustomer",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_DealerCorporateCustomer",
                schema: "contacts");
        }
    }
}
