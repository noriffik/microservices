﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_Comunication_Preferences : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_CommunicationPreferences",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.AddColumn<int>(
                name: "CommunicationsId",
                schema: "contacts",
                table: "PersonalPreferences",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CommunicationPreferences",
                schema: "contacts",
                columns: table => new
                {
                    CommunicationPreferencesId = table.Column<int>(nullable: false),
                    PreferredMedium = table.Column<string>(nullable: true),
                    BannedMediums = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommunicationPreferences", x => x.CommunicationPreferencesId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PersonalPreferences_CommunicationsId",
                schema: "contacts",
                table: "PersonalPreferences",
                column: "CommunicationsId");

            migrationBuilder.AddForeignKey(
                name: "FK_PersonalPreferences_CommunicationPreferences_CommunicationsId",
                schema: "contacts",
                table: "PersonalPreferences",
                column: "CommunicationsId",
                principalSchema: "contacts",
                principalTable: "CommunicationPreferences",
                principalColumn: "CommunicationPreferencesId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonalPreferences_CommunicationPreferences_CommunicationsId",
                schema: "contacts",
                table: "PersonalPreferences");

            migrationBuilder.DropTable(
                name: "CommunicationPreferences",
                schema: "contacts");

            migrationBuilder.DropIndex(
                name: "IX_PersonalPreferences_CommunicationsId",
                schema: "contacts",
                table: "PersonalPreferences");

            migrationBuilder.DropSequence(
                name: "sq_CommunicationPreferences",
                schema: "contacts");

            migrationBuilder.DropColumn(
                name: "CommunicationsId",
                schema: "contacts",
                table: "PersonalPreferences");
        }
    }
}
