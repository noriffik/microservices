﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_Requisites_To_Company : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RequisitesAddress",
                schema: "contacts",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BankRequisites",
                schema: "contacts",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RequisitesTelephones",
                schema: "contacts",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RequisitesFullName",
                schema: "contacts",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RequisitesShortName",
                schema: "contacts",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DirectorFirstname",
                schema: "contacts",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DirectorLastname",
                schema: "contacts",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DirectorMiddlename",
                schema: "contacts",
                table: "Company",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RequisitesAddress",
                schema: "contacts",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "BankRequisites",
                schema: "contacts",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "RequisitesTelephones",
                schema: "contacts",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "RequisitesFullName",
                schema: "contacts",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "RequisitesShortName",
                schema: "contacts",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "DirectorFirstname",
                schema: "contacts",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "DirectorLastname",
                schema: "contacts",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "DirectorMiddlename",
                schema: "contacts",
                table: "Company");
        }
    }
}
