﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_LeadTopic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_LeadTopic",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "LeadTopic",
                schema: "contacts",
                columns: table => new
                {
                    LeadTopicId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadTopic", x => x.LeadTopicId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LeadTopic",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_LeadTopic",
                schema: "contacts");
        }
    }
}
