﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Change_Employee_To_Abstract : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Dealer_DealerId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Individual_IndividualId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Job_JobId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Organization_OrganizationId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Employee",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "EmployeeType",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.RenameTable(
                name: "Employee",
                schema: "contacts",
                newName: "DealerEmployee",
                newSchema: "contacts");

            migrationBuilder.RenameIndex(
                name: "IX_Employee_OrganizationId_IndividualId_JobId",
                schema: "contacts",
                table: "DealerEmployee",
                newName: "IX_DealerEmployee_OrganizationId_IndividualId_JobId");

            migrationBuilder.RenameIndex(
                name: "IX_Employee_JobId",
                schema: "contacts",
                table: "DealerEmployee",
                newName: "IX_DealerEmployee_JobId");

            migrationBuilder.RenameIndex(
                name: "IX_Employee_IndividualId",
                schema: "contacts",
                table: "DealerEmployee",
                newName: "IX_DealerEmployee_IndividualId");

            migrationBuilder.RenameIndex(
                name: "IX_Employee_DealerId",
                schema: "contacts",
                table: "DealerEmployee",
                newName: "IX_DealerEmployee_DealerId");

            migrationBuilder.AlterColumn<string>(
                name: "DealerId",
                schema: "contacts",
                table: "DealerEmployee",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_DealerEmployee",
                schema: "contacts",
                table: "DealerEmployee",
                column: "EmployeeId");

            migrationBuilder.AddForeignKey(
                name: "FK_DealerEmployee_Dealer_DealerId",
                schema: "contacts",
                table: "DealerEmployee",
                column: "DealerId",
                principalSchema: "contacts",
                principalTable: "Dealer",
                principalColumn: "DealerId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DealerEmployee_Individual_IndividualId",
                schema: "contacts",
                table: "DealerEmployee",
                column: "IndividualId",
                principalSchema: "contacts",
                principalTable: "Individual",
                principalColumn: "IndividualId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DealerEmployee_Job_JobId",
                schema: "contacts",
                table: "DealerEmployee",
                column: "JobId",
                principalSchema: "contacts",
                principalTable: "Job",
                principalColumn: "JobId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DealerEmployee_Organization_OrganizationId",
                schema: "contacts",
                table: "DealerEmployee",
                column: "OrganizationId",
                principalSchema: "contacts",
                principalTable: "Organization",
                principalColumn: "OrganizationId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DealerEmployee_Dealer_DealerId",
                schema: "contacts",
                table: "DealerEmployee");

            migrationBuilder.DropForeignKey(
                name: "FK_DealerEmployee_Individual_IndividualId",
                schema: "contacts",
                table: "DealerEmployee");

            migrationBuilder.DropForeignKey(
                name: "FK_DealerEmployee_Job_JobId",
                schema: "contacts",
                table: "DealerEmployee");

            migrationBuilder.DropForeignKey(
                name: "FK_DealerEmployee_Organization_OrganizationId",
                schema: "contacts",
                table: "DealerEmployee");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DealerEmployee",
                schema: "contacts",
                table: "DealerEmployee");

            migrationBuilder.RenameTable(
                name: "DealerEmployee",
                schema: "contacts",
                newName: "Employee",
                newSchema: "contacts");

            migrationBuilder.RenameIndex(
                name: "IX_DealerEmployee_OrganizationId_IndividualId_JobId",
                schema: "contacts",
                table: "Employee",
                newName: "IX_Employee_OrganizationId_IndividualId_JobId");

            migrationBuilder.RenameIndex(
                name: "IX_DealerEmployee_JobId",
                schema: "contacts",
                table: "Employee",
                newName: "IX_Employee_JobId");

            migrationBuilder.RenameIndex(
                name: "IX_DealerEmployee_IndividualId",
                schema: "contacts",
                table: "Employee",
                newName: "IX_Employee_IndividualId");

            migrationBuilder.RenameIndex(
                name: "IX_DealerEmployee_DealerId",
                schema: "contacts",
                table: "Employee",
                newName: "IX_Employee_DealerId");

            migrationBuilder.AlterColumn<string>(
                name: "DealerId",
                schema: "contacts",
                table: "Employee",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "EmployeeType",
                schema: "contacts",
                table: "Employee",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Employee",
                schema: "contacts",
                table: "Employee",
                column: "EmployeeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Dealer_DealerId",
                schema: "contacts",
                table: "Employee",
                column: "DealerId",
                principalSchema: "contacts",
                principalTable: "Dealer",
                principalColumn: "DealerId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Individual_IndividualId",
                schema: "contacts",
                table: "Employee",
                column: "IndividualId",
                principalSchema: "contacts",
                principalTable: "Individual",
                principalColumn: "IndividualId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Job_JobId",
                schema: "contacts",
                table: "Employee",
                column: "JobId",
                principalSchema: "contacts",
                principalTable: "Job",
                principalColumn: "JobId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Organization_OrganizationId",
                schema: "contacts",
                table: "Employee",
                column: "OrganizationId",
                principalSchema: "contacts",
                principalTable: "Organization",
                principalColumn: "OrganizationId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
