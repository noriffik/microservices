﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_DealerPrivateCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PrivateCustomer_Company_CompanyId",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_PrivateCustomer_Person_ContactableId",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.AddColumn<string>(
                name: "PrivateCustomerType",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "DealerId",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PrivateCustomer_DealerId",
                schema: "contacts",
                table: "PrivateCustomer",
                column: "DealerId");

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateCustomer_Company_CompanyId",
                schema: "contacts",
                table: "PrivateCustomer",
                column: "CompanyId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateCustomer_Person_ContactableId",
                schema: "contacts",
                table: "PrivateCustomer",
                column: "ContactableId",
                principalSchema: "contacts",
                principalTable: "Person",
                principalColumn: "PersonId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateCustomer_Dealer_DealerId",
                schema: "contacts",
                table: "PrivateCustomer",
                column: "DealerId",
                principalSchema: "contacts",
                principalTable: "Dealer",
                principalColumn: "DealerId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PrivateCustomer_Company_CompanyId",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_PrivateCustomer_Person_ContactableId",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropForeignKey(
                name: "FK_PrivateCustomer_Dealer_DealerId",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropIndex(
                name: "IX_PrivateCustomer_DealerId",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "PrivateCustomerType",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "DealerId",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateCustomer_Company_CompanyId",
                schema: "contacts",
                table: "PrivateCustomer",
                column: "CompanyId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PrivateCustomer_Person_ContactableId",
                schema: "contacts",
                table: "PrivateCustomer",
                column: "ContactableId",
                principalSchema: "contacts",
                principalTable: "Person",
                principalColumn: "PersonId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
