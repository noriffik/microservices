﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_FamilyInfo_ToIndividual : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Children",
                schema: "contacts",
                table: "Individual",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FamilyStatus",
                schema: "contacts",
                table: "Individual",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Children",
                schema: "contacts",
                table: "Individual");

            migrationBuilder.DropColumn(
                name: "FamilyStatus",
                schema: "contacts",
                table: "Individual");
        }
    }
}
