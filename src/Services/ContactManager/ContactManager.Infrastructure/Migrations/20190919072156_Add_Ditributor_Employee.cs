﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_Ditributor_Employee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DistributorEmployee",
                schema: "contacts",
                columns: table => new
                {
                    EmployeeId = table.Column<int>(nullable: false),
                    IndividualId = table.Column<int>(nullable: false),
                    OrganizationId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: false),
                    DistributorId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DistributorEmployee", x => x.EmployeeId);
                    table.ForeignKey(
                        name: "FK_DistributorEmployee_Distributor_DistributorId",
                        column: x => x.DistributorId,
                        principalSchema: "contacts",
                        principalTable: "Distributor",
                        principalColumn: "DistributorId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DistributorEmployee_Individual_IndividualId",
                        column: x => x.IndividualId,
                        principalSchema: "contacts",
                        principalTable: "Individual",
                        principalColumn: "IndividualId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DistributorEmployee_Job_JobId",
                        column: x => x.JobId,
                        principalSchema: "contacts",
                        principalTable: "Job",
                        principalColumn: "JobId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DistributorEmployee_Organization_OrganizationId",
                        column: x => x.OrganizationId,
                        principalSchema: "contacts",
                        principalTable: "Organization",
                        principalColumn: "OrganizationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DistributorEmployee_DistributorId",
                schema: "contacts",
                table: "DistributorEmployee",
                column: "DistributorId");

            migrationBuilder.CreateIndex(
                name: "IX_DistributorEmployee_IndividualId",
                schema: "contacts",
                table: "DistributorEmployee",
                column: "IndividualId");

            migrationBuilder.CreateIndex(
                name: "IX_DistributorEmployee_JobId",
                schema: "contacts",
                table: "DistributorEmployee",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_DistributorEmployee_OrganizationId_IndividualId_JobId",
                schema: "contacts",
                table: "DistributorEmployee",
                columns: new[] { "OrganizationId", "IndividualId", "JobId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DistributorEmployee",
                schema: "contacts");
        }
    }
}
