﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_Importer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Importer",
                schema: "contacts",
                columns: table => new
                {
                    ImporterId = table.Column<string>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Importer", x => x.ImporterId);
                    table.ForeignKey(
                        name: "FK_Importer_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "contacts",
                        principalTable: "Company",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Importer_CompanyId",
                schema: "contacts",
                table: "Importer",
                column: "CompanyId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Importer",
                schema: "contacts");
        }
    }
}
