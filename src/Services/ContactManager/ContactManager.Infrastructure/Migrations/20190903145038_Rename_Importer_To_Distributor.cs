﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Rename_Importer_To_Distributor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dealer_Importer_ImporterId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.DropTable(
                name: "Importer",
                schema: "contacts");

            migrationBuilder.DropIndex(
                name: "IX_Dealer_ImporterId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.DropColumn(
                name: "ImporterId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.AddColumn<string>(
                name: "DistributorId",
                schema: "contacts",
                table: "Dealer",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Distributor",
                schema: "contacts",
                columns: table => new
                {
                    DistributorId = table.Column<string>(nullable: false),
                    OrganizationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Distributor", x => x.DistributorId);
                    table.ForeignKey(
                        name: "FK_Distributor_Organization_OrganizationId",
                        column: x => x.OrganizationId,
                        principalSchema: "contacts",
                        principalTable: "Organization",
                        principalColumn: "OrganizationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Dealer_DistributorId",
                schema: "contacts",
                table: "Dealer",
                column: "DistributorId");

            migrationBuilder.CreateIndex(
                name: "IX_Distributor_OrganizationId",
                schema: "contacts",
                table: "Distributor",
                column: "OrganizationId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Dealer_Distributor_DistributorId",
                schema: "contacts",
                table: "Dealer",
                column: "DistributorId",
                principalSchema: "contacts",
                principalTable: "Distributor",
                principalColumn: "DistributorId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dealer_Distributor_DistributorId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.DropTable(
                name: "Distributor",
                schema: "contacts");

            migrationBuilder.DropIndex(
                name: "IX_Dealer_DistributorId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.DropColumn(
                name: "DistributorId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.AddColumn<string>(
                name: "ImporterId",
                schema: "contacts",
                table: "Dealer",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Importer",
                schema: "contacts",
                columns: table => new
                {
                    ImporterId = table.Column<string>(nullable: false),
                    OrganizationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Importer", x => x.ImporterId);
                    table.ForeignKey(
                        name: "FK_Importer_Organization_OrganizationId",
                        column: x => x.OrganizationId,
                        principalSchema: "contacts",
                        principalTable: "Organization",
                        principalColumn: "OrganizationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Dealer_ImporterId",
                schema: "contacts",
                table: "Dealer",
                column: "ImporterId");

            migrationBuilder.CreateIndex(
                name: "IX_Importer_OrganizationId",
                schema: "contacts",
                table: "Importer",
                column: "OrganizationId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Dealer_Importer_ImporterId",
                schema: "contacts",
                table: "Dealer",
                column: "ImporterId",
                principalSchema: "contacts",
                principalTable: "Importer",
                principalColumn: "ImporterId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
