﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_LeadInterstSource : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_LeadInterestSource",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "LeadInterestSource",
                schema: "contacts",
                columns: table => new
                {
                    LeadInterestSourceId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadInterestSource", x => x.LeadInterestSourceId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LeadInterestSource",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_LeadInterestSource",
                schema: "contacts");
        }
    }
}
