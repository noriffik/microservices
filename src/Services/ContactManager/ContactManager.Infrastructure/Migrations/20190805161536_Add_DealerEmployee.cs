﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_DealerEmployee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Company_CompanyId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Job_JobId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Person_PersonId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.AddColumn<string>(
                name: "DealerId",
                schema: "contacts",
                table: "Employee",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmployeeType",
                schema: "contacts",
                table: "Employee",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_DealerId",
                schema: "contacts",
                table: "Employee",
                column: "DealerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Dealer_DealerId",
                schema: "contacts",
                table: "Employee",
                column: "DealerId",
                principalSchema: "contacts",
                principalTable: "Dealer",
                principalColumn: "DealerId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Company_CompanyId",
                schema: "contacts",
                table: "Employee",
                column: "CompanyId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Job_JobId",
                schema: "contacts",
                table: "Employee",
                column: "JobId",
                principalSchema: "contacts",
                principalTable: "Job",
                principalColumn: "JobId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Person_PersonId",
                schema: "contacts",
                table: "Employee",
                column: "PersonId",
                principalSchema: "contacts",
                principalTable: "Person",
                principalColumn: "PersonId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Dealer_DealerId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Company_CompanyId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Job_JobId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Person_PersonId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropIndex(
                name: "IX_Employee_DealerId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "DealerId",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "EmployeeType",
                schema: "contacts",
                table: "Employee");

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Company_CompanyId",
                schema: "contacts",
                table: "Employee",
                column: "CompanyId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Job_JobId",
                schema: "contacts",
                table: "Employee",
                column: "JobId",
                principalSchema: "contacts",
                principalTable: "Job",
                principalColumn: "JobId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Person_PersonId",
                schema: "contacts",
                table: "Employee",
                column: "PersonId",
                principalSchema: "contacts",
                principalTable: "Person",
                principalColumn: "PersonId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
