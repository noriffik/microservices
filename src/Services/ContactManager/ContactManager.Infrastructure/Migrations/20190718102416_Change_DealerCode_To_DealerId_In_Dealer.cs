﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Change_DealerCode_To_DealerId_In_Dealer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dealer_Company_CompanyId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.DropForeignKey(
                name: "FK_Importer_Company_CompanyId",
                schema: "contacts",
                table: "Importer");

            migrationBuilder.DropIndex(
                name: "IX_Dealer_CompanyId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.AddColumn<string>(
                name: "DealerCode",
                schema: "contacts",
                table: "Dealer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ImporterId",
                schema: "contacts",
                table: "Dealer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Dealer_CompanyId",
                schema: "contacts",
                table: "Dealer",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Dealer_ImporterId",
                schema: "contacts",
                table: "Dealer",
                column: "ImporterId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dealer_Company_CompanyId",
                schema: "contacts",
                table: "Dealer",
                column: "CompanyId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Dealer_Importer_ImporterId",
                schema: "contacts",
                table: "Dealer",
                column: "ImporterId",
                principalSchema: "contacts",
                principalTable: "Importer",
                principalColumn: "ImporterId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Importer_Company_CompanyId",
                schema: "contacts",
                table: "Importer",
                column: "CompanyId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dealer_Company_CompanyId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.DropForeignKey(
                name: "FK_Dealer_Importer_ImporterId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.DropForeignKey(
                name: "FK_Importer_Company_CompanyId",
                schema: "contacts",
                table: "Importer");

            migrationBuilder.DropIndex(
                name: "IX_Dealer_CompanyId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.DropIndex(
                name: "IX_Dealer_ImporterId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.DropColumn(
                name: "DealerCode",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.DropColumn(
                name: "ImporterId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.CreateIndex(
                name: "IX_Dealer_CompanyId",
                schema: "contacts",
                table: "Dealer",
                column: "CompanyId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Dealer_Company_CompanyId",
                schema: "contacts",
                table: "Dealer",
                column: "CompanyId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Importer_Company_CompanyId",
                schema: "contacts",
                table: "Importer",
                column: "CompanyId",
                principalSchema: "contacts",
                principalTable: "Company",
                principalColumn: "CompanyId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
