﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Extract_Dealer_To_Aggregate_Root : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dealer_Importer_ImporterId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.AddForeignKey(
                name: "FK_Dealer_Importer_ImporterId",
                schema: "contacts",
                table: "Dealer",
                column: "ImporterId",
                principalSchema: "contacts",
                principalTable: "Importer",
                principalColumn: "ImporterId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dealer_Importer_ImporterId",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.AddForeignKey(
                name: "FK_Dealer_Importer_ImporterId",
                schema: "contacts",
                table: "Dealer",
                column: "ImporterId",
                principalSchema: "contacts",
                principalTable: "Importer",
                principalColumn: "ImporterId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
