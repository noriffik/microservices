﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Change_DealerId_Type_To_DealerCode_Stage_1_Drop : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Dealer",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_Dealer",
                schema: "contacts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_Dealer",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Dealer",
                schema: "contacts",
                columns: table => new
                {
                    DealerId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    Since = table.Column<DateTime>(nullable: false),
                    Till = table.Column<DateTime>(nullable: true),
                    Sales = table.Column<bool>(nullable: false),
                    Service = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dealer", x => x.DealerId);
                    table.ForeignKey(
                        name: "FK_Dealer_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "contacts",
                        principalTable: "Company",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Dealer_Code",
                schema: "contacts",
                table: "Dealer",
                column: "Code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Dealer_CompanyId",
                schema: "contacts",
                table: "Dealer",
                column: "CompanyId",
                unique: true);
        }
    }
}
