﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_Passport_To_Person : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "PassportIssuedOn",
                schema: "contacts",
                table: "Person",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PassportIssuer",
                schema: "contacts",
                table: "Person",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PassportNumber",
                schema: "contacts",
                table: "Person",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PassportSeries",
                schema: "contacts",
                table: "Person",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PassportIssuedOn",
                schema: "contacts",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "PassportIssuer",
                schema: "contacts",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "PassportNumber",
                schema: "contacts",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "PassportSeries",
                schema: "contacts",
                table: "Person");
        }
    }
}
