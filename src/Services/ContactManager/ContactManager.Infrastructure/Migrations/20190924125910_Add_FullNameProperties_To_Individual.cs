﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_FullNameProperties_To_Individual : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FullName_FirstNameFirst",
                schema: "contacts",
                table: "Individual",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FullName_LastNameFirst",
                schema: "contacts",
                table: "Individual",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullName_FirstNameFirst",
                schema: "contacts",
                table: "Individual");

            migrationBuilder.DropColumn(
                name: "FullName_LastNameFirst",
                schema: "contacts",
                table: "Individual");
        }
    }
}
