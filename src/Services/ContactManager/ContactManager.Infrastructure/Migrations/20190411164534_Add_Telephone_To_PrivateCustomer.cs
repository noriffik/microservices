﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_Telephone_To_PrivateCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AreaCode",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CountryCode",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "E123",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LocalNumber",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Number",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Type",
                schema: "contacts",
                table: "PrivateCustomer",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AreaCode",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "CountryCode",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "E123",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "LocalNumber",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "Number",
                schema: "contacts",
                table: "PrivateCustomer");

            migrationBuilder.DropColumn(
                name: "Type",
                schema: "contacts",
                table: "PrivateCustomer");
        }
    }
}
