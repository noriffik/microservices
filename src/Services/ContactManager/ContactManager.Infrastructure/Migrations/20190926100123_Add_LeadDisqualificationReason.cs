﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_LeadDisqualificationReason : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_LeadDisqualificationReason",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "LeadDisqualificationReason",
                schema: "contacts",
                columns: table => new
                {
                    LeadDisqualificationReasonId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadDisqualificationReason", x => x.LeadDisqualificationReasonId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LeadDisqualificationReason",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_LeadDisqualificationReason",
                schema: "contacts");
        }
    }
}
