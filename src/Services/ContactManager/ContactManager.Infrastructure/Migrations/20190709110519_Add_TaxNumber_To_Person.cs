﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_TaxNumber_To_Person : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TaxNumber",
                schema: "contacts",
                table: "Person",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "TaxNumberRegisteredOn",
                schema: "contacts",
                table: "Person",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "TaxNumberIssuedOn",
                schema: "contacts",
                table: "Person",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TaxNumberIssuer",
                schema: "contacts",
                table: "Person",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TaxNumber",
                schema: "contacts",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "TaxNumberRegisteredOn",
                schema: "contacts",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "TaxNumberIssuedOn",
                schema: "contacts",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "TaxNumberIssuer",
                schema: "contacts",
                table: "Person");
        }
    }
}
