﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_CityCode_ToDealer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sales",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.DropColumn(
                name: "Service",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.DropColumn(
                name: "PeriodFrom",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.DropColumn(
                name: "PeriodTo",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.AddColumn<string>(
                name: "CityCode",
                schema: "contacts",
                table: "Dealer",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CityCode",
                schema: "contacts",
                table: "Dealer");

            migrationBuilder.AddColumn<bool>(
                name: "Sales",
                schema: "contacts",
                table: "Dealer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Service",
                schema: "contacts",
                table: "Dealer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "PeriodFrom",
                schema: "contacts",
                table: "Dealer",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "PeriodTo",
                schema: "contacts",
                table: "Dealer",
                nullable: true);
        }
    }
}
