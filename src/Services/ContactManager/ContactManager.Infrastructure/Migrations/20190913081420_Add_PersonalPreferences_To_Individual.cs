﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Add_PersonalPreferences_To_Individual : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_PersonalPreferences",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.AddColumn<int>(
                name: "PreferencesId",
                schema: "contacts",
                table: "Individual",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PersonalPreferences",
                schema: "contacts",
                columns: table => new
                {
                    PersonalPreferencesId = table.Column<int>(nullable: false),
                    Language = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonalPreferences", x => x.PersonalPreferencesId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Individual_PreferencesId",
                schema: "contacts",
                table: "Individual",
                column: "PreferencesId");

            migrationBuilder.AddForeignKey(
                name: "FK_Individual_PersonalPreferences_PreferencesId",
                schema: "contacts",
                table: "Individual",
                column: "PreferencesId",
                principalSchema: "contacts",
                principalTable: "PersonalPreferences",
                principalColumn: "PersonalPreferencesId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Individual_PersonalPreferences_PreferencesId",
                schema: "contacts",
                table: "Individual");

            migrationBuilder.DropTable(
                name: "PersonalPreferences",
                schema: "contacts");

            migrationBuilder.DropIndex(
                name: "IX_Individual_PreferencesId",
                schema: "contacts",
                table: "Individual");

            migrationBuilder.DropSequence(
                name: "sq_PersonalPreferences",
                schema: "contacts");

            migrationBuilder.DropColumn(
                name: "PreferencesId",
                schema: "contacts",
                table: "Individual");
        }
    }
}
