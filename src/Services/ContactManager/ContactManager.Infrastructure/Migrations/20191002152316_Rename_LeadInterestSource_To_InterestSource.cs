﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Rename_LeadInterestSource_To_InterestSource : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LeadInterestSource",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_LeadInterestSource",
                schema: "contacts");

            migrationBuilder.CreateSequence(
                name: "sq_InterestSource",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "InterestSource",
                schema: "contacts",
                columns: table => new
                {
                    InterestSourceId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    DateArchived = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InterestSource", x => x.InterestSourceId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InterestSource",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_InterestSource",
                schema: "contacts");

            migrationBuilder.CreateSequence(
                name: "sq_LeadInterestSource",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "LeadInterestSource",
                schema: "contacts",
                columns: table => new
                {
                    LeadInterestSourceId = table.Column<int>(nullable: false),
                    DateArchived = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadInterestSource", x => x.LeadInterestSourceId);
                });
        }
    }
}
