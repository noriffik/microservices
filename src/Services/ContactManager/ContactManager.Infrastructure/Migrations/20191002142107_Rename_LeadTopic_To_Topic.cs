﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.ContactManager.Infrastructure.Migrations
{
    public partial class Rename_LeadTopic_To_Topic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lead_LeadTopic_TopicId",
                schema: "contacts",
                table: "Lead");

            migrationBuilder.DropTable(
                name: "LeadTopic",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_LeadTopic",
                schema: "contacts");

            migrationBuilder.CreateSequence(
                name: "sq_Topic",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Topic",
                schema: "contacts",
                columns: table => new
                {
                    TopicId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Topic", x => x.TopicId);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Lead_Topic_TopicId",
                schema: "contacts",
                table: "Lead",
                column: "TopicId",
                principalSchema: "contacts",
                principalTable: "Topic",
                principalColumn: "TopicId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lead_Topic_TopicId",
                schema: "contacts",
                table: "Lead");

            migrationBuilder.DropTable(
                name: "Topic",
                schema: "contacts");

            migrationBuilder.DropSequence(
                name: "sq_Topic",
                schema: "contacts");

            migrationBuilder.CreateSequence(
                name: "sq_LeadTopic",
                schema: "contacts",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "LeadTopic",
                schema: "contacts",
                columns: table => new
                {
                    LeadTopicId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadTopic", x => x.LeadTopicId);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Lead_LeadTopic_TopicId",
                schema: "contacts",
                table: "Lead",
                column: "TopicId",
                principalSchema: "contacts",
                principalTable: "LeadTopic",
                principalColumn: "LeadTopicId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
