﻿using System;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Infrastructure.Exceptions
{
    public class DuplicateCorporateCustomerException : Exception
    {
        protected DuplicateCorporateCustomerException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public DuplicateCorporateCustomerException(string message) : base(message)
        {
        }

        public DuplicateCorporateCustomerException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
