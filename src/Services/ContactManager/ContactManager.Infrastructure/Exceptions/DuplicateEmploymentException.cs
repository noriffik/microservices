﻿using System;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Infrastructure.Exceptions
{
    public class DuplicateEmploymentException : Exception
    {
        protected DuplicateEmploymentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public DuplicateEmploymentException(string message) : base(message)
        {
        }

        public DuplicateEmploymentException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
