﻿using NexCore.Infrastructure;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Infrastructure
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Used by EF tool")]
    public class ContactContextDesignTimeFactory : DbContextBaseDesignFactory<ContactContext>
    {
        public ContactContextDesignTimeFactory() : base("ConnectionString")
        {
        }
    }
}
