﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Organizations;

namespace NexCore.ContactManager.Infrastructure.EntityTypes
{
    public class DealerEntityTypeConfiguration : IEntityTypeConfiguration<Dealer>
    {
        public void Configure(EntityTypeBuilder<Dealer> builder)
        {
            builder.ToTable(nameof(Dealer), ContactContext.Schema);

            builder.Property(x => x.Id)
                .HasConversion(x => x.Value, x => x)
                .HasColumnName(typeof(Dealer).Name + "Id")
                .IsRequired();

            builder.Property(c => c.DistributorId).HasConversion(x => x.Value, x => x);
            builder.Property(c => c.DealerCode).HasConversion(x => x.Value, x => x);

            builder.HasOne<Organization>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(d => d.OrganizationId);

            builder.Property(c => c.OrganizationId).HasColumnName(nameof(Dealer.OrganizationId));

            builder.HasOne<Distributor>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(d => d.DistributorId);
        }
    }
}
