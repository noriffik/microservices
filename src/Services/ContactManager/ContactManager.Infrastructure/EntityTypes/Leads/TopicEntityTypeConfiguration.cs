﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.Leads;

namespace NexCore.ContactManager.Infrastructure.EntityTypes.Leads
{
    public class TopicEntityTypeConfiguration : IEntityTypeConfiguration<Topic>
    {
        public void Configure(EntityTypeBuilder<Topic> builder)
        {
            builder.ToTable(nameof(Topic), ContactContext.Schema);

            builder.HasKey(j => j.Id);
            builder.Property(j => j.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Topic), ContactContext.Schema)
                .HasColumnName(nameof(Topic) + "Id");

            builder.Property(j => j.Name).IsRequired();
        }
    }
}
