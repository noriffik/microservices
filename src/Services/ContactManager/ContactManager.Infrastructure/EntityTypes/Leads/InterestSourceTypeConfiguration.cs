﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.Leads;

namespace NexCore.ContactManager.Infrastructure.EntityTypes.Leads
{
    public class InterestSourceTypeConfiguration : IEntityTypeConfiguration<InterestSource>
    {
        public void Configure(EntityTypeBuilder<InterestSource> builder)
        {
            builder.ToTable(nameof(InterestSource), ContactContext.Schema);

            builder.HasKey(j => j.Id);
            builder.Property(j => j.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(InterestSource), ContactContext.Schema)
                .HasColumnName(nameof(InterestSource) + "Id");

            builder.Property(j => j.Name).IsRequired();
        }
    }
}
