﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.Leads;

namespace NexCore.ContactManager.Infrastructure.EntityTypes.Leads
{
    public class DisqualificationReasonTypeConfiguration : IEntityTypeConfiguration<DisqualificationReason>
    {
        public void Configure(EntityTypeBuilder<DisqualificationReason> builder)
        {
            builder.ToTable(nameof(DisqualificationReason), ContactContext.Schema);

            builder.HasKey(j => j.Id);
            builder.Property(j => j.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(DisqualificationReason), ContactContext.Schema)
                .HasColumnName(nameof(DisqualificationReason) + "Id");

            builder.Property(j => j.Name).IsRequired();
        }
    }
}
