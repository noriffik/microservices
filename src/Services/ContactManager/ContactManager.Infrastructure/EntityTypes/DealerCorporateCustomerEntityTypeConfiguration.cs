﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.DealerCorporateCustomers;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Organizations;

namespace NexCore.ContactManager.Infrastructure.EntityTypes
{
    public class DealerCorporateCustomerEntityTypeConfiguration : IEntityTypeConfiguration<DealerCorporateCustomer>
    {
        public void Configure(EntityTypeBuilder<DealerCorporateCustomer> builder)
        {
            builder.ToTable(nameof(DealerCorporateCustomer), ContactContext.Schema);

            builder.HasKey(e => e.Id);
            builder.HasIndex(e => new { e.ContactableId, e.OrganizationId })
                .IsUnique();

            builder.Property(e => e.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(DealerCorporateCustomer), ContactContext.Schema)
                .HasColumnName(nameof(DealerCorporateCustomer) + "Id");

            builder.HasOne<Organization>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.ContactableId);

            builder.HasOne<Organization>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.OrganizationId);

            builder.Property(c => c.ContactableId).HasColumnName(nameof(DealerCorporateCustomer.ContactableId));
            builder.Property(c => c.OrganizationId).HasColumnName(nameof(DealerCorporateCustomer.OrganizationId));

            builder.Property(x => x.DealerId)
                .HasConversion(x => x.Value, x => x)
                .IsRequired();

            builder.HasOne<Dealer>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.DealerId);
        }
    }
}
