﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.Dictionaries;

namespace NexCore.ContactManager.Infrastructure.EntityTypes
{
    public class JobEntityTypeConfiguration : IEntityTypeConfiguration<Job>
    {
        public void Configure(EntityTypeBuilder<Job> builder)
        {
            builder.ToTable(nameof(Job), ContactContext.Schema);

            builder.HasKey(j => j.Id);
            builder.Property(j => j.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Job), ContactContext.Schema)
                .HasColumnName(nameof(Job) + "Id");

            builder.Property(j => j.Title).HasColumnName(nameof(Job.Title));
        }
    }
}
