﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Infrastructure.Extensions;

namespace NexCore.ContactManager.Infrastructure.EntityTypes
{
    public class ContactPointEntityTypeConfiguration
        : IEntityTypeConfiguration<ContactPoint<PhysicalAddress>>
        , IEntityTypeConfiguration<ContactPoint<Telephone>>
        , IEntityTypeConfiguration<ContactPoint<EmailAddress>>
    {
        public void Configure(EntityTypeBuilder<ContactPoint<PhysicalAddress>> builder)
        {
            builder.ToTable(nameof(PhysicalAddress), ContactContext.Schema);

            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(PhysicalAddress), ContactContext.Schema)
                .HasColumnName(nameof(PhysicalAddress) + "Id");

            builder.OwnsPhysicalAddress(c => c.Value);

            builder.HasOne<ContactInfo>()
                .WithMany("PhysicalAddressesInternal")
                .OnDelete(DeleteBehavior.Cascade);
        }

        public void Configure(EntityTypeBuilder<ContactPoint<Telephone>> builder)
        {
            builder.ToTable(nameof(Telephone), ContactContext.Schema);

            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Telephone), ContactContext.Schema)
                .HasColumnName(nameof(Telephone) + "Id");

            builder.OwnsOne(c => c.Value, v =>
            {
                v.Property(p => p.CountryCode).HasColumnName(nameof(Telephone.CountryCode));
                v.Property(p => p.AreaCode).HasColumnName(nameof(Telephone.AreaCode));
                v.Property(p => p.LocalNumber).HasColumnName(nameof(Telephone.LocalNumber));
                v.Property(p => p.Type).HasColumnName(nameof(Telephone.Type));
                v.Property(p => p.Number).HasColumnName(nameof(Telephone.Number));
                v.Property(p => p.E123).HasColumnName(nameof(Telephone.E123));
            });

            builder.HasOne<ContactInfo>()
                .WithMany("TelephonesInternal")
                .OnDelete(DeleteBehavior.Cascade);
        }

        public void Configure(EntityTypeBuilder<ContactPoint<EmailAddress>> builder)
        {
            builder.ToTable(nameof(EmailAddress), ContactContext.Schema);

            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(EmailAddress), ContactContext.Schema)
                .HasColumnName(nameof(EmailAddress) + "Id");

            builder.OwnsOne(c => c.Value, v =>
            {
                v.Property(p => p.Address).HasColumnName(nameof(EmailAddress.Address));
            });

            builder.HasOne<ContactInfo>()
                .WithMany("EmailAddressesInternal")
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
