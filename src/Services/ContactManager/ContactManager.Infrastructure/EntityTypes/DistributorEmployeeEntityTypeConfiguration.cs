﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.DistributorEmployees;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Employees;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Organizations;

namespace NexCore.ContactManager.Infrastructure.EntityTypes
{
    public class DistributorEmployeeEntityTypeConfiguration : IEntityTypeConfiguration<DistributorEmployee>
    {
        public void Configure(EntityTypeBuilder<DistributorEmployee> builder)
        {
            builder.ToTable(nameof(DistributorEmployee), ContactContext.Schema);

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Employee), ContactContext.Schema)
                .HasColumnName(nameof(Employee) + "Id");

            builder.HasIndex(e => new { e.OrganizationId, e.IndividualId, e.JobId })
                .IsUnique();

            builder.Property(c => c.IndividualId).IsRequired();
            builder.Property(c => c.OrganizationId).IsRequired();
            builder.Property(c => c.JobId).IsRequired();
            builder.Property(x => x.DistributorId).HasConversion(x => x.Value, x => x).IsRequired();

            builder.HasOne<Individual>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.IndividualId);
            builder.HasOne<Organization>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.OrganizationId);
            builder.HasOne<Job>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.JobId);
            builder.HasOne<Distributor>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.DistributorId);
        }
    }
}
