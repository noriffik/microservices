﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.Employees;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Organizations;

namespace NexCore.ContactManager.Infrastructure.EntityTypes
{
    class DealerEmployeeEntityTypeConfiguration : IEntityTypeConfiguration<DealerEmployee>
    {
        public void Configure(EntityTypeBuilder<DealerEmployee> builder)
        {
            builder.ToTable(nameof(DealerEmployee), ContactContext.Schema);

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Employee), ContactContext.Schema)
                .HasColumnName(nameof(Employee) + "Id");

            builder.HasIndex(e => new { e.OrganizationId, e.IndividualId, e.JobId })
                .IsUnique();

            builder.Property(c => c.IndividualId).IsRequired();
            builder.Property(c => c.OrganizationId).IsRequired();
            builder.Property(c => c.JobId).IsRequired();
            builder.Property(x => x.DealerId).HasConversion(x => x.Value, x => x).IsRequired();

            builder.HasOne<Individual>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.IndividualId);
            builder.HasOne<Organization>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.OrganizationId);
            builder.HasOne<Job>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.JobId);
            builder.HasOne<Dealer>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.DealerId);
        }
    }
}
