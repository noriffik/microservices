﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.Legal;

namespace NexCore.ContactManager.Infrastructure.EntityTypes
{
    public class IndividualEntityTypeConfiguration : IEntityTypeConfiguration<Individual>
    {
        public void Configure(EntityTypeBuilder<Individual> builder)
        {
            builder.ToTable(nameof(Individual), ContactContext.Schema);

            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Individual), ContactContext.Schema)
                .HasColumnName(nameof(Individual) + "Id");

            builder.OwnsOne(p => p.Name, n =>
            {
                n.Property(p => p.Firstname).HasColumnName(nameof(PersonName.Firstname));
                n.Property(p => p.Lastname).HasColumnName(nameof(PersonName.Lastname));
                n.Property(p => p.Middlename).HasColumnName(nameof(PersonName.Middlename));
                n.Property<string>("FullFirstNameFirst").HasColumnName("FullName_FirstNameFirst");
                n.Property<string>("FullLastNameFirst").HasColumnName("FullName_LastNameFirst");
            });

            builder.OwnsOne(p => p.Passport, n =>
            {
                n.Property(p => p.Series).HasColumnName(nameof(Passport) + nameof(Passport.Series));
                n.Property(p => p.Number).HasColumnName(nameof(Passport) + nameof(Passport.Number));
                n.OwnsOne(p => p.Issue, i =>
                {
                    i.Property(p => p.Issuer).HasColumnName(nameof(Passport) + nameof(Issue.Issuer));
                    i.Property(p => p.IssuedOn).HasColumnName(nameof(Passport) + nameof(Issue.IssuedOn));
                });
            });

            builder.OwnsOne(p => p.TaxNumber, n =>
            {
                n.Property(p => p.Number).HasColumnName(nameof(Individual.TaxNumber));
                n.Property(p => p.RegisteredOn).HasColumnName(nameof(Individual.TaxNumber) + nameof(Individual.TaxNumber.RegisteredOn));
                n.OwnsOne(p => p.Issue, i =>
                {
                    i.Property(p => p.Issuer).HasColumnName(nameof(Individual.TaxNumber) + nameof(Issue.Issuer));
                    i.Property(p => p.IssuedOn).HasColumnName(nameof(Individual.TaxNumber) + nameof(Issue.IssuedOn));
                });
            });

            builder.Ignore(m => m.FamilyStatus);

            builder.Property(m => m.FamilyStatus)
                .HasConversion(f => f.Id, f => FamilyStatus.Get(f));

            var personalPreferences = builder.Metadata.FindNavigation(nameof(Individual.Preferences));
            personalPreferences.IsEagerLoaded = true;

            var contactNavigation = builder.Metadata.FindNavigation(nameof(Individual.Contact));
            contactNavigation.IsEagerLoaded = true;
        }
    }
}
