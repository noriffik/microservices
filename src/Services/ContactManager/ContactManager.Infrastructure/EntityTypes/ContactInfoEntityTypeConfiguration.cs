﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.Contacts;

namespace NexCore.ContactManager.Infrastructure.EntityTypes
{
    public class ContactInfoEntityTypeConfiguration : IEntityTypeConfiguration<ContactInfo>
    {
        public void Configure(EntityTypeBuilder<ContactInfo> builder)
        {
            builder.ToTable(nameof(ContactInfo), ContactContext.Schema);

            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(ContactInfo), ContactContext.Schema)
                .HasColumnName(nameof(ContactInfo) + "Id");

            builder.Metadata.FindNavigation("PhysicalAddressesInternal").IsEagerLoaded = true;
            builder.Metadata.FindNavigation("TelephonesInternal").IsEagerLoaded = true;
            builder.Metadata.FindNavigation("EmailAddressesInternal").IsEagerLoaded = true;
        }
    }
}
