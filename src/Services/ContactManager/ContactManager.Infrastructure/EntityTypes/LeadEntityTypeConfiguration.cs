﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.Domain.Organizations;

namespace NexCore.ContactManager.Infrastructure.EntityTypes
{
    public class LeadEntityTypeConfiguration : IEntityTypeConfiguration<Lead>
    {
        public void Configure(EntityTypeBuilder<Lead> builder)
        {
            builder.ToTable(nameof(Lead), ContactContext.Schema);

            builder.HasKey(j => j.Id);
            builder.Property(j => j.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Lead), ContactContext.Schema)
                .HasColumnName(nameof(Lead) + "Id");

            builder.Property(x => x.DealerId).HasConversion(x => x.Value, x => x).IsRequired();
            builder.Property(x => x.DealerCode).HasConversion(x => x.Value, x => x).IsRequired();
            builder.Property(x => x.DistributorId).HasConversion(x => x.Value, x => x).IsRequired();

            builder.OwnsOne(l => l.Contact, n =>
                {
                    n.Property(c => c.IndividualId).HasColumnName(nameof(Contact.IndividualId));
                    n.Property(c => c.OrganizationId).HasColumnName(nameof(Contact.OrganizationId));
                    n.Property(c => c.Type).HasColumnName(nameof(Contact) + nameof(Contact.Type));

                    n.HasOne<Individual>()
                        .WithMany()
                        .OnDelete(DeleteBehavior.Restrict)
                        .HasForeignKey(c => c.IndividualId);
                    n.HasOne<Organization>()
                        .WithMany()
                        .OnDelete(DeleteBehavior.Restrict)
                        .HasForeignKey(c => c.OrganizationId);
                });

            builder.HasOne<Dealer>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.DealerId);
            builder.HasOne<Distributor>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.DistributorId);
            builder.HasOne<DealerEmployee>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.OwnerEmployeeId);
            builder.HasOne<Topic>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.TopicId);
        }
    }
}
