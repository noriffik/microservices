﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.Individuals;

namespace NexCore.ContactManager.Infrastructure.EntityTypes
{
    public class CommunicationPreferencesEntityTypeConfiguration : IEntityTypeConfiguration<CommunicationPreferences>
    {
        public void Configure(EntityTypeBuilder<CommunicationPreferences> builder)
        {
            builder.ToTable(nameof(CommunicationPreferences), ContactContext.Schema);

            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(CommunicationPreferences), ContactContext.Schema)
                .HasColumnName(nameof(CommunicationPreferences) + "Id");

            builder.Property(c => c.PreferredMedium)
                .HasConversion(m => m.Id, id => CommunicationMedium.Get(id))
                .IsRequired(false);

            builder.Property(c => c.BannedMediums)
                .HasConversion(m => m.AsString, ids => CommunicationMediumSet.Parse(ids));
        }
    }
}
