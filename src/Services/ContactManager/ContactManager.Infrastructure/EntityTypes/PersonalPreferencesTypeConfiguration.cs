﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.Individuals;

namespace NexCore.ContactManager.Infrastructure.EntityTypes
{
    public class PersonalPreferencesTypeConfiguration : IEntityTypeConfiguration<PersonalPreferences>
    {
        public void Configure(EntityTypeBuilder<PersonalPreferences> builder)
        {
            builder.ToTable(nameof(PersonalPreferences), ContactContext.Schema);

            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(PersonalPreferences), ContactContext.Schema)
                .HasColumnName(nameof(PersonalPreferences) + "Id");

            var communicationPreferences = builder.Metadata.FindNavigation(nameof(PersonalPreferences.Communications));
            communicationPreferences.IsEagerLoaded = true;
        }
    }
}
