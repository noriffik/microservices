﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.Legal;
using System;
using System.Linq;

namespace NexCore.ContactManager.Infrastructure.EntityTypes
{
    public class OrganizationEntityTypeConfiguration : IEntityTypeConfiguration<Organization>
    {
        public void Configure(EntityTypeBuilder<Organization> builder)
        {
            builder.ToTable(nameof(Organization), ContactContext.Schema);

            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Organization), ContactContext.Schema)
                .HasColumnName(nameof(Organization) + "Id");

            builder.Property(c => c.Name).HasColumnName(nameof(Organization.Name));

            builder.OwnsOne(c => c.Identity, i =>
            {
                i.Property(p => p.Edrpou).HasColumnName(nameof(Identity.Edrpou));
                i.Property(p => p.Ipn).HasColumnName(nameof(Identity.Ipn));
            });

            builder.OwnsOne(c => c.Requisites, r =>
            {
                r.OwnsOne(p => p.Name, n =>
                    {
                        n.Property(p => p.ShortName).HasColumnName(nameof(Organization.Requisites) + nameof(LegalOrganizationName.ShortName));
                        n.Property(p => p.FullName).HasColumnName(nameof(Organization.Requisites) + nameof(LegalOrganizationName.FullName));
                    });
                r.Property(p => p.Address).HasColumnName(nameof(Organization.Requisites) + nameof(OrganizationRequisites.Address));
                r.Property(p => p.BankRequisites).HasColumnName(nameof(OrganizationRequisites.BankRequisites));
                r.OwnsOne(p => p.Director, d =>
                {
                    d.Property(p => p.Firstname).HasColumnName(nameof(OrganizationRequisites.Director) + nameof(LegalPersonName.Firstname));
                    d.Property(p => p.Lastname).HasColumnName(nameof(OrganizationRequisites.Director) + nameof(LegalPersonName.Lastname));
                    d.Property(p => p.Middlename).HasColumnName(nameof(OrganizationRequisites.Director) + nameof(LegalPersonName.Middlename));
                });
                r.Property(p => p.Telephones).HasColumnName(nameof(Organization.Requisites) + nameof(OrganizationRequisites.Telephones))
                    .HasConversion(t => t.Any() ? t.Aggregate((a, b) => $"{a},{b}") : "", s => s.Split(',', StringSplitOptions.RemoveEmptyEntries));
            });

            builder.Metadata.FindNavigation(nameof(Organization.Identity)).IsEagerLoaded = true;

            var contactNavigation = builder.Metadata.FindNavigation(nameof(Organization.Contact));
            contactNavigation.IsEagerLoaded = true;
        }
    }
}

