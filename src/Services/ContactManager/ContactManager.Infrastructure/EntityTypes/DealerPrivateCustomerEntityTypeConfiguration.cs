﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure.Extensions;

namespace NexCore.ContactManager.Infrastructure.EntityTypes
{
    class DealerPrivateCustomerEntityTypeConfiguration : IEntityTypeConfiguration<DealerPrivateCustomer>
    {
        public void Configure(EntityTypeBuilder<DealerPrivateCustomer> builder)
        {
            builder.ToTable(nameof(DealerPrivateCustomer), ContactContext.Schema);

            builder.HasKey(e => e.Id);
            builder.HasIndex(e => new { e.ContactableId, e.OrganizationId })
                .IsUnique();

            builder.Property(e => e.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(DealerPrivateCustomer), ContactContext.Schema)
                .HasColumnName(nameof(DealerPrivateCustomer) + "Id");

            builder.HasOne<Individual>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.ContactableId);
            builder.HasOne<Organization>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.OrganizationId);

            builder.Property(c => c.ContactableId).HasColumnName(nameof(DealerPrivateCustomer.ContactableId));
            builder.Property(c => c.OrganizationId).HasColumnName(nameof(DealerPrivateCustomer.OrganizationId));

            builder.OwnsOne(c => c.Telephone, v =>
            {
                v.Property(p => p.CountryCode);
                v.Property(p => p.AreaCode);
                v.Property(p => p.LocalNumber);
                v.Property(p => p.Type);
                v.Property(p => p.Number);
                v.Property(p => p.E123);
            });

            builder.OwnsPhysicalAddress(c => c.PhysicalAddress, nameof(DealerPrivateCustomer.PhysicalAddress));

            builder.Property(x => x.DealerId)
                .HasConversion(x => x.Value, x => x)
                .IsRequired();

            builder.HasOne<Dealer>()
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(e => e.DealerId);
        }
    }
}
