﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Organizations;

namespace NexCore.ContactManager.Infrastructure.EntityTypes
{
    public class DistributorEntityTypeConfiguration : IEntityTypeConfiguration<Distributor>
    {
        public void Configure(EntityTypeBuilder<Distributor> builder)
        {
            builder.ToTable(nameof(Distributor), ContactContext.Schema);

            builder.Property(i => i.Id)
                .HasConversion(x => x.Value, x => x)
                .HasColumnName(typeof(Distributor).Name + "Id")
                .IsRequired();

            builder.HasOne<Organization>()
                .WithOne()
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey<Distributor>(d => d.OrganizationId)
                .IsRequired();
        }
    }
}
