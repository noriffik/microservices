﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.ContactManager.Domain.Contacts;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Infrastructure.Extensions
{
    static class EntityTypeBuilderExtensions
    {
        public static EntityTypeBuilder<TEntity> OwnsPhysicalAddress<TEntity>(
            this EntityTypeBuilder<TEntity> builder,
            Expression<Func<TEntity, PhysicalAddress>> navigationExpression,
            string prefix = null)
            where TEntity : class
        {
            prefix = string.IsNullOrEmpty(prefix) ? string.Empty : prefix + "_";

            return builder.OwnsOne(navigationExpression, v =>
            {
                v.OwnsOne(a => a.Street, s => s.Property(p => p.Value).HasColumnName(nameof(PhysicalAddress.Street)))
                    .OnDelete(DeleteBehavior.Cascade);

                v.OwnsOne(a => a.City, c => {
                        c.Property(p => p.Code).HasColumnName(prefix + nameof(PhysicalAddress.City) + "Code");
                        c.Property(p => p.Value).HasColumnName(prefix + nameof(PhysicalAddress.City));
                    })
                    .OnDelete(DeleteBehavior.Cascade);

                v.OwnsOne(a => a.Region, c => {
                        c.Property(p => p.Code).HasColumnName(prefix + nameof(PhysicalAddress.Region) + "Code");
                        c.Property(p => p.Value).HasColumnName(prefix + nameof(PhysicalAddress.Region));
                    })
                    .OnDelete(DeleteBehavior.Cascade);

                v.OwnsOne(a => a.Country, c => {
                        c.Property(p => p.Code).HasColumnName(prefix + nameof(PhysicalAddress.Country) + "Code");
                        c.Property(p => p.Value).HasColumnName(prefix + nameof(PhysicalAddress.Country));
                    })
                    .OnDelete(DeleteBehavior.Cascade);

                v.OwnsOne(a => a.Geolocation, c => {
                        c.Property(p => p.Latitude).HasColumnName(prefix + nameof(PhysicalAddress.Geolocation) + "Latitude");
                        c.Property(p => p.Longitude).HasColumnName(prefix + nameof(PhysicalAddress.Geolocation) + "Longitude");
                    })
                    .OnDelete(DeleteBehavior.Cascade);

                v.OwnsOne(a => a.PostCode, c => c.Property(p => p.Value).HasColumnName(prefix + nameof(PhysicalAddress.PostCode)))
                    .OnDelete(DeleteBehavior.Cascade);

                v.Property(p => p.FullAddress).HasColumnName(prefix + nameof(PhysicalAddress.FullAddress));
            });
        }
    }
}
