﻿using MediatR;
using System.Collections.Generic;

namespace NexCore.ContactManager.Application.Languages.Queries
{
    public class LanguageQuery : IRequest<IEnumerable<LanguageDto>>
    {
    }
}
