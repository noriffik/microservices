﻿using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Languages.Queries
{
    [DataContract]
    public class LanguageDto
    {
        [DataMember]
        public string ThreeLetterIsoCode { get; set; }

        [DataMember]
        public string TwoLetterIsoCode { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
