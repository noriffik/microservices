﻿using System;

namespace NexCore.ContactManager.Application.Providers
{
    public class InvalidCountryCodeException : Exception
    {
        public string CountryCode { get; }

        public InvalidCountryCodeException()
        {
        }

        public InvalidCountryCodeException(string message) : base(message)
        {
        }

        public InvalidCountryCodeException(string message, Exception inner) : base(message, inner)
        {
        }

        public InvalidCountryCodeException(string message, string countryCode) : this(message, countryCode, null)
        {
        }

        public InvalidCountryCodeException(string message, string countryCode, Exception inner) : base(message, inner)
        {
            CountryCode = countryCode;
        }
    }
}
