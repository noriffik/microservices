﻿using NexCore.ContactManager.Domain.Languages;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Providers
{
    public interface ILanguageProvider
    {
        Task<Language> GetByIso2(string code);

        Task<bool> IsValidIso2(string code);

        Task<IEnumerable<Language>> Get();
    }
}
