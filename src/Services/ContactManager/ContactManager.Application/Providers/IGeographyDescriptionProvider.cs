﻿namespace NexCore.ContactManager.Application.Providers
{
    public interface IGeographyDescriptionProvider
    {
        string GetCountryNameByIso2(string code);
        bool IsValidIso2(string code);
    }
}