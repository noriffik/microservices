﻿using System;

namespace NexCore.ContactManager.Application.Providers
{
    public class InvalidLanguageCodeException : Exception
    {
        public string LanguageCode { get; set; }

        public InvalidLanguageCodeException()
        {
        }

        public InvalidLanguageCodeException(string message) : base(message)
        {
        }

        public InvalidLanguageCodeException(string message, Exception inner) : base(message, inner)
        {
        }

        public InvalidLanguageCodeException(string message, string languageCode) : this(message, languageCode, null)
        {
        }

        public InvalidLanguageCodeException(string message, string languageCode, Exception inner) : base(message, inner)
        {
            LanguageCode = languageCode;
        }
    }
}
