﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Queries.InterestSources
{
    [DataContract]
    public class InterestSourceQuery : IRequest<IEnumerable<InterestSourceDto>>
    {
        [DataMember] 
        public string Name { get; set; }
    }
}
