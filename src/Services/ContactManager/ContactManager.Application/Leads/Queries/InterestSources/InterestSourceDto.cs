﻿using System;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Queries.InterestSources
{
    [DataContract]
    public class InterestSourceDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime? DateArchived { get; set; }
    }
}
