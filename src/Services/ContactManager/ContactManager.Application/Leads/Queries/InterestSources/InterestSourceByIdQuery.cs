﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Queries.InterestSources

{
    [DataContract]
    public class InterestSourceByIdQuery : IRequest<InterestSourceDto>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
