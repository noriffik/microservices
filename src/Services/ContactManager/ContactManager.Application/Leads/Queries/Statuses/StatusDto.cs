﻿using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Queries.Statuses
{
    [DataContract]
    public class StatusDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
