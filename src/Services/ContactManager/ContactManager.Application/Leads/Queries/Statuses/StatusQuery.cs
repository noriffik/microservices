﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Queries.Statuses
{
    [DataContract]
    public class StatusQuery : IRequest<IEnumerable<StatusDto>>
    {
    }
}
