﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Queries.Topics
{
    [DataContract]
    public class TopicByIdQuery : IRequest<TopicDto>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
