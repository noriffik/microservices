﻿using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Queries.Topics
{
    [DataContract]
    public class TopicDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
