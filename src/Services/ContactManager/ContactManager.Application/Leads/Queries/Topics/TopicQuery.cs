﻿using NexCore.Application.Queries;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Queries.Topics
{
    [DataContract]
    public class TopicQuery : PagedRequest<TopicDto>
    {
        [DataMember]
        public string Name { get; set; }
    }
}
