﻿using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Queries.DisqualificationReasons
{
    [DataContract]
    public class DisqualificationReasonDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
