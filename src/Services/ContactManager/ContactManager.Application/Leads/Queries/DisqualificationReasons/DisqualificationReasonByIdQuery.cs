﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Queries.DisqualificationReasons
{
    [DataContract]
    public class DisqualificationReasonByIdQuery : IRequest<DisqualificationReasonDto>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
