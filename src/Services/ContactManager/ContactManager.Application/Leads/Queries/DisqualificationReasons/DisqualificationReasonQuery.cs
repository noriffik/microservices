﻿using NexCore.Application.Queries;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Queries.DisqualificationReasons
{
    [DataContract]
    public class DisqualificationReasonQuery : PagedRequest<DisqualificationReasonDto>
    {
        [DataMember]
        public string Name { get; set; }
    }
}
