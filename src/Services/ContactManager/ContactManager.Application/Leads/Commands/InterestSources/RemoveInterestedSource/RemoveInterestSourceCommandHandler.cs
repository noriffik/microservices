﻿using MediatR;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Leads.Commands.InterestSources.RemoveInterestedSource
{
    public class RemoveInterestSourceCommandHandler : IRequestHandler<RemoveInterestSourceCommand, Unit>
    {
        private readonly IUnitOfWork _work;

        public RemoveInterestSourceCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(RemoveInterestSourceCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var interest = await _work.EntityRepository.Require<InterestSource>(request.Id, cancellationToken);

            if (interest.IsArchived)
                return Unit.Value;

            interest.Archive();

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
