﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Leads.Commands.InterestSources.RemoveInterestedSource
{
    public class RemoveInterestSourceCommandValidator : AbstractValidator<RemoveInterestSourceCommand>
    {
        public RemoveInterestSourceCommandValidator()
        {
            RuleFor(c => c.Id).GreaterThan(0);
        }
    }
}
