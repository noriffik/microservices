﻿using System.Runtime.Serialization;
using MediatR;

namespace NexCore.ContactManager.Application.Leads.Commands.InterestSources.RemoveInterestedSource
{
    [DataContract]
    public class RemoveInterestSourceCommand : IRequest
    {
        [DataMember]
        public int Id { get; set; }
    }
}
