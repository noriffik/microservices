﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Commands.InterestSources.AddInterestSource
{
    [DataContract]
    public class AddInterestSourceCommand : IRequest<int>
    {
        [DataMember]
        public string Name { get; set; }
    }
}
