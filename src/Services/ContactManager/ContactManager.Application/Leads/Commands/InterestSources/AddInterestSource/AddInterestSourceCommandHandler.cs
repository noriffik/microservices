﻿using AutoMapper;
using MediatR;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Leads.Commands.InterestSources.AddInterestSource
{
    public class AddInterestSourceCommandHandler : IRequestHandler<AddInterestSourceCommand, int>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public AddInterestSourceCommandHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<int> Handle(AddInterestSourceCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var interest = _mapper.Map<InterestSource>(request);

            await _work.EntityRepository.Add(interest);

            await _work.Commit(cancellationToken);

            return interest.Id;
        }
    }
}
