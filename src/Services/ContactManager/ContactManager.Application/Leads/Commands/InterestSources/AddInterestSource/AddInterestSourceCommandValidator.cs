﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Leads.Commands.InterestSources.AddInterestSource
{
    public class AddInterestSourceCommandValidator : AbstractValidator<AddInterestSourceCommand>
    {
        public AddInterestSourceCommandValidator()
        {
            RuleFor(c => c.Name).NotEmpty();
        }
    }
}
