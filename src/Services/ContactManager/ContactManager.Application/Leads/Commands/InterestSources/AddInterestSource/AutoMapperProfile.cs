﻿using AutoMapper;
using NexCore.ContactManager.Domain.Leads;

namespace NexCore.ContactManager.Application.Leads.Commands.InterestSources.AddInterestSource
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AddInterestSourceCommand, InterestSource>()
                .ConvertUsing(s => new InterestSource(s.Name));
        }
    }
}
