﻿using MediatR;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Leads.Commands.InterestSources.UpdateInterestSource
{
    public class UpdateInterestSourceCommandHandler : IRequestHandler<UpdateInterestSourceCommand, Unit>
    {
        private readonly IUnitOfWork _work;

        public UpdateInterestSourceCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(UpdateInterestSourceCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var interest = await _work.EntityRepository.Require<InterestSource>(request.Id, cancellationToken);

            interest.ChangeName(request.Name);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
