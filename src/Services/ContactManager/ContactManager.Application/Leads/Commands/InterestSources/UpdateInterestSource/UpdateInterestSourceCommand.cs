﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Commands.InterestSources.UpdateInterestSource
{
    [DataContract]
    public class UpdateInterestSourceCommand : IRequest
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
