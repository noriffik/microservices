﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Leads.Commands.InterestSources.UpdateInterestSource
{
    public class UpdateInterestSourceCommandValidator : AbstractValidator<UpdateInterestSourceCommand>
    {
        public UpdateInterestSourceCommandValidator()
        {
            RuleFor(c => c.Id).GreaterThan(0);
            RuleFor(c => c.Name).NotEmpty();
        }
    }
}
