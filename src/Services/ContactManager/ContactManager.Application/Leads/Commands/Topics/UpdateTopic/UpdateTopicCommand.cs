﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Commands.Topics.UpdateTopic
{
    [DataContract]
    public class UpdateTopicCommand : IRequest
    {
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
