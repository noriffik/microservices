﻿using MediatR;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Leads.Commands.Topics.UpdateTopic
{
    public class UpdateTopicCommandHandler : IRequestHandler<UpdateTopicCommand>
    {
        private readonly IUnitOfWork _work;

        public UpdateTopicCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(UpdateTopicCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var topic = await _work.EntityRepository.Require<Topic>(request.Id, cancellationToken);

            topic.ChangeName(request.Name);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
