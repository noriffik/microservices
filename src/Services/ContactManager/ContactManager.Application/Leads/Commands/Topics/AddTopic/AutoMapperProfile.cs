﻿using AutoMapper;
using NexCore.ContactManager.Domain.Leads;

namespace NexCore.ContactManager.Application.Leads.Commands.Topics.AddTopic
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AddTopicCommand, Topic>()
                .ConvertUsing(s => new Topic(s.Name));
        }
    }
}
