﻿using AutoMapper;
using MediatR;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Leads.Commands.Topics.AddTopic
{
    public class AddTopicCommandHandler : IRequestHandler<AddTopicCommand, int>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public AddTopicCommandHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<int> Handle(AddTopicCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var topic = _mapper.Map<Topic>(request);

            await _work.EntityRepository.Add(topic);

            await _work.Commit(cancellationToken);

            return topic.Id;
        }
    }
}
