﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Leads.Commands.Topics.AddTopic
{
    public class AddTopicCommandValidator : AbstractValidator<AddTopicCommand>
    {
        public AddTopicCommandValidator()
        {
            RuleFor(c => c.Name).NotEmpty();
        }
    }
}
