﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Commands.Topics.AddTopic
{
    [DataContract]
    public class AddTopicCommand : IRequest<int>
    {
        [DataMember]
        public string Name { get; set; }
    }
}
