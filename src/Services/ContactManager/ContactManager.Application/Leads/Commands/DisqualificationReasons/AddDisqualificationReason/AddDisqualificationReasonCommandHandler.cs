﻿using AutoMapper;
using MediatR;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.AddDisqualificationReason
{
    public class AddDisqualificationReasonCommandHandler : IRequestHandler<AddDisqualificationReasonCommand, int>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public AddDisqualificationReasonCommandHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<int> Handle(AddDisqualificationReasonCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var disqualificationReason = _mapper.Map<DisqualificationReason>(request);

            await _work.EntityRepository.Add(disqualificationReason);

            await _work.Commit(cancellationToken);

            return disqualificationReason.Id;
        }
    }
}
