﻿using AutoMapper;
using NexCore.ContactManager.Domain.Leads;

namespace NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.AddDisqualificationReason
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AddDisqualificationReasonCommand, DisqualificationReason>()
                .ConvertUsing(s => new DisqualificationReason(s.Name));
        }
    }
}
