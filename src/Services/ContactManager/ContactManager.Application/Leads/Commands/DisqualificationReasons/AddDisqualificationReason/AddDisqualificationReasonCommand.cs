﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.AddDisqualificationReason
{
    [DataContract]
    public class AddDisqualificationReasonCommand : IRequest<int>
    {
        [DataMember]
        public string Name { get; set; }
    }
}
