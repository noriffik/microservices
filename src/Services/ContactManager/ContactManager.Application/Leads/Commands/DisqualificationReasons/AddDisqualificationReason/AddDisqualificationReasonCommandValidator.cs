﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.AddDisqualificationReason
{
    public class AddDisqualificationReasonCommandValidator : AbstractValidator<AddDisqualificationReasonCommand>
    {
        public AddDisqualificationReasonCommandValidator()
        {
            RuleFor(c => c.Name).NotEmpty();
        }
    }
}
