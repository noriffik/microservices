﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.UpdateDisqualificationReason
{
    public class UpdateDisqualificationReasonCommandValidator : AbstractValidator<UpdateDisqualificationReasonCommand>
    {
        public UpdateDisqualificationReasonCommandValidator()
        {
            RuleFor(c => c.Id).GreaterThan(0);
            RuleFor(c => c.Name).NotEmpty();
        }
    }
}
