﻿using MediatR;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.UpdateDisqualificationReason
{
    public class UpdateDisqualificationReasonCommandHandler : IRequestHandler<UpdateDisqualificationReasonCommand, Unit>
    {
        private readonly IUnitOfWork _work;

        public UpdateDisqualificationReasonCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(UpdateDisqualificationReasonCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var reason = await _work.EntityRepository.Require<DisqualificationReason>(request.Id, cancellationToken);

            reason.ChangeName(request.Name);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
