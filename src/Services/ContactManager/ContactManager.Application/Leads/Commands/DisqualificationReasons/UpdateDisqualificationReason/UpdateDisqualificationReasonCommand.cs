﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.UpdateDisqualificationReason
{
    [DataContract]
    public class UpdateDisqualificationReasonCommand : IRequest
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
