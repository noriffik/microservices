﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries
{
    [DataContract]
    public class DistributorByIdQuery : IRequest<DistributorDto>
    {
        [DataMember]
        public string Id { get; set; }
    }
}
