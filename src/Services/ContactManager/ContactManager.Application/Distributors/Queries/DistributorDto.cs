﻿using NexCore.ContactManager.Application.Organizations.Queries;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries
{
    [DataContract]
    public class DistributorDto
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string CountryCode { get; set; }

        [DataMember]
        public OrganizationDto Organization { get; set; }
    }
}
