﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Employees
{
    [DataContract]
    public class EmployeeByIdQuery : IRequest<EmployeeDto>
    {
        [DataMember]
        public string DistributorId { get; set; }

        [DataMember]
        public int Id { get; set; }
    }
}
