﻿using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Jobs.Queries;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Employees
{
    [DataContract]
    public class EmployeeDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string DistributorId { get; set; }

        [DataMember]
        public JobDto Job { get; set; }

        [DataMember]
        public IndividualDto Individual { get; set; }
    }
}
