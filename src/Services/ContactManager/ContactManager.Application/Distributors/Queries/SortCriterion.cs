﻿namespace NexCore.ContactManager.Application.Distributors.Queries
{
    public enum SortCriterion
    {
        DistributorId = 0,
        OrganizationName = 1
    }
}