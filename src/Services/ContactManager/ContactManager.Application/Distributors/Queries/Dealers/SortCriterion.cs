﻿namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers
{
    public enum SortCriterion
    {
        DealerCode = 0,
        OrganizationName = 1
    }
}