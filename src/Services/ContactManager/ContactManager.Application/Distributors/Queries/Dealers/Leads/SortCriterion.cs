﻿namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers.Leads
{
    public enum SortCriterion
    {
        IndividualContactFirstname = 0,
        IndividualContactLastname = 1,
        IndividualContactMiddlename = 2,
        ContactOrganizationName = 3
    }
}
