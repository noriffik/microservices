﻿using NexCore.Application.Queries;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers.Leads
{
    [DataContract]
    public class LeadQuery : PagedRequest<LeadDto>
    {
        [DataMember]
        public string DistributorId { get; set; }

        [DataMember]
        public string DealerCode { get; set; }

        [DataMember]
        public SortCriterion? SortCriterion { get; set; }

        [DataMember]
        public SortDirection? SortDirection { get; set; }

        [DataMember]
        public string IndividualName { get; set; }

        [DataMember]
        public string OrganizationName { get; set; }

        [DataMember]
        public string IndividualTelephone { get; set; }

        [DataMember]
        public string OrganizationTelephone { get; set; }

        [DataMember]
        public int? EmployeeOwnerId { get; set; }

        [DataMember]
        public int? Status { get; set; }
    }
}
