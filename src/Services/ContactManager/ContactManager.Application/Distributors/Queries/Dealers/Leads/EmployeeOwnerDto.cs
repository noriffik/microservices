﻿using NexCore.ContactManager.Application.Individuals.Dtos;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers.Leads
{
    [DataContract]
    public class EmployeeOwnerDto
    {
        [DataMember]
        public int EmployeeId { get; set; }

        [DataMember]
        public int EmployeeIndividualId { get; set; }

        [DataMember]
        public PersonNameDto EmployeeName { get; set; }

        [DataMember]
        public string DealerId { get; set; }

        [DataMember]
        public int DealerOrganizationId { get; set; }

        [DataMember]
        public string DealerName { get; set; }
    }
}