﻿using System.Runtime.Serialization;
using MediatR;

namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers.Leads
{
    [DataContract]
    public class LeadByIdQuery : IRequest<LeadDto>
    {
        public string DistributorId { get; set; }

        public string DealerCode { get; set; }

        public int Id { get; set; }
    }
}
