﻿using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Leads.Queries.Topics;
using NexCore.ContactManager.Application.Organizations.Queries;
using NexCore.ContactManager.Domain.Leads;
using System;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers.Leads
{
    [DataContract]
    public class LeadDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string DealerId { get; set; }

        [DataMember]
        public TopicDto Topic { get; set; }

        [DataMember]
        public ContactType ContactType { get; set; }

        [DataMember]
        public IndividualDto Individual { get; set; }

        [DataMember]
        public OrganizationDto Organization { get; set; }

        [DataMember]
        public EmployeeOwnerDto EmployeeOwner { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public DateTime CreatedAt { get; set; }

        [DataMember]
        public DateTime UpdatedAt { get; set; }
    }
}
