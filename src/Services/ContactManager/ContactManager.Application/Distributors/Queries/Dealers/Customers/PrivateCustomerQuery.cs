﻿using NexCore.Application.Queries;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers
{
    public class PrivateCustomerQuery : PagedRequest<PrivateCustomerDto>
    {
        [DataMember]
        public string DistributorId { get; set; }

        [DataMember]
        public string DealerCode { get; set; }

        [DataMember]
        public SortCriterion? SortCriterion { get; set; }

        [DataMember]
        public SortDirection? SortDirection { get; set; }

        [DataMember]
        public string Telephone { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
