﻿using NexCore.Application.Queries;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers.Corporate
{
    [DataContract]
    public class CorporateCustomerQuery : PagedRequest<CorporateCustomerDto>
    {
        [DataMember]
        public string DistributorId { get; set; }

        [DataMember]
        public string DealerCode { get; set; }

        [DataMember]
        public string Edrpou { get; set; }

        [DataMember]
        public string Ipn { get; set; }
    }
}
