﻿using NexCore.ContactManager.Application.Individuals.Dtos;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers
{
    [DataContract]
    public class PrivateCustomerDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string DealerId { get; set; }

        [DataMember]
        public IndividualDto Individual { get; set; }
    }
}
