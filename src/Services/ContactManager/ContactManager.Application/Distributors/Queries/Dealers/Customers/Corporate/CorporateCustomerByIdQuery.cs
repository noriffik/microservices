﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers.Corporate
{
    [DataContract]
    public class CorporateCustomerByIdQuery : IRequest<CorporateCustomerDto>
    {
        [DataMember]
        public string DistributorId { get; set; }

        [DataMember]
        public string DealerCode { get; set; }

        [DataMember]
        public int CorporateCustomerId { get; set; }
    }
}
