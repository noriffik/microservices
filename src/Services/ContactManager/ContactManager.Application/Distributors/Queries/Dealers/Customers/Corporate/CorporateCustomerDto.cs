﻿using NexCore.ContactManager.Application.Organizations.Queries;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers.Corporate
{
    [DataContract]
    public class CorporateCustomerDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string DealerId { get; set; }

        [DataMember]
        public OrganizationDto Organization { get; set; }
    }
}