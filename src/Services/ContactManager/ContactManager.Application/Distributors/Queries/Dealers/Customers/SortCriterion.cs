﻿namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers
{
    public enum SortCriterion
    {
        Firstname = 0,
        Lastname = 1,
        Middlename = 2,
    }
}
