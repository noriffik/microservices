﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers
{
    [DataContract]
    public class PrivateCustomerByIdQuery : IRequest<PrivateCustomerDto>
    {
        [DataMember]
        public string DistributorId { get; set; }

        [DataMember]
        public string DealerCode { get; set; }

        [DataMember]
        public int PrivateCustomerId { get; set; }
    }
}
