﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers
{
    [DataContract]
    public class DealerByCodeQuery : IRequest<DealerDto>
    {
        [DataMember]
        public string DistributorId { get; set; }

        [DataMember]
        public string DealerCode { get; set; }
    }
}
