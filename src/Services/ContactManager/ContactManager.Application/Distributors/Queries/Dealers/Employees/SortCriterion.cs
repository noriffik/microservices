﻿namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers.Employees
{
    public enum SortCriterion
    {
        Firstname = 0,
        Lastname = 1,
        Middlename = 2,
        JobTitle = 3
    }
}
