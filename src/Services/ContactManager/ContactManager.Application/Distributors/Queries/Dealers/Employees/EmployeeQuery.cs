﻿using NexCore.Application.Queries;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers.Employees
{
    [DataContract]
    public class EmployeeQuery : PagedRequest<EmployeeDto>
    {
        [DataMember]
        public string DistributorId { get; set; }

        [DataMember]
        public string DealerCode { get; set; }

        [DataMember]
        public SortCriterion? SortCriterion { get; set; }

        [DataMember]
        public SortDirection? SortDirection { get; set; }
    }
}
