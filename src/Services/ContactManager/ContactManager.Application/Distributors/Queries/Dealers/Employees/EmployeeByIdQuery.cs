﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers.Employees
{
    [DataContract]
    public class EmployeeByIdQuery : IRequest<EmployeeDto>
    {
        [DataMember]
        public string DistributorId { get; set; }

        [DataMember]
        public string DealerCode { get; set; }

        [DataMember]
        public int EmployeeId { get; set; }
    }
}
