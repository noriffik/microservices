﻿using NexCore.ContactManager.Application.Organizations.Queries;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers
{
    [DataContract]
    public class DealerDto
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string DistributorId { get; set; }

        [DataMember]
        public string DealerCode { get; set; }

        [DataMember]
        public string CityCode { get; set; }

        [DataMember]
        public OrganizationDto Organization { get; set; }
    }
}
