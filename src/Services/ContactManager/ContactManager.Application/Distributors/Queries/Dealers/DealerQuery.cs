﻿using NexCore.Application.Queries;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Queries.Dealers
{
    public class DealerQuery : PagedRequest<DealerDto>
    {
        [DataMember]
        public string DistributorId { get; set; }

        [DataMember]
        public SortCriterion? SortCriterion { get; set; }

        [DataMember]
        public SortDirection? SortDirection { get; set; }

        [DataMember]
        public string Edrpou { get; set; }

        [DataMember]
        public string Ipn { get; set; }
    }
}
