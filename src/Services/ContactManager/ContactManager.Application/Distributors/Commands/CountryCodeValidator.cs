﻿using FluentValidation;
using NexCore.ContactManager.Application.Providers;
using System;

namespace NexCore.ContactManager.Application.Distributors.Commands
{
    public class CountryCodeValidator : AbstractValidator<string>
    {
        public CountryCodeValidator(IGeographyDescriptionProvider geographyProvider)
        {
            if (geographyProvider == null)
                throw new ArgumentNullException(nameof(geographyProvider));

            RuleFor(c => c).Must(geographyProvider.IsValidIso2)
                .WithMessage("Invalid value");
        }
    }
}
