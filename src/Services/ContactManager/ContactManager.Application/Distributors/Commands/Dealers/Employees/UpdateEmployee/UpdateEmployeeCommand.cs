﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.UpdateEmployee
{
    [DataContract]
    public class UpdateEmployeeCommand : IRequest
    {
        public string DistributorId { get; set; }

        public string DealerCode { get; set; }

        public int EmployeeId { get; set; }

        [DataMember]
        public int JobId { get; set; }
    }
}
