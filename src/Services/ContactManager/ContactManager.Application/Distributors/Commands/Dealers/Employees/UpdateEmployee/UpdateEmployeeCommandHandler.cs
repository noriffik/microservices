﻿using MediatR;
using NexCore.ContactManager.Domain.DealerEmployees.Specifications;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.UpdateEmployee
{
    public class UpdateEmployeeCommandHandler : IRequestHandler<UpdateEmployeeCommand>
    {
        private readonly IUnitOfWork _work;

        public UpdateEmployeeCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(UpdateEmployeeCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            await _work.EntityRepository.HasRequired<Job>(request.JobId, cancellationToken);

            var employee = await _work.EntityRepository.Require(new ByDealerIdAndEmployeeIdSpecification(
                DealerId.Parse(request.DistributorId, request.DealerCode), request.EmployeeId), cancellationToken);

            employee.ChangeJobId(request.JobId);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
