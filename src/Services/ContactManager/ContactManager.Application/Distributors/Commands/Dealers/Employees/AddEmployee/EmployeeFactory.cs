﻿using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.Employees;
using NexCore.ContactManager.Domain.Employees.Specifications;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.AddEmployee
{
    public class EmployeeFactory
    {
        protected readonly IEntityRepository Repository;

        protected EmployeeFactory()
        {
        }

        public EmployeeFactory(IEntityRepository repository)
        {
            Repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public virtual async Task<DealerEmployee> Create(DealerId dealerId, int individualId, int jobId, CancellationToken cancellationToken)
        {
            if (dealerId == null)
                throw new ArgumentNullException(nameof(dealerId));

            var dealer = await Repository.Require<Dealer, DealerId>(dealerId, cancellationToken);
            await Repository.HasRequired<Individual>(individualId, cancellationToken);
            await Repository.HasRequired<Job>(jobId, cancellationToken);

            var isEmployed = new EmployeeByIndividualAndOrganizationSpecification<DealerEmployee>(individualId, dealer.OrganizationId);
            if (await Repository.Has(isEmployed, cancellationToken))
                throw new DuplicateEmploymentException(individualId, dealer.OrganizationId);

            return new DealerEmployee(individualId, dealer.OrganizationId, dealer.Id, jobId);
        }
    }
}
