﻿using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.Legal;
using System;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.AddEmployee
{
    public class EmployeeAddedEventFactory
    {
        public virtual DealerEmployeeAddedEvent Create(DealerEmployee employee, PersonName personName)
        {
            if (employee == null) throw new ArgumentNullException(nameof(employee));
            if (personName == null) throw new ArgumentNullException(nameof(personName));

            return new DealerEmployeeAddedEvent
            {
                DealerId = employee.DealerId.Value,
                EmployeeId = employee.Id,
                FirstName = personName.Firstname,
                LastName = personName.Lastname,
                MiddleName = personName.Middlename
            };
        }
    }
}
