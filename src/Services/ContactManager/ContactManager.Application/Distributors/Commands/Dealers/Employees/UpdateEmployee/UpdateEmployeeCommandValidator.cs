﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.UpdateEmployee
{
    public class UpdateEmployeeCommandValidator : AbstractValidator<UpdateEmployeeCommand>
    {
        public UpdateEmployeeCommandValidator()
        {
            RuleFor(c => c.DistributorId).NotNull().SetValidator(new DistributorIdValidator());
            RuleFor(c => c.DealerCode).NotNull().SetValidator(new DealerCodeValidator());
            RuleFor(c => c.EmployeeId).GreaterThan(0);
            RuleFor(c => c.JobId).GreaterThan(0);
        }
    }
}
