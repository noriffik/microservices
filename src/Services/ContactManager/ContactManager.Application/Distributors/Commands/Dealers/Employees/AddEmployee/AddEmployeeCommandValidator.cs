﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.AddEmployee
{
    public class AddEmployeeCommandValidator : AbstractValidator<AddEmployeeCommand>
    {
        public AddEmployeeCommandValidator()
        {
            RuleFor(c => c.DistributorId).NotNull().SetValidator(new DistributorIdValidator());
            RuleFor(c => c.DealerCode).NotNull().SetValidator(new DealerCodeValidator());
            RuleFor(c => c.IndividualId).GreaterThan(0);
            RuleFor(c => c.JobId).GreaterThan(0);
        }
    }
}
