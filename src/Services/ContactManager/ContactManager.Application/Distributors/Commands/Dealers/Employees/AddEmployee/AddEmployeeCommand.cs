﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.AddEmployee
{
    [DataContract]
    public class AddEmployeeCommand : IRequest<int>
    {
        public string DistributorId { get; set; }

        public string DealerCode { get; set; }

        [DataMember]
        public int IndividualId { get; set; }

        [DataMember]
        public int JobId { get; set; }
    }
}
