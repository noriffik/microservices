﻿using FluentValidation;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers
{
    public class DealerIdValidator : AbstractValidator<string>
    {
        public DealerIdValidator()
        {
            RuleFor(v => v).Must(v => DealerId.TryParse(v, out _))
                .WithMessage("Invalid Format");
        }
    }
}
