﻿using AutoMapper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.Legal;
using System;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer
{
    public class DealerPrivateCustomerAddedEventFactory
    {
        private readonly IMapper _mapper;

        protected DealerPrivateCustomerAddedEventFactory()
        {
        }

        public DealerPrivateCustomerAddedEventFactory(IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public virtual DealerPrivateCustomerAddedEvent Create(Individual individual, DealerPrivateCustomer customer)
        {
            if (individual == null)
                throw new ArgumentNullException(nameof(individual));

            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            return new DealerPrivateCustomerAddedEvent
            {
                DealerPrivateCustomerId = customer.Id,
                DealerId = customer.DealerId,
                OrganizationId = customer.OrganizationId,
                FirstName = individual.Name.Firstname,
                LastName = individual.Name.Lastname,
                MiddleName = individual.Name.Middlename,
                PhysicalAddress = customer.PhysicalAddress.FullAddress,
                Telephone = customer.Telephone.E123,
                Passport = individual.Passport == Passport.Empty ? null : _mapper.Map<Passport, PassportDto>(individual.Passport),
                TaxIdentificationNumber = individual.TaxNumber == TaxIdentificationNumber.Empty ? null : _mapper.Map<TaxIdentificationNumber, TaxIdentificationNumberDto>(individual.TaxNumber)
            };
        }
    }
}