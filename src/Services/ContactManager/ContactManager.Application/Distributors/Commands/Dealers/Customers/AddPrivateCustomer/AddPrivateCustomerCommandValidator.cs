﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer
{
    public class AddPrivateCustomerCommandValidator : AbstractValidator<AddPrivateCustomerCommand>
    {
        public AddPrivateCustomerCommandValidator()
        {
            RuleFor(c => c.DistributorId).NotNull().SetValidator(new DistributorIdValidator());
            RuleFor(c => c.DealerCode).NotNull().SetValidator(new DealerCodeValidator());
            RuleFor(c => c.IndividualId).GreaterThan(0);
        }
    }
}
