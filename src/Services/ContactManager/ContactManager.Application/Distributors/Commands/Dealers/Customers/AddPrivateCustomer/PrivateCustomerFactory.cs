﻿using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.DealerPrivateCustomers.Specifications;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer
{
    public class PrivateCustomerFactory
    {
        protected readonly IEntityRepository Repository;

        protected PrivateCustomerFactory()
        {
        }

        public PrivateCustomerFactory(IEntityRepository repository)
        {
            Repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public virtual async Task<DealerPrivateCustomer> Create(DealerId dealerId, int individualId, CancellationToken cancellationToken)
        {
            if (dealerId == null)
                throw new ArgumentNullException(nameof(dealerId));

            var dealer = await Repository.Require<Dealer, DealerId>(dealerId, cancellationToken);
            await Repository.HasRequired<Individual>(individualId, cancellationToken);

            var isRegisteredSpecification = new ByDealerIdAndContactableIdSpecifications(dealerId, individualId);
            if (await Repository.Has(isRegisteredSpecification, cancellationToken))
                throw new DuplicateDealerPrivateCustomerException(dealerId, individualId);

            return new DealerPrivateCustomer(individualId, dealer.OrganizationId, dealerId);
        }
    }
}
