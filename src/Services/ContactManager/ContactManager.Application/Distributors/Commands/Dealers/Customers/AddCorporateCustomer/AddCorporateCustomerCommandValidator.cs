﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddCorporateCustomer
{
    public class AddCorporateCustomerCommandValidator : AbstractValidator<AddCorporateCustomerCommand>
    {
        public AddCorporateCustomerCommandValidator()
        {
            RuleFor(c => c.DistributorId).NotNull().SetValidator(new DistributorIdValidator());
            RuleFor(c => c.DealerCode).NotNull().SetValidator(new DealerCodeValidator());
            RuleFor(c => c.OrganizationId).GreaterThan(0);
        }
    }
}
