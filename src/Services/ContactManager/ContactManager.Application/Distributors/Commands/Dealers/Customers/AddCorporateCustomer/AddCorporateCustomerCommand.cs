﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddCorporateCustomer
{
    [DataContract]
    public class AddCorporateCustomerCommand : IRequest<int>
    {
        public string DistributorId { get; set; }

        public string DealerCode { get; set; }

        [DataMember]
        public int OrganizationId { get; set; }
    }
}
