﻿using System;
using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.DealerCorporateCustomers;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddCorporateCustomer
{
    public class DealerCorporateCustomerAddedEventFactory
    {
        public DealerCorporateCustomerAddedEventFactory()
        {
        }

        public virtual DealerCorporateCustomerAddedEvent Create(DealerCorporateCustomer corporateCustomer)
        {
            if (corporateCustomer == null) 
                throw new ArgumentNullException(nameof(corporateCustomer));

            return new DealerCorporateCustomerAddedEvent
            {
                DealerCorporateCustomerId = corporateCustomer.Id,
                ContactableId = corporateCustomer.ContactableId,
                OrganizationId = corporateCustomer.OrganizationId,
                DealerId = DealerId.Parse(corporateCustomer.DealerId)
            };
        }
    }
}
