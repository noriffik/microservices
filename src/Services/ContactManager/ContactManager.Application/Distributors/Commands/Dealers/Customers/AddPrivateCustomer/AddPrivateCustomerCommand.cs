﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer
{
    [DataContract]
    public class AddPrivateCustomerCommand : IRequest<int>
    {
        public string DistributorId { get; set; }

        public string DealerCode { get; set; }

        [DataMember]
        public int IndividualId { get; set; }
    }
}
