﻿using NexCore.ContactManager.Domain.DealerCorporateCustomers;
using NexCore.ContactManager.Domain.DealerCorporateCustomers.Specifications;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddCorporateCustomer
{
    public class DealerCorporateCustomerFactory
    {
        protected readonly IEntityRepository Repository;

        protected DealerCorporateCustomerFactory()
        {
        }

        public DealerCorporateCustomerFactory(IEntityRepository repository)
        {
            Repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public virtual async Task<DealerCorporateCustomer> Create(int contactableId, DealerId dealerId, CancellationToken cancellationToken)
        {
            if (dealerId == null)
                throw new ArgumentNullException(nameof(dealerId));

            var dealer = await Repository.Require<Dealer, DealerId>(dealerId, cancellationToken);

            await Repository.HasRequired<Organization>(contactableId, cancellationToken);

            var specification = new ByDealerIdAndContactableIdSpecification(contactableId, dealerId);

            if (await Repository.Has(specification, cancellationToken))
                throw new DuplicateDealerCorporateCustomerException(dealerId, contactableId);

            return new DealerCorporateCustomer(contactableId, dealer.OrganizationId, dealerId);
        }
    }
}
