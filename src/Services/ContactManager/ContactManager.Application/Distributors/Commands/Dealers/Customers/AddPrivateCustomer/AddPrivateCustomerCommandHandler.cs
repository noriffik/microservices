﻿using MediatR;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer
{
    public class AddPrivateCustomerCommandHandler : IRequestHandler<AddPrivateCustomerCommand, int>
    {
        private readonly IUnitOfWork _work;
        private readonly PrivateCustomerFactory _customerFactory;
        private readonly IIntegrationEventStorage _eventStorage;
        private readonly DealerPrivateCustomerAddedEventFactory _eventFactory;

        public AddPrivateCustomerCommandHandler(IUnitOfWork work, PrivateCustomerFactory customerFactory,
            IIntegrationEventStorage eventStorage, DealerPrivateCustomerAddedEventFactory eventFactory)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _customerFactory = customerFactory ?? throw new ArgumentNullException(nameof(customerFactory));
            _eventStorage = eventStorage ?? throw new ArgumentNullException(nameof(eventStorage));
            _eventFactory = eventFactory ?? throw new ArgumentNullException(nameof(eventFactory));
        }

        public async Task<int> Handle(AddPrivateCustomerCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var customer = await AddCustomer(request, cancellationToken);

            await AddEvent(customer, cancellationToken);

            await _work.Commit(cancellationToken);

            return customer.Id;
        }

        private async Task<DealerPrivateCustomer> AddCustomer(AddPrivateCustomerCommand request, CancellationToken cancellationToken)
        {
            var dealerId = DealerId.Parse(request.DistributorId, request.DealerCode);
            var customer = await _customerFactory.Create(dealerId, request.IndividualId, cancellationToken);
            await _work.EntityRepository.Add(customer);

            return customer;
        }

        private async Task AddEvent(DealerPrivateCustomer customer, CancellationToken cancellationToken)
        {
            var individual = await _work.EntityRepository.Require<Individual>(customer.ContactableId, cancellationToken);
            var @event = _eventFactory.Create(individual, customer);
            await _eventStorage.Add(@event);
        }
    }
}
