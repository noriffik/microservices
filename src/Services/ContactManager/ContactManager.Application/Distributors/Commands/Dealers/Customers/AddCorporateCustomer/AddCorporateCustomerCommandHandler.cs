﻿using MediatR;
using NexCore.ContactManager.Domain.DealerCorporateCustomers;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddCorporateCustomer
{
    public class AddCorporateCustomerCommandHandler : IRequestHandler<AddCorporateCustomerCommand, int>
    {
        private readonly DealerCorporateCustomerFactory _customerFactory;
        private readonly IIntegrationEventStorage _eventStorage;
        private readonly IUnitOfWork _work;
        private readonly DealerCorporateCustomerAddedEventFactory _eventFactory;

        public AddCorporateCustomerCommandHandler(DealerCorporateCustomerFactory customerFactory, DealerCorporateCustomerAddedEventFactory eventFactory, 
            IUnitOfWork work, IIntegrationEventStorage eventStorage)
        {
            _customerFactory = customerFactory ?? throw new ArgumentNullException(nameof(customerFactory));
            _eventFactory = eventFactory ?? throw new ArgumentNullException(nameof(eventFactory));
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _eventStorage = eventStorage ?? throw new ArgumentNullException(nameof(eventStorage));
        }

        public async Task<int> Handle(AddCorporateCustomerCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var customer = await AddCustomer(request, cancellationToken);

            await AddEvent(customer, cancellationToken);

            await _work.Commit(cancellationToken);

            return customer.Id;
        }

        private async Task<DealerCorporateCustomer> AddCustomer(AddCorporateCustomerCommand request, CancellationToken cancellationToken)
        {
            var dealerId = DealerId.Parse(request.DistributorId, request.DealerCode);
            var customer = await _customerFactory.Create(request.OrganizationId, dealerId, cancellationToken);
            await _work.EntityRepository.Add(customer);

            return customer;
        }

        private async Task AddEvent(DealerCorporateCustomer customer, CancellationToken cancellationToken)
        {
            var corporateCustomer = await _work.EntityRepository.Require<DealerCorporateCustomer>(customer.Id, cancellationToken);
            var @event = _eventFactory.Create(corporateCustomer);
            await _eventStorage.Add(@event);
        }
    }
}
