﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.UpdateDealer
{
    public class UpdateDealerCommandValidator : AbstractValidator<UpdateDealerCommand>
    {
        public UpdateDealerCommandValidator()
        {
            RuleFor(c => c.DistributorId).NotNull().SetValidator(new DistributorIdValidator());
            RuleFor(c => c.Code).NotNull().SetValidator(new DealerCodeValidator());
        }
    }
}
