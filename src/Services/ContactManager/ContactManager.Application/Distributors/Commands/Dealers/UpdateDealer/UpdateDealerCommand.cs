﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.UpdateDealer
{
    [DataContract]
    public class UpdateDealerCommand : IRequest
    {
        public string DistributorId { get; set; }

        public string Code { get; set; }

        [DataMember]
        public string CityCode { get; set; }
    }
}
