﻿using MediatR;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.UpdateDealer
{
    public class UpdateDealerCommandHandler : IRequestHandler<UpdateDealerCommand>
    {
        private readonly IUnitOfWork _work;

        public UpdateDealerCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(UpdateDealerCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var dealerId = DealerId.Parse(request.DistributorId, request.Code);
            var dealer = await _work.EntityRepository.Require<Dealer, DealerId>(dealerId, cancellationToken);

            dealer.ChangeCityCode(request.CityCode);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
