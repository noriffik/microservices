﻿using AutoMapper;
using MediatR;
using NexCore.Application.Extensions;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Dealers.Specifications;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using System;
using System.Threading;
using System.Threading.Tasks;
using ByOrganizationIdSpecification = NexCore.ContactManager.Domain.Distributors.Specifications.ByOrganizationIdSpecification;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.AddDealer
{
    public class AddDealerCommandHandler : IRequestHandler<AddDealerCommand, string>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;
        private readonly IIntegrationEventStorage _eventStorage;
        private IDealerAddedEventFactory _eventFactory;

        public AddDealerCommandHandler(IUnitOfWork work, IMapper mapper, IIntegrationEventStorage eventStorage)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _eventStorage = eventStorage ?? throw new ArgumentNullException(nameof(eventStorage));
        }

        public IDealerAddedEventFactory EventFactory
        {
            get => _eventFactory ?? (_eventFactory = new DealerAddedEventFactory(_mapper));
            set => _eventFactory = value ?? throw new ArgumentNullException(nameof(value));
        }

        public async Task<string> Handle(AddDealerCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var dealer = _mapper.Map<Dealer>(request);

            await _work.EntityRepository.HasRequired<Distributor, DistributorId>(dealer.DistributorId, cancellationToken);
            await ThrowIfOrganizationIsDistributor(dealer.OrganizationId, cancellationToken);
            await ThrowIfDealerAlreadyExist(dealer.Id, cancellationToken);
            await ThrowIfOrganizationAlreadyIsDealer(dealer.OrganizationId, dealer.DistributorId, cancellationToken);

            var organization = await _work.EntityRepository.Require<Organization>(dealer.OrganizationId, cancellationToken);

            await _work.EntityRepository.Add<Dealer, DealerId>(dealer);

            await _eventStorage.Add(EventFactory.Create(dealer, organization));

            await _work.Commit(cancellationToken);

            return dealer.Id;
        }

        private async Task ThrowIfOrganizationIsDistributor(int organizationId, CancellationToken cancellationToken)
        {
            var exists = await _work.EntityRepository.Has<Distributor, DistributorId>(new ByOrganizationIdSpecification(organizationId), cancellationToken);
            if (!exists) return;

            throw ValidationExtensions.ExceptionForCommandHandler("OrganizationId",
                "Organization is already an distributor", organizationId);
        }

        private async Task ThrowIfDealerAlreadyExist(DealerId dealerId, CancellationToken cancellationToken)
        {
            var exists = await _work.EntityRepository.Has<Dealer, DealerId>(dealerId, cancellationToken);
            if (!exists) return;

            throw ValidationExtensions.ExceptionForCommandHandler("Code",
                $"Distributor with id {dealerId.DistributorId} already has Dealer with code {dealerId.DealerCode}",
                dealerId.DealerCode);
        }

        private async Task ThrowIfOrganizationAlreadyIsDealer(int organizationId, DistributorId distributorId, CancellationToken cancellationToken)
        {
            var exists = await _work.EntityRepository.Has<Dealer, DealerId>(new ByOrganizationAndDistributorSpecification(organizationId, distributorId), cancellationToken);
            if (!exists) return;

            throw ValidationExtensions.ExceptionForCommandHandler("OrganizationId",
                $"Distributor with id {distributorId} already has Dealer for Organization with id {organizationId}",
                organizationId);
        }
    }
}
