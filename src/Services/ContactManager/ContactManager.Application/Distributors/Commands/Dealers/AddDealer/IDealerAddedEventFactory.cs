﻿using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Organizations;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.AddDealer
{
    public interface IDealerAddedEventFactory
    {
        DealerAddedEvent Create(Dealer dealer, Organization organization);
    }
}
