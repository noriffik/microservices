﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.AddDealer
{
    public class AddDealerCommandValidator : AbstractValidator<AddDealerCommand>
    {
        public AddDealerCommandValidator()
        {
            RuleFor(d => d.OrganizationId).NotEmpty().GreaterThan(0);
            RuleFor(d => d.DistributorId).NotNull().SetValidator(new DistributorIdValidator());
            RuleFor(d => d.Code).NotNull().SetValidator(new DealerCodeValidator());
        }
    }
}
