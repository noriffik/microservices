﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.AddDealer
{
    [DataContract]
    public class AddDealerCommand : IRequest<string>
    {
        public string DistributorId { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public int OrganizationId { get; set; }

        [DataMember]
        public string CityCode { get; set; }
    }
}
