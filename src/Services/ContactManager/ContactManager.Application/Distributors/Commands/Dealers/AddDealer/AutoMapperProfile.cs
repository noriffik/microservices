﻿using AutoMapper;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.AddDealer
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AddDealerCommand, Dealer>()
                .ConvertUsing(s => MapDealer(s));
        }

        private static Dealer MapDealer(AddDealerCommand command)
        {
            var dealerId = DealerId.Parse(command.DistributorId, command.Code);
            var dealer = new Dealer(dealerId, command.OrganizationId);
            dealer.ChangeCityCode(command.CityCode);

            return dealer;
        }
    }
}
