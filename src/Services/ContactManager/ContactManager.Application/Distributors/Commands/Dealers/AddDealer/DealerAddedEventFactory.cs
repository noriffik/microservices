﻿using AutoMapper;
using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Application.Organizations.Dtos;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.Legal;
using System;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.AddDealer
{
    public class DealerAddedEventFactory : IDealerAddedEventFactory
    {
        private readonly IMapper _mapper;

        public DealerAddedEventFactory(IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public DealerAddedEvent Create(Dealer dealer, Organization organization)
        {
            if (dealer == null)
                throw new ArgumentNullException(nameof(dealer));
            if (organization == null)
                throw new ArgumentNullException(nameof(organization));

            return new DealerAddedEvent
            {
                DealerId = dealer.Id.Value,
                DistributorId = dealer.Id.DistributorId,
                DealerCode = dealer.Id.DealerCode,
                OrganizationId = dealer.OrganizationId,
                Requisites = organization.Requisites == OrganizationRequisites.Empty
                    ? null
                    : _mapper.Map<OrganizationRequisitesDto>(organization.Requisites),
                Name = organization.Name
            };
        }
    }
}
