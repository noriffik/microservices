﻿using AutoMapper;
using MediatR;
using NexCore.ContactManager.Domain.DealerEmployees.Specifications;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Leads.OpenLead
{
    public class OpenLeadCommandHandler : IRequestHandler<OpenLeadCommand, int>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public OpenLeadCommandHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<int> Handle(OpenLeadCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var lead = _mapper.Map<Lead>(request);

            await HasRequiredRelationships(lead, cancellationToken);

            await _work.EntityRepository.Add(lead);

            await _work.Commit(cancellationToken);

            return lead.Id;
        }

        private async Task HasRequiredRelationships(Lead lead, CancellationToken cancellationToken)
        {
            await _work.EntityRepository.HasRequired<Dealer, DealerId>(lead.DealerId, cancellationToken);

            await _work.EntityRepository.HasRequired<Topic>(lead.TopicId, cancellationToken);

            await _work.EntityRepository.HasRequired<Individual>(lead.Contact.IndividualId, cancellationToken);

            if (lead.Contact.OrganizationId.HasValue)
                await _work.EntityRepository.HasRequired<Organization>(lead.Contact.OrganizationId.Value, cancellationToken);

            if (lead.OwnerEmployeeId.HasValue)
                await _work.EntityRepository.HasRequired(
                    new ByDealerIdAndEmployeeIdSpecification(lead.DealerId, lead.OwnerEmployeeId.Value), cancellationToken);
        }
    }
}
