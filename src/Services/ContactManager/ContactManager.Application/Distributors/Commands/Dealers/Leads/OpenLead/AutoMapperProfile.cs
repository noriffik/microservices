﻿using AutoMapper;
using NexCore.ContactManager.Domain.Leads;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Leads.OpenLead
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<OpenLeadCommand, Lead>()
                .ConvertUsing(s => MapLead(s));
        }

        private static Lead MapLead(OpenLeadCommand command)
        {
            var contact = new Contact(command.IndividualId, command.OrganizationId);
            var dealerId = DealerId.Parse(command.DistributorId, command.DealerCode);

            var lead = new Lead(dealerId, contact, command.TopicId);
            lead.AssignOwner(command.OwnerEmployeeId);

            return lead;
        }
    }
}
