﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Leads.OpenLead
{
    public class OpenLeadCommandValidator : AbstractValidator<OpenLeadCommand>
    {
        public OpenLeadCommandValidator()
        {
            RuleFor(c => c.DistributorId).NotNull().SetValidator(new DistributorIdValidator());
            RuleFor(c => c.DealerCode).NotNull().SetValidator(new DealerCodeValidator());
            RuleFor(c => c.TopicId).GreaterThan(0);
            RuleFor(c => c.IndividualId).GreaterThan(0);
            RuleFor(c => c.OrganizationId).GreaterThan(0);
            RuleFor(c => c.OwnerEmployeeId).GreaterThan(0);
        }
    }
}
