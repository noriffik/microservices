﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers.Leads.OpenLead
{
    [DataContract]
    public class OpenLeadCommand : IRequest<int>
    {
        public string DistributorId { get; set; }

        public string DealerCode { get; set; }

        [DataMember]
        public int TopicId { get; set; }

        [DataMember]
        public int IndividualId { get; set; }

        [DataMember]
        public int? OrganizationId { get; set; }

        [DataMember]
        public int? OwnerEmployeeId { get; set; }
    }
}
