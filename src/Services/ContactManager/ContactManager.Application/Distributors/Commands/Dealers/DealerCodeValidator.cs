﻿using FluentValidation;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Application.Distributors.Commands.Dealers
{
    public class DealerCodeValidator : AbstractValidator<string>
    {
        public DealerCodeValidator()
        {
            RuleFor(v => v).Must(v => DealerCode.TryParse(v, out _))
                .WithMessage("Invalid Format");
        }
    }
}
