﻿using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Application.Distributors.Commands.AddDistributor
{
    public interface IDistributorRegisteredEventFactory
    {
        DistributorRegisteredEvent Create(DistributorId distributorId, Organization organization);
    }
}
