﻿using AutoMapper;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Application.Distributors.Commands.AddDistributor
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AddDistributorCommand, Distributor>()
                .ConvertUsing(s => new Distributor(DistributorId.Parse(s.Id), s.OrganizationId, s.CountryCode));
        }
    }
}
