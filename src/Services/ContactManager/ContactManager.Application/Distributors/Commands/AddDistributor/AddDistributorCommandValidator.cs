﻿using FluentValidation;
using NexCore.ContactManager.Application.Providers;
using System;

namespace NexCore.ContactManager.Application.Distributors.Commands.AddDistributor
{
    public class AddDistributorCommandValidator : AbstractValidator<AddDistributorCommand>
    {
        public AddDistributorCommandValidator(IGeographyDescriptionProvider geographyProvider)
        {
            if (geographyProvider == null)
                throw new ArgumentNullException(nameof(geographyProvider));

            RuleFor(c => c.Id).NotNull().SetValidator(new DistributorIdValidator());
            RuleFor(c => c.OrganizationId).GreaterThan(0);
            RuleFor(c => c.CountryCode).NotEmpty().SetValidator(new CountryCodeValidator(geographyProvider));
        }
    }
}
