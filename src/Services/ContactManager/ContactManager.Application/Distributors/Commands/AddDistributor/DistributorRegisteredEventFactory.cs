﻿using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.DealerNetwork;
using System;

namespace NexCore.ContactManager.Application.Distributors.Commands.AddDistributor
{
    public class DistributorRegisteredEventFactory : IDistributorRegisteredEventFactory
    {
        public DistributorRegisteredEvent Create(DistributorId distributorId, Organization organization)
        {
            if (distributorId == null)
                throw new ArgumentNullException(nameof(distributorId));
            if (organization == null)
                throw new ArgumentNullException(nameof(organization));

            return new DistributorRegisteredEvent
            {
                DistributorId = distributorId.Value,
                OrganizationId = organization.Id,
                Name = organization.Name
            };
        }
    }
}
