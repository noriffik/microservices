﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Commands.AddDistributor
{
    [DataContract]
    public class AddDistributorCommand : IRequest<string>
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public int OrganizationId { get; set; }

        [DataMember]
        public string CountryCode { get; set; }
    }
}
