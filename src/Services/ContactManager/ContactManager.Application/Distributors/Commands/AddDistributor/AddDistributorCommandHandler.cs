﻿using AutoMapper;
using MediatR;
using NexCore.Application.Extensions;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using System;
using System.Threading;
using System.Threading.Tasks;
using ByOrganizationIdSpecification = NexCore.ContactManager.Domain.Distributors.Specifications.ByOrganizationIdSpecification;

namespace NexCore.ContactManager.Application.Distributors.Commands.AddDistributor
{
    public class AddDistributorCommandHandler : IRequestHandler<AddDistributorCommand, string>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;
        private IDistributorRegisteredEventFactory _eventFactory;
        private readonly IIntegrationEventStorage _eventStorage;

        public AddDistributorCommandHandler(IUnitOfWork work, IMapper mapper, IIntegrationEventStorage eventStorage)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _eventStorage = eventStorage ?? throw new ArgumentNullException(nameof(eventStorage));
        }

        public IDistributorRegisteredEventFactory EventFactory
        {
            get => _eventFactory ?? (_eventFactory = new DistributorRegisteredEventFactory());
            set => _eventFactory = value ?? throw new ArgumentNullException(nameof(value));
        }

        public async Task<string> Handle(AddDistributorCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            await ThrowIfOrganizationIsDealer(request.OrganizationId, cancellationToken);
            await ThrowIfOrganizationIsDistributor(request.OrganizationId, cancellationToken);

            var organization = await _work.EntityRepository.Require<Organization>(request.OrganizationId, cancellationToken);
            var distributor = _mapper.Map<Distributor>(request);

            await ThrowIfDistributorIdIsInUse(distributor.Id, cancellationToken);

            await _work.EntityRepository.Add<Distributor, DistributorId>(distributor);

            await _eventStorage.Add(EventFactory.Create(distributor.Id, organization));

            await _work.Commit(cancellationToken);

            return distributor.Id.Value;
        }

        private async Task ThrowIfDistributorIdIsInUse(DistributorId distributorId, CancellationToken cancellationToken)
        {
            var isInUse = await _work.EntityRepository.Has<Distributor, DistributorId>(distributorId, cancellationToken);
            if (isInUse)
                throw ValidationExtensions.ExceptionForCommandHandler("Id", "DistributorId is in use", distributorId.Value);
        }

        private async Task ThrowIfOrganizationIsDealer(int organizationId, CancellationToken cancellationToken)
        {
            var exists = await _work.EntityRepository.Has<Dealer, DealerId>(new Domain.Dealers.Specifications.ByOrganizationIdSpecification(organizationId), cancellationToken);
            if (exists)
                throw ValidationExtensions.ExceptionForCommandHandler("OrganizationId", "Organization is already a dealer", organizationId);
        }

        private async Task ThrowIfOrganizationIsDistributor(int organizationId, CancellationToken cancellationToken)
        {
            var exists = await _work.EntityRepository.Has<Distributor, DistributorId>(new ByOrganizationIdSpecification(organizationId), cancellationToken);
            if (exists)
                throw ValidationExtensions.ExceptionForCommandHandler("OrganizationId", "Organization is already an distributor", organizationId);
        }
    }
}
