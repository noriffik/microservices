﻿using FluentValidation;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Application.Distributors.Commands
{
    public class DistributorIdValidator : AbstractValidator<string>
    {
        public DistributorIdValidator()
        {
            RuleFor(v => v).Must(v => DistributorId.TryParse(v, out _))
                .WithMessage("Invalid Format");
        }
    }
}
