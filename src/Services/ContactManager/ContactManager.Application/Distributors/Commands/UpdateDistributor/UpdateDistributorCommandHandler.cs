﻿using MediatR;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Distributors.Commands.UpdateDistributor
{
    public class UpdateDistributorCommandHandler : IRequestHandler<UpdateDistributorCommand>
    {
        private readonly IUnitOfWork _work;

        public UpdateDistributorCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(UpdateDistributorCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var distributor = await _work.EntityRepository.Require<Distributor, DistributorId>(
                DistributorId.Parse(request.DistributorId), cancellationToken);

            distributor.ChangeCountryCode(request.CountryCode);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
