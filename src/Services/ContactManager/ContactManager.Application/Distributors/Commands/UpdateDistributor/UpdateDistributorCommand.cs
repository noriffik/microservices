﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Commands.UpdateDistributor
{
    [DataContract]
    public class UpdateDistributorCommand : IRequest
    {
        [DataMember]
        public string DistributorId { get; set; }

        [DataMember]
        public string CountryCode { get; set; }
    }
}
