﻿using FluentValidation;
using NexCore.ContactManager.Application.Providers;

namespace NexCore.ContactManager.Application.Distributors.Commands.UpdateDistributor
{
    public class UpdateDistributorCommandValidator : AbstractValidator<UpdateDistributorCommand>
    {
        public UpdateDistributorCommandValidator(IGeographyDescriptionProvider geographyProvider)
        {
            RuleFor(c => c.DistributorId).NotNull().SetValidator(new DistributorIdValidator());
            RuleFor(c => c.CountryCode).NotNull().SetValidator(new CountryCodeValidator(geographyProvider));
        }
    }
}
