﻿using MediatR;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Distributors.Commands.Employees.AddEmployee
{
    public class AddEmployeeCommandHandler : IRequestHandler<AddEmployeeCommand, int>
    {
        private readonly IUnitOfWork _work;
        private readonly IIntegrationEventStorage _eventStorage;
        private readonly EmployeeFactory _employeeFactory;
        private EmployeeAddedEventFactory _eventFactory;

        public EmployeeAddedEventFactory EventFactory
        {
            get => _eventFactory ?? (_eventFactory = new EmployeeAddedEventFactory());
            set => _eventFactory = value ?? throw new ArgumentNullException(nameof(value));
        }

        public AddEmployeeCommandHandler(EmployeeFactory employeeFactory, IUnitOfWork work,
            IIntegrationEventStorage eventStorage)
        {
            _employeeFactory = employeeFactory ?? throw new ArgumentNullException(nameof(employeeFactory));
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _eventStorage = eventStorage ?? throw new ArgumentNullException(nameof(eventStorage));
        }

        public async Task<int> Handle(AddEmployeeCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var distributorId = DistributorId.Parse(request.DistributorId);
            var employee = await _employeeFactory.Create(distributorId, request.IndividualId, request.JobId, cancellationToken);

            await _work.EntityRepository.Add(employee);

            var individual = await _work.EntityRepository.Require<Individual>(employee.IndividualId, cancellationToken);

            await _eventStorage.Add(EventFactory.Create(employee, individual.Name));

            await _work.Commit(cancellationToken);

            return employee.Id;
        }
    }
}
