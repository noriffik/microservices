﻿using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.DistributorEmployees;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Employees;
using NexCore.ContactManager.Domain.Employees.Specifications;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Distributors.Commands.Employees.AddEmployee
{
    public class EmployeeFactory
    {
        protected readonly IEntityRepository Repository;

        protected EmployeeFactory()
        {
        }

        public EmployeeFactory(IEntityRepository repository)
        {
            Repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public virtual async Task<DistributorEmployee> Create(DistributorId distributorId, int individualId, int jobId, CancellationToken cancellationToken )
        {
            if (distributorId == null)
                throw new ArgumentNullException(nameof(distributorId));

            var distributor = await Repository.Require<Distributor, DistributorId>(distributorId, cancellationToken);

            await Repository.HasRequired<Individual>(individualId, cancellationToken);
            await Repository.HasRequired<Job>(jobId, cancellationToken);

            var employedSpecification = new EmployeeByIndividualAndOrganizationSpecification<DistributorEmployee>(individualId, distributor.OrganizationId);
            if (await Repository.Has(employedSpecification, cancellationToken))
                throw new DuplicateEmploymentException(individualId, distributor.OrganizationId);

            return new DistributorEmployee(individualId, distributor.OrganizationId, distributorId, jobId);
        }
    }
}
