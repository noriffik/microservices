﻿using FluentValidation;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Application.Distributors.Commands.Employees.AddEmployee
{
    public class AddEmployeeCommandValidator : AbstractValidator<AddEmployeeCommand>
    {
        public AddEmployeeCommandValidator()
        {
            RuleFor(c => c.DistributorId).NotNull().Must(v => DistributorId.TryParse(v, out _))
                .WithMessage("Invalid Format");

            RuleFor(c => c.IndividualId).GreaterThan(0);
            RuleFor(c => c.JobId).GreaterThan(0);
        }
    }
}
