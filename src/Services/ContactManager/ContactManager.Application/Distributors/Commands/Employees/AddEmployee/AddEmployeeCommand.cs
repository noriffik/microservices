﻿using System.Runtime.Serialization;
using MediatR;

namespace NexCore.ContactManager.Application.Distributors.Commands.Employees.AddEmployee
{
    [DataContract]
    public class AddEmployeeCommand : IRequest<int>
    {
        public string DistributorId { get; set; }

        [DataMember]
        public int IndividualId { get; set; }

        [DataMember]
        public int JobId { get; set; }
    }
}
