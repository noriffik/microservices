﻿using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.DistributorEmployees;
using NexCore.Legal;
using System;

namespace NexCore.ContactManager.Application.Distributors.Commands.Employees.AddEmployee
{
    public class EmployeeAddedEventFactory
    {
        public virtual DistributorEmployeeAddedEvent Create(DistributorEmployee employee, PersonName personName)
        {
            if (employee == null) throw new ArgumentNullException(nameof(employee));
            if (personName == null) throw new ArgumentNullException(nameof(personName));

            return new DistributorEmployeeAddedEvent
            {
                DistributorId = employee.DistributorId.Value,
                EmployeeId = employee.Id,
                FirstName = personName.Firstname,
                LastName = personName.Lastname,
                MiddleName = personName.Middlename
            };
        }
    }
}
