﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Distributors.Commands.Employees.UpdateEmployee
{
    [DataContract]
    public class UpdateEmployeeCommand : IRequest
    {
        public string DistributorId { get; set; }

        public int Id { get; set; }

        [DataMember]
        public int JobId { get; set; }
    }
}
