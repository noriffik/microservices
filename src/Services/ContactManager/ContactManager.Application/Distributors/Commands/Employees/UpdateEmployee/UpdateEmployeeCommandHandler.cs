﻿using MediatR;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.DistributorEmployees.Specifications;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Distributors.Commands.Employees.UpdateEmployee
{
    public class UpdateEmployeeCommandHandler : IRequestHandler<UpdateEmployeeCommand>
    {
        private readonly IUnitOfWork _work;

        public UpdateEmployeeCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(UpdateEmployeeCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            await _work.EntityRepository.HasRequired<Job>(request.JobId, cancellationToken);

            var employee = await _work.EntityRepository.Require(new ByDistributorIdAndEmployeeIdSpecification(
                DistributorId.Parse(request.DistributorId), request.Id), cancellationToken);

            employee.ChangeJobId(request.JobId);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
