﻿using FluentValidation;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Application.Distributors.Commands.Employees.UpdateEmployee
{
    public class UpdateEmployeeCommandValidator : AbstractValidator<UpdateEmployeeCommand>
    {
        public UpdateEmployeeCommandValidator()
        {
            RuleFor(c => c.DistributorId).NotNull().Must(v => DistributorId.TryParse(v, out _))
                .WithMessage("Invalid Format");
            RuleFor(c => c.Id).GreaterThan(0);
            RuleFor(c => c.JobId).GreaterThan(0);
        }
    }
}
