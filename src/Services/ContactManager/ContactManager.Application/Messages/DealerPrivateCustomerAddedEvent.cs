﻿using NexCore.EventBus.Abstract;
using System.Diagnostics.CodeAnalysis;
using NexCore.ContactManager.Application.Individuals.Dtos;

namespace NexCore.ContactManager.Application.Messages
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global", Justification = "Required by messages broker")]
    public class DealerPrivateCustomerAddedEvent : IntegrationEvent
    {
        public int DealerPrivateCustomerId { get; set; }

        public string DealerId { get; set; }

        public int OrganizationId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string PhysicalAddress { get; set; }

        public string Telephone { get; set; }

        public PassportDto Passport { get; set; }

        public TaxIdentificationNumberDto TaxIdentificationNumber { get; set; }
    }
}
