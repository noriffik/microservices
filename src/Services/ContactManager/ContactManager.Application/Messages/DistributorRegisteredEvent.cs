﻿using NexCore.EventBus.Abstract;
using System;

namespace NexCore.ContactManager.Application.Messages
{
    public class DistributorRegisteredEvent : IntegrationEvent
    {
        public DistributorRegisteredEvent()
        {
        }

        public DistributorRegisteredEvent(Guid id, DateTime createdAt) : base(id, createdAt)
        {
        }

        public string DistributorId { get; set; }

        public int OrganizationId { get; set; }

        public string Name { get; set; }
    }
}
