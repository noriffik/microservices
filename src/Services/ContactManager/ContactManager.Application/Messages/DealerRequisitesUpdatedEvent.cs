﻿using NexCore.ContactManager.Application.Organizations.Dtos;
using NexCore.EventBus.Abstract;
using System;

namespace NexCore.ContactManager.Application.Messages
{
    public class DealerRequisitesUpdatedEvent : IntegrationEvent
    {
        public DealerRequisitesUpdatedEvent()
        {
        }

        public DealerRequisitesUpdatedEvent(Guid id, DateTime createdAt) : base(id, createdAt)
        {
        }

        public string DealerId { get; set; }

        public OrganizationRequisitesDto Requisites { get; set; }
    }
}
