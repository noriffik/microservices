﻿using NexCore.ContactManager.Application.Organizations.Dtos;
using NexCore.EventBus.Abstract;
using System;

namespace NexCore.ContactManager.Application.Messages
{
    public class DealerAddedEvent : IntegrationEvent
    {
        public DealerAddedEvent()
        {
        }

        public DealerAddedEvent(Guid id, DateTime createdAt) : base(id, createdAt)
        {
        }

        public string DealerId { get; set; }

        public string DistributorId { get; set; }

        public string DealerCode { get; set; }

        public int OrganizationId { get; set; }

        public OrganizationRequisitesDto Requisites { get; set; }

        public string Name { get; set; }
    }
}
