﻿using NexCore.EventBus.Abstract;
using System;

namespace NexCore.ContactManager.Application.Messages
{
    public class DistributorEmployeeAddedEvent : IntegrationEvent
    {
        public DistributorEmployeeAddedEvent()
        {
        }

        public DistributorEmployeeAddedEvent(Guid id, DateTime createdAt) : base(id, createdAt)
        {
        }
        
        public string DistributorId { get; set; }

        public int EmployeeId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }
    }
}
