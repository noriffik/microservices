﻿using NexCore.EventBus.Abstract;
using System;
using NexCore.ContactManager.Application.Individuals.Dtos;

namespace NexCore.ContactManager.Application.Messages
{
    public class DealerEmployeeNameUpdatedEvent : IntegrationEvent
    {
        public DealerEmployeeNameUpdatedEvent()
        {
        }

        public DealerEmployeeNameUpdatedEvent(Guid id, DateTime createdAt) : base(id, createdAt)
        {
        }

        public int DealerEmployeeId { get; set; }

        public PersonNameDto PersonName { get; set; }
    }
}
