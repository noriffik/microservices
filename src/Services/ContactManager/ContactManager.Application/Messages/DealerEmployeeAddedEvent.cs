﻿using NexCore.EventBus.Abstract;
using System;

namespace NexCore.ContactManager.Application.Messages
{
    public class DealerEmployeeAddedEvent : IntegrationEvent
    {
        public DealerEmployeeAddedEvent()
        {
        }

        public DealerEmployeeAddedEvent(Guid id, DateTime createdAt) : base(id, createdAt)
        {
        }
        
        public string DealerId { get; set; }

        public int EmployeeId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }
    }
}
