﻿using NexCore.EventBus.Abstract;

namespace NexCore.ContactManager.Application.Messages
{
    public class DealerCorporateCustomerAddedEvent : IntegrationEvent
    {
        public int DealerCorporateCustomerId { get; set; }

        public int ContactableId { get; set; }

        public int OrganizationId { get; set; }

        public string DealerId { get; set; }
    }
}
