﻿using NexCore.EventBus.Abstract;
using System;
using NexCore.ContactManager.Application.Individuals.Dtos;

namespace NexCore.ContactManager.Application.Messages
{
    public class DealerPrivateCustomerInfoUpdatedEvent : IntegrationEvent
    {
        public DealerPrivateCustomerInfoUpdatedEvent()
        {
        }

        public DealerPrivateCustomerInfoUpdatedEvent(Guid id, DateTime createdAt) : base(id, createdAt)
        {
        }

        public int DealerPrivateCustomerId { get; set; }

        public PersonNameDto PersonName { get; set; }

        public PassportDto Passport { get; set; }

        public TaxIdentificationNumberDto TaxNumber { get; set; }
    }
}
