﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    public class PassportDtoValidator : AbstractValidator<PassportDto>
    {
        public PassportDtoValidator()
        {
            RuleFor(d => d.Series).NotEmpty();
            RuleFor(d => d.Number).NotEmpty();
            RuleFor(d => d.Issuer).NotEmpty();
        }
    }
}
