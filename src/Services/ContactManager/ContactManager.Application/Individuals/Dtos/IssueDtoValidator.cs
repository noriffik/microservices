﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    public class IssueDtoValidator : AbstractValidator<IssueDto>
    {
        public IssueDtoValidator()
        {
            RuleFor(c => c.Issuer).NotEmpty();
        }
    }
}
