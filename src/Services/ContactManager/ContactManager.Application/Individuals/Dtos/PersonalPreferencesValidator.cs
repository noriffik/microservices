﻿using FluentValidation;
using NexCore.ContactManager.Application.Providers;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    public class PersonalPreferencesValidator : AbstractValidator<PersonalPreferencesDto>
    {
        public PersonalPreferencesValidator(ILanguageProvider languageProvider)
        {
            RuleFor(d => d.Language).Must(d => d == null || languageProvider.IsValidIso2(d).Result);
            RuleFor(d => d.Communications).SetValidator(new CommunicationPreferencesValidator());
        }
    }
}
