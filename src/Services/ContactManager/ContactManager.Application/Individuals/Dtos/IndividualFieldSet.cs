﻿using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Domain.Individuals;
using System;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    [DataContract]
    public class IndividualFieldSet
    {
        [DataMember]
        public PersonNameDto Name { get; set; }

        [DataMember]
        public DateTime? BirthDay { get; set; }

        [DataMember]
        public Gender? Gender { get; set; }

        [DataMember]
        public int? FamilyStatus { get; set; }

        [DataMember]
        public int? Children { get; set; }

        [DataMember]
        public PersonalPreferencesDto Preferences { get; set; }

        [DataMember]
        public PassportDto Passport { get; set; }

        [DataMember]
        public TaxIdentificationNumberDto TaxNumber { get; set; }

        [DataMember]
        public ContactDto Contact { get; set; }
    }
}
