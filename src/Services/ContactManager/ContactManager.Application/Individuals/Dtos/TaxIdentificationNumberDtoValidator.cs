﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    public class TaxIdentificationNumberDtoValidator : AbstractValidator<TaxIdentificationNumberDto>
    {
        public TaxIdentificationNumberDtoValidator()
        {
            RuleFor(c => c.Number).NotEmpty();
            RuleFor(c => c.Issue).SetValidator(new IssueDtoValidator());
        }
    }
}
