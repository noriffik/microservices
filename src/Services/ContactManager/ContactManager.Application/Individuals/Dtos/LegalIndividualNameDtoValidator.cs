﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    public class LegalIndividualNameDtoValidator : AbstractValidator<PersonNameDto>
    {
        public LegalIndividualNameDtoValidator()
        {
            RuleFor(c => c.Firstname).NotEmpty();
            RuleFor(c => c.Lastname).NotEmpty();
            RuleFor(c => c.Middlename).NotEmpty();
        }
    }
}
