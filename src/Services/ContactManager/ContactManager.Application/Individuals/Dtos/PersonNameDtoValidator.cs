﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    public class PersonNameDtoValidator : AbstractValidator<PersonNameDto>
    {
        public PersonNameDtoValidator()
        {
            RuleFor(p => p.Firstname).NotEmpty()
                .When(p => string.IsNullOrEmpty(p.Lastname) && string.IsNullOrEmpty(p.Middlename));
            RuleFor(p => p.Lastname).NotEmpty()
                .When(p => string.IsNullOrEmpty(p.Firstname) && string.IsNullOrEmpty(p.Middlename));
            RuleFor(p => p.Middlename).NotEmpty()
                .When(p => string.IsNullOrEmpty(p.Lastname) && string.IsNullOrEmpty(p.Firstname));
        }
    }
}
