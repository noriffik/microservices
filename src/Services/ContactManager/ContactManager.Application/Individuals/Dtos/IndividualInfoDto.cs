﻿using NexCore.ContactManager.Domain.Individuals;
using System;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    [DataContract]
    public class IndividualInfoDto
    {
        [DataMember]
        public PersonNameDto Name { get; set; }

        [DataMember]
        public DateTime? BirthDay { get; set; }

        [DataMember]
        public Gender? Gender { get; set; }

        [DataMember]
        public int? FamilyStatus { get; set; }

        [DataMember]
        public int? Children { get; set; }
    }
}
