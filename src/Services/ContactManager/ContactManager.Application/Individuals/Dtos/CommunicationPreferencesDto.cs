﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    [DataContract]
    public class CommunicationPreferencesDto
    {
        [DataMember]
        public string PreferredMedium { get; set; }

        [DataMember]
        public IEnumerable<string> BannedMediums { get; set; }
    }
}
