﻿using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    [DataContract]
    public class PersonalPreferencesDto
    {
        [DataMember]
        public string Language { get; set; }

        [DataMember]
        public CommunicationPreferencesDto Communications { get; set; }
    }
}
