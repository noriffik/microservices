﻿using AutoMapper;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.Legal;
using System.Linq;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            MapFromDto();
            MapToDto();
        }

        private void MapFromDto()
        {
            CreateMap<PersonNameDto, PersonName>()
                .ConvertUsing(s => new PersonName(s.Firstname, s.Lastname, s.Middlename));

            CreateMap<PassportDto, Passport>()
                .ConvertUsing(s => MapPassportFromDto(s));

            CreateMap<TaxIdentificationNumberDto, TaxIdentificationNumber>()
                .ConvertUsing(s => MapTaxNumberFromDto(s));

            CreateMap<IndividualFieldSet, Individual>()
                .ForMember(d => d.Name, c => c.MapFrom(s => s.Name))
                .ForMember(d => d.Birthday, c => c.MapFrom(s => s.BirthDay))
                .ForMember(d => d.Gender, c => c.MapFrom(s => s.Gender))
                .ForMember(d => d.FamilyStatus, c => c.Ignore())
                .ForMember(d => d.Contact, c => c.Ignore())
                .ForMember(d => d.Preferences, c => c.Ignore())
                .ForMember(d => d.Passport, c => c.Ignore())
                .ForMember(d => d.TaxNumber, c => c.Ignore())
                .AfterMap((s, d) => d.UpdateFamilyStatus(MapFamilyStatusFromDto(s.FamilyStatus)))
                .AfterMap((s, d) => d.ChangePassport(MapPassportFromDto(s.Passport)))
                .AfterMap((s, d) => d.ChangeTaxNumber(MapTaxNumberFromDto(s.TaxNumber)))
                .AfterMap((s, d, context) => context.Mapper.Map(s.Contact, d))
                .AfterMap((s, d, context) => context.Mapper.Map(s.Preferences ?? new PersonalPreferencesDto(), d.Preferences));

            CreateMap<IndividualInfoDto, Individual>()
                .ConvertUsing(s => new Individual(new PersonName(s.Name.Firstname, s.Name.Lastname, s.Name.Middlename))
                {
                    Birthday = s.BirthDay,
                    Gender = s.Gender
                });

            CreateMap<PersonalPreferencesDto, PersonalPreferences>()
                .ForMember(d => d.Communications, c => c.Ignore())
                .AfterMap((s, d, context) => context.Mapper.Map(s.Communications ?? new CommunicationPreferencesDto(), d.Communications));

            CreateMap<CommunicationPreferencesDto, CommunicationPreferences>()
                .ConstructUsing(d => new CommunicationPreferences())
                .ForMember(d => d.PreferredMedium, c => c.MapFrom(s => CommunicationMedium.Get(s.PreferredMedium)))
                .ForMember(d => d.BannedMediums, c => c.Ignore())
                .AfterMap((s, d) =>
                {
                    if (s.BannedMediums == null)
                        d.ClearBannedMediums();
                    else
                        d.ChangeBannedMediums(CommunicationMediumSet.Parse(s.BannedMediums));
                });
        }

        private void MapToDto()
        {
            CreateMap<Passport, PassportDto>()
                .ForMember(d => d.Issuer, p => p.MapFrom(s => s.Issue.Issuer))
                .ForMember(d => d.IssuedOn, p => p.MapFrom(s => s.Issue.IssuedOn));

            CreateMap<CommunicationPreferences, CommunicationPreferencesDto>()
                .ForMember(d => d.PreferredMedium, p => p.MapFrom(s => s.PreferredMedium != null ? s.PreferredMedium.Id : null))
                .ForMember(d => d.BannedMediums, p => p.MapFrom(s => s.BannedMediums.Values.Select(m => m.Id)));

            CreateMap<Individual, IndividualDto>()
                .ForMember(d => d.FamilyStatus, p => p.MapFrom(s => s.FamilyStatus == null ? null : new int?(s.FamilyStatus.Id)))
                .ForMember(d => d.Passport, p => p.MapFrom(s => s.Passport == Passport.Empty ? null : s.Passport))
                .ForMember(d => d.TaxNumber,
                    p => p.MapFrom(s => s.TaxNumber == TaxIdentificationNumber.Empty ? null : s.TaxNumber));

            CreateMap<TaxIdentificationNumber, TaxIdentificationNumberDto>()
                .ForMember(d => d.Issue, p => p.MapFrom(s => s.Issue == Issue.Empty ? null : s.Issue));

            CreateMap<Issue, IssueDto>()
                .ForMember(d => d.IssuedOn, p => p.MapFrom(s => s.IssuedOn.GetValueOrDefault()));

            CreateMap<FamilyStatus, int?>()
               .ConvertUsing(s => MapFamilyStatusToDto(s));
        }

        private static Passport MapPassportFromDto(PassportDto dto)
        {
            return dto == null
                ? Passport.Empty
                : new Passport(dto.Series, dto.Number, new Issue(dto.Issuer, dto.IssuedOn));
        }

        private static TaxIdentificationNumber MapTaxNumberFromDto(TaxIdentificationNumberDto dto)
        {
            return dto == null
                ? TaxIdentificationNumber.Empty
                : new TaxIdentificationNumber(dto.Number, dto.RegisteredOn, new Issue(dto.Issue.Issuer, dto.Issue.IssuedOn));
        }

        private static FamilyStatus MapFamilyStatusFromDto(int? id)
        {
            return id.HasValue ? FamilyStatus.Get(id.Value) : null;
        }

        private static int? MapFamilyStatusToDto(FamilyStatus familyStatus)
        {
            return familyStatus?.Id ?? 0;
        }
    }
}
