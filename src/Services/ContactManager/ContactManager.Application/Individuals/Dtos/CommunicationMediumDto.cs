﻿using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    [DataContract]
    public class CommunicationMediumDto
    {
        [DataMember] 
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
