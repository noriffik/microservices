﻿using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    [DataContract]
    public class IndividualDto : IndividualFieldSet
    {
        [DataMember]
        public int Id { get; set; }
    }
}
