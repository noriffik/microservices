﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    public class IndividualInfoDtoValidator : AbstractValidator<IndividualInfoDto>
    {
        public IndividualInfoDtoValidator()
        {
            RuleFor(p => p.Name).SetValidator(new PersonNameDtoValidator());
            RuleFor(p => p.Gender).IsInEnum();
        }
    }
}
