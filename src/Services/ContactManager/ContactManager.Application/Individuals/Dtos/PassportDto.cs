﻿using System;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    [DataContract]
    public class PassportDto
    {
        [DataMember]
        public string Series { get; set; }

        [DataMember]
        public string Number { get; set; }

        [DataMember]
        public string Issuer { get; set; }

        [DataMember]
        public DateTime IssuedOn { get; set; }
    }
}
