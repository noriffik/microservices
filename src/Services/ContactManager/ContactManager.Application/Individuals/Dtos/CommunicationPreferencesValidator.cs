﻿using FluentValidation;
using NexCore.ContactManager.Domain.Individuals;
using System.Linq;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    public class CommunicationPreferencesValidator : AbstractValidator<CommunicationPreferencesDto>
    {
        public CommunicationPreferencesValidator()
        {
            RuleFor(c => c.PreferredMedium).Must(c => c == null || CommunicationMedium.Has(c.ToUpperInvariant()));
            RuleFor(c => c.BannedMediums).Must(banned => banned?.All(CommunicationMedium.Has) ?? true);
        }
    }
}
