﻿using FluentValidation;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Providers;
using NexCore.Legal;

namespace NexCore.ContactManager.Application.Individuals.Dtos
{
    public abstract class IndividualFieldSetValidator<T> : AbstractValidator<T> where T : IndividualFieldSet
    {
        protected IndividualFieldSetValidator(ILanguageProvider languageProvider)
        {
            RuleFor(c => c.Name).NotNull().SetValidator(new PersonNameDtoValidator());
            RuleFor(p => p.Gender).IsInEnum();
            RuleFor(c => c.Passport).SetValidator(new PassportDtoValidator());
            RuleFor(c => c.TaxNumber).SetValidator(new TaxIdentificationNumberDtoValidator());
            RuleFor(c => c.FamilyStatus).Must(f => f == null || FamilyStatus.Has(f.Value));
            RuleFor(c => c.Preferences).SetValidator(new PersonalPreferencesValidator(languageProvider));
            RuleFor(c => c.Children).GreaterThanOrEqualTo(0);
            RuleFor(c => c.Contact).NotNull().SetValidator(new ContactDtoValidator());
        }
    }
}