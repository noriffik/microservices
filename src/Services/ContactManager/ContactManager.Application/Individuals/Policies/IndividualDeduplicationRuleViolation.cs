﻿using NexCore.Domain;
using System;

namespace NexCore.ContactManager.Application.Individuals.Policies
{
    public class IndividualDeduplicationRuleViolationException : DomainException
    {
        private const string DefaultMessage = "Rule of IndividualDeduplicationRulePolicy was violated";

        public IndividualDeduplicationRuleViolationException() : base(DefaultMessage)
        {
        }

        public IndividualDeduplicationRuleViolationException(string message) : base(message)
        {
        }

        public IndividualDeduplicationRuleViolationException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
