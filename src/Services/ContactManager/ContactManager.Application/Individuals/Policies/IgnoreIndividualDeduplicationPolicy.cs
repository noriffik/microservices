﻿using NexCore.ContactManager.Domain.Individuals;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Individuals.Policies
{
    public class IgnoreIndividualDeduplicationPolicy : IIndividualDeduplicationRulePolicy
    {
        public Task Apply(Individual individual, CancellationToken cancellationToken) => Task.CompletedTask;
    }
}
