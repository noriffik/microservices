﻿using System.Threading;
using NexCore.ContactManager.Domain.Individuals;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Individuals.Policies
{
    public interface IIndividualDeduplicationRulePolicy
    {
        Task Apply(Individual individual, CancellationToken cancellationToken);
    }
}
