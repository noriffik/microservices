﻿using NexCore.ContactManager.Domain.Contacts.Specifications;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.Domain;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Individuals.Policies
{
    public class EnforceIndividualDeduplicationPolicy : IIndividualDeduplicationRulePolicy
    {
        private readonly IEntityRepository _repository;

        public EnforceIndividualDeduplicationPolicy(IEntityRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task Apply(Individual individual, CancellationToken cancellationToken)
        {
            if (individual == null)
                throw new ArgumentNullException(nameof(individual));

            var telephones = individual.Contact.Telephones.Select(p => p.Value).ToList();

            var specification = new TelephoneInUseByOtherContactableSpecification<Individual>(individual.Id, telephones);

            if (await _repository.Has(specification, cancellationToken))
                throw new IndividualDeduplicationRuleViolationException("One of the telephones is in use.");
        }
    }
}
