﻿using MediatR;
using NexCore.ContactManager.Application.Individuals.Dtos;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Individuals.Commands.AddIndividual
{
    [DataContract]
    public class AddIndividualCommand : IndividualFieldSet, IRequest<int>
    {
    }
}
