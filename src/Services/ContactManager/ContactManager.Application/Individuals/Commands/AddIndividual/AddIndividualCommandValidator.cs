﻿using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Providers;

namespace NexCore.ContactManager.Application.Individuals.Commands.AddIndividual
{
    public class AddIndividualCommandValidator : IndividualFieldSetValidator<AddIndividualCommand>
    {
        public AddIndividualCommandValidator(ILanguageProvider languageProvider) : base(languageProvider)
        {
        }
    }
}
