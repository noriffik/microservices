﻿using AutoMapper;
using MediatR;
using NexCore.ContactManager.Application.Individuals.Policies;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Individuals.Commands.AddIndividual
{
    public class AddIndividualCommandHandler : IRequestHandler<AddIndividualCommand, int>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;
        private readonly IIndividualDeduplicationRulePolicy _deduplicationPolicy;

        public AddIndividualCommandHandler(IUnitOfWork work, IMapper mapper, IIndividualDeduplicationRulePolicy deduplicationPolicy)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _deduplicationPolicy = deduplicationPolicy ?? throw new ArgumentNullException(nameof(deduplicationPolicy));
        }

        public async Task<int> Handle(AddIndividualCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var individual = _mapper.Map<Individual>(request);

            await _deduplicationPolicy.Apply(individual, cancellationToken);

            await _work.EntityRepository.Add(individual);

            await _work.Commit(cancellationToken);

            return individual.Id;
        }
    }
}
