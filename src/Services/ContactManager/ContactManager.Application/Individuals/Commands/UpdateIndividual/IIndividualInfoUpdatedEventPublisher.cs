﻿using NexCore.ContactManager.Domain.Individuals;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual
{
    public interface IIndividualInfoUpdatedEventPublisher
    {
        Task Publish(Individual individual, CancellationToken cancellationToken);
    }
}
