﻿using AutoMapper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.Individuals;
using System;

namespace NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual
{
    public class DealerEmployeeNameUpdatedEventFactory : IDealerEmployeeNameUpdatedEventFactory
    {
        private readonly IMapper _mapper;

        public DealerEmployeeNameUpdatedEventFactory(IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public DealerEmployeeNameUpdatedEvent Create(int dealerEmployeeId, Individual individual)
        {
            if (individual == null)
                throw new ArgumentNullException(nameof(individual));

            return new DealerEmployeeNameUpdatedEvent
            {
                DealerEmployeeId = dealerEmployeeId,
                PersonName = _mapper.Map<PersonNameDto>(individual.Name),
            };
        }
    }
}
