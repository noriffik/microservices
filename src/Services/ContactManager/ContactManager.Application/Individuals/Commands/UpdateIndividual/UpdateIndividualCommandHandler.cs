﻿using AutoMapper;
using MediatR;
using NexCore.ContactManager.Application.Individuals.Policies;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual
{
    public class UpdateIndividualCommandHandler : IRequestHandler<UpdateIndividualCommand>
    {
        private readonly IIndividualInfoUpdatedEventPublisher _eventPublisher;
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;
        private readonly IIndividualDeduplicationRulePolicy _deduplicationPolicy;

        public UpdateIndividualCommandHandler(IUnitOfWork work, IMapper mapper,
            IIndividualInfoUpdatedEventPublisher eventPublisher, IIndividualDeduplicationRulePolicy deduplicationPolicy)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _eventPublisher = eventPublisher ?? throw new ArgumentNullException(nameof(eventPublisher));
            _deduplicationPolicy = deduplicationPolicy ?? throw new ArgumentNullException(nameof(deduplicationPolicy));
        }

        public async Task<Unit> Handle(UpdateIndividualCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var individual = await _work.EntityRepository.Require<Individual>(request.Id, cancellationToken);

            _mapper.Map(request, individual);

            await _deduplicationPolicy.Apply(individual, cancellationToken);

            await _eventPublisher.Publish(individual, cancellationToken);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
