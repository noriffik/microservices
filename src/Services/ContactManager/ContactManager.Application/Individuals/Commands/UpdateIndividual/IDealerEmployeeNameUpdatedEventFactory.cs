﻿using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.Individuals;

namespace NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual
{
    public interface IDealerEmployeeNameUpdatedEventFactory
    {
        DealerEmployeeNameUpdatedEvent Create(int dealerEmployeeId, Individual individual);
    }
}