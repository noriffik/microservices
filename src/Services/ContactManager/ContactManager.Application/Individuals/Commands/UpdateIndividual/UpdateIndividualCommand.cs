﻿using MediatR;
using NexCore.ContactManager.Application.Individuals.Dtos;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual
{
    [DataContract]
    public class UpdateIndividualCommand : IndividualFieldSet, IRequest
    {
        public int Id { get; set; }
    }
}
