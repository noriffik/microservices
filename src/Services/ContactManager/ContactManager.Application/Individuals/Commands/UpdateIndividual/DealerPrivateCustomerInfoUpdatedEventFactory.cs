﻿using AutoMapper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.Legal;
using System;

namespace NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual
{
    public class DealerPrivateCustomerInfoUpdatedEventFactory : IDealerPrivateCustomerInfoUpdatedEventFactory
    {
        private readonly IMapper _mapper;

        public DealerPrivateCustomerInfoUpdatedEventFactory(IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public DealerPrivateCustomerInfoUpdatedEvent Create(int customerId, Individual individual)
        {
            if (individual == null)
                throw new ArgumentNullException(nameof(individual));

            return new DealerPrivateCustomerInfoUpdatedEvent
            {
                DealerPrivateCustomerId = customerId,
                PersonName = _mapper.Map<PersonNameDto>(individual.Name),
                Passport = individual.Passport == Passport.Empty
                    ? null
                    : _mapper.Map<PassportDto>(individual.Passport),
                TaxNumber = individual.TaxNumber == TaxIdentificationNumber.Empty
                    ? null
                    : _mapper.Map<TaxIdentificationNumberDto>(individual.TaxNumber)
            };
        }
    }
}
