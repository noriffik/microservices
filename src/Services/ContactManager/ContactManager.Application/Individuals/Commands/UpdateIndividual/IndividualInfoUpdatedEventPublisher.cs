﻿using NexCore.ContactManager.Domain.Customers.Specifications;
using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.Employees.Specifications;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual
{
    public class IndividualInfoUpdatedEventPublisher : IIndividualInfoUpdatedEventPublisher
    {
        private readonly IEntityRepository _repository;
        private readonly IDealerPrivateCustomerInfoUpdatedEventFactory _customerEventFactory;
        private readonly IDealerEmployeeNameUpdatedEventFactory _employeeEventFactory;
        private readonly IIntegrationEventStorage _eventStorage;

        public IndividualInfoUpdatedEventPublisher(IEntityRepository repository,
            IDealerPrivateCustomerInfoUpdatedEventFactory customerEventFactory,
            IDealerEmployeeNameUpdatedEventFactory employeeEventFactory, IIntegrationEventStorage eventStorage)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _customerEventFactory = customerEventFactory ?? throw new ArgumentNullException(nameof(customerEventFactory));
            _employeeEventFactory = employeeEventFactory ?? throw new ArgumentNullException(nameof(employeeEventFactory));
            _eventStorage = eventStorage ?? throw new ArgumentNullException(nameof(eventStorage));
        }

        public async Task Publish(Individual individual, CancellationToken cancellationToken)
        {
            if (individual == null)
                throw new ArgumentNullException(nameof(individual));

            var customerEvents = await GetCustomerEvents(individual, cancellationToken);

            var employeeEvents = await GetEmployeeEvents(individual, cancellationToken);

            var events = customerEvents.Concat(employeeEvents);

            await _eventStorage.AddRange(events);
        }

        private async Task<IEnumerable<IntegrationEvent>> GetCustomerEvents(Individual individual, CancellationToken cancellationToken)
        {
            var privateCustomers = await _repository.Find(
                new ByContactableIdSpecification<DealerPrivateCustomer>(individual.Id), cancellationToken);

            return privateCustomers.Select(c => _customerEventFactory.Create(c.Id, individual));
        }

        private async Task<IEnumerable<IntegrationEvent>> GetEmployeeEvents(Individual individual, CancellationToken cancellationToken)
        {
            var employees = await _repository.Find(
                new EmployeeByIndividualIdSpecification<DealerEmployee>(individual.Id), cancellationToken);

            return employees.Select(c => _employeeEventFactory.Create(c.Id, individual));
        }
    }
}
