﻿using FluentValidation;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Providers;

namespace NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual
{
    public class UpdateIndividualCommandValidator : IndividualFieldSetValidator<UpdateIndividualCommand>
    {
        public UpdateIndividualCommandValidator(ILanguageProvider languageProvider) : base(languageProvider)
        {
            RuleFor(p => p.Id).GreaterThan(0);
        }
    }
}
