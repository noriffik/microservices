﻿using System.Runtime.Serialization;
using MediatR;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Individuals.Dtos;

namespace NexCore.ContactManager.Application.Individuals.Queries
{
    [DataContract]
    public class ByTelephoneQuery : IRequest<IndividualDto>
    {
        [DataMember]
        public TelephoneNumberDto Number { get; set; }
    }
}