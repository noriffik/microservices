﻿using MediatR;
using NexCore.ContactManager.Application.Individuals.Dtos;
using System.Collections.Generic;

namespace NexCore.ContactManager.Application.Individuals.Queries
{
    public class CommunicationMediumQuery : IRequest<IEnumerable<CommunicationMediumDto>>
    {
    }
}
