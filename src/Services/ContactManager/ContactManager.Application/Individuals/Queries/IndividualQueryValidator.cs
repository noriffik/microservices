﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Individuals.Queries
{
    public class IndividualQueryValidator : AbstractValidator<IndividualQuery>
    {
        public IndividualQueryValidator()
        {
            RuleFor(c => c.Telephone).Must(s => s.Length > 2).When(s => !string.IsNullOrEmpty(s.Telephone));
        }
    }
}
