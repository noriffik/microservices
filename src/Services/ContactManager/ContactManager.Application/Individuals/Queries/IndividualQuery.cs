﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Individuals.Dtos;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Individuals.Queries
{
    [DataContract]
    public class IndividualQuery : PagedRequest<IndividualDto>
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Telephone { get; set; }
    }
}
