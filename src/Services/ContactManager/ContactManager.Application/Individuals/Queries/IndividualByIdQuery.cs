﻿using System.Runtime.Serialization;
using MediatR;
using NexCore.ContactManager.Application.Individuals.Dtos;

namespace NexCore.ContactManager.Application.Individuals.Queries
{
    [DataContract]
    public class IndividualByIdQuery : IRequest<IndividualDto>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
