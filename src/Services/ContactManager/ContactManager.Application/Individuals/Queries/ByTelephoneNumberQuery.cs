﻿using System.Runtime.Serialization;
using MediatR;
using NexCore.ContactManager.Application.Individuals.Dtos;

namespace NexCore.ContactManager.Application.Individuals.Queries
{
    [DataContract]
    public class ByTelephoneNumberQuery : IRequest<IndividualDto>
    {
        [DataMember]
        public string Number { get; set; }
    }
}
