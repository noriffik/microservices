﻿using AutoMapper;
using NexCore.ContactManager.Domain.Dictionaries;

namespace NexCore.ContactManager.Application.Jobs.Commands.AddJob
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AddJobCommand, Job>()
                .ConvertUsing(s => new Job(s.Title));
        }
    }
}
