﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Jobs.Commands.AddJob
{
    public class AddJobCommandValidator : AbstractValidator<AddJobCommand>
    {
        public AddJobCommandValidator()
        {
            RuleFor(m => m.Title).NotEmpty();
        }
    }
}
