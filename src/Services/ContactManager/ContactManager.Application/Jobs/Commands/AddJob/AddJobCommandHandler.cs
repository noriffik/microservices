﻿using AutoMapper;
using MediatR;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Jobs.Commands.AddJob
{
    public class AddJobCommandHandler : IRequestHandler<AddJobCommand, int>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public AddJobCommandHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<int> Handle(AddJobCommand request, CancellationToken cancellationToken)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            var job = _mapper.Map<Job>(request);

            await _work.EntityRepository.Add(job);

            await _work.Commit(cancellationToken);

            return job.Id;
        }
    }
}
