﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Jobs.Commands.AddJob
{
    [DataContract]
    public class AddJobCommand : IRequest<int>
    {
        [DataMember]
        public string Title { get; set; }
    }
}
