﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Jobs.Queries
{
    [DataContract]
    public class JobByIdQuery : IRequest<JobDto>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
