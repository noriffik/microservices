﻿using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Jobs.Queries
{
    public class JobDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Title { get; set; }
    }
}
