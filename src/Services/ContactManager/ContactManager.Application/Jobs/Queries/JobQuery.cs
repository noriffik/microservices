﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Jobs.Queries
{
    [DataContract]
    public class JobQuery : IRequest<IEnumerable<JobDto>>
    {
    }
}
