﻿using MediatR;
using NexCore.ContactManager.Application.FamilyStatuses.Dtos;
using System.Collections.Generic;

namespace NexCore.ContactManager.Application.FamilyStatuses.Queries
{
    public class FamilyStatusQuery : IRequest<IEnumerable<FamilyStatusDto>>
    {
    }
}
