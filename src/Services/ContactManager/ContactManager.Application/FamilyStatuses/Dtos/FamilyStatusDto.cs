﻿using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.FamilyStatuses.Dtos
{
    public class FamilyStatusDto
    {
        [DataMember]
        public int? Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
