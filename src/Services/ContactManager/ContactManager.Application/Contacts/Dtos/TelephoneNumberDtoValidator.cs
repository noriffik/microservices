﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    public class TelephoneNumberDtoValidator : AbstractValidator<TelephoneNumberDto>
    {
        public TelephoneNumberDtoValidator()
        {
            RuleFor(m => m.CountryCode).NotEmpty().Matches(@"^\d+$");
            RuleFor(m => m.AreaCode).NotEmpty().Matches(@"^\d+$");
            RuleFor(m => m.LocalNumber).NotEmpty().Matches(@"^\d+$");
        }
    }
}
