﻿using NexCore.ContactManager.Domain.Contacts;
using NexCore.Domain;
using System;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    public class EmailAddressDtoMapper : BaseMapper<EmailAddressDto, IContactPoint<EmailAddress>>
    {
        public override IContactPoint<EmailAddress> Map(EmailAddressDto source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            return new ContactPoint<EmailAddress>(new EmailAddress(source.Address))
            {
                IsPrimary = source.IsPrimary,
                Description = source.Description
            };
        }
    }
}
