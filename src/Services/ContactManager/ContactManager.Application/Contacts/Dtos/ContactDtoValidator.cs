﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    public class ContactDtoValidator : AbstractValidator<ContactDto>
    {
        public ContactDtoValidator()
        {
            RuleForEach(p => p.Telephones).SetValidator(new TelephoneDtoValidator());
            RuleForEach(p => p.Emails).SetValidator(new EmailAddressDtoValidator());
            RuleForEach(p => p.Addresses).SetValidator(new PhysicalAddressDtoValidator());
        }
    }
}
