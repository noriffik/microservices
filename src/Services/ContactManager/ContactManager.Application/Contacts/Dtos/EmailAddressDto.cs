﻿using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    [DataContract]
    public class EmailAddressDto
    {
        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool IsPrimary { get; set; }
    }
}
