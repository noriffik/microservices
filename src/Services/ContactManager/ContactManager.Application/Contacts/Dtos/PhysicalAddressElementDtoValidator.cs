﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    public class PhysicalAddressElementDtoValidator : AbstractValidator<PhysicalAddressElementDto>
    {
        public static readonly PhysicalAddressElementDtoValidator Instance = new PhysicalAddressElementDtoValidator();

        public PhysicalAddressElementDtoValidator()
        {
            RuleFor(m => m.Name).NotEmpty();
        }
    }
}
