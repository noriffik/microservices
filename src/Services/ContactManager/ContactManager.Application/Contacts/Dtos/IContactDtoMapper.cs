﻿using NexCore.ContactManager.Domain.Contacts;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    public interface IContactDtoMapper
    {
        void Map(ContactDto model, IContactable contactable);
    }
}