﻿using NexCore.ContactManager.Domain.Contacts;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    [DataContract]
    public class TelephoneDto
    {        
        [DataMember]
        public TelephoneType? Type { get; set; }

        [DataMember]
        public TelephoneNumberDto Number { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool IsPrimary { get; set; }
    }
}
