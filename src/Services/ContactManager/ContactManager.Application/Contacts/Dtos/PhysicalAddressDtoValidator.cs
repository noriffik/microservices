﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    public class PhysicalAddressDtoValidator : AbstractValidator<PhysicalAddressDto>
    {
        public PhysicalAddressDtoValidator()
        {
            RuleFor(m => m.Street).NotEmpty();
            RuleFor(m => m.City).NotNull().SetValidator(PhysicalAddressElementDtoValidator.Instance);
            RuleFor(m => m.Region).NotNull().SetValidator(PhysicalAddressElementDtoValidator.Instance);
            RuleFor(m => m.Country).NotNull().SetValidator(PhysicalAddressElementDtoValidator.Instance);
            RuleFor(m => m.Geolocation).SetValidator(GeolocationDtoValidator.Instance);
            RuleFor(m => m.PostCode).NotEmpty();
        }
    }
}
