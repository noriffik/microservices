﻿using NexCore.ContactManager.Domain.Contacts;
using NexCore.Domain;
using System;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    public class PhysicalAddressDtoMapper : BaseMapper<PhysicalAddressDto, IContactPoint<PhysicalAddress>>
    {
        public override IContactPoint<PhysicalAddress> Map(PhysicalAddressDto source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            return new ContactPoint<PhysicalAddress>(
                new PhysicalAddress(
                    source.Street,
                    source.City?.Code, source.City?.Name,
                    source.Region?.Code, source.Region?.Name,
                    source.Country?.Code, source.Country?.Name,
                    source.Geolocation?.Latitude, source.Geolocation?.Longitude,
                    source.PostCode))
            {
                Description = source.Description,
                IsPrimary = source.IsPrimary
            };
        }
    }
}
