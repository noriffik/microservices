﻿using NexCore.ContactManager.Domain.Contacts;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    [DataContract]
    public class TelephoneNumberDto
    {
        [DataMember]
        public string CountryCode { get; set; }

        [DataMember]
        public string AreaCode { get; set; }

        [DataMember]
        public string LocalNumber { get; set; }

        [IgnoreDataMember]
        public string Number => Telephone.FormatAsNumber(CountryCode, AreaCode, LocalNumber);

        [IgnoreDataMember]
        public string E123 => Telephone.FormatAsE123(CountryCode, AreaCode, LocalNumber);
    }
}