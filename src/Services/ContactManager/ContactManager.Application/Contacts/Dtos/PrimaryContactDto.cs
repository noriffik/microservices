﻿using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    [DataContract]
    public class PrimaryContactDto
    {
        [DataMember]
        public TelephoneNumberDto TelephoneNumber { get; set; }

        [DataMember]
        public PhysicalAddressDto PhysicalAddress { get; set; }

        [DataMember]
        public EmailAddressDto EmailAddress { get; set; }
    }
}
