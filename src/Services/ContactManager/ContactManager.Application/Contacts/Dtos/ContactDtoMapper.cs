﻿using AutoMapper;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    public class ContactDtoMapper : IContactDtoMapper, ITypeConverter<ContactDto, IContactable>
    {
        private readonly IDictionary<Type, object> _mappers = new Dictionary<Type, object>
        {
            { typeof(PhysicalAddressDto), new PhysicalAddressDtoMapper() },
            { typeof(TelephoneDto), new TelephoneDtoMapper() },
            { typeof(EmailAddressDto), new EmailAddressDtoMapper() }
        };

        public IMapper<TModel, IContactPoint<TValue>> GetMapper<TModel, TValue>()
            where TModel : class where TValue : ValueObject
        {
            if (!_mappers.ContainsKey(typeof(TModel)))
                throw new NotSupportedException($"Mapper for give {typeof(TModel)} is not supported");

            return _mappers[typeof(TModel)] as IMapper<TModel, IContactPoint<TValue>>;
        }

        public void SetMapper<TModel, TValue>(IMapper<TModel, IContactPoint<TValue>> mapper)
            where TModel : class where TValue : ValueObject
        {
            _mappers[typeof(TModel)] = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public virtual void Map(ContactDto model, IContactable contactable)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (contactable == null)
                throw new ArgumentNullException(nameof(contactable));

            contactable.Contact.Clear<Telephone>();
            contactable.Contact.Clear<EmailAddress>();
            contactable.Contact.Clear<PhysicalAddress>();

            AssignTo<PhysicalAddressDto, PhysicalAddress>(model.Addresses, contactable);
            AssignTo<TelephoneDto, Telephone>(model.Telephones, contactable);
            AssignTo<EmailAddressDto, EmailAddress>(model.Emails, contactable);
        }

        private void AssignTo<TModel, TValue>(IEnumerable<TModel> models, IContactable contactable)
            where TModel : class where TValue : ValueObject
        {
            if (models == null)
                return;

            GetMapper<TModel, TValue>()
                .MapMany(models)
                .ToList()
                .ForEach(c => contactable.Contact.Add(c.Value, c.Description, c.IsPrimary));
        }

        public IContactable Convert(ContactDto source, IContactable destination, ResolutionContext context)
        {
            Map(source, destination);

            return destination;
        }
    }
}