﻿using AutoMapper;
using NexCore.ContactManager.Domain.Contacts;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ContactDto, IContactable>()
                .ConvertUsing(new ContactDtoMapper());

            CreateMap<ContactDto, ContactInfo>()
                .ForMember(d => d.Id, c => c.Ignore());
        }
    }
}
