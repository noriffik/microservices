﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    public class TelephoneDtoValidator : AbstractValidator<TelephoneDto>
    {
        public TelephoneDtoValidator()
        {
            RuleFor(m => m.Number).SetValidator(new TelephoneNumberDtoValidator());
            RuleFor(m => m.Type).IsInEnum();
        }
    }    
}
