﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    public class GeolocationDtoValidator : AbstractValidator<GeolocationDto>
    {
        public static readonly GeolocationDtoValidator Instance = new GeolocationDtoValidator();

        public GeolocationDtoValidator()
        {
            RuleFor(m => m.Latitude).NotEmpty().When(m => m.Longitude != null);
            RuleFor(m => m.Longitude).NotEmpty().When(m => m.Latitude != null);
        }
    }
}
