﻿using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    [DataContract]
    public class PhysicalAddressDto
    {
        [DataMember]
        public string Street { get; set; }

        [DataMember]
        public PhysicalAddressElementDto City { get; set; }

        [DataMember]
        public PhysicalAddressElementDto Region { get; set; }

        [DataMember]
        public PhysicalAddressElementDto Country { get; set; }

        [DataMember]
        public GeolocationDto Geolocation { get; set; }

        [DataMember]
        public string PostCode { get;set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool IsPrimary { get; set; }
    }
}