﻿using NexCore.ContactManager.Domain.Contacts;
using NexCore.Domain;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    public class TelephoneDtoMapper : BaseMapper<TelephoneDto, IContactPoint<Telephone>>
    {
        public override IContactPoint<Telephone> Map(TelephoneDto source)
        {
            if (source == null)
                throw new System.ArgumentNullException(nameof(source));

            return new ContactPoint<Telephone>(MapToTelephone(source))
            {
                IsPrimary = source.IsPrimary,
                Description = source.Description
            };
        }

        private static Telephone MapToTelephone(TelephoneDto model)
        {
            return new Telephone(
                model.Number.CountryCode,
                model.Number.AreaCode,
                model.Number.LocalNumber,
                model.Type);
        }
    }
}
