﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    [DataContract]
    public class ContactDto
    {
        [DataMember]
        public IEnumerable<TelephoneDto> Telephones { get; set; }

        [DataMember]
        public IEnumerable<EmailAddressDto> Emails { get; set; }

        [DataMember]
        public IEnumerable<PhysicalAddressDto> Addresses { get; set; }
    }
}