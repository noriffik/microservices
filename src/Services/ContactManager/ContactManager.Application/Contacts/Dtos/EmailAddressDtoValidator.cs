﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Contacts.Dtos
{
    public class EmailAddressDtoValidator : AbstractValidator<EmailAddressDto>
    {
        public EmailAddressDtoValidator()
        {
            RuleFor(m => m.Address).NotEmpty().EmailAddress();
        }
    }
}
