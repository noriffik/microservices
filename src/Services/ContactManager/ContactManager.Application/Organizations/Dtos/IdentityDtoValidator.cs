﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Organizations.Dtos
{
    public class IdentityDtoValidator : AbstractValidator<IdentityDto>
    {
        public IdentityDtoValidator()
        {
            RuleFor(m => m.Edrpou).NotEmpty().When(m => string.IsNullOrEmpty(m.Ipn));
            RuleFor(m => m.Edrpou).Empty().When(m => !string.IsNullOrEmpty(m.Ipn));
            RuleFor(m => m.Ipn).NotEmpty().When(m => string.IsNullOrEmpty(m.Edrpou));
            RuleFor(m => m.Ipn).Empty().When(m => !string.IsNullOrEmpty(m.Edrpou));
        }
    }
}
