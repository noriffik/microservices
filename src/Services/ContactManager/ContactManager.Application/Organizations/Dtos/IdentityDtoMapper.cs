﻿using AutoMapper;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.Domain;
using System;

namespace NexCore.ContactManager.Application.Organizations.Dtos
{
    public class IdentityDtoMapper : BaseMapper<IdentityDto, Identity>, IIdentityDtoMapper, ITypeConverter<IdentityDto, Identity>
    {
        public override Identity Map(IdentityDto source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            if (string.IsNullOrEmpty(source.Edrpou) && string.IsNullOrEmpty(source.Ipn))
                throw new InvalidOperationException("Code for identity are not set. Choose either Edrpou or Ipn.");

            if (!string.IsNullOrEmpty(source.Edrpou) && !string.IsNullOrEmpty(source.Ipn))
                throw new InvalidOperationException("Code for identity are set. Choose either Edrpou or Ipn.");
            
            return source.Edrpou != null ? Identity.WithEdrpou(source.Edrpou) : Identity.WithIpn(source.Ipn);
        }

        public Identity Convert(IdentityDto source, Identity destination, ResolutionContext context)
        {
            return Map(source);
        }
    }
}
