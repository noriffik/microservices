﻿using NexCore.ContactManager.Application.Individuals.Dtos;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Organizations.Dtos
{
    [DataContract]
    public class OrganizationRequisitesDto
    {
        [DataMember]
        public string ShortName { get; set; }

        [DataMember]
        public string FullName { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public IEnumerable<string> Telephones { get; set; }

        [DataMember]
        public string BankRequisites { get; set; }

        [DataMember]
        public PersonNameDto Director { get; set; }
    }
}
