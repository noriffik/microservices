﻿using NexCore.ContactManager.Domain.Organizations;
using NexCore.Domain;

namespace NexCore.ContactManager.Application.Organizations.Dtos
{
    public interface IOrganizationInfoDtoMapper : IMapper<OrganizationInfoDto, Organization>
    {
        void Map(OrganizationInfoDto destination, Organization source);
    }
}
