﻿using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Organizations.Dtos
{
    public class OrganizationInfoDto
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public IdentityDto Identity { get; set; }
    }
}
