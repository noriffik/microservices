﻿using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Organizations.Dtos
{
    public class IdentityDto
    {
        [DataMember]
        public string Edrpou { get; set; }

        [DataMember]
        public string Ipn { get; set; }
    }
}
