﻿using FluentValidation;

namespace NexCore.ContactManager.Application.Organizations.Dtos
{
    public class OrganizationInfoDtoValidator : AbstractValidator<OrganizationInfoDto>
    {
        public OrganizationInfoDtoValidator()
        {
            RuleFor(c => c.Name).NotEmpty();
            RuleFor(c => c.Identity).NotNull().SetValidator(new IdentityDtoValidator());
        }
    }
}
