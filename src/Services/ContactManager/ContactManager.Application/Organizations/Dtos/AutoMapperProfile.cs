﻿using AutoMapper;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.Legal;

namespace NexCore.ContactManager.Application.Organizations.Dtos
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<OrganizationInfoDto, Organization>();

            CreateMap<IdentityDto, Identity>()
                .ConvertUsing(new IdentityDtoMapper());

            CreateMap<OrganizationRequisitesDto, OrganizationRequisites>()
                .ConvertUsing(s => new OrganizationRequisites(new LegalOrganizationName(s.ShortName, s.FullName),
                    s.Address, s.Telephones, s.BankRequisites,
                    new LegalPersonName(s.Director.Firstname, s.Director.Lastname, s.Director.Middlename)));
        }
    }
}
