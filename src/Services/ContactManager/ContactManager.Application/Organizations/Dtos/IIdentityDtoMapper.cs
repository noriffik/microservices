﻿using NexCore.ContactManager.Domain.Organizations;
using NexCore.Domain;

namespace NexCore.ContactManager.Application.Organizations.Dtos
{
    public interface IIdentityDtoMapper : IMapper<IdentityDto, Identity>
    {
    }
}