﻿using AutoMapper;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.Domain;
using System;

namespace NexCore.ContactManager.Application.Organizations.Dtos
{
    public class OrganizationInfoDtoMapper : BaseMapper<OrganizationInfoDto, Organization>, IOrganizationInfoDtoMapper, ITypeConverter<OrganizationInfoDto, Organization>
    {
        private IIdentityDtoMapper _identityMapper;

        public IIdentityDtoMapper IdentityMapper
        {
            get => _identityMapper ?? (_identityMapper = new IdentityDtoMapper());
            set => _identityMapper = value ?? throw new ArgumentNullException(nameof(value));
        }

        public override Organization Map(OrganizationInfoDto destination)
        {
            if (destination == null)
                throw new ArgumentNullException(nameof(destination));

            return new Organization(destination.Name, IdentityMapper.Map(destination.Identity));
        }

        public void Map(OrganizationInfoDto destination, Organization source)
        {
            if (destination == null)
                throw new ArgumentNullException(nameof(destination));

            if (source == null)
                throw new ArgumentNullException(nameof(source));

            source.Name = destination.Name;
            source.Identity = IdentityMapper.Map(destination.Identity);
        }

        public Organization Convert(OrganizationInfoDto source, Organization destination, ResolutionContext context)
        {
            Map(source, destination);

            return destination;
        }
    }
}
