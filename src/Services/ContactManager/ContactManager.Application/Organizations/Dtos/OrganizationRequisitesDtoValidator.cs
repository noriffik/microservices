﻿using FluentValidation;
using NexCore.ContactManager.Application.Individuals.Dtos;

namespace NexCore.ContactManager.Application.Organizations.Dtos
{
    public class OrganizationRequisitesDtoValidator : AbstractValidator<OrganizationRequisitesDto>
    {
        public OrganizationRequisitesDtoValidator()
        {
            RuleFor(c => c.ShortName).NotEmpty();
            RuleFor(c => c.FullName).NotEmpty();
            RuleFor(c => c.Address).NotEmpty();
            RuleFor(c => c.Telephones).NotNull();
            RuleForEach(c => c.Telephones).NotEmpty();
            RuleFor(c => c.BankRequisites).NotEmpty();
            RuleFor(c => c.Director).NotNull().SetValidator(new LegalIndividualNameDtoValidator());
        }
    }
}
