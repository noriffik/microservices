﻿using AutoMapper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Organizations.Dtos;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.Legal;

namespace NexCore.ContactManager.Application.Organizations.Commands
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UpdateOrganizationCommand, Organization>()
                .ForMember(d => d.Identity, c => c.MapFrom(s => s.Identity))
                .ForMember(d => d.Name, c => c.MapFrom(s => s.Name))
                .ForMember(d => d.Contact, c => c.Ignore())
                .ForMember(d => d.Requisites, c => c.Ignore())
                .AfterMap((s, d) => new ContactDtoMapper().Map(s.Contact, d))
                .AfterMap((s, d) => d.ChangeRequisites(MapRequisites(s.Requisites)));
        }

        public static OrganizationRequisites MapRequisites(OrganizationRequisitesDto dto)
        {
            return dto == null
                ? OrganizationRequisites.Empty
                : new OrganizationRequisites(new LegalOrganizationName(dto.ShortName, dto.FullName),
                    dto.Address, dto.Telephones, dto.BankRequisites,
                    new LegalPersonName(dto.Director.Firstname, dto.Director.Lastname, dto.Director.Middlename));
        }
    }
}
