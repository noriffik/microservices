﻿using AutoMapper;
using MediatR;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Organizations.Commands
{
    public class UpdateOrganizationCommandHandler : IRequestHandler<UpdateOrganizationCommand>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public UpdateOrganizationCommandHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<Unit> Handle(UpdateOrganizationCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var organization = await _work.EntityRepository.Require<Organization>(request.Id, cancellationToken);

            _mapper.Map(request, organization);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
