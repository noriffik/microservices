﻿using FluentValidation;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Organizations.Dtos;

namespace NexCore.ContactManager.Application.Organizations.Commands
{
    public class UpdateOrganizationCommandValidator : AbstractValidator<UpdateOrganizationCommand>
    {
        public UpdateOrganizationCommandValidator()
        {
            RuleFor(p => p.Id).NotEmpty().GreaterThan(0);
            RuleFor(c => c.Name).NotEmpty();
            RuleFor(p => p.Identity).NotNull().SetValidator(new IdentityDtoValidator());
            RuleFor(c => c.Requisites).SetValidator(new OrganizationRequisitesDtoValidator());
            RuleFor(p => p.Contact).NotNull().SetValidator(new ContactDtoValidator());
        }
    }
}
