﻿using FluentValidation;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Organizations.Dtos;

namespace NexCore.ContactManager.Application.Organizations.Commands.AddOrganization
{
    public class AddOrganizationCommandValidator : AbstractValidator<AddOrganizationCommand>
    {
        public AddOrganizationCommandValidator()
        {
            RuleFor(c => c.Name).NotEmpty();
            RuleFor(p => p.Identity).NotNull().SetValidator(new IdentityDtoValidator());
            RuleFor(c => c.Requisites).SetValidator(new OrganizationRequisitesDtoValidator());
            RuleFor(p => p.Contact).NotNull().SetValidator(new ContactDtoValidator());
        }
    }
}
