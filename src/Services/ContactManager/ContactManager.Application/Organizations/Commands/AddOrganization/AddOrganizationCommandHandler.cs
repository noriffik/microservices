﻿using AutoMapper;
using MediatR;
using NexCore.Application.Extensions;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Application.Organizations.Commands.AddOrganization
{
    public class AddOrganizationCommandHandler : IRequestHandler<AddOrganizationCommand, int>
    {
        private readonly IUnitOfWork _work;
        private readonly IOrganizationRegistrationService _service;
        private readonly IMapper _mapper;

        public AddOrganizationCommandHandler(IUnitOfWork work, IOrganizationRegistrationService service, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<int> Handle(AddOrganizationCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var organization = _mapper.Map<Organization>(request);

            try
            {
                await _service.Register(organization);
            }
            catch (DuplicateIdentityException e)
            {
                throw ValidationExtensions.ExceptionForCommandHandler(
                    "Identity", "Organization with given identity already registered.", e.Identity);
            }

            await _work.Commit(cancellationToken);

            return organization.Id;
        }
    }
}
