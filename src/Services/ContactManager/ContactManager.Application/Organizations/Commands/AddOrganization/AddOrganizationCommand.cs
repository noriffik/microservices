﻿using MediatR;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Organizations.Dtos;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Organizations.Commands.AddOrganization
{
    [DataContract]
    public class AddOrganizationCommand : IRequest<int>
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public IdentityDto Identity { get; set; }

        [DataMember]
        public OrganizationRequisitesDto Requisites { get; set; }

        [DataMember]
        public ContactDto Contact { get; set; }
    }
}
