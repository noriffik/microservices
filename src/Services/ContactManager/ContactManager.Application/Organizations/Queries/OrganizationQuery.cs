﻿using NexCore.Application.Queries;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Organizations.Queries
{
    public class OrganizationQuery : PagedRequest<OrganizationDto>
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Edrpou { get; set; }

        [DataMember]
        public string Ipn { get; set; }
    }
}
