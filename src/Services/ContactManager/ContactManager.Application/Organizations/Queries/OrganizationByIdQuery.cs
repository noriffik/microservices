﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.ContactManager.Application.Organizations.Queries
{
    [DataContract]
    public class OrganizationByIdQuery : IRequest<OrganizationDto>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
