﻿using NexCore.Domain;

namespace NexCore.ContactManager.Domain.Customers
{
    public abstract class Customer : Entity, IAggregateRoot
    {
        public int ContactableId { get; protected set; }

        public int OrganizationId { get; protected set; }

        protected Customer() { }

        protected Customer(int contactableId, int organizationId) : this(0, contactableId, organizationId)
        {
        }

        protected Customer(int id, int contactableId, int organizationId) : base(id)
        {
            ContactableId = contactableId;
            OrganizationId = organizationId;
        }
    }
}
