﻿using NexCore.ContactManager.Domain.Contacts;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Domain.Customers.Private
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    public abstract class PrivateCustomer : Customer
    {
        public Telephone Telephone { get; private set; }

        public PhysicalAddress PhysicalAddress { get; private set; }

        protected PrivateCustomer()
        {
            Telephone = Telephone.Empty;
            PhysicalAddress = PhysicalAddress.Empty;
        }

        protected PrivateCustomer(int contactableId, int organizationId) : base(contactableId, organizationId)
        {
            Telephone = Telephone.Empty;
            PhysicalAddress = PhysicalAddress.Empty;
        }

        protected PrivateCustomer(int id, int contactableId, int organizationId) : base(id, contactableId, organizationId)
        {
            Telephone = Telephone.Empty;
            PhysicalAddress = PhysicalAddress.Empty;
        }

        public void UpdateTelephone(Telephone telephone)
        {
            Telephone = new Telephone(telephone ?? Telephone.Empty);
        }

        public void UpdatePhysicalAddress(PhysicalAddress physicalAddress)
        {
            PhysicalAddress.Assign(physicalAddress ?? PhysicalAddress.Empty);
        }
    }
}
