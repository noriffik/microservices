﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.Customers.Specifications
{
    public class ByOrganizationAndContactableSpecification<TCustomer> : Specification<TCustomer>
        where TCustomer : Customer
    {
        public int OrganizationId { get; }

        public int ContactableId { get; }

        private readonly Specification<TCustomer> _inner;

        public ByOrganizationAndContactableSpecification(int organizationId, int contactableId)
        {
            OrganizationId = organizationId;
            ContactableId = contactableId;

            _inner = new AndSpecification<TCustomer>(
                new ByOrganizationIdSpecification<TCustomer>(organizationId),
                new ByContactableIdSpecification<TCustomer>(contactableId));
        }

        public override Expression<Func<TCustomer, bool>> ToExpression() => _inner.ToExpression();

        public override string Description => $"{typeof(TCustomer).Name} for organization with id {OrganizationId}";
    }
}
