﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.Customers.Specifications
{
    public class ByOrganizationIdSpecification<TCustomer> : Specification<TCustomer>
        where TCustomer : Customer
    {
        public ByOrganizationIdSpecification(int organizationId)
        {
            OrganizationId = organizationId;
        }

        public int OrganizationId { get; }

        public override Expression<Func<TCustomer, bool>> ToExpression()
        {
            return customer => customer.OrganizationId == OrganizationId;
        }

        public override string Description => $"{typeof(TCustomer).Name} for organization with id {OrganizationId}";
    }
}
