﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.Customers.Specifications
{
    public class ByContactableIdSpecification<TCustomer> : Specification<TCustomer>
        where TCustomer : Customer
    {
        public int ContactableId { get; }

        public ByContactableIdSpecification(int contactableId)
        {
            ContactableId = contactableId;
        }

        public override Expression<Func<TCustomer, bool>> ToExpression()
        {
            return customer => customer.ContactableId == ContactableId;
        }

        public override string Description => $"{typeof(TCustomer).Name} with contactableId {ContactableId}";
    }
}
