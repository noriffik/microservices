﻿using System.Threading.Tasks;

namespace NexCore.ContactManager.Domain.Customers
{
    public interface ICustomerRegistrationService<in TCustomer> where TCustomer : Customer
    {
        Task Register(TCustomer customer);
    }
}