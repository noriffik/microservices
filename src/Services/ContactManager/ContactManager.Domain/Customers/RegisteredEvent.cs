﻿using NexCore.Domain;
using System;

namespace NexCore.ContactManager.Domain.Customers
{
    public class RegisteredEvent<TCustomer> : IEntityEvent where TCustomer : Customer
    {
        public TCustomer Customer { get; }

        public RegisteredEvent(TCustomer customer)
        {
            Customer = customer ?? throw new ArgumentNullException(nameof(customer));
        }
    }
}