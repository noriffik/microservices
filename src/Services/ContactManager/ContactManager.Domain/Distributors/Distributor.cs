﻿using NexCore.DealerNetwork;
using NexCore.Domain;
using System;

namespace NexCore.ContactManager.Domain.Distributors
{
    public class Distributor : Entity<DistributorId>, IAggregateRoot<DistributorId>
    {
        public int OrganizationId { get; private set; }

        public string CountryCode { get; private set; }

        private Distributor()
        {
        }

        public Distributor(DistributorId id, int organizationId, string countryCode) : base(id)
        {
            OrganizationId = organizationId;
            CountryCode = countryCode?.ToUpperInvariant() ?? throw new ArgumentNullException(nameof(countryCode));
        }

        public virtual void ChangeCountryCode(string countryCode)
        {
            CountryCode = countryCode?.ToUpperInvariant() ?? throw new ArgumentNullException(nameof(countryCode));
        }
    }
}
