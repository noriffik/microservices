﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.Distributors.Specifications
{
    public class ByOrganizationIdSpecification : Specification<Distributor>
    {
        public int OrganizationId { get; }

        public ByOrganizationIdSpecification(int organizationId)
        {
            OrganizationId = organizationId;
        }

        public override Expression<Func<Distributor, bool>> ToExpression()
        {
            return distributor => distributor.OrganizationId == OrganizationId;
        }

        public override string Description => $"Distributor for organization with id {OrganizationId}";
    }
}
