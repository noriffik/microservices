﻿using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.DealerPrivateCustomers.Specifications
{
    public class ByDealerIdSpecification : Specification<DealerPrivateCustomer>
    {
        public DealerId DealerId { get; }

        public ByDealerIdSpecification(DealerId dealerId)
        {
            DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
        }

        public override Expression<Func<DealerPrivateCustomer, bool>> ToExpression()
        {
            return customer => Convert.ToString(customer.DealerId) == DealerId.Value;
        }

        public override string Description => $"DealerPrivateCustomer with DealerId {DealerId.Value}";
    }
}
