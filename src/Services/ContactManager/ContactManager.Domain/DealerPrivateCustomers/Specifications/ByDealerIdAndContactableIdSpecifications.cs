﻿using NexCore.ContactManager.Domain.Customers.Specifications;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.DealerPrivateCustomers.Specifications
{
    public class ByDealerIdAndContactableIdSpecifications : Specification<DealerPrivateCustomer>
    {
        public DealerId DealerId { get; }

        public int ContactableId { get; }

        private readonly Specification<DealerPrivateCustomer> _inner;

        public ByDealerIdAndContactableIdSpecifications(DealerId dealerId, int contactableId)
        {
            DealerId = dealerId;
            ContactableId = contactableId;

            _inner = new AndSpecification<DealerPrivateCustomer>(new ByDealerIdSpecification(dealerId),
                new ByContactableIdSpecification<DealerPrivateCustomer>(contactableId));
        }

        public override Expression<Func<DealerPrivateCustomer, bool>> ToExpression() => _inner.ToExpression();

        public override string Description => $"DealerPrivateCustomer with DealerId {DealerId.Value}" +
                                              $"and ContactableId {ContactableId}";
    }
}
