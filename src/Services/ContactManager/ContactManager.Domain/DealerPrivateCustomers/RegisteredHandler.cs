﻿using MediatR;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Customers;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using NexCore.ContactManager.Domain.Individuals;

namespace NexCore.ContactManager.Domain.DealerPrivateCustomers
{
    public class RegisteredHandler : INotificationHandler<RegisteredEvent<DealerPrivateCustomer>>
    {
        private readonly IUnitOfWork _work;

        public RegisteredHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Handle(RegisteredEvent<DealerPrivateCustomer> notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            var individual = await _work.EntityRepository.Require<Individual>(notification.Customer.ContactableId, cancellationToken);
            var address = individual.Contact.GetPrimary<PhysicalAddress>();
            var telephone = individual.Contact.GetPrimary<Telephone>();

            if (telephone != null)
                notification.Customer.UpdateTelephone(telephone.Value);

            if (address != null)
                notification.Customer.UpdatePhysicalAddress(address.Value);
        }
    }
}
