﻿using NexCore.ContactManager.Domain.Customers.Private;
using NexCore.DealerNetwork;
using System;

namespace NexCore.ContactManager.Domain.DealerPrivateCustomers
{
    public class DealerPrivateCustomer : PrivateCustomer
    {
        public DealerId DealerId { get; private set; }

        private DealerPrivateCustomer()
        {
        }

        public DealerPrivateCustomer(int contactableId, int organizationId, DealerId dealerId)
            : base(contactableId, organizationId)
        {
            DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
        }

        public DealerPrivateCustomer(int id, int contactableId, int organizationId, DealerId dealerId)
            : base(id, contactableId, organizationId)
        {
            DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
        }
    }
}
