﻿using NexCore.DealerNetwork;
using NexCore.Domain;
using System;

namespace NexCore.ContactManager.Domain.DealerPrivateCustomers
{
    public class DuplicateDealerPrivateCustomerException : DomainException
    {
        private const string DefaultMessage = "Private customer is already registered for given dealer";
        private const string DefaultExtendedMessage =
            "Individual with id {0} is already private customer for dealer with id {1}";

        public DealerId DealerId { get; }

        public int ContactableId { get; }

        public DuplicateDealerPrivateCustomerException()
            : base(DefaultMessage)
        {
        }

        public DuplicateDealerPrivateCustomerException(string message) : base(message)
        {
        }

        public DuplicateDealerPrivateCustomerException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public DuplicateDealerPrivateCustomerException(DealerId dealerId, int contactableId)
            : this(string.Format(DefaultExtendedMessage, contactableId, dealerId.Value), dealerId, contactableId)
        {
        }

        public DuplicateDealerPrivateCustomerException(string message, DealerId dealerId, int contactableId)
            : this(message, dealerId, contactableId, null)
        {
        }

        public DuplicateDealerPrivateCustomerException(string message, DealerId dealerId, int contactableId, Exception innerException)
            : base(message, innerException)
        {
            DealerId = dealerId;
            ContactableId = contactableId;
        }
    }
}