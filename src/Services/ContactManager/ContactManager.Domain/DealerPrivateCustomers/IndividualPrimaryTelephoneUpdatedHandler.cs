﻿using MediatR;
using NexCore.ContactManager.Domain.Customers.Specifications;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using NexCore.ContactManager.Domain.Individuals;

namespace NexCore.ContactManager.Domain.DealerPrivateCustomers
{
    public class IndividualPrimaryTelephoneUpdatedHandler : INotificationHandler<PrimaryTelephoneUpdatedEvent>
    {
        private readonly IUnitOfWork _work;

        public IndividualPrimaryTelephoneUpdatedHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Handle(PrimaryTelephoneUpdatedEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            var customers = await _work.EntityRepository.Find(
                new ByContactableIdSpecification<DealerPrivateCustomer>(notification.ContactableId),
                cancellationToken);

            foreach (var customer in customers)
                customer.UpdateTelephone(notification.Telephone);
        }
    }
}
