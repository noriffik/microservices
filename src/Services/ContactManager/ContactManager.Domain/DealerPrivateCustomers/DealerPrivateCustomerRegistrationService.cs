﻿using NexCore.ContactManager.Domain.Customers;
using NexCore.ContactManager.Domain.DealerPrivateCustomers.Specifications;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Domain.DealerPrivateCustomers
{
    public class DealerPrivateCustomerRegistrationService : ICustomerRegistrationService<DealerPrivateCustomer>
    {
        private readonly IUnitOfWork _work;

        public DealerPrivateCustomerRegistrationService(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Register(DealerPrivateCustomer customer)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            await _work.EntityRepository.HasRequired<Organization>(customer.OrganizationId);
            await _work.EntityRepository.HasRequired<Individual>(customer.ContactableId);
            await _work.EntityRepository.HasRequired<Dealer, DealerId>(customer.DealerId);
            await ValidateDuplicate(customer);

            await _work.EntityRepository.Add(customer);
            customer.AddEntityEvent(new RegisteredEvent<DealerPrivateCustomer>(customer));
        }

        private async Task ValidateDuplicate(DealerPrivateCustomer customer)
        {
            var isRegisteredSpecification = new ByDealerIdAndContactableIdSpecifications(customer.DealerId, customer.ContactableId);
            if (await _work.EntityRepository.Has(isRegisteredSpecification))
                throw new DuplicateDealerPrivateCustomerException(customer.DealerId, customer.ContactableId);
        }
    }
}
