﻿using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.DealerEmployees.Specifications
{
    public class ByDealerIdAndEmployeeIdSpecification : Specification<DealerEmployee>
    {
        public DealerId DealerId { get; }

        public int DealerEmployeeId { get; }

        private readonly Specification<DealerEmployee> _inner;

        public ByDealerIdAndEmployeeIdSpecification(DealerId dealerId, int dealerEmployeeId)
        {
            DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
            DealerEmployeeId = dealerEmployeeId;

            _inner = new AndSpecification<DealerEmployee>(
                new ByDealerIdSpecification(dealerId), new IdSpecification<DealerEmployee>(dealerEmployeeId));
        }

        public override Expression<Func<DealerEmployee, bool>> ToExpression() => _inner.ToExpression();

        public override string Description => $"Employee with id {DealerEmployeeId} of Dealer with id {DealerId} ";
    }
}
