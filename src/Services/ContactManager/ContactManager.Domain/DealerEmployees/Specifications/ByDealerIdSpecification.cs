﻿using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.DealerEmployees.Specifications
{
    public class ByDealerIdSpecification : Specification<DealerEmployee>
    {
        public DealerId DealerId { get; }

        public ByDealerIdSpecification(DealerId dealerId)
        {
            DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
        }

        public override Expression<Func<DealerEmployee, bool>> ToExpression()
        {
            return employee => Convert.ToString(employee.DealerId) == DealerId.Value;
        }

        public override string Description => $"Employee of Dealer with id {DealerId.Value}";
    }
}
