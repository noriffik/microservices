﻿using System;
using NexCore.ContactManager.Domain.Employees;
using NexCore.DealerNetwork;

namespace NexCore.ContactManager.Domain.DealerEmployees
{
	public class DealerEmployee : Employee
	{
		public DealerId DealerId { get; private set; }

		private DealerEmployee()
		{
		}

		public DealerEmployee(int individualId, int organizationId, DealerId dealerId, int jobId)
			: base(individualId, organizationId, jobId)
		{
			DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
		}

		public DealerEmployee(int id, int individualId, int organizationId, DealerId dealerId, int jobId)
			: base(id, individualId, organizationId, jobId)
		{
			DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
		}
	}
}