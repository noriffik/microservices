﻿using NexCore.Domain;
using System;

namespace NexCore.ContactManager.Domain.Employees
{
    public class DuplicateEmploymentException : DomainException
    {
        private const string DefaultMessage = "Individual is already employeed.";
        private const string DefaultExtendedMessage = "Individual with id {0} is already employee of organization with id {1}.";

        public int IndividualId { get; }

        public int OrganizationId { get; }

        public DuplicateEmploymentException() : base(DefaultMessage)
        {
        }

        public DuplicateEmploymentException(string message) : base(message)
        {
        }

        public DuplicateEmploymentException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public DuplicateEmploymentException(int individualId, int organizationId)
            : this(string.Format(DefaultExtendedMessage, individualId, organizationId), individualId, organizationId)
        {
        }

        public DuplicateEmploymentException(string message, int individualId, int organizationId)
            : this(message, individualId, organizationId, null)
        {
        }

        public DuplicateEmploymentException(string message, int individualId, int organizationId, Exception innerException)
            : base(message, innerException)
        {
            IndividualId = individualId;
            OrganizationId = organizationId;
        }
    }
}
