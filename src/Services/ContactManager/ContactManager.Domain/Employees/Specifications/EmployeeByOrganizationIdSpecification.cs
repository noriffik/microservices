﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.Employees.Specifications
{
    public class EmployeeByOrganizationIdSpecification<TEmployee> : Specification<TEmployee>
        where TEmployee : Employee
    {
        public int OrganizationId { get; }

        public EmployeeByOrganizationIdSpecification(int organizationId)
        {
            OrganizationId = organizationId;
        }

        public override Expression<Func<TEmployee, bool>> ToExpression() =>
            employee => employee.OrganizationId == OrganizationId;

        public override string Description => $"{typeof(TEmployee).Name} for organization with id {OrganizationId}";
    }
}
