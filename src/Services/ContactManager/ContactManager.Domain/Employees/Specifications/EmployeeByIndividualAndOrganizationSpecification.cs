﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.Employees.Specifications
{
    public class EmployeeByIndividualAndOrganizationSpecification<TEmployee> : Specification<TEmployee>
        where TEmployee : Employee
    {
        public int IndividualId { get; }

        public int OrganizationId { get; }

        private readonly Specification<TEmployee> _inner;

        public EmployeeByIndividualAndOrganizationSpecification(int individualId, int organizationId)
        {
            IndividualId = individualId;
            OrganizationId = organizationId;

            _inner = new AndSpecification<TEmployee>(
                new EmployeeByIndividualIdSpecification<TEmployee>(individualId),
                new EmployeeByOrganizationIdSpecification<TEmployee>(organizationId));
        }

        public override Expression<Func<TEmployee, bool>> ToExpression() => _inner.ToExpression();

        public override string Description => $"{typeof(TEmployee).Name} for individual with id {IndividualId} and organization with id {OrganizationId}";
    }
}
