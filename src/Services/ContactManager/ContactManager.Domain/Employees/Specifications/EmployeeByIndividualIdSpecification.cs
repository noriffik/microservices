﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.Employees.Specifications
{
    public class EmployeeByIndividualIdSpecification<TEmployee> : Specification<TEmployee> where TEmployee : Employee
    {
        public int IndividualId { get; }

        public EmployeeByIndividualIdSpecification(int individualId)
        {
            IndividualId = individualId;
        }

        public override Expression<Func<TEmployee, bool>> ToExpression() =>
            employee => employee.IndividualId == IndividualId;

        public override string Description => $"{typeof(TEmployee).Name} for individual with id {IndividualId}";
    }
}
