﻿using NexCore.Domain;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Domain.Employees
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public abstract class Employee : Entity, IAggregateRoot
    {
        public int IndividualId { get; private set; }

        public int OrganizationId { get; private set; }

        public int JobId { get; private set; }

        protected Employee()
        {
        }

        protected Employee(int individualId, int organizationId, int jobId)
        {
            IndividualId = individualId;
            OrganizationId = organizationId;
            JobId = jobId;
        }

        protected Employee(int id, int individualId, int organizationId, int jobId)
            : this(individualId, organizationId, jobId)
        {
            Id = id;
        }

        public virtual void ChangeJobId(int jobId)
        {
            JobId = jobId;
        }
    }
}
