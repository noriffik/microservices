﻿using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace NexCore.ContactManager.Domain.Languages
{
    public class Language : ValueObject
    {
        public Language(string threeLetterIsoCode, string twoLetterIsoCode, string name)
        {
            ThreeLetterIsoCode = threeLetterIsoCode ?? throw new ArgumentNullException(nameof(threeLetterIsoCode));
            TwoLetterIsoCode = twoLetterIsoCode ?? throw new ArgumentNullException(nameof(twoLetterIsoCode));
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public string ThreeLetterIsoCode { get; }

        public string TwoLetterIsoCode { get; }

        public string Name { get; }

        protected override IEnumerable<object> GetValues()
        {
            yield return ThreeLetterIsoCode;
        }

        public static Language From(CultureInfo culture)
        {
            if (culture == null)
                throw new ArgumentNullException(nameof(culture));

            return new Language(culture.ThreeLetterISOLanguageName, culture.TwoLetterISOLanguageName, culture.Name);
        }
    }
}
