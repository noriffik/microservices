﻿using NexCore.Domain;
using System;

namespace NexCore.ContactManager.Domain.Leads
{
    public class DisqualificationReason : Entity, IAggregateRoot
    {
        public string Name { get; private set;  }

        protected DisqualificationReason()
        {
        }

        public DisqualificationReason(string name)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public DisqualificationReason(int id, string name) : base(id)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public virtual void ChangeName(string name)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }
    }
}
