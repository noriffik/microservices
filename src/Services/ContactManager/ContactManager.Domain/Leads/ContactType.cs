﻿namespace NexCore.ContactManager.Domain.Leads
{
    public enum ContactType
    {
        Private = 0,
        Corporate = 1
    }
}
