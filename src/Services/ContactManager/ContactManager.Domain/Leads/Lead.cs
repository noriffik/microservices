﻿using NexCore.Common;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;

namespace NexCore.ContactManager.Domain.Leads
{
    public class Lead : Entity, IAggregateRoot
    {
        public DealerId DealerId { get; private set; }

        public DealerCode DealerCode { get; private set; }

        public DistributorId DistributorId { get; private set; }

        public int TopicId { get; private set; }

        public Contact Contact { get; private set; }

        public Status Status { get; private set; }

        public int? OwnerEmployeeId { get; private set; }

        public DateTime CreatedAt { get; private set; }

        public DateTime UpdatedAt { get; private set; }

        private Lead()
        {
        }

        public Lead(DealerId dealerId, Contact contact, int topicId)
        {
            DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
            DistributorId = dealerId.DistributorId;
            DealerCode = dealerId.DealerCode;
            TopicId = topicId;
            Contact = contact ?? throw new ArgumentNullException(nameof(contact));

            Status = Status.Open;
            CreatedAt = SystemTimeProvider.Instance.Get();
            UpdatedAt = CreatedAt;
        }

        public Lead(int id, DealerId dealerId, Contact contact, int topicId)
            : this(dealerId, contact, topicId)
        {
            Id = id;
        }

        public void AssignOwner(int? employeeId)
        {
            OwnerEmployeeId = employeeId;

            UpdatedAt = SystemTimeProvider.Instance.Get();
        }

        public void ChangeStatus(Status status)
        {
            Status = status;
        }
    }
}
