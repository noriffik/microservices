﻿using NexCore.Common;
using NexCore.Domain;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Domain.Leads
{
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Needed for EF Core")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Needed for EF Core")]
    public class InterestSource : Entity, IAggregateRoot
    {
        public string Name { get; private set; }

        public DateTime? DateArchived { get; private set; }

        public virtual bool IsArchived => DateArchived.HasValue;

        private InterestSource()
        {
        }

        public InterestSource(string name)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public InterestSource(int id, string name) : base(id)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public virtual void ChangeName(string name)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public virtual void Archive()
        {
            DateArchived = SystemTimeProvider.Instance.Get();
        }
    }
}
