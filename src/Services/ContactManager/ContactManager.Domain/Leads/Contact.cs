﻿using NexCore.Domain;
using System.Collections.Generic;

namespace NexCore.ContactManager.Domain.Leads
{
    public class Contact : ValueObject
    {
        public int IndividualId { get; private set; }

        public int? OrganizationId { get; private set; }

        public ContactType Type { get; private set; }

        public Contact(int individualId, int? organizationId)
        {
            IndividualId = individualId;
            OrganizationId = organizationId;
            Type = organizationId.HasValue ? ContactType.Corporate : ContactType.Private;
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return IndividualId;
            yield return OrganizationId;
        }
    }
}
