﻿namespace NexCore.ContactManager.Domain.Leads
{
    public enum Status
    {
        Open = 0,
        Contacted = 1,
        Qualified = 2,
        Disqualified = 3,
    }
}
