﻿using NexCore.Domain;
using System;

namespace NexCore.ContactManager.Domain.Leads
{
    public class Topic : Entity, IAggregateRoot
    {
        public string Name { get; private set; }

        protected Topic()
        {
        }

        public Topic(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Value cannot be null or empty", nameof(name));

            Name = name;
        }

        public Topic(int id, string name) : base(id)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Value cannot be null or empty", nameof(name));

            Name = name;
        }

        public virtual void ChangeName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Value cannot be null or empty", nameof(name));

            Name = name;
        }
    }
}
