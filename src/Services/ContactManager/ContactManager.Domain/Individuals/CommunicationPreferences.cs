﻿using NexCore.Domain;
using System;

namespace NexCore.ContactManager.Domain.Individuals
{
    public class CommunicationPreferences : Entity
    {
        public CommunicationMedium PreferredMedium { get; private set; }

        public CommunicationMediumSet BannedMediums { get; private set; }

        public CommunicationPreferences()
        {
            BannedMediums = new CommunicationMediumSet();
        }

        public CommunicationPreferences(CommunicationMediumSet bannedMediums)
        {
            BannedMediums = bannedMediums ?? throw new ArgumentNullException(nameof(bannedMediums));
        }

        public void PermitMedium(CommunicationMedium medium)
        {
            if (medium == null)
                throw new ArgumentNullException(nameof(medium));

            BannedMediums = BannedMediums.Shrink(medium);
        }

        public void BanMedium(CommunicationMedium medium)
        {
            if (medium == null)
                throw new ArgumentNullException(nameof(medium));

            if (medium == PreferredMedium)
                throw new PreferredMediumCannotBeBannedException();

            BannedMediums = BannedMediums.Expand(medium);
        }

        public void ChangePreferredMedium(CommunicationMedium medium)
        {
            if (medium == null)
                throw new ArgumentNullException(nameof(medium));

            if (BannedMediums.Has(medium))
                throw new PreferredMediumCannotBeBannedException();

            PreferredMedium = medium;
        }

        public void ClearPreferredMedium()
        {
            PreferredMedium = null;
        }

        public void ChangeBannedMediums(CommunicationMediumSet mediums)
        {
            if (mediums == null)
                throw new ArgumentNullException(nameof(mediums));

            if (PreferredMedium != null && mediums.Has(PreferredMedium))
                throw new PreferredMediumCannotBeBannedException();

            BannedMediums = mediums;
        }

        public void ClearBannedMediums()
        {
            BannedMediums = CommunicationMediumSet.Empty;
        }
    }
}