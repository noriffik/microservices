﻿using NexCore.ContactManager.Domain.Contacts;
using NexCore.Domain;

namespace NexCore.ContactManager.Domain.Individuals
{
    public class PrimaryTelephoneUpdatedEvent : IEntityEvent
    {
        public int ContactableId { get; set; }

        public Telephone Telephone { get; set; }
    }
}