﻿using NexCore.ContactManager.Domain.Contacts;
using NexCore.Domain;
using NexCore.Legal;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Domain.Individuals
{
    public class Individual : Entity, IAggregateRoot, IContactable
    {
        private PersonName _name;

        private ContactInfo _contact;

        public PersonalPreferences Preferences { get; private set; }

        public FamilyStatus FamilyStatus { get; private set; }

        public int? Children { get; private set; }

        public Passport Passport { get; private set; }

        public TaxIdentificationNumber TaxNumber { get; private set; }

        public PersonName Name
        {
            get => _name;
            set => _name = value ?? throw new ArgumentNullException(nameof(value));
        }

        [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
        public ContactInfo Contact
        {
            get => _contact;
            private set
            {
                if (_contact != null)
                    _contact.PrimaryUpdatedEvent -= ContactUpdated;

                _contact = value;
                _contact.PrimaryUpdatedEvent += ContactUpdated;
            }
        }

        public DateTime? Birthday { get; set; }

        public Gender? Gender { get; set; }

        private Individual()
        {
            Contact = new ContactInfo();
            Passport = Passport.Empty;
            TaxNumber = TaxIdentificationNumber.Empty;
            Preferences = new PersonalPreferences();
        }

        public Individual(PersonName name) : this()
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public Individual(int id, PersonName name) : this(name)
        {
            Id = id;
        }

        public void ChangePassport(Passport passport)
        {
            if (passport == null)
                throw new ArgumentNullException(nameof(passport));

            Passport.Assign(passport);
        }

        public void ChangeTaxNumber(TaxIdentificationNumber number)
        {
            if (number == null)
                throw new ArgumentNullException(nameof(number));

            TaxNumber.Assign(number);
        }

        public void UpdateFamilyStatus(FamilyStatus familyStatus)
        {
            FamilyStatus = familyStatus;
        }

        public void UpdateChildrenStatus(int? positive)
        {
            Children = positive.GetValueOrDefault();
        }

        private void ContactUpdated(object sender, PrimaryContactPointUpdatedEventArgs eventArgs)
        {
            if (eventArgs.ContactValue is Telephone telephone)
                AddEntityEvent(new PrimaryTelephoneUpdatedEvent
                {
                    ContactableId = Id,
                    Telephone = telephone
                });

            if (eventArgs.ContactValue is PhysicalAddress physicalAddress)
                AddEntityEvent(new PrimaryPhysicalAddressUpdateEvent
                {
                    ContactableId = Id,
                    PhysicalAddress = physicalAddress
                });
        }
    }

    public enum Gender
    {
        Female,
        Male
    }

}