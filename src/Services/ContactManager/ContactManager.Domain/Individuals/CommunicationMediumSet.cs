﻿using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.ContactManager.Domain.Individuals
{
    public class CommunicationMediumSet : ValueObject, IValueSet<CommunicationMedium>
    {
        private BaseValueSet<CommunicationMedium> _inner;

        public static CommunicationMediumSet Empty => new CommunicationMediumSet();

        public CommunicationMediumSet()
        {
            _inner = new BaseValueSet<CommunicationMedium>(CommunicationMedium.Get);
        }

        public CommunicationMediumSet(IValueSet<CommunicationMedium> valueSet)
        {
            _inner = new BaseValueSet<CommunicationMedium>(valueSet, CommunicationMedium.Get);
        }

        public CommunicationMediumSet(params CommunicationMedium[] values)
        {
            _inner = new BaseValueSet<CommunicationMedium>(values, CommunicationMedium.Get);
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return AsString;
        }

        public string AsString
        {
            get => _inner.AsString;
            private set => _inner = new BaseValueSet<CommunicationMedium>(value, CommunicationMedium.Get);
        }

        public IEnumerable<CommunicationMedium> Values => _inner.Values;

        public static CommunicationMediumSet Parse(string value)
        {
            return new CommunicationMediumSet(BaseValueSet<CommunicationMedium>.Parse(value, CommunicationMedium.Get));
        }

        public static CommunicationMediumSet Parse(IEnumerable<string> values)
        {
            if (values == null)
                throw new ArgumentNullException(nameof(values));

            return new CommunicationMediumSet(values.Select(CommunicationMedium.Get).ToArray());
        }

        public CommunicationMediumSet Expand(CommunicationMedium medium)
        {
            if (medium == null)
                throw new ArgumentNullException(nameof(medium));

            return new CommunicationMediumSet(_inner.Expand(medium.Id));
        }

        public CommunicationMediumSet Shrink(CommunicationMedium medium)
        {
            if (medium == null)
                throw new ArgumentNullException(nameof(medium));

            return new CommunicationMediumSet(_inner.Shrink(medium.Id));
        }

        public bool Has(CommunicationMedium medium)
        {
            if (medium == null)
                throw new ArgumentNullException(nameof(medium));

            return _inner.Values.Contains(medium);
        }
    }
}