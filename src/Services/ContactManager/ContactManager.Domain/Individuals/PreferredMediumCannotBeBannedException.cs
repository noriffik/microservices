﻿using NexCore.Domain;
using System;

namespace NexCore.ContactManager.Domain.Individuals
{
    public class PreferredMediumCannotBeBannedException : DomainException
    {
        public PreferredMediumCannotBeBannedException() : this("Предпочтительное средство связи не может быть в списке запрещенных.")
        {
        }

        public PreferredMediumCannotBeBannedException(string message) : base(message)
        {
        }

        public PreferredMediumCannotBeBannedException(string message, Exception innerException) : base(message,
            innerException)
        {
        }
    }
}