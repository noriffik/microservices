﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.ContactManager.Domain.Individuals
{
    public class CommunicationMedium
    {
        public string Id { get; private set; }

        public string Name { get; private set; }

        public static readonly CommunicationMedium Email = new CommunicationMedium("EMAIL", "Email");

        public static readonly CommunicationMedium Telegram = new CommunicationMedium("TELEGRAM", "Telegram");

        public static readonly CommunicationMedium Viber = new CommunicationMedium("VIBER", "Viber");

        public static readonly CommunicationMedium Telephone = new CommunicationMedium("TELEPHONE", "Telephone");

        public static IEnumerable<CommunicationMedium> All => AllInner.ToList().AsReadOnly();

        private static readonly IEnumerable<CommunicationMedium> AllInner = new List<CommunicationMedium>
        {
            Email,
            Telegram,
            Viber,
            Telephone
        };

        private CommunicationMedium()
        {
        }

        private CommunicationMedium(string id, string name)
        {
            Id = id.ToUpperInvariant();
            Name = name;
        }

        public static CommunicationMedium Get(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            return Has(id) ? All.Single(c => c.Id == id.ToUpperInvariant()) : throw new ArgumentOutOfRangeException();
        }

        public static bool Has(string id)
        {
            return All.Any(i => i.Id == id.ToUpperInvariant());
        }

        public override bool Equals(object obj)
        {
            return obj is CommunicationMedium other && other.Id.ToUpperInvariant() == Id;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(GetType(), Id);
        }

        public override string ToString()
        {
            return $"{Name}";
        }

        public static bool operator ==(CommunicationMedium left, CommunicationMedium right)
        {
            return left?.Equals(right) ?? Equals(right, null);
        }

        public static bool operator !=(CommunicationMedium left, CommunicationMedium right)
        {
            return !(left == right);
        }
    }
}
