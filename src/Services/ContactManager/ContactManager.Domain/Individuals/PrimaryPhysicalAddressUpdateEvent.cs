﻿using NexCore.ContactManager.Domain.Contacts;
using NexCore.Domain;

namespace NexCore.ContactManager.Domain.Individuals
{
    public class PrimaryPhysicalAddressUpdateEvent : IEntityEvent
    {
         public int ContactableId { get; set; }

         public PhysicalAddress PhysicalAddress { get; set; }
    }
}
