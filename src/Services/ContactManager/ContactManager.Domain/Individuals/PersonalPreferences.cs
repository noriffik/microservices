﻿using NexCore.Domain;
using System;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("NexCore.ContactManager.UnitTests")]
[assembly: InternalsVisibleTo("NexCore.ContactManager.FunctionalTests")]
namespace NexCore.ContactManager.Domain.Individuals
{
    public class PersonalPreferences : Entity
    {
        public string Language { get; private set; }

        public CommunicationPreferences Communications { get; private set; }

        internal PersonalPreferences()
        {
            Communications = new CommunicationPreferences();
        }

        public void ChangeLanguage(string language)
        {
            if (language == null)
                throw new ArgumentNullException(nameof(language));

            ThrowIfIsNotValidIsoCode(language, nameof(language));

            Language = language;
        }

        private static void ThrowIfIsNotValidIsoCode(string code, string paramName)
        {
            var isValid = CultureInfo.GetCultures(CultureTypes.NeutralCultures)
                .Select(c => c.ThreeLetterISOLanguageName)
                .Contains(code);

            if (isValid) return;

            throw new ArgumentException("Given value is not a valid ISO code", paramName);
        }
    }
}