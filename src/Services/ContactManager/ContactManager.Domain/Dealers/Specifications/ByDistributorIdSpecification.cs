﻿using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.Dealers.Specifications
{
    public class ByDistributorIdSpecification : Specification<Dealer>
    {
        public DistributorId DistributorId { get; }

        public ByDistributorIdSpecification(DistributorId distributorId)
        {
            DistributorId = distributorId ?? throw new ArgumentNullException(nameof(distributorId));
        }

        public override Expression<Func<Dealer, bool>> ToExpression()
        {
            return dealer => Convert.ToString(dealer.DistributorId) == DistributorId.Value;
        }

        public override string Description => $"Dealer for Distributor with id {DistributorId}";
    }
}
