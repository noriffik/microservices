﻿using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.Dealers.Specifications
{
    public class ByOrganizationAndDistributorSpecification : Specification<Dealer>
    {
        public int OrganizationId { get; }

        public DistributorId DistributorId { get; }

        private readonly Specification<Dealer> _inner;

        public ByOrganizationAndDistributorSpecification(int organizationId, DistributorId distributorId)
        {
            OrganizationId = organizationId;
            DistributorId = distributorId ?? throw new ArgumentNullException(nameof(distributorId));

            _inner = new AndSpecification<Dealer>(new ByOrganizationIdSpecification(organizationId),
                new ByDistributorIdSpecification(DistributorId));
        }

        public override Expression<Func<Dealer, bool>> ToExpression() => _inner.ToExpression();

        public override string Description => $"Dealer for Organization with id {OrganizationId}" +
                                              $"and Distributor with id {DistributorId}";

    }
}
