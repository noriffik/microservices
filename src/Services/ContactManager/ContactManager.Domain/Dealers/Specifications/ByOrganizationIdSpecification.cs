﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.Dealers.Specifications
{
    public class ByOrganizationIdSpecification : Specification<Dealer>
    {
        public int OrganizationId { get; }

        public ByOrganizationIdSpecification(int organizationId)
        {
            OrganizationId = organizationId;
        }

        public override Expression<Func<Dealer, bool>> ToExpression()
        {
            return dealer => dealer.OrganizationId == OrganizationId;
        }

        public override string Description => $"Dealer for organization with id {OrganizationId}";
    }
}
