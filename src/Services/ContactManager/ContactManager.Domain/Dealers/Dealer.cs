﻿using NexCore.DealerNetwork;
using NexCore.Domain;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Domain.Dealers
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class Dealer : Entity<DealerId>, IAggregateRoot<DealerId>
    {
        public int OrganizationId { get; private set; }

        public DistributorId DistributorId { get; private set; }

        public DealerCode DealerCode { get; private set; }

        public string CityCode { get; private set; }

        private Dealer()
        {
        }

        public Dealer(DealerId id, int organizationId)
            : base(id)
        {
            OrganizationId = organizationId;
            DistributorId = id.DistributorId;
            DealerCode = id.DealerCode;
        }

        public virtual void ChangeCityCode(string cityCode)
        {
            CityCode = cityCode;
        }
    }
}
