﻿using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Domain.Dealers
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class Units : ValueObject
    {
        public interface IUnitsBuilder
        {
            IUnitsBuilderFactory HasService();
            IUnitsBuilderFactory HasSales();
        }

        public interface IUnitsBuilderFactory : IUnitsBuilder
        {
            Units Create();
        }

        class UnitsBuilderBuilder : IUnitsBuilderFactory
        {
            private readonly Units _units = new Units();

            public IUnitsBuilderFactory HasService()
            {
                _units.Service = true;

                return this;
            }

            public IUnitsBuilderFactory HasSales()
            {
                _units.Sales = true;

                return this;
            }

            public Units Create()
            {
                return _units;
            }
        }

        public static readonly Units Empty = new Units();

        public bool Sales { get; private set; }
        public bool Service { get; private set; }

        private Units()
        {
        }

        public Units(bool sales, bool service)
        {
            if (!sales && !service)
            {
                throw new InvalidOperationException("At least one of unit required");
            }

            Sales = sales;
            Service = service;
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return Sales;
            yield return Service;
        }

        public override string ToString() =>
            (UnitToString(Sales, nameof(Sales)) + UnitToString(Service, nameof(Service)))
            .TrimEnd(',', ' ');
        
        private static string UnitToString(bool value, string name)
        {
            return value ? name + ", " : "";
        }

        public static IUnitsBuilder Build()
        {
            return new UnitsBuilderBuilder();
        }
    }
}

