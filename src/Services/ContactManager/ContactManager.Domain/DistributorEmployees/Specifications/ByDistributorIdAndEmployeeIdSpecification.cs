﻿using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.DistributorEmployees.Specifications
{
    public class ByDistributorIdAndEmployeeIdSpecification : Specification<DistributorEmployee>
    {
        public DistributorId DistributorId { get; }

        public int EmployeeId { get; }

        public ByDistributorIdAndEmployeeIdSpecification(DistributorId distributorId, int employeeId)
        {
            DistributorId = distributorId ?? throw new ArgumentNullException(nameof(distributorId));
            EmployeeId = employeeId;
        }

        public override Expression<Func<DistributorEmployee, bool>> ToExpression()
        {
            return (employee) => employee.DistributorId == DistributorId && employee.Id == EmployeeId;
        }

        public override string Description =>
            $"Employee by distributor id {DistributorId} and employee id {EmployeeId}";
    }
}
