﻿using NexCore.ContactManager.Domain.Employees;
using NexCore.DealerNetwork;
using System;

namespace NexCore.ContactManager.Domain.DistributorEmployees
{
    public class DistributorEmployee : Employee
    {
        public DistributorId DistributorId { get; private set; }

        private DistributorEmployee()
        {
        }

        public DistributorEmployee(int individualId, int organizationId, DistributorId distributorId, int jobId)
            : base(individualId, organizationId, jobId)
        {
            DistributorId = distributorId ?? throw new ArgumentNullException(nameof(distributorId));
        }

        public DistributorEmployee(int id, int individualId, int organizationId, DistributorId distributorId, int jobId)
            : base(id, individualId, organizationId, jobId)
        {
            DistributorId = distributorId ?? throw new ArgumentNullException(nameof(distributorId));
        }
    }
}
