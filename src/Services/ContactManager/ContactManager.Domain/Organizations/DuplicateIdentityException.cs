﻿using System;

namespace NexCore.ContactManager.Domain.Organizations
{
    public class DuplicateIdentityException : Exception
    {
        private const string DefaultMessage = "Organization with given identity already registered.";

        public DuplicateIdentityException() : base(DefaultMessage)
        {
        }

        public DuplicateIdentityException(string message) : base(message)
        {
        }

        public DuplicateIdentityException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public DuplicateIdentityException(string message, string identity, Exception innerException) : base(message, innerException)
        {
            Identity = identity;
        }

        public DuplicateIdentityException(string message, string identity) : this(message, identity, null)
        {
        }

        public string Identity { get; set; }
    }
}
