﻿using System.Threading.Tasks;

namespace NexCore.ContactManager.Domain.Organizations
{
    public interface IOrganizationRegistrationService
    {
        Task Register(Organization organization);
    }
}