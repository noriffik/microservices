﻿using NexCore.ContactManager.Domain.Contacts;
using NexCore.Domain;
using NexCore.Legal;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Domain.Organizations
{
    public class Organization : Entity, IAggregateRoot, IContactable
    {
        private string _name;
        private Identity _identity;

        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException("Value cannot be null or empty.", nameof(value));

                _name = value;
            }
        }

        public Identity Identity
        {
            get => _identity;
            set => _identity = value ?? throw new ArgumentNullException(nameof(value));
        }

        [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
        public ContactInfo Contact { get; private set; }

        public OrganizationRequisites Requisites { get; private set; }

        private Organization()
        {
            Contact = new ContactInfo();
            Requisites = OrganizationRequisites.Empty;
        }

        public Organization(string name, Identity identity) : this()
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Value cannot be null or empty.", nameof(name));

            _name = name;
            _identity = identity ?? throw new ArgumentNullException(nameof(identity));
        }

        public Organization(int id, string name, Identity identity) : this(name, identity)
        {
            Id = id;
        }

        public void ChangeRequisites(OrganizationRequisites requisites)
        {
            if (requisites == null)
                throw new ArgumentNullException(nameof(requisites));

            Requisites.Assign(requisites);
        }
    }
}
