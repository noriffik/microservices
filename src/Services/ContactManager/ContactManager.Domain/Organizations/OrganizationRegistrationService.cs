﻿using NexCore.ContactManager.Domain.Organizations.Specifications;
using NexCore.Domain;
using System;
using System.Threading.Tasks;

namespace NexCore.ContactManager.Domain.Organizations
{
    public class OrganizationRegistrationService : IOrganizationRegistrationService
    {
        private readonly IUnitOfWork _work;

        public OrganizationRegistrationService(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Register(Organization organization)
        {
            if (await _work.EntityRepository.Has(new ByIdentitySpecification(organization.Identity)))
                throw new DuplicateIdentityException
                {
                    Identity = organization.Identity.ToString()
                };

            await _work.EntityRepository.Add(organization);
        }
    }
}
