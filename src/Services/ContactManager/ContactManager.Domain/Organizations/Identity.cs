﻿using NexCore.Domain;
using System;
using System.Collections.Generic;

namespace NexCore.ContactManager.Domain.Organizations
{
    public class Identity : ValueObject
    {
        public string Edrpou { get; private set; }

        public string Ipn { get; private set; }

        private Identity()
        {
        }
        
        public static Identity WithEdrpou(string code)
        {
            ThrowIfGivenCodeIsNullOrEmpty(code);

            return new Identity
            {
                Edrpou = code
            };
        }

        private static void ThrowIfGivenCodeIsNullOrEmpty(string code)
        {
            if (string.IsNullOrEmpty(code))
                throw new ArgumentException("Value cannot be null or empty.", nameof(code));
        }

        public static Identity WithIpn(string code)
        {
            ThrowIfGivenCodeIsNullOrEmpty(code);

            return new Identity
            {
                Ipn = code
            };
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return Edrpou;
            yield return Ipn;
        }
    }
}
