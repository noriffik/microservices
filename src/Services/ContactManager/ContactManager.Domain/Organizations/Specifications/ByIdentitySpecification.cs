﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.Organizations.Specifications
{
    public class ByIdentitySpecification : Specification<Organization>
    {
        public Identity Identity { get; }

        public ByIdentitySpecification(Identity identity)
        {
            Identity = identity ?? throw new ArgumentNullException(nameof(identity));
        }

        public override Expression<Func<Organization, bool>> ToExpression()
        {
            return organization => Identity.Ipn != null
                ? organization.Identity.Ipn == Identity.Ipn
                : organization.Identity.Edrpou == Identity.Edrpou;
        }

        public override string Description
        {
            get
            {
                var identity = Identity.Ipn != null
                    ? nameof(Identity.Ipn) + " " + Identity.Ipn
                    : nameof(Identity.Edrpou) + " " + Identity.Edrpou;

                return $"Organization with {identity}";
            }
        }
    }
}
