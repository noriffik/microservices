﻿using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.DealerCorporateCustomers.Specifications
{
    public class ByDealerIdAndContactableIdSpecification : Specification<DealerCorporateCustomer>
    {
        public int ContactableId { get; }

        public DealerId DealerId { get; }

        public ByDealerIdAndContactableIdSpecification(int contactableId, DealerId dealerId)
        {
            ContactableId = contactableId;
            DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
        }

        public override Expression<Func<DealerCorporateCustomer, bool>> ToExpression()
        {
            return corporateCustomer =>
                corporateCustomer.DealerId == DealerId && corporateCustomer.ContactableId == ContactableId;
        }

        public override string Description =>
            $"Dealer corporate customer by contactable id {ContactableId} and dealer id {DealerId}";
    }
}
