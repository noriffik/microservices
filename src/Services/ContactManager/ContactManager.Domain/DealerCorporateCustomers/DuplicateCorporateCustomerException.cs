﻿using System;
using NexCore.DealerNetwork;
using NexCore.Domain;

namespace NexCore.ContactManager.Domain.DealerCorporateCustomers
{
    public class DuplicateDealerCorporateCustomerException : DomainException
    {
        private const string DefaultMessage = "Corporate customer is already registered for given dealer";
        private const string DefaultExtendedMessage =
            "Organization with id {0} is already corporate customer for dealer with id {1}";

        public DealerId DealerId { get; }

        public int ContactableId { get; }

        public DuplicateDealerCorporateCustomerException()
            : base(DefaultMessage)
        {
        }

        public DuplicateDealerCorporateCustomerException(string message) : base(message)
        {
        }

        public DuplicateDealerCorporateCustomerException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public DuplicateDealerCorporateCustomerException(DealerId dealerId, int contactableId)
            : this(string.Format(DefaultExtendedMessage, contactableId, dealerId.Value), dealerId, contactableId)
        {
        }

        public DuplicateDealerCorporateCustomerException(string message, DealerId dealerId, int contactableId)
            : this(message, dealerId, contactableId, null)
        {
        }

        public DuplicateDealerCorporateCustomerException(string message, DealerId dealerId, int contactableId, Exception innerException)
            : base(message, innerException)
        {
            DealerId = dealerId;
            ContactableId = contactableId;
        }
    }
}