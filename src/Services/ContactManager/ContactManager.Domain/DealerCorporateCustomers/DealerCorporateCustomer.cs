﻿using NexCore.ContactManager.Domain.Customers;
using NexCore.DealerNetwork;
using System;

namespace NexCore.ContactManager.Domain.DealerCorporateCustomers
{
    public class DealerCorporateCustomer : Customer
    {
        public DealerId DealerId { get; private set; }

        public DealerCorporateCustomer(int contactableId, int organizationId, DealerId dealerId) 
            : base(contactableId, organizationId)
        {
            DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
        }

        public DealerCorporateCustomer(int id, int contactableId, int organizationId, DealerId dealerId) 
            : base(id, contactableId, organizationId)
        {
            DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
        }
    }
}
