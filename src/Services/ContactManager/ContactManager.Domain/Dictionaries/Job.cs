﻿using System;
using NexCore.Domain;

namespace NexCore.ContactManager.Domain.Dictionaries
{
    public class Job : Entity, IAggregateRoot
    {
        public string Title { get; set; }

        public Job(string title)
        {
            if (string.IsNullOrEmpty(title))
                throw new ArgumentException(nameof(title));

            Title = title;
        }

        public Job(int id, string title) : this(title)
        {
            Id = id;
        }
    }
}