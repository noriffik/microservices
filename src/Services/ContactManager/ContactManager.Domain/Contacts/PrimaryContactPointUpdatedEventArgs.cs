﻿using NexCore.Domain;
using System;

namespace NexCore.ContactManager.Domain.Contacts
{
    public class PrimaryContactPointUpdatedEventArgs : EventArgs
    {
        public ValueObject ContactValue { get; set; }
    }
}