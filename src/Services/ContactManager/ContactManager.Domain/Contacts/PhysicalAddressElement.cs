﻿using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Domain.Contacts
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class PhysicalAddressElement : ValueObject
    {
        private PhysicalAddressElement()
        {
        }

        public PhysicalAddressElement(string value)
        {
            Value = value;
        }

        public static PhysicalAddressElement Empty => new PhysicalAddressElement();

        public string Value { get; private set; }

        protected override IEnumerable<object> GetValues()
        {
            yield return Value;
        }

        public override string ToString()
        {
            return Value ?? string.Empty;
        }

        public void Assign(PhysicalAddressElement element)
        {
            if (element == null)
                throw new ArgumentNullException(nameof(element));

            Value = element.Value;
        }
    }
}
