﻿using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Domain.Contacts
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    public class PhysicalAddress : ValueObject
    {
        public interface IPhysicalAddressBuilder
        {
            IPhysicalAddressBuilder WithStreet(string street);
            IPhysicalAddressBuilder WithCity(string name, string code = null);
            IPhysicalAddressBuilder WithRegion(string name, string code = null);
            IPhysicalAddressBuilder WithCountry(string name, string code = null);
            IPhysicalAddressBuilder WithGeolocation(double latitude, double longitude);
            IPhysicalAddressBuilder WithPostCode(string postCode);

            PhysicalAddress Create();
        }

        class PhysicalAddressBuilder : IPhysicalAddressBuilder
        {
            private PhysicalAddressElement _street = PhysicalAddressElement.Empty;
            private PhysicalAddressCodedElement _city = PhysicalAddressCodedElement.Empty;
            private PhysicalAddressCodedElement _region = PhysicalAddressCodedElement.Empty;
            private PhysicalAddressCodedElement _country = PhysicalAddressCodedElement.Empty;
            private Geolocation _geolocation = Geolocation.Empty;
            private PhysicalAddressElement _postCode = PhysicalAddressElement.Empty;

            public IPhysicalAddressBuilder WithStreet(string street)
            {
                _street = new PhysicalAddressElement(street);

                return this;
            }

            public IPhysicalAddressBuilder WithCity(string name, string code)
            {
                _city = new PhysicalAddressCodedElement(code, name);

                return this;
            }

            public IPhysicalAddressBuilder WithRegion(string name, string code)
            {
                _region = new PhysicalAddressCodedElement(code, name);

                return this;
            }

            public IPhysicalAddressBuilder WithCountry(string name, string code)
            {
                _country = new PhysicalAddressCodedElement(code, name);

                return this;
            }

            public IPhysicalAddressBuilder WithGeolocation(double latitude, double longitude)
            {
                _geolocation = new Geolocation(latitude, longitude);

                return this;
            }

            public IPhysicalAddressBuilder WithPostCode(string postCode)
            {
                _postCode = new PhysicalAddressElement(postCode);

                return this;
            }

            public PhysicalAddress Create()
            {
                return new PhysicalAddress
                {
                    Street = _street,
                    City = _city,
                    Region = _region,
                    Country = _country,
                    Geolocation = _geolocation,
                    PostCode = _postCode
                };
            }
        }

        private string _fullAddress;

        public PhysicalAddressElement Street { get; private set; }
        public PhysicalAddressCodedElement City { get; private set; }
        public PhysicalAddressCodedElement Region { get; private set; }
        public PhysicalAddressCodedElement Country { get; private set; }
        public Geolocation Geolocation { get; private set; }
        public PhysicalAddressElement PostCode { get; private set; }

        public static PhysicalAddress Empty => new PhysicalAddress();

        public string FullAddress
        {
            get => _fullAddress ?? (_fullAddress = ToString());
            private set => _fullAddress = value;
        }

        [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
        internal PhysicalAddress()
        {
            Street = PhysicalAddressElement.Empty;
            City = PhysicalAddressCodedElement.Empty;
            Region = PhysicalAddressCodedElement.Empty;
            Country = PhysicalAddressCodedElement.Empty;
            PostCode = PhysicalAddressElement.Empty;
            Geolocation = Geolocation.Empty;
        }

        public PhysicalAddress(
            string street,
            string cityCode, string cityName,
            string regionCode, string regionName,
            string countryCode, string countryName,
            double? latitude, double? longitude,
            string postCode)
        {
            Street = new PhysicalAddressElement(street);
            City = new PhysicalAddressCodedElement(cityCode, cityName);
            Region = new PhysicalAddressCodedElement(regionCode, regionName);
            Country = new PhysicalAddressCodedElement(countryCode, countryName);
            Geolocation = new Geolocation(latitude, longitude);
            PostCode = new PhysicalAddressElement(postCode);
        }

        public PhysicalAddress(PhysicalAddress physicalAddress)
        {
            if (physicalAddress == null)
                throw new ArgumentNullException(nameof(physicalAddress));

            Street = new PhysicalAddressElement(physicalAddress.Street.Value);
            City = new PhysicalAddressCodedElement(physicalAddress.City.Code, physicalAddress.City.Value);
            Region = new PhysicalAddressCodedElement(physicalAddress.Region.Code, physicalAddress.Region.Value);
            Country = new PhysicalAddressCodedElement(physicalAddress.Country.Code, physicalAddress.Country.Value);
            Geolocation = new Geolocation(physicalAddress.Geolocation.Latitude, physicalAddress.Geolocation.Longitude);
            PostCode = new PhysicalAddressElement(physicalAddress.PostCode.Value);
        }

        public void Assign(PhysicalAddress address)
        {
            if (address == null)
                throw new ArgumentNullException(nameof(address));

            Street.Assign(address.Street);
            City.Assign(address.City);
            Region.Assign(address.Region);
            Country.Assign(address.Country);
            Geolocation.Assign(address.Geolocation);
            PostCode.Assign(address.PostCode);
            _fullAddress = ToString();
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return Street.ToString();
            yield return City.ToString();
            yield return Region.ToString();
            yield return Country.ToString();
            yield return Geolocation.ToString();
            yield return PostCode.ToString();
        }

        public static IPhysicalAddressBuilder Build()
        {
            return new PhysicalAddressBuilder();
        }
    }
}
