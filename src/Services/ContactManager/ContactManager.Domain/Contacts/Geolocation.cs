﻿using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Domain.Contacts
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class Geolocation : ValueObject
    {
        public double? Latitude { get; private set; }
        public double? Longitude { get; private set; }

        private Geolocation()
        {
        }

        public Geolocation(double? latitude, double? longitude)
        {
            if ((latitude == null && longitude != null) || longitude == null && latitude != null)
            {
                throw new InvalidOperationException("Both coordinates are required");
            }

            Latitude = latitude;
            Longitude = longitude;
        }

        public static Geolocation Empty => new Geolocation();

        protected override IEnumerable<object> GetValues()
        {
            yield return Latitude;
            yield return Longitude;
        }

        public void Assign(Geolocation geolocation)
        {
            if (geolocation == null)
                throw new ArgumentNullException(nameof(geolocation));

            Latitude = geolocation.Latitude;
            Longitude = geolocation.Longitude;
        }

        public override string ToString() =>
            Latitude != null ? $"{Latitude}, {Longitude}" : string.Empty;
    }
}
