﻿using NexCore.Domain;

namespace NexCore.ContactManager.Domain.Contacts
{
    public interface IContactPoint<out TValue> where TValue : ValueObject
    {
        string Description { get; }

        TValue Value { get; }

        bool IsPrimary { get; }
    }
}
