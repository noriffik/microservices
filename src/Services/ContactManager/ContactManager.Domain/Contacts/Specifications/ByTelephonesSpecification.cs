﻿using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.Contacts.Specifications
{
    public class ByTelephonesSpecification<TContactable> : Specification<TContactable>
        where TContactable : IContactable
    {
        public IEnumerable<Telephone> Telephones { get; }

        public ByTelephonesSpecification(IEnumerable<Telephone> telephones)
        {
            Telephones = telephones ?? throw new ArgumentNullException(nameof(telephones));
        }

        public ByTelephonesSpecification(IEnumerable<IContactPoint<Telephone>> points)
        {
            if (points == null)
                throw new ArgumentNullException(nameof(points));

            Telephones = points.Select(t => t.Value);
        }

        public override Expression<Func<TContactable, bool>> ToExpression()
        {
            var numbers = Telephones.Select(t => t.Number);

            return contactable => contactable.Contact.TelephonesInternal.Select(t => t.Value.Number)
                .Any(t => numbers.Contains(t));
        }

        public override string Description => "Entities which include telephones:" +
                                              $"{Telephones.Select(t => t.Number).Aggregate((a, b) => $"{a} {b}")}";
    }
}
