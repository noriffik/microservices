﻿using NexCore.Domain;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.Contacts.Specifications
{
    public class ByTelephoneSpecification<TContactable> : Specification<TContactable>
        where TContactable : IContactable
    {
        public Telephone Telephone { get; }

        public ByTelephoneSpecification(Telephone telephone)
        {
            Telephone = telephone;
        }

        public override Expression<Func<TContactable, bool>> ToExpression()
        {
            return contactable => contactable.Contact.TelephonesInternal.Select(t => t.Value.Number).Contains(Telephone.Number);
        }

        public override string Description => $"{typeof(TContactable).Name} with telephone {Telephone.Number}";
    }
}
