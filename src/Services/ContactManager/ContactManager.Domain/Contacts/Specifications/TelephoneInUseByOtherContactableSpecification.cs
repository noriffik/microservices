﻿using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.ContactManager.Domain.Contacts.Specifications
{
    public class TelephoneInUseByOtherContactableSpecification<TContactable> : Specification<TContactable>
        where TContactable : Entity, IContactable
    {
        public int ContactableId { get; }

        public IEnumerable<Telephone> Telephones { get; }

        private readonly Specification<TContactable> _inner;

        public TelephoneInUseByOtherContactableSpecification(int contactableId, IEnumerable<Telephone> telephones)
        {
            ContactableId = contactableId;
            Telephones = telephones ?? throw new ArgumentNullException(nameof(telephones));

            _inner = new AndSpecification<TContactable>(
                new ByTelephonesSpecification<TContactable>(telephones),
                new NotSpecification<TContactable>(new IdSpecification<TContactable>(contactableId)));
        }

        public override Expression<Func<TContactable, bool>> ToExpression() => _inner.ToExpression();

        public override string Description => "Entities which include telephones:" +
                                              $"{Telephones.Select(t => t.Number).Aggregate((a, b) => $"{a} {b}")}" +
                                              $", except contactable with id {ContactableId}";
    }
}
