using System;

namespace NexCore.ContactManager.Domain.Contacts
{
    public class DuplicateContactPointException : Exception
    {
        public DuplicateContactPointException(string message) : base(message)
        {
        }
    }
}