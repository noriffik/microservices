﻿using NexCore.Domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.ContactManager.Domain.Contacts
{
    public class ContactInfo : Entity
    {
        private readonly IDictionary<Type, IList> _containPoints;
        internal readonly List<ContactPoint<PhysicalAddress>> PhysicalAddressesInternal =
            new List<ContactPoint<PhysicalAddress>>();
        internal readonly List<ContactPoint<Telephone>> TelephonesInternal =
            new List<ContactPoint<Telephone>>();
        internal readonly List<ContactPoint<EmailAddress>> EmailAddressesInternal =
            new List<ContactPoint<EmailAddress>>();

        public event EventHandler<PrimaryContactPointUpdatedEventArgs> PrimaryUpdatedEvent;

        public ContactInfo()
        {
            _containPoints = new Dictionary<Type, IList> {
                { typeof(PhysicalAddress), PhysicalAddressesInternal},
                { typeof(Telephone), TelephonesInternal},
                { typeof(EmailAddress), EmailAddressesInternal}
            };
        }

        public ContactInfo(int id) : this()
        {
            Id = id;
        }

        public IReadOnlyCollection<IContactPoint<PhysicalAddress>> PhysicalAddresses =>
            GetContactPointsInternal<PhysicalAddress>().AsReadOnly();

        public IReadOnlyCollection<IContactPoint<Telephone>> Telephones =>
            GetContactPointsInternal<Telephone>().AsReadOnly();

        public IReadOnlyCollection<IContactPoint<EmailAddress>> EmailAddresses =>
            GetContactPointsInternal<EmailAddress>().AsReadOnly();

        public void Add<TValue>(TValue value, string description = null, bool isPrimary = false)
            where TValue : ValueObject
        {
            if (QueryContactPoints<TValue>().Select(x => x.Value).Contains(value))
                throw new DuplicateContactPointException("Contact already exists");

            var hadAny = QueryContactPoints<TValue>().Any();

            _containPoints[typeof(TValue)].Add(
                new ContactPoint<TValue>(value) { Description = description });

            if (!hadAny || isPrimary) SetPrimary(value);
        }

        public IEnumerable<ContactPoint<TValue>> GetContactPoints<TValue>() where TValue : ValueObject =>
            GetContactPointsInternal<TValue>();

        private List<ContactPoint<TValue>> GetContactPointsInternal<TValue>() where TValue : ValueObject
        {
            ThrowIfUnsupported<TValue>();

            return (List<ContactPoint<TValue>>)_containPoints[typeof(TValue)];
        }

        private void ThrowIfUnsupported<TValue>() where TValue : ValueObject
        {
            if (!_containPoints.ContainsKey(typeof(TValue)))
                throw new NotSupportedException("Given TValue is not supported");
        }

        private IQueryable<ContactPoint<TValue>> QueryContactPoints<TValue>() where TValue : ValueObject
        {
            ThrowIfUnsupported<TValue>();

            return GetContactPoints<TValue>()
                .AsQueryable()
                .Cast<ContactPoint<TValue>>();
        }

        public void SetPrimary<TValue>(TValue value) where TValue : ValueObject
        {
            if (value == null) throw new ArgumentNullException(nameof(value));

            QueryContactPoints<TValue>().ToList().ForEach(p => p.IsPrimary = false);

            var contactPoint = FindByValueOrThrow(value);

            contactPoint.IsPrimary = true;

            PrimaryUpdatedEvent?.Invoke(this, new PrimaryContactPointUpdatedEventArgs { ContactValue = value });
        }

        public void Replace<TValue>(TValue existing, TValue with) where TValue : ValueObject
        {
            if (existing == null) throw new ArgumentNullException(nameof(existing));
            if (with == null) throw new ArgumentNullException(nameof(with));

            if (Has(with) && with != existing)
                throw new InvalidOperationException("Replacing value already exists.");

            FindByValueOrThrow(existing).Value = with;

            PrimaryUpdatedEvent?.Invoke(this, new PrimaryContactPointUpdatedEventArgs { ContactValue = with });
        }

        private ContactPoint<TValue> FindByValueOrThrow<TValue>(TValue existing) where TValue : ValueObject =>
            FindByValue(existing) ?? throw new InvalidOperationException("Value does not exist.");

        private ContactPoint<TValue> FindByValue<TValue>(TValue existing) where TValue : ValueObject =>
            QueryContactPoints<TValue>().ToList().SingleOrDefault(p => p.Value.ToString() == existing.ToString());

        public IContactPoint<TValue> Find<TValue>(TValue value) where TValue : ValueObject => FindByValue(value);

        public bool Has<TValue>(TValue value) where TValue : ValueObject
        {
            if (value == null) throw new ArgumentNullException(nameof(value));

            return FindByValue(value) != null;
        }

        public bool Remove<TValue>(TValue value) where TValue : ValueObject
        {
            if (value == null) throw new ArgumentNullException(nameof(value));

            var point = FindByValue(value);
            if (point == null)
                return false;

            GetContactPointsInternal<TValue>().Remove(point);

            if (point.IsPrimary)
            {
                var first = GetContactPointsInternal<TValue>().FirstOrDefault();
                if (first != null)
                    first.IsPrimary = true;
            }

            return true;
        }

        public IContactPoint<TValue> GetPrimary<TValue>() where TValue : ValueObject =>
            GetContactPointsInternal<TValue>().SingleOrDefault(c => c.IsPrimary);

        public void Clear<TValue>() where TValue : ValueObject
        {
            ThrowIfUnsupported<TValue>();
            _containPoints[typeof(TValue)].Clear();
        }
    }
}
