﻿using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Domain.Contacts
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class EmailAddress : ValueObject
    {
        [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
        private EmailAddress()
        {
        }

        public EmailAddress(string address)
        {
            if (string.IsNullOrEmpty(address))
                throw new ArgumentException("Value cannot be null or empty.", nameof(address));

            Address = address;
        }

        public string Address { get; private set; }

        public static EmailAddress Create(string address)
        {
            return new EmailAddress(address);
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return Address;
        }

        public override string ToString() => Address;
    }
}
