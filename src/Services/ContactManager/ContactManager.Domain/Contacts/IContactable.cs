﻿using NexCore.Domain;

namespace NexCore.ContactManager.Domain.Contacts
{
    public interface IContactable : IAggregateRoot
    {
        ContactInfo Contact { get; }
    }
}