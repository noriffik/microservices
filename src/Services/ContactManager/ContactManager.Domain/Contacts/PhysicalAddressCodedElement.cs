﻿using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Domain.Contacts
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    public class PhysicalAddressCodedElement : ValueObject
    {
        public string Code { get; private set; }

        private PhysicalAddressCodedElement()
        {
        }

        public PhysicalAddressCodedElement(string value)
        {
            Value = value;
        }

        public PhysicalAddressCodedElement(string code, string value) : this(value)
        {
            Code = code;
        }

        public static PhysicalAddressCodedElement Empty => new PhysicalAddressCodedElement();

        public string Value { get; private set; }

        protected override IEnumerable<object> GetValues()
        {
            yield return Code;
            yield return Value;
        }

        public override string ToString()
        {
            return Value ?? string.Empty;
        }

        public void Assign(PhysicalAddressCodedElement element)
        {
            if (element == null)
                throw new ArgumentNullException(nameof(element));

            Code = element.Code;
            Value = element.Value;
        }
    }
}
