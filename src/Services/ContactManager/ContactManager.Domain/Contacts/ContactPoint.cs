﻿using NexCore.Domain;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Domain.Contacts
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class ContactPoint<TValue> : Entity, IContactPoint<TValue> where TValue : ValueObject
    {
        [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
        private ContactPoint()
        {
        }

        public ContactPoint(TValue value)
        {
            Value = value ?? throw new ArgumentNullException(nameof(value));
        }

        public string Description { get; set; }

        public TValue Value { get; internal set; }

        public bool IsPrimary { get; set; }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
