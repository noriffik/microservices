﻿using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.ContactManager.Domain.Contacts
{
    public enum TelephoneType
    {
        Landline,
        Mobile
    }

    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core")]
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    public class Telephone : ValueObject
    {
        private string _e123;
        private string _number;

        private Telephone()
        {
            CountryCode = string.Empty;
            AreaCode = string.Empty;
            LocalNumber = string.Empty;
        }

        public Telephone(string countryCode, string areaCode, string localNumber, TelephoneType? type = null)
        {
            if (string.IsNullOrEmpty(countryCode))
                throw new ArgumentException("Value cannot be null or empty.", nameof(countryCode));

            if (string.IsNullOrEmpty(areaCode))
                throw new ArgumentException("Value cannot be null or empty.", nameof(areaCode));

            if (string.IsNullOrEmpty(localNumber))
                throw new ArgumentException("Value cannot be null or empty.", nameof(localNumber));

            CountryCode = countryCode;
            AreaCode = areaCode;
            LocalNumber = localNumber;
            Type = type;
        }

        public Telephone(Telephone telephone)
        {
            if (telephone == null)
                throw new ArgumentNullException(nameof(telephone));

            CountryCode = telephone.CountryCode;
            AreaCode = telephone.AreaCode;
            LocalNumber = telephone.LocalNumber;
            Type = telephone.Type;
        }

        public string CountryCode { get; private set; }
        public string AreaCode { get; private set; }
        public string LocalNumber { get; private set; }
        public TelephoneType? Type { get; private set; }

        public string E123
        {
            get => _e123 ?? (_e123 = FormatAsE123(CountryCode, AreaCode, LocalNumber));
            private set => _e123 = value;
        }

        public string Number
        {
            get => _number ?? (_number = FormatAsNumber(CountryCode, AreaCode, LocalNumber));
            private set => _number = value;
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return CountryCode;
            yield return AreaCode;
            yield return LocalNumber;
            yield return Type;
        }

        public static string FormatAsNumber(string countryCode, string areaCode, string localNumber)
            => $"{countryCode}{areaCode}{localNumber}";

        public static string FormatAsE123(string countryCode, string areaCode, string localNumber)
            => $"+{countryCode} {areaCode} {localNumber}";

        public override string ToString() => Number;

        //public Telephone Clone() => new Telephone(CountryCode, AreaCode, LocalNumber, Type);

        public Telephone ChangeType(TelephoneType to)
        {
            var copy = (Telephone)MemberwiseClone();
            copy.Type = to;

            return copy;
        }

        public static Telephone Empty => new Telephone();
    }
}
