﻿using Autofac;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.HealthChecks;
using Microsoft.Extensions.Logging;
using NexCore.EventBus;
using NexCore.EventBus.Abstract;
using NexCore.EventBus.Azure;
using NexCore.EventBus.RabbitMq;
using NexCore.VehicleStock.Application.Messages;
using NexCore.WebApi.Extensions;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleStock.Api.Extensions
{
    static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddHealthChecks(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddHealthChecks(checks =>
            {
                var cacheDurationInMinutes = configuration.GetValue("HealthCheck:CacheDurationInMinutes", 1);

                checks.AddSqlCheck(
                    "VehicleStock",
                    configuration["ConnectionString"],
                    TimeSpan.FromMinutes(cacheDurationInMinutes));
            });

            return services;
        }

        public static IServiceCollection AddSwagger(this IServiceCollection services,
            IConfiguration configuration)
        {
            return services.AddSwaggerGen(
                configuration,
                "The Vehicle Stock Service HTTP API",
                "v1",
                new Dictionary<string, string>
                {
                    { "NexCore.VehicleStock", "Vehicle Stock Service API" }
                });
        }

        public static IServiceCollection AddIntegrationServices(this IServiceCollection services,
           IConfiguration configuration)
        {
            //TODO: Replace this with Stub EventBus provider configurable using appsettings.json
            var hasRegistration = services.Any(r => r.ServiceType == typeof(IEventBus));
            if (hasRegistration)
                return services;

            var eventBusType = (EBusType)configuration.GetValue("EventBusType", EBusType.RabbitMq);
            var subscriptionClientName = configuration["SubscriptionClientName"];

			switch (eventBusType)
            {
                case EBusType.Azure:
                    services.AddSingleton<IServiceBusPersistentConnection>(s =>
                    {
                        var logger = s.GetRequiredService<ILogger<DefaultServiceBusPersistentConnection>>();

                        var serviceBusConnectionString = configuration["EventBusConnection"];
                        var serviceBusConnection = new ServiceBusConnectionStringBuilder(serviceBusConnectionString);

                        return new DefaultServiceBusPersistentConnection(serviceBusConnection, logger);
                    });

                    services.AddSingleton<IEventBus, EventBusServiceBus>(s =>
                    {
                        var serviceBusConnection = s.GetRequiredService<IServiceBusPersistentConnection>();
                        var iLifetimeScope = s.GetRequiredService<ILifetimeScope>();
                        var logger = s.GetRequiredService<ILogger<EventBusServiceBus>>();
                        var subscriptionsManager = s.GetRequiredService<IEventBusSubscriptionsManager>();
                        return new EventBusServiceBus(serviceBusConnection, logger, subscriptionsManager, iLifetimeScope, subscriptionClientName);
                    });
                    break;
                case EBusType.RabbitMq:
                    services.AddSingleton<IRabbitMqPersistentConnection>(s =>
                    {
                        var logger = s.GetRequiredService<ILogger<DefaultRabbitMqPersistentConnection>>();

                        var factory = new ConnectionFactory
                        {
                            HostName = configuration["EventBusConnection"],
                            DispatchConsumersAsync = true
                        };

                        if (!string.IsNullOrEmpty(configuration["EventBusUserName"]))
                        {
                            factory.UserName = configuration["EventBusUserName"];
                        }

                        if (!string.IsNullOrEmpty(configuration["EventBusPassword"]))
                        {
                            factory.Password = configuration["EventBusPassword"];
                        }

                        var retryCount = 5;
                        if (!string.IsNullOrEmpty(configuration["EventBusRetryCount"]))
                        {
                            retryCount = int.Parse(configuration["EventBusRetryCount"]);
                        }

                        return new DefaultRabbitMqPersistentConnection(factory, logger, retryCount);
                    });

                    services.AddSingleton<IEventBus, EventBusRabbitMq>(s =>
                    {
                        var serviceBusConnection = s.GetRequiredService<IRabbitMqPersistentConnection>();
                        var iLifetimeScope = s.GetRequiredService<ILifetimeScope>();
                        var logger = s.GetRequiredService<ILogger<EventBusRabbitMq>>();
                        var subscriptionsManager = s.GetRequiredService<IEventBusSubscriptionsManager>();
                        var retryCount = 5;
                        if (!string.IsNullOrEmpty(configuration["EventBusRetryCount"]))
                        {
                            retryCount = int.Parse(configuration["EventBusRetryCount"]);
                        }

                        return new EventBusRabbitMq(serviceBusConnection, logger, iLifetimeScope, subscriptionsManager,
                            subscriptionClientName, retryCount);
                    });
                    break;
            }

            services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();

            RegisterIntegrationEventsHandlers(services);

            return services;
        }

        private static void RegisterIntegrationEventsHandlers(IServiceCollection services)
        {
            services.AddTransient<DistributorRegisteredEventHandler>();
            services.AddTransient<DealerAddedEventHandler>();
        }
    }
}