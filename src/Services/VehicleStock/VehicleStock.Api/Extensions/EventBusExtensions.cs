﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using NexCore.EventBus.Abstract;
using NexCore.VehicleStock.Application.Messages;

namespace NexCore.VehicleStock.Api.Extensions
{
    public static class EventBusExtensions
    {
        public static void RunEventBus(this IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
            eventBus.Subscribe<DistributorRegisteredEvent, DistributorRegisteredEventHandler>();
            eventBus.Subscribe<DealerAddedEvent, DealerAddedEventHandler>();
        }
    }
}
