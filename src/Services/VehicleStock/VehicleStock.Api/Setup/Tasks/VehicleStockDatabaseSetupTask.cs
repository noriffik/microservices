﻿using Microsoft.Extensions.Configuration;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Setup;
using NexCore.VehicleStock.Infrastructure;
using System;

namespace NexCore.VehicleStock.Api.Setup.Tasks
{
    public class VehicleStockDatabaseSetupTask : DatabaseSetupTask<VehicleStockContext>
    {
        private const string IsSeedingEnabledKey = "IsSeedingEnabled";

        public VehicleStockDatabaseSetupTask(VehicleStockContext context) : base(context)
        {
        }

        public VehicleStockDatabaseSetupTask(VehicleStockContext context, IDbContextSeeder<VehicleStockContext> seeder,
            IConfiguration configuration) : base(context, seeder)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            
            IsSeedingEnabled = configuration.GetValue(IsSeedingEnabledKey, true);
        }
    }
}
