﻿using Autofac;
using Microsoft.Extensions.Configuration;
using NexCore.Infrastructure.Seeds;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Infrastructure;
using NexCore.VehicleStock.Infrastructure.Seeds;

namespace NexCore.VehicleStock.Api.Setup.AutofacModules
{
    public class SeederModule : Module
    {
        private readonly IConfiguration _configuration;

        public SeederModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VehicleStockSeeder>()
                .As<IDbContextSeeder<VehicleStockContext>>()
                .InstancePerLifetimeScope();

            RegisterVehiclesSeeder(builder);
        }

        private void RegisterVehiclesSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<VehicleSeeder>()
                .As<ISeeder<Vehicle>>();

            builder.RegisterType<VehicleSeederDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:VehicleDataPath"]))
                .As<ISeederDataSource<VehicleRecord>>();
        }
    }
}
