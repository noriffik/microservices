﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Application.Extensions;
using NexCore.Infrastructure.Extensions;
using NexCore.VehicleStock.Api.Extensions;
using NexCore.VehicleStock.Api.Setup.AutofacModules;
using NexCore.VehicleStock.Api.Setup.Tasks;
using NexCore.VehicleStock.Application.Vehicles.Commands.Add;
using NexCore.VehicleStock.Domain.Warehouses;
using NexCore.VehicleStock.Infrastructure;
using NexCore.WebApi.Extensions;
using NexCore.WebApi.Filters;
using System;

namespace NexCore.VehicleStock.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var application = typeof(AddCommand).Assembly;
            var infrastructure = typeof(VehicleStockContext).Assembly;
            var domain = typeof(Warehouse).Assembly;

            services
                .AddTransient<VehicleStockDatabaseSetupTask>()
                .AddAutoMapper(application, infrastructure)
                .AddUnitOfWork<VehicleStockContext>(Configuration["ConnectionString"], Configuration)
                .AddHealthChecks(Configuration)
                .AddSwagger(Configuration)
                .AddIntegrationServices(Configuration)
                .AddMvc(options =>
                {
                    options.Filters.Add(typeof(ExceptionFilter));
                })
                .AddControllersAsServices();

            ConfigureAuthentication(services);

            var containerBuilder = services.GetAutofacContainerBuilder(application, domain, infrastructure);

            containerBuilder.RegisterModule(new SeederModule(Configuration));

            return new AutofacServiceProvider(containerBuilder.Build());
        }

        // This method is required by testing to change authentication options
        protected virtual void ConfigureAuthentication(IServiceCollection services)
        {
            services.AddJwtBearerAuthentication(Configuration, "NexCore.VehicleStock");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");

                c.OAuthClientId("NexCore.VehicleStock.Swagger");
            });

            app.RunEventBus();

            app.UseMvc();
        }
    }
}
