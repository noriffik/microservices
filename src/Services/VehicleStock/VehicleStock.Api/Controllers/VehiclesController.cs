﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleStock.Api.Authorization;
using NexCore.VehicleStock.Application.Vehicles.Commands.Add;
using NexCore.VehicleStock.Application.Vehicles.Commands.ChangeUnavailability;
using NexCore.VehicleStock.Application.Vehicles.Commands.Delete;
using NexCore.VehicleStock.Application.Vehicles.Queries;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Api.Controllers
{
    [Authorize]
    [Route("api/vehicles")]
    public class VehiclesController : MediatrController
    {
        public VehiclesController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{Id}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [Produces(typeof(VehicleDto))]
        public Task<IActionResult> Get([FromRoute] ByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("many")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<VehicleDto>))]
        public Task<IActionResult> GetAll() => ExecuteQuery(new AnyQuery(), QueryAuthorizationSettings.None);

        [Route("many/by-warehouse/{WarehouseId}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<VehicleDto>))]
        public Task<IActionResult> GetByWarehouseId([FromRoute] ByWarehouseIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("add")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Add([FromBody] AddCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None);

        [Route("change/unavailability")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> ChangeUnavailability([FromBody] ChangeUnavailabilityCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("remove/{Id}")]
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> RemoveById([FromRoute] DeleteByIdCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("remove/by-vin/{Vin}")]
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> RemoveByVin([FromRoute] DeleteByVinCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("matching/{ModelKey}/{ColorId}/{Options}/mask/{Mask}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<MatchedVehicleDto>))]
        public Task<IActionResult> Matching([FromRoute] MatchingQuery command) => ExecuteQuery(command, QueryAuthorizationSettings.None);
    }
}
