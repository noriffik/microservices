﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.VehicleStock.Api.Authorization;
using NexCore.VehicleStock.Application.Reservations.Commands.Add;
using NexCore.VehicleStock.Application.Reservations.Commands.Cancel;
using NexCore.VehicleStock.Application.Reservations.Commands.Complete;
using NexCore.VehicleStock.Application.Reservations.Queries;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Api.Controllers
{
    [Authorize]
    [Route("api/reservations")]
    public class ReservationsController : MediatrController
    {
        public ReservationsController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{Id}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Produces(typeof(ReservationDto))]
        public Task<IActionResult> Get([FromRoute] ByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("many/by-vin/{Vin}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [Produces(typeof(IEnumerable<ReservationDto>))]
        public Task<IActionResult> GetByVin([FromRoute] ByVinQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("add/by-commercial-number")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> AddByCommercialNumber([FromBody] AddByCommercialNumberCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None);

        [Route("add/by-vin")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> AddByVin([FromBody] AddByVinCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None);

        [Route("{Id}/cancel")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Cancel([FromRoute] CancelCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("{Id}/complete")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Complete([FromRoute] CompleteCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);
    }
}
