﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.Infrastructure.Seeds;
using NexCore.VehicleStock.Infrastructure;
using System;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Api.Controllers
{
    [Authorize]
    [Route("api/seed")]
    public class SeedsController : Controller
    {
        private readonly IDbContextSeeder<VehicleStockContext> _seeder;

        public SeedsController(IDbContextSeeder<VehicleStockContext> context)
        {
            _seeder = context ?? throw new ArgumentNullException(nameof(context));
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Post()
        {
            await _seeder.Seed();

            return Ok();
        }
    }
}
