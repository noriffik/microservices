﻿using NexCore.DealerNetwork;
using System;

namespace NexCore.VehicleStock.Domain.Warehouses.DistributorWarehouses
{
    public class DistributorWarehouse : Warehouse
    {
        public DistributorId DistributorId { get; private set; }

        private DistributorWarehouse()
        {
        }

        public DistributorWarehouse(DistributorId distributorId, string name, int companyId) : base(name, companyId)
        {
            DistributorId = distributorId ?? throw new ArgumentNullException(nameof(distributorId));
        }

        public DistributorWarehouse(int id, DistributorId distributorId, string name, int companyId)
            : base(id, name, companyId)
        {
            DistributorId = distributorId ?? throw new ArgumentNullException(nameof(distributorId));
        }
    }
}
