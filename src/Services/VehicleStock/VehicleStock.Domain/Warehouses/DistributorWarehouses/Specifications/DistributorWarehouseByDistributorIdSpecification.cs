﻿using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleStock.Domain.Warehouses.DistributorWarehouses.Specifications
{
    public class DistributorWarehouseByDistributorIdSpecification : Specification<DistributorWarehouse>
    {
        public DistributorId DistributorId { get; }

        public DistributorWarehouseByDistributorIdSpecification(DistributorId distributorId)
        {
            DistributorId = distributorId ?? throw new ArgumentNullException(nameof(distributorId));
        }

        public override Expression<Func<DistributorWarehouse, bool>> ToExpression()
        {
            return warehouse => Convert.ToString(warehouse.DistributorId) == DistributorId.Value;
        }

        public override string Description => $"DistributorWarehouse for Distributor with id {DistributorId}";
    }
}
