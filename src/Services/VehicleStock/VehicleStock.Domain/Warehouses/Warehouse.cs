﻿using NexCore.Common;
using NexCore.Domain;
using System;

namespace NexCore.VehicleStock.Domain.Warehouses
{
    public abstract class Warehouse : Entity, IAggregateRoot
    {
        public string Name { get; private set; }

        public int CompanyId { get; private set; }

        public int ItemQuantity { get; private set; }

        public DateTime CreatedAt { get; private set; }

        public DateTime UpdatedAt { get; private set; }

        protected Warehouse()
        {
        }

        protected Warehouse(string name, int companyId)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            CompanyId = companyId;
            CreatedAt = SystemTimeProvider.Instance.Get();
            UpdatedAt = CreatedAt;
        }

        protected Warehouse(int id, string name, int companyId) : base(id)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            CompanyId = companyId;
            CreatedAt = SystemTimeProvider.Instance.Get();
            UpdatedAt = CreatedAt;
        }
    }
}
