﻿using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleStock.Domain.Warehouses.DealerWarehouses.Specifications
{
    public class DealerWarehouseByDealerIdSpecification : Specification<DealerWarehouse>
    {
        public DealerId DealerId { get; }

        public DealerWarehouseByDealerIdSpecification(DealerId dealerId)
        {
            DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
        }

        public override Expression<Func<DealerWarehouse, bool>> ToExpression()
        {
            return warehouse => Convert.ToString(warehouse.DealerId) == DealerId.Value;
        }

        public override string Description => $"DealerWarehouse for Dealer with id {DealerId}";
    }
}
