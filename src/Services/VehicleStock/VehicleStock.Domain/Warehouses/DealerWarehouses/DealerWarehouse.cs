﻿using NexCore.DealerNetwork;
using System;

namespace NexCore.VehicleStock.Domain.Warehouses.DealerWarehouses
{
    public class DealerWarehouse : Warehouse
    {
        public DealerId DealerId { get; private set; }

        public DistributorId DistributorId { get; private set; }

        public DealerCode DealerCode { get; private set; }

        private DealerWarehouse()
        {
        }

        public DealerWarehouse(DealerId dealerId, string name, int companyId) : base(name, companyId)
        {
            DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
            DistributorId = dealerId.DistributorId;
            DealerCode = dealerId.DealerCode;
        }

        public DealerWarehouse(int id, DealerId dealerId, string name, int companyId) : base(id, name, companyId)
        {
            DealerId = dealerId ?? throw new ArgumentNullException(nameof(dealerId));
            DistributorId = dealerId.DistributorId;
            DealerCode = dealerId.DealerCode;
        }
    }
}
