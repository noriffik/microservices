﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleStock.Domain.Vehicles
{
    public class DuplicateVehicleException : DomainException
    {
        private const string DefaultMessage = "Vehicle for given commercial number or VIN already exist.";
        private const string DefaultExtendedMessage = "Vehicle for commercial number {0} or VIN {1} already exist.";

        public CommercialNumber CommercialNumber { get; }

        public string Vin { get; }

        public DuplicateVehicleException() : base(DefaultMessage)
        {
        }

        public DuplicateVehicleException(string message) : base(message)
        {
        }

        public DuplicateVehicleException(string message, Exception inner) : base(message, inner)
        {
        }

        public DuplicateVehicleException(CommercialNumber commercialNumber, string vin)
            : this(string.Format(DefaultExtendedMessage, commercialNumber.Value, vin), commercialNumber, vin)
        {
        }

        public DuplicateVehicleException(string message, CommercialNumber commercialNumber, string vin)
            : this(message, commercialNumber, vin, null)
        {
        }

        public DuplicateVehicleException(string message, CommercialNumber commercialNumber, string vin, Exception inner)
            : base(message, inner)
        {
            CommercialNumber = commercialNumber;
            Vin = vin;
        }
    }
}
