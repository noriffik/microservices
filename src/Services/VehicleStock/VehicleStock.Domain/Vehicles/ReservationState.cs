﻿using NexCore.Domain;
using System;
using System.Collections.Generic;

namespace NexCore.VehicleStock.Domain.Vehicles
{
    public class ReservationState : ValueObject
    {
        public int? ReservationId { get; private set; }

        public ReservationStateStatus Status { get; private set; }

        public DateTime? OnDate { get; private set; }

        public static ReservationState None => new ReservationState();

        private ReservationState()
        {
        }

        public ReservationState(int reservationId, DateTime onDate)
        {
            ReservationId = reservationId;
            OnDate = onDate;
            Status = ReservationStateStatus.Reserved;
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return ReservationId;
            yield return Status;
            yield return OnDate;
        }
    }

    public enum ReservationStateStatus
    {
        NotReserved = 0,
        Reserved = 1
    }
}
