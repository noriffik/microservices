﻿using NexCore.Domain;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Vehicles.Specifications;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Domain.Vehicles
{
    public class ReservationCanceledHandler : IEntityEventHandler<ReservationCanceledEvent>
    {
        private readonly IUnitOfWork _work;

        public ReservationCanceledHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Handle(ReservationCanceledEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            var vehicle = await _work.EntityRepository.SingleOrDefault<Vehicle, CommercialNumber>(
                new VehicleByReservationSpecification(notification.ReservationId), cancellationToken);

            if (vehicle != null)
                vehicle.ChangeReservationState(ReservationState.None);
        }
    }
}
