﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleStock.Domain.Vehicles.Specifications
{
    public class VehicleByIdOrVinSpecification : Specification<Vehicle>
    {
        public CommercialNumber CommercialNumber { get; }

        public string Vin { get; }

        private readonly Specification<Vehicle> _inner;

        public VehicleByIdOrVinSpecification(CommercialNumber commercialNumber, string vin)
        {
            CommercialNumber = commercialNumber ?? throw new ArgumentNullException(nameof(commercialNumber));
            Vin = vin ?? throw new ArgumentNullException(nameof(vin));

            _inner = new AnySpecification<Vehicle>(
                new IdSpecification<Vehicle, CommercialNumber>(commercialNumber),
                new VehicleByVinSpecification(vin));
        }

        public override Expression<Func<Vehicle, bool>> ToExpression()
        {
            return _inner.ToExpression();
        }

        public override string Description => $"Vehicle with commercial number {CommercialNumber} or Vin {Vin}";
    }
}
