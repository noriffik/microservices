﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleStock.Domain.Vehicles.Specifications
{
    public class VehicleByVinSpecification : Specification<Vehicle>
    {
        public string Vin { get; }

        public VehicleByVinSpecification(string vin)
        {
            Vin = vin?.ToUpperInvariant() ?? throw new ArgumentNullException(nameof(vin));
        }

        public override Expression<Func<Vehicle, bool>> ToExpression()
        {
            return vehicle => vehicle.Specification.Vin == Vin;
        }

        public override string Description => $"Vehicle with VIN {Vin}";
    }
}
