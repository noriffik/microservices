﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleStock.Domain.Vehicles.Specifications
{
    public class VehicleByReservationSpecification : Specification<Vehicle>
    {
        public int ReservationId { get; }

        public VehicleByReservationSpecification(int reservationId)
        {
            ReservationId = reservationId;
        }

        public override Expression<Func<Vehicle, bool>> ToExpression()
        {
            return vehicle => vehicle.ReservationState.ReservationId == ReservationId;
        }

        public override string Description => $"Vehicle reserved by Reservation with Id {ReservationId}";
    }
}
