﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Collections.Generic;

namespace NexCore.VehicleStock.Domain.Vehicles
{
    public class VehicleSpecification : ValueObject
    {
        public ModelKey ModelKey { get; private set; }

        public PrNumberSet Options { get; private set; }

        public ColorId ColorId { get; private set; }

        public string Engine { get; private set; }

        public string Vin { get; private set; }

        private VehicleSpecification()
        {
        }

        public VehicleSpecification(VehicleSpecification vehicleSpecification)
        {
            ModelKey = vehicleSpecification.ModelKey;
            Options = vehicleSpecification.Options;
            Engine = vehicleSpecification.Engine;
            Vin = vehicleSpecification.Vin;
            ColorId = vehicleSpecification.ColorId;
        }

        public VehicleSpecification(ModelKey modelKey, PrNumberSet options, string engine, string vin, ColorId colorId)
        {
            if (engine == null)
                throw new ArgumentNullException(nameof(engine));
            if (vin == null)
                throw new ArgumentNullException(nameof(vin));

            ModelKey = modelKey ?? throw new ArgumentNullException(nameof(modelKey));
            Options = options ?? throw new ArgumentNullException(nameof(options));
            Engine = engine.ToUpperInvariant();
            ColorId = colorId ?? throw new ArgumentNullException(nameof(colorId));
            Vin = vin.ToUpperInvariant();
        }


        protected override IEnumerable<object> GetValues()
        {
            yield return ModelKey;
            yield return Options;
            yield return Engine;
            yield return ColorId;
            yield return Vin;
        }
    }
}
