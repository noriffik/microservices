﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleStock.Domain.Vehicles
{
    public class CommercialNumber : EntityId
    {
        private CommercialNumber()
        {
        }

        private CommercialNumber(string value) : base(value)
        {
        }

        public static CommercialNumber Parse(string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (!TryParse(value, out var commercialNumber))
                throw new FormatException($@"""{value}"" is invalid value for CommercialNumber");

            return commercialNumber;
        }

        public static bool TryParse(string value, out CommercialNumber commercialNumber)
        {
            commercialNumber = string.IsNullOrWhiteSpace(value) ? null : new CommercialNumber(value);

            return commercialNumber != null;
        }

        public static implicit operator string(CommercialNumber id)
        {
            return id?.Value;
        }

        public static implicit operator CommercialNumber(string value)
        {
            return value == null ? null : new CommercialNumber(value);
        }
    }
}
