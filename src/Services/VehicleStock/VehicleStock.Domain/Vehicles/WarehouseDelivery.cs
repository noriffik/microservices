﻿using NexCore.Domain;
using System;
using System.Collections.Generic;

namespace NexCore.VehicleStock.Domain.Vehicles
{
    public class WarehouseDelivery : ValueObject
    {
        public DateTime AssembledOn { get; private set; }

        public DateTime DeliveredOn { get; private set; }

        private WarehouseDelivery()
        {
        }

        public WarehouseDelivery(DateTime assembledOn, DateTime deliveredOn)
        {
            if (deliveredOn < assembledOn)
                throw new CannotDeliverBeforeAssemblyException(assembledOn, deliveredOn);

            AssembledOn = assembledOn;
            DeliveredOn = deliveredOn;
        }

        protected override IEnumerable<object> GetValues()
        {
            yield return AssembledOn;
            yield return DeliveredOn;
        }
    }
}
