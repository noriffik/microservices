﻿using NexCore.Domain;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Reservations.Specifications;
using NexCore.VehicleStock.Domain.Vehicles.Specifications;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Domain.Vehicles
{
    public class ReservedHandler : IEntityEventHandler<ReservedEvent>
    {
        private readonly IUnitOfWork _work;

        public ReservedHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task Handle(ReservedEvent notification, CancellationToken cancellationToken)
        {
            if (notification == null)
                throw new ArgumentNullException(nameof(notification));

            var reservation = await _work.EntityRepository.Require(new ActualByVinSpecification(notification.Vin), cancellationToken);

            var vehicle = await _work.EntityRepository.Require<Vehicle, CommercialNumber>(
                new VehicleByVinSpecification(notification.Vin), cancellationToken);

            vehicle.ChangeReservationState(new ReservationState(reservation.Id, reservation.OnDate));
        }
    }
}
