﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleStock.Domain.Vehicles
{
    public class CannotDeliverBeforeAssemblyException : DomainException
    {
        private const string DefaultMessage = "Delivery date can not be before assembly date.";
        private const string DefaultExtendedMessage = "Delivery date {0} can not be before assembly date {1}.";

        public DateTime AssembledOn;

        public DateTime DeliveredOn;

        public CannotDeliverBeforeAssemblyException() : base(DefaultMessage)
        {
        }

        public CannotDeliverBeforeAssemblyException(string message) : base(message)
        {
        }

        public CannotDeliverBeforeAssemblyException(string message, Exception inner) : base(message, inner)
        {
        }

        public CannotDeliverBeforeAssemblyException(DateTime assembledOn, DateTime deliveredOn)
            : this(string.Format(DefaultExtendedMessage, deliveredOn, assembledOn), assembledOn, deliveredOn)
        {
        }

        public CannotDeliverBeforeAssemblyException(string message, DateTime assembledOn, DateTime deliveredOn)
            : this(message, assembledOn, deliveredOn, null)
        {
        }

        public CannotDeliverBeforeAssemblyException(string message, DateTime assembledOn, DateTime deliveredOn, Exception inner)
            : base(message, inner)
        {
            AssembledOn = assembledOn;
            DeliveredOn = deliveredOn;
        }
    }
}
