﻿using NexCore.Domain;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleStock.Domain.Vehicles
{
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core.")]
    public class Vehicle : Entity<CommercialNumber>, IAggregateRoot<CommercialNumber>
    {
        public VehicleSpecification Specification { get; private set; }

        public WarehouseDelivery WarehouseDelivery { get; private set; }

        public int WarehouseId { get; private set; }

        public bool IsTemporaryUnavailable { get; private set; }

        public ReservationState ReservationState { get; private set; }

        private Vehicle() : base()
        {
        }

        public Vehicle(CommercialNumber id, VehicleSpecification specification,
            WarehouseDelivery warehouseDelivery, int warehouseId)
            : base(id)
        {
            Specification = specification ?? throw new ArgumentNullException(nameof(specification));
            WarehouseDelivery = warehouseDelivery ?? throw new ArgumentNullException(nameof(warehouseDelivery));
            WarehouseId = warehouseId;
            ReservationState = ReservationState.None;
        }

        public void ChangeUnavailability(bool isTemporaryUnavailable)
        {
            IsTemporaryUnavailable = isTemporaryUnavailable;
        }

        public void ChangeReservationState(ReservationState reservationState)
        {
            ReservationState = reservationState ?? throw new ArgumentNullException(nameof(reservationState));
        }
    }
}
