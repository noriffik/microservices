﻿using NexCore.Common;
using NexCore.Domain;
using NexCore.VehicleStock.Domain.Vehicles;
using System;

namespace NexCore.VehicleStock.Domain.Reservations
{
    public sealed class Reservation : Entity, IAggregateRoot
    {
        public DateTime OnDate { get; private set; }

        public int? CustomerId { get; private set; }

        public VehicleSpecification Vehicle { get; private set; }

        public int WarehouseId { get; private set; }

        public ReservationStatus Status { get; private set; }

        private Reservation() : base()
        {
        }

        public Reservation(VehicleSpecification vehicle, int warehouseId, int? customerId)
        {
            if (vehicle == null)
                throw new ArgumentNullException(nameof(vehicle));

            WarehouseId = warehouseId;
            Vehicle = new VehicleSpecification(vehicle);
            CustomerId = customerId;
            OnDate = SystemTimeProvider.Instance.Get();
            Status = ReservationStatus.Actual;
            AddEntityEvent(new ReservedEvent(vehicle.Vin));
        }

        public Reservation(Vehicle vehicle, int? customerId) :
            this(vehicle?.Specification ?? throw new ArgumentNullException(nameof(vehicle)), vehicle.WarehouseId, customerId)
        {
        }

        public void Cancel()
        {
            if (Status != ReservationStatus.Actual)
                throw new ReservationIsNotActualOperationException("Cancel operation allowed only for Actual Reservations");

            Status = ReservationStatus.Canceled;
            AddEntityEvent(new ReservationCanceledEvent(Id));
        }

        public void Complete()
        {
            if (Status != ReservationStatus.Actual)
                throw new ReservationIsNotActualOperationException("Complete operation allowed only for Actual Reservations");

            Status = ReservationStatus.Completed;
            AddEntityEvent(new ReservationCompletedEvent(Id));
        }
    }

    public enum ReservationStatus
    {
        Canceled = default(int),
        Actual = 1,
        Completed = 2
    }
}
