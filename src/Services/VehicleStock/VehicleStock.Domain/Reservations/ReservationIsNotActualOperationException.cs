﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleStock.Domain.Reservations
{
    public class ReservationIsNotActualOperationException : InvalidDomainOperationException
    {
        public ReservationIsNotActualOperationException() : base("Operation allowed only for Actual Reservations")
        {
        }

        public ReservationIsNotActualOperationException(string message) : base(message)
        {
        }

        public ReservationIsNotActualOperationException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
