﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleStock.Domain.Reservations
{
    public class ReservedEvent : IEntityEvent
    {
        public string Vin { get; }

        public ReservedEvent(string vin)
        {
            Vin = vin ?? throw new ArgumentNullException(nameof(vin));
        }
    }
}
