﻿using NexCore.Domain;

namespace NexCore.VehicleStock.Domain.Reservations
{
    public class ReservationCanceledEvent : IEntityEvent
    {
        public int ReservationId { get; }

        public ReservationCanceledEvent(int reservationId)
        {
            ReservationId = reservationId;
        }
    }
}
