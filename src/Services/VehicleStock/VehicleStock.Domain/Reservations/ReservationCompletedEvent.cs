﻿using NexCore.Domain;

namespace NexCore.VehicleStock.Domain.Reservations
{
    public class ReservationCompletedEvent : IEntityEvent
    {
        public int ReservationId { get; }

        public ReservationCompletedEvent(int reservationId)
        {
            ReservationId = reservationId;
        }
    }
}
