﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleStock.Domain.Reservations.Specifications
{
    public class ActualByVinSpecification : Specification<Reservation>
    {
        public string Vin { get; }

        private readonly Specification<Reservation> _inner;

        public ActualByVinSpecification(string vin)
        {
            Vin = vin ?? throw new ArgumentNullException(nameof(vin));

            _inner = new AndSpecification<Reservation>(new ActualSpecification(), new ByVinSpecification(vin));
        }

        public override Expression<Func<Reservation, bool>> ToExpression() => _inner.ToExpression();

        public override string Description => $"Actual Reservation for vehicle with Vin {Vin}";
    }
}
