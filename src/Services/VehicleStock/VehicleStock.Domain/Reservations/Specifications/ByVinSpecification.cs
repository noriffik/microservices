﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleStock.Domain.Reservations.Specifications
{
    public class ByVinSpecification : Specification<Reservation>
    {
        public string Vin { get; }

        public ByVinSpecification(string vin)
        {
            Vin = vin?.ToUpperInvariant() ?? throw new ArgumentNullException(nameof(vin));
        }

        public override Expression<Func<Reservation, bool>> ToExpression()
        {
            return reservation => reservation.Vehicle.Vin == Vin;
        }

        public override string Description => $"Reservation for vehicle with Vin {Vin}";
    }
}
