﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.VehicleStock.Domain.Reservations.Specifications
{
    public class ActualSpecification : Specification<Reservation>
    {
        public override Expression<Func<Reservation, bool>> ToExpression()
        {
            return reservation => reservation.Status == ReservationStatus.Actual;
        }

        public override string Description => "Reservation with Actual status";
    }
}
