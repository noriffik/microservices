﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleStock.Domain.Reservations
{
    public class IsReservedException : DomainException
    {
        public IsReservedException() : base("Vehicle is reserved.")
        {
        }

        public IsReservedException(string message) : base(message)
        {
        }

        public IsReservedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}


