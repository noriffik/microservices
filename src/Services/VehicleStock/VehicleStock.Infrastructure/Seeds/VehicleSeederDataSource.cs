﻿using AutoMapper;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Seeds.DataSources;
using System;

namespace NexCore.VehicleStock.Infrastructure.Seeds
{
    public class VehicleSeederDataSource : EntitySeederCsvDataSource<VehicleRecord, VehicleSeederDataSource.Record>
    {
        public class Record
        {
            public string DealerCode { get; set; }

            public string DistributorId { get; set; }

            public string CommercialNumber { get; set; }

            public string Vin { get; set; }

            public string ModelKey { get; set; }

            public string Options { get; set; }

            public string ColorId { get; set; }

            public string Engine { get; set; }

            public DateTime AssembledOn { get; set; }

            public DateTime DeliveredOn { get; set; }
        }

        public VehicleSeederDataSource(IMapper mapper, ITextReaderProvider readerProvider)
            : base(mapper, readerProvider)
        {
        }

        public VehicleSeederDataSource(IMapper mapper, string text)
            : base(mapper, new StringReaderProvider(text))
        {
        }
    }
}
