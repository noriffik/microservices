﻿using NexCore.Infrastructure.Seeds;
using NexCore.VehicleStock.Domain.Vehicles;

namespace NexCore.VehicleStock.Infrastructure.Seeds
{
    public class VehicleStockSeeder : CompositeSeeder, IDbContextSeeder<VehicleStockContext>
    {
        public VehicleStockSeeder(ISeeder<Vehicle> vehicleSeeder)
            : base(vehicleSeeder)
        {
        }
    }
}
