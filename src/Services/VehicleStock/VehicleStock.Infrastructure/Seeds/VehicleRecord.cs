﻿using NexCore.DealerNetwork;
using NexCore.VehicleStock.Domain.Vehicles;

namespace NexCore.VehicleStock.Infrastructure.Seeds
{
    public class VehicleRecord
    {
        public CommercialNumber Id { get; set; }

        public VehicleSpecification Specification { get; set; }

        public WarehouseDelivery WarehouseDelivery { get; set; }

        public DistributorId DistributorId { get; set; }

        public DealerCode DealerCode { get; set; }
    }
}
