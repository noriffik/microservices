﻿using AutoMapper;
using NexCore.DealerNetwork;
using NexCore.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Vehicles;

namespace NexCore.VehicleStock.Infrastructure.Seeds
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<VehicleSeederDataSource.Record, VehicleRecord>()
                .ForMember(d => d.Id, p => p.MapFrom(s => CommercialNumber.Parse(s.CommercialNumber)))
                .ForMember(d => d.Specification, p => p.MapFrom(s =>
                    new VehicleSpecification(ModelKey.Parse(s.ModelKey), PrNumberSet.Parse(s.Options), s.Engine, s.Vin, ColorId.Parse(s.ColorId))))
                .ForMember(d => d.WarehouseDelivery, p => p.MapFrom(s => new WarehouseDelivery(s.AssembledOn, s.DeliveredOn)))
                .ForMember(d => d.DistributorId, p => p.MapFrom(s => DistributorId.Parse(s.DistributorId)))
                .ForMember(d => d.DealerCode, p => p.MapFrom(s =>
                    string.IsNullOrEmpty(s.DealerCode) ? null : DealerCode.Parse(s.DealerCode)));
        }
    }
}
