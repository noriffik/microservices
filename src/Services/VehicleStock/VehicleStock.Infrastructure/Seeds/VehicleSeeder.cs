﻿using Microsoft.EntityFrameworkCore;
using NexCore.DealerNetwork;
using NexCore.Infrastructure.Seeds;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Warehouses;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Infrastructure.Seeds
{
    public class VehicleSeeder : ISeeder<Vehicle>
    {
        private readonly VehicleStockContext _dbContext;
        private readonly ISeederDataSource<VehicleRecord> _dataSource;

        public VehicleSeeder(VehicleStockContext dbContext, ISeederDataSource<VehicleRecord> dataSource)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _dataSource = dataSource ?? throw new ArgumentNullException(nameof(dataSource));
        }

        public async Task Seed()
        {
            var dealerWarehouses = await _dbContext.DealerWarehouses.ToListAsync();
            var distributorWarehouses = await _dbContext.DistributorWarehouses.ToListAsync();

            var vehicleRecords = _dataSource.Get()
                .ToList();

            var existingVehicles = await _dbContext.Vehicles
                .ToListAsync();
            var existingCommercialNumbers = existingVehicles.Select(v => v.Id).ToList();
            var existingVins = existingVehicles.Select(v => v.Specification.Vin).ToList();

            foreach (var record in vehicleRecords)
            {
                if (existingCommercialNumbers.Contains(record.Id) || existingVins.Contains(record.Specification.Vin))
                    continue;

                Warehouse warehouse;
                if (record.DealerCode == null)
                    warehouse = distributorWarehouses.SingleOrDefault(w => w.DistributorId == record.DistributorId);
                else
                {
                    var dealerId = DealerId.Next(record.DistributorId, record.DealerCode);
                    warehouse = dealerWarehouses.SingleOrDefault(w => w.DealerId == dealerId);
                }

                if (warehouse == null)
                    continue;

                var vehicle = new Vehicle(record.Id, record.Specification, record.WarehouseDelivery, warehouse.Id);
                _dbContext.Add(vehicle);
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}
