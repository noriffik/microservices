﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Infrastructure;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Warehouses;
using NexCore.VehicleStock.Domain.Warehouses.DealerWarehouses;
using NexCore.VehicleStock.Domain.Warehouses.DistributorWarehouses;
using NexCore.VehicleStock.Infrastructure.EntityTypes;

namespace NexCore.VehicleStock.Infrastructure
{
    public class VehicleStockContext : UnitOfWorkDbContext
    {
        public const string Schema = "stock";

        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<DealerWarehouse> DealerWarehouses { get; set; }
        public DbSet<DistributorWarehouse> DistributorWarehouses { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Reservation> Reservations { get; set; }

        public VehicleStockContext(DbContextOptions options) : base(options)
        {
        }

        public VehicleStockContext(DbContextOptions options, IMediator mediator) : base(options, mediator)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new WarehouseEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DealerWarehouseEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DistributorWarehouseEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new VehicleEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ReservationEntityTypeConfiguration());
        }
    }
}
