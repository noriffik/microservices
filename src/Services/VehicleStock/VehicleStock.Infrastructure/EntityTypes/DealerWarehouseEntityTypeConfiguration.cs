﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleStock.Domain.Warehouses;
using NexCore.VehicleStock.Domain.Warehouses.DealerWarehouses;

namespace NexCore.VehicleStock.Infrastructure.EntityTypes
{
    class DealerWarehouseEntityTypeConfiguration : IEntityTypeConfiguration<DealerWarehouse>
    {
        public void Configure(EntityTypeBuilder<DealerWarehouse> builder)
        {
            builder.HasBaseType<Warehouse>();

            builder.Property(x => x.DealerId)
                    .HasConversion(x => x.Value, x => x)
                    .IsRequired();

            builder.Property(x => x.DistributorId)
                .HasConversion(x => x.Value, x => x)
                .HasColumnName(nameof(DealerWarehouse.DistributorId))
                .IsRequired();

            builder.Property(x => x.DealerCode)
                .HasConversion(x => x.Value, x => x)
                .IsRequired();
        }
    }
}
