﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Warehouses;

namespace NexCore.VehicleStock.Infrastructure.EntityTypes
{
    class ReservationEntityTypeConfiguration : IEntityTypeConfiguration<Reservation>
    {
        public void Configure(EntityTypeBuilder<Reservation> builder)
        {
            builder.ToTable(nameof(Reservation), VehicleStockContext.Schema);

            builder.Property(r => r.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Reservation), VehicleStockContext.Schema)
                .HasColumnName(nameof(Reservation) + "Id");

            builder.OwnsOne(r => r.Vehicle, n =>
            {
                n.Property(r => r.ModelKey)
                    .HasColumnName(nameof(VehicleSpecification.ModelKey))
                    .HasConversion(k => k.Value, k => k)
                    .IsRequired();
                n.Property(r => r.Options)
                    .HasColumnName(nameof(VehicleSpecification.Options))
                    .HasConversion(o => o.AsString, o => o)
                    .IsRequired();
                n.Property(r => r.Engine)
                    .HasColumnName(nameof(VehicleSpecification.Engine))
                    .IsRequired();
                n.Property(p => p.Vin)
                    .HasColumnName(nameof(VehicleSpecification.Vin))
                    .IsRequired();
                n.Property(p => p.ColorId)
                    .HasColumnName(nameof(VehicleSpecification.ColorId))
                    .HasConversion(k => k.Value, k => k)
                    .IsRequired();
            });

            builder.HasOne<Warehouse>()
                .WithMany()
                .HasForeignKey(r => r.WarehouseId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
