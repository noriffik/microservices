﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleStock.Domain.Warehouses;
using NexCore.VehicleStock.Domain.Warehouses.DistributorWarehouses;

namespace NexCore.VehicleStock.Infrastructure.EntityTypes
{
    class DistributorWarehouseEntityTypeConfiguration : IEntityTypeConfiguration<DistributorWarehouse>
    {
        public void Configure(EntityTypeBuilder<DistributorWarehouse> builder)
        {
            builder.HasBaseType<Warehouse>();

            builder.Property(x => x.DistributorId)
                    .HasConversion(x => x.Value, x => x)
                    .HasColumnName(nameof(DistributorWarehouse.DistributorId))
                    .IsRequired();
        }
    }
}
