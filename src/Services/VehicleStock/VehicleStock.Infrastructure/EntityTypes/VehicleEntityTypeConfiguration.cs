﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Warehouses;

namespace NexCore.VehicleStock.Infrastructure.EntityTypes
{
    class VehicleEntityTypeConfiguration : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            builder.ToTable(nameof(Vehicle), VehicleStockContext.Schema);

            builder.Property(x => x.Id)
                .HasConversion(x => x.Value, x => x)
                .HasColumnName(typeof(Vehicle).Name + "Id")
                .IsRequired();

            builder.OwnsOne(p => p.Specification, n =>
            {
                n.Property(p => p.ModelKey)
                    .HasColumnName(nameof(VehicleSpecification.ModelKey))
                    .HasConversion(k => k.Value, k => k)
                    .IsRequired();
                n.Property(p => p.Options)
                    .HasColumnName(nameof(VehicleSpecification.Options))
                    .HasConversion(o => o.AsString, o => o)
                    .IsRequired();
                n.Property(p => p.Engine)
                    .HasColumnName(nameof(VehicleSpecification.Engine))
                    .IsRequired();
                n.Property(p => p.Vin)
                    .HasColumnName(nameof(VehicleSpecification.Vin))
                    .IsRequired();
                n.HasIndex(p => p.Vin).IsUnique();
                n.Property(p => p.ColorId)
                    .HasColumnName(nameof(VehicleSpecification.ColorId))
                    .HasConversion(k => k.Value, k => k)
                    .IsRequired();
            });

            builder.OwnsOne(p => p.WarehouseDelivery, n =>
            {
                n.Property(p => p.AssembledOn).HasColumnName(nameof(WarehouseDelivery.AssembledOn)).IsRequired();
                n.Property(p => p.DeliveredOn).HasColumnName(nameof(WarehouseDelivery.DeliveredOn));
            });

            builder.OwnsOne(p => p.ReservationState, n =>
            {
                n.Property(p => p.ReservationId).HasColumnName(nameof(ReservationState.ReservationId));
                n.Property(p => p.OnDate).HasColumnName("Reservation" + nameof(ReservationState.OnDate));
                n.Property(p => p.Status).HasColumnName("Reservation" + nameof(ReservationState.Status));
            });

            builder.HasOne<Warehouse>()
                .WithMany()
                .HasForeignKey(r => r.WarehouseId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
