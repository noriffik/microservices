﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.VehicleStock.Domain.Warehouses;
using NexCore.VehicleStock.Domain.Warehouses.DealerWarehouses;
using NexCore.VehicleStock.Domain.Warehouses.DistributorWarehouses;

namespace NexCore.VehicleStock.Infrastructure.EntityTypes
{
    public class WarehouseEntityTypeConfiguration : IEntityTypeConfiguration<Warehouse>
    {
        public void Configure(EntityTypeBuilder<Warehouse> builder)
        {
            builder.ToTable(nameof(Warehouse), VehicleStockContext.Schema);

            builder.Property(r => r.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Warehouse), VehicleStockContext.Schema)
                .HasColumnName(nameof(Warehouse) + "Id");

            builder.HasDiscriminator<string>("WarehouseType")
                .HasValue<DealerWarehouse>(nameof(DealerWarehouse))
                .HasValue<DistributorWarehouse>(nameof(DistributorWarehouse));

            builder.Property(w => w.Name).IsRequired();
        }
    }
}
