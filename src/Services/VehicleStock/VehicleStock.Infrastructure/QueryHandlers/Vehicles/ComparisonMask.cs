﻿using System;
using System.Text.RegularExpressions;

namespace NexCore.VehicleStock.Infrastructure.QueryHandlers.Vehicles
{
    public class ComparisonMask
    {
        private static readonly Regex Validation = new Regex(@"^[01]{5}[0-9]$");

        private string Value { get; }
            
        protected ComparisonMask(string value)
        {
            Value = value;
        }

        public bool IsBodyRequired => Value[0] == '1';

        public bool IsEquipmentRequired => Value[1] == '1';

        public bool IsEngineRequired => Value[2] == '1';

        public bool IsGearboxRequired => Value[3] == '1';

        public bool IsColorRequired => Value[4] == '1';

        public int DispensableOptionsCount => int.Parse(Value[5].ToString());

        public static ComparisonMask Parse(string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (!TryParse(value))
                throw new FormatException();

            return new ComparisonMask(value);
        }

        private static bool TryParse(string value)
        {
            if (Validation.Match(value).Success) return true;

            return false;
        }
    }
}
