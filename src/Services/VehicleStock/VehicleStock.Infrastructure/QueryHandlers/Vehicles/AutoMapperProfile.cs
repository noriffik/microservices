﻿using AutoMapper;
using NexCore.VehicleStock.Application.Vehicles.Queries;
using NexCore.VehicleStock.Domain.Vehicles;

namespace NexCore.VehicleStock.Infrastructure.QueryHandlers.Vehicles
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CommercialNumber, string>()
                .ConvertUsing(n => n.Value);

            CreateMap<Vehicle, VehicleDto>()
                .ForMember(d => d.ModelKey, p => p.MapFrom(s => s.Specification.ModelKey.Value))
                .ForMember(d => d.Engine, p => p.MapFrom(s => s.Specification.Engine))
                .ForMember(d => d.Options, p => p.MapFrom(s => s.Specification.Options.AsString))
                .ForMember(d => d.Vin, p => p.MapFrom(s => s.Specification.Vin))
                .ForMember(d => d.ColorId, p => p.MapFrom(s => s.Specification.ColorId))
                .ForMember(d => d.AssembledOn, p => p.MapFrom(s => s.WarehouseDelivery.AssembledOn))
                .ForMember(d => d.DeliveredOn, p => p.MapFrom(s => s.WarehouseDelivery.DeliveredOn))
                .ForMember(d => d.WarehouseId, p => p.MapFrom(s => s.WarehouseId))
                .ForMember(d => d.CommercialNumber, p => p.MapFrom(s => s.Id))
                .ForMember(d => d.ReservationState, p => p.MapFrom(s => s.ReservationState));
        }
    }
}
