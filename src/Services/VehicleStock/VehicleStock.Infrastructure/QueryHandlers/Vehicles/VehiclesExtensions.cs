﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Vehicles;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.VehicleStock.Infrastructure.QueryHandlers.Vehicles
{
    public static class VehiclesExtensions
    {
        private static IEnumerable<Vehicle> WhereBodyIdIs(this IEnumerable<Vehicle> source, ModelKey modelKey, ComparisonMask mask)
        {
            return mask.IsBodyRequired
                ? source.Where(o => o.Specification.ModelKey.BodyId == modelKey.BodyId)
                : source;
        }

        private static IEnumerable<Vehicle> WhereEquipmentIdIs(this IEnumerable<Vehicle> source, ModelKey modelKey, ComparisonMask mask)
        {
            return mask.IsEquipmentRequired
                ? source.Where(o => o.Specification.ModelKey.EquipmentId == modelKey.EquipmentId)
                : source;
        }

        private static IEnumerable<Vehicle> WhereEngineIdIs(this IEnumerable<Vehicle> source, ModelKey modelKey, ComparisonMask mask)
        {
            return mask.IsEngineRequired
                ? source.Where(o => o.Specification.ModelKey.EngineId == modelKey.EngineId)
                : source;
        }

        private static IEnumerable<Vehicle> WhereGearboxIdIs(this IEnumerable<Vehicle> source, ModelKey modelKey, ComparisonMask mask)
        {
            return mask.IsGearboxRequired
                ? source.Where(o => o.Specification.ModelKey.GearboxId == modelKey.GearboxId)
                : source;
        }

        public static IEnumerable<Vehicle> FilterByColorId(this IEnumerable<Vehicle> source, ColorId colorId)
        {
            return source.Where(o => o.Specification.ColorId == colorId).ToList();
        }

        public static IEnumerable<Vehicle> FilterByModelConfiguration(this IEnumerable<Vehicle> source, ModelKey modelKey, ComparisonMask mask)
        {
            return source.Distinct()
                .WhereBodyIdIs(modelKey, mask)
                .WhereEquipmentIdIs(modelKey, mask)
                .WhereEngineIdIs(modelKey, mask)
                .WhereGearboxIdIs(modelKey, mask);
        }

        public static IEnumerable<Vehicle> WhereHasAtLeastOptions(this IEnumerable<Vehicle> source, PrNumberSet options, int dispensableOptionsCount)
        {
            return source.Distinct()
                .Where(o => o.Specification.Options.HasAtLeast(options, options.Values.Count() - dispensableOptionsCount))
                .ToList();
        }
    }
}
