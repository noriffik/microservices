﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain.Vehicles;
using NexCore.VehicleStock.Application.Vehicles.Queries;
using NexCore.VehicleStock.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Infrastructure.QueryHandlers.Vehicles
{
    public class VehicleQueryHandler : IRequestHandler<ByIdQuery, VehicleDto>,
        IRequestHandler<AnyQuery, IEnumerable<VehicleDto>>,
        IRequestHandler<ByWarehouseIdQuery, IEnumerable<VehicleDto>>,
        IRequestHandler<MatchingQuery, IEnumerable<MatchedVehicleDto>>
    {
        private readonly VehicleStockContext _context;
        private readonly IMapper _mapper;

        public VehicleQueryHandler(VehicleStockContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<VehicleDto> Handle(ByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var vehicles = await _context.Vehicles.SingleOrDefaultAsync(v => Convert.ToString(v.Id) == request.Id, cancellationToken);

            return _mapper.Map<VehicleDto>(vehicles);
        }

        public async Task<IEnumerable<VehicleDto>> Handle(AnyQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var vehicles = await _context.Vehicles.ToListAsync(cancellationToken);

            return _mapper.Map<IEnumerable<VehicleDto>>(vehicles);
        }

        public async Task<IEnumerable<VehicleDto>> Handle(ByWarehouseIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var vehicles = await _context.Vehicles
                .Where(v => v.WarehouseId == request.WarehouseId)
                .ToListAsync(cancellationToken);

            return _mapper.Map<IEnumerable<VehicleDto>>(vehicles);
        }

        public async Task<IEnumerable<MatchedVehicleDto>> Handle(MatchingQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var modelKey = ModelKey.Parse(request.ModelKey);

            var options = PrNumberSet.Parse(request.Options);

            var colorId = ColorId.Parse(request.ColorId);

            var mask = ComparisonMask.Parse(request.Mask);

            var vehicles = await _context.Vehicles
                .Where(o => o.Specification.ModelKey.ModelId == modelKey.ModelId)
                .ToListAsync(cancellationToken);

            var matchingVehicles = vehicles
                .FilterByModelConfiguration(modelKey, mask)
                .WhereHasAtLeastOptions(options, mask.DispensableOptionsCount);

            if (mask.IsColorRequired) matchingVehicles = matchingVehicles.FilterByColorId(colorId);

            return GetMatchingVehiclesDto(matchingVehicles, options, colorId)
                .OrderBy(v => v.Comparison.AbsentOptionsCount)
                .ThenBy(v => v.Comparison.AdditionalOptionCount)
                .ThenByDescending(v => v.Comparison.IsColorMatched);
        }

        private IEnumerable<MatchedVehicleDto> GetMatchingVehiclesDto(IEnumerable<Vehicle> source, PrNumberSet options, ColorId colorId)
        {
            var result = new List<MatchedVehicleDto>();

            foreach (var vehicle in source)
            {
                result.Add(new MatchedVehicleDto
                {
                    Vehicle = _mapper.Map<VehicleDto>(vehicle),
                    Comparison = VehicleComparisonDto.Create(vehicle, options, colorId)
                });
            }

            return result;
        }
    }
}
