﻿using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Vehicles;

namespace NexCore.VehicleStock.Infrastructure.QueryHandlers.Reservations
{
    public class ReservationDtoRecord
    {
        public Reservation Reservation;

        public Vehicle Vehicle;
    }
}
