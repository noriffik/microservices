﻿using AutoMapper;
using NexCore.VehicleStock.Application.Reservations.Queries;

namespace NexCore.VehicleStock.Infrastructure.QueryHandlers.Reservations
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ReservationDtoRecord, ReservationDto>()
                .ForMember(d => d.Id, p => p.MapFrom(s => s.Reservation.Id))
                .ForMember(d => d.OnDate, p => p.MapFrom(s => s.Reservation.OnDate))
                .ForMember(d => d.CustomerId, p => p.MapFrom(s => s.Reservation.CustomerId))
                .ForMember(d => d.Vin, p => p.MapFrom(s => s.Reservation.Vehicle.Vin))
                .ForMember(d => d.ColorId, p => p.MapFrom(s => s.Reservation.Vehicle.ColorId))
                .ForMember(d => d.ModelKey, p => p.MapFrom(s => s.Reservation.Vehicle.ModelKey))
                .ForMember(d => d.Options, p => p.MapFrom(s => s.Reservation.Vehicle.Options))
                .ForMember(d => d.Engine, p => p.MapFrom(s => s.Reservation.Vehicle.Engine))
                .ForMember(d => d.WarehouseId, p => p.MapFrom(s => s.Reservation.WarehouseId))
                .ForMember(d => d.Status, p => p.MapFrom(s => s.Reservation.Status))
                .ForMember(d => d.Vehicle, p => p.MapFrom(s => s.Vehicle));
        }
    }
}
