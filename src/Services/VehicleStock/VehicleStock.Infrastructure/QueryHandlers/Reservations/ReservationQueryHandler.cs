﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.VehicleStock.Application.Reservations.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Infrastructure.QueryHandlers.Reservations
{
    public class ReservationQueryHandler : IRequestHandler<ByIdQuery, ReservationDto>,
        IRequestHandler<ByVinQuery, IEnumerable<ReservationDto>>
    {
        private readonly VehicleStockContext _context;
        private readonly IMapper _mapper;

        public ReservationQueryHandler(VehicleStockContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<ReservationDto> Handle(ByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await GetRecords.SingleOrDefaultAsync(r => r.Reservation.Id == request.Id, cancellationToken);

            return _mapper.Map<ReservationDto>(result);
        }

        private IQueryable<ReservationDtoRecord> GetRecords =>
            from reservation in _context.Reservations
            join vehicles in _context.Vehicles on reservation.Vehicle.Vin equals vehicles.Specification.Vin into vehicleJoin
            from vehicle in vehicleJoin.DefaultIfEmpty()
            select new ReservationDtoRecord
            {
                Reservation = reservation,
                Vehicle = vehicle
            };

        public async Task<IEnumerable<ReservationDto>> Handle(ByVinQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = await GetRecords
                .Where(r => r.Reservation.Vehicle.Vin == request.Vin.ToUpperInvariant())
                .ToListAsync(cancellationToken);

            return _mapper.Map<IEnumerable<ReservationDto>>(result);
        }
    }
}
