﻿using NexCore.Infrastructure;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.VehicleStock.Infrastructure
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Used by EF tool")]
    public class VehicleStockContextDesignTimeFactory : DbContextBaseDesignFactory<VehicleStockContext>
    {
        public VehicleStockContextDesignTimeFactory() : base("ConnectionString")
        {
        }
    }
}
