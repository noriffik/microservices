﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleStock.Infrastructure.Migrations
{
    public partial class Add_Reservation_As_AggregateRoot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_Reservation",
                schema: "stock",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Reservation",
                schema: "stock",
                columns: table => new
                {
                    ReservationId = table.Column<int>(nullable: false),
                    OnDate = table.Column<DateTime>(nullable: false),
                    CustomerId = table.Column<int>(nullable: true),
                    Vin = table.Column<string>(nullable: false),
                    ModelKey = table.Column<string>(nullable: false),
                    Options = table.Column<string>(nullable: false),
                    Engine = table.Column<string>(nullable: false),
                    DealerId = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reservation", x => x.ReservationId);
                    table.ForeignKey(
                        name: "FK_Reservation_DealerWarehouse_DealerId",
                        column: x => x.DealerId,
                        principalSchema: "stock",
                        principalTable: "DealerWarehouse",
                        principalColumn: "DealerWarehouseId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Reservation_DealerId",
                schema: "stock",
                table: "Reservation",
                column: "DealerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Reservation",
                schema: "stock");

            migrationBuilder.DropSequence(
                name: "sq_Reservation",
                schema: "stock");
        }
    }
}
