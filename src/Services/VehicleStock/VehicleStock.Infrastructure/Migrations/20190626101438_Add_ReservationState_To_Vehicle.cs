﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleStock.Infrastructure.Migrations
{
    public partial class Add_ReservationState_To_Vehicle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ReservationOnDate",
                schema: "stock",
                table: "Vehicle",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ReservationId",
                schema: "stock",
                table: "Vehicle",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ReservationStatus",
                schema: "stock",
                table: "Vehicle",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReservationOnDate",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "ReservationId",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "ReservationStatus",
                schema: "stock",
                table: "Vehicle");
        }
    }
}
