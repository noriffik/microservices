﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleStock.Infrastructure.Migrations
{
    public partial class Add_ColorId_To_VehicleSpecification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ColorId",
                schema: "stock",
                table: "Vehicle",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ColorId",
                schema: "stock",
                table: "Reservation",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ColorId",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "ColorId",
                schema: "stock",
                table: "Reservation");
        }
    }
}
