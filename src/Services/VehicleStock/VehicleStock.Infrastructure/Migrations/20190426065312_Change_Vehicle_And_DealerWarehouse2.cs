﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleStock.Infrastructure.Migrations
{
    public partial class Change_Vehicle_And_DealerWarehouse2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_DealerWarehouse_DealerId2",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.RenameColumn(
                name: "DealerId2",
                schema: "stock",
                table: "Vehicle",
                newName: "DealerId");

            migrationBuilder.RenameIndex(
                name: "IX_Vehicle_DealerId2",
                schema: "stock",
                table: "Vehicle",
                newName: "IX_Vehicle_DealerId");

            migrationBuilder.RenameColumn(
                name: "DealerWarehouseId2",
                schema: "stock",
                table: "DealerWarehouse",
                newName: "DealerWarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_DealerWarehouse_DealerId",
                schema: "stock",
                table: "Vehicle",
                column: "DealerId",
                principalSchema: "stock",
                principalTable: "DealerWarehouse",
                principalColumn: "DealerWarehouseId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_DealerWarehouse_DealerId",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.RenameColumn(
                name: "DealerId",
                schema: "stock",
                table: "Vehicle",
                newName: "DealerId2");

            migrationBuilder.RenameIndex(
                name: "IX_Vehicle_DealerId",
                schema: "stock",
                table: "Vehicle",
                newName: "IX_Vehicle_DealerId2");

            migrationBuilder.RenameColumn(
                name: "DealerWarehouseId",
                schema: "stock",
                table: "DealerWarehouse",
                newName: "DealerWarehouseId2");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_DealerWarehouse_DealerId2",
                schema: "stock",
                table: "Vehicle",
                column: "DealerId2",
                principalSchema: "stock",
                principalTable: "DealerWarehouse",
                principalColumn: "DealerWarehouseId2",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
