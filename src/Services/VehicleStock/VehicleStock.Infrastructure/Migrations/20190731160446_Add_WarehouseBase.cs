﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleStock.Infrastructure.Migrations
{
    public partial class Add_WarehouseBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reservation_DealerWarehouse_DealerId",
                schema: "stock",
                table: "Reservation");

            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_DealerWarehouse_DealerId",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropTable(
                name: "DealerWarehouse",
                schema: "stock");

            migrationBuilder.DropIndex(
                name: "IX_Vehicle_DealerId",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropIndex(
                name: "IX_Reservation_DealerId",
                schema: "stock",
                table: "Reservation");

            migrationBuilder.DropColumn(
                name: "DealerId",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "DealerId",
                schema: "stock",
                table: "Reservation");

            migrationBuilder.CreateSequence(
                name: "sq_Warehouse",
                schema: "stock",
                incrementBy: 10);

            migrationBuilder.AddColumn<int>(
                name: "WarehouseId",
                schema: "stock",
                table: "Vehicle",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "WarehouseId",
                schema: "stock",
                table: "Reservation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Warehouse",
                schema: "stock",
                columns: table => new
                {
                    WarehouseId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    ItemQuantity = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    WarehouseType = table.Column<string>(nullable: false),
                    DealerId = table.Column<string>(nullable: true),
                    ImporterId = table.Column<string>(nullable: true),
                    DealerCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Warehouse", x => x.WarehouseId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_WarehouseId",
                schema: "stock",
                table: "Vehicle",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_Reservation_WarehouseId",
                schema: "stock",
                table: "Reservation",
                column: "WarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reservation_Warehouse_WarehouseId",
                schema: "stock",
                table: "Reservation",
                column: "WarehouseId",
                principalSchema: "stock",
                principalTable: "Warehouse",
                principalColumn: "WarehouseId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_Warehouse_WarehouseId",
                schema: "stock",
                table: "Vehicle",
                column: "WarehouseId",
                principalSchema: "stock",
                principalTable: "Warehouse",
                principalColumn: "WarehouseId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reservation_Warehouse_WarehouseId",
                schema: "stock",
                table: "Reservation");

            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_Warehouse_WarehouseId",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropTable(
                name: "Warehouse",
                schema: "stock");

            migrationBuilder.DropIndex(
                name: "IX_Vehicle_WarehouseId",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropIndex(
                name: "IX_Reservation_WarehouseId",
                schema: "stock",
                table: "Reservation");

            migrationBuilder.DropSequence(
                name: "sq_Warehouse",
                schema: "stock");

            migrationBuilder.DropColumn(
                name: "WarehouseId",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "WarehouseId",
                schema: "stock",
                table: "Reservation");

            migrationBuilder.AddColumn<string>(
                name: "DealerId",
                schema: "stock",
                table: "Vehicle",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DealerId",
                schema: "stock",
                table: "Reservation",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DealerWarehouse",
                schema: "stock",
                columns: table => new
                {
                    DealerWarehouseId = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DealerWarehouse", x => x.DealerWarehouseId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_DealerId",
                schema: "stock",
                table: "Vehicle",
                column: "DealerId");

            migrationBuilder.CreateIndex(
                name: "IX_Reservation_DealerId",
                schema: "stock",
                table: "Reservation",
                column: "DealerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reservation_DealerWarehouse_DealerId",
                schema: "stock",
                table: "Reservation",
                column: "DealerId",
                principalSchema: "stock",
                principalTable: "DealerWarehouse",
                principalColumn: "DealerWarehouseId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_DealerWarehouse_DealerId",
                schema: "stock",
                table: "Vehicle",
                column: "DealerId",
                principalSchema: "stock",
                principalTable: "DealerWarehouse",
                principalColumn: "DealerWarehouseId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
