﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleStock.Infrastructure.Migrations
{
    public partial class Change_Vehicle_And_DealerWarehouse1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_DealerWarehouse_DealerId",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropIndex(
                name: "IX_Vehicle_DealerId",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DealerWarehouse",
                schema: "stock",
                table: "DealerWarehouse");

            migrationBuilder.DropColumn(
                name: "DealerId",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "DealerWarehouseId",
                schema: "stock",
                table: "DealerWarehouse");

            migrationBuilder.DropColumn(
                name: "DealerCode",
                schema: "stock",
                table: "DealerWarehouse");

            migrationBuilder.AddColumn<string>(
                name: "DealerId2",
                schema: "stock",
                table: "Vehicle",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DealerWarehouseId2",
                schema: "stock",
                table: "DealerWarehouse",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DealerWarehouse",
                schema: "stock",
                table: "DealerWarehouse",
                column: "DealerWarehouseId2");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_DealerId2",
                schema: "stock",
                table: "Vehicle",
                column: "DealerId2");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_DealerWarehouse_DealerId2",
                schema: "stock",
                table: "Vehicle",
                column: "DealerId2",
                principalSchema: "stock",
                principalTable: "DealerWarehouse",
                principalColumn: "DealerWarehouseId2",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_DealerWarehouse_DealerId2",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropIndex(
                name: "IX_Vehicle_DealerId2",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DealerWarehouse",
                schema: "stock",
                table: "DealerWarehouse");

            migrationBuilder.DropColumn(
                name: "DealerId2",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "DealerWarehouseId2",
                schema: "stock",
                table: "DealerWarehouse");

            migrationBuilder.AddColumn<int>(
                name: "DealerId",
                schema: "stock",
                table: "Vehicle",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DealerWarehouseId",
                schema: "stock",
                table: "DealerWarehouse",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "DealerCode",
                schema: "stock",
                table: "DealerWarehouse",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_DealerWarehouse",
                schema: "stock",
                table: "DealerWarehouse",
                column: "DealerWarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_DealerId",
                schema: "stock",
                table: "Vehicle",
                column: "DealerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_DealerWarehouse_DealerId",
                schema: "stock",
                table: "Vehicle",
                column: "DealerId",
                principalSchema: "stock",
                principalTable: "DealerWarehouse",
                principalColumn: "DealerWarehouseId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
