﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleStock.Infrastructure.Migrations
{
    public partial class Move_Vin_To_VehicleSpecification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_Vehicle_Vin",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_Vin",
                schema: "stock",
                table: "Vehicle",
                column: "Vin",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Vehicle_Vin",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_Vehicle_Vin",
                schema: "stock",
                table: "Vehicle",
                column: "Vin");
        }
    }
}
