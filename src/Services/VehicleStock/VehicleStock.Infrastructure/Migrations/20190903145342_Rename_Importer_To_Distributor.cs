﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleStock.Infrastructure.Migrations
{
    public partial class Rename_Importer_To_Distributor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImporterId",
                schema: "stock",
                table: "Warehouse");

            migrationBuilder.AddColumn<string>(
                name: "DistributorId",
                schema: "stock",
                table: "Warehouse",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DistributorId",
                schema: "stock",
                table: "Warehouse");

            migrationBuilder.AddColumn<string>(
                name: "ImporterId",
                schema: "stock",
                table: "Warehouse",
                nullable: true);
        }
    }
}
