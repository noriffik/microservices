﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleStock.Infrastructure.Migrations
{
    public partial class Add_Vehicle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Vehicle",
                schema: "stock",
                columns: table => new
                {
                    VehicleId = table.Column<string>(nullable: false),
                    Vin = table.Column<string>(nullable: false),
                    ModelKey = table.Column<string>(nullable: false),
                    Options = table.Column<string>(nullable: false),
                    Engine = table.Column<string>(nullable: false),
                    AssembledOn = table.Column<DateTime>(nullable: false),
                    DeliveredOn = table.Column<DateTime>(nullable: false),
                    DealerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicle", x => x.VehicleId);
                    table.UniqueConstraint("AK_Vehicle_Vin", x => x.Vin);
                    table.ForeignKey(
                        name: "FK_Vehicle_DealerWarehouse_DealerId",
                        column: x => x.DealerId,
                        principalSchema: "stock",
                        principalTable: "DealerWarehouse",
                        principalColumn: "DealerWarehouseId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_DealerId",
                schema: "stock",
                table: "Vehicle",
                column: "DealerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Vehicle",
                schema: "stock");
        }
    }
}
