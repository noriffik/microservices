﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.VehicleStock.Infrastructure.Migrations
{
    public partial class Drop_Reservation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_Reservation_ReservationId",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropTable(
                name: "Reservation",
                schema: "stock");

            migrationBuilder.DropIndex(
                name: "IX_Vehicle_ReservationId",
                schema: "stock",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "ReservationId",
                schema: "stock",
                table: "Vehicle");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ReservationId",
                schema: "stock",
                table: "Vehicle",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Reservation",
                schema: "stock",
                columns: table => new
                {
                    VehicleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CustomerId = table.Column<int>(nullable: true),
                    OnDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reservation", x => x.VehicleId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_ReservationId",
                schema: "stock",
                table: "Vehicle",
                column: "ReservationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_Reservation_ReservationId",
                schema: "stock",
                table: "Vehicle",
                column: "ReservationId",
                principalSchema: "stock",
                principalTable: "Reservation",
                principalColumn: "VehicleId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
