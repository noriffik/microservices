﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleStock.Application.Vehicles.Queries
{
    [DataContract]
    public class ByWarehouseIdQuery : IRequest<IEnumerable<VehicleDto>>
    {
        [DataMember]
        public int WarehouseId { get; set; }
    }
}
