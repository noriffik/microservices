﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleStock.Application.Vehicles.Queries
{
    [DataContract]
    public class MatchingQuery : IRequest<IEnumerable<MatchedVehicleDto>>
    {
        [DataMember]
        public string ModelKey { get; set; }

        [DataMember]
        public string ColorId { get; set; }

        [DataMember]
        public string Options { get; set; }

        [DataMember]
        public string Mask { get; set; }
    }
}
