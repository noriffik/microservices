﻿using System;
using System.Runtime.Serialization;

namespace NexCore.VehicleStock.Application.Vehicles.Queries
{
    [DataContract]
    public class VehicleDto
    {
        [DataMember]
        public string CommercialNumber { get; set; }

        [DataMember]
        public string ColorId { get; set; }

        [DataMember]
        public string Vin { get; set; }

        [DataMember]
        public string ModelKey { get; set; }

        [DataMember]
        public string Options { get; set; }

        [DataMember]
        public string Engine { get; set; }

        [DataMember]
        public DateTime AssembledOn { get; set; }

        [DataMember]
        public DateTime DeliveredOn { get; set; }

        [DataMember]
        public int WarehouseId { get; set; }

        [DataMember]
        public bool IsTemporaryUnavailable { get; set; }

        [DataMember]
        public ReservationStateDto ReservationState { get; set; }
    }
}
