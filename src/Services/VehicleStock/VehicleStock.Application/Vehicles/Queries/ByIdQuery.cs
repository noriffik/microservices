﻿using System.Runtime.Serialization;
using MediatR;

namespace NexCore.VehicleStock.Application.Vehicles.Queries
{
    [DataContract]
    public class ByIdQuery : IRequest<VehicleDto>
    {
        [DataMember]
        public string Id { get; set; }
    }
}
