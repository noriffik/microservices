﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using NexCore.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Vehicles;

namespace NexCore.VehicleStock.Application.Vehicles.Queries
{
    [DataContract]
    public class VehicleComparisonDto
    {
        [DataMember]
        public bool IsColorMatched { get; private set; }

        [DataMember]
        public int AbsentOptionsCount { get; private set; }

        [DataMember]
        public double MatchedOptionsPercentage { get; private set; }

        [DataMember]
        public int AdditionalOptionCount { get; private set; }

        [DataMember]
        public double AdditionalOptionPercentage { get; private set; }

        public static VehicleComparisonDto Create(Vehicle vehicle, PrNumberSet options, ColorId colorId)
        {
            if (vehicle == null) throw new ArgumentNullException(nameof(vehicle));
            if (options == null) throw new ArgumentNullException(nameof(options));
            if (colorId == null) throw new ArgumentNullException(nameof(colorId));

            var requiredOptionsNumber = options.Values.Count();
            var presentOptionsNumber = vehicle.Specification.Options.Values.Count();
            var matchingOptionsNumber = vehicle.Specification.Options.Values.Intersect(options.Values).Count();
           
            return new VehicleComparisonDto
            {
                IsColorMatched = vehicle.Specification.ColorId.Equals(colorId),
                AbsentOptionsCount = requiredOptionsNumber - matchingOptionsNumber,
                
                MatchedOptionsPercentage = Math.Round((double)matchingOptionsNumber/requiredOptionsNumber * 100),
                AdditionalOptionCount = presentOptionsNumber - matchingOptionsNumber,
                AdditionalOptionPercentage = Math.Round((double)(requiredOptionsNumber + presentOptionsNumber - matchingOptionsNumber) / requiredOptionsNumber * 100 - 100)
            };
        }
    }
}
