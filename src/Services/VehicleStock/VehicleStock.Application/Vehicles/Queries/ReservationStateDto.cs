﻿using NexCore.VehicleStock.Domain.Vehicles;
using System;
using System.Runtime.Serialization;

namespace NexCore.VehicleStock.Application.Vehicles.Queries
{
    [DataContract]
    public class ReservationStateDto
    {
        [DataMember]
        public int? ReservationId { get; set; }

        [DataMember]
        public ReservationStateStatus Status { get; set; }

        [DataMember]
        public DateTime? OnDate { get; set; }
    }
}
