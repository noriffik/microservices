﻿using System.Runtime.Serialization;

namespace NexCore.VehicleStock.Application.Vehicles.Queries
{
    [DataContract]
    public class MatchedVehicleDto
    {
        [DataMember]
        public VehicleDto Vehicle { get; set; }

        [DataMember]
        public VehicleComparisonDto Comparison { get; set; }
    }
}
