﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MediatR;

namespace NexCore.VehicleStock.Application.Vehicles.Queries
{
    [DataContract]
    public class AnyQuery : IRequest<IEnumerable<VehicleDto>>
    {
    }
}
