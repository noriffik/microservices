﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleStock.Application.Vehicles.Commands.Delete
{
    [DataContract]
    public class DeleteByVinCommand : IRequest
    {
        [DataMember]
        public string Vin { get; set; }
    }
}
