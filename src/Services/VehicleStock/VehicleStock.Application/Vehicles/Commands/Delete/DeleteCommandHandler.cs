﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Reservations.Specifications;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Vehicles.Specifications;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Application.Vehicles.Commands.Delete
{
    public class DeleteCommandHandler : IRequestHandler<DeleteByVinCommand>,
        IRequestHandler<DeleteByIdCommand>
    {
        private readonly IUnitOfWork _work;

        public DeleteCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public Task<Unit> Handle(DeleteByVinCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return InnerHandle(new VehicleByVinSpecification(request.Vin), cancellationToken);
        }

        public Task<Unit> Handle(DeleteByIdCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var commercialNumber = CommercialNumber.Parse(request.Id);

            return InnerHandle(new IdSpecification<Vehicle, CommercialNumber>(commercialNumber), cancellationToken);
        }

        private async Task<Unit> InnerHandle(Specification<Vehicle> specification, CancellationToken cancellationToken)
        {
            var vehicle = await _work.EntityRepository.Require<Vehicle, CommercialNumber>(specification, cancellationToken);

            var isReserved = await _work.EntityRepository.Has(new ActualByVinSpecification(vehicle.Specification.Vin), cancellationToken);
            if (isReserved)
                throw new IsReservedException("Deletion of reserved Vehicle is not allowed");

            await _work.EntityRepository.Remove<Vehicle, CommercialNumber>(vehicle);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
