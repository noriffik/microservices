﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleStock.Application.Vehicles.Commands.Delete
{
    [DataContract]
    public class DeleteByIdCommand : IRequest
    {
        [DataMember]
        public string Id { get; set; }
    }
}
