﻿using FluentValidation;

namespace NexCore.VehicleStock.Application.Vehicles.Commands.Delete
{
    public class DeleteByVinCommandValidator : AbstractValidator<DeleteByVinCommand>
    {
        public DeleteByVinCommandValidator()
        {
            RuleFor(c => c.Vin).NotEmpty();
        }
    }
}
