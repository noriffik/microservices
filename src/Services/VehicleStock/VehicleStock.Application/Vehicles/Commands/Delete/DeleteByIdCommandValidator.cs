﻿using FluentValidation;

namespace NexCore.VehicleStock.Application.Vehicles.Commands.Delete
{
    public class DeleteByIdCommandValidator : AbstractValidator<DeleteByIdCommand>
    {
        public DeleteByIdCommandValidator()
        {
            RuleFor(c => c.Id).NotEmpty();
        }
    }
}
