﻿using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleStock.Application.Vehicles.Commands
{
    public class PrNumberSetValidator : AbstractValidator<string>
    {
        public PrNumberSetValidator()
        {
            RuleFor(v => v).NotNull()
                .Must(v => PrNumberSet.TryParse(v, out _))
                .WithMessage("Invalid format.");
        }
    }
}
