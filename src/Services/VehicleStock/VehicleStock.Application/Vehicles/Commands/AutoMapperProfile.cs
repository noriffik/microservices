﻿using AutoMapper;
using NexCore.Domain.Vehicles;
using NexCore.VehicleStock.Application.Vehicles.Commands.Add;
using NexCore.VehicleStock.Domain.Vehicles;

namespace NexCore.VehicleStock.Application.Vehicles.Commands
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AddCommand, Vehicle>()
                .ConvertUsing(s => new Vehicle(CommercialNumber.Parse(s.CommercialNumber),
                    new VehicleSpecification(ModelKey.Parse(s.ModelKey),
                        PrNumberSet.Parse(s.Options),
                        s.Engine,
                        s.Vin,
                        ColorId.Parse(s.ColorId)),
                    new WarehouseDelivery(s.AssembledOn, s.DeliveredOn),
                    s.WarehouseId));
        }
    }
}
