﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleStock.Application.Vehicles.Commands.ChangeUnavailability
{
    [DataContract]
    public class ChangeUnavailabilityCommand : IRequest
    {
        [DataMember]
        public string CommercialNumber { get; set; }

        [DataMember]
        public bool IsTemporaryUnavailable { get; set; }
    }
}
