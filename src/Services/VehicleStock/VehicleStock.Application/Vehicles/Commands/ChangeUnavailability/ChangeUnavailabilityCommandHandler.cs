﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleStock.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Application.Vehicles.Commands.ChangeUnavailability
{
    public class ChangeUnavailabilityCommandHandler : IRequestHandler<ChangeUnavailabilityCommand>
    {
        private readonly IUnitOfWork _work;

        public ChangeUnavailabilityCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(ChangeUnavailabilityCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var vehicle = await _work.EntityRepository.Require<Vehicle, CommercialNumber>(
                    CommercialNumber.Parse(request.CommercialNumber), cancellationToken);

            vehicle.ChangeUnavailability(request.IsTemporaryUnavailable);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
