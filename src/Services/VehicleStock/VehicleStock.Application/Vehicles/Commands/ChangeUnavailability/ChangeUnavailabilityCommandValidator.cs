﻿using FluentValidation;

namespace NexCore.VehicleStock.Application.Vehicles.Commands.ChangeUnavailability
{
    public class ChangeUnavailabilityCommandValidator : AbstractValidator<ChangeUnavailabilityCommand>
    {
        public ChangeUnavailabilityCommandValidator()
        {
            RuleFor(c => c.CommercialNumber).NotEmpty();
        }
    }
}
