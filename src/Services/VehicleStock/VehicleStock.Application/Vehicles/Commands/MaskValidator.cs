﻿using FluentValidation;

namespace NexCore.VehicleStock.Application.Vehicles.Commands
{
    public class MaskValidator : AbstractValidator<string>
    {
        public MaskValidator()
        {
            RuleFor(m => m)
                .NotNull()
                .NotEmpty()
                .Matches(@"^[01]{5}[0-9]$")
                .WithMessage("Invalid mask format.");
        }
    }
}
