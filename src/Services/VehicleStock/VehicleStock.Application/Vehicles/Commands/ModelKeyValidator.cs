﻿using FluentValidation;
using NexCore.Domain.Vehicles;

namespace NexCore.VehicleStock.Application.Vehicles.Commands
{
    public class ModelKeyValidator : AbstractValidator<string>
    {
        public ModelKeyValidator()
        {
            RuleFor(v => v).NotEmpty()
                .Must(v => ModelKey.TryParse(v, out _))
                .WithMessage("Invalid format.");
        }
    }
}
