﻿using AutoMapper;
using MediatR;
using NexCore.Domain;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Vehicles.Specifications;
using NexCore.VehicleStock.Domain.Warehouses;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Application.Vehicles.Commands.Add
{
    public class AddCommandHandler : IRequestHandler<AddCommand, string>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public AddCommandHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<string> Handle(AddCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var vehicle = _mapper.Map<Vehicle>(request);

            await _work.EntityRepository.HasRequired<Warehouse>(vehicle.WarehouseId, cancellationToken);

            if (await _work.EntityRepository.Has<Vehicle, CommercialNumber>(new VehicleByIdOrVinSpecification(vehicle.Id, vehicle.Specification.Vin), cancellationToken))
                throw new DuplicateVehicleException(vehicle.Id, vehicle.Specification.Vin);

            await _work.EntityRepository.Add<Vehicle, CommercialNumber>(vehicle);

            await _work.Commit(cancellationToken);

            return vehicle.Id.Value;
        }
    }
}
