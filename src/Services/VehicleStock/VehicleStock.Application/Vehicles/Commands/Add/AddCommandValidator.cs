﻿using FluentValidation;

namespace NexCore.VehicleStock.Application.Vehicles.Commands.Add
{
    public class AddCommandValidator : AbstractValidator<AddCommand>
    {
        public AddCommandValidator()
        {
            RuleFor(c => c.CommercialNumber).NotEmpty();
            RuleFor(c => c.Vin).NotEmpty();
            RuleFor(c => c.ModelKey).NotNull().SetValidator(new ModelKeyValidator());
            RuleFor(c => c.Engine).NotEmpty();
            RuleFor(c => c.Options).NotNull().SetValidator(new PrNumberSetValidator());
        }
    }
}
