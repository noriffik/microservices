﻿using NexCore.EventBus.Abstract;

namespace NexCore.VehicleStock.Application.Messages
{
    public class DealerAddedEvent : IntegrationEvent
    {
        public string DealerId { get; set; }

        public int OrganizationId { get; set; }

        public string Name { get; set; }
    }
}
