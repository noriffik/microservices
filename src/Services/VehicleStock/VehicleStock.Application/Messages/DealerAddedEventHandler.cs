﻿using AutoMapper;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using NexCore.VehicleStock.Domain.Warehouses.DealerWarehouses;
using NexCore.VehicleStock.Domain.Warehouses.DealerWarehouses.Specifications;
using NexCore.VehicleStock.Domain.Warehouses.DistributorWarehouses.Specifications;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Application.Messages
{
    public class DealerAddedEventHandler : IIntegrationEventHandler<DealerAddedEvent>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public DealerAddedEventHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task Handle(DealerAddedEvent @event)
        {
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));

            var warehouse = _mapper.Map<DealerWarehouse>(@event);

            var distributorExist = await _work.EntityRepository.Has(new DistributorWarehouseByDistributorIdSpecification(warehouse.DistributorId));
            if (!distributorExist)
                return;

            var dealerExist = await _work.EntityRepository.Has(new DealerWarehouseByDealerIdSpecification(warehouse.DealerId));
            if (dealerExist)
                return;

            await _work.EntityRepository.Add(warehouse);

            await _work.Commit();
        }
    }
}
