﻿using AutoMapper;
using NexCore.DealerNetwork;
using NexCore.VehicleStock.Domain.Warehouses.DealerWarehouses;
using NexCore.VehicleStock.Domain.Warehouses.DistributorWarehouses;

namespace NexCore.VehicleStock.Application.Messages
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<DistributorRegisteredEvent, DistributorWarehouse>()
                .ConvertUsing(s => new DistributorWarehouse(DistributorId.Parse(s.DistributorId), s.Name, s.OrganizationId));

            CreateMap<DealerAddedEvent, DealerWarehouse>()
                .ConvertUsing(s => new DealerWarehouse(DealerId.Parse(s.DealerId), s.Name, s.OrganizationId));
        }
    }
}
