﻿using AutoMapper;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using NexCore.VehicleStock.Domain.Warehouses.DistributorWarehouses;
using NexCore.VehicleStock.Domain.Warehouses.DistributorWarehouses.Specifications;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Application.Messages
{
    public class DistributorRegisteredEventHandler : IIntegrationEventHandler<DistributorRegisteredEvent>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public DistributorRegisteredEventHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task Handle(DistributorRegisteredEvent @event)
        {
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));

            var warehouse = _mapper.Map<DistributorWarehouse>(@event);

            if (await _work.EntityRepository.Has(new DistributorWarehouseByDistributorIdSpecification(warehouse.DistributorId)))
                return;

            await _work.EntityRepository.Add(warehouse);

            await _work.Commit();
        }
    }
}
