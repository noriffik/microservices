﻿using FluentValidation;

namespace NexCore.VehicleStock.Application.Reservations.Queries
{
    public class ByVinQueryValidator : AbstractValidator<ByVinQuery>
    {
        public ByVinQueryValidator()
        {
            RuleFor(q => q.Vin).NotEmpty();
        }
    }
}
