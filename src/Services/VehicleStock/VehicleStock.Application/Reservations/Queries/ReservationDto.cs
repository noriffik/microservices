﻿using NexCore.VehicleStock.Application.Vehicles.Queries;
using NexCore.VehicleStock.Domain.Reservations;
using System;
using System.Runtime.Serialization;

namespace NexCore.VehicleStock.Application.Reservations.Queries
{
    [DataContract]
    public class ReservationDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public DateTime OnDate { get; set; }

        [DataMember]
        public int? CustomerId { get; set; }

        [DataMember]
        public string Vin { get; set; }

        [DataMember]
        public string ColorId { get; set; }

        [DataMember]
        public string ModelKey { get; set; }

        [DataMember]
        public string Options { get; set; }

        [DataMember]
        public string Engine { get; set; }

        [DataMember]
        public int WarehouseId { get; set; }

        [DataMember]
        public ReservationStatus Status { get; set; }

        [DataMember]
        public VehicleDto Vehicle { get; set; }
    }
}
