﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.VehicleStock.Application.Reservations.Queries
{
    [DataContract]
    public class ByVinQuery : IRequest<IEnumerable<ReservationDto>>
    {
        [DataMember]
        public string Vin { get; set; }
    }
}
