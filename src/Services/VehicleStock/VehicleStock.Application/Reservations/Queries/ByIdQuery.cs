﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleStock.Application.Reservations.Queries
{
    [DataContract]
    public class ByIdQuery : IRequest<ReservationDto>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
