﻿using FluentValidation;

namespace NexCore.VehicleStock.Application.Reservations.Commands.Add
{
    public class AddByVinCommandValidator : AbstractValidator<AddByVinCommand>
    {
        public AddByVinCommandValidator()
        {
            RuleFor(c => c.CustomerId).GreaterThan(0);
            RuleFor(c => c.Vin).NotEmpty();
        }
    }
}
