﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleStock.Application.Reservations.Commands.Add
{
    [DataContract]
    public class AddByVinCommand : IRequest<int>
    {
        [DataMember]
        public int? CustomerId { get; set; }

        [DataMember]
        public string Vin { get; set; }
    }
}
