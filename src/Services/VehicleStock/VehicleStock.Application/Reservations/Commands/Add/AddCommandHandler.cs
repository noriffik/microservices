﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Reservations.Specifications;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Vehicles.Specifications;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Application.Reservations.Commands.Add
{
    public class AddCommandHandler : IRequestHandler<AddByVinCommand, int>, IRequestHandler<AddByCommercialNumberCommand, int>
    {
        private readonly IUnitOfWork _work;

        public AddCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public Task<int> Handle(AddByVinCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return InnerHandle(new VehicleByVinSpecification(request.Vin), request.CustomerId, cancellationToken);
        }

        public Task<int> Handle(AddByCommercialNumberCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return InnerHandle(new IdSpecification<Vehicle, CommercialNumber>(request.CommercialNumber), request.CustomerId, cancellationToken);
        }

        private async Task<int> InnerHandle(Specification<Vehicle> vehicleSpecification, int? customerId, CancellationToken cancellationToken)
        {
            var vehicle = await _work.EntityRepository.Require<Vehicle, CommercialNumber>(vehicleSpecification, cancellationToken);

            if (vehicle.IsTemporaryUnavailable)
                throw new InvalidDomainOperationException("Vehicle is temporary unavailable");

            var isReserved = await _work.EntityRepository.Has(new ActualByVinSpecification(vehicle.Specification.Vin), cancellationToken);
            if (isReserved)
                throw new IsReservedException("Vehicle is already reserved.");

            var reservation = new Reservation(vehicle, customerId);

            await _work.EntityRepository.Add(reservation);

            await _work.Commit(cancellationToken);

            return reservation.Id;
        }
    }
}
