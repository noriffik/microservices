﻿using FluentValidation;

namespace NexCore.VehicleStock.Application.Reservations.Commands.Add
{
    public class AddByCommercialNumberCommandValidator : AbstractValidator<AddByCommercialNumberCommand>
    {
        public AddByCommercialNumberCommandValidator()
        {
            RuleFor(c => c.CustomerId).GreaterThan(0);
            RuleFor(c => c.CommercialNumber).NotEmpty();
        }
    }
}
