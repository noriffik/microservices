﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleStock.Application.Reservations.Commands.Cancel
{
    [DataContract]
    public class CancelCommand : IRequest
    {
        [DataMember]
        public int Id { get; set; }
    }
}
