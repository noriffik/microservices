﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleStock.Domain.Reservations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Application.Reservations.Commands.Cancel
{
    public class CancelCommandHandler : IRequestHandler<CancelCommand>
    {
        private readonly IUnitOfWork _work;

        public CancelCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(CancelCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var reservation = await _work.EntityRepository.Require<Reservation>(request.Id, cancellationToken);

            reservation.Cancel();

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
