﻿using MediatR;
using NexCore.Domain;
using NexCore.VehicleStock.Domain.Reservations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.Application.Reservations.Commands.Complete
{
    public class CompleteCommandHandler : IRequestHandler<CompleteCommand>
    {
        private readonly IUnitOfWork _work;

        public CompleteCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(CompleteCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var reservation = await _work.EntityRepository.Require<Reservation>(request.Id, cancellationToken);

            reservation.Complete();

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
