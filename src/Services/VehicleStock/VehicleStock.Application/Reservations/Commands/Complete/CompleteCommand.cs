﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.VehicleStock.Application.Reservations.Commands.Complete
{
    [DataContract]
    public class CompleteCommand : IRequest
    {
        [DataMember]
        public int Id { get; set; }
    }
}
