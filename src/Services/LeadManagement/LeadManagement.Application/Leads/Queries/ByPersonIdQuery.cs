﻿using MediatR;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Queries
{
    [DataContract]
    public class ByPersonIdQuery : IRequest<IEnumerable<LeadDto>>
    {
        [DataMember]
        public int PersonId { get; set; }
    }
}
