﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Queries
{
    [DataContract]
    public class LeadDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int PersonId { get; set; }

        [DataMember]
        public int? PrivateCustomerId { get; set; }

        [DataMember]
        public DateTime CreatedAt { get; set; }

        [DataMember]
        public DateTime UpdatedAt { get; set; }

        [DataMember]
        public OfferDto MainOffer { get; set; }

        [DataMember]
        public int ModelsInterestedInCount { get; set; }

        [DataMember]
        public int ConfigurationsCount { get; set; }

        [DataMember]
        public int TestDrivesCount { get; set; }

        [DataMember]
        public int OffersCount { get; set; }

        [DataMember]
        public IEnumerable<ModelInterestedInDto> ModelsInterestedIn { get; set; }

        [DataMember]
        public IEnumerable<ConfigurationDto> Configurations { get; set; }

        [DataMember]
        public IEnumerable<TestDriveDto> TestDrives { get; set; }

        [DataMember]
        public IEnumerable<OfferDto> Offers { get; set; }

        [DataMember]
        public VehicleReservationDto Reservation { get; set; }
    }
}
