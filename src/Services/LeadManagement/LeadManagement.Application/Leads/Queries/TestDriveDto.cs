﻿using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Queries
{
    [DataContract]
    public class TestDriveDto
    {
        [DataMember]
        public int Id { get; set; }
    }
}
