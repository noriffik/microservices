﻿using System;
using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Queries
{
    [DataContract]
    public class VehicleReservationDto
    {
        [DataMember]
        public string CommercialNumber { get; set; }

        [DataMember]
        public string Vin { get; set; }

        [DataMember]
        public DateTime OnDate { get; set; }
    }
}
