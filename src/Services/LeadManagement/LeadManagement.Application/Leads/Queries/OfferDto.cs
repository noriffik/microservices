﻿using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Queries
{
    [DataContract]
    public class OfferDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int? Interest { get; set; }
    }
}
