﻿using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Queries
{
    [DataContract]
    public class ModelInterestedInDto
    {
        [DataMember]
        public string ModelId { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
