﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Queries
{
    [DataContract]
    public class ByIdQuery : IRequest<LeadDto>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
