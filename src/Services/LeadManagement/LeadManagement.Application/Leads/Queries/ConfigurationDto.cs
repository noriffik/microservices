﻿using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Queries
{
    [DataContract]
    public class ConfigurationDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int? Interest { get; set; }
    }
}
