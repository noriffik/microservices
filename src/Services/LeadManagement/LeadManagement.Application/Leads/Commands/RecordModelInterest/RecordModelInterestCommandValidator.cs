﻿using FluentValidation;

namespace NexCore.LeadManagement.Application.Leads.Commands.RecordModelInterest
{
    public class RecordModelInterestCommandValidator : AbstractValidator<RecordModelInterestCommand>
    {
        public RecordModelInterestCommandValidator()
        {
            RuleFor(c => c.LeadId).GreaterThan(0);
            RuleFor(c => c.ModelIds).NotNull();
        }
    }
}
