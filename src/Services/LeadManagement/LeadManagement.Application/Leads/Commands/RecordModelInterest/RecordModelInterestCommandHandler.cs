﻿using AutoMapper;
using MediatR;
using NexCore.Domain;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.LeadManagement.Application.Leads.Commands.RecordModelInterest
{
    public class RecordModelInterestCommandHandler : IRequestHandler<RecordModelInterestCommand>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public RecordModelInterestCommandHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<Unit> Handle(RecordModelInterestCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var lead = await _work.EntityRepository.Require<Lead>(request.LeadId, cancellationToken);

            var modelIds = _mapper.Map<IEnumerable<ModelId>>(request.ModelIds).ToList();

            await _work.EntityRepository.HasManyRequired<Model, ModelId>(modelIds, cancellationToken);

            lead.RecordModelInterest(modelIds);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
