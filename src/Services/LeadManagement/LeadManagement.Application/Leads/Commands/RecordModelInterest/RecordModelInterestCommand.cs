﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Commands.RecordModelInterest
{
    [DataContract]
    public class RecordModelInterestCommand : IRequest
    {
        [DataMember]
        public int LeadId { get; set; }

        [DataMember]
        public string ModelIds { get; set; }
    }
}
