﻿using FluentValidation;

namespace NexCore.LeadManagement.Application.Leads.Commands.RecordOffer
{
    public class RecordOfferCommandValidator : AbstractValidator<RecordOfferCommand>
    {
        public RecordOfferCommandValidator()
        {
            RuleFor(c => c.LeadId).GreaterThan(0);
            RuleFor(c => c.OfferId).GreaterThan(0);
        }
    }
}
