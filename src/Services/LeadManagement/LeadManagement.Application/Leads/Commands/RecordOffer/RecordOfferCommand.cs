﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Commands.RecordOffer
{
    [DataContract]
    public class RecordOfferCommand : IRequest
    {
        [DataMember]
        public int LeadId { get; set; }

        [DataMember]
        public int OfferId { get; set; }
    }
}
