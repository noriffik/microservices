﻿using MediatR;
using NexCore.Domain;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Domain.Leads.Specifications;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.LeadManagement.Application.Leads.Commands.RecordOffer
{
    public class RecordOfferCommandHandler : IRequestHandler<RecordOfferCommand>
    {
        private readonly IUnitOfWork _work;

        public RecordOfferCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(RecordOfferCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var lead = await _work.EntityRepository.Require<Lead>(request.LeadId, cancellationToken);

            if (await _work.EntityRepository.Has(new LeadIncludedOfferSpecification(request.OfferId), cancellationToken))
                throw new DuplicateOfferException(request.OfferId);

            lead.RecordOffer(request.OfferId);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
