﻿using AutoMapper;
using MediatR;
using NexCore.Domain;
using NexCore.LeadManagement.Domain.Leads;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.LeadManagement.Application.Leads.Commands.Add
{
    public class AddCommandHandler : IRequestHandler<AddCommand, int>
    {
        private readonly IUnitOfWork _work;
        private readonly IMapper _mapper;

        public AddCommandHandler(IUnitOfWork work, IMapper mapper)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<int> Handle(AddCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var lead = _mapper.Map<AddCommand, Lead>(request);

            await _work.EntityRepository.Add(lead);

            await _work.Commit(cancellationToken);

            return lead.Id;
        }
    }
}
