﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Commands.Add
{
    [DataContract]
    public class AddCommand : IRequest<int>
    {
        [DataMember]
        public int PersonId { get; set; }

        [DataMember]
        public int? PrivateCustomerId { get; set; }
    }
}
