﻿using FluentValidation;

namespace NexCore.LeadManagement.Application.Leads.Commands.Add
{
    public class AddCommandValidator :  AbstractValidator<AddCommand>
    {
        public AddCommandValidator()
        {
            RuleFor(c => c.PersonId).GreaterThan(0);
            RuleFor(c => c.PrivateCustomerId).GreaterThan(0);
        }
    }
}
