﻿using FluentValidation;

namespace NexCore.LeadManagement.Application.Leads.Commands.RecordConfiguration
{
    public class RecordConfigurationCommandValidator : AbstractValidator<RecordConfigurationCommand>
    {
        public RecordConfigurationCommandValidator()
        {
            RuleFor(c => c.LeadId).GreaterThan(0);
            RuleFor(c => c.ConfigurationId).GreaterThan(0);
        }
    }
}
