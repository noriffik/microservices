﻿using MediatR;
using NexCore.Domain;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Domain.Leads.Specifications;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.LeadManagement.Application.Leads.Commands.RecordConfiguration
{
    public class RecordConfigurationCommandHandler : IRequestHandler<RecordConfigurationCommand>
    {
        private readonly IUnitOfWork _work;

        public RecordConfigurationCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(RecordConfigurationCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var lead = await _work.EntityRepository.Require<Lead>(request.LeadId, cancellationToken);

            if (await _work.EntityRepository.Has(new LeadIncludedConfigurationSpecification(request.ConfigurationId), cancellationToken))
                throw new DuplicateConfigurationException(request.ConfigurationId);

            lead.RecordConfiguration(request.ConfigurationId);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
