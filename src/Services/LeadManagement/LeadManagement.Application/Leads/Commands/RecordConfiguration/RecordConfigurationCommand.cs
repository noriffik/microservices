﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Commands.RecordConfiguration
{
    [DataContract]
    public class RecordConfigurationCommand : IRequest
    {
        [DataMember]
        public int LeadId { get; set; }

        [DataMember]
        public int ConfigurationId { get; set; }
    }
}
