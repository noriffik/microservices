﻿using FluentValidation;

namespace NexCore.LeadManagement.Application.Leads.Commands.SetConfigurationInterest
{
    public class SetConfigurationInterestCommandValidator : AbstractValidator<SetConfigurationInterestCommand>
    {
        public SetConfigurationInterestCommandValidator()
        {
            RuleFor(c => c.LeadId).GreaterThan(0);
            RuleFor(c => c.ConfigurationId).GreaterThan(0);
            RuleFor(c => c.Interest).GreaterThan(0);
        }
    }
}
