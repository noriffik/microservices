﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Commands.SetConfigurationInterest
{
    [DataContract]
    public class SetConfigurationInterestCommand : IRequest
    {
        [DataMember]
        public int LeadId { get; set; }

        [DataMember]
        public int ConfigurationId { get; set; }

        [DataMember]
        public int Interest { get; set; }
    }
}
