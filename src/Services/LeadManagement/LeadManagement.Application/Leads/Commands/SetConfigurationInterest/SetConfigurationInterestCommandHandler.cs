﻿using MediatR;
using NexCore.Domain;
using NexCore.LeadManagement.Domain.Leads;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.LeadManagement.Application.Leads.Commands.SetConfigurationInterest
{
    public class SetConfigurationInterestCommandHandler : IRequestHandler<SetConfigurationInterestCommand>
    {
        private readonly IUnitOfWork _work;

        public SetConfigurationInterestCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(SetConfigurationInterestCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var lead = await _work.EntityRepository.Require<Lead>(request.LeadId, cancellationToken);

            lead.SetConfigurationInterest(request.ConfigurationId, request.Interest);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
