﻿using AutoMapper;
using NexCore.LeadManagement.Application.Leads.Commands.Add;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.LeadManagement.Application.Leads.Commands
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AddCommand, Lead>();

            CreateMap<string, IEnumerable<ModelId>>()
                .ConstructUsing(s => s.Split(' ', StringSplitOptions.RemoveEmptyEntries).Select(ModelId.Parse));
        }
    }
}
