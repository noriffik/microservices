﻿using MediatR;
using NexCore.Domain;
using NexCore.LeadManagement.Domain.Leads;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.LeadManagement.Application.Leads.Commands.SetOfferInterest
{
    public class SetOfferInterestCommandHandler : IRequestHandler<SetOfferInterestCommand>
    {
        private readonly IUnitOfWork _work;

        public SetOfferInterestCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(SetOfferInterestCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var lead = await _work.EntityRepository.Require<Lead>(request.LeadId, cancellationToken);

            lead.SetOfferInterest(request.OfferId, request.Interest);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
