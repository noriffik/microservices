﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Commands.SetOfferInterest
{
    [DataContract]
    public class SetOfferInterestCommand : IRequest
    {
        [DataMember]
        public int LeadId { get; set; }

        [DataMember]
        public int OfferId { get; set; }

        [DataMember]
        public int Interest { get; set; }
    }
}
