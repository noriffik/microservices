﻿using FluentValidation;

namespace NexCore.LeadManagement.Application.Leads.Commands.SetOfferInterest
{
    public class SetOfferInterestCommandValidator : AbstractValidator<SetOfferInterestCommand>
    {
        public SetOfferInterestCommandValidator()
        {
            RuleFor(c => c.LeadId).GreaterThan(0);
            RuleFor(c => c.OfferId).GreaterThan(0);
            RuleFor(c => c.Interest).GreaterThan(0);
        }
    }
}
