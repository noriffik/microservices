﻿using MediatR;
using NexCore.Domain;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Domain.Leads.Specifications;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.LeadManagement.Application.Leads.Commands.RecordTestDrive
{
    public class RecordTestDriveCommandHandler : IRequestHandler<RecordTestDriveCommand>
    {
        private readonly IUnitOfWork _work;

        public RecordTestDriveCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(RecordTestDriveCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var lead = await _work.EntityRepository.Require<Lead>(request.LeadId, cancellationToken);

            if (await _work.EntityRepository.Has(new LeadIncludedTestDriveSpecification(request.TestDriveId), cancellationToken))
                throw new DuplicateTestDriveException(request.TestDriveId);

            lead.RecordTestDrive(request.TestDriveId);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
