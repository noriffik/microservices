﻿using FluentValidation;

namespace NexCore.LeadManagement.Application.Leads.Commands.RecordTestDrive
{
    public class RecordTestDriveCommandValidator : AbstractValidator<RecordTestDriveCommand>
    {
        public RecordTestDriveCommandValidator()
        {
            RuleFor(c => c.LeadId).GreaterThan(0);
            RuleFor(c => c.TestDriveId).GreaterThan(0);
        }
    }
}
