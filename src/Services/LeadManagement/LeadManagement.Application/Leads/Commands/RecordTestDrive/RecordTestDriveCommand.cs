﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Commands.RecordTestDrive
{
    [DataContract]
    public class RecordTestDriveCommand : IRequest
    {
        [DataMember]
        public int LeadId { get; set; }

        [DataMember]
        public int TestDriveId { get; set; }
    }
}
