﻿using FluentValidation;

namespace NexCore.LeadManagement.Application.Leads.Commands.RecordVehicleReservation
{
    public class RecordVehicleReservationCommandValidator : AbstractValidator<RecordVehicleReservationCommand>
    {
        public RecordVehicleReservationCommandValidator()
        {
            RuleFor(c => c.LeadId).GreaterThan(0);
            RuleFor(c => c.CommercialNumber).NotEmpty();
            RuleFor(c => c.Vin).NotEmpty().Length(17);
        }
    }
}
