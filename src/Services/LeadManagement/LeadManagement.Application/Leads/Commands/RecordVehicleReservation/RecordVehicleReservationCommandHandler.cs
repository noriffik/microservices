﻿using MediatR;
using NexCore.Domain;
using NexCore.LeadManagement.Domain.Leads;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.LeadManagement.Application.Leads.Commands.RecordVehicleReservation
{
    public class RecordVehicleReservationCommandHandler : IRequestHandler<RecordVehicleReservationCommand>
    {
        private readonly IUnitOfWork _work;

        public RecordVehicleReservationCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(RecordVehicleReservationCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var lead = await _work.EntityRepository.Require<Lead>(request.LeadId, cancellationToken);

            lead.RecordVehicleReservation(request.CommercialNumber, request.Vin, request.OnDate);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
