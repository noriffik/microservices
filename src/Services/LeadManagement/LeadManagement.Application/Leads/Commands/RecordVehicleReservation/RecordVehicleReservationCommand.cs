﻿using MediatR;
using System;
using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Commands.RecordVehicleReservation
{
    [DataContract]
    public class RecordVehicleReservationCommand : IRequest
    {
        [DataMember]
        public int LeadId { get; set; }

        [DataMember]
        public string CommercialNumber { get; set; }

        [DataMember]
        public string Vin { get; set; }

        [DataMember]
        public DateTime OnDate { get; set; }
    }
}
