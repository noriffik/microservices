﻿using MediatR;
using System.Runtime.Serialization;

namespace NexCore.LeadManagement.Application.Leads.Commands.SetMainOffer
{
    [DataContract]
    public class SetMainOfferCommand : IRequest
    {
        [DataMember]
        public int LeadId { get; set; }

        [DataMember]
        public int? OfferId { get; set; }
    }
}
