﻿using MediatR;
using NexCore.Domain;
using NexCore.LeadManagement.Domain.Leads;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.LeadManagement.Application.Leads.Commands.SetMainOffer
{
    public class SetMainOfferCommandHandler : IRequestHandler<SetMainOfferCommand>
    {
        private readonly IUnitOfWork _work;

        public SetMainOfferCommandHandler(IUnitOfWork work)
        {
            _work = work ?? throw new ArgumentNullException(nameof(work));
        }

        public async Task<Unit> Handle(SetMainOfferCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var lead = await _work.EntityRepository.Require<Lead>(request.LeadId, cancellationToken);

            lead.SetMainOffer(request.OfferId);

            await _work.Commit(cancellationToken);

            return Unit.Value;
        }
    }
}
