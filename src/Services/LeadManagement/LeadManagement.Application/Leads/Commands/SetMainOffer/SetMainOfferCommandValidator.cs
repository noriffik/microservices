﻿using FluentValidation;

namespace NexCore.LeadManagement.Application.Leads.Commands.SetMainOffer
{
    public class SetMainOfferCommandValidator : AbstractValidator<SetMainOfferCommand>
    {
        public SetMainOfferCommandValidator()
        {
            RuleFor(c => c.LeadId).GreaterThan(0);
            RuleFor(c => c.OfferId).GreaterThan(0);
        }
    }
}
