﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.LeadManagement.Domain.Models;

namespace NexCore.LeadManagement.Infrastructure.EntityTypes.Models
{
    public class ModelEntityTypeConfiguration : IEntityTypeConfiguration<Model>
    {
        public void Configure(EntityTypeBuilder<Model> builder)
        {
            builder.ToTable(nameof(Model), LeadManagementContext.Schema);

            builder.Property(x => x.Id)
                .HasConversion(x => x.Value, x => x)
                .HasColumnName(typeof(Model).Name + "Id")
                .IsRequired();
        }
    }
}
