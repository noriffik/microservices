﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.LeadManagement.Domain.Leads;

namespace NexCore.LeadManagement.Infrastructure.EntityTypes.Leads
{
    class VehicleReservationEntityTypeConfiguration : IEntityTypeConfiguration<VehicleReservation>
    {
        public void Configure(EntityTypeBuilder<VehicleReservation> builder)
        {
            builder.ToTable(nameof(VehicleReservation), LeadManagementContext.Schema);

            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(VehicleReservation), LeadManagementContext.Schema)
                .HasColumnName(nameof(VehicleReservation) + "Id");
        }
    }
}
