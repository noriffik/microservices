﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using NexCore.LeadManagement.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.LeadManagement.Infrastructure.EntityTypes.Leads
{
    public partial class LeadEntityTypeConfiguration
    {
        public class ModelsInterestedInConverter : ValueConverter<IEnumerable<ModelId>, string>
        {
            public ModelsInterestedInConverter() : base(ids => ToProvider(ids), s => FromProvider(s))
            {
            }

            private static string ToProvider(IEnumerable<ModelId> ids)
            {
                var list = ids.ToList();

                return list.Any() ? list.Select(id => id.Value).Aggregate((a, b) => $"{a} {b}") : string.Empty;
            }

            private static IEnumerable<ModelId> FromProvider(string s)
            {
                return s.Split(' ', StringSplitOptions.RemoveEmptyEntries)
                    .Select(ModelId.Parse)
                    .ToList();
            }
        }
    }
}
