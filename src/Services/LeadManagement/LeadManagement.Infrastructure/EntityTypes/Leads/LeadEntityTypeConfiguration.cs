﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.LeadManagement.Domain.Leads;

namespace NexCore.LeadManagement.Infrastructure.EntityTypes.Leads
{
    public partial class LeadEntityTypeConfiguration : IEntityTypeConfiguration<Lead>
    {
        public void Configure(EntityTypeBuilder<Lead> builder)
        {
            builder.ToTable(nameof(Lead), LeadManagementContext.Schema);
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id)
                .ForSqlServerUseSequenceHiLo("sq_" + nameof(Lead), LeadManagementContext.Schema)
                .HasColumnName(nameof(Lead) + "Id");

            builder.Property(l => l.ModelsInterestedIn)
                .HasConversion(new ModelsInterestedInConverter());

            builder.Ignore(l => l.Offers);
            
            builder.HasMany<Offer>(Internal(nameof(Lead.Offers))).WithOne().OnDelete(DeleteBehavior.Cascade);
            builder.Metadata.FindNavigation(Internal(nameof(Lead.Offers))).IsEagerLoaded = true;

            builder.Ignore(l => l.Configurations);
            builder.HasMany<Configuration>(Internal(nameof(Lead.Configurations))).WithOne().OnDelete(DeleteBehavior.Cascade);
            builder.Metadata.FindNavigation(Internal(nameof(Lead.Configurations))).IsEagerLoaded = true;

            builder.Ignore(l => l.TestDrives);
            builder.HasMany<TestDrive>(Internal(nameof(Lead.TestDrives))).WithOne().OnDelete(DeleteBehavior.Cascade);
            builder.Metadata.FindNavigation(Internal(nameof(Lead.TestDrives))).IsEagerLoaded = true;

            builder.Metadata.FindNavigation(nameof(Lead.Reservation)).IsEagerLoaded = true;
        }

        private static string Internal(string propertyName)
        {
            return $"{propertyName}Internal";
        }
    }
}
