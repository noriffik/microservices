﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.LeadManagement.Domain.Leads;

namespace NexCore.LeadManagement.Infrastructure.EntityTypes.Leads
{
    public class ConfigurationEntityTypeConfiguration : IEntityTypeConfiguration<Configuration>
    {
        public void Configure(EntityTypeBuilder<Configuration> builder)
        {
            builder.ToTable(nameof(Configuration), LeadManagementContext.Schema);

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id).ValueGeneratedNever();

            builder.Property(c => c.Id)
                .HasColumnName(nameof(Configuration) + "Id");
        }
    }
}
