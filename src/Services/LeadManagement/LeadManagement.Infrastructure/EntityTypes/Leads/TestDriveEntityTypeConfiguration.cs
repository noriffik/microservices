﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.LeadManagement.Domain.Leads;

namespace NexCore.LeadManagement.Infrastructure.EntityTypes.Leads
{
    public class TestDriveEntityTypeConfiguration : IEntityTypeConfiguration<TestDrive>
    {
        public void Configure(EntityTypeBuilder<TestDrive> builder)
        {
            builder.ToTable(nameof(TestDrive), LeadManagementContext.Schema);

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id).ValueGeneratedNever();

            builder.Property(c => c.Id)
                .HasColumnName(nameof(TestDrive) + "Id");
        }
    }
}
