﻿using AutoMapper;
using NexCore.LeadManagement.Domain.Models;

namespace NexCore.LeadManagement.Infrastructure.Seeds
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ModelSeederDataSource.Record, Model>()
                .ConstructUsing(record => new Model(ModelId.Parse(record.Id), record.Name))
                .ForAllMembers(m => m.Ignore());
        }
    }
}
