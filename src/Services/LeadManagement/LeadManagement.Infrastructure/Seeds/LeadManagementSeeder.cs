﻿using NexCore.Infrastructure.Seeds;
using NexCore.LeadManagement.Domain.Models;

namespace NexCore.LeadManagement.Infrastructure.Seeds
{
    public class LeadManagementSeeder : CompositeSeeder, IDbContextSeeder<LeadManagementContext>
    {
        public LeadManagementSeeder(ISeeder<Model> modelSeeder)
            : base(modelSeeder)
        {
        }
    }
}
