﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Infrastructure;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Domain.Models;
using NexCore.LeadManagement.Infrastructure.EntityTypes.Leads;
using NexCore.LeadManagement.Infrastructure.EntityTypes.Models;

namespace NexCore.LeadManagement.Infrastructure
{
    public class LeadManagementContext : UnitOfWorkDbContext
    {
        public const string Schema = "leads";

        public DbSet<Lead> Leads { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        public DbSet<TestDrive> TestDrives { get; set; }
        public DbSet<VehicleReservation> VehicleReservations { get; set; }

        public LeadManagementContext(DbContextOptions options) : base(options)
        {
        }

        public LeadManagementContext(DbContextOptions options, IMediator mediator)
            : base(options, mediator)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new LeadEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ModelEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OfferEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ConfigurationEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TestDriveEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new VehicleReservationEntityTypeConfiguration());
        }
    }
}
