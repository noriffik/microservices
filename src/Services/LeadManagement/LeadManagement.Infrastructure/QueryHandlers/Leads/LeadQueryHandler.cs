﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.LeadManagement.Application.Leads.Queries;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.LeadManagement.Infrastructure.QueryHandlers.Leads
{
    public class LeadQueryHandler : IRequestHandler<ByIdQuery, LeadDto>,
        IRequestHandler<ByPersonIdQuery, IEnumerable<LeadDto>>
    {
        private readonly LeadManagementContext _context;
        private readonly IMapper _mapper;

        public LeadQueryHandler(LeadManagementContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<LeadDto> Handle(ByIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var lead = await _context.Leads.SingleOrDefaultAsync(l => l.Id == request.Id, CancellationToken.None);

            if (lead == null)
                return null;

            var models = await GetModelsInterestedIn(new[] { lead });

            return MapLeadModel(lead, models);
        }

        public async Task<IEnumerable<LeadDto>> Handle(ByPersonIdQuery request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var leads = await _context.Leads
                .Where(l => l.PersonId == request.PersonId)
                .ToListAsync(CancellationToken.None);

            var models = await GetModelsInterestedIn(leads);

            return leads.Select(lead => MapLeadModel(lead, models));
        }

        private async Task<IEnumerable<Model>> GetModelsInterestedIn(IEnumerable<Lead> leads)
        {
            var modelIds = leads.SelectMany(l => l.ModelsInterestedIn)
                .Select(id => id.Value)
                .Distinct()
                .ToList();

            var models = modelIds.Any()
                ? await _context.Models.Where(m => modelIds.Contains(m.Id))
                    .ToListAsync()
                : new List<Model>();

            return models;
        }

        private LeadDto MapLeadModel(Lead lead, IEnumerable<Model> models)
        {
            var leadModel = _mapper.Map<LeadDto>(lead);

            var modelsInterestedIn = models.Where(m => lead.ModelsInterestedIn.Contains(m.Id));

            leadModel.ModelsInterestedIn = _mapper.Map<IEnumerable<ModelInterestedInDto>>(modelsInterestedIn);

            return leadModel;
        }
    }
}
