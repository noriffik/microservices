﻿using AutoMapper;
using NexCore.LeadManagement.Application.Leads.Queries;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Domain.Models;
using System.Linq;

namespace NexCore.LeadManagement.Infrastructure.QueryHandlers.Leads
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Lead, LeadDto>()
                .ForMember(d => d.ModelsInterestedIn, p => p.Ignore())
                .ForMember(d => d.ModelsInterestedInCount, p => p.MapFrom(s => s.ModelsInterestedIn.Count()))
                .ForMember(d => d.ConfigurationsCount, p => p.MapFrom(s => s.Configurations.Count()))
                .ForMember(d => d.TestDrivesCount, p => p.MapFrom(s => s.TestDrives.Count()))
                .ForMember(d => d.OffersCount, p => p.MapFrom(s => s.Offers.Count()));

            CreateMap<Model, ModelInterestedInDto>()
                .ForMember(d => d.ModelId, p => p.MapFrom(s => s.Id));

            CreateMap<Configuration, ConfigurationDto>();
        }
    }
}
