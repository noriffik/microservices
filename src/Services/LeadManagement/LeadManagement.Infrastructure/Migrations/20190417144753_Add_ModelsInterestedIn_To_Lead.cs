﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.LeadManagement.Infrastructure.Migrations
{
    public partial class Add_ModelsInterestedIn_To_Lead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ModelsInterestedIn",
                schema: "leads",
                table: "Lead",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ModelsInterestedIn",
                schema: "leads",
                table: "Lead");
        }
    }
}
