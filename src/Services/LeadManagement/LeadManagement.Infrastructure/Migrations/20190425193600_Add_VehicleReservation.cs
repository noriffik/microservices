﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.LeadManagement.Infrastructure.Migrations
{
    public partial class Add_VehicleReservation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "sq_VehicleReservation",
                schema: "leads",
                incrementBy: 10);

            migrationBuilder.AddColumn<int>(
                name: "ReservationId",
                schema: "leads",
                table: "Lead",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "VehicleReservation",
                schema: "leads",
                columns: table => new
                {
                    VehicleReservationId = table.Column<int>(nullable: false),
                    CommercialNumber = table.Column<string>(nullable: true),
                    Vin = table.Column<string>(nullable: true),
                    OnDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleReservation", x => x.VehicleReservationId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Lead_ReservationId",
                schema: "leads",
                table: "Lead",
                column: "ReservationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Lead_VehicleReservation_ReservationId",
                schema: "leads",
                table: "Lead",
                column: "ReservationId",
                principalSchema: "leads",
                principalTable: "VehicleReservation",
                principalColumn: "VehicleReservationId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lead_VehicleReservation_ReservationId",
                schema: "leads",
                table: "Lead");

            migrationBuilder.DropTable(
                name: "VehicleReservation",
                schema: "leads");

            migrationBuilder.DropIndex(
                name: "IX_Lead_ReservationId",
                schema: "leads",
                table: "Lead");

            migrationBuilder.DropSequence(
                name: "sq_VehicleReservation",
                schema: "leads");

            migrationBuilder.DropColumn(
                name: "ReservationId",
                schema: "leads",
                table: "Lead");
        }
    }
}
