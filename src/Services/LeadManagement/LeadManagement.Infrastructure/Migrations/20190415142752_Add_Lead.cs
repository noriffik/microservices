﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.LeadManagement.Infrastructure.Migrations
{
    public partial class Add_Lead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "leads");

            migrationBuilder.CreateSequence(
                name: "sq_Lead",
                schema: "leads",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "Lead",
                schema: "leads",
                columns: table => new
                {
                    LeadId = table.Column<int>(nullable: false),
                    PersonId = table.Column<int>(nullable: false),
                    PrivateCustomerId = table.Column<int>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lead", x => x.LeadId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Lead",
                schema: "leads");

            migrationBuilder.DropSequence(
                name: "sq_Lead",
                schema: "leads");
        }
    }
}
