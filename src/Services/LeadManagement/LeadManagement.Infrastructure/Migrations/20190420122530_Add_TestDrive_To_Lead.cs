﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.LeadManagement.Infrastructure.Migrations
{
    public partial class Add_TestDrive_To_Lead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TestDrive",
                schema: "leads",
                columns: table => new
                {
                    TestDriveId = table.Column<int>(nullable: false),
                    LeadId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestDrive", x => x.TestDriveId);
                    table.ForeignKey(
                        name: "FK_TestDrive_Lead_LeadId",
                        column: x => x.LeadId,
                        principalSchema: "leads",
                        principalTable: "Lead",
                        principalColumn: "LeadId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TestDrive_LeadId",
                schema: "leads",
                table: "TestDrive",
                column: "LeadId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TestDrive",
                schema: "leads");
        }
    }
}
