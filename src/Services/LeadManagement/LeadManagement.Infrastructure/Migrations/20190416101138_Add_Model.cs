﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.LeadManagement.Infrastructure.Migrations
{
    public partial class Add_Model : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Model",
                schema: "leads",
                columns: table => new
                {
                    ModelId = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Model", x => x.ModelId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Model",
                schema: "leads");
        }
    }
}
