﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.LeadManagement.Infrastructure.Migrations
{
    public partial class Add_Offer_To_Lead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Offer",
                schema: "leads",
                columns: table => new
                {
                    OfferId = table.Column<int>(nullable: false),
                    Interest = table.Column<int>(nullable: true),
                    LeadId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Offer", x => x.OfferId);
                    table.ForeignKey(
                        name: "FK_Offer_Lead_LeadId",
                        column: x => x.LeadId,
                        principalSchema: "leads",
                        principalTable: "Lead",
                        principalColumn: "LeadId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Offer_LeadId",
                schema: "leads",
                table: "Offer",
                column: "LeadId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Offer",
                schema: "leads");
        }
    }
}
