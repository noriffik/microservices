﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexCore.LeadManagement.Infrastructure.Migrations
{
    public partial class Add_Configuration_To_Lead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Configuration",
                schema: "leads",
                columns: table => new
                {
                    ConfigurationId = table.Column<int>(nullable: false),
                    Interest = table.Column<int>(nullable: true),
                    LeadId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Configuration", x => x.ConfigurationId);
                    table.ForeignKey(
                        name: "FK_Configuration_Lead_LeadId",
                        column: x => x.LeadId,
                        principalSchema: "leads",
                        principalTable: "Lead",
                        principalColumn: "LeadId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Configuration_LeadId",
                schema: "leads",
                table: "Configuration",
                column: "LeadId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Configuration",
                schema: "leads");
        }
    }
}
