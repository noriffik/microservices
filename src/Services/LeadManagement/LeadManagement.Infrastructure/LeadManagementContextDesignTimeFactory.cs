﻿using NexCore.Infrastructure;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.LeadManagement.Infrastructure
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Used by EF tool")]
    public class LeadManagementContextDesignTimeFactory : DbContextBaseDesignFactory<LeadManagementContext>
    {
        public LeadManagementContextDesignTimeFactory() : base("ConnectionString")
        {
        }
    }
}
