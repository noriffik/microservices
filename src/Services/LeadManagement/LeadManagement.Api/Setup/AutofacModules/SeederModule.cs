﻿using Autofac;
using Microsoft.Extensions.Configuration;
using NexCore.Infrastructure.Seeds;
using NexCore.LeadManagement.Domain.Models;
using NexCore.LeadManagement.Infrastructure;
using NexCore.LeadManagement.Infrastructure.Seeds;

namespace NexCore.LeadManagement.Api.Setup.AutofacModules
{
    public class SeederModule : Module
    {
        private readonly IConfiguration _configuration;

        public SeederModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<LeadManagementSeeder>()
                .As<IDbContextSeeder<LeadManagementContext>>()
                .InstancePerLifetimeScope();

            RegisterModelSeeder(builder);
        }

        private void RegisterModelSeeder(ContainerBuilder builder)
        {
            builder.RegisterType<EntitySeeder<LeadManagementContext, Model, ModelId>>()
                .As<ISeeder<Model>>();

            builder.RegisterType<ModelSeederDataSource>()
                .WithParameter("readerProvider", new TextFileReaderProvider(
                    _configuration["Setup:Data:ModelDataPath"]))
                .As<ISeederDataSource<Model>>();
        }
    }
}
