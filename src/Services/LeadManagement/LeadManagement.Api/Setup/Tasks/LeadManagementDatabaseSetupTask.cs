﻿using Microsoft.Extensions.Configuration;
using NexCore.Infrastructure.Seeds;
using NexCore.Infrastructure.Setup;
using NexCore.LeadManagement.Infrastructure;
using System;

namespace NexCore.LeadManagement.Api.Setup.Tasks
{
    public class LeadManagementDatabaseSetupTask : DatabaseSetupTask<LeadManagementContext>
    {
        private const string IsSeedingEnabledKey = "IsSeedingEnabled";

        public LeadManagementDatabaseSetupTask(LeadManagementContext context) : base(context)
        {
        }

        public LeadManagementDatabaseSetupTask(LeadManagementContext context, IDbContextSeeder<LeadManagementContext> seeder,
            IConfiguration configuration) : base(context, seeder)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            
            IsSeedingEnabled = configuration.GetValue(IsSeedingEnabledKey, true);
        }
    }
}
