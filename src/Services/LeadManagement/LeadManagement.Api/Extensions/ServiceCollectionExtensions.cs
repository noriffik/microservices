﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.HealthChecks;
using NexCore.WebApi.Extensions;
using System;
using System.Collections.Generic;

namespace NexCore.LeadManagement.Api.Extensions
{
    static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddHealthChecks(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddHealthChecks(checks =>
            {
                var cacheDurationInMinutes = configuration.GetValue("HealthCheck:CacheDurationInMinutes", 1);

                checks.AddSqlCheck(
                    "LeadManagement",
                    configuration["ConnectionString"],
                    TimeSpan.FromMinutes(cacheDurationInMinutes));
            });

            return services;
        }

        public static IServiceCollection AddSwagger(this IServiceCollection services,
            IConfiguration configuration)
        {
            return services.AddSwaggerGen(
                configuration,
                "The Lead Management Service HTTP API",
                "v1",
                new Dictionary<string, string>
                {
                    { "NexCore.LeadManagement", "Lead Management Service API" }
                });
        }
    }
}
