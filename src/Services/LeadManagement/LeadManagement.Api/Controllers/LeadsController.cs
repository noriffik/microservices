﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NexCore.LeadManagement.Api.Authorization;
using NexCore.LeadManagement.Application.Leads.Commands.Add;
using NexCore.LeadManagement.Application.Leads.Commands.RecordConfiguration;
using NexCore.LeadManagement.Application.Leads.Commands.RecordModelInterest;
using NexCore.LeadManagement.Application.Leads.Commands.RecordOffer;
using NexCore.LeadManagement.Application.Leads.Commands.RecordTestDrive;
using NexCore.LeadManagement.Application.Leads.Commands.RecordVehicleReservation;
using NexCore.LeadManagement.Application.Leads.Commands.SetConfigurationInterest;
using NexCore.LeadManagement.Application.Leads.Commands.SetMainOffer;
using NexCore.LeadManagement.Application.Leads.Commands.SetOfferInterest;
using NexCore.LeadManagement.Application.Leads.Queries;
using NexCore.WebApi;
using NexCore.WebApi.Authorization;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace NexCore.LeadManagement.Api.Controllers
{
    [Authorize]
    [Route("api/leads")]
    public class LeadsController : MediatrController
    {
        public LeadsController(IMediator mediator, IAuthorizationService authorizationService)
            : base(mediator, authorizationService)
        {
        }

        [Route("{Id}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [Produces(typeof(LeadDto))]
        public Task<IActionResult> Get([FromRoute] ByIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("many/person/{PersonId}")]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<LeadDto>))]
        public Task<IActionResult> GetAll([FromRoute] ByPersonIdQuery query) => ExecuteQuery(query, QueryAuthorizationSettings.None);

        [Route("add")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> Add([FromBody] AddCommand command) => ExecuteCreateCommand(command, AuthorizationRequirements.None);

        [Route("record/model-interest")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> RecordModelInterest([FromBody] RecordModelInterestCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("record/test-drive")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> RecordTestDrive([FromBody] RecordTestDriveCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("record/configuration")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> RecordConfiguration([FromBody] RecordConfigurationCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("set/configuration-interest")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> SetConfigurationInterest([FromBody] SetConfigurationInterestCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("record/offer")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> RecordOffer([FromBody] RecordOfferCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("set/offer-interest")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> SetOfferInterest([FromBody] SetOfferInterestCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("set/main-offer")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> SetMainOffer([FromBody] SetMainOfferCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);

        [Route("record/vehicle-reservation")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public Task<IActionResult> RecordVehicleReservation([FromBody] RecordVehicleReservationCommand command) => ExecuteCommand(command, AuthorizationRequirements.None);
    }
}
