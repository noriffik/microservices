﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace NexCore.LeadManagement.Api.Authorization
{
    public static class AuthorizationRequirements
    {
        public static readonly IAuthorizationRequirement None = new AssertionRequirement(c => true);
    }
}
