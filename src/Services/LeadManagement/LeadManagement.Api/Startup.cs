﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NexCore.Application.Extensions;
using NexCore.EventBus.Abstract;
using NexCore.EventBus.Azure;
using NexCore.EventBus.RabbitMq;
using NexCore.Infrastructure.Extensions;
using NexCore.LeadManagement.Api.Extensions;
using NexCore.LeadManagement.Api.Setup.AutofacModules;
using NexCore.LeadManagement.Api.Setup.Tasks;
using NexCore.LeadManagement.Application.Leads.Commands.Add;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Infrastructure;
using NexCore.WebApi.Extensions;
using NexCore.WebApi.Filters;
using System;

namespace NexCore.LeadManagement.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var application = typeof(AddCommand).Assembly;
            var infrastructure = typeof(LeadManagementContext).Assembly;
            var domain = typeof(Lead).Assembly;

            services
                .AddTransient<LeadManagementDatabaseSetupTask>()
                .AddAutoMapper(application)
                .AddUnitOfWork<LeadManagementContext>(Configuration["ConnectionString"], Configuration)
                .AddHealthChecks(Configuration)
                .AddSwagger(Configuration)
                .AddMvc(options =>
                {
                    options.Filters.Add(typeof(ExceptionFilter));
                })
                .AddControllersAsServices();

            ConfigureAuthentication(services);
            
			RegisterEventBus(services);

            var containerBuilder = services.GetAutofacContainerBuilder(application, domain, infrastructure);

            containerBuilder.RegisterModule(new SeederModule(Configuration));

            return new AutofacServiceProvider(containerBuilder.Build());
        }

        // This method is required by testing to change authentication options
        protected virtual void ConfigureAuthentication(IServiceCollection services)
        {
            services.AddJwtBearerAuthentication(Configuration, "NexCore.LeadManagement");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");

                c.OAuthClientId("NexCore.LeadManagement.Swagger");
            });

            app.UseMvc();
        }

        private void RegisterEventBus(IServiceCollection service)
        {
	        var subscriptionClientName = Configuration["SubscriptionClientName"];
	        if (Configuration.GetValue<bool>("AzureServiceBusEnabled"))
	        {
		        service.AddSingleton<IEventBus, EventBusServiceBus>(s =>
		        {
			        var serviceBusConnection = s.GetRequiredService<IServiceBusPersistentConnection>();
			        var iLifetimeScope = s.GetRequiredService<ILifetimeScope>();
			        var logger = s.GetRequiredService<ILogger<EventBusServiceBus>>();
			        var subscriptionsManager = s.GetRequiredService<IEventBusSubscriptionsManager>();
			        return new EventBusServiceBus(serviceBusConnection, logger, subscriptionsManager, iLifetimeScope, subscriptionClientName);
		        });
	        }
	        else
	        {
		        service.AddSingleton<IEventBus, EventBusRabbitMq>(s =>
		        {
			        var serviceBusConnection = s.GetRequiredService<IRabbitMqPersistentConnection>();
			        var iLifetimeScope = s.GetRequiredService<ILifetimeScope>();
			        var logger = s.GetRequiredService<ILogger<EventBusRabbitMq>>();
			        var subscriptionsManager = s.GetRequiredService<IEventBusSubscriptionsManager>();
			        var retryCount = 5;
			        if (!string.IsNullOrEmpty(Configuration["EventBusRetryCount"]))
			        {
				        retryCount = int.Parse(Configuration["EventBusRetryCount"]);
			        }
			        return new EventBusRabbitMq(serviceBusConnection, logger, iLifetimeScope, subscriptionsManager, subscriptionClientName, retryCount);
		        });
	        }
        }
	}
}
