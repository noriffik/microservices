﻿using System;

namespace NexCore.LeadManagement.Domain.Leads
{
    public class OfferNotFoundException : OfferException
    {
        private const string DefaultMessage = "Offer with given id is not found.";
        private const string DefaultExtendedMessage = "Offer with id {0} is not found.";

        public OfferNotFoundException() : base(DefaultMessage)
        {
        }

        public OfferNotFoundException(string message) : base(message)
        {
        }

        public OfferNotFoundException(string message, Exception inner) : base(message, inner)
        {
        }

        public OfferNotFoundException(int offerId)
            : base(string.Format(DefaultExtendedMessage, offerId), offerId)
        {
        }

        public OfferNotFoundException(string message, int offerId)
            : base(message, offerId)
        {
        }

        public OfferNotFoundException(string message, int offerId, Exception inner)
            : base(message, offerId, inner)
        {
        }
    }
}
