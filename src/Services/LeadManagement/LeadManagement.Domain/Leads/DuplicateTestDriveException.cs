﻿using NexCore.Domain;
using System;

namespace NexCore.LeadManagement.Domain.Leads
{
    public class DuplicateTestDriveException : DomainException
    {
        private const string DefaultMessage = "TestDrive for given id already exist.";
        private const string DefaultExtendedMessage = "TestDrive for id {0} already exist.";

        public int TestDriveId { get; }

        public DuplicateTestDriveException() : base(DefaultMessage)
        {
        }

        public DuplicateTestDriveException(string message) : base(message)
        {
        }

        public DuplicateTestDriveException(string message, Exception inner) : base(message, inner)
        {
        }

        public DuplicateTestDriveException(int testDriveId)
            : this(string.Format(DefaultExtendedMessage, testDriveId), testDriveId)
        {
        }

        public DuplicateTestDriveException(string message, int testDriveId)
            : this(message, testDriveId, null)
        {
        }

        public DuplicateTestDriveException(string message, int testDriveId, Exception inner)
            : base(message, inner)
        {
            TestDriveId = testDriveId;
        }
    }
}
