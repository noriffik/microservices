﻿using System.Runtime.CompilerServices;
using NexCore.Domain;

[assembly: InternalsVisibleTo("NexCore.LeadManagement.UnitTests")]
namespace NexCore.LeadManagement.Domain.Leads
{
    public class TestDrive : Entity
    {
        private TestDrive()
        {
        }

        internal TestDrive(int id) : base(id)
        {
        }
    }
}
