﻿using NexCore.Domain;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.LeadManagement.Domain.Leads
{
    [SuppressMessage("ReSharper", "UnusedMember.Global", Justification = "Required by EF Core.")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core.")]
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core.")]
    public class VehicleReservation : Entity
    {
        public string CommercialNumber { get; private set; }
        public string Vin { get; private set; }
        public DateTime OnDate { get; private set; }

        private VehicleReservation()
        {
        }

        public VehicleReservation(string commercialNumber, string vin, DateTime onDate)
        {
            CommercialNumber = commercialNumber;
            Vin = vin;
            OnDate = onDate;
        }
    }
}