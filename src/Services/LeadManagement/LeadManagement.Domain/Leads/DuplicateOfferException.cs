﻿using System;

namespace NexCore.LeadManagement.Domain.Leads
{
    public class DuplicateOfferException : OfferException
    {
        private const string DefaultMessage = "Offer for given id already exist.";
        private const string DefaultExtendedMessage = "Offer for id {0} already exist.";

        public DuplicateOfferException() : base(DefaultMessage)
        {
        }

        public DuplicateOfferException(string message) : base(message)
        {
        }

        public DuplicateOfferException(string message, Exception inner) : base(message, inner)
        {
        }

        public DuplicateOfferException(int offerId)
            : base(string.Format(DefaultExtendedMessage, offerId), offerId)
        {
        }

        public DuplicateOfferException(string message, int offerId)
            : base(message, offerId)
        {
        }

        public DuplicateOfferException(string message, int offerId, Exception inner)
            : base(message, offerId, inner)
        {
        }
    }
}
