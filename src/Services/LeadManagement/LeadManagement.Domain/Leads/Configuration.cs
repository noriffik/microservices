﻿using NexCore.Domain;
using NexCore.LeadManagement.Domain.Leads.InterestIn;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("NexCore.LeadManagement.UnitTests")]

namespace NexCore.LeadManagement.Domain.Leads
{
    public class Configuration : Entity, IInterestIn<int>
    {
        public int? Interest { get; set; }

        private Configuration()
        {
        }

        internal Configuration(int id) : base(id)
        {
        }
    }
}
