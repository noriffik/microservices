﻿using NexCore.Domain;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.LeadManagement.Domain.Leads.Specifications
{
    public class LeadIncludedTestDriveSpecification : Specification<Lead>
    {
        public int TestDriveId { get; }

        public LeadIncludedTestDriveSpecification(int testDriveId)
        {
            TestDriveId = testDriveId;
        }

        public override Expression<Func<Lead, bool>> ToExpression()
        {
            return lead => lead.TestDrivesInternal.Select(c => c.Id).Contains(TestDriveId);
        }

        public override string Description => $"Lead including TestDrive with id {TestDriveId}";
    }
}
