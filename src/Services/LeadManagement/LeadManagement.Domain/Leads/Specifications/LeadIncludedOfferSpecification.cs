﻿using NexCore.Domain;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.LeadManagement.Domain.Leads.Specifications
{
    public class LeadIncludedOfferSpecification : Specification<Lead>
    {
        public int OfferId { get; }

        public LeadIncludedOfferSpecification(int offerId)
        {
            OfferId = offerId;
        }

        public override Expression<Func<Lead, bool>> ToExpression()
        {
            return lead => lead.OffersInternal.Select(c => c.Id).Contains(OfferId);
        }

        public override string Description => $"Lead including Offer with id {OfferId}";
    }
}
