﻿using NexCore.Domain;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace NexCore.LeadManagement.Domain.Leads.Specifications
{
    public class LeadIncludedConfigurationSpecification : Specification<Lead>
    {
        public int ConfigurationId { get; }

        public LeadIncludedConfigurationSpecification(int configurationId)
        {
            ConfigurationId = configurationId;
        }

        public override Expression<Func<Lead, bool>> ToExpression()
        {
            return lead => lead.ConfigurationsInternal.Select(c => c.Id).Contains(ConfigurationId);
        }

        public override string Description => $"Lead including Configuration with id {ConfigurationId}";
    }
}
