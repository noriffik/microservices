﻿using NexCore.Domain;
using System;

namespace NexCore.LeadManagement.Domain.Leads
{
    public class DuplicateConfigurationException : DomainException
    {
        private const string DefaultMessage = "Configuration for given id already exist.";
        private const string DefaultExtendedMessage = "Configuration for id {0} already exist.";

        public int ConfigurationId { get; }

        public DuplicateConfigurationException() : base(DefaultMessage)
        {
        }

        public DuplicateConfigurationException(string message) : base(message)
        {
        }

        public DuplicateConfigurationException(string message, Exception inner) : base(message, inner)
        {
        }

        public DuplicateConfigurationException(int configurationId)
            : this(string.Format(DefaultExtendedMessage, configurationId), configurationId)
        {
        }

        public DuplicateConfigurationException(string message, int configurationId)
            : this(message, configurationId, null)
        {
        }

        public DuplicateConfigurationException(string message, int configurationId, Exception inner)
            : base(message, inner)
        {
            ConfigurationId = configurationId;
        }
    }
}
