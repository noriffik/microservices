﻿namespace NexCore.LeadManagement.Domain.Leads.InterestIn
{
    public interface IInterestIn<TId>
    {
        TId Id { get; }

        int? Interest { get; set; }
    }
}
