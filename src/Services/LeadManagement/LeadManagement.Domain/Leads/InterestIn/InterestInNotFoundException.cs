﻿using System;

namespace NexCore.LeadManagement.Domain.Leads.InterestIn
{
    public class InterestInNotFoundException : InterestInListException
    {
        private const string DefaultMessage = "InterestIn with given id not found.";
        private const string DefaultExtendedMessage = "{0} with id {1} not found.";

        public object Id { get; }

        public InterestInNotFoundException() : base(DefaultMessage)
        {
        }

        public InterestInNotFoundException(string message) : base(message)
        {
        }

        public InterestInNotFoundException(string message, Exception inner) : base(message, inner)
        {
        }

        public InterestInNotFoundException(Type interestInType, object id)
            : this(string.Format(DefaultExtendedMessage, interestInType.Name, id), interestInType, id)
        {
        }

        public InterestInNotFoundException(string message, Type interestInType, object id)
            : this(message, interestInType, id, null)
        {
        }

        public InterestInNotFoundException(string message, Type interestInType, object id, Exception inner)
            : base(message, interestInType, inner)
        {
            Id = id;
        }
    }
}
