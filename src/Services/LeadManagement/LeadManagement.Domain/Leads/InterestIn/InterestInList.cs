﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.LeadManagement.Domain.Leads.InterestIn
{
    public class InterestInList<T, TId> : List<T>
    where T : class, IInterestIn<TId>
    {
        public void SetInterest(TId configurationId, int interest)
        {
            if (configurationId == null)
                throw new ArgumentNullException(nameof(configurationId));
            if (interest <= 0)
                throw new ArgumentOutOfRangeException(nameof(interest));

            var targetInterestIn = Find(e => Equals(e.Id, configurationId));
            if (targetInterestIn == null)
                throw new InterestInNotFoundException(typeof(T), configurationId);

            //We can only swap interests, when targetItem has value.
            //We can swap interests or put at last position, when targetItem has NOT value.
            var interestMaxValue = this.Count(c => c.Interest.HasValue) + (targetInterestIn.Interest.HasValue ? 0 : 1);
            if (interest > interestMaxValue)
                throw new InterestOutOfCountException(typeof(T), interest, interestMaxValue);

            if (!targetInterestIn.Interest.HasValue)//Down one position all interests from newInterest
                foreach (var interestIn in this.Where(c => c.Interest >= interest))
                    interestIn.Interest++;
            else if (targetInterestIn.Interest < interest) //Up one position all interests from oldInterest to newInterest
                foreach (var interestIn in this.Where(c => c.Interest > targetInterestIn.Interest && c.Interest <= interest))
                    interestIn.Interest--;
            else  //Down one position all interests from newInterest to oldInterest
                foreach (var interestIn in this.Where(c => c.Interest >= interest && c.Interest < targetInterestIn.Interest))
                    interestIn.Interest++;

            targetInterestIn.Interest = interest;
        }

        public T Find(TId id) => Find(o => Equals(o.Id, id));
    }
}
