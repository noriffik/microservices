﻿using System;

namespace NexCore.LeadManagement.Domain.Leads.InterestIn
{
    public class InterestOutOfCountException : InterestInListException
    {
        private const string DefaultMessage = "Given interest value is greater than last allowed value in list.";
        private const string DefaultExtendedMessage = "Interest value {0} of {1} can not be greater than {2}.";

        public int Interest { get; }

        public int MaxValue { get; }

        public InterestOutOfCountException() : base(DefaultMessage)
        {
        }

        public InterestOutOfCountException(string message) : base(message)
        {
        }

        public InterestOutOfCountException(string message, Exception inner) : base(message, inner)
        {
        }

        public InterestOutOfCountException(Type interestInType, int interest, int maxValue)
        : this(string.Format(DefaultExtendedMessage, interest, interestInType.Name, maxValue), interestInType, interest, maxValue)
        {
        }

        public InterestOutOfCountException(string message, Type interestInType, int interest, int maxValue)
            : this(message, interestInType, interest, maxValue, null)
        {
        }

        public InterestOutOfCountException(string message, Type interestInType, int interest, int maxValue, Exception inner)
            : base(message, interestInType, inner)
        {
            Interest = interest;
            MaxValue = maxValue;
        }
    }
}
