﻿using NexCore.Domain;
using System;

namespace NexCore.LeadManagement.Domain.Leads.InterestIn
{
    public abstract class InterestInListException : DomainException
    {
        public Type InterestInType { get; }

        protected InterestInListException() : base()
        {
        }

        protected InterestInListException(string message) : base(message)
        {
        }

        protected InterestInListException(string message, Exception inner) : base(message, inner)
        {
        }

        protected InterestInListException(string message, Type interestInType)
            : this(message, interestInType, null)
        {
        }

        protected InterestInListException(string message, Type interestInType, Exception inner)
            : base(message, inner)
        {
            InterestInType = interestInType;
        }
    }
}
