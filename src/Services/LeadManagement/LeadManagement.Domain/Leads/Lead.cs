﻿using NexCore.Common;
using NexCore.Domain;
using NexCore.LeadManagement.Domain.Leads.InterestIn;
using NexCore.LeadManagement.Domain.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace NexCore.LeadManagement.Domain.Leads
{
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Local", Justification = "Required by EF Core.")]
    [SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Local", Justification = "Required by EF Core.")]
    public class Lead : Entity, IAggregateRoot
    {
        private List<ModelId> _modelsInterestedIn;
        internal InterestInList<Offer, int> OffersInternal;
        internal InterestInList<Configuration, int> ConfigurationsInternal;
        internal List<TestDrive> TestDrivesInternal;

        public IEnumerable<ModelId> ModelsInterestedIn
        {
            get => _modelsInterestedIn;
            private set => _modelsInterestedIn = value.ToList();
        }

        public IEnumerable<Offer> Offers => OffersInternal.AsReadOnly();

        public IEnumerable<Configuration> Configurations => ConfigurationsInternal.AsReadOnly();

        public IEnumerable<TestDrive> TestDrives => TestDrivesInternal.AsReadOnly();

        public int PersonId { get; private set; }

        public int? PrivateCustomerId { get; private set; }

        public LeadStatus Status { get; private set; }

        public DateTime CreatedAt { get; private set; }

        public DateTime UpdatedAt { get; private set; }

        public int? MainOfferId { get; private set; }

        public Offer MainOffer => MainOfferId.HasValue ? FindOffer(MainOfferId.Value) : null;
        
        public VehicleReservation Reservation { get; private set; }

        private Lead()
        {
            Status = LeadStatus.Open;
            CreatedAt = SystemTimeProvider.Instance.Get();
            UpdatedAt = CreatedAt;
            _modelsInterestedIn = new List<ModelId>();
            ConfigurationsInternal = new InterestInList<Configuration, int>();
            OffersInternal = new InterestInList<Offer, int>();
            TestDrivesInternal = new List<TestDrive>();
        }

        public Lead(int personId, int? privateCustomerId) : this()
        {
            PrivateCustomerId = privateCustomerId;
            PersonId = personId;
        }

        public Configuration FindConfiguration(int configurationId) =>
            ConfigurationsInternal.SingleOrDefault(c => c.Id == configurationId);

        public Offer FindOffer(int offerId) => OffersInternal.SingleOrDefault(c => c.Id == offerId);

        public void RecordModelInterest(IEnumerable<ModelId> modelIds)
        {
            ModelsInterestedIn = modelIds ?? throw new ArgumentNullException(nameof(modelIds));
            UpdatedAt = SystemTimeProvider.Instance.Get();
        }

        public void RecordOffer(int offerId)
        {
            if (OffersInternal.Select(c => c.Id).Contains(offerId))
                throw new DuplicateOfferException(
                    $"Offer with id {offerId} already exist for Lead {Id}", offerId);

            OffersInternal.Add(new Offer(offerId));
            UpdatedAt = SystemTimeProvider.Instance.Get();
        }

        public void RecordConfiguration(int configurationId)
        {
            if (ConfigurationsInternal.Select(c => c.Id).Contains(configurationId))
                throw new DuplicateConfigurationException(
                    $"Configuration with id {configurationId} already exist for Lead {Id}", configurationId);

            ConfigurationsInternal.Add(new Configuration(configurationId));
            UpdatedAt = SystemTimeProvider.Instance.Get();
        }

        public void RecordTestDrive(int testDriveId)
        {
            if (TestDrivesInternal.Select(c => c.Id).Contains(testDriveId))
                throw new DuplicateTestDriveException(
                    $"TestDrive with id {testDriveId} already exist for Lead {Id}", testDriveId);

            TestDrivesInternal.Add(new TestDrive(testDriveId));
            UpdatedAt = SystemTimeProvider.Instance.Get();
        }

        public void SetConfigurationInterest(int configurationId, int interest)
        {
            ConfigurationsInternal.SetInterest(configurationId, interest);
        }

        public void SetOfferInterest(int offerId, int interest)
        {
            OffersInternal.SetInterest(offerId, interest);
        }

        public void SetMainOffer(int? offerId)
        {
            if (offerId.HasValue && !OffersInternal.Select(o => o.Id).Contains(offerId.Value))
                throw new OfferNotFoundException(
                    $"Offer with id {offerId} is not found in Lead {Id}", offerId.Value);

            MainOfferId = offerId;
        }

        public void RecordVehicleReservation(string commercialNumber, string vin, DateTime onDate)
        {
            Reservation = new VehicleReservation(commercialNumber, vin, onDate);
        }
    }

    public enum LeadStatus
    {
        Open = default(int),
        Closed = 1
    }
}
