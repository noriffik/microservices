﻿using NexCore.Domain;
using NexCore.LeadManagement.Domain.Leads.InterestIn;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("NexCore.LeadManagement.UnitTests")]

namespace NexCore.LeadManagement.Domain.Leads
{
    public class Offer : Entity, IInterestIn<int>
    {
        public int? Interest { get; set; }

        private Offer()
        {
        }

        internal Offer(int id) : base(id)
        {
        }
    }
}
