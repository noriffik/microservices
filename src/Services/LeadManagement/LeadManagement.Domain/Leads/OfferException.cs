﻿using NexCore.Domain;
using System;

namespace NexCore.LeadManagement.Domain.Leads
{
    public abstract class OfferException : DomainException
    {
        public int OfferId { get; }

        protected OfferException() : base()
        {
        }

        protected OfferException(string message) : base(message)
        {
        }

        protected OfferException(string message, Exception inner) : base(message, inner)
        {
        }

        protected OfferException(string message, int offerId)
            : this(message, offerId, null)
        {
        }

        protected OfferException(string message, int offerId, Exception inner)
            : base(message, inner)
        {
            OfferId = offerId;
        }

    }
}
