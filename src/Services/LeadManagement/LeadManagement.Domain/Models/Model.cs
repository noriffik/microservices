﻿using NexCore.Domain;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.LeadManagement.Domain.Models
{
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    public class Model : Entity<ModelId>, IAggregateRoot<ModelId>
    {
        public string Name { get; set; }

        private Model()
        {
        }

        public Model(ModelId id, string name) : base(id)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }
    }
}
