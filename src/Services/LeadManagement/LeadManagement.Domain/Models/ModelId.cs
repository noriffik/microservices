﻿using NexCore.Domain;
using System;
using System.Diagnostics.CodeAnalysis;

namespace NexCore.LeadManagement.Domain.Models
{
    [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core")]
    public class ModelId : EntityId
    {
        private ModelId()
        {
        }

        private ModelId(string value) : base(value)
        {
        }

        public static ModelId Parse(string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (!TryParse(value, out var modelId))
                throw new FormatException($@"""{value}"" is invalid value for modelId");

            return modelId;
        }

        public static bool TryParse(string value, out ModelId modelId)
        {
            modelId = string.IsNullOrWhiteSpace(value) ? null : new ModelId(value);

            return modelId != null;
        }

        public static implicit operator string(ModelId id)
        {
            return id?.Value;
        }

        public static implicit operator ModelId(string value)
        {
            return value == null ? null : Parse(value);
        }
    }
}
