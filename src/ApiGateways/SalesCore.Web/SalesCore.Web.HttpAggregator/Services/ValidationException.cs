﻿using System;

namespace NexCore.SalesCore.Web.HttpAggregator.Services
{
    public class ValidationException : Exception
    {
        public ValidationException()
        {
        }

        public ValidationException(string message)
            : base(message)
        {
        }

        public ValidationException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public object Errors { get; set; }
    }
}
