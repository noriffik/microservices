﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.HealthChecks;
using NexCore.SalesCore.Web.HttpAggregator.Infrastructure.HttpHandlers;
using NexCore.SalesCore.Web.HttpAggregator.Services.Contact;
using NexCore.WebApi.Extensions;
using Polly;
using Polly.Extensions.Http;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace NexCore.SalesCore.Web.HttpAggregator.Extensions
{
    static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddHealthChecks(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddHealthChecks(checks =>
            {
                var contactServiceUrl = configuration["Services:Contact:BaseUrl"].TrimEnd('/') + "/hc";

                checks.AddUrlCheck(contactServiceUrl);
            });

            return services;
        }

        public static IServiceCollection AddSwagger(this IServiceCollection services,
            IConfiguration configuration)
        {
            return services.AddSwaggerGen(
                configuration,
                "The SalesCore.Web HTTP Aggregator API",
                "v1",
                new Dictionary<string, string>
                {
                    { "NexCore.SalesCore.Web.HttpAggregator", "The SalesCore.Web HTTP Aggregator API" },
                    { "NexCore.ContactManager", "Управление контактами" },
                    { "NexCore.VehicleConfigurator", "Конфигуратор автомобилей" },
                    { "NexCore.VehicleStock", "Склад автомобилей" }
                });
        }

        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<HttpClientAuthorizationHandler>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddHttpClient<IContactService, HttpContactService>()
                .AddHttpMessageHandler<HttpClientAuthorizationHandler>()
                .AddPolicyHandler(GetRetryPolicy())
                .AddPolicyHandler(GetCircuitBreakerPolicy());

            services.AddTransient<IContactServiceUrlProvider>(s =>
                new ContactServiceUrlProvider(
                    new Uri(configuration["Services:Contact:BaseUrl"]),
                    new Uri(configuration["Services:ClientSelfService:BaseUrl"])));

            return services;
        }

        private static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .WaitAndRetryAsync(6, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));
        }

        private static IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .CircuitBreakerAsync(5, TimeSpan.FromSeconds(30));
        }
    }
}
