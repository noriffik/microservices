﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexCore.SalesCore.Web.HttpAggregator.Extensions;
using NexCore.SalesCore.Web.HttpAggregator.Infrastructure.Filters;
using NexCore.WebApi.Extensions;

namespace NexCore.SalesCore.Web.HttpAggregator
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddServices(Configuration)
                .AddHealthChecks(Configuration)
                .AddSwagger(Configuration)
                .AddMvc(options =>
                {
                    options.Filters.Add<ExceptionFilter>();
                });

            ConfigureAuthentication(services);
        }

        // This method is required by testing to change authentication options
        protected virtual void ConfigureAuthentication(IServiceCollection services)
        {
            services.AddJwtBearerAuthentication(
                Configuration, "NexCore.SalesCore.Web.HttpAggregator");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");

                c.OAuthClientId("NexCore.SalesCore.Web.HttpAggregator");
            });

            app.UseMvc();
        }
    }
}
