﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace NexCore.OcelotApiGateway.Extensions
{
    static class AuthenticationBuilderExtensions
    {
        public static AuthenticationBuilder AddJwtBearer(this AuthenticationBuilder builder, IConfiguration configuration, IEnumerable<string> audiences)
        {
            var identityUrl = configuration.GetValue("IdentityUrl", "http://identity.api");
            var authenticationScheme = "IdentityApiKey";

            builder.AddJwtBearer(authenticationScheme, o =>
            {
                o.Authority = identityUrl;
                o.RequireHttpsMetadata = false;
                o.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidAudiences = audiences
                };
            });

            return builder;
        }
    }
}
