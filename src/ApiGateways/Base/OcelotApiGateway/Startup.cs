﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexCore.OcelotApiGateway.Extensions;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using System.Linq;

namespace NexCore.OcelotApiGateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.WithOrigins(GetCorsOrigins())
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });
            
            services.AddAuthentication().AddJwtBearer(Configuration, new[]
            {
                "NexCore.SalesCore.Web.HttpAggregator",
                "NexCore.ContactManager",
                "NexCore.VehicleConfigurator"
            });

            services.AddOcelot(Configuration);
        }

        private string[] GetCorsOrigins()
        {
            var origins = Configuration.GetValue("CorsOrigins", string.Empty);

            return origins.Split(',')
                .Select(origin => origin.Trim())
                .ToArray();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("CorsPolicy");

            app.UseOcelot().Wait();
        }
    }
}
