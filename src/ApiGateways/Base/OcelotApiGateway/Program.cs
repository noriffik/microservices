﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace NexCore.OcelotApiGateway
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = CreateWebHostBuilder(args);
                
            builder.Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            var builder = WebHost.CreateDefaultBuilder(args);

            builder
                .ConfigureAppConfiguration(c => c.AddJsonFile(Path.Combine("configuration", "ocelot.json")))
                .UseStartup<Startup>();

            return builder;
        }
    }
}
