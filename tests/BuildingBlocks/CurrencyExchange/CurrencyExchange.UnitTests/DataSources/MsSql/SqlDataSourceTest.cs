﻿using AutoFixture;
using Microsoft.EntityFrameworkCore;
using NexCore.CurrencyExchange.Abstract;
using NexCore.CurrencyExchange.DataSources.MsSql;
using NexCore.Testing.Extensions;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.CurrencyExchange.UnitTests.DataSources.MsSql
{
    public class SqlDataSourceTest : IDisposable
    {
        //Dependencies
        private readonly DbContextOptions<CurrencyExchangeContext> _options;

        //Entity
        private readonly Rate _rate;

        //DataSource
        private readonly SqlDataSource _dataSource;

        public SqlDataSourceTest()
        {
            //Dependencies
            _options = new DbContextOptionsBuilder<CurrencyExchangeContext>()
                .UseInMemoryDatabase(nameof(SqlDataSourceTest))
                .Options;

            //Entity
            var fixture = new Fixture();
            _rate = fixture.Construct<Rate>(new { from = Currency.Usd, to = Currency.Uah });

            //DataSource
            _dataSource = new SqlDataSource(new CurrencyExchangeContext(_options));
        }

        [Fact]
        public async Task TryGet()
        {
            //Arrange
            using (var context = new CurrencyExchangeContext(_options))
            {
                await context.Rates.AddAsync(_rate);
                await context.SaveChangesAsync();
            }

            //Act
            var rate = await _dataSource.TryGet(_rate.From, _rate.To, _rate.Date);

            //Assert
            Assert.Equal(_rate.Value, rate);
        }

        [Fact]
        public async Task TryGet_WhenRate_IsNotFound_ReturnsNull()
        {
            //Act
            var rate = await _dataSource.TryGet(_rate.From, _rate.To, _rate.Date);

            //Assert
            Assert.Null(rate);
        }

        [Fact]
        public void Cache()
        {
            //Arrange
            var cache = _dataSource.GetType().GetMethod("Cache", BindingFlags.NonPublic | BindingFlags.Instance);
            var parameters = new object[] { _rate.From, _rate.To, _rate.Date, _rate.Value };

            //Act
            cache.Invoke(_dataSource, parameters);

            //Assert
            using (var context = new CurrencyExchangeContext(_options))
            {
                Assert.Contains(_rate, context.Rates, new RatePropertiesComparer());
            }
        }

        private class RatePropertiesComparer : IEqualityComparer<Rate>
        {
            public bool Equals(Rate x, Rate y) =>
                x.From == y.From && x.To == y.To && x.Value == y.Value && x.Date == y.Date;

            public int GetHashCode(Rate obj) => throw new NotSupportedException();
        }

        public void Dispose()
        {
            using (var context = new CurrencyExchangeContext(_options))
            {
                context.Database.EnsureDeleted();
                context.SaveChanges();
            }
        }
    }
}
