﻿using AutoFixture;
using NexCore.CurrencyExchange.Abstract;
using NexCore.CurrencyExchange.DataSources.MsSql.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.CurrencyExchange.UnitTests.DataSources.MsSql.Specifications
{
    public class FindSpecificationTest
    {
        private readonly Currency _from;
        private readonly Currency _to;
        private readonly DateTime _date;

        //Entities
        private readonly IReadOnlyList<Rate> _expected;
        private readonly IReadOnlyList<Rate> _unexpected;
        private readonly IReadOnlyList<Rate> _all;

        //Specification
        private readonly FindSpecification _specification;

        public FindSpecificationTest()
        {
            var fixture = new Fixture();

            _from = Currency.Usd;
            _to = Currency.Uah;
            _date = fixture.Create<DateTime>();

            _expected = new[] { new Rate(_from, _to, fixture.Create<decimal>(), _date) };
            _unexpected = new[]
            {
                new Rate(_from, _to, fixture.Create<decimal>(), fixture.Create<DateTime>()),
                new Rate(_from, Currency.Eur, fixture.Create<decimal>(), _date),
                new Rate(Currency.Eur, _to, fixture.Create<decimal>(), _date),
                new Rate(Currency.Eur, _to, fixture.Create<decimal>(), fixture.Create<DateTime>()),
                new Rate(_from, Currency.Eur, fixture.Create<decimal>(), fixture.Create<DateTime>())
            };
            _all = _unexpected.Concat(_expected).ToList();

            _specification = new FindSpecification(_from, _to, _date);
        }

        [Fact]
        public void Ctor()
        {
            //Assert
            Assert.Equal(_from, _specification.From);
            Assert.Equal(_to, _specification.To);
            Assert.Equal(_date.Date, _specification.Date);
        }

        [Fact]
        public void Ctor_WhenGivenFrom_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("from", () => new FindSpecification(null, _to, _date));
        }

        [Fact]
        public void Ctor_WhenGivenTo_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("to", () => new FindSpecification(_from, null, _date));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
