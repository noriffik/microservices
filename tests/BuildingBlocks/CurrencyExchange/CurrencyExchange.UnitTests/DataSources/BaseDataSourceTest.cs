﻿using AutoFixture;
using Moq;
using Moq.Protected;
using Moq.Sequences;
using NexCore.CurrencyExchange.Abstract;
using NexCore.CurrencyExchange.DataSources;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.CurrencyExchange.UnitTests.DataSources
{
    public class BaseDataSourceTest
    {
        //Dependency
        private readonly Mock<ICurrencyRateDataSource> _inner;

        //Entities
        private readonly DateTime _date;
        private readonly Currency _from;
        private readonly Currency _to;
        private readonly decimal _rate;

        //DataSource
        private readonly Mock<BaseDataSource> _dataSource;

        public BaseDataSourceTest()
        {
            var fixture = new Fixture();

            //Dependency
            _inner = new Mock<ICurrencyRateDataSource>();

            //Entities
            _date = fixture.Create<DateTime>();
            _from = Currency.Usd;
            _to = Currency.Uah;
            _rate = fixture.Create<decimal>();

            //DataSource
            _dataSource = new Mock<BaseDataSource>(_inner.Object)
            {
                CallBase = true
            };
        }

        [Fact]
        public Task TryGet_GivenFrom_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("from", () => _dataSource.Object.TryGet(null, _to, _date));
        }

        [Fact]
        public Task TryGet_GivenTo_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("to", () => _dataSource.Object.TryGet(_from, null, _date));
        }

        [Fact]
        public async Task TryGet_GivenFromAndTo_IsSame_ReturnsOne()
        {
            //Act
            var rate = await _dataSource.Object.TryGet(_from, _from, _date);

            //Assert
            Assert.Equal(1, rate);
            _dataSource.Protected().Verify("TryGetLocally", Times.Never(), _from, _from, _date);
        }

        [Fact]
        public async Task TryGet_WhenTryGetLocally_IsSuccess_ReturnsRate()
        {
            //Arrange
            _dataSource.Protected().Setup<Task<decimal?>>("TryGetLocally", _from, _to, _date)
                .ReturnsAsync(_rate)
                .Verifiable();

            //Act
            var rate = await _dataSource.Object.TryGet(_from, _to, _date);

            //Assert
            Assert.Equal(_rate, rate);
            _dataSource.Verify();
        }

        [Fact]
        public async Task TryGet_WhenTryGetLocally_IsNotSuccess_And_Inner_IsNull_ReturnsNull()
        {
            //Arrange
            var dataSource = new Mock<BaseDataSource>(null)
            {
                CallBase = true
            };

            //Act
            var rate = await dataSource.Object.TryGet(_from, _to, _date);

            //Assert
            Assert.Null(rate);
        }

        [Fact]
        public async Task TryGet_WhenTryGetLocally_IsNotSuccess_And_Inner_IsSuccess_ReturnsRate()
        {
            using (Sequence.Create())
            {
                //Arrange
                _dataSource.Protected().Setup<Task<decimal?>>("TryGetLocally", _from, _to, _date)
                    .InSequence()
                    .ReturnsAsync((decimal?)null);
                _inner.Setup(s => s.TryGet(_from, _to, _date))
                    .InSequence()
                    .ReturnsAsync(_rate);
                _dataSource.Protected().Setup<Task>("Cache", _from, _to, _date, _rate)
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var rate = await _dataSource.Object.TryGet(_from, _to, _date);

                //Assert
                Assert.Equal(_rate, rate);
            }
        }

        [Fact]
        public async Task TryGet_WhenTryGetLocally_And_Inner_IsNotSuccess_ReturnsNull()
        {
            //Act
            var rate = await _dataSource.Object.TryGet(_from, _to, _date);

            //Assert
            Assert.Null(rate);
        }

        [Fact]
        public Task Get_GivenFrom_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("from", () => _dataSource.Object.Get(null, _to, _date));
        }

        [Fact]
        public Task Get_GivenTo_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("to", () => _dataSource.Object.Get(_from, null, _date));
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            _dataSource.Setup(s => s.TryGet(_from, _to, _date))
                .ReturnsAsync(_rate)
                .Verifiable();

            //Act
            var rate = await _dataSource.Object.Get(_from, _to, _date);

            //Assert
            Assert.Equal(_rate, rate);
            _dataSource.Verify();
        }

        [Fact]
        public async Task Get_WhenRate_IsNotFound_Throws()
        {
            //Act
            var e = await Assert.ThrowsAsync<CurrencyExchangeException>(() =>
                _dataSource.Object.Get(_from, _to, _date));

            //Assert
            Assert.Contains("failed to get currency", e.Message.ToLower());
        }
    }
}
