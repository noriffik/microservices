﻿using AutoFixture;
using NexCore.CurrencyExchange.Abstract;
using NexCore.CurrencyExchange.DataSources.InMemory;
using System;
using System.Collections.Concurrent;
using System.Reflection;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.CurrencyExchange.UnitTests.DataSources.InMemory
{
    public class InMemoryDataSourceTest
    {
        private readonly DateTime _date;
        private readonly decimal _rate;
        private readonly Currency _from;
        private readonly Currency _to;

        public InMemoryDataSourceTest()
        {
            var fixture = new Fixture();

            _date = fixture.Create<DateTime>();
            _rate = fixture.Create<decimal>();
            _from = Currency.Usd;
            _to = Currency.Uah;
        }

        [Fact]
        public async Task TryGetRate()
        {
            //Arrange
            var dataSource = new InMemoryDataSource();
            var innerRates = GetInnerRates(dataSource);

            var tuple = (_from, _to, _date.Date);
            innerRates.AddOrUpdate(tuple, _rate, (key, oldValue) => _rate);

            //Act
            var rate = await dataSource.TryGet(_from, _to, _date);

            //Assert
            Assert.Equal(_rate, rate);
        }

        private static ConcurrentDictionary<(Currency, Currency, DateTime), decimal> GetInnerRates(InMemoryDataSource dataSource)
        {
            var innerRates = typeof(InMemoryDataSource)
                .GetField("_rates", BindingFlags.NonPublic | BindingFlags.Instance)
                .GetValue(dataSource);

            return (ConcurrentDictionary<(Currency, Currency, DateTime), decimal>)innerRates;
        }

        [Fact]
        public async Task TryGetRate_WhenRate_IsNotFound_ReturnsNull()
        {
            //Arrange
            var dataSource = new InMemoryDataSource();

            //Act
            var rate = await dataSource.TryGet(_from, _to, _date);

            //Assert
            Assert.Null(rate);
        }

        [Fact]
        public void Cache()
        {
            //Arrange
            var dataSource = new InMemoryDataSource();
            var innerRates = GetInnerRates(dataSource);

            var cache = dataSource.GetType().GetMethod("Cache", BindingFlags.NonPublic | BindingFlags.Instance);
            var parameters = new object[] { _from, _to, _date, _rate };

            //Act
            cache.Invoke(dataSource, parameters);

            //Assert
            Assert.True(innerRates.TryGetValue((_from, _to, _date.Date), out var rate));
            Assert.Equal(_rate, rate);
        }
    }
}
