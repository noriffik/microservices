﻿using NexCore.CurrencyExchange.Abstract;
using NexCore.CurrencyExchange.DataSources.Http.Nbu;
using System;
using System.Collections.Generic;
using Xunit;

namespace NexCore.CurrencyExchange.UnitTests.DataSources.Http.Nbu
{
    public class NbuUrlProviderTest
    {
        private const string PartialTemplate = "http://resource.api/";
        private const string Template = PartialTemplate + "?date=<![DATE]>&from=<![FROM]>&to=<![TO]>";

        public static IEnumerable<object[]> Values =>
            new List<object[]>
            {
                new object[] { Currency.Eur, Currency.Usd, new DateTime(2011, 03, 07) },
                new object[] { Currency.Usd, Currency.Eur, new DateTime(1999, 06, 19) },
                new object[] { Currency.Uah, Currency.Uah, new DateTime(1092, 11, 15) }
            };

        [Theory]
        [MemberData(nameof(Values))]
        public void Get(Currency from, Currency to, DateTime onDate)
        {
            //Arrange
            var expected = new Uri($"{PartialTemplate}?date={onDate:yyyyMMdd}&from={from.Code}&to={to.Code}");
            var provider = new NbuUrlProvider(Template);

            //Act
            var actual = provider.Get(from, to, onDate);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [MemberData(nameof(Values))]
        public void Get_WhenPlaceholder_IsNotFound_IgnoresValue(Currency from, Currency to, DateTime onDate)
        {
            //Arrange
            var expected = new Uri(PartialTemplate);
            var provider = new NbuUrlProvider(PartialTemplate);

            //Act
            var actual = provider.Get(from, to, onDate);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Get_GivenFrom_IsNull_Throws()
        {
            //Arrange
            var provider = new NbuUrlProvider(PartialTemplate);

            //Assert
            Assert.Throws<ArgumentNullException>("from", () => provider.Get(null, Currency.Eur, DateTime.MinValue));
        }

        [Fact]
        public void Get_GivenTo_IsNull_Throws()
        {
            //Arrange
            var provider = new NbuUrlProvider(PartialTemplate);

            //Assert
            Assert.Throws<ArgumentNullException>("to", () => provider.Get(Currency.Eur, null, DateTime.MinValue));
        }
    }
}
