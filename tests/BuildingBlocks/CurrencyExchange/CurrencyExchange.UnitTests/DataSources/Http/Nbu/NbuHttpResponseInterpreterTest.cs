﻿using Newtonsoft.Json;
using NexCore.CurrencyExchange.DataSources.Http.Nbu;
using Xunit;

namespace NexCore.CurrencyExchange.UnitTests.DataSources.Http.Nbu
{
    public class NbuHttpResponseInterpreterTest
    {
        private readonly NbuHttpResponseInterpreter _interpreter = new NbuHttpResponseInterpreter();

        [Fact]
        public void Interpret()
        {
            //Arrange
            var rate = 123;
            var response = JsonConvert.SerializeObject(new[] { new { rate, param2 = 555, param3 = "qwerty" } });

            //Act
            var result = _interpreter.Interpret(response);

            //Assert
            Assert.Equal(rate, result);
        }

        [Fact]
        public void Interpret_WhenResponse_IsInvalid_ReturnsNull()
        {
            //Act
            var result = _interpreter.Interpret("invalid");

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public void Interpret_WhenResponse_IsEmpty_ReturnsNull()
        {
            //Arrange
            var response = JsonConvert.SerializeObject(new object[] { });

            //Act
            var result = _interpreter.Interpret(response);

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public void Interpret_WhenResponse_DoesNotContains_RateValue_ReturnsNull()
        {
            //Arrange
            var response = JsonConvert.SerializeObject(new[] { new { param1 = 123, param2 = 555, param3 = "qwerty" } });

            //Act
            var result = _interpreter.Interpret(response);

            //Assert
            Assert.Null(result);
        }

    }
}
