﻿using Moq;
using NexCore.CurrencyExchange.Abstract;
using NexCore.CurrencyExchange.DataSources.Http.Abstract;
using NexCore.CurrencyExchange.DataSources.Http.Nbu;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.CurrencyExchange.UnitTests.DataSources.Http.Nbu
{
    public class NbuHttpDataSourceTest
    {
        [Fact]
        public void Ctor_GivenUrlProvider_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("urlProvider", () =>
                new NbuHttpDataSource(null, new Mock<IHttpClientFactory>().Object));
        }

        [Fact]
        public void Ctor_GivenHttpFactory_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("httpFactory", () =>
                new NbuHttpDataSource(new Mock<IUrlProvider>().Object, null));
        }

        [Fact]
        public Task TryGet_GivenTo_IsNotUah_Throws()
        {
            //Arrange
            var dataSource = new NbuHttpDataSource(new Mock<IUrlProvider>().Object, new Mock<IHttpClientFactory>().Object);

            //Assert
            return Assert.ThrowsAsync<ArgumentException>("to", () =>
                dataSource.TryGet(Currency.Usd, Currency.Eur, DateTime.MinValue));
        }
    }
}
