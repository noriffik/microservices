﻿using AutoFixture;
using Moq;
using NexCore.CurrencyExchange.Abstract;
using NexCore.CurrencyExchange.DataSources.Http;
using NexCore.CurrencyExchange.DataSources.Http.Abstract;
using NexCore.Testing.Extensions.Moq;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.CurrencyExchange.UnitTests.DataSources.Http
{
    public class HttpDataSourceTest
    {
        private readonly decimal _rate;
        private readonly DateTime _date;
        private readonly Uri _uri;
        private readonly Currency _from;
        private readonly Currency _to;

        //Dependencies
        private readonly Mock<HttpMessageHandler> _httpHandler;

        //DataSource
        private readonly Mock<BaseHttpDataSource> _dataSource;

        public HttpDataSourceTest()
        {
            var fixture = new Fixture();

            _date = fixture.Create<DateTime>();
            _uri = fixture.Create<Uri>();
            _rate = fixture.Create<decimal>();
            _from = Currency.Usd;
            _to = Currency.Uah;
            var rateContent = fixture.Create<string>();

            //Setup url provider
            var urlProvider = new Mock<IUrlProvider>();
            urlProvider.Setup(p => p.Get(_from, _to, _date))
                .Returns(_uri);

            //Setup http handler
            _httpHandler = new Mock<HttpMessageHandler>();
            _httpHandler.SetupSendAsync(_uri, HttpMethod.Get, HttpStatusCode.OK, rateContent);

            //Setup response interpreter
            var responseInterpreter = new Mock<IHttpResponseInterpreter>();
            responseInterpreter.Setup(i => i.Interpret(rateContent)).Returns(_rate);

            //Setup http client factory
            var httpClientFactory = new Mock<IHttpClientFactory>();
            httpClientFactory.Setup(f => f.GetHttpClient()).Returns(new HttpClient(_httpHandler.Object));

            //DataSource
            _dataSource = new Mock<BaseHttpDataSource>(
                urlProvider.Object, httpClientFactory.Object, responseInterpreter.Object, null)
            {
                CallBase = true
            };
        }

        [Fact]
        public async Task TryGet()
        {
            //Act
            var rate = await _dataSource.Object.TryGet(_from, _to, _date);

            //Assert
            Assert.Equal(_rate, rate);
        }

        [Fact]
        public async Task TryGet_WhenThrowsOnSend_AfterThirdRetry_ReturnsNull()
        {
            //Arrange
            _httpHandler.SetupThrowsOnSendAsync(_uri, HttpMethod.Get);

            //Act
            var rate = await _dataSource.Object.TryGet(_from, _to, _date);

            //Assert
            Assert.Null(rate);
            _httpHandler.Verify(Times.Exactly(4), _uri, HttpMethod.Get);
        }
    }
}
