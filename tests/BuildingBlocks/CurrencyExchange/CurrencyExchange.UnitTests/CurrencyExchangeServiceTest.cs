﻿using AutoFixture;
using Moq;
using NexCore.CurrencyExchange.Abstract;
using NexCore.Domain;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.CurrencyExchange.UnitTests
{
    public class CurrencyExchangeServiceTest
    {
        //Dependencies
        private readonly Mock<ICurrencyRateDataSource> _rateDataSource;

        //Entities
        private readonly DateTime _date;
        private readonly decimal _price;
        private readonly decimal _rate;
        private readonly Currency _from;
        private readonly Currency _to;

        //Service
        private readonly CurrencyExchangeService _service;

        public CurrencyExchangeServiceTest()
        {
            var fixture = new Fixture();

            //Entities
            _date = fixture.Create<DateTime>();
            _price = fixture.Create<decimal>();
            _rate = fixture.Create<decimal>();
            _from = Currency.Usd;
            _to = Currency.Uah;

            //Dependencies
            _rateDataSource = new Mock<ICurrencyRateDataSource>();
            _rateDataSource.Setup(s => s.Get(_from, _to, _date))
                .ReturnsAsync(_rate)
                .Verifiable();

            //Service
            _service = new CurrencyExchangeService(_rateDataSource.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(CurrencyExchangeService)).HasNullGuard();
        }

        [Fact]
        public Task Convert_WhenGivenFrom_IsNull()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("from", () =>
                _service.Convert(null, Currency.Eur, 123));

        }

        [Fact]
        public Task Convert_WhenGivenTo_IsNull()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("to", () =>
                _service.Convert(Currency.Usd, null, 123));
        }

        [Fact]
        public async Task Convert()
        {
            //Arrange
            using (new SystemTimeContext(_date))
            {
                //Act
                var result = await _service.Convert(_from, _to, _price);

                //Assert
                Assert.Equal(_price * _rate, result);
                _rateDataSource.Verify();
            }
        }

        [Fact]
        public Task Convert_WithDate_WhenGivenFrom_IsNull()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("from", () =>
                _service.Convert(null, Currency.Eur, 123, _date));
        }

        [Fact]
        public Task Convert_WithDate_WhenGivenTo_IsNull()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("to", () =>
                _service.Convert(Currency.Usd, null, 123, _date));
        }

        [Fact]
        public async Task Convert_WithDate()
        {
            //Act
            var result = await _service.Convert(_from, _to, _price, _date);

            //Assert
            Assert.Equal(_price * _rate, result);
            _rateDataSource.Verify();
        }

        [Fact]
        public Task GetRate_WhenGivenFrom_IsNull()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("from", () =>
                _service.GetRate(null, Currency.Eur));
        }

        [Fact]
        public Task GetRate_WhenGivenTo_IsNull()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("to", () =>
                _service.GetRate(Currency.Usd, null));
        }

        [Fact]
        public async Task GetRate()
        {
            //Arrange
            using (new SystemTimeContext(_date))
            {
                //Act
                var result = await _service.GetRate(_from, _to);

                //Assert
                Assert.Equal(_rate, result);
                _rateDataSource.Verify();
            }
        }

        [Fact]
        public Task GetRate_WithDate_WhenGivenFrom_IsNull()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("from", () =>
                _service.GetRate(null, Currency.Eur, _date));
        }

        [Fact]
        public Task GetRate_WithDate_WhenGivenTo_IsNull()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("to", () =>
                _service.GetRate(Currency.Usd, null, _date));
        }

        [Fact]
        public async Task GetRate_WithDate()
        {
            //Act
            var result = await _service.GetRate(_from, _to, _date);

            //Assert
            Assert.Equal(_rate, result);
            _rateDataSource.Verify();
        }
    }
}
