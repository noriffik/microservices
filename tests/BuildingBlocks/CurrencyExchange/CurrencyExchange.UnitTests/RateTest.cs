﻿using AutoFixture;
using NexCore.CurrencyExchange.Abstract;
using System;
using Xunit;

namespace NexCore.CurrencyExchange.UnitTests
{
    public class RateTest
    {
        //Entities
        private readonly int _id;
        private readonly Currency _from;
        private readonly Currency _to;
        private readonly decimal _value;
        private readonly DateTime _date;

        public RateTest()
        {
            var fixture = new Fixture();

            _id = fixture.Create<int>();
            _from = Currency.Eur;
            _to = Currency.Usd;
            _value = fixture.Create<decimal>();
            _date = fixture.Create<DateTime>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var rate = new Rate(_from, _to, _value, _date);

            //Assert
            Assert.Equal(default(int), rate.Id);
            Assert.Equal(_from, rate.From);
            Assert.Equal(_to, rate.To);
            Assert.Equal(_value, rate.Value);
            Assert.Equal(_date.Date, rate.Date);
        }

        [Fact]
        public void Ctor_WithId()
        {
            //Act
            var rate = new Rate(_id, _from, _to, _value, _date);

            //Assert
            Assert.Equal(_id, rate.Id);
            Assert.Equal(_from, rate.From);
            Assert.Equal(_to, rate.To);
            Assert.Equal(_value, rate.Value);
            Assert.Equal(_date.Date, rate.Date);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void Ctor_GivenValue_IsInvalid_Throws(decimal value)
        {
            Assert.Throws<ArgumentOutOfRangeException>("value", () => new Rate(_from, _to, value, _date));
        }
    }
}
