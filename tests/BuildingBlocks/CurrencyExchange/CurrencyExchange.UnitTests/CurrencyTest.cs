﻿using NexCore.CurrencyExchange.Abstract;
using System;
using Xunit;

namespace NexCore.CurrencyExchange.UnitTests
{
    public class CurrencyTest
    {
        [Theory]
        [InlineData("UAH")]
		[InlineData("uAh")]
        [InlineData("USD")]
		[InlineData("usD")]
        [InlineData("EUR")]
        public void Parse(string code)
        {
            //Act
            var currency = Currency.Parse(code);

            //Assert
            Assert.NotNull(currency);
            Assert.Equal(code.ToUpperInvariant(), currency.Code);
        }

        [Fact]
        public void Parse_GivenCode_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                Currency.Parse(null);
            });
            Assert.Equal("code", e.ParamName);
        }

        [Theory]
        [InlineData("AAA")]
        [InlineData("/DD")]
        [InlineData("XA11")]
        [InlineData("XC")]
        public void Parse_GivenCode_IsInvalid_Throws(string code)
        {
            Assert.Throws<FormatException>(() =>
            {
                Currency.Parse(code);
            });
        }

        [Theory]
        [InlineData("UAH")]
		[InlineData("uAh")]
        [InlineData("USD")]
		[InlineData("usD")]
        [InlineData("EUR")]
        public void TryParse(string code)
        {
            //Act
            var result = Currency.TryParse(code, out var currency);

            //Assert
            Assert.True(result);
            Assert.NotNull(currency);
            Assert.Equal(code.ToUpperInvariant(), currency.Code);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("AAA")]
        [InlineData("/DD")]
        [InlineData("XA11")]
        [InlineData("XC")]
        public void TryParse_GivenCode_IsNullOrInvalid_ReturnsFalse(string code)
        {
            //Act
            var result = Currency.TryParse(code, out var currency);

            //Assert
            Assert.False(result);
            Assert.Null(currency);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var currency = Currency.Parse("UAH");

            //Assert
            Assert.Equal(currency.Code, currency.ToString());
        }
    }
}
