﻿using Moq;
using NexCore.CurrencyExchange.Abstract;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.CurrencyExchange.UnitTests
{
    public class StubbedCurrencyExchangeServiceTest
    {
        private readonly StubbedCurrencyExchangeService _service;

        public StubbedCurrencyExchangeServiceTest()
        {
            _service = new StubbedCurrencyExchangeService();
        }

        [Fact]
        public async Task Convert()
        {
            //Arrange
            const decimal expected = 28;

            //Act
            var result = await _service.Convert(Currency.Usd, Currency.Uah, 1, It.IsAny<DateTime>());

            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public async Task Convert_GivenFrom_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("from",
                () => _service.Convert(null, Currency.Uah, 1, It.IsAny<DateTime>()));
        }

        [Fact]
        public async Task Convert_GivenTo_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("to",
                () => _service.Convert(Currency.Usd, null, 1, It.IsAny<DateTime>()));
        }
    }
}
