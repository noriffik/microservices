﻿using AutoFixture;
using MediatR;
using Moq;
using Moq.Sequences;
using NexCore.Application.Processors;
using NexCore.EventBus.Abstract;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Application.UnitTests
{
    public class IntegrationEventPublishProcessorTest
	{
		[Fact]
		public void Ctor()
		{
			Assert.Injection
                .OfConstructor(typeof(IntegrationEventPublishProcessor<IRequest<int>, int>))
                .HasNullGuard();
		}

		[Fact]
		public async Task Process()
		{
			//Arrange
            var fixture = new Fixture();
            var request = new Mock<IRequest<int>>();
            var eventBus = new Mock<IEventBus>();
            var eventStorage = new Mock<IIntegrationEventStorage>();
            var processor = new IntegrationEventPublishProcessor<IRequest<int>, int>(eventBus.Object, eventStorage.Object);
            var events = fixture.CreateMany<IntegrationEvent>().ToList();

            using (Sequence.Create())
            {
                eventStorage.Setup(e => e.Get())
                    .InSequence()
                    .ReturnsAsync(events);
                eventBus.Setup(e => e.Publish(events))
                    .InSequence();
                eventStorage.Setup(e => e.Clear())
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await processor.Process(request.Object, 10, CancellationToken.None);
            }
        }
	}
}