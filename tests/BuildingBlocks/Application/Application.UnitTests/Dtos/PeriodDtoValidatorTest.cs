﻿using FluentValidation.TestHelper;
using NexCore.Application.Dtos;
using System;
using System.Collections.Generic;
using Xunit;

namespace NexCore.Application.UnitTests
{
    public class PeriodDtoValidatorTest
    {
        private readonly PeriodDtoValidator _validator = new PeriodDtoValidator();

        [Theory]
        [MemberData(nameof(InvalidData))]
        public void ShouldHaveError(PeriodDto dto)
        {
            _validator.ShouldHaveValidationErrorFor(m => m, objectToTest: dto)
                .WithErrorMessage("Invalid period.");
        }

        private static IEnumerable<object[]> InvalidData() => new List<object[]>
        {
            new object[] {new PeriodDto {From = DateTime.MaxValue, To = DateTime.MinValue}},
            new object[] {new PeriodDto {From = DateTime.MinValue, To = DateTime.MinValue}}
        };

        [Theory]
        [MemberData(nameof(ValidData))]
        public void ShouldNotHaveError(PeriodDto dto)
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m, objectToTest: dto);
        }

        private static IEnumerable<object[]> ValidData() => new List<object[]>
        {
            new object [] {new PeriodDto {From = DateTime.MinValue }},
            new object [] {new PeriodDto {From = DateTime.MinValue, To = DateTime.MaxValue }}
        };
    }
}
