﻿using NexCore.Common;
using System;
using Xunit;

namespace NexCore.EventBus.Abstract.UnitTests
{
    public class IntegrationEventTest
    {
        [Fact]
        public void Ctor()
        {
            using (var guidContext = new GuidFactoryContext(Guid.NewGuid()))
            using (var timeContext = new SystemTimeContext(DateTime.UtcNow))
            {
                //Act
                var result = new IntegrationEvent();

                //Assert
                Assert.Equal(guidContext.Value, result.Id);
                Assert.Equal(timeContext.Now, result.CreatedAt);
            }
        }

        [Fact]
        public void Ctor_WithParameters()
        {
            //Arrange
            var id = Guid.NewGuid();
            
            using (var timeContext = new SystemTimeContext(DateTime.UtcNow))
            {
                //Act
                var result = new IntegrationEvent(id, timeContext.Now);

                //Assert
                Assert.Equal(id, result.Id);
                Assert.Equal(timeContext.Now, result.CreatedAt);
            }
        }
    }
}
