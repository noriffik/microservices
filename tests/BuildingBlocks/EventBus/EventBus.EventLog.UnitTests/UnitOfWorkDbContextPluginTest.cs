﻿using AutoFixture;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Storage;
using Moq;
using NexCore.EventBus.Abstract;
using NexCore.Testing;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.EventBus.EventLog.UnitTests
{
    public class UnitOfWorkDbContextPluginTest
    {
        private readonly Guid _transactionId = Guid.NewGuid();

        private readonly List<IntegrationEvent> _events;
        private readonly Mock<IIntegrationEventLogDbContextFactory> _factory;
        private readonly Mock<IIntegrationEventStorage> _store;
        private readonly DbConnection _connection;
        private readonly Mock<IDbContextTransaction> _transaction;

        public UnitOfWorkDbContextPluginTest()
        {
            var fixture = new Fixture();
            _events = fixture.CreateMany<IntegrationEvent>().ToList();

            _factory = new Mock<IIntegrationEventLogDbContextFactory>();
            _store = new Mock<IIntegrationEventStorage>();
            _connection = new Mock<DbConnection>().Object;
            _transaction = new Mock<IDbContextTransaction>();
            
            //Setup dependencies
            _factory.Setup(f => f.Create(_connection)).Returns(CreateDbContext());
            _store.Setup(s => s.Get()).ReturnsAsync(_events);
            _transaction.Setup(t => t.TransactionId).Returns(_transactionId);
        }

        private IntegrationEventLogDbContext CreateDbContext()
        {
            return new IntegrationEventLogDbContext(new DbContextOptionsBuilder<IntegrationEventLogDbContext>()
                .UseInMemoryDatabase(nameof(UnitOfWorkDbContextPluginTest))
                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var plugin = new UnitOfWorkDbContextPlugin(_factory.Object, _store.Object);

            //Assert
            Assert.True(plugin.UseTransaction);
        }

        [Fact]
        public void Ctor_GivenDbContextFactory_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dbContextFactory",
                () => new UnitOfWorkDbContextPlugin(null, _store.Object));
        }

        [Fact]
        public void Ctor_GivenEventStorage_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("eventStorage", () => new UnitOfWorkDbContextPlugin(_factory.Object, null));
        }

        [Fact]
        public async Task Commit()
        {
            //Arrange
            var plugin = new UnitOfWorkDbContextPlugin(_factory.Object, _store.Object)
            {
                UseTransaction = false
            };

            var expected = _events.Select(e => new IntegrationEventLogEntry(e, _transactionId));

            //Act
            await plugin.Commit(_connection, _transaction.Object, CancellationToken.None);

            //Assert
            using (var context = CreateDbContext())
            {
                var actual = await context.IntegrationEventLogs.ToListAsync();
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<IntegrationEventLogEntry>>.Instance);
            }
        }

        [Fact]
        public async Task Commit_WhenUseTransaction_IsTrue_CommitsInTransaction()
        {
            //Arrange
            var plugin = new UnitOfWorkDbContextPlugin(_factory.Object, _store.Object)
            {
                UseTransaction = true
            };

            //Act
            var exception = await Assert.ThrowsAsync<InvalidOperationException>(() =>
                plugin.Commit(_connection, _transaction.Object, CancellationToken.None));

            //Assert
            Assert.Contains("Relational", exception.Message);
        }

        [Fact]
        public async Task Commit_GivenConnection_IsNull_Throws()
        {
            //Arrange
            var plugin = new UnitOfWorkDbContextPlugin(_factory.Object, _store.Object);

            await Assert.ThrowsAsync<ArgumentNullException>("connection",
                () => plugin.Commit(null, _transaction.Object, CancellationToken.None));
        }

        [Fact]
        public async Task Commit_GivenTransaction_IsNull_Throws()
        {
            //Arrange
            var plugin = new UnitOfWorkDbContextPlugin(_factory.Object, _store.Object);

            await Assert.ThrowsAsync<ArgumentNullException>("transaction",
                () => plugin.Commit(_connection, null, CancellationToken.None));
        }
    }
}