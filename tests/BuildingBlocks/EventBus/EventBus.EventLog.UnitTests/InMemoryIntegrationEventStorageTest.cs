﻿using AutoFixture;
using NexCore.EventBus.Abstract;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.EventBus.EventLog.UnitTests
{
    public class InMemoryIntegrationEventStorageTest
	{
		private readonly InMemoryIntegrationEventStorage _storage;
        private readonly Fixture _fixture = new Fixture();

		public InMemoryIntegrationEventStorageTest()
		{
			_storage = new InMemoryIntegrationEventStorage();
		}

		[Fact]
		public async Task Get()
		{
			Assert.NotNull(await _storage.Get());
		}

		[Fact]
		public async Task Add_GivenEvent_IsNull_Throws()
		{
			await Assert.ThrowsAsync<ArgumentNullException>("event",
				async () => await _storage.Add(null));
		}

		[Fact]
		public async Task Add()
		{
            //Arrange
			var @event = _fixture.Create<IntegrationEvent>();

            //Act
			await _storage.Add(@event);

            //Assert
			Assert.Contains(@event, await _storage.Get());
		}

        [Fact]
        public async Task AddRange()
        {
            //Arrange
            var events = _fixture.CreateMany<IntegrationEvent>().ToList();

            //Act
            await _storage.AddRange(events);

            //Assert
            Assert.Equal(events, await _storage.Get());
        }

        [Fact]
        public async Task AddRange_GivenEvents_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("events",
                async () => await _storage.AddRange(null));
        }

        [Fact]
        public async Task Clear()
        {
            //Arrange
            var events = _fixture.CreateMany<IntegrationEvent>().ToList();
            await _storage.AddRange(events);

            //Act
            await _storage.Clear();

            //Assert
            Assert.Empty(await _storage.Get());
        }
	}
}