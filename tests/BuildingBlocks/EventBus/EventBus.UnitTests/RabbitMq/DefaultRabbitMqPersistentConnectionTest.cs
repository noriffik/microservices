using Microsoft.Extensions.Logging;
using Moq;
using NexCore.EventBus.RabbitMq;
using RabbitMQ.Client;
using System;
using Xunit;

namespace EventBus.UnitTests.RabbitMq
{
    public class DefaultRabbitMqPersistentConnectionTest
	{
		[Fact]
		public void RabbitMQ_ConnectionFactory_IsNull_Throws()
		{
			var mockLogger = new Mock<ILogger<DefaultRabbitMqPersistentConnection>>();

			Assert.Throws<ArgumentNullException>("connectionFactory",() => 
				new DefaultRabbitMqPersistentConnection(null, mockLogger.Object));
		}

		[Fact]
		public void RabbitMQ_Logger_IsNull_Throws()
		{
			var mockFactory = new Mock<IConnectionFactory>();

			Assert.Throws<ArgumentNullException>("logger",() =>
				new DefaultRabbitMqPersistentConnection(mockFactory.Object, null));
		}

		[Fact]
		public void RabbitMQ_CreateModel_Throws()
		{
			var mockFactory = new Mock<IConnectionFactory>();
			var mockLogger = new Mock<ILogger<DefaultRabbitMqPersistentConnection>>();
			var rabbitMq = new DefaultRabbitMqPersistentConnection(mockFactory.Object, mockLogger.Object);

			Assert.Throws<InvalidOperationException>(() => rabbitMq.CreateModel());
		}
	}
}
