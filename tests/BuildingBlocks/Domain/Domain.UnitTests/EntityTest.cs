﻿using Moq;
using NexCore.Domain.UnitTests.Fixtures;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.Domain.UnitTests
{
    public class EntityTest
    {
        [Fact]
        public void IsTransient_WhenId_IsNotSet_ReturnsTrue()
        {
            //Arrange
            var transient = new AnotherDerivedEntity();

            //Assert
            Assert.True(transient.IsTransient());
            Assert.Empty(transient.EntityEvents);
        }

        [Fact]
        public void IsTransient_WhenId_IsSet_ReturnsFalse()
        {
            //Arrange
            var nonTransient = DerivedEntity.NonTransient(1);

            //Assert
            Assert.False(nonTransient.IsTransient());
            Assert.Empty(nonTransient.EntityEvents);
        }

        [Theory]
        [MemberData(nameof(EqualityData))]
        public void Equality(object objA, object objB, bool expectation)
        {
            var entityA = objA as Entity;
            var entityB = objB as Entity;

            Assert.True(Equals(objA, objB) == expectation);
            Assert.True(entityA == entityB == expectation);
            Assert.True(entityA != entityB == !expectation);
        }

        public static IEnumerable<object[]> EqualityData
        {
            get
            {
                var nonTransient = DerivedEntity.NonTransient(1);
                var sameNonTransient = DerivedEntity.NonTransient(1);
                var anotherNonTransient = DerivedEntity.NonTransient(2);
                var transient = DerivedEntity.Transient();
                var anotherTransient = DerivedEntity.Transient();

                return new List<object[]>
                {
                    new object[] { nonTransient, nonTransient, true },
                    new object[] { transient, transient, true },
                    new object[] { nonTransient, sameNonTransient, true },
                    new object[] { nonTransient, anotherNonTransient, false },
                    new object[] { nonTransient, AnotherDerivedEntity.NonTransient(0), false },
                    new object[] { nonTransient, transient, false },
                    new object[] { nonTransient, anotherTransient, false },
                    new object[] { nonTransient, null, false },
                    new object[] { null, nonTransient, false }
                };
            }
        }

        [Theory]
        [MemberData(nameof(EqualityData))]
        public void Hashing(object objA, object objB, bool expectation)
        {
            Assert.True(Equals(objA?.GetHashCode(), objB?.GetHashCode()) == expectation);
        }

        [Fact]
        public void AddEntityEvent()
        {
            //Arrange
            var entityEvent = new Mock<IEntityEvent>();
            var entity = new DerivedEntity();

            //Act
            entity.AddEntityEvent(entityEvent.Object);

            //Assert
            Assert.Contains(entityEvent.Object, entity.EntityEvents);
        }

        [Fact]
        public void AddEntityEvent_GivenEntityEvent_IsNull_Throws()
        {
            //Arrange
            var entity = new DerivedEntity();

            //Assert
            Assert.Throws<ArgumentNullException>("entityEvent", () => entity.AddEntityEvent(null));
        }

        [Fact]
        public void RemoveEntityEvent()
        {
            //Arrange
            var entity = new DerivedEntity();
            Enumerable.Range(0, 5)
                .Select(n => new Mock<IEntityEvent>().Object)
                .ToList()
                .ForEach(e => entity.AddEntityEvent(e));
            
            var expected = entity.EntityEvents.ElementAt(2);
            
            //Act
            entity.RemoveEntityEvent(expected);

            //Assert
            Assert.DoesNotContain(expected, entity.EntityEvents);
        }

        [Fact]
        public void RemoveEntityEvent_GivenEntityEvent_IsNull_Throws()
        {
            //Arrange
            var entity = new DerivedEntity();

            //Assert
            Assert.Throws<ArgumentNullException>("entityEvent", () => entity.RemoveEntityEvent(null));
        }

        [Fact]
        public void ClearEntityEvents()
        {
            //Arrange
            var entity = new DerivedEntity();
            Enumerable.Range(0, 5)
                .Select(n => new Mock<IEntityEvent>().Object)
                .ToList()
                .ForEach(e => entity.AddEntityEvent(e));

            //Act
            entity.ClearEntityEvents();

            //Assert
            Assert.Empty(entity.EntityEvents);
        }
    }
}
