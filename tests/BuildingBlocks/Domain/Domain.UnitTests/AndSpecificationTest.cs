﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace NexCore.Domain.UnitTests
{
    public class AndSpecificationTest
    {
        private class Value
        {
            public bool Left { get; }

            public bool Right { get; }

            public bool IsEqual => Left && Right;

            public Value(bool left, bool right)
            {
                Left = left;
                Right = right;
            }
        }

        private class Is : Specification<Value>
        {
            public static readonly Specification<Value> Left = new Is(v => v.Left);
            public static readonly Specification<Value> Right = new Is(v => v.Right);
            
            private readonly Func<Value, bool> _selector;

            private Is(Func<Value, bool> selector)
            {
                _selector = selector;
            }

            public override Expression<Func<Value, bool>> ToExpression()
            {
                return v => _selector.Invoke(v);
            }

            public override string Description => throw new NotSupportedException();
        }

        [Fact]
        public void Ctor_GivenLeft_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("left", () => new AndSpecification<Value>(null, Is.Left));
        }

        [Fact]
        public void Ctor_GivenRight_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("right", () => new AndSpecification<Value>(Is.Left, null));
        }

        [Fact]
        public void SatisfiedBy_WhenIs_ReturnsTrue()
        {
            //Arrange
            var specification = new AndSpecification<Value>(Is.Left, Is.Right);

            //Act
            Assert.True(specification.IsSatisfiedBy(new Value(true, true)));
        }

        [Fact]
        public void SatisfiedBy_WhenLeft_IsNot_ReturnsFalse()
        {
            //Arrange
            var specification = new AndSpecification<Value>(Is.Left, Is.Right);

            //Act
            Assert.False(specification.IsSatisfiedBy(new Value(false, true)));
        }

        [Fact]
        public void SatisfiedBy_WhenRight_IsNot_ReturnsFalse()
        {
            //Arrange
            var specification = new AndSpecification<Value>(Is.Left, Is.Right);

            //Act
            Assert.False(specification.IsSatisfiedBy(new Value(true, false)));
        }

        [Fact]
        public void SatisfiedBy_IsNot_ReturnsFalse()
        {
            //Arrange
            var specification = new AndSpecification<Value>(Is.Left, Is.Right);

            //Act
            Assert.False(specification.IsSatisfiedBy(new Value(false, false)));
        }

        [Fact]
        public void ToExpression()
        {
            //Arrange
            var all = new[]
                {
                    new Value(true, false),
                    new Value(true, false),
                    new Value(false, true),
                    new Value(false, true),
                    new Value(true, true),
                    new Value(true, true),
                    new Value(false, false),
                    new Value(false, false)
                }
                .ToList()
                .AsQueryable();

            var expected = all.Where(v => v.IsEqual);
            var specification = new AndSpecification<Value>(Is.Left, Is.Right);

            //Act
            var expression = specification.ToExpression();

            //Assert
            Assert.Equal(expected, all.Where(expression));
        }
    }
}
