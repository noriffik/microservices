﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace NexCore.Domain.UnitTests
{
    public class NotSpecificationTest
    {
        private class Value
        {
            public bool Specification { get; }

            public bool IsEqual => !Specification;

            public Value(bool specification)
            {
                Specification = specification;
            }
        }

        private class Is : Specification<Value>
        {
            public static readonly Specification<Value> Specification = new Is(v => v.Specification);
            
            private readonly Func<Value, bool> _selector;

            private Is(Func<Value, bool> selector)
            {
                _selector = selector;
            }

            public override Expression<Func<Value, bool>> ToExpression()
            {
                return v => _selector.Invoke(v);
            }

            public override string Description => throw new NotSupportedException();
        }

        [Fact]
        public void Ctor_GivenSpecification_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("specification", () => new NotSpecification<Value>(null));
        }

        [Fact]
        public void SatisfiedBy_WhenIs_ReturnsFalse()
        {
            //Arrange
            var specification = new NotSpecification<Value>(Is.Specification);

            //Act
            Assert.False(specification.IsSatisfiedBy(new Value(true)));
        }

        [Fact]
        public void SatisfiedBy_WhenIsNot_ReturnsTrue()
        {
            //Arrange
            var specification = new NotSpecification<Value>(Is.Specification);

            //Act
            Assert.True(specification.IsSatisfiedBy(new Value(false)));
        }

        [Fact]
        public void ToExpression()
        {
            //Arrange
            var all = new[]
                {
                    new Value(true),
                    new Value(false)
                }
                .ToList()
                .AsQueryable();

            var expected = all.Where(v => v.IsEqual);
            var specification = new NotSpecification<Value>(Is.Specification);

            //Act
            var expression = specification.ToExpression();

            //Assert
            Assert.Equal(expected, all.Where(expression));
        }
    }
}
