﻿using System;
using System.Collections.Generic;
using Xunit;

namespace NexCore.Domain.UnitTests
{
    public class PeriodTest
    {
        private readonly Period _period = new Period(new DateTime(1, 1, 5), new DateTime(1, 1, 10));

        [Theory]
        [MemberData(nameof(Valid))]
        public void Ctor(DateTime from, DateTime? to)
        {
            //Act
            var result = new Period(from, to);

            //Assert
            Assert.Equal(from.Date, result.From);
            Assert.Equal(to?.Date, result.To);
        }

        public static IEnumerable<object[]> Valid => new List<object[]>
        {
            new object[]{DateTime.MinValue, DateTime.MaxValue},
            new object[]{DateTime.MinValue, null}
        };

        [Fact]
        public void Ctor_WithYear()
        {
            //Arrange
            var year = new Year(2099);

            //Act
            var result = new Period(year);

            //Assert
            Assert.Equal(new DateTime(year.Value, 1, 1), result.From);
            Assert.Equal(new DateTime(year.Value, 12, 31), result.To);
        }

        [Theory]
        [MemberData(nameof(Invalid))]
        public void Ctor_GivenParameters_AreInvalid_Throws(DateTime from, DateTime? to)
        {
            Assert.Throws<InvalidPeriodException>(() => new Period(from, to));
        }

        public static IEnumerable<object[]> Invalid => new List<object[]>
        {
            new object[]{DateTime.MaxValue, DateTime.MinValue},
            new object[]{DateTime.MinValue, DateTime.MinValue}
        };

        [Theory]
        [InlineData(05, 10, true)]
        [InlineData(06, 10, true)]
        [InlineData(06, 11, true)]
        [InlineData(04, 10, true)]
        [InlineData(04, 09, true)]
        [InlineData(10, 11, true)]
        [InlineData(09, 11, true)]
        [InlineData(01, 04, false)]
        [InlineData(11, 20, false)]
        public void IsOverlapping(int from, int to, bool expected)
        {
            var overlapping = new Period(new DateTime(1, 1, from), new DateTime(1, 1, to));

            Assert.Equal(expected, _period.IsOverlapping(overlapping));
            Assert.Equal(expected, Period.IsOverlapping(_period, overlapping));
        }

        [Fact]
        public void IsOverlapping_GivenPeriod_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("period", () => _period.IsOverlapping(null));
        }

        [Fact]
        public void IsOverlapping_GivenA_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("a", () => Period.IsOverlapping(null, _period));
        }

        [Fact]
        public void IsOverlapping_GivenB_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("b", () => Period.IsOverlapping(_period, null));
        }

        [Theory]
        [InlineData(05, 10, false)]
        [InlineData(06, 10, false)]
        [InlineData(06, 11, false)]
        [InlineData(04, 10, false)]
        [InlineData(04, 09, false)]
        [InlineData(10, 11, false)]
        [InlineData(09, 11, false)]
        [InlineData(01, 04, true)]
        [InlineData(11, 20, true)]
        public void IsNonOverlapping(int from, int to, bool expected)
        {
            var overlapping = new Period(new DateTime(1, 1, from), new DateTime(1, 1, to));

            Assert.Equal(expected, _period.IsNonOverlapping(overlapping));
            Assert.Equal(expected, Period.IsNonOverlapping(_period, overlapping));
        }

        [Fact]
        public void IsNonOverlapping_GivenPeriod_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("period", () => _period.IsNonOverlapping(null));
        }

        [Fact]
        public void IsNonOverlapping_GivenA_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("a", () => Period.IsNonOverlapping(null, _period));
        }

        [Fact]
        public void IsNonOverlapping_GivenB_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("b", () => Period.IsNonOverlapping(_period, null));
        }

        [Fact]
        public void WithAnotherEnding()
        {
            //Act
            var period = _period.WithAnotherEnding(DateTime.MaxValue);

            //Assert
            Assert.Equal(_period.From, period.From);
            Assert.Equal(DateTime.MaxValue.Date, period.To);
        }

        [Theory]
        [InlineData(1, 1, 1)]
        [InlineData(1, 1, 2)]
        public void WithAnotherEnding_GivenTo_IsInvalid_Throws(int year, int month, int day)
        {
            var period = new Period(new DateTime(1, 1, 2), null);
            var periodEnd = new DateTime(year, month, day);

            Assert.Throws<InvalidPeriodException>(() => period.WithAnotherEnding(periodEnd));
        }
    }
}
