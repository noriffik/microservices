﻿using NexCore.Testing;
using System;
using System.Linq;
using Xunit;

namespace NexCore.Domain.UnitTests
{
    public class IdSpecificationTest
    {
        //Entities
        private readonly TestAggregateRoot _entity;
        private readonly TestAggregateRoot _anotherEntity;

        //Specification
        private readonly IdSpecification<TestAggregateRoot> _specification;

        public IdSpecificationTest()
        {
            var entities = Enumerable.Range(0, 5)
                .Select(id => new TestAggregateRoot(id))
                .ToList();

            _entity = entities[2];
            _anotherEntity = entities[3];

            _specification = new IdSpecification<TestAggregateRoot>(_entity.Id);
        }

        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public void IsSatisfied(bool expected)
        {
            //Arrange
            var entity = expected ? _entity : _anotherEntity;
            
            //Act
            var actual = _specification.IsSatisfiedBy(entity);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }
        
        [Fact]
        public void ToExpression()
        {
            //Arrange
            var expected = new[] {_entity};
            
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(expected, expected.AsQueryable().Where(expression));
        }
    }
}
