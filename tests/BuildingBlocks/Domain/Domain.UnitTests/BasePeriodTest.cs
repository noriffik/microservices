﻿using System;
using NexCore.Domain.UnitTests.Fixtures;
using Xunit;

namespace NexCore.Domain.UnitTests
{
    public class BasePeriodTest
    {
        [Fact]
        public void Formatting()
        {
            var period = new TestPeriod(new DateTime(2019, 1, 28), new DateTime(2019, 11, 29));

            Assert.Equal("28.01.2019 - 29.11.2019", period.ToString());
        }

        [Fact]
        public void Formatting_WhenTo_IsNull()
        {
            var period = new TestPeriod(new DateTime(2019, 1, 28), null);

            Assert.Equal("From 28.01.2019", period.ToString());
        }

        [Fact]
        public void Formatting_WhenFrom_IsNull()
        {
            var period = new TestPeriod(null, new DateTime(2019, 1, 28));

            Assert.Equal("To 28.01.2019", period.ToString());
        }

        [Fact]
        public void Formatting_WhenBoth_AreNull_ReturnsEmptyString()
        {
            var period = new TestPeriod(null, null);

            Assert.Empty(period.ToString());
        }
    }
}