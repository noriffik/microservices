﻿using System.Collections.Generic;
using Xunit;

namespace NexCore.Domain.UnitTests
{
    public class ValueObjectTest
    {
        class DerivedValueObject : ValueObject
        {
            public DerivedValueObject(int firstValue, int secondValue)
            {
                FirstValue = firstValue;
                SecondValue = secondValue;
            }

            public int FirstValue { get; }

            public int SecondValue { get; }

            protected override IEnumerable<object> GetValues()
            {
                yield return FirstValue;
                yield return SecondValue;
            }
        }

        class AnotherDerivedValueObject : ValueObject
        {
            public AnotherDerivedValueObject(int firstValue, int secondValue)
            {
                FirstValue = firstValue;
                SecondValue = secondValue;
            }

            public int FirstValue { get; }

            public int SecondValue { get; }

            protected override IEnumerable<object> GetValues()
            {
                yield return FirstValue;
                yield return SecondValue;
            }
        }

        class GenericValueObject<T1, T2> : ValueObject
        {
            public GenericValueObject(T1 firstValue, T2 secondValue)
            {
                FirstValue = firstValue;
                SecondValue = secondValue;
            }

            T1 FirstValue { get; }

            T2 SecondValue { get; }

            protected override IEnumerable<object> GetValues()
            {
                yield return FirstValue;
                yield return SecondValue;
            }
        }

        class SingleValueObject : ValueObject
        {
            public SingleValueObject(int value)
            {
                Value = value;
            }

            int Value { get; }

            protected override IEnumerable<object> GetValues()
            {
                yield return Value;
            }
        }

        class MixedTypeValueObject : ValueObject
        {
            public MixedTypeValueObject(int? firstValue, string secondValue)
            {
                FirstValue = firstValue;
                SecondValue = secondValue;
            }

            public int? FirstValue { get; }

            public string SecondValue { get; }

            protected override IEnumerable<object> GetValues()
            {
                yield return FirstValue;
                yield return SecondValue;
            }
        }

        class EnumerableValueObject : ValueObject
        {
            public IEnumerable<int> EnumerableIntValue { get; }

            public IEnumerable<SingleValueObject> EnumerableObjectValue { get; }

            public string StringValue { get; }

            public EnumerableValueObject(IEnumerable<int> enumerableValue, IEnumerable<SingleValueObject> enumerableObjectValue, string stringValue)
            {
                EnumerableIntValue = enumerableValue;
                EnumerableObjectValue = enumerableObjectValue;
                StringValue = stringValue;
            }

            protected override IEnumerable<object> GetValues()
            {
                yield return EnumerableIntValue;
                yield return EnumerableObjectValue;
                yield return StringValue;
            }
        }

        [Theory]
        [MemberData(nameof(EqualityData))]
        public void Equality(object objA, object objB, bool expectation)
        {
            var valueA = objA as ValueObject;
            var valueB = objB as ValueObject;

            Assert.True(Equals(objA, objB) == expectation);
            Assert.True(valueA == valueB == expectation);
            Assert.True(valueA != valueB == !expectation);
        }

        public static IEnumerable<object[]> EqualityData
        {
            get
            {
                var value = new DerivedValueObject(1, 2);
                var enumerableValue = new EnumerableValueObject(new[] { 1, 2 }, new[] { new SingleValueObject(1), new SingleValueObject(2) }, "1 2");

                return new List<object[]>
                {
                    new object[] { value, value, true },
                    new object[] { value, new DerivedValueObject(1, 2), true },
                    new object[] { null, value, false },
                    new object[] { value, null, false },
                    new object[] { value, new AnotherDerivedValueObject(1, 2), false },
                    new object[] { value, new DerivedValueObject(3, 2), false },
                    new object[] { value, new DerivedValueObject(1, 3), false },
                    new object[] { new SingleValueObject(1), 1, false },
                    new object[] { enumerableValue, enumerableValue, true},
                    new object[] { enumerableValue, new EnumerableValueObject(new[] { 1, 2 }, new[] { new SingleValueObject(1), new SingleValueObject(2) }, "1 2"), true},
                    new object[] { enumerableValue, null, false},
                    new object[] { enumerableValue, new EnumerableValueObject(new[] { 1, 3 }, new[] { new SingleValueObject(1), new SingleValueObject(2) }, "1 2"), false},
                    new object[] { enumerableValue, new EnumerableValueObject(new[] { 1, 2 }, new[] { new SingleValueObject(1), new SingleValueObject(3) }, "1 2"), false},
                    new object[] { enumerableValue, new EnumerableValueObject(new[] { 1, 2 }, new[] { new SingleValueObject(1), new SingleValueObject(2) }, "1 3"), false},
                    new object[] { enumerableValue, new EnumerableValueObject(new[] { 1, 2 }, new SingleValueObject[] { null, null }, "1 2"), false},
                };
            }
        }

        [Theory]
        [MemberData(nameof(EqualityData))]
        public void Hashing(object objA, object objB, bool expectation)
        {
            Assert.True(Equals(objA?.GetHashCode(), objB?.GetHashCode()) == expectation);
        }

        [Fact]
        public void ToString_ReturnsString_WithAllValues()
        {
            var value = new DerivedValueObject(1, 2);

            Assert.Equal("1, 2", value.ToString());
        }

        [Fact]
        public void IsNullOrEmpty_GivenValue_IsNullOrEmpty_ReturnsTrue()
        {
            Assert.True(ValueObject.IsNullOrEmpty(null));
            Assert.True(ValueObject.IsNullOrEmpty(new MixedTypeValueObject(null, string.Empty)));
        }

        [Theory]
        [InlineData(33, "")]
        [InlineData(null, "test")]
        public void IsNullOrEmpty_GivenValue_IsNot_ReturnsFalse(int? first, string second)
        {
            //Arrange
            var address = new GenericValueObject<int?, string>(first, second);

            //Assert
            Assert.False(ValueObject.IsNullOrEmpty(address));
        }
    }
}
