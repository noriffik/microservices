﻿using System;
using Xunit;

namespace NexCore.Domain.UnitTests
{
    public class YearTest
    {
        [Theory]
        [InlineData(1990)]
        [InlineData(2000)]
        [InlineData(2020)]
        public void Ctor(int value)
        {
            //Act
            var year = new Year(value);

            //Assert
            Assert.Equal(year.Value, value);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(10000)]
        [InlineData(20290)]
        public void Define_GivenInvalidValue_Throws(int value)
        {
            //Act
            var e = Assert.Throws<ArgumentOutOfRangeException>(() => new Year(value));

            //Assert
            Assert.Equal("value", e.ParamName);
        }

        [Fact]
        public void From_WithDateTime()
        {
            //Arrange
            var expected = new Year(DateTime.Now.Year);

            //Act
            var result = Year.From(DateTime.Now);

            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void From_WithInteger()
        {
            //Arrange
            var expected = new Year(2019);

            //Act
            var result = Year.From(2019);

            //Assert
            Assert.Equal(expected, result);
        }
    }
}
