﻿namespace NexCore.Domain.UnitTests.Fixtures
{
    public class AnotherDerivedEntity : Entity
    {
        public static Entity NonTransient(int id) => new AnotherDerivedEntity {Id = id};
    }
}