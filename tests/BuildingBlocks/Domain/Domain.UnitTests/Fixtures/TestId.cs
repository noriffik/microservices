﻿namespace NexCore.Domain.UnitTests.Fixtures
{
    public class TestId : EntityId
    {
        public TestId(string value) : base(value)
        {
        }

        public static TestId Parse(string value)
        {
            return new TestId(value);
        }
    }
}
