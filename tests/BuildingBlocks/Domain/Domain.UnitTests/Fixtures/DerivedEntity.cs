﻿namespace NexCore.Domain.UnitTests.Fixtures
{
    public class DerivedEntity : Entity, IAggregateRoot
    {
        public DerivedEntity()
        {
        }

        public DerivedEntity(int id) : base(id)
        {
        }

        public static Entity NonTransient(int id) => new DerivedEntity(id);

        public static Entity Transient() => new DerivedEntity();
    }
}