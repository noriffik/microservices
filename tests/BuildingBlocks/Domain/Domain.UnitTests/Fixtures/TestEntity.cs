﻿namespace NexCore.Domain.UnitTests.Fixtures
{
    public class TestEntity : Entity<TestId>
    {
        public TestEntity(TestId id) : base(id)
        {
        }

        public static TestEntity WithId(string id)
        {
            return new TestEntity(TestId.Parse(id));
        }
    }
}
