﻿using System;

namespace NexCore.Domain.UnitTests.Fixtures
{
    internal class TestPeriod : PeriodBase
    {
        public TestPeriod(DateTime? @from, DateTime? to)
            : base(PeriodValidator.NoneRequired, @from, to)
        {
        }
    }
}