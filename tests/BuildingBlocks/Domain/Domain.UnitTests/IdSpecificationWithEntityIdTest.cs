﻿using AutoFixture;
using NexCore.Testing;
using System;
using System.Linq;
using Xunit;

namespace NexCore.Domain.UnitTests
{
    public class IdSpecificationWithEntityIdTest
    {
        //Entities
        private readonly TestAggregateWithTestIdRoot _entity;
        private readonly TestAggregateWithTestIdRoot _anotherEntity;

        //Specification
        private readonly IdSpecification<TestAggregateWithTestIdRoot, TestId> _specification;

        public IdSpecificationWithEntityIdTest()
        {
            var fixture = new Fixture();

            var entities = fixture.CreateMany<TestAggregateWithTestIdRoot>(5).ToList();
            _entity = entities[2];
            _anotherEntity = entities[3];

            _specification = new IdSpecification<TestAggregateWithTestIdRoot, TestId>(_entity.Id);
        }

        [Fact]
        public void Ctor_GivenId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(
                () => new IdSpecification<TestAggregateWithTestIdRoot, TestId>(null));
        }

        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public void IsSatisfied(bool expected)
        {
            //Arrange
            var entity = expected ? _entity : _anotherEntity;
            
            //Act
            var actual = _specification.IsSatisfiedBy(entity);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }
        
        [Fact]
        public void ToExpression()
        {
            //Arrange
            var expected = new[] {_entity};
            
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(expected, expected.AsQueryable().Where(expression));
        }
    }
}
