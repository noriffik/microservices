﻿using Moq;
using NexCore.Domain.UnitTests.Fixtures;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace NexCore.Domain.UnitTests
{
    [SuppressMessage("ReSharper", "SuspiciousTypeConversion.Global", Justification = "Required for testing purposes.")]
    public class EntityWithEntityIdTest
    {
        [Fact]
        public void Equality()
        {
            //Arrange
            var entity = TestEntity.WithId("1");

            //Assert
            Assert.Equal(entity, entity);
            Assert.Equal(entity, TestEntity.WithId("1"));
            Assert.NotEqual(entity, TestEntity.WithId("2"));
            Assert.NotEqual(entity, null);
        }

        [Fact]
        public void Equality_WhenCompared_AgainstObject_OfDifferentType()
        {
            //Arrange
            var entity = TestEntity.WithId("1");

            //Assert
            Assert.False(entity.Equals(null));
            Assert.False(entity.Equals("1"));
            Assert.False(entity.Equals(TestId.Parse("1")));
        }

        [Fact]
        public void Hashing()
        {
            //Arrange
            var hash = TestEntity.WithId("1").GetHashCode();

            //Assert
            Assert.Equal(hash, TestEntity.WithId("1").GetHashCode());
            Assert.NotEqual(hash, TestEntity.WithId("2").GetHashCode());
            Assert.NotEqual(hash, "1".GetHashCode());
        }
        
        
        [Fact]
        public void AddEntityEvent()
        {
            //Arrange
            var entityEvent = new Mock<IEntityEvent>();
            var entity = TestEntity.WithId("1");

            //Act
            entity.AddEntityEvent(entityEvent.Object);

            //Assert
            Assert.Contains(entityEvent.Object, entity.EntityEvents);
        }

        [Fact]
        public void AddEntityEvent_GivenEntityEvent_IsNull_Throws()
        {
            //Arrange
            var entity = TestEntity.WithId("1");

            //Assert
            Assert.Throws<ArgumentNullException>("entityEvent", () => entity.AddEntityEvent(null));
        }

        [Fact]
        public void RemoveEntityEvent()
        {
            //Arrange
            var entity = TestEntity.WithId("1");
            Enumerable.Range(0, 5)
                .Select(n => new Mock<IEntityEvent>().Object)
                .ToList()
                .ForEach(e => entity.AddEntityEvent(e));
            
            var expected = entity.EntityEvents.ElementAt(2);
            
            //Act
            entity.RemoveEntityEvent(expected);

            //Assert
            Assert.DoesNotContain(expected, entity.EntityEvents);
        }

        [Fact]
        public void RemoveEntityEvent_GivenEntityEvent_IsNull_Throws()
        {
            //Arrange
            var entity = TestEntity.WithId("1");

            //Assert
            Assert.Throws<ArgumentNullException>("entityEvent", () => entity.RemoveEntityEvent(null));
        }

        [Fact]
        public void ClearEntityEvents()
        {
            //Arrange
            var entity = TestEntity.WithId("1");
            Enumerable.Range(0, 5)
                .Select(n => new Mock<IEntityEvent>().Object)
                .ToList()
                .ForEach(e => entity.AddEntityEvent(e));

            //Act
            entity.ClearEntityEvents();

            //Assert
            Assert.Empty(entity.EntityEvents);
        }
    }
}
