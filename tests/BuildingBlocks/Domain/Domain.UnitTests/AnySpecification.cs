﻿using Moq;
using NexCore.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.Domain.UnitTests
{
    public class AnySpecificationTest
    {
        private readonly TestAggregateRoot _entity = new TestAggregateRoot(123);

        [Fact]
        public void Ctor_GivenInner_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(
                "inner", () => new AnySpecification<TestAggregateRoot>(
                    null as IEnumerable<Specification<TestAggregateRoot>>));
        }

        [Fact]
        public void Ctor_GivenInnerParams_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(
                "inner", () => new AnySpecification<TestAggregateRoot>(null));
        }

        [Theory]
        [InlineData(true, true, true)]
        [InlineData(false, false, false)]
        [InlineData(true, true, true, false)]
        [InlineData(true)]
        public void IsSatisfied(bool expected, params bool[] results)
        {
            //Arrange
            var inner = results.Select(SetupSpecification).ToArray();
            var specification = new AnySpecification<TestAggregateRoot>(inner);

            //Act
            var result = specification.IsSatisfiedBy(_entity);

            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            //Arrange
            var specification = new AnySpecification<TestAggregateRoot>();

            //Assert
            Assert.Throws<ArgumentNullException>("entity", () => specification.IsSatisfiedBy(null));
        }
        
        [Fact]
        public void ToExpression()
        {
            //Arrange
            var entities = Enumerable.Range(1, 5)
                .Select(id => new TestAggregateRoot(id))
                .Concat(new[] {_entity});
            var expected = entities.TakeLast(1)
                .ToList();

            var specification = new AnySpecification<TestAggregateRoot>(SetupSpecification(true));
            
            //Act
            var expression = specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(expected, expected.AsQueryable().Where(expression));
        }

        private Specification<TestAggregateRoot> SetupSpecification(bool isSatisfied)
        {
            return new TestSpecification<TestAggregateRoot>(_entity, isSatisfied);
        }
    }
}
