using DocumentFormat.OpenXml.Packaging;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using NexCore.DocumentProcessor.Exceptions;
using Xunit;

namespace NexCore.DocumentProcessor.UnitTests
{
    public class WordDocumentFillerUnitTest
	{
        private readonly string _file = Path.Combine(new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName, "Test_Template.docx");

        private readonly IDictionary<string, string> _replace = new Dictionary<string, string>
		{
			{"Test", "REPLACED_1"},
			{"Test2", "REPLACED_2"}
        };

        private readonly IDictionary<string, IList<string>> _replaceValues = new Dictionary<string, IList<string>>
        {
	        {"Test3", new List<string> {"First value", "Second value"}},
	        {"Test4", new List<string> {"First value", "Second value"}}
        };

		[Fact]
		public void Fill_GivenSteam_IsNull_Throws()
		{
			Assert.Throws<ArgumentNullException>("stream", () => new WordDocumentFiller().Transform(null, _replace));
		}

        [Fact]
        public void Fill_GivenValues_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("values", () => new WordDocumentFiller().Transform(new Mock<Stream>().Object, null));
        }

		[Fact]
		public void Fill()
		{
            using (var stream = ReadFile(_file))
            {
				//Act
				new WordDocumentFiller().Transform(stream, _replace);

                //Assert
                var result = ReadDocument(stream);

                _replace.Keys.ToList().ForEach(k => Assert.DoesNotContain(k + ":", result));
                _replace.Values.ToList().ForEach(v => Assert.Contains(v, result));
            }
        }

		[Fact]
        public void Fill_WhenField_IsRequired_Replaces()
        {
            //Arrange
            const string value = "value";

            using (var stream = ReadFile(_file))
            {
				//Act
				new WordDocumentFiller().Transform(stream, new Dictionary<string, string>
                {
                    {"RequiredField", value}
                });

                //Assert
                var result = ReadDocument(stream);

                Assert.DoesNotContain("RequiredField:", result);
                Assert.Contains(value, result);
            }
        }

        [Fact]
        public void Fill_WhenRequiredField_HasNoReplacement_Throws()
        {
	        var e = Assert.Throws<MissingRequiredValueException>(() =>
	        {
		        using (var stream = ReadFile(_file))
		        {
			        new WordDocumentFiller().Transform(stream, new Dictionary<string, string>
			        {
				        {"RequiredField", string.Empty}
			        });
		        }
	        });

	        Assert.Equal("RequiredField", e.Field);
        }

        [Fact]
        public void Fill_WhenField_WasNotFound_DoesNothing()
        {
	        //Arrange
	        const string value = "value";

	        using (var stream = ReadFile(_file))
	        {
		        //Act
		        new WordDocumentFiller().Transform(stream, new Dictionary<string, string>
		        {
			        { "Test3:", value }
		        });

		        //Assert
		        var result = ReadDocument(stream);

		        Assert.DoesNotContain(value, result);
	        }
        }

		[Fact]
		public void Fill_ListValues()
		{
			using (var stream = ReadFile(_file))
			{
				//Act
				new WordDocumentFiller().TransformListValues(stream, _replaceValues);

				//Assert
				var result = ReadDocument(stream);

				_replaceValues.Keys.ToList().ForEach(k => Assert.DoesNotContain(k + ":", result));
				_replaceValues.Values.ToList().ForEach(v => Assert.DoesNotContain(v + ":", result));
			}
		}

        [Fact]
        public void Fill_WhenListFields_IsRequired_Replaces()
        {
	        //Arrange
	         var values = new List<string> {"value", "value1"};

			using (var stream = ReadFile(_file))
	        {
		        //Act
		        new WordDocumentFiller().TransformListValues(stream, new Dictionary<string, IList<string>>
		        {
			        {"RequiredField", values}
		        });

		        //Assert
		        var result = ReadDocument(stream);

		        Assert.DoesNotContain("RequiredField:", result);
		        Assert.DoesNotContain(result, values);
	        }
        }

        [Fact]
        public void Fill_WhenListFields_WasNotFound_DoesNothing()
        {
            //Arrange
            var values = new List<string> { "value", "Value1" };

			using (var stream = ReadFile(_file))
            {
				//Act
				new WordDocumentFiller().TransformListValues(stream, new Dictionary<string, IList<string>>
                {
                    { "Test3:", values }
                });

                //Assert
                var result = ReadDocument(stream);

                Assert.DoesNotContain(values.First(), result);
            }
        }

        [Fact]
        public void Fill_WhenListField_WasNotFound_DoesNothing()
        {
	        //Arrange
	        var values = new List<string> {"value", "Value1"};

			using (var stream = ReadFile(_file))
	        {
				//Act
				new WordDocumentFiller().TransformListValues(stream, new Dictionary<string, IList<string>>
				{
					{"RequiredField", values}
				});

				//Assert
				var result = ReadDocument(stream);

		        Assert.DoesNotContain(result, values);
	        }
        }

		private static Stream ReadFile(string path)
        {
            var stream = new MemoryStream();
            using (var sr = File.Open(path, FileMode.Open, FileAccess.Read))
                sr.CopyTo(stream);

            return stream;
        }

        private static string ReadDocument(Stream stream)
        {
            using (var wordDoc = WordprocessingDocument.Open(stream, false))
            using (var sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
            {
                return sr.ReadToEnd();
            }
        }
	}
}
