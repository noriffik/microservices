using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using DocumentFormat.OpenXml.Packaging;
using Moq;
using Xunit;
using Enumerable = System.Linq.Enumerable;

namespace NexCore.DocumentProcessor.UnitTests
{
	public class WordMergeFieldFillerUnitTest
	{
		private readonly string _file = Path.Combine(new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName, "MergeField_Template.docx");

		private readonly IDictionary<string, string> _replace = new Dictionary<string, string>
		{
			{"Test1", "REPLACED_1"},
			{"Test2", "REPLACED_2"},
			{"Required", "REPLACED" }
		};

		private readonly IDictionary<string, IList<string>> _replaceValues = new Dictionary<string, IList<string>>
		{
			{"Test", new List<string>{"First value", "Second value"}},
			{"Test2", new List<string>{"First value", "Second value"}}
		};

		[Fact]
		public void Fill_GivenSteam_IsNull_Throws()
		{
			Assert.Throws<ArgumentNullException>("stream", () => new WordMergeFieldFiller().Transform(null, _replace));
		}

		[Fact]
		public void Fill_GivenValues_IsNull_Throws()
		{
			Assert.Throws<ArgumentNullException>("values", () => new WordMergeFieldFiller().Transform(new Mock<Stream>().Object, null));
		}

		[Fact]
		public void Fill_GivenListValues_IsNull_Throws()
		{
			Assert.Throws<ArgumentNullException>("values", () => new WordMergeFieldFiller().TransformListValues(new Mock<Stream>().Object, null));
		}

		[Fact]
		public void Fill()
		{
			using (var stream = ReadFile(_file))
			{
				//Act
				new WordMergeFieldFiller().Transform(stream, _replace);

				//Assert
				var result = ReadDocument(stream);

				Enumerable.ToList(_replace.Keys).ForEach(k => Assert.DoesNotContain(k + ":", result));
				Enumerable.ToList(_replace.Values).ForEach(v => Assert.Contains(v, result));
			}
		}

		[Fact]
		public void Fill_WhenField_IsRequired_Replaces()
		{
			//Arrange
			const string value = "Required";

			using (var stream = ReadFile(_file))
			{
				//Act
				new WordMergeFieldFiller().Transform(stream, new Dictionary<string, string>
				{
					{"Required", value}
				});

				//Assert
				var result = ReadDocument(stream);

				Assert.DoesNotContain("RequiredField:", result);
				Assert.Contains(value, result);
			}
		}

		[Fact]
		public void Fill_WhenField_WasNotFound_DoesNothing()
		{
			//Arrange
			const string value = "value";

			using (var stream = ReadFile(_file))
			{
				//Act
				new WordMergeFieldFiller().Transform(stream, new Dictionary<string, string>
				{
					{ "Test3:", value }
				});

				//Assert
				var result = ReadDocument(stream);

				Assert.DoesNotContain(value, result);
			}
		}

		[Fact]
		public void FillList()
		{
			using (var stream = ReadFile(_file))
			{
				//Act
				new WordMergeFieldFiller().TransformListValues(stream, _replaceValues);

				//Assert
				var result = ReadDocument(stream);

				Enumerable.ToList(_replaceValues.Keys).ForEach(k => Assert.DoesNotContain(k + ":", result));
				Enumerable.ToList(_replaceValues.Values).ForEach(v => Assert.DoesNotContain(v + ":", result));
			}
		}

		private static Stream ReadFile(string path)
		{
			var stream = new MemoryStream();
			using (var sr = File.Open(path, FileMode.Open, FileAccess.Read))
				sr.CopyTo(stream);

			return stream;
		}

		private static string ReadDocument(Stream stream)
		{
			using (var wordDoc = WordprocessingDocument.Open(stream, false))
			using (var sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
			{
				return sr.ReadToEnd();
			}
		}
	}
}