﻿using System;
using Xunit;

namespace NexCore.Common.UnitTests
{
    public class GuidFactoryTest
    {
        [Fact]
        public void Get()
        {
            using (var context = new GuidFactoryContext(Guid.NewGuid()))
            {
                Assert.Equal(context.Value, GuidFactory.Instance.Next());
            }
        }

        [Fact]
        public void Get_Default()
        {
            Assert.NotEqual(Guid.Empty, GuidFactory.Instance.Next());
        }
    }
}
