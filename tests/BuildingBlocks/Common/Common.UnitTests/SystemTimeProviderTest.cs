﻿using System;
using Xunit;

namespace NexCore.Common.UnitTests
{
    public class SystemTimeProviderTest
    {
        [Fact]
        public void Get()
        {
            using (var time = new SystemTimeContext(DateTime.Now))
            {
                Assert.Equal(time.Now, SystemTimeProvider.Instance.Get());
            }
        }

        [Fact]
        public void Get_Default()
        {
            Assert.Equal(DateTime.UtcNow.ToShortTimeString(), SystemTimeProvider.Instance.Get().ToShortTimeString());
        }
    }
}
