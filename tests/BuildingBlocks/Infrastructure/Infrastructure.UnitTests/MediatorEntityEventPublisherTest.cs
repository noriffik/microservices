﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Infrastructure.UnitTests
{
    public class MediatorEntityEventPublisherTest : IDisposable
    {
        private readonly DbContext _context;

        public MediatorEntityEventPublisherTest()
        {
            var options = new DbContextOptionsBuilder<DbContext>()
                .UseInMemoryDatabase(nameof(MediatorEntityEventPublisherTest))
                .Options;

            _context = new TestUnitOfWorkDbContext(options);
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () =>
                new MediatorEntityEventPublisher(null, new Mock<IMediator>().Object));
        }
        
        [Fact]
        public void Ctor_GivenMediator_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mediator", () => new MediatorEntityEventPublisher(_context, null));
        }
        
        [Fact]
        public async Task Publish()
        {
            //Arrange
            //Setup entities
            var entities = CreateEntities();

            //Setup mediator
            var mediator = new Mock<IMediator>();

            //Setup event publisher
            var publisher = new MediatorEntityEventPublisher(_context, mediator.Object);

            //Add entities to context
            _context.AddRange(entities);

            using (Sequence.Create())
            {
                //Publishes events for each entity
                foreach (var entity in entities)
                {
                    foreach (var entityEvent in entity.EntityEvents)
                    {
                        mediator.Setup(m => m.Publish(entityEvent, CancellationToken.None))
                            .InSequence()
                            .Returns(Task.CompletedTask);
                    }
                }

                //Act
                await publisher.Publish();
            }
        }

        [Fact]
        public async Task Publish_WhenThereAre_NoEntities_ToProcess_DoesNothing()
        {
            //Arrange
            //Setup mediator
            var mediator = new Mock<IMediator>();

            //Setup event publisher
            var publisher = new MediatorEntityEventPublisher(_context, mediator.Object);
            
            //Act
            await publisher.Publish();

            //Assert
            mediator.Verify(m => m.Publish(It.IsAny<IEntityEvent>(), CancellationToken.None), Times.Never);
        }

        [Fact]
        public async Task Publish_SendsEventsAdded_ByEventHandlers()
        {
            //Arrange
            //Setup entities
            var originalEntity = new TestAggregateRoot();
            var affectedEntity = new TestAggregateRoot();

            //Setup events
            var originalEvent = new Mock<IEntityEvent>().Object;
            var consequenceEvent = new Mock<IEntityEvent>().Object;
            var anotherEvent = new Mock<IEntityEvent>().Object;

            //Add original event
            originalEntity.AddEntityEvent(originalEvent);
        
            //Setup mediator
            var mediator = new Mock<IMediator>();

            //Setup event publisher
            var publisher = new MediatorEntityEventPublisher(_context, mediator.Object);

            //Add entities to context
            _context.AddRange(originalEntity);

            mediator.Setup(m => m.Publish(It.IsAny<IEntityEvent>(), CancellationToken.None))
                .Callback<IEntityEvent, CancellationToken>((e, c) =>
                {
                    if (e == originalEvent)
                    {
                        //Original event loads affected entity into the context
                        // ReSharper disable once AccessToDisposedClosure
                        _context.Add(affectedEntity);

                        //Original event affects another entity by generating new consequence event
                        affectedEntity.AddEntityEvent(consequenceEvent);
                        return;
                    }
                    
                    if (e == consequenceEvent)
                    {
                        //Consequence event affects original entity by generating another event
                        originalEntity.AddEntityEvent(anotherEvent);
                    }
                })
                .Returns(Task.CompletedTask)
                .Verifiable();

            //Act
            await publisher.Publish();

            //Assert
            mediator.Verify(m => m.Publish(It.IsAny<IEntityEvent>(), CancellationToken.None), Times.Exactly(3));
        }

        [Fact]
        public async Task Publish_WhenNumberOfEvents_SurpassLimit_Throws()
        {
            //Arrange
            //Setup entities
            var entity = new TestAggregateRoot();

            //Setup events
            var @event = new Mock<IEntityEvent>().Object;

            //Add original event
            entity.AddEntityEvent(@event);

            //Setup mediator
            var mediator = new Mock<IMediator>();

            //Setup event publisher
            var publisher = new MediatorEntityEventPublisher(_context, mediator.Object);

            //Add entities to context
            _context.AddRange(entity);

            var eventCount = 0;

            mediator.Setup(m => m.Publish(It.IsAny<IEntityEvent>(), CancellationToken.None))
                .Callback<IEntityEvent, CancellationToken>((e, c) =>
                    {
                        //Event handler adds save event back to the entities event list
                        if (eventCount > MediatorEntityEventPublisher.EventLimit)
                            return;

                        entity.AddEntityEvent(@event);

                        eventCount++;
                    })
                .Returns(Task.CompletedTask)
                .Verifiable();
            
            //Act
            var exception = await Assert.ThrowsAsync<InvalidOperationException>(async () => await publisher.Publish());

            //Assert
            Assert.Contains(MediatorEntityEventPublisher.EventLimit.ToString(), exception.Message);
            mediator.Verify(m => m.Publish(It.IsAny<IEntityEvent>(), CancellationToken.None), Times.Exactly(MediatorEntityEventPublisher.EventLimit));
        }

        [Fact]
        public async Task Publish_ClearsEntities_Events()
        {
            //Arrange
            //Setup entities
            var entities = CreateEntities();

            //Setup mediator
            var mediator = new Mock<IMediator>();

            //Setup event publisher
            var publisher = new MediatorEntityEventPublisher(_context, mediator.Object);

            //Add entities to context
            _context.AddRange(entities);

            //Act
            await publisher.Publish();

            //Assert
            Assert.Empty(entities.SelectMany(e => e.EntityEvents));
        }

        private static List<IEventful> CreateEntities()
        {
            //Setup entities
            var entities = Enumerable.Range(1, 5)
                .Select(n => new Mock<TestAggregateWithTestIdRoot>(new TestId(n.ToString()))
                {
                    CallBase = true
                })
                .Select(m => m.Object)
                .Concat(Enumerable.Range(1, 3)
                    .Select(n => new Mock<TestAggregateRoot>(n)
                    {
                        CallBase = true
                    })
                    .Select(m => m.Object)
                    .Cast<IEventful>())
                .ToList();

            //Setup entity events
            foreach (var entity in entities)
            {
                Enumerable.Range(0, 3)
                    .Select(n => new Mock<IEntityEvent>().Object)
                    .ToList()
                    .ForEach(e => entity.AddEntityEvent(e));
            }

            return entities;
        }

        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}
