﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Storage;
using Moq;
using Moq.Sequences;
using NexCore.Infrastructure.Utility;
using NexCore.Testing;
using NexCore.Testing.Extensions.Moq;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Infrastructure.UnitTests
{
    public class UnitOfWorkDbContextTest
	{
		private readonly DbContextOptions _options;

		private readonly Mock<IResilientTransaction> _resilientTransaction;

		private readonly List<Mock<IUnitOfWorkDbContextPlugin>> _plugins;
		private readonly List<Mock<IEntityEventPublisher>> _publishers;

		public UnitOfWorkDbContextTest()
		{
			_options = new DbContextOptionsBuilder<DbContext>()
				.UseInMemoryDatabase(nameof(UnitOfWorkDbContextTest))
				.ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
				.Options;
			
			_resilientTransaction = new Mock<IResilientTransaction>();
            
            _publishers = Enumerable.Range(0, 3)
				.Select(n => new Mock<IEntityEventPublisher>())
				.ToList();

            _plugins = Enumerable.Range(0, 3)
                .Select(n => new Mock<IUnitOfWorkDbContextPlugin>())
                .ToList();
        }

        private TestUnitOfWorkDbContext CreateContext()
        {
            var context = new Mock<TestUnitOfWorkDbContext>(_options,
                _publishers.Select(e => e.Object),
                _plugins.Select(p => p.Object))
            {
                CallBase = true
            };

            context.Object.ResilientTransaction = _resilientTransaction.Object;

            return context.Object;
        }

		[Fact]
		public void GetEventPublishers_WhenNotAssigned_ReturnsEmpty()
		{
			using (var context = new TestUnitOfWorkDbContext(_options))
			{
				Assert.Empty(context.EventPublishers);
			}
		}

		[Fact]
		public void GetPlugins_WhenNotAssigned_ReturnsEmpty()
		{
			using (var context = new TestUnitOfWorkDbContext(_options))
			{
				Assert.Empty(context.Plugins);
			}
		}

		[Fact]
		public void Ctor_GivenMediator_IsNull_Throws()
		{
			Assert.Throws<ArgumentNullException>("mediator",
				() => new TestUnitOfWorkDbContext(_options, null));
		}

		[Fact]
		public void Ctor_GivenEventPublishers_IsNull_Throws()
		{
			Assert.Throws<ArgumentNullException>("eventPublishers", () => new TestUnitOfWorkDbContext(_options,
				null,
				new List<IUnitOfWorkDbContextPlugin>()));
		}

		[Fact]
		public void Ctor_GivenPlugins_IsNull_Throws()
		{
			Assert.Throws<ArgumentNullException>("plugins", () => new TestUnitOfWorkDbContext(_options,
				new List<IEntityEventPublisher>(),
				null));
		}

        [Fact]
        public void SetResilientTransaction_GivenNull_SetsDefaultValue()
        {
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                //Act
                context.ResilientTransaction = null;

                //Assert
                Assert.NotNull(context.ResilientTransaction);
            }
        }

        [Fact]
        public async Task Commit_PublishedEvents_BeforeExecutingTransaction()
        {
            //Setup context
            using (var context = CreateContext())
            using (Sequence.Create())
            {
                //Publishes events for each entity
                _publishers.ForEach(p => p.Setup(c => c.Publish())
                    .InSequence()
                    .Returns(Task.CompletedTask));

                //Executes transaction
                // ReSharper disable once AccessToDisposedClosure
                _resilientTransaction.Setup(t => t.ExecuteAsync(context,
                        It.IsAny<Func<DbConnection, IDbContextTransaction, Task>>(), CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await context.Commit(It.IsAny<CancellationToken>());
            }
        }

        [Fact]
        public async Task Commit_DelegatesToPlugins()
        {
            //Arrange
            var connection = new Mock<DbConnection>().Object;
            var transaction = new Mock<IDbContextTransaction>().Object;

            _resilientTransaction.SetupExecuteAsync(connection, transaction);

            using (var context = CreateContext())
            using (Sequence.Create())
            {
                _plugins.ForEach(p =>
                    p.Setup(_ => _.Commit(connection, transaction, CancellationToken.None))
                        .InSequence()
                        .Returns(Task.CompletedTask));

                //Act
                await context.Commit(CancellationToken.None);
            }
        }
    }
}
