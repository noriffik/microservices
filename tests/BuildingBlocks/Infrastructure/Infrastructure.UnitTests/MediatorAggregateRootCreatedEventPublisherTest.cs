﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Infrastructure.UnitTests
{
    public class MediatorAggregateRootCreatedEventPublisherTest
    {
        public class MediatorStub : IMediator
        {
            public readonly List<object> Notifications = new List<object>();

            public Task<TResponse> Send<TResponse>(IRequest<TResponse> request, CancellationToken cancellationToken = new CancellationToken())
            {
                throw new NotSupportedException();
            }

            public Task Publish(object notification, CancellationToken cancellationToken = new CancellationToken())
            {
                Notifications.Add(notification);

                return Task.CompletedTask;
            }

            public Task Publish<TNotification>(TNotification notification, CancellationToken cancellationToken = new CancellationToken()) where TNotification : INotification
            {
                return Publish(notification as object, cancellationToken);
            }
        }
        
        private readonly DbContext _context;

        public MediatorAggregateRootCreatedEventPublisherTest()
        {
            var options = new DbContextOptionsBuilder<DbContext>()
                .UseInMemoryDatabase(nameof(MediatorAggregateRootCreatedEventPublisherTest))
                .Options;

            _context = new TestUnitOfWorkDbContext(options);
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () =>
                new MediatorAggregateRootCreatedEventPublisher(null, new MediatorStub()));
        }
        
        [Fact]
        public void Ctor_GivenMediator_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mediator", () => new MediatorAggregateRootCreatedEventPublisher(_context, null));
        }

        [Fact]
        public async Task Publish()
        {
            //Arrange
            //Setup entities
            var basicEntities = Enumerable.Range(1, 3)
                .Select(n => new TestAggregateRoot(n))
                .Cast<IAggregateRoot>()
                .ToList();

            var complexEntities = Enumerable.Range(1, 3)
                .Select(n => new TestAggregateWithTestIdRoot(new TestId(n.ToString())))
                .Cast<IAggregateRoot<TestId>>()
                .ToList();

            var attached = Enumerable.Range(4, 2)
                .Select(n => new TestAggregateRoot(n))
                .Cast<IAggregateRoot>()
                .ToList();

            //Setup events
            var events = basicEntities.Select(e => e.Id)
                .Select(id => new AggregateRootCreatedEvent<TestAggregateRoot>(id))
                .Cast<object>()
                .Concat(complexEntities.Select(e => e.Id)
                    .Select(id => new AggregateRootCreatedEvent<TestAggregateWithTestIdRoot, TestId>(id)))
                .ToList();
            
            //Setup mediator
            var mediator = new MediatorStub();

            //Setup event publisher
            var publisher = new MediatorAggregateRootCreatedEventPublisher(_context, mediator);

            //Add entities to context
            _context.AddRange(basicEntities);
            _context.AddRange(complexEntities);
            _context.AttachRange(attached);
            _context.Add(new DummyEntity());
            
            //Act
            await publisher.Publish();

            //Assert
            Assert.Equal(events, mediator.Notifications, PropertyComparer<IEnumerable<object>>.Instance);
        }
    }
}
