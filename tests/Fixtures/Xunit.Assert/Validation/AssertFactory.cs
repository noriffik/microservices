﻿using FluentValidation;
using System;
using System.Threading.Tasks;

namespace Xunit.Validation
{
    public class AssertFactory
    {
        public AssertValidation Throws(string propertyName, Action testCode)
        {
            return new AssertValidation(Assert.Throws<ValidationException>(testCode), propertyName);
        }

        public async Task<AssertValidation> ThrowsAsync(string propertyName, Func<Task> testCode)
        {
            var e = await Assert.ThrowsAsync<ValidationException>(testCode);

            return new AssertValidation(e, propertyName);
        }
    }
}
