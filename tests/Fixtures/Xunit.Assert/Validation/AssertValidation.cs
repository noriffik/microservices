﻿using FluentValidation;
using FluentValidation.Results;
using System;
using System.Linq;

namespace Xunit.Validation
{
    public class AssertValidation
    {
        private readonly ValidationException _exception;
        private readonly string _propertyName;

        internal AssertValidation(ValidationException exception, string propertyName)
        {
            _exception = exception;
            _propertyName = propertyName;
        }

        public AssertValidation HasError()
        {
            var error = FindError();

            Assert.True(error != null, $"Does not contain error with given property \"{_propertyName}\"");

            return this;
        }

        private ValidationFailure FindError()
        {
            return _exception.Errors.SingleOrDefault(e => e.PropertyName == _propertyName);
        }
        
        public AssertValidation WithValue(object value)
        {
            HasError();
            Assert.True(value.Equals(FindError().AttemptedValue), $"Does not contain value \"{value}\"");

            return this;
        }

        public AssertValidation WithMessage(string message)
        {
            HasError();
            Assert.True(FindError().ErrorMessage.Contains(message, StringComparison.InvariantCultureIgnoreCase), $"Does not contain message \"{message}\"");

            return this;
        }
    }
}
