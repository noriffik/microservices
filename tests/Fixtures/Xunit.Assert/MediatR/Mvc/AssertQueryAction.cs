﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace Xunit.MediatR.Mvc
{
    public class AssertQueryAction
    {
        private readonly Type _controllerType;
        private readonly Type _commandType;

        public AssertQueryAction(Type controllerType, Type commandType)
        {
            _controllerType = controllerType;
            _commandType = commandType;
        }
    }
}
