﻿using System.Diagnostics.CodeAnalysis;
using NexCore.Domain;

namespace NexCore.Testing
{
    public class TestAggregateWithTestIdRoot : Entity<TestId>, IAggregateRoot<TestId>
    {
        [SuppressMessage("ReSharper", "UnusedMember.Local", Justification = "Required by EF Core.")]
        private TestAggregateWithTestIdRoot()
        {
        }

        public TestAggregateWithTestIdRoot(TestId id) : base(id)
        {
        }
    }
}