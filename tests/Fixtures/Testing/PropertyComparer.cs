﻿using NexCore.Domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Testing
{
    public class PropertyComparerOptions
    {
        private CollectionComparison _collectionComparison;

        public CollectionComparison CollectionComparison
        {
            get => _collectionComparison;
            set
            {
                if (value != null) _collectionComparison = value;
            }
        }

        public PropertyComparerOptions()
        {
            CollectionComparison = CollectionComparison.Ordered;
        }
    }

    public class PropertyComparer<T> : PropertyComparer, IEqualityComparer<T>
    {
        public static PropertyComparer<T> Instance { get; } = new PropertyComparer<T>();

        public PropertyComparer()
        {
        }

        public PropertyComparer(PropertyComparerOptions options) : base(options)
        {
        }

        public bool Equals(T x, T y)
        {
            return base.Equals(x, y);
        }

        public int GetHashCode(T obj)
        {
            return base.GetHashCode(obj);
        }
    }

    public class PropertyComparer : IEqualityComparer
    {
        public PropertyComparerOptions Options { get; }

        private const int RecursionLimit = 3;

        private Type _recursiveType;
        private int _recursiveDepth;

        public PropertyComparer() : this(new PropertyComparerOptions())
        {
        }

        public PropertyComparer(PropertyComparerOptions options)
        {
            Options = options;
        }

        public new bool Equals(object x, object y)
        {
            if (ReferenceEquals(x, y))
                return true;

            if (x == null || y == null)
                return false;

            if (x is IEnumerable && y is IEnumerable)
                return SequenceEqual(x, y);

            if (x.GetType() != y.GetType())
                return false;

            if (x.GetType().IsPrimitive || x is decimal || x.GetType().IsEnum)
                return x.Equals(y);

            return ObjectEqual(x, y);
        }

        private bool ObjectEqual(object x, object y)
        {
            if (HasReachedRecursiveLimit(x.GetType())) return true;

            return x.GetType()
                .GetProperties()
                .Select(p => p.GetMethod)
                .Where(p => !p.IsStatic)
                .Where(m => m.GetParameters().Length == 0)
                .All(m => Equals(m.Invoke(x, null), m.Invoke(y, null)));
        }

        private bool HasReachedRecursiveLimit(Type type)
        {
            if (_recursiveType == type)
            {
                if (_recursiveDepth > RecursionLimit)
                    return true;

                _recursiveDepth++;
                return false;
            }

            _recursiveType = type;
            _recursiveDepth = 0;

            return false;
        }

        private bool SequenceEqual(object x, object y)
        {
            return ReferenceEquals(x, y)
                   || x != null
                   && Options.CollectionComparison.Compare((IEnumerable)x, (IEnumerable)y, new PropertyComparer());
        }

        public int GetHashCode(object obj)
        {
            throw new NotSupportedException();
        }
    }
}
