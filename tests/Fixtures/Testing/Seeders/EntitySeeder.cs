﻿using AutoFixture;
using AutoFixture.Dsl;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Testing.Seeders
{
    public class EntitySeeder<TEntity> : EntitySeeder where TEntity : Entity, IAggregateRoot
    {
        public EntitySeeder(DbContext context) : base(context)
        {
        }

        public EntitySeeder(DbContext context, IFixture fixture) : base(context, fixture)
        {
        }

        public virtual Task<TEntity> Add() => Add<TEntity>();

        public virtual Task<TEntity> Add(TEntity entity) => Add<TEntity>(entity);

        public virtual Task<TEntity> Add(int id) => Add<TEntity>(id);

        public virtual Task<TEntity> Add(object parameters) => Add<TEntity>(parameters);

        public virtual Task<TEntity> Add(int id, object parameters) => Add<TEntity>(id, parameters);

        public async Task<TEntity[]> AddRange(int count)
        {
            var entities = new List<TEntity>(count);
            for (var i = 0; i < count; i++)
                entities.Add(await Add());

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange(params TEntity[] entities)
        {
            foreach (var entity in entities)
                await Add(entity);

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange(params object[] parameters)
        {
            var entities = new List<TEntity>(parameters.Length);
            foreach (var p in parameters)
                entities.Add(await Add(p));

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange(int count, object parameters)
        {
            var entities = new List<TEntity>(count);
            for (var i = 0; i < count; i++)
                entities.Add(await Add(parameters));

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange(int[] ids)
        {
            var entities = new List<TEntity>(ids.Length);
            foreach (var id in ids)
                entities.Add(await Add(id));

            return entities.ToArray();
        }

        public Task<TEntity[]> AddRange(int from, int to) => AddRange<TEntity>(from, to);

        public Task<TEntity[]> AddRange(int from, int to, object parameters) => AddRange<TEntity>(from, to, parameters);

        public TEntity Create() => Create<TEntity>();

        public TEntity Create(int id) => Create<TEntity>(id);

        public TEntity Create(object parameters) => Create<TEntity>(parameters);

        public TEntity Create(int id, object parameters) => Create<TEntity>(id, parameters);

        public bool Has(int id) => Has<TEntity>(id);

        public TEntity Find(int id) => Find<TEntity>(id);

        public IEnumerable<TEntity> Get() => Get<TEntity>();
    }

    public class EntitySeeder<TEntity, TId> : EntitySeeder where TEntity : Entity<TId>, IAggregateRoot<TId>
        where TId : EntityId
    {
        public EntitySeeder(DbContext context) : base(context)
        {
        }

        public EntitySeeder(DbContext context, IFixture fixture) : base(context, fixture)
        {
        }

        public virtual Task<TEntity> Add() => Add<TEntity, TId>();

        public virtual Task<TEntity> Add(TEntity entity) => Add<TEntity, TId>(entity);

        public virtual Task<TEntity> Add(TId id) => Add<TEntity, TId>(id);

        public virtual Task<TEntity> Add(object parameters) => Add<TEntity, TId>(parameters);

        public virtual Task<TEntity> Add(TId id, object parameters) => Add<TEntity, TId>(id, parameters);

        public async Task<TEntity[]> AddRange(int count)
        {
            var entities = new List<TEntity>(count);
            for (var i = 0; i < count; i++)
                entities.Add(await Add());

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange(params TEntity[] entities)
        {
            foreach (var entity in entities)
                await Add(entity);

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange(params object[] parameters)
        {
            var entities = new List<TEntity>(parameters.Length);
            foreach (var p in parameters)
                entities.Add(await Add(p));

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange(int count, object parameters)
        {
            var entities = new List<TEntity>(count);
            for (var i = 0; i < count; i++)
                entities.Add(await Add(parameters));

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange(TId[] ids)
        {
            var entities = new List<TEntity>(ids.Length);
            foreach (var id in ids)
                entities.Add(await Add(id));

            return entities.ToArray();
        }

        public TEntity Create() => Create<TEntity, TId>();

        public TEntity Create(TId id) => Create<TEntity, TId>(id);

        public TEntity Create(object parameters) => Create<TEntity, TId>(parameters);

        public TEntity Create(TId id, object parameters) => Create<TEntity, TId>(id, parameters);

        public bool Has(TId id) => Has<TEntity, TId>(id);

        public TEntity Find(TId id) => Find<TEntity, TId>(id);

        public IEnumerable<TEntity> Get() => Get<TEntity, TId>();
    }

    public class EntitySeeder : IDisposable
    {
        private readonly DbContext _context;

        private IFixture _fixture;

        protected readonly Random Random = new Random();

        public IFixture Fixture
        {
            get
            {
                if (_fixture != null)
                    return _fixture;

                _fixture = new Fixture();
                _fixture.Customizations.Add(new NumericSequenceGenerator());

                return _fixture;
            }
        }

        public EntitySeeder(DbContext context)
        {
            _context = context;
        }

        public EntitySeeder(DbContext context, IFixture fixture)
        {
            _context = context;
            _fixture = fixture;
        }

        //Methods for typeof(TId) == some class
        public virtual async Task<TEntity> Add<TEntity, TId>(TEntity entity)
            where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId
        {
            await _context.AddAsync(entity);

            return entity;
        }

        public Task<TEntity> Add<TEntity, TId>()
            where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId =>
            Add<TEntity, TId>(Create<TEntity, TId>());

        public Task<TEntity> Add<TEntity, TId>(TId id)
            where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId =>
            Add<TEntity, TId>(Create<TEntity, TId>(id));

        public Task<TEntity> Add<TEntity, TId>(object parameters)
            where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId =>
         Add<TEntity, TId>(Create<TEntity, TId>(parameters));

        public Task<TEntity> Add<TEntity, TId>(TId id, object parameters)
            where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId =>
            Add<TEntity, TId>(Create<TEntity, TId>(id, parameters));

        public async Task<TEntity[]> AddRange<TEntity, TId>(int count)
            where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId
        {
            var entities = new List<TEntity>(count);
            for (var i = 0; i < count; i++)
                entities.Add(await Add<TEntity, TId>());

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange<TEntity, TId>(params TEntity[] entities)
            where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId
        {
            foreach (var entity in entities)
                await Add<TEntity, TId>(entity);

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange<TEntity, TId>(params object[] parameters)
            where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId
        {
            var entities = new List<TEntity>(parameters.Length);
            foreach (var p in parameters)
                entities.Add(await Add<TEntity, TId>(p));

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange<TEntity, TId>(int count, object parameters)
            where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId
        {
            var entities = new List<TEntity>(count);
            for (var i = 0; i < count; i++)
                entities.Add(await Add<TEntity, TId>(parameters));

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange<TEntity, TId>(TId[] ids)
            where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId
        {
            var entities = new List<TEntity>(ids.Length);
            foreach (var id in ids)
                entities.Add(await Add<TEntity, TId>(id));

            return entities.ToArray();
        }

        public async Task<TEntity> FindOrAdd<TEntity, TId>(TId id) where TId : EntityId
            where TEntity : Entity<TId>, IAggregateRoot<TId>
        {
            return Get<TEntity, TId>().SingleOrDefault(e => Equals(e.Id, id)) ?? await Add<TEntity, TId>(id);
        }

        public TEntity Create<TEntity, TId>()
            where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId => Fixture.Create<TEntity>();

        public TEntity Create<TEntity, TId>(TId id)
            where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId =>
            Fixture.Construct<TEntity, TId>(id);

        public TEntity Create<TEntity, TId>(object parameters)
            where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId =>
            Fixture.Construct<TEntity>(parameters);

        public TEntity Create<TEntity, TId>(TId id, object parameters)
            where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId =>
            Fixture.Construct<TEntity, TId>(id, parameters);

        public bool Has<TEntity, TId>(TId id) where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId
        {
            var has = Get<TEntity, TId>().SingleOrDefault(e => Equals(e.Id, id)) != null;

            return has;
        }

        public TEntity Find<TEntity, TId>(TId id) where TEntity : Entity<TId>, IAggregateRoot<TId> where TId : EntityId
        {
            var entity = Get<TEntity, TId>().SingleOrDefault(e => Equals(e.Id, id));
            if (entity == null)
                throw new ArgumentException("Entity with given id was not found", nameof(id));

            return entity;
        }

        public IEnumerable<TEntity> Get<TEntity, TId>() where TEntity : Entity<TId>, IAggregateRoot<TId>
            where TId : EntityId =>
            _context.ChangeTracker.Entries<TEntity>().Select(e => e.Entity).Concat(
                _context.Set<TEntity>()
                    .ToList()
                    .AsReadOnly())
                    .Distinct();

        //Methods for typeof(TId) == int
        public virtual async Task<TEntity> Add<TEntity>(TEntity entity) where TEntity : Entity, IAggregateRoot
        {
            await _context.AddAsync(entity);

            return entity;
        }

        public Task<TEntity> Add<TEntity>() where TEntity : Entity, IAggregateRoot => Add(Create<TEntity>());

        public Task<TEntity> Add<TEntity>(int id) where TEntity : Entity, IAggregateRoot =>
            Add(Create<TEntity>(id));

        public Task<TEntity> Add<TEntity>(object parameters) where TEntity : Entity, IAggregateRoot =>
            Add(Create<TEntity>(parameters));

        public Task<TEntity> Add<TEntity>(int id, object parameters) where TEntity : Entity, IAggregateRoot =>
            Add(Create<TEntity>(id, parameters));

        public async Task<TEntity[]> AddRange<TEntity>(int count)
            where TEntity : Entity, IAggregateRoot
        {
            var entities = new List<TEntity>(count);
            for (var i = 0; i < count; i++)
                entities.Add(await Add<TEntity>());

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange<TEntity>(params TEntity[] entities)
            where TEntity : Entity, IAggregateRoot
        {
            foreach (var entity in entities)
                await Add(entity);

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange<TEntity>(params object[] parameters)
            where TEntity : Entity, IAggregateRoot
        {
            var entities = new List<TEntity>(parameters.Length);
            foreach (var p in parameters)
                entities.Add(await Add<TEntity>(p));

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange<TEntity>(int count, object parameters)
            where TEntity : Entity, IAggregateRoot
        {
            var entities = new List<TEntity>(count);
            for (var i = 0; i < count; i++)
                entities.Add(await Add<TEntity>(parameters));

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange<TEntity>(int[] ids)
            where TEntity : Entity, IAggregateRoot
        {
            var entities = new List<TEntity>(ids.Length);
            foreach (var id in ids)
                entities.Add(await Add<TEntity>(id));

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange<TEntity>(int from, int to)
            where TEntity : Entity, IAggregateRoot
        {
            var entities = new List<TEntity>(to - from + 1);
            for (var id = from; id <= to; id++)
                entities.Add(await Add<TEntity>(id));

            return entities.ToArray();
        }

        public async Task<TEntity[]> AddRange<TEntity>(int from, int to, object parameters)
            where TEntity : Entity, IAggregateRoot
        {
            var entities = new List<TEntity>(to - from + 1);
            for (var id = from; id <= to; id++)
                entities.Add(await Add<TEntity>(id, parameters));

            return entities.ToArray();
        }

        public async Task<TEntity> FindOrAdd<TEntity>(int id) where TEntity : Entity, IAggregateRoot
        {
            return Get<TEntity>().SingleOrDefault(e => Equals(e.Id, id)) ?? await Add<TEntity>(id);
        }

        public T Create<T>() => Fixture.Create<T>();

        public IEnumerable<T> CreateMany<T>(int count) => Fixture.CreateMany<T>(count);

        public TEntity Create<TEntity>(int id) where TEntity : Entity, IAggregateRoot =>
            Fixture.Construct<TEntity>(id);

        public TEntity Create<TEntity>(object parameters) where TEntity : Entity, IAggregateRoot =>
            Fixture.Construct<TEntity>(parameters);

        public TEntity Create<TEntity>(int id, object parameters) where TEntity : Entity, IAggregateRoot =>
            Fixture.Construct<TEntity>(id, parameters);

        public ICustomizationComposer<T> Build<T>() => Fixture.Build<T>();

        public bool Has<TEntity>(int id) where TEntity : Entity, IAggregateRoot
        {
            var has = Get<TEntity>().SingleOrDefault(e => Equals(e.Id, id)) != null;

            return has;
        }

        public TEntity Find<TEntity>(int id) where TEntity : Entity, IAggregateRoot
        {
            var entity = Get<TEntity>().SingleOrDefault(e => Equals(e.Id, id));
            if (entity == null)
                throw new ArgumentException("Entity with given id was not found", nameof(id));

            return entity;
        }

        public IEnumerable<TEntity> Get<TEntity>() where TEntity : Entity =>
            _context.ChangeTracker.Entries<TEntity>().Select(e => e.Entity).Concat(
            _context.Set<TEntity>()
                .ToList()
                .AsReadOnly())
                .Distinct();

        public virtual async Task SeedAsync()
        {
            await _context.SaveChangesAsync(CancellationToken.None);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
