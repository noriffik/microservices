﻿using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;

namespace NexCore.Testing.Functional.Auth
{
    public static class ApplicationBuilderExtensions
    {
        public static AuthenticationBuilder AddTestAuthentication(this AuthenticationBuilder builder,
            string authenticationScheme)
        {
            return builder.AddScheme<TestAuthenticationSchemeOptions, TestAuthenticationHandler>(authenticationScheme,
                options =>
                {
                    options.Identity.AddClaim(new Claim(options.Identity.RoleClaimType, "Administrator"));
                });
        }
    }
}