﻿using NexCore.Domain;

namespace NexCore.Testing
{
    public class TestAggregateRoot : Entity, IAggregateRoot
    {
        public TestAggregateRoot()
        {

        }

        public TestAggregateRoot(int id)
        {
            Id = id;
        }
    }
}
