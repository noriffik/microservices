﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NexCore.Infrastructure;
using System.Collections.Generic;

namespace NexCore.Testing
{
    public class TestUnitOfWorkDbContext : UnitOfWorkDbContext
    {
        private class TestAggregateWithTestIdRootTypeConfiguration : IEntityTypeConfiguration<TestAggregateWithTestIdRoot>
        {
            public void Configure(EntityTypeBuilder<TestAggregateWithTestIdRoot> builder)
            {
                builder.Property(x => x.Id)
                    .HasConversion(x => x.Value, x => x)
                    .HasColumnName(typeof(TestAggregateWithTestIdRoot).Name + "Id")
                    .IsRequired();
            }
        }

        public DbSet<TestAggregateRoot> TestAggregateRoots { get; set; }

        public DbSet<TestAggregateWithTestIdRoot> TestAggregateWithTestIdRoots { get; set; }
        
        public DbSet<DummyEntity> DummyEntities { get; set; }

        //This constructor is reserved for use with dotnet utility
        //DO NOT use it in code
        public TestUnitOfWorkDbContext(DbContextOptions options) : base(options)
        {
        }

        public TestUnitOfWorkDbContext(DbContextOptions options, IMediator mediator)
            : base(options, mediator)
        {
        }

        public TestUnitOfWorkDbContext(DbContextOptions options, IEnumerable<IEntityEventPublisher> eventPublishers, IEnumerable<IUnitOfWorkDbContextPlugin> plugins)
            : base(options, eventPublishers, plugins)
        {
        }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new TestAggregateWithTestIdRootTypeConfiguration());
        }
    }
}
