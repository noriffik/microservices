﻿using NexCore.Domain;

namespace NexCore.Testing
{
    public class TestDomainEvent : IEntityEvent
    {
        public int Id { get; set; }
    }
}
