﻿using NexCore.Domain;

namespace NexCore.Testing
{
    public class TestId : EntityId
    {
        public TestId(string value) : base(value)
        {
        }

        public static implicit operator string(TestId id)
        {
            return id?.Value;
        }

        public static implicit operator TestId(string value)
        {
            return value == null ? null : new TestId(value);
        }
    }
}
