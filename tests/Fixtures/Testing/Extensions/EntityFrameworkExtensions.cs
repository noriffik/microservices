﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Linq;

namespace NexCore.Testing.Extensions
{
    public static class EntityFrameworkExtensions
    {
        public static void SetAutoIncrementStep(this ModelBuilder modelBuilder, long step)
        {
            var keys = modelBuilder.Model
                .GetEntityTypes()
                .Select(t => t.FindPrimaryKey())
                .Where(k => k != null)
                .SelectMany(k => k.Properties)
                .Where(p => p.ClrType == typeof(int) && p.ValueGenerated != ValueGenerated.Never);

            foreach (var key in keys)
                key.SetValueGeneratorFactory((p, t) => new TestNumericValueGenerator<int>(step));
        }
    }
}
