﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Moq;
using Moq.Language.Flow;
using NexCore.Infrastructure.Utility;
using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace NexCore.Testing.Extensions.Moq
{
    public static class ResilientTransactionExtensions
    {
        public static IReturnsResult<IResilientTransaction> SetupExecuteAsync(this Mock<IResilientTransaction> transaction)
        {
            var connection = new Mock<DbConnection>().Object;
            var contextTransaction = new Mock<IDbContextTransaction>().Object;

            return SetupExecuteAsync(transaction, connection, contextTransaction);
        }

        public static IReturnsResult<IResilientTransaction> SetupExecuteAsync(this Mock<IResilientTransaction> transaction, DbConnection connection, IDbContextTransaction contextTransaction)
        {
            return transaction.Setup(t => t.ExecuteAsync(It.IsAny<DbContext>(),
                    It.IsAny<Func<DbConnection, IDbContextTransaction, Task>>(), It.IsAny<CancellationToken>()))
                .Callback<DbContext, Func<DbConnection, IDbContextTransaction, Task>, CancellationToken>(
                    async (context, action, cancellationToken) =>
                    {
                        await context.SaveChangesAsync(cancellationToken);
                        await action(connection, contextTransaction);
                    })
                .Returns(Task.CompletedTask);
        }
    }
}
