﻿using NexCore.Domain;
using System;
using System.Linq.Expressions;

namespace NexCore.Testing
{
    public class TestSpecification<T> : Specification<T>
    {
        public T Against { get; }

        public bool IsSatisfied { get; }

        public TestSpecification(T against, bool isSatisfied)
        {
            Against = against;
            IsSatisfied = isSatisfied;
        }

        public override Expression<Func<T, bool>> ToExpression()
        {
            if (IsSatisfied)
                return obj => Equals(obj, Against);

            return obj => Equals(obj, Against) && true == false;
        }

        public override string Description => $"Object {Against} must {(IsSatisfied ? "" : "not ")} satisfy";
    }
}
