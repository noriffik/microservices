﻿using AutoFixture.Kernel;
using NexCore.Domain;
using System;
using System.Linq;
using System.Reflection;

namespace NexCore.Testing.Fixtures.SpecimenBuilders.Domain
{
    public class EntityConstructorSpecimenBuilder<TEntity> : ConstructorSpecimenBuilder<TEntity> where TEntity : IAggregateRoot
    {
        private const string IdParamName = "id";

        public EntityConstructorSpecimenBuilder(int id)
        {
            AddParameter(IdParamName, id);
        }

        public EntityConstructorSpecimenBuilder(object parameters) : base(parameters)
        {
        }

        public EntityConstructorSpecimenBuilder(int id, object parameters) : base(parameters)
        {
            AddParameter(IdParamName, id);
        }

        protected override object GetValue(ISpecimenContext context, ParameterInfo parameter)
        {
            return Parameters.ContainsKey(parameter.Name)
                ? Parameters[parameter.Name]
                : parameter.Name == IdParamName
                    ? 0
                    : context.Resolve(parameter.ParameterType);
        }

        protected override ConstructorInfo SelectConstructor(Type type)
        {
            var constructors = type.GetConstructors(BindingFlags.Instance | BindingFlags.Public);
            foreach (var constructor in constructors)
            {
                if (constructor.GetParameters().Any(p => p.Name == IdParamName))
                    return constructor;
            }

            return base.SelectConstructor(type);
        }
    }

    public class EntityConstructorSpecimenBuilder<TEntity, TId> : ConstructorSpecimenBuilder<TEntity>
        where TEntity : IAggregateRoot<TId>
        where TId : EntityId
    {
        private const string IdParamName = "id";

        public EntityConstructorSpecimenBuilder(TId id)
        {
            AddParameter(IdParamName, id);
        }

        public EntityConstructorSpecimenBuilder(object parameters) : base(parameters)
        {
        }

        public EntityConstructorSpecimenBuilder(TId id, object parameters) : base(parameters)
        {
            AddParameter(IdParamName, id);
        }

        protected override object GetValue(ISpecimenContext context, ParameterInfo parameter)
        {
            return Parameters.ContainsKey(parameter.Name)
                ? Parameters[parameter.Name]
                : parameter.Name == IdParamName
                    ? 0
                    : context.Resolve(parameter.ParameterType);
        }

        protected override ConstructorInfo SelectConstructor(Type type)
        {
            var constructors = type.GetConstructors(BindingFlags.Instance | BindingFlags.Public);
            foreach (var constructor in constructors)
            {
                if (constructor.GetParameters().Any(p => p.Name == IdParamName))
                    return constructor;
            }

            return base.SelectConstructor(type);
        }
    }
}
