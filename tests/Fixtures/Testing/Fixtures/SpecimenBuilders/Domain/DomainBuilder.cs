﻿using AutoFixture.Kernel;
using NexCore.Testing.Fixtures.SpecimenBuilders.Numbers;
using NexCore.Testing.Fixtures.SpecimenBuilders.Strings;

namespace NexCore.Testing.Fixtures.SpecimenBuilders.Domain
{
    public class DomainBuilder : CompositeSpecimenBuilder
    {
        public DomainBuilder()
            : base(
                new NumericStringGenerator(),
                new AlphaNumericStringGenerator(),
                new AlphaNumericSequentialStringGenerator(),
                new NumberRangeGenerator(),
                new YearGenerator(),
                new PeriodGenerator())
        {
        }
    }
}
