﻿using AutoFixture.Kernel;
using NexCore.Domain;
using NexCore.Testing.Fixtures.SpecimenBuilders.Numbers;
using System;

namespace NexCore.Testing.Fixtures.SpecimenBuilders.Domain
{
    public class YearGenerator : ISpecimenBuilder
    {
        private static readonly NumberRange YearRange = new NumberRange(DateTime.MinValue.Year, DateTime.MaxValue.Year);

        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type;
            if (type == null || type != typeof(Year))
                return new NoSpecimen();
            
            return new Year((int) context.Resolve(YearRange));
        }
    }
}
