﻿using AutoFixture.Kernel;
using NexCore.Domain;
using NexCore.Testing.Fixtures.SpecimenBuilders.Strings;
using System;

namespace NexCore.Testing.Fixtures.SpecimenBuilders.Domain
{
    public abstract class SequentialEntityIdGenerator<TId> : ISpecimenBuilder where TId : EntityId
    {
        private readonly int _length;
        private readonly Func<string, TId> _idParser;
        private string _lastId = "";

        protected SequentialEntityIdGenerator(int length, Func<string, TId> idParser)
        {
            _length = length;
            _idParser = idParser;
        }

        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type;
            if (type == null || type != typeof(TId))
                return new NoSpecimen();

            _lastId = (string)context.Resolve(new AlphaNumericSequentialString(_length, _lastId));

            return _idParser(_lastId);
        }
    }
}