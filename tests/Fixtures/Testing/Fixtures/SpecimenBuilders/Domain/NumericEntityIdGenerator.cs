﻿using System;
using AutoFixture.Kernel;
using NexCore.Domain;
using NexCore.Testing.Fixtures.SpecimenBuilders.Strings;

namespace NexCore.Testing.Fixtures.SpecimenBuilders.Domain
{
    public abstract class NumericEntityIdGenerator<TId> : ISpecimenBuilder where TId : EntityId
    {
        private readonly int _length;
        private readonly Func<string, TId> _idParser;

        protected NumericEntityIdGenerator(int length, Func<string, TId> idParser)
        {
            _length = length;
            _idParser = idParser;
        }

        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type;
            if (type == null || type != typeof(TId))
                return new NoSpecimen();

            var numbers = new SpecimenContext(new NumericStringGenerator());

            var numericString = (string)numbers.Resolve(new NumericString(_length));

            return _idParser(numericString);
        }
    }
}