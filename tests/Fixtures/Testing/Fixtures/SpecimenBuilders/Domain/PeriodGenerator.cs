﻿using AutoFixture;
using AutoFixture.Kernel;
using NexCore.Domain;
using System;

namespace NexCore.Testing.Fixtures.SpecimenBuilders.Domain
{
    public class PeriodGenerator : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {  
            var type = request as Type;
            if (type == null || type != typeof(Period))
                return new NoSpecimen();

            var from = context.Create<DateTime>();
            var to = from.AddDays(100);

            return new Period(from, to);
        }
    }
}
