﻿using AutoFixture.Kernel;
using System;
using System.Linq;

namespace NexCore.Testing.Fixtures.SpecimenBuilders.Strings
{
    public class AlphaNumericSequentialStringGenerator : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            if (!(request is AlphaNumericSequentialString stringRequest))
                return new NoSpecimen();

            var blank = FillBlank(stringRequest.BeginningFrom, stringRequest.Length);
            if (stringRequest.BeginningFrom.Length < stringRequest.Length)
                return new string(blank.ToArray());

            var hasGenerated = false;
            for (var i = blank.Length - 1; i >= 0; i--)
            {
                if (hasGenerated)
                    continue;

                hasGenerated = NextChar(blank[i], out blank[i]);
            }
            
            var result = new string(blank.ToArray());
            if (result == stringRequest.BeginningFrom)
                throw new InvalidOperationException("Available combinations for given request are exhausted");
            
            return result;
        }

        private static char[] FillBlank(string with, int length)
        {
            var blank = CreateBlank(length);

            for (var i = 0; i < with.Length; i++)
                blank[i] = with[i];
            
            return blank;
        }

        private static char[] CreateBlank(int length)
        {
            var blank = new char[length];

            for (var i = 0; i < length; i++)
                blank[i] = '0';

            return blank;
        }

        private bool NextChar(char from, out char to)
        {
            if (from == 'Z')
            {
                to = '0';
                return false;
            }
            
            to = from >= '9' && from < 'A' ? 'A' : (char) (from + 1);
            return true;
        }
    }
}