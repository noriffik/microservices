﻿using System;

namespace NexCore.Testing.Fixtures.SpecimenBuilders.Strings
{
    public class AlphaNumericSequentialString
    {
        public int Length { get; }

        public string BeginningFrom { get; }

        public AlphaNumericSequentialString(int length, string beginningFrom)
        {
            if (length <= 0)
                throw new ArgumentOutOfRangeException(nameof(length));

            Length = length;
            BeginningFrom = beginningFrom ?? throw new ArgumentNullException(nameof(beginningFrom));
        }
    }
}