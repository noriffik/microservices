﻿using AutoFixture.Kernel;
using System;
using System.Linq;

namespace NexCore.Testing.Fixtures.SpecimenBuilders.Strings
{
    public class AlphaNumericStringGenerator : ISpecimenBuilder
    {
        private readonly Random _random = new Random();

        public object Create(object request, ISpecimenContext context)
        {
            if (!(request is AlphaNumericString stringRequest))
                return new NoSpecimen();

            return new string(Enumerable.Range(0, stringRequest.Length)
                .Select(n => NextChar())
                .ToArray());
        }

        private char NextChar()
        {
            var result = _random.Next(48, 90);

            if (result > 48 && result < 65)
                return (char) (result - 48 + 65);

            return (char) result;
        }
    }
}