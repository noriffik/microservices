﻿using AutoFixture.Kernel;
using System;
using System.Linq;

namespace NexCore.Testing.Fixtures.SpecimenBuilders.Strings
{
    public class NumericStringGenerator : ISpecimenBuilder
    {
        private readonly Random _random = new Random();

        public object Create(object request, ISpecimenContext context)
        {
            if (!(request is NumericString stringRequest))
                return new NoSpecimen();
            
            return new string(Enumerable.Range(0, stringRequest.Length)
                .Select(n => NextChar())
                .ToArray());
        }

        private char NextChar()
        {
            return (char) _random.Next(48, 57);
        }
    }
}