﻿using System;

namespace NexCore.Testing.Fixtures.SpecimenBuilders.Strings
{
    public class AlphaNumericString
    {
        public int Length { get; }

        public AlphaNumericString(int length)
        {
            if (length <= 0)
                throw new ArgumentOutOfRangeException(nameof(length));

            Length = length;
        }
    }
}