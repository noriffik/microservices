﻿using System;

namespace NexCore.Testing.Fixtures.SpecimenBuilders.Strings
{
    public class NumericString
    {
        public NumericString(int length)
        {
            if (length <= 0)
                throw new ArgumentOutOfRangeException(nameof(length));

            Length = length;
        }

        public int Length { get; }
    }
}