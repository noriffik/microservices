﻿using System;

namespace NexCore.Testing.Fixtures.SpecimenBuilders.Numbers
{
    public class NumberRange
    {
        public int From { get; }

        public int To { get; }

        public NumberRange(int from, int to)
        {
            if (from > to)
                throw new ArgumentOutOfRangeException(nameof(from));

            From = from;
            To = to;
        }
    }
}