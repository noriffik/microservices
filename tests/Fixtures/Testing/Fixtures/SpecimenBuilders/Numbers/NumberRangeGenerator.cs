﻿using AutoFixture.Kernel;
using System;

namespace NexCore.Testing.Fixtures.SpecimenBuilders.Numbers
{
    public class NumberRangeGenerator : ISpecimenBuilder
    {
        private readonly Random _random = new Random();

        public object Create(object request, ISpecimenContext context)
        {
            if (!(request is NumberRange range))
                return new NoSpecimen();

            return _random.Next(range.From, range.To);
        }
    }
}
