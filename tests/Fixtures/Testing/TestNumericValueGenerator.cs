﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using System;
using System.Globalization;
using System.Threading;

namespace NexCore.Testing
{
    public class TestNumericValueGenerator<TValue> : ValueGenerator<TValue>
    {
        private readonly long _step;
        private long _current;

        public TestNumericValueGenerator() : this(1000)
        {
        }

        public TestNumericValueGenerator(long step)
        {
            _step = step;
        }
        
        public override TValue Next(EntityEntry entry)
        {
            return (TValue) Convert.ChangeType(Interlocked.Add(ref _current, _step), typeof (TValue), CultureInfo.InvariantCulture);
        }

        public override bool GeneratesTemporaryValues => false;
    }
}
