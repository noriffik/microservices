﻿using NexCore.Domain;
using System.Collections.Generic;

namespace NexCore.Testing
{
    public class TestValueObject : ValueObject
    {
        public string Value { get; set; }

        protected override IEnumerable<object> GetValues()
        {
            yield return Value;
        }
    }
}
