﻿using AutoFixture.Kernel;
using Moq;
using NexCore.Testing.Fixtures.SpecimenBuilders.Strings;
using Xunit;

namespace NexCore.Testing.UnitTests.Fixtures.SpecimenBuilders.Strings
{
    public class AlphaNumericSequentialStringGeneratorTest
    {
        [Theory]
        [InlineData("", "000")]
        [InlineData("A", "A00")]
        [InlineData("AA", "AA0")]
        [InlineData("AA9", "AAA")]
        [InlineData("000", "001")]
        [InlineData("100", "101")]
        [InlineData("AAB", "AAC")]
        [InlineData("ACZ", "AD0")]
        [InlineData("DZZ", "E00")]
        [InlineData("00Y", "00Z")]
        [InlineData("00Z", "010")]
        [InlineData("NJ", "NJ0")]
        [InlineData("NJA", "NJB")]
        [InlineData("NJAE", "NJAF")]
        public void Create(string from, string expected)
        {
            //Arrange
            var context = new Mock<ISpecimenContext>();
            var builder = new AlphaNumericSequentialStringGenerator();
            var request = new AlphaNumericSequentialString(expected.Length, from);

            //Act
            var result = builder.Create(request, context.Object);

            //Assert
            var actual = Assert.IsType<string>(result);
            Assert.NotEmpty(actual);
            Assert.Equal(request.Length, actual.Length);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Create_GivenRequest_IsNull_ReturnsNoSpecimen()
        {
            //Arrange
            var context = new Mock<ISpecimenContext>();
            var builder = new AlphaNumericSequentialStringGenerator();

            //Act
            var result = builder.Create(null, context.Object);

            //Assert
            Assert.IsType<NoSpecimen>(result);
        }
    }
}
