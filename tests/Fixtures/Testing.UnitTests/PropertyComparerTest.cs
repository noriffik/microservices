﻿using AutoFixture;
using AutoFixture.Kernel;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.Testing.UnitTests
{
    public class PropertyComparerTest
    {
        public class Subject : ICloneable
        {
            public string ValueType { get; set; }

            public Subject ReferenceType { get; set; }

            public IEnumerable<Subject> Collection { get; set; }

            public IDictionary<string, Subject> Dictionary { get; set; }

            public object Clone()
            {
                return new Subject
                {
                    ValueType = ValueType,
                    ReferenceType = ReferenceType?.Clone<Subject>(),
                    Collection = Collection?.Clone<Subject>(),
                    Dictionary = Dictionary?.Clone()
                };
            }
        }
        
        public class KeyValuePairBuilder : ISpecimenBuilder
        {
            public object Create(object request, ISpecimenContext context)
            {
                var type = request as Type;
                if (type == null || type != typeof(KeyValuePair<string, Subject>))
                    return new NoSpecimen();

                var subject = new Subject
                {
                    ValueType = context.Create<string>()
                };

                return new KeyValuePair<string, Subject>(context.Create<string>(), subject);
            }
        }

        private readonly Fixture _fixture;

        public PropertyComparerTest()
        {
            _fixture = new Fixture();
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior(1));
            _fixture.Customizations.Add(new KeyValuePairBuilder());
        }

        [Fact]
        public void Equals_GivenBoth_ObjectsAre_Contain_EqualByValue_ValueType()
        {
            //Arrange
            var a = _fixture.Create<Subject>();
            var b = a.Clone() as Subject;
            var comparer = new PropertyComparer<Subject>();

            //Assert
            Assert.Equal(a, b, comparer);
        }

        [Fact]
        public void Equals_GivenBoth_ObjectsAre_Contain_NotEqualByValue_ValueType()
        {
            //Arrange
            var a = _fixture.Create<Subject>();
            var b = _fixture.Create<Subject>();
            var comparer = new PropertyComparer<Subject>();

            //Assert
            Assert.NotEqual(a, b, comparer);
        }

        [Fact]
        public void Equals_GivenBoth_ObjectsAre_Contain_EqualByValue_ReferenceType()
        {
            //Arrange
            var inner = new Subject {ValueType = _fixture.Create<string>()};
            var a = new Subject {ReferenceType = inner};
            var b = new Subject {ReferenceType = inner.Clone() as Subject};
            var comparer = new PropertyComparer<Subject>();

            //Assert
            Assert.Equal(a, b, comparer);
        }

        [Fact]
        public void Equals_GivenBoth_ObjectsAre_Contain_NotEqualByValue_ReferenceType()
        {
            //Arrange
            var a = new Subject {ReferenceType = _fixture.Create<Subject>()};
            var b = new Subject {ReferenceType = _fixture.Create<Subject>()};
            var comparer = new PropertyComparer<Subject>();

            //Assert
            Assert.NotEqual(a, b, comparer);
        }

        [Fact]
        public void Equals_GivenBoth_ObjectsAre_Contain_EqualByValue_Collection()
        {
            //Arrange
            var collection = CreateCollection();

            var a = new Subject {Collection = collection};
            var b = new Subject {Collection = collection.Clone<Subject>().ToList()};
            var comparer = new PropertyComparer<Subject>();

            //Assert
            Assert.Equal(a, b, comparer);
        }

        [Fact]
        public void Equals_GivenBoth_ObjectsAre_Contain_NotEqualByValue_Collection()
        {
            //Arrange
            var a = new Subject {Collection = CreateCollection()};
            var b = new Subject {Collection = CreateCollection()};
            var comparer = new PropertyComparer<Subject>();

            //Assert
            Assert.NotEqual(a, b, comparer);
        }

        [Fact]
        public void Equals_GivenBoth_ObjectsAre_Contain_EqualByValue_Dictionary()
        {
            //Arrange
            var dictionary = _fixture.Create<IDictionary<string, Subject>>();

            var a = new Subject {Dictionary = dictionary};
            var b = new Subject {Dictionary = dictionary.Clone()};
            var comparer = new PropertyComparer<Subject>();

            //Assert
            Assert.Equal(a, b, comparer);
        }

        [Fact]
        public void Equals_GivenBoth_ObjectsAre_Contain_NotEqualByValue_Dictionary()
        {
            //Arrange
            var a = new Subject {Dictionary = _fixture.Create<IDictionary<string, Subject>>()};
            var b = new Subject {Dictionary = _fixture.Create<IDictionary<string, Subject>>()};
            var comparer = new PropertyComparer<Subject>();

            //Assert
            Assert.NotEqual(a, b, comparer);
        }

        [Fact]
        public void Equals_GivenBoth_AreRecursive()
        {
            //Arrange
            var a = DateTime.MinValue;
            var b = DateTime.MaxValue;
            var comparer = new PropertyComparer<DateTime>();

            //Assert
            Assert.NotEqual(a, b, comparer);
        }

        [Fact]
        public void Equals_GivenB_IsRecursive()
        {
            //Arrange
            var a = "test";
            var b = DateTime.MaxValue;
            var comparer = new PropertyComparer();

            //Assert
            Assert.False(comparer.Equals(a, b));
        }

        private List<Subject> CreateCollection(int count = 3)
        {
            return Enumerable.Range(0, count)
                .Select(n => new Subject {ValueType = _fixture.Create<string>()})
                .ToList();
        }
    }
}
