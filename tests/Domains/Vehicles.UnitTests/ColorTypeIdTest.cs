﻿using NexCore.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.Vehicles.UnitTests
{
    public class ColorTypeIdTest
    {
        [Theory]
        [InlineData("PH9")]
        [InlineData("pkV")]
        [InlineData("Pbz")]
        public void Parse(string value)
        {
            //Act
            var colorTypeId = ColorTypeId.Parse(value);

            //Assert
            Assert.NotNull(colorTypeId);
            Assert.Equal(value.ToUpperInvariant(), colorTypeId.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                ColorTypeId.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("PH91")]
        [InlineData("N$S")]
        [InlineData("F")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>
            {
                ColorTypeId.Parse(value);
            });
        }

        [Theory]
        [InlineData("PH9")]
        [InlineData("pkV")]
        [InlineData("Pbz")]
        public void TryParse(string value)
        {
            //Act
            var result = ColorTypeId.TryParse(value, out var colorTypeId);

            //Assert
            Assert.True(result);
            Assert.NotNull(colorTypeId);
            Assert.Equal(value.ToUpperInvariant(), colorTypeId.Value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("PH91")]
        [InlineData("N$")]
        [InlineData("N ")]
        [InlineData("F")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = ColorTypeId.TryParse(value, out var colorTypeId);

            //Assert
            Assert.False(result);
            Assert.Null(colorTypeId);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var colorTypeId = ColorTypeId.Parse("PH9");

            //Assert
            Assert.Equal(colorTypeId.Value, colorTypeId.ToString());
        }
    }
}
