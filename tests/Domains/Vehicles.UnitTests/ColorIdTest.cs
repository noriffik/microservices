﻿using NexCore.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.Vehicles.UnitTests
{
    public class ColorIdTest
    {
        [Theory]
        [InlineData("PH9f")]
        [InlineData("pkVs")]
        [InlineData("Pbz8")]
        public void Parse(string value)
        {
            //Act
            var colorId = ColorId.Parse(value);

            //Assert
            Assert.NotNull(colorId);
            Assert.Equal(value.ToUpperInvariant(), colorId.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                ColorId.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("PH91b")]
        [InlineData("N$S")]
        [InlineData("F")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>
            {
                ColorId.Parse(value);
            });
        }

        [Theory]
        [InlineData("PH9f")]
        [InlineData("pkV2")]
        [InlineData("Pbz1")]
        public void TryParse(string value)
        {
            //Act
            var result = ColorId.TryParse(value, out var colorId);

            //Assert
            Assert.True(result);
            Assert.NotNull(colorId);
            Assert.Equal(value.ToUpperInvariant(), colorId.Value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("PH914")]
        [InlineData("N$")]
        [InlineData("N2 ")]
        [InlineData("F2")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = ColorId.TryParse(value, out var colorId);

            //Assert
            Assert.False(result);
            Assert.Null(colorId);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var colorId = ColorId.Parse("PH9W");

            //Assert
            Assert.Equal(colorId.Value, colorId.ToString());
        }
    }
}
