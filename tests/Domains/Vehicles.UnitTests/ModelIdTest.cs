﻿using NexCore.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.Vehicles.UnitTests
{
    public class ModelIdTest
    {
        [Theory]
        [InlineData("NF")]
        [InlineData("Nj")]
        [InlineData("3V")]
        public void Parse(string value)
        {
            //Act
            var modelId = ModelId.Parse(value);

            //Assert
            Assert.NotNull(modelId);
            Assert.Equal(value.ToUpperInvariant(), modelId.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                ModelId.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("NFF")]
        [InlineData("N$")]
        [InlineData("N ")]
        [InlineData("F")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>
            {
                ModelId.Parse(value);
            });
        }

        [Theory]
        [InlineData("NF")]
        [InlineData("Nj")]
        [InlineData("3V")]
        public void TryParse(string value)
        {
            //Act
            var result = ModelId.TryParse(value, out var modelId);

            //Assert
            Assert.True(result);
            Assert.NotNull(modelId);
            Assert.Equal(value.ToUpperInvariant(), modelId.Value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("NFF")]
        [InlineData("N$")]
        [InlineData("N ")]
        [InlineData("F")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = ModelId.TryParse(value, out var modelId);

            //Assert
            Assert.False(result);
            Assert.Null(modelId);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var modelId = ModelId.Parse("NF");

            //Assert
            Assert.Equal(modelId.Value, modelId.ToString());
        }
    }
}
