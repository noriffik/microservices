﻿using NexCore.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.Vehicles.UnitTests
{
    public class EquipmentIdTest
    {
        [Theory]
        [InlineData("NJ3")]
        [InlineData("nH4")]
        [InlineData("Nx2")]
        [InlineData("3vr")]
        public void Parse(string value)
        {
            //Act
            var equipmentId = EquipmentId.Parse(value);

            //Assert
            Assert.NotNull(equipmentId);
            Assert.Equal(value.ToUpperInvariant(), equipmentId.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                EquipmentId.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("/N1")]
        [InlineData("F%1")]
        [InlineData("NF1X")]
        [InlineData("NF")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>
            {
                EquipmentId.Parse(value);
            });
        }

        [Theory]
        [InlineData("NJ3")]
        [InlineData("nH4")]
        [InlineData("Nx2")]
        [InlineData("3vr")]
        public void TryParse(string value)
        {
            //Act
            var result = EquipmentId.TryParse(value, out var equipmentId);

            //Assert
            Assert.True(result);
            Assert.NotNull(equipmentId);
            Assert.Equal(value.ToUpperInvariant(), equipmentId.Value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("/N1")]
        [InlineData("F%1")]
        [InlineData("NF1X")]
        [InlineData("NF")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = EquipmentId.TryParse(value, out var equipmentId);

            //Assert
            Assert.False(result);
            Assert.Null(equipmentId);
        }

        [Fact]
        public void Next()
        {
            //Arrange
            var modelId = ModelId.Parse("3V");

            //Act
            var equipmentId = EquipmentId.Next(modelId, 'R');

            //Assert
            Assert.NotNull(equipmentId);
            Assert.Equal("3VR", equipmentId.Value);
        }

        [Fact]
        public void Next_GivenModelId_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                EquipmentId.Next(null, 'A');
            });
            Assert.Equal("modelId", e.ParamName);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var equipmentId = EquipmentId.Parse("NJ3");

            //Assert
            Assert.Equal(equipmentId.Value, equipmentId.ToString());
        }
    }
}
