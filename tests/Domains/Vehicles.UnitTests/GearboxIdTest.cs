﻿using NexCore.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.Vehicles.UnitTests
{
    public class GearboxIdTest
    {
        [Theory]
        [InlineData("NFV")]
        [InlineData("3VD")]
        [InlineData("NHc")]
        [InlineData("3vZ")]
        public void Parse(string value)
        {
            //Act
            var gearboxId = GearboxId.Parse(value);

            //Assert
            Assert.NotNull(gearboxId);
            Assert.Equal(value.ToUpperInvariant(), gearboxId.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                GearboxId.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("/N1")]
        [InlineData("F%1")]
        [InlineData("NF1X")]
        [InlineData("NF")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>
            {
                GearboxId.Parse(value);
            });
        }

        [Theory]
        [InlineData("NFV")]
        [InlineData("3VD")]
        [InlineData("NHc")]
        [InlineData("3vZ")]
        public void TryParse(string value)
        {
            //Act
            var result = GearboxId.TryParse(value, out var gearboxId);

            //Assert
            Assert.True(result);
            Assert.NotNull(gearboxId);
            Assert.Equal(value.ToUpperInvariant(), gearboxId.Value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("/N1")]
        [InlineData("F%1")]
        [InlineData("NF1X")]
        [InlineData("NF")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = GearboxId.TryParse(value, out var gearboxId);

            //Assert
            Assert.False(result);
            Assert.Null(gearboxId);
        }

        [Fact]
        public void Next()
        {
            //Arrange
            var modelId = ModelId.Parse("NF");

            //Act
            var gearboxId = GearboxId.Next(modelId, '1');

            //Assert
            Assert.NotNull(gearboxId);
            Assert.Equal("NF1", gearboxId.Value);
        }

        [Fact]
        public void Next_GivenModelId_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                GearboxId.Next(null, 'A');
            });
            Assert.Equal("modelId", e.ParamName);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var gearboxId = GearboxId.Parse("NF1");

            //Assert
            Assert.Equal(gearboxId.Value, gearboxId.ToString());
        }
    }
}
