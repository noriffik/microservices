﻿using NexCore.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.Vehicles.UnitTests
{
    public class PrNumberTest
    {
        [Theory]
        [InlineData("PH9")]
        [InlineData("pkV")]
        [InlineData("Pbz")]
        public void Parse(string value)
        {
            //Act
            var prNumber = PrNumber.Parse(value);

            //Assert
            Assert.NotNull(prNumber);
            Assert.Equal(value.ToUpperInvariant(), prNumber.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                PrNumber.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("PH91")]
        [InlineData("N$")]
        [InlineData("N ")]
        [InlineData("F")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>
            {
                PrNumber.Parse(value);
            });
        }

        [Theory]
        [InlineData("PH9")]
        [InlineData("pkV")]
        [InlineData("Pbz")]
        public void TryParse(string value)
        {
            //Act
            var result = PrNumber.TryParse(value, out var prNumber);

            //Assert
            Assert.True(result);
            Assert.NotNull(prNumber);
            Assert.Equal(value.ToUpperInvariant(), prNumber.Value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("PH91")]
        [InlineData("N$")]
        [InlineData("N ")]
        [InlineData("F")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = PrNumber.TryParse(value, out var prNumber);

            //Assert
            Assert.False(result);
            Assert.Null(prNumber);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var prNumber = PrNumber.Parse("PH9");

            //Assert
            Assert.Equal(prNumber.Value, prNumber.ToString());
        }
    }
}
