﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Vehicles.UnitTests.Fixtures;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.Vehicles.UnitTests
{
    public class BoundModelKeyDictionaryTest
    {
        [Fact]
        public void Ctor()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehiclesSpecimenBuilder());

            var inner = fixture.Create<Dictionary<ModelKey, int>>();
            var modelId = inner.Keys.First().ModelId;
            
            //Act
            var dictionary = new BoundModelKeyDictionary<int>(inner);

            //Assert
            Assert.Equal(inner, dictionary);
            Assert.Equal(modelId, dictionary.ModelId);
        }

        [Fact]
        public void Ctor_GivenValues_IsEmpty_Throws()
        {
            //Arrange
            var inner = new Dictionary<ModelKey, int>();
            
            //Act
            Assert.Throws<SingleModelViolationException>(() => new BoundModelKeyDictionary<int>(inner));
        }

        [Fact]
        public void Ctor_GivenValues_ModelKeys_Models_DoNotMatch_Throws()
        {
            //Arrange
            var inner = Enumerable.Range(0, 3)
                .Select(n => new KeyValuePair<ModelKey, int>(ModelKey.Parse($"{n}00000"), n));
            
            //Act
            Assert.Throws<SingleModelViolationException>(() => new BoundModelKeyDictionary<int>(inner));
        }
    }
}
