﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Vehicles.UnitTests.Fixtures;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.Vehicles.UnitTests
{
    public class BoundModelKeySetTest
    {
        private readonly ModelId _modelId;
        private readonly IEnumerable<ModelKey> _modelKeys;
        private readonly IEnumerable<ModelKey> _invalidModelKeys;

        public BoundModelKeySetTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehiclesSpecimenBuilder());

            _modelId = fixture.Create<ModelId>();
            _modelKeys = Enumerable.Range(0, 3).Select(i => ModelKey.Parse($"{_modelId}000{i}"));
            _invalidModelKeys = _modelKeys.Concat(new [] {ModelKey.Parse("ABC123")});
        }

        [Fact]
        public void Parse_GivenValue_Implies_DifferentModelId_Throws()
        {
            Assert.Throws<SingleModelViolationException>(
                () => BoundModelKeySet.Parse("ABC123 AAC123"));
        }

        [Fact]
        public void Ctor()
        {
            //Arrange
            var set = new BoundModelKeySet(_modelKeys);

            //Assert
            Assert.Equal(_modelId, set.ModelId);
        }

        [Fact]
        public void Ctor_GivenModelKeys_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelKeys",
                () => new BoundModelKeySet(null as IEnumerable<ModelKey>));
        }

        [Fact]
        public void Ctor_GivenModelKeys_IsEmpty_Throws()
        {
            Assert.Throws<SingleModelViolationException>(() => new BoundModelKeySet(new ModelKey[] {}));
        }

        [Fact]
        public void Ctor_GivenModelKeys_ViolateSpecification_Throws()
        {
            Assert.Throws<SingleModelViolationException>(() => new BoundModelKeySet(_invalidModelKeys));
        }

        [Fact]
        public void Ctor_WithModelKeySet()
        {
            //Arrange
            var set = new BoundModelKeySet(new ModelKeySet(_modelKeys));

            //Assert
            Assert.Equal(_modelId, set.ModelId);
        }

        [Fact]
        public void Ctor_WithModelKeySet_GivenModelKeys_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelKeys",
                () => new BoundModelKeySet(null as IEnumerable<ModelKey>));
        }
        
        [Fact]
        public void Ctor_WithModelKeySet_GivenModelKeys_IsEmpty_Throws()
        {
            var e = Assert.Throws<SingleModelViolationException>(
                () => new BoundModelKeySet(new ModelKeySet()));
        }

        [Fact]
        public void Ctor_WithModelKeySet_GivenModelKeys_ViolateSpecification_Throws()
        {
            Assert.Throws<SingleModelViolationException>(() => new BoundModelKeySet(new ModelKeySet(_invalidModelKeys)));
        }

        [Theory]
        [InlineData(" ABc123", "ABC123")]
        [InlineData("ABa123  AB0123 ABy123  ", "AB0123 ABA123 ABY123")]
        [InlineData("   ABC123  AB0123    ABy123   ABx123   ", "AB0123 ABC123 ABX123 ABY123")]
        public void TryParse(string value, string expected)
        {
            //Act
            var result = BoundModelKeySet.TryParse(value, out var set);

            //Assert
            Assert.True(result);
            Assert.NotNull(set);
            Assert.Equal(expected, set.ToString());
        }

        [Theory]
        [InlineData(null)]
        [InlineData("ABZ123 AB23")]
        [InlineData("ABZ123 AB23456")]
        [InlineData("ABZ123 AB1%34")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = BoundModelKeySet.TryParse(value, out var set);

            //Assert
            Assert.False(result);
            Assert.Null(set);
        }

        [Fact]
        public void TryParse_GivenValue_ContainsDuplicate_PrNumbers_IgnoresThem()
        {
            //Act
            var result = BoundModelKeySet.TryParse("PBZ123 PBC123 PBA123 PBC123", out var set);

            //Assert
            Assert.True(result);
            Assert.Equal("PBA123 PBC123 PBZ123", set.AsString);
        }

        [Fact]
        public void Expand_GivenWith_IsForDifferentModel_Throws()
        {
            //Arrange
            var valid = ModelKeySet.Parse("BB0000");
            var invalid = ModelKeySet.Parse("AA0000");
            var set = new BoundModelKeySet(valid);

            //Assert
            Assert.Throws<SingleModelViolationException>(() => set.Expand(invalid));
        }

        [Fact]
        public void Expand_GivenWith_IsString_AndIsForDifferentModel_Throws()
        {
            //Arrange
            var valid = ModelKeySet.Parse("BB0000");
            var invalid = "AA0000";
            var set = new BoundModelKeySet(valid);

            //Assert
            Assert.Throws<SingleModelViolationException>(() => set.Expand(invalid));
        }

        [Fact]
        public void Expand_GivenWith_IsEnumerable_AndIsForDifferentModel_Throws()
        {
            //Arrange
            var valid = ModelKeySet.Parse("BB0000");
            var invalid = new[] { ModelKey.Parse("AA0000") };
            var set = new BoundModelKeySet(valid);

            //Assert
            Assert.Throws<SingleModelViolationException>(() => set.Expand(invalid));
        }

        [Fact]
        public void Expand_GivenWith_IsEmpty_DoesNothing()
        {
            //Arrange
            var empty = new ModelKeySet();
            var set = BoundModelKeySet.Parse("BB0000");

            //Act
            var result = set.Expand(empty);

            //Assert
            Assert.Equal(set, result);
            Assert.NotSame(set, result);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public void GetByIndex(int index)
        {
            //Arrange
            var set = new BoundModelKeySet(_modelKeys);

            //Assert
            Assert.Equal(_modelKeys.ElementAt(index), set[index]);
        }
    }
}