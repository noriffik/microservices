﻿using Moq;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.Vehicles.UnitTests
{
    public class PrNumberSetTest
    {
        [Fact]
        public void Ctor()
        {
            //Act
            var set = new PrNumberSet();

            //Arrange
            Assert.Empty(set.Values);
        }

        [Fact]
        public void Ctor_WithPrNumbers()
        {
            //Arrange
            var values = CreatePrNumbers("PBZ", "PBA", "PBC");

            //Act
            var set = new PrNumberSet(values);

            //Arrange
            Assert.Equal(values.OrderBy(n => n.Value), set.Values);
            Assert.Equal("PBA PBC PBZ", set.AsString);
        }

        [Fact]
        public void Ctor_WithPrNumbers_GivenPrNumbers_ContainDuplicates_IgnoresThem()
        {
            //Arrange
            var values = CreatePrNumbers("PBZ", "PBC", "PBA", "PBC");

            //Act
            var set = new PrNumberSet(values);

            //Arrange
            Assert.Equal("PBA PBC PBZ", set.AsString);
        }

        [Fact]
        public void Ctor_WithPrNumberSet()
        {
            //Arrange
            var values = CreatePrNumbers("BA1", "AB1", "AC1");
            var inner = new Mock<IValueSet<PrNumber>>();
            inner.Setup(i => i.Values).Returns(values);
            inner.Setup(i => i.AsString).Returns("AB1 AC1 BA1");

            //Act
            var set = new PrNumberSet(inner.Object);

            //Arrange
            Assert.Equal(values, set.Values);
            Assert.Equal("AB1 AC1 BA1", set.AsString);
        }

        [Fact]
        public void Ctor_WithPrNumberArray()
        {
            //Arrange
            var values = CreatePrNumbers("BA1", "AB1", "AC1");

            //Act
            var set = new PrNumberSet(values);

            //Arrange
            Assert.Equal("AB1 AC1 BA1", set.AsString);
        }

        private static PrNumber[] CreatePrNumbers(params string[] values)
        {
            return values.Select(PrNumber.Parse).ToArray();
        }

        [Fact]
        public void Equality()
        {
            //Arrange
            var set = PrNumberSetWith("PBZ", "PBA", "PBC");

            //Assert
            Assert.Equal(set, PrNumberSetWith("PBZ", "PBA", "PBC"));
            Assert.Equal(set, PrNumberSetWith("PBZ", "PBC", "PBA"));
            Assert.NotEqual(set, PrNumberSetWith("PBZ", "PBA"));
        }

        private static PrNumberSet PrNumberSetWith(params string[] values)
        {
            return new PrNumberSet(CreatePrNumbers(values));
        }

        [Fact]
        public void Formatting()
        {
            Assert.Equal(string.Empty, new PrNumberSet().ToString());
            Assert.Equal("PBA PBC PBZ", PrNumberSetWith("PBZ", "PBA", "PBC").ToString());
        }

        [Theory]
        [InlineData(" pBz", "PBZ")]
        [InlineData(" PBa bn0 01x", "01X BN0 PBA")]
        [InlineData("PBa  bn0 01y  ", "01Y BN0 PBA")]
        [InlineData("   PBC  bn0    01y   PBx   ", "01Y BN0 PBC PBX")]
        public void Parse(string value, string expected)
        {
            //Act
            var set = PrNumberSet.Parse(value);

            //Assert
            Assert.NotNull(set);
            Assert.Equal(expected, set.ToString());
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() => PrNumberSet.Parse(null));
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("PBZ A")]
        [InlineData("PBZ A1")]
        [InlineData("PBZ A%1")]
        [InlineData("PB,")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>  PrNumberSet.Parse(value));
        }

        [Theory]
        [InlineData(" pBz", "PBZ")]
        [InlineData(" PBa bn0 01x", "01X BN0 PBA")]
        [InlineData("PBa  bn0 01y  ", "01Y BN0 PBA")]
        [InlineData("   PBC  bn0    01y   PBx   ", "01Y BN0 PBC PBX")]
        public void TryParse(string value, string expected)
        {
            //Act
            var result = PrNumberSet.TryParse(value, out var set);

            //Assert
            Assert.True(result);
            Assert.NotNull(set);
            Assert.Equal(expected, set.ToString());
        }

        [Theory]
        [InlineData(null)]
        [InlineData("PBZ A")]
        [InlineData("PBZ A1")]
        [InlineData("PBZ A%1")]
        [InlineData("PB,")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = PrNumberSet.TryParse(value, out var set);

            //Assert
            Assert.False(result);
            Assert.Null(set);
        }

        [Fact]
        public void TryParse_GivenValue_ContainsDuplicate_PrNumbers_IgnoresThem()
        {
            //Act
            var result = PrNumberSet.TryParse("PBZ PBC PBA PBC", out var set);

            //Assert
            Assert.True(result);
            Assert.Equal("PBA PBC PBZ", set.AsString);
        }

        [Fact]
        public void Expand_GivenWith_IsEnumerable()
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AA2");
            var expected = PrNumberSet.Parse(set.AsString + " AA3 AA4");

            //Act
            var actual = set.Expand(new[] {PrNumber.Parse("AA3"), PrNumber.Parse("AA4")});

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Expand_GivenWith_IsEnumerable_And_IsNull_Throws()
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AA2");

            //Assert
            Assert.Throws<ArgumentNullException>("with", () => set.Expand(null as IEnumerable<PrNumber>));
        }

        [Fact]
        public void Expand_GivenWith_IsPrNumberSet()
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AA2");
            var expected = PrNumberSet.Parse(set.AsString + " AA3 AA4");

            //Act
            var actual = set.Expand(PrNumberSet.Parse("AA3 AA4"));

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Expand_GivenWith_IsPrNumberSet_And_IsNull_Throws()
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AA2");

            //Assert
            Assert.Throws<ArgumentNullException>("with", () => set.Expand(null as IValueSet<PrNumber>));
        }

        [Fact]
        public void Shrink_GivenWith_IsEnumerable()
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AA2 AA3 AA4");
            var expected = PrNumberSet.Parse("AA1 AA2");

            //Act
            var actual = set.Shrink(new[] {PrNumber.Parse("AA3"), PrNumber.Parse("AA4")});

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Shrink_GivenWith_IsEnumerable_And_IsNull_Throws()
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AA2 AA3 AA4");

            //Assert
            Assert.Throws<ArgumentNullException>("with", () => set.Shrink(null as IEnumerable<PrNumber>));
        }

        [Fact]
        public void Shrink_GivenWith_IsPrNumberSet()
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AA2 AA3 AA4");
            var expected = PrNumberSet.Parse("AA1 AA2");

            //Act
            var actual = set.Shrink(PrNumberSet.Parse("AA3 AA4"));

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Shrink_GivenWith_IsPrNumberSet_And_IsNull_Throws()
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AA2 AA3 AA4");

            //Assert
            Assert.Throws<ArgumentNullException>("with", () => set.Shrink(null as IValueSet<PrNumber>));
        }
       
        [Fact]
        public void IsIncludeAll_GivenPrNumberSet_IsNull_Throws()
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AA2 AA3 AA4");

            //Assert
            Assert.Throws<ArgumentNullException>("prNumberSet", () => set.Has((PrNumberSet) null));
        }

        [Fact]
        public void IsIncludeAll()
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AA2 AA3 AA4");

            //Act
            var actualTrue = set.Has(PrNumberSet.Parse("AA3 AA4"));
            var actualFalse = set.Has(PrNumberSet.Parse("AA3 AA4 AA5"));

            //Assert
            Assert.True(actualTrue);
            Assert.False(actualFalse);
        }

        [Fact]
        public void IsIncludeOne_GivenString_IsNull_Throws()
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AA2 AA3 AA4");

            //Assert
            Assert.Throws<ArgumentNullException>("prNumberSet", () => set.HasExactlyOne(null));
        }

        [Fact]
        public void IsIncludeOne()
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AA2 AA3 AA4");

            //Act
            var actualAllFalse = set.HasExactlyOne(PrNumberSet.Parse("AA1 AA2 AA3"));
            var actualFalse = set.HasExactlyOne(PrNumberSet.Parse("AA5 AA6"));
            var actualTrue = set.HasExactlyOne(PrNumberSet.Parse("AA1"));

            //Assert
            Assert.False(actualAllFalse);
            Assert.False(actualFalse);
            Assert.True(actualTrue);
        }

        [Fact]
        public void IsIncludePartially_GivenPrNumberSetString_IsNull_Throws()
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AA2 AA3 AA4");

            //Assert
            Assert.Throws<ArgumentNullException>("prNumberSet", () => set.HasAtLeast(null, 1));
        }

        [Theory]
        [InlineData("AA1 AA2 AA3 AA4")]
        [InlineData("AA1 AA2 AA3")]
        [InlineData("AA1 AA2")]
        [InlineData("AA1 AA2 AA5 AA6")]
        [InlineData("AA1 AA2 AA5")]
        public void IsIncludePartially(string prNumbers)
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AA2 AA3 AA4");
            var matchingCount = 2;

            //Act
            var actual = set.HasAtLeast(PrNumberSet.Parse(prNumbers), matchingCount);

            //Assert
            Assert.True(actual);
        }

        [Theory]
        [InlineData("AA5 AA6 AA7")]
        [InlineData("AA1")]
        [InlineData("AA1 AA5 AA6 AA7")]
        [InlineData("AA1 AA5 AA6")]
        public void IsIncludePartially_WhenNot(string prNumbers)
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AA2 AA3 AA4");
            var matchingCount = 2;

            //Act
            var actual = set.HasAtLeast(PrNumberSet.Parse(prNumbers), matchingCount);

            //Assert
            Assert.False(actual);
        }

        [Fact]
        public void Has()
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AB3 AA2");

            //Assert
            Assert.True(set.Has(set.Values.ElementAt(1)));
            Assert.False(set.Has(PrNumber.Parse("XXX")));
        }

        [Fact]
        public void Has_GivenPrNumber_IsNull_Throws()
        {
            //Arrange
            var set = PrNumberSet.Parse("AA1 AB3 AA2");

            //Assert
            Assert.Throws<ArgumentNullException>("prNumber", () => set.Has((PrNumber) null));
        }
    }
}
