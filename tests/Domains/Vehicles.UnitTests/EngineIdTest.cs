﻿using NexCore.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.Vehicles.UnitTests
{
    public class EngineIdTest
    {
        [Theory]
        [InlineData("NFM")]
        [InlineData("3VN")]
        [InlineData("NHl")]
        [InlineData("3v7")]
        public void Parse(string value)
        {
            //Act
            var engineId = EngineId.Parse(value);

            //Assert
            Assert.NotNull(engineId);
            Assert.Equal(value.ToUpperInvariant(), engineId.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                EngineId.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("/N1")]
        [InlineData("F%1")]
        [InlineData("NF1X")]
        [InlineData("NF")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>
            {
                EngineId.Parse(value);
            });
        }

        [Theory]
        [InlineData("NFM")]
        [InlineData("3VN")]
        [InlineData("NHl")]
        [InlineData("3v7")]
        public void TryParse(string value)
        {
            //Act
            var result = EngineId.TryParse(value, out var engineId);

            //Assert
            Assert.True(result);
            Assert.NotNull(engineId);
            Assert.Equal(value.ToUpperInvariant(), engineId.Value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("/N1")]
        [InlineData("F%1")]
        [InlineData("NF1X")]
        [InlineData("NF")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = EngineId.TryParse(value, out var engineId);

            //Assert
            Assert.False(result);
            Assert.Null(engineId);
        }

        [Fact]
        public void Next()
        {
            //Arrange
            var modelId = ModelId.Parse("NF");

            //Act
            var engineCode = EngineId.Next(modelId, 'C');

            //Assert
            Assert.NotNull(engineCode);
            Assert.Equal("NFC", engineCode.Value);
        }

        [Fact]
        public void Next_GivenModelId_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                EngineId.Next(null, 'C');
            });
            Assert.Equal("modelId", e.ParamName);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var engineId = EngineId.Parse("NFA");

            //Assert
            Assert.Equal(engineId.Value, engineId.ToString());
        }
    }
}
