﻿using Moq;
using NexCore.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.Vehicles.UnitTests
{
    public class ModelKeySetTest
    {
        [Fact]
        public void Ctor()
        {
            //Act
            var set = new ModelKeySet();

            //Assert
            Assert.Empty(set.Values);
        }

        [Fact]
        public void Ctor_WithModelKeys()
        {
            //Arrange
            var values = CreateModelKeys("BA1234", "AB1234", "AC1234");

            //Act
            var set = new ModelKeySet(values);

            //Arrange
            Assert.Equal(values.OrderBy(n => n.Value), set.Values);
            Assert.Equal("AB1234 AC1234 BA1234", set.AsString);
        }

        [Fact]
        public void Ctor_WithModelKeySet()
        {
            //Arrange
            var values = CreateModelKeys("BA1234", "AB1234", "AC1234");
            var inner = new Mock<IModelKeySet>();
            inner.Setup(i => i.Values).Returns(values);
            inner.Setup(i => i.AsString).Returns("AB1234 AC1234 BA1234");

            //Act
            var set = new ModelKeySet(inner.Object);

            //Arrange
            Assert.Equal(values, set.Values);
            Assert.Equal("AB1234 AC1234 BA1234", set.AsString);
        }

        private static ModelKey[] CreateModelKeys(params string[] values)
        {
            return values.Select(ModelKey.Parse).ToArray();
        }

        [Fact]
        public void Ctor_WithModelKeys_GivenModelKeys_ContainDuplicates_IgnoresThem()
        {
            //Arrange
            var values = CreateModelKeys("BA1234", "AB1234", "AC1234", "BA1234");

            //Act
            var set = new ModelKeySet(values);

            //Arrange
            Assert.Equal("AB1234 AC1234 BA1234", set.AsString);
        }

        [Theory]
        [InlineData(" pBz123", "PBZ123")]
        [InlineData("PBa123  bn0123 01y123  ", "01Y123 BN0123 PBA123")]
        [InlineData("   PBC123  bn0123    01y123   PBx123   ", "01Y123 BN0123 PBC123 PBX123")]
        public void Parse(string value, string expected)
        {
            //Act
            var set = ModelKeySet.Parse(value);

            //Assert
            Assert.NotNull(set);
            Assert.Equal(expected, set.ToString());
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                ModelKeySet.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("PBZ123 A123")]
        [InlineData("PBZ123 A123456")]
        [InlineData("PBZ123 A%1123")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() => ModelKeySet.Parse(value));
        }

        [Theory]
        [InlineData(" pBz123", "PBZ123")]
        [InlineData(" PBa123 bn0123 01x123", "01X123 BN0123 PBA123")]
        [InlineData("   PBC123  bn0123    01y123   PBx123   ", "01Y123 BN0123 PBC123 PBX123")]
        public void TryParse(string value, string expected)
        {
            //Act
            var result = ModelKeySet.TryParse(value, out var set);

            //Assert
            Assert.True(result);
            Assert.NotNull(set);
            Assert.Equal(expected, set.ToString());
        }

        [Theory]
        [InlineData(null)]
        [InlineData("PBZ123 A123")]
        [InlineData("PBZ123 A123456")]
        [InlineData("PB,123")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = ModelKeySet.TryParse(value, out var set);

            //Assert
            Assert.False(result);
            Assert.Null(set);
        }

        [Fact]
        public void TryParse_GivenValue_ContainsDuplicate_PrNumbers_IgnoresThem()
        {
            //Act
            var result = ModelKeySet.TryParse("PBZ123 PBC123 PBA123 PBC123", out var set);

            //Assert
            Assert.True(result);
            Assert.Equal("PBA123 PBC123 PBZ123", set.AsString);
        }

        [Fact]
        public void Equality()
        {
            //Arrange
            var set = ModelKeySet.Parse("ABC123 ABB123 ABA123");

            //Assert
            Assert.Equal(set, ModelKeySet.Parse("ABA123 ABC123 ABB123"));
            Assert.Equal(set, ModelKeySet.Parse("ABC123 ABA123 ABB123"));
            Assert.NotEqual(set, ModelKeySet.Parse("ABC123 ABB123 ABX123"));
        }

        [Fact]
        public void Formatting()
        {
            Assert.Equal(string.Empty, new PrNumberSet().ToString());
            Assert.Equal("PBA123 PBC123 PBZ123", ModelKeySet.Parse("PBZ123 PBC123 PBA123").ToString());
        }

        [Fact]
        public void Expand_GivenWith_IsString()
        {
            //Arrange
            var set = ModelKeySet.Parse("AA0001 AA0002");
            var expected = ModelKeySet.Parse(set.AsString + " AA0003 AA0004");

            //Act
            var actual = set.Expand("AA0003 AA0004");

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Expand_GivenWith_IsEnumerable()
        {
            //Arrange
            var set = ModelKeySet.Parse("AA0001 AA0002");
            var expected = ModelKeySet.Parse(set.AsString + " AA0003 AA0004");

            //Act
            var actual = set.Expand(new[] { ModelKey.Parse("AA0003"), ModelKey.Parse("AA0004") });

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Expand_GivenWith_IsEnumerable_And_IsNull_Throws()
        {
            //Arrange
            var set = ModelKeySet.Parse("AA0001 AA0002");

            //Assert
            Assert.Throws<ArgumentNullException>("with", () => set.Expand(null as IEnumerable<ModelKey>));
        }

        [Fact]
        public void Expand_GivenWith_IsModelKeySet()
        {
            //Arrange
            var set = ModelKeySet.Parse("AA0001 AA0002");
            var expected = ModelKeySet.Parse(set.AsString + " AA0003 AA0004");

            //Act
            var actual = set.Expand(ModelKeySet.Parse("AA0003 AA0004"));

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Expand_GivenWith_IsModelSet_And_IsNull_Throws()
        {
            //Arrange
            var set = ModelKeySet.Parse("AA0001 AA0002");

            //Assert
            Assert.Throws<ArgumentNullException>("with", () => set.Expand(null as IModelKeySet));
        }

        [Fact]
        public void Shrink_GivenWith_IsString()
        {
            //Arrange
            var set = ModelKeySet.Parse("AA0001 AA0002 AA0003 AA0004");
            var expected = ModelKeySet.Parse("AA0001 AA0002");

            //Act
            var actual = set.Shrink("AA0003 AA0004");

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Shrink_GivenWith_IsEnumerable()
        {
            //Arrange
            var set = ModelKeySet.Parse("AA0001 AA0002 AA0003 AA0004");
            var expected = ModelKeySet.Parse("AA0001 AA0002");

            //Act
            var actual = set.Shrink(new[] { ModelKey.Parse("AA0003"), ModelKey.Parse("AA0004") });

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Shrink_GivenWith_IsEnumerable_And_IsNull_Throws()
        {
            //Arrange
            var set = ModelKeySet.Parse("AA0001 AA0002 AA0003 AA0004");

            //Assert
            Assert.Throws<ArgumentNullException>("with", () => set.Shrink(null as IEnumerable<ModelKey>));
        }

        [Fact]
        public void Shrink_GivenWith_IsModelKeySet()
        {
            //Arrange
            var set = ModelKeySet.Parse("AA0001 AA0002 AA0003 AA0004");
            var expected = ModelKeySet.Parse("AA0001 AA0002");

            //Act
            var actual = set.Shrink(ModelKeySet.Parse("AA0003 AA0004"));

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Shrink_GivenWith_IsModelKeySet_And_IsNull_Throws()
        {
            //Arrange
            var set = ModelKeySet.Parse("AA0001 AA0002 AA0003 AA0004");

            //Assert
            Assert.Throws<ArgumentNullException>("with", () => set.Shrink(null as IModelKeySet));
        }
    }
}
