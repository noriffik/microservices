﻿using NexCore.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.Vehicles.UnitTests
{
    public class ModelKeyTest
    {
        private readonly ModelKey _key = ModelKey.Parse("NF12AW");

        [Theory]
        [InlineData("NF12AW", "NF", '1', '2', 'A', 'W')]
        [InlineData("NE13F1", "NE", '1', '3', 'F', '1')]
        [InlineData("nX52Ly", "NX", '5', '2', 'L', 'Y')]
        public void Parse(string value, string model, char body, char equipment, char engine, char gearbox)
        {
            //Act
            var key = ModelKey.Parse(value);

            //Assert
            Assert.NotNull(key);
            Assert.Equal(value.ToUpperInvariant(), key.Value);
            Assert.Equal(model, key.ModelCode);
            Assert.Equal(body, key.BodyCode);
            Assert.Equal(equipment, key.EquipmentCode);
            Assert.Equal(engine, key.EngineCode);
            Assert.Equal(gearbox, key.GearboxCode);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                ModelKey.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("NFF")]
        [InlineData("N$")]
        [InlineData("N ")]
        [InlineData("F")]
        [InlineData("NFXX")]
        [InlineData("NFXXX")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>
            {
                ModelKey.Parse(value);
            });
        }

        [Theory]
        [InlineData("NF12AW", "NF", '1', '2', 'A', 'W')]
        [InlineData("NE13F1", "NE", '1', '3', 'F', '1')]
        [InlineData("nX52Ly", "NX", '5', '2', 'L', 'Y')]
        public void TryParse(string value, string model, char body, char equipment, char engine, char gearbox)
        {
            //Act
            var result = ModelKey.TryParse(value, out var key);

            //Assert
            Assert.True(result);
            Assert.NotNull(key);
            Assert.Equal(value.ToUpperInvariant(), key.Value);
            Assert.Equal(model, key.ModelCode);
            Assert.Equal(body, key.BodyCode);
            Assert.Equal(equipment, key.EquipmentCode);
            Assert.Equal(engine, key.EngineCode);
            Assert.Equal(gearbox, key.GearboxCode);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("NFF")]
        [InlineData("N$")]
        [InlineData("N ")]
        [InlineData("F")]
        [InlineData("NFXX")]
        [InlineData("NFXXX")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = ModelKey.TryParse(value, out var key);

            //Assert
            Assert.False(result);
            Assert.Null(key);
        }

        [Fact]
        public void GetModelCode()
        {
            //Assert
            Assert.Equal(ModelId.Parse(_key.ModelCode), _key.ModelId);
        }

        [Fact]
        public void GetBodyCode()
        {
            //Assert
            Assert.Equal(BodyId.Next(_key.ModelId, _key.BodyCode), _key.BodyId);
        }

        [Fact]
        public void GetEquipmentCode()
        {
            //Assert
            Assert.Equal(EquipmentId.Next(_key.ModelId, _key.EquipmentCode), _key.EquipmentId);
        }

        [Fact]
        public void GetEngineCode()
        {
            //Assert
            Assert.Equal(EngineId.Next(_key.ModelId, _key.EngineCode), _key.EngineId);
        }

        [Fact]
        public void GetGearboxCode()
        {
            //Assert
            Assert.Equal(GearboxId.Next(_key.ModelId, _key.GearboxCode), _key.GearboxId);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var key = ModelKey.Parse("NE13F1");

            //Assert
            Assert.Equal(key.Value, key.ToString());
        }
    }
}
