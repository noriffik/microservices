﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Vehicles.UnitTests.Fixtures;
using System;
using System.Collections.Generic;
using Xunit;

namespace NexCore.Vehicles.UnitTests
{
    public class OptionIdTest
    {
        private readonly ModelId _modelId;
        private readonly PrNumber _prNumber;

        public OptionIdTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehiclesSpecimenBuilder());

            _modelId = fixture.Create<ModelId>();
            _prNumber = fixture.Create<PrNumber>();
        }

        [Theory]
        [InlineData("NFPBZ")]
        [InlineData("3VP01")]
        [InlineData("NHSV1")]
        [InlineData("3vs7P")]
        public void Parse(string value)
        {
            //Act
            var optionId = OptionId.Parse(value);

            //Assert
            Assert.NotNull(optionId);
            Assert.Equal(value.ToUpperInvariant(), optionId.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                OptionId.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("/N1")]
        [InlineData("F%1X2")]
        [InlineData("NF1X")]
        [InlineData("NF1")]
        [InlineData("NF")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>
            {
                OptionId.Parse(value);
            });
        }

        [Theory]
        [InlineData("NFPBZ")]
        [InlineData("3VP01")]
        [InlineData("NHSV1")]
        [InlineData("3vs7P")]
        public void TryParse(string value)
        {
            //Act
            var result = OptionId.TryParse(value, out var optionId);

            //Assert
            Assert.True(result);
            Assert.NotNull(optionId);
            Assert.Equal(value.ToUpperInvariant(), optionId.Value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("/N1")]
        [InlineData("F%1X2")]
        [InlineData("NF1X")]
        [InlineData("NF1")]
        [InlineData("NF")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = OptionId.TryParse(value, out var optionId);

            //Assert
            Assert.False(result);
            Assert.Null(optionId);
        }

        [Fact]
        public void Next()
        {
            //Act
            var optionId = OptionId.Next(_modelId, _prNumber);

            //Assert
            Assert.NotNull(optionId);
            Assert.Equal(_modelId.Value + _prNumber.Value, optionId.Value);
            Assert.Equal(_prNumber, optionId.PrNumber);
        }

        [Fact]
        public void Next_GivenModelId_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                OptionId.Next(null, _prNumber);
            });
            Assert.Equal("modelId", e.ParamName);
        }

        [Fact]
        public void Next_GivenPrNumber_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                OptionId.Next(_modelId, null as PrNumber);
            });
            Assert.Equal("prNumber", e.ParamName);
        }

        [Fact]
        public void Next_WithPrNumberSet()
        {
            //Arrange
            var expected = new[] { OptionId.Parse(_modelId + "AAA"), OptionId.Parse(_modelId + "BBB") };

            //Act
            var actual = OptionId.Next(_modelId, PrNumberSet.Parse("AAA BBB"));

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Next_WithPrNumberSet_GivenModelId_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                OptionId.Next(null, new PrNumberSet());
            });
            Assert.Equal("modelId", e.ParamName);
        }

        [Fact]
        public void Next_WithPrNumberSet_GivenPrNumberSet_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                OptionId.Next(_modelId, null as PrNumberSet);
            });
            Assert.Equal("prNumbers", e.ParamName);
        }

        [Fact]
        public void Next_WithIEnumerable()
        {
            //Arrange
            var expected = new[] { OptionId.Parse(_modelId + "AAA"), OptionId.Parse(_modelId + "BBB") };

            //Act
            var actual = OptionId.Next(_modelId, new[] { PrNumber.Parse("AAA"), PrNumber.Parse("BBB") });

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Next_WithIEnumerable_GivenModelId_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                OptionId.Next(null, new List<PrNumber>());
            });
            Assert.Equal("modelId", e.ParamName);
        }

        [Fact]
        public void Next_WithIEnumerable_GivenIEnumerable_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                OptionId.Next(_modelId, null as IEnumerable<PrNumber>);
            });
            Assert.Equal("prNumbers", e.ParamName);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var optionId = OptionId.Parse(_modelId.Value + _prNumber.Value);

            //Assert
            Assert.Equal(optionId.Value, optionId.ToString());
        }

        [Fact]
        public void ModelId()
        {
            //Arrange
            var optionId = OptionId.Parse("00000");

            //Act
            var result = optionId.ModelId;

            //Assert
            Assert.Equal("00", result.Value);
        }
    }
}
