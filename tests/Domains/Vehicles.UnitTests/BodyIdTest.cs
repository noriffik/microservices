﻿using NexCore.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.Vehicles.UnitTests
{
    public class BodyIdTest
    {
        [Theory]
        [InlineData("NF1")]
        [InlineData("3V3")]
        [InlineData("NHX")]
        [InlineData("3v5")]
        public void Parse(string value)
        {
            //Act
            var bodyId = BodyId.Parse(value);

            //Assert
            Assert.NotNull(bodyId);
            Assert.Equal(value.ToUpperInvariant(), bodyId.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                BodyId.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("/N1")]
        [InlineData("F%1")]
        [InlineData("NF1X")]
        [InlineData("NF")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>
            {
                BodyId.Parse(value);
            });
        }

        [Theory]
        [InlineData("NF1")]
        [InlineData("3V3")]
        [InlineData("NHX")]
        [InlineData("3v5")]
        public void TryParse(string value)
        {
            //Act
            var result = BodyId.TryParse(value, out var bodyId);

            //Assert
            Assert.True(result);
            Assert.NotNull(bodyId);
            Assert.Equal(value.ToUpperInvariant(), bodyId.Value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("/N1")]
        [InlineData("F%1")]
        [InlineData("NF1X")]
        [InlineData("NF")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = BodyId.TryParse(value, out var bodyId);

            //Assert
            Assert.False(result);
            Assert.Null(bodyId);
        }

        [Fact]
        public void Next()
        {
            //Arrange
            var modelId = ModelId.Parse("NF");

            //Act
            var bodyCode = BodyId.Next(modelId, '1');

            //Assert
            Assert.NotNull(bodyCode);
            Assert.Equal("NF1", bodyCode.Value);
        }

        [Fact]
        public void Next_GivenModelId_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                BodyId.Next(null, 'A');
            });
            Assert.Equal("modelId", e.ParamName);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var bodyId = BodyId.Parse("NF1");

            //Assert
            Assert.Equal(bodyId.Value, bodyId.ToString());
        }
    }
}
