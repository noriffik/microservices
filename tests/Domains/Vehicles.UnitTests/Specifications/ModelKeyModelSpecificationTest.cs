﻿using NexCore.Domain.Vehicles;
using NexCore.Domain.Vehicles.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.Vehicles.UnitTests.Specifications
{
    public class ModelKeyModelSpecificationTest
    {
        //Entities
        private readonly ModelId _modelId;
        private readonly IReadOnlyList<ModelKey> _expected;
        private readonly IReadOnlyList<ModelKey> _unexpected;
        private readonly IReadOnlyList<ModelKey> _all;

        //Specification
        private readonly ModelKeyModelSpecification _specification;

        public ModelKeyModelSpecificationTest()
        {
            _modelId = ModelId.Parse("XA");

            //Create entities
            _expected = Enumerable.Range(0, 2)
                .Select(n => ModelKey.Parse($"{_modelId}000{n}"))
                .ToList();

            _unexpected = Enumerable.Range(2, 3)
                .Select(n => ModelKey.Parse($"00000{n}"))
                .ToList();

            _all = _unexpected.Concat(_expected)
                .ToList();

            //Create specification
            _specification = new ModelKeyModelSpecification(_modelId);
        }

        [Fact]
        public void Ctor()
        {
            //Arrange
            var specification = new ModelKeyModelSpecification(_modelId);

            //Assert
            Assert.Equal(_modelId, specification.ModelId);
        }

        [Fact]
        public void Ctor_GivenModelId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelId", () => new ModelKeyModelSpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
