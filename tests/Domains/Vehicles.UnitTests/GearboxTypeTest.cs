﻿using NexCore.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.Vehicles.UnitTests
{
    public class GearboxTypeTest
    {
        [Theory]
        [InlineData("6ag")]
        [InlineData("7dsG")]
        [InlineData("7dsg 4x4")]
        [InlineData("7dSG 4X4")]
        public void Parse(string value)
        {
            //Act
            var type = GearboxType.Parse(value);

            //Assert
            Assert.NotNull(type);
            Assert.Equal(value.ToUpperInvariant(), type.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                GearboxType.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("_A3")]
        [InlineData("4B$")]
        [InlineData("7dSG 4d4")]
        [InlineData("7dSG 5x4")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>
            {
                GearboxType.Parse(value);
            });
        }

        [Theory]
        [InlineData("6ag")]
        [InlineData("7dsG")]
        [InlineData("7dsg 4x4")]
        [InlineData("7dSG 4X4")]
        public void TryParse(string value)
        {
            //Act
            var result = GearboxType.TryParse(value, out var type);

            //Assert
            Assert.True(result);
            Assert.NotNull(type);
            Assert.Equal(value.ToUpperInvariant(), type.Value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("/A3")]
        [InlineData("4B%")]
        [InlineData("7dSG 4d4")]
        [InlineData("7dSG 5x4")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = GearboxType.TryParse(value, out var type);

            //Assert
            Assert.False(result);
            Assert.Null(type);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var type = GearboxType.Parse("3DX");

            //Assert
            Assert.Equal(type.Value, type.ToString());
        }
    }
}