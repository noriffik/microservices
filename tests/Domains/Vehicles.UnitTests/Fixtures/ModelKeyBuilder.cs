﻿using NexCore.Domain.Vehicles;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    public class ModelKeyBuilder : SequentialEntityIdGenerator<ModelKey>
    {
        public ModelKeyBuilder() : base(6, ModelKey.Parse)
        {
        }
    }
}
