﻿using NexCore.Domain.Vehicles;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    public class ModelIdBuilder : SequentialEntityIdGenerator<ModelId>
    {
        public ModelIdBuilder() : base(2, ModelId.Parse)
        {
        }
    }
}
