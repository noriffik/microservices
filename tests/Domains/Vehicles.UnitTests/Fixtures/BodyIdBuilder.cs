﻿using NexCore.Domain.Vehicles;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    public class BodyIdBuilder : SequentialEntityIdGenerator<BodyId>
    {
        public BodyIdBuilder() : base(3, BodyId.Parse)
        {
        }
    }
}
