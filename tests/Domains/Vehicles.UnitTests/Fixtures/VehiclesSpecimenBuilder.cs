﻿using AutoFixture.Kernel;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    public class VehiclesSpecimenBuilder : CompositeSpecimenBuilder
    {
        public VehiclesSpecimenBuilder() : base(
            new DomainBuilder(),
            new ModelIdBuilder(),
            new ModelKeyBuilder(),
            new BodyIdBuilder(),
            new EquipmentIdBuilder(),
            new EngineIdBuilder(),
            new GearboxIdBuilder(),
            new GearboxTypeBuilder(),
            new OptionIdBuilder(),
            new PrNumberBuilder(),
            new PrNumberSetBuilder(),
            new ModelKeySetBuilder(),
            new ColorIdBuilder(),
            new ColorTypeIdBuilder(),
            new SystemColorBuilder(),
            new BoundModelKeySetBuilder())
        {
        }
    }
}