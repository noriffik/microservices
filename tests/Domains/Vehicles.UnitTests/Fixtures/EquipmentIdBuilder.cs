﻿using NexCore.Domain.Vehicles;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    public class EquipmentIdBuilder : SequentialEntityIdGenerator<EquipmentId>
    {
        public EquipmentIdBuilder() : base(3, EquipmentId.Parse)
        {
        }
    }
}
