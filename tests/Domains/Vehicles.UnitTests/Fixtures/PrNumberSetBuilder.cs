﻿using AutoFixture;
using AutoFixture.Kernel;
using NexCore.Domain.Vehicles;
using System;
using System.Linq;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    public class PrNumberSetBuilder : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type;
            if (type == null || type != typeof(PrNumberSet))
                return new NoSpecimen();

            return PrNumberSet.Parse(
                Enumerable.Range(0, 5)
                    .Select(n => context.Create<PrNumber>().Value)
                    .Aggregate((a, b) => $"{a} {b}"));
        }
    }
}
