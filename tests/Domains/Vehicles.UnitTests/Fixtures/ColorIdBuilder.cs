﻿using NexCore.Domain.Vehicles;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    class ColorIdBuilder : SequentialEntityIdGenerator<ColorId>
    {
        public ColorIdBuilder() : base(4, ColorId.Parse)
        {
        }
    }
}
