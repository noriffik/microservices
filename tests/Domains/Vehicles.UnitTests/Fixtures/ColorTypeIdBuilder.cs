﻿using AutoFixture;
using AutoFixture.Kernel;
using NexCore.Domain.Vehicles;
using System;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    class ColorTypeIdBuilder : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type;
            if (type == null || type != typeof(ColorTypeId))
                return new NoSpecimen();

            var prNumber = context.Create<PrNumber>();

            return ColorTypeId.Parse(prNumber.Value);
        }
    }
}
