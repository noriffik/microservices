﻿using AutoFixture;
using AutoFixture.Kernel;
using System;
using System.Drawing;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    class SystemColorBuilder : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type;
            if (type == null || type != typeof(Color))
                return new NoSpecimen();
            
            return Color.FromArgb(
                context.Create<byte>(),
                context.Create<byte>(),
                context.Create<byte>(),
                context.Create<byte>());
        }
    }
}