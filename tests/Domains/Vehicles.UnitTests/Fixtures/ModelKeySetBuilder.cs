﻿using AutoFixture;
using AutoFixture.Kernel;
using NexCore.Domain.Vehicles;
using System;
using System.Linq;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    public class ModelKeySetBuilder : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type;
            if (type == null || type != typeof(ModelKeySet))
                return new NoSpecimen();

            return ModelKeySet.Parse(
                Enumerable.Range(0, 5)
                    .Select(n => context.Create<ModelKey>().Value)
                    .Aggregate((a, b) => $"{a} {b}"));
        }
    }
}
