﻿using NexCore.Domain.Vehicles;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    public class OptionIdBuilder : SequentialEntityIdGenerator<OptionId>
    {
        public OptionIdBuilder() : base(5, OptionId.Parse)
        {
        }
    }
}
