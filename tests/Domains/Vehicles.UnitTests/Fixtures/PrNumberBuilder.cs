﻿using NexCore.Domain.Vehicles;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    class PrNumberBuilder : SequentialEntityIdGenerator<PrNumber>
    {
        public PrNumberBuilder() : base(3, PrNumber.Parse)
        {
        }
    }
}
