﻿using NexCore.Domain.Vehicles;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    public class EngineIdBuilder : SequentialEntityIdGenerator<EngineId>
    {
        public EngineIdBuilder() : base(3, EngineId.Parse)
        {
        }
    }
}
