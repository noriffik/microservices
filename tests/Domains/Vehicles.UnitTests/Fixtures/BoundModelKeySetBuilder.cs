﻿using AutoFixture;
using AutoFixture.Kernel;
using NexCore.Domain.Vehicles;
using System;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    public class BoundModelKeySetBuilder : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type;
            if (type == null || type != typeof(BoundModelKeySet))
                return new NoSpecimen();
            
            return new BoundModelKeySet(context.CreateMany<ModelKey>(5));
        }
    }
}
