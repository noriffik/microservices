﻿using AutoFixture.Kernel;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Fixtures.SpecimenBuilders.Strings;
using System;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    public class GearboxTypeBuilder : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type;
            if (type == null || type != typeof(GearboxType))
                return new NoSpecimen();

            return GearboxType.Parse((string) context.Resolve(new AlphaNumericString(3)));
        }
    }
}
