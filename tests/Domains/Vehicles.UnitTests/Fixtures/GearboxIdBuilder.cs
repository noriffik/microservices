﻿using NexCore.Domain.Vehicles;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;

namespace NexCore.Vehicles.UnitTests.Fixtures
{
    public class GearboxIdBuilder : SequentialEntityIdGenerator<GearboxId>
    {
        public GearboxIdBuilder() : base(3, GearboxId.Parse)
        {
        }
    }
}
