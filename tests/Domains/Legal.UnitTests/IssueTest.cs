﻿using System;
using Xunit;

namespace NexCore.Legal.UnitTests
{
    public class IssueTest
    {
        [Fact]
        public void Ctor()
        {
            //Arrange
            var issuer = "Issuer";
            var issuedOn = DateTime.MaxValue;

            //Act
            var issue = new Issue(issuer, issuedOn);

            //Assert
            Assert.Equal(issuer, issue.Issuer);
            Assert.Equal(issuedOn.Date, issue.IssuedOn);
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void Ctor_GivenIssuer_IsEmpty_Throws(string issuer)
        {
            Assert.Throws<ArgumentException>("issuer", () => new Issue(issuer, DateTime.MaxValue));
        }

        [Fact]
        public void Empty()
        {
            //Act
            var issue = Issue.Empty;

            //Assert
            Assert.Null(issue.Issuer);
            Assert.Null(issue.IssuedOn);
        }

        [Fact]
        public void Assign()
        {
            //Arrange
            var expectedIssue = new Issue("expectedIssue", DateTime.MinValue);
            var actualIssue = new Issue("expectedIssue", DateTime.MaxValue);

            //Act
            actualIssue.Assign(expectedIssue);

            //Assert
            Assert.Equal(expectedIssue, actualIssue);
        }
    }
}
