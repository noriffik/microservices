﻿using AutoFixture;
using System;
using Xunit;

namespace NexCore.Legal.UnitTests
{
    public class TaxIdentificationNumberTest
    {
        private readonly string _number;
        private readonly DateTime _registeredOn;
        private readonly Issue _issue;

        public TaxIdentificationNumberTest()
        {
            var fixture = new Fixture();

            _number = fixture.Create<string>();
            _registeredOn = fixture.Create<DateTime>();
            _issue = fixture.Create<Issue>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var result = new TaxIdentificationNumber(_number, _registeredOn, _issue);

            //Assert
            Assert.Equal(_number, result.Number);
            Assert.Equal(_registeredOn.Date, result.RegisteredOn);
            Assert.Equal(_issue, result.Issue);
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void Ctor_GivenNumber_IsEmpty_Throws(string number)
        {
            Assert.Throws<ArgumentException>("number", () => new TaxIdentificationNumber(number, _registeredOn, _issue));
        }

        [Fact]
        public void Ctor_GivenIssue_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("issue", () => new TaxIdentificationNumber(_number, _registeredOn, null));
        }

        [Fact]
        public void Empty()
        {
            //Act
            var result = TaxIdentificationNumber.Empty;

            //Assert
            Assert.Null(result.Number);
            Assert.Null(result.RegisteredOn);
            Assert.Equal(Issue.Empty, result.Issue);
        }

        [Fact]
        public void Assign()
        {
            //Arrange
            var expectedNumber = new TaxIdentificationNumber(_number, _registeredOn, _issue);
            var actualNumber = new TaxIdentificationNumber("number", _registeredOn.AddDays(4), Issue.Empty);

            //Act
            actualNumber.Assign(expectedNumber);

            //Assert
            Assert.Equal(expectedNumber, actualNumber);
        }
    }
}
