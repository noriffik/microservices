﻿using System;
using Xunit;

namespace NexCore.Legal.UnitTests
{
    public class PassportTest
    {
        private const string Series = "Series";
        private const string Number = "Number";
        private readonly Issue _issue = new Issue("Issuer", DateTime.MinValue);

        [Fact]
        public void Ctor()
        {
            //Act
            var passport = new Passport(Series, Number, _issue);

            //Assert
            Assert.Equal(Series.ToUpperInvariant(), passport.Series);
            Assert.Equal(Number.ToUpperInvariant(), passport.Number);
            Assert.Equal(_issue, passport.Issue);
        }

        [Fact]
        public void Ctor_GivenSeries_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("series", () => new Passport(null, Number, _issue));
        }

        [Fact]
        public void Ctor_GivenNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("number", () => new Passport(Series, null, _issue));
        }

        [Fact]
        public void Ctor_GivenIssue_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("issue", () => new Passport(Series, Number, null));
        }

        [Fact]
        public void Empty()
        {
            //Act
            var passport = Passport.Empty;

            //Assert
            Assert.Null(passport.Series);
            Assert.Null(passport.Number);
            Assert.Equal(Issue.Empty, passport.Issue);
        }

        [Fact]
        public void Assign()
        {
            //Arrange
            var expectedPassport = new Passport("ExpectedSeries", "ExpectedNumber", _issue);
            var actualPassport = new Passport(Series, Number, Issue.Empty);

            //Act
            actualPassport.Assign(expectedPassport);

            //Assert
            Assert.Equal(expectedPassport, actualPassport);
        }
    }
}
