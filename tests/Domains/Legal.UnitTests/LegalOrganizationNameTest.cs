﻿using System;
using Xunit;

namespace NexCore.Legal.UnitTests
{
    public class LegalOrganizationNameTest
    {
        private const string ShortName = "Short name";
        private const string FullName = "Full name";

        [Fact]
        public void Ctor()
        {
            //Act
            var name = new LegalOrganizationName(ShortName, FullName);

            //Assert
            Assert.Equal(ShortName, name.ShortName);
            Assert.Equal(FullName, name.FullName);
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void Ctor_GivenShortName_IsNull_Throws(string value)
        {
            Assert.Throws<ArgumentException>("shortName", () => new LegalOrganizationName(value, FullName));
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void Ctor_GivenFullName_IsNull_Throws(string value)
        {
            Assert.Throws<ArgumentException>("fullName", () => new LegalOrganizationName(ShortName, value));
        }

        [Fact]
        public void Empty()
        {
            //Act
            var name = LegalOrganizationName.Empty;

            //Assert
            Assert.Null(name.ShortName);
            Assert.Null(name.FullName);
        }

        [Fact]
        public void Assign()
        {
            //Arrange
            var expectedName = new LegalOrganizationName("expectedShortName", "expectedFullName");
            var actualName = new LegalOrganizationName(ShortName, FullName);

            //Act
            actualName.Assign(expectedName);

            //Assert
            Assert.Equal(expectedName, actualName);
        }
    }
}
