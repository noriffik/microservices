﻿using System;
using Xunit;

namespace NexCore.Legal.UnitTests
{
    public class LegalPersonNameTest
    {
        private const string Firstname = "Firstname";
        private const string Lastname = "Lastname";
        private const string Middlename = "Middlename";

        [Theory]
        [InlineData(Firstname, Lastname, Middlename)]
        [InlineData(" " + Firstname + "  ", " " + Lastname + "  ", "   " + Middlename + " ")]
        [InlineData("FIRSTNAME", "LASTNAME", "MiDdLeNaME")]
        [InlineData("firstname", "lastNAME", "middleName")]
        public void Ctor(string firstname, string lastname, string middlename)
        {
            //Act
            var name = new LegalPersonName(firstname, lastname, middlename);

            //Assert
            Assert.Equal(Firstname, name.Firstname);
            Assert.Equal(Lastname, name.Lastname);
            Assert.Equal(Middlename, name.Middlename);
        }

        [Theory]
        [InlineData("", Lastname, Middlename, "firstname")]
        [InlineData(Firstname, "", Middlename, "lastname")]
        [InlineData(Firstname, Lastname, "", "middlename")]
        [InlineData(" ", Lastname, Middlename, "firstname")]
        [InlineData(Firstname, " ", Middlename, "lastname")]
        [InlineData(Firstname, Lastname, " ", "middlename")]
        [InlineData(null, Lastname, Middlename, "firstname")]
        [InlineData(Firstname, null, Middlename, "lastname")]
        [InlineData(Firstname, Lastname, null, "middlename")]
        public void Ctor_OneOfGivenParameters_IsEmpty(string firstname, string lastname, string middlename, string paramName)
        {
            Assert.Throws<ArgumentException>(paramName, () => new LegalPersonName(firstname, lastname, middlename));
        }

        [Fact]
        public void ShortName()
        {
            //Arrange
            var name = new LegalPersonName("Firstname", "Lastname", "Middlename");
            var expected = "F.M.Lastname";

            //Act
            var actual = name.ShortName;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ShortName_WhenName_IsEmpty_ReturnsNull()
        {
            //Arrange
            var name = LegalPersonName.Empty;

            //Act
            var shortName = name.ShortName;

            //Assert
            Assert.Null(shortName);
        }

        [Fact]
        public void Empty()
        {
            //Act
            var name = LegalPersonName.Empty;

            //Assert
            Assert.Null(name.Firstname);
            Assert.Null(name.Lastname);
            Assert.Null(name.Middlename);
        }

        [Fact]
        public void Assign()
        {
            //Arrange
            var expectedName = new LegalPersonName("expectedFirstname", "expectedLastname", "expectedMiddlename");
            var actualName = new LegalPersonName(Firstname, Lastname, Middlename);

            //Act
            actualName.Assign(expectedName);

            //Assert
            Assert.Equal(expectedName, actualName);
        }

        [Fact]
        public void ToString_ReturnsFullName()
        {
            //Arrange
            const string fullName = "Lastname Firstname Middlename";
            var name = new PersonName(Firstname, Lastname, Middlename);

            //Act
            var actual = name.ToString();

            //Assert
            Assert.Equal(fullName, actual);
        }
    }
}
