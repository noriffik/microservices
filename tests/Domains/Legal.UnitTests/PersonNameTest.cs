﻿using System;
using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace NexCore.Legal.UnitTests
{
    public class PersonNameTest
    {
        private const string Firstname = "Firstname";
        private const string Lastname = "Lastname";
        private const string Middlename = "Middlename";

        [Theory]
        [InlineData(Firstname, Lastname, Middlename)]
        [InlineData(" " + Firstname + "  ", " " + Lastname + "  ", "   " + Middlename + " ")]
        [InlineData("FIRSTNAME", "LASTNAME", "MiDdLeNaME")]
        [InlineData("firstname", "lastNAME", "middleName")]
        public void Ctor(string firstname, string lastname, string middlename)
        {
            //Act
            var name = new PersonName(firstname, lastname, middlename);

            //Assert
            Assert.Equal(Firstname, name.Firstname);
            Assert.Equal(Lastname, name.Lastname);
            Assert.Equal(Middlename, name.Middlename);
        }

        [Theory]
        [InlineData("", Lastname, Middlename)]
        [InlineData(" ", Lastname, Middlename)]
        [InlineData(null, Lastname, Middlename)]
        public void Ctor_GivenFirstname_IsEmpty(string firstname, string lastname, string middlename)
        {
            //Act
            var name = new PersonName(firstname, lastname, middlename);

            //Assert
            Assert.Null(name.Firstname);
            Assert.Equal(lastname, name.Lastname);
            Assert.Equal(middlename, name.Middlename);
        }

        [Theory]
        [InlineData(Firstname, "", Middlename)]
        [InlineData(Firstname, " ", Middlename)]
        [InlineData(Firstname, null, Middlename)]
        public void Ctor_GivenLastname_IsEmpty(string firstname, string lastname, string middlename)
        {
            //Act
            var name = new PersonName(firstname, lastname, middlename);

            //Assert
            Assert.Equal(firstname, name.Firstname);
            Assert.Null(name.Lastname);
            Assert.Equal(middlename, name.Middlename);
        }

        [Theory]
        [InlineData(Firstname, Lastname, "")]
        [InlineData(Firstname, Lastname, " ")]
        [InlineData(Firstname, Lastname, null)]
        public void Ctor_GivenMiddlename_IsEmpty(string firstname, string lastname, string middlename)
        {
            //Act
            var name = new PersonName(firstname, lastname, middlename);

            //Assert
            Assert.Equal(firstname, name.Firstname);
            Assert.Equal(lastname, name.Lastname);
            Assert.Null(name.Middlename);
        }

        [Theory]
        [InlineData("", "", "")]
        [InlineData(" ", " ", " ")]
        [InlineData(null, null, null)]
        public void PersonName_Given_AllParts_AreInvalid_Throws(string firstname, string lastname, string middlename)
        {
            Assert.Throws<ArgumentException>(() => new PersonName(firstname, lastname, middlename));
        }

        [Theory]
        [ClassData(typeof(EqualityTestData))]
        public void Equality(string firstname, string lastname, string middlename, bool isEqual)
        {
            //Arrange
            var objA = new PersonName("firstname", "lastname", "middlename");
            var objB = new PersonName(firstname, lastname, middlename);

            //Assert
            Assert.True(Equals(objA, objB) == isEqual);
        }

        [Theory]
        [ClassData(typeof(EqualityTestData))]
        public void EqualsOperator(string firstname, string lastname, string middlename, bool isEqual)
        {
            //Arrange
            var objA = new PersonName("firstname", "lastname", "middlename");
            var objB = new PersonName(firstname, lastname, middlename);

            //Assert
            Assert.True((objA == objB) == isEqual);
        }

        [Theory]
        [ClassData(typeof(EqualityTestData))]
        public void NotEqualsOperator(string firstname, string lastname, string middlename, bool isEqual)
        {
            //Arrange
            var objA = new PersonName("firstname", "lastname", "middlename");
            var objB = new PersonName(firstname, lastname, middlename);

            //Assert
            Assert.True((objA != objB) != isEqual);
        }

        [Theory]
        [ClassData(typeof(EqualityTestData))]
        public void Hashing(string firstname, string lastname, string middlename, bool isEqual)
        {
            //Arrange
            var hashA = new PersonName("firstname", "lastname", "middlename").GetHashCode();
            var hashB = new PersonName(firstname, lastname, middlename).GetHashCode();

            //Assert
            Assert.True(Equals(hashA, hashB) == isEqual);
        }

        [Theory]
        [InlineData("Firstname", "Lastname", "Middlename", "Lastname Firstname Middlename")]
        [InlineData(null, "Lastname", "Middlename", "Lastname Middlename")]
        [InlineData("Firstname", null, "Middlename", "Firstname Middlename")]
        [InlineData("Firstname", "Lastname", null, "Lastname Firstname")]
        public void ToString_ReturnsFullName(string firstname, string lastname, string middlename, string expected)
        {
            //Arrange
            var name = new PersonName(firstname, lastname, middlename);

            //Act
            var actual = name.ToString();

            //Assert
            Assert.Equal(expected, actual);
        }

        class EqualityTestData : IEnumerable<object[]>
        {
            private readonly List<object[]> _data = new List<object[]>
            {
                new object[] { "firstname", "lastname", "middlename", true },
                new object[] { "other", "lastname", "middlename", false },
                new object[] { "firstname", "other", "middlename", false },
                new object[] { "firstname", "lastname", "other", false }
            };

            public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        [Fact]
        public void Assign()
        {
            //Arrange
            var expectedName = new PersonName("expectedFirstname", "expectedLastname", "expectedMiddlename");
            var actualName = new PersonName(Firstname, Lastname, Middlename);

            //Act
            actualName.Assign(expectedName);

            //Assert
            Assert.Equal(expectedName, actualName);
        }
    }
}
