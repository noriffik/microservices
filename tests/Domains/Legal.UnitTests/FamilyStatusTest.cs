﻿using System;
using Xunit;

namespace NexCore.Legal.UnitTests
{
    public class FamilyStatusTest
    {
        [Fact]
        public void Get()
        {
            //Act
            var result = FamilyStatus.Get(FamilyStatus.Single.Id);

            //Assert
            Assert.Equal(FamilyStatus.Single, result);
        }

        [Fact]
        public void Get_GivenId_IsInvalid_Throws()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => FamilyStatus.Get(100));
        }

        [Fact]
        public void Has()
        {
            //Act
            var result = FamilyStatus.Has(2);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Has_ReturnsFalse()
        {
            //Act
            var result = FamilyStatus.Has(100);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void Equals()
        {
	        //Arrange
	        var status = FamilyStatus.Single;
	
	        //Assert
	        Assert.Equal(status, FamilyStatus.Single);
	        Assert.False(status.Equals(null));
	        Assert.False(status.Equals(FamilyStatus.Married));
        }

        [Fact]
        public void Hashing()
        {
            //Act
            var hash = FamilyStatus.Divorced.GetHashCode();

            //Assert
            Assert.Equal(hash, FamilyStatus.Divorced.GetHashCode());
            Assert.NotEqual(hash, FamilyStatus.Single.GetHashCode());
            Assert.NotEqual(hash, FamilyStatus.Divorced.Id.GetHashCode());
        }
    }
}
