﻿using AutoFixture;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.Legal.UnitTests
{
    public class OrganizationRequisitesTest
    {
        private readonly LegalOrganizationName _name;
        private readonly string _address;
        private readonly IEnumerable<string> _telephones;
        private readonly string _bankRequisites;
        private readonly LegalPersonName _director;

        public OrganizationRequisitesTest()
        {
            var fixture = new Fixture();

            _name = fixture.Create<LegalOrganizationName>();
            _address = fixture.Create<string>();
            _telephones = fixture.Create<IEnumerable<string>>();
            _bankRequisites = fixture.Create<string>();
            _director = fixture.Create<LegalPersonName>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var requisites = new OrganizationRequisites(_name, _address, _telephones, _bankRequisites, _director);

            //Assert
            Assert.Equal(_name, requisites.Name);
            Assert.Equal(_address, requisites.Address);
            Assert.Equal(_telephones, requisites.Telephones);
            Assert.Equal(_bankRequisites, requisites.BankRequisites);
            Assert.Equal(_director, requisites.Director);
        }

        public static IEnumerable<object[]> EmptyNameData =>
            new List<object[]>
            {
                new object[] { null },
                new object[] { LegalOrganizationName.Empty }
            };

        [Theory]
        [MemberData(nameof(EmptyNameData))]
        public void Ctor_GivenName_IsEmpty_Throws(LegalOrganizationName name)
        {
            Assert.Throws<ArgumentException>("name", () =>
                new OrganizationRequisites(name, _address, _telephones, _bankRequisites, _director));
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void Ctor_GivenAddress_IsEmpty_Throws(string address)
        {
            Assert.Throws<ArgumentException>("address", () =>
                new OrganizationRequisites(_name, address, _telephones, _bankRequisites, _director));
        }

        [Fact]
        public void Ctor_GivenTelephones_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("telephones", () =>
                new OrganizationRequisites(_name, _address, null, _bankRequisites, _director));
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void Ctor_GivenBankRequisites_IsEmpty_Throws(string bankRequisites)
        {
            Assert.Throws<ArgumentException>("bankRequisites", () =>
                new OrganizationRequisites(_name, _address, _telephones, bankRequisites, _director));
        }

        public static IEnumerable<object[]> EmptyDirectorData =>
            new List<object[]>
            {
                new object[] { null },
                new object[] { LegalPersonName.Empty }
            };

        [Theory]
        [MemberData(nameof(EmptyDirectorData))]
        public void Ctor_GivenDirector_IsNull_Throws(LegalPersonName director)
        {
            Assert.Throws<ArgumentException>("director", () =>
                new OrganizationRequisites(_name, _address, _telephones, _bankRequisites, director));
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void Ctor_OneOfGivenTelephones_IsEmpty_Throws(string telephone)
        {
            Assert.Throws<ArgumentException>(() => new OrganizationRequisites(
                _name, _address, _telephones.Concat(new[] { telephone }), _bankRequisites, _director));
        }

        [Fact]
        public void Empty()
        {
            //Act
            var requisites = OrganizationRequisites.Empty;

            //Assert
            Assert.Equal(LegalOrganizationName.Empty, requisites.Name);
            Assert.Null(requisites.Address);
            Assert.Empty(requisites.Telephones);
            Assert.Null(requisites.BankRequisites);
            Assert.Equal(LegalPersonName.Empty, requisites.Director);
        }

        [Fact]
        public void Assign()
        {
            //Arrange
            var expectedRequisites = new OrganizationRequisites(_name, _address, _telephones, _bankRequisites, _director);
            var actualRequisites = OrganizationRequisites.Empty;

            //Act
            actualRequisites.Assign(expectedRequisites);

            //Assert
            Assert.Equal(expectedRequisites, actualRequisites);
        }
    }
}
