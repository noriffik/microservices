﻿using System;
using System.Collections.Generic;
using Xunit;

namespace NexCore.DealerNetwork.UnitTests
{
    public class DistributorIdTest
    {
        public static IEnumerable<object[]> ValidDistributorIdData => new List<object[]>
        {
            new object[] { "PH9" }, new object[] { "pkV" }, new object[] { "000" }
        };

        public static IEnumerable<object[]> InvalidDistributorIdData => new List<object[]>
        {
            new object[] { "" }, new object[] { "   " }, new object[] { " nS" },
            new object[] { "PH91" }, new object[] { "nS" }, new object[] { "%nS" }
        };

        [Theory]
        [MemberData(nameof(ValidDistributorIdData))]
        public void Parse(string value)
        {
            //Act
            var distributorId = DistributorId.Parse(value);

            //Assert
            Assert.NotNull(distributorId);
            Assert.Equal(value.ToUpperInvariant(), distributorId.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("value", () => DistributorId.Parse(null));
        }

        [Theory]
        [MemberData(nameof(InvalidDistributorIdData))]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() => DistributorId.Parse(value));
        }

        [Theory]
        [MemberData(nameof(ValidDistributorIdData))]
        public void TryParse(string value)
        {
            //Act
            var result = DistributorId.TryParse(value, out var distributorId);

            //Assert
            Assert.True(result);
            Assert.NotNull(distributorId);
            Assert.Equal(value.ToUpperInvariant(), distributorId.Value);
        }

        [Theory]
        [InlineData(null)]
        [MemberData(nameof(InvalidDistributorIdData))]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = DistributorId.TryParse(value, out var distributorId);

            //Assert
            Assert.False(result);
            Assert.Null(distributorId);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var distributorId = DistributorId.Parse("PH9");

            //Assert
            Assert.Equal(distributorId.Value, distributorId.ToString());
        }
    }
}
