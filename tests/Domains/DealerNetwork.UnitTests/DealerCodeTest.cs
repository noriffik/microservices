﻿using System;
using System.Collections.Generic;
using Xunit;

namespace NexCore.DealerNetwork.UnitTests
{
    public class DealerCodeTest
    {
        public static IEnumerable<object[]> ValidDealerCodeData =>
            new List<object[]>
            {
                new object[] { "38001" }, new object[] { "00000" }, new object[] { "99999" }
            };

        public static IEnumerable<object[]> InvalidDealerCodeData =>
            new List<object[]>
            {
                new object[] { "" }, new object[] { "  " }, new object[] { " " },
                new object[] { "3800" }, new object[] { "380011" },
                new object[] { "PH91" }, new object[] { "PH91b" }, new object[] { "PH91b1" },
                new object[] { "-1232" }, new object[] { "-12321" }, new object[] {  "-12321" }, new object[] { "+1232" }, new object[] { "+12321" },
                new object[] { "38001 " }, new object[] { " 38001" }, new object[] { "38 001" }, new object[] { "    38001" },
            };

        [Theory]
        [MemberData(nameof(ValidDealerCodeData))]
        public void Parse(string value)
        {
            //Act
            var dealerCode = DealerCode.Parse(value);

            //Assert
            Assert.NotNull(dealerCode);
            Assert.Equal(value.ToUpperInvariant(), dealerCode.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            //Act
            var e = Assert.Throws<ArgumentNullException>(() => DealerCode.Parse(null));

            //Assert
            Assert.Equal("value", e.ParamName);
        }


        [Theory]
        [MemberData(nameof(InvalidDealerCodeData))]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() => DealerCode.Parse(value));
        }

        [Theory]
        [MemberData(nameof(ValidDealerCodeData))]
        public void TryParse(string value)
        {
            //Act
            var result = DealerCode.TryParse(value, out var dealerCode);

            //Assert
            Assert.True(result);
            Assert.NotNull(dealerCode);
            Assert.Equal(value.ToUpperInvariant(), dealerCode.Value);
        }

        [Theory]
        [InlineData(null)]
        [MemberData(nameof(InvalidDealerCodeData))]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = DealerCode.TryParse(value, out var dealerCode);

            //Assert
            Assert.False(result);
            Assert.Null(dealerCode);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var dealerCode = DealerCode.Parse("38001");

            //Assert
            Assert.Equal(dealerCode.Value, dealerCode.ToString());
        }
    }
}
