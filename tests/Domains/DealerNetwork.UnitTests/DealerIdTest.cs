﻿using System;
using System.Collections.Generic;
using Xunit;

namespace NexCore.DealerNetwork.UnitTests
{
    public class DealerIdTest
    {
        public static IEnumerable<object[]> ValidDealerIdData =>
            new List<object[]>
            {
                new object[] { "EKK38001" }, new object[] { "00000000" }, new object[] { "ekk99999" }
            };

        public static IEnumerable<object[]> InvalidDealerIdData =>
            new List<object[]>
            {
                new object[] { "" }, new object[] { "  " }, new object[] { " " },
                new object[] { "EKK3800" }, new object[] { "EKK380011" },
                new object[] { "EKKPH91" }, new object[] { "EKKPH91b" }, new object[] { "EKKPH91b1" },
                new object[] { "EKK38001 " }, new object[] { " EKK38001" }, new object[] { "EKK38 001" }
            };

        private readonly DistributorId _distributorId = DistributorId.Parse("EKK");
        private readonly DealerCode _dealerCode = DealerCode.Parse("38001");

        [Theory]
        [MemberData(nameof(ValidDealerIdData))]
        public void Parse(string value)
        {
            //Act
            var dealerId = DealerId.Parse(value);

            //Assert
            Assert.NotNull(dealerId);
            Assert.Equal(value.ToUpperInvariant(), dealerId.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("value", () => DealerId.Parse(null));
        }

        [Theory]
        [MemberData(nameof(InvalidDealerIdData))]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() => DealerId.Parse(value));
        }

        [Theory]
        [MemberData(nameof(ValidDealerIdData))]
        public void TryParse(string value)
        {
            //Act
            var result = DealerId.TryParse(value, out var dealerId);

            //Assert
            Assert.True(result);
            Assert.NotNull(dealerId);
            Assert.Equal(value.ToUpperInvariant(), dealerId.Value);
        }

        [Theory]
        [InlineData(null)]
        [MemberData(nameof(InvalidDealerIdData))]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = DealerId.TryParse(value, out var dealerId);

            //Assert
            Assert.False(result);
            Assert.Null(dealerId);
        }

        [Fact]
        public void Next()
        {
            //Arrange
            var expectedValue = _distributorId.Value + _dealerCode.Value;

            //Act
            var dealerId = DealerId.Next(_distributorId, _dealerCode);

            //Assert
            Assert.Equal(expectedValue, dealerId.Value);
        }

        [Fact]
        public void Next_GivenDistributorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("distributorId", () => DealerId.Next(null, _dealerCode));
        }

        [Fact]
        public void Next_GivenDealerCode_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealerCode", () => DealerId.Next(_distributorId, null));
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var dealerId = DealerId.Next(_distributorId, _dealerCode);

            //Assert
            Assert.Equal(dealerId.Value, dealerId.ToString());
        }

        [Fact]
        public void DistributorIdProperty()
        {
            //Arrange
            var dealerId = DealerId.Next(_distributorId, _dealerCode);

            //Assert
            Assert.Equal(_distributorId, dealerId.DistributorId);
        }

        [Fact]
        public void DealerCodeProperty()
        {
            //Arrange
            var dealerId = DealerId.Next(_distributorId, _dealerCode);

            //Assert
            Assert.Equal(_dealerCode, dealerId.DealerCode);
        }
    }
}
