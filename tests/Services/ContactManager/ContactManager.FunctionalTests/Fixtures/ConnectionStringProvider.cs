﻿namespace NexCore.ContactManager.FunctionalTests.Fixtures
{
    class ConnectionStringProvider
    {
        public static string Get(string name) => TestConfiguration.Get["ConnectionString"]
            .Replace("%DATABASE%", $"NexCore.Services.ContactManager_{name}");
    }
}
