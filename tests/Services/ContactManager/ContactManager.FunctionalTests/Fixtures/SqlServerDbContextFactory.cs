﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NexCore.ContactManager.Infrastructure;
using System.Reflection;

namespace NexCore.ContactManager.FunctionalTests.Fixtures
{
    public class SqlServerDbContextFactory
    {
        public ContactContext Create(string databaseName)
        {
            var connectionString = ConnectionStringProvider.Get(databaseName);

            var migrationAssembly = typeof(ContactContext).GetTypeInfo().Assembly.GetName().Name;

            var services = new ServiceCollection()
                .AddEntityFrameworkSqlServer();

            var options = new DbContextOptionsBuilder<ContactContext>()
                .UseInternalServiceProvider(services.BuildServiceProvider())
                .UseSqlServer(
                    connectionString,
                    sqlOptions => {sqlOptions
                        .MigrationsAssembly(migrationAssembly)
                        .EnableRetryOnFailure(10);
                    })
                .Options;

            var mediator = new Mock<IMediator>();

            return new ContactContext(options, mediator.Object);
        }
    }
}
