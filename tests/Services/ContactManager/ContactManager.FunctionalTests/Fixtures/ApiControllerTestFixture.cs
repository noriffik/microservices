﻿using Microsoft.Extensions.DependencyInjection;
using NexCore.ContactManager.FunctionalTests.Api;
using NexCore.ContactManager.Infrastructure;
using System;
using System.Net.Http;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Fixtures
{
    public abstract class ApiControllerTestFixture : IClassFixture<ServiceFactory<TestStartup>>, IDisposable
    {
        protected readonly HttpClient Client;
        protected readonly ContactContext DbContext;

        private readonly IServiceScope _scope;

        protected ApiControllerTestFixture(ServiceFactory<TestStartup> factory)
        {
            Client = factory.CreateClient();
            _scope = factory.CreateScope();
            DbContext = _scope.ServiceProvider.GetRequiredService<ContactContext>();
        }
        
        public void Dispose()
        {
            DbContext.Database.EnsureDeleted();
            
            _scope.Dispose();
        }
    }
}
