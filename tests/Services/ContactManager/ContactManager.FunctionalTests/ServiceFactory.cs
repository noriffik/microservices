﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NexCore.ContactManager.Infrastructure;
using NexCore.EventBus.Abstract;
using NexCore.Infrastructure.Utility;
using NexCore.Testing.Extensions.Moq;
using System.Collections.Generic;
using System.Security.Claims;

namespace NexCore.ContactManager.FunctionalTests
{
    public class ServiceFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            base.ConfigureWebHost(builder);

            builder.UseContentRoot(".");

            builder.ConfigureServices(services =>
            {
                var serviceProvider = new ServiceCollection()
                    .AddEntityFrameworkInMemoryDatabase()
                    .BuildServiceProvider();

                services.AddScoped(c =>
                {
                    var context = new ContactContext(new DbContextOptionsBuilder<ContactContext>()
                        .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                        .UseInMemoryDatabase(nameof(ContactContext))
                        .UseInternalServiceProvider(serviceProvider)
                        .Options)
                    {
                        ResilientTransaction = ResilientTransactionStub()
                    };

                    //Eager load dealer entities to enable InMemory database testing
                    context.Distributors.Load();

                    return context;
                });

                services.AddScoped(c =>
                {
                    var service = new Mock<IAuthorizationService>();

                    service.Setup(s => s.AuthorizeAsync(It.IsAny<ClaimsPrincipal>(), It.IsAny<object>(),
                            It.IsAny<IEnumerable<IAuthorizationRequirement>>()))
                        .ReturnsAsync(AuthorizationResult.Success);

                    return service.Object;
                });

                services.AddScoped(s => new Mock<IEventBus>().Object);
            });
        }

        private static IResilientTransaction ResilientTransactionStub()
        {
            var transaction = new Mock<IResilientTransaction>();

            transaction.SetupExecuteAsync();

            return transaction.Object;
        }

        protected override IWebHostBuilder CreateWebHostBuilder()
        {
            return WebHost.CreateDefaultBuilder()
                .UseStartup<TStartup>();
        }

        public IServiceScope CreateScope()
        {
            return Server.Host.Services.CreateScope();
        }
    }
}
