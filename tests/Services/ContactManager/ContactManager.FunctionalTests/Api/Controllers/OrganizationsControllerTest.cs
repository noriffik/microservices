﻿using AutoFixture;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Organizations.Commands;
using NexCore.ContactManager.Application.Organizations.Commands.AddOrganization;
using NexCore.ContactManager.Application.Organizations.Dtos;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Testing.Extensions;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers
{
    public class OrganizationsControllerTest : ApiControllerTestFixture
    {
        private static readonly IFixture Fixture = new Fixture { RepeatCount = 1 }
            .Customize(new ContactModelCustomization());

        private const int OrganizationId = 10;
        private static readonly DealerId DealerId = Fixture.Create<DealerId>();
        private const int DealerCorporateCustomerId = 30;
        private const string Edrpou = "0000001";
        private const string Ipn = "0000002";

        private const string BaseUrl = "api/contacts/organizations";

        public static IEnumerable<object[]> FindSingleEndpoints = CommandEndpoint.Wrap(
            $"{BaseUrl}/{OrganizationId}");

        public static IEnumerable<object[]> FindManyEndpoints = CommandEndpoint.Wrap(
            $"{BaseUrl}/{Edrpou}",
            BaseUrl);

        private UpdateOrganizationCommand updateOrganizationCommand = new UpdateOrganizationCommand
            {
                Id = OrganizationId,
                Name = Fixture.Create<string>(),
                Identity = new IdentityDto { Edrpou = Edrpou },
                Requisites = Fixture.Create<OrganizationRequisitesDto>(),
                Contact = Fixture.Create<ContactDto>()
            };
        
        public OrganizationsControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Theory]
        [MemberData(nameof(FindSingleEndpoints))]
        public async Task FindSingle(string point)
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync(point);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Theory]
        [MemberData(nameof(FindSingleEndpoints))]
        public async Task FindSingle_WhenOrganization_NotFound_ReturnsNotFound(string point)
        {
            //Act
            var response = await Client.GetAsync(point);

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Theory]
        [MemberData(nameof(FindManyEndpoints))]
        public async Task Get(string point)
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync(point);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Post()
        {
            //Arrange
            var command = Fixture.Build<AddOrganizationCommand>()
                .With(c => c.Identity, new IdentityDto { Edrpou = Edrpou })
                .Create();

            //Act
            var response = await Client.PostCommand(BaseUrl, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Post_WhenGivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PostCommand(BaseUrl, null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Put()
        {
            //Arrange
            await Seed();
            
            //Act
            var response = await Client.PutCommand(BaseUrl, updateOrganizationCommand);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Put_WhenOrganization_NotFound_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PutCommand(BaseUrl, updateOrganizationCommand);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Put_WhenGivenCommand_IsNull_ReturnsBadRequest()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.PutCommand(BaseUrl, "invalid");

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        private async Task Seed()
        {
            var dealerSeeder = new DealerSeeder(DbContext);

            await dealerSeeder.Add<Organization>(OrganizationId, new { identity = Identity.WithEdrpou(Edrpou) });
            await dealerSeeder.Add<Organization>(OrganizationId + 1, new { identity = Identity.WithIpn(Ipn) });

            await dealerSeeder.Add(DealerId, new { organizationId = OrganizationId });

            await DbContext.SaveChangesAsync();
        }
    }
}
