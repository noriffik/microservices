﻿using AutoFixture;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.AddDealer;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.UpdateDealer;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Testing.Extensions;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers
{
    public class DealersControllerTest : ApiControllerTestFixture
    {
        private static readonly IFixture Fixture = new Fixture { RepeatCount = 1 }
            .Customize(new ContactModelCustomization())
            .Customize(new DealerIdCustomization());

        private const int OrganizationId = 10;
        private const int UnregisteredOrganizationId = 11;
        private readonly DealerId _dealerId;

        private readonly string _baseUrl;

        public DealersControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
            _dealerId = Fixture.Create<DealerId>();
            _baseUrl = $"api/distributors/{_dealerId.DistributorId.Value}/dealers";
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync($"{_baseUrl}/{_dealerId.DealerCode}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenDealer_NotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{_baseUrl}/{_dealerId.DealerCode}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task Post()
        {
            //Arrange
            await Seed();

            var command = Fixture.Build<AddDealerCommand>()
                .With(c => c.OrganizationId, UnregisteredOrganizationId)
                .With(c => c.Code, Fixture.Create<DealerCode>().Value)
                .Without(c => c.DistributorId)
                .Create();

            //Act
            var response = await Client.PostCommand(_baseUrl, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Post_WhenOrganization_NotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Build<AddDealerCommand>()
                .With(c => c.Code, Fixture.Create<DealerCode>().Value)
                .Without(c => c.DistributorId)
                .Create();

            //Act
            var response = await Client.PostCommand(_baseUrl, command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Post_WhenGivenCommand_IsNull_ReturnsBadRequest()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.PostCommand(_baseUrl, "invalid");

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Put()
        {
            //Arrange
            await Seed();

            var command = Fixture.Build<UpdateDealerCommand>()
                .Without(c => c.DistributorId)
                .Without(c => c.Code)
                .Create();

            //Act
            var response = await Client.PutCommand($"{_baseUrl}/{_dealerId.DealerCode.Value}", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }


        [Fact]
        public async Task Put_WhenDealer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            await Seed();

            var command = Fixture.Build<UpdateDealerCommand>()
                .Without(c => c.DistributorId)
                .Without(c => c.Code)
                .Create();

            //Act
            var response = await Client.PutCommand($"{_baseUrl}/{Fixture.Create<DealerCode>().Value}", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        private async Task Seed()
        {
            var seeder = new DealerSeeder(DbContext);

            await seeder.Add(_dealerId, new { organizationId = OrganizationId });

            await seeder.Add<Organization>(UnregisteredOrganizationId);

            await seeder.SeedAsync();
        }
    }
}
