﻿using NexCore.ContactManager.FunctionalTests.Fixtures;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers
{
    public class LanguagesControllerTest : ApiControllerTestFixture
    {
        public LanguagesControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Get()
        {
            //Act
            var response = await Client.GetAsync("api/languages");

            //Assert
            response.EnsureSuccessStatusCode();
        }
    }
}
