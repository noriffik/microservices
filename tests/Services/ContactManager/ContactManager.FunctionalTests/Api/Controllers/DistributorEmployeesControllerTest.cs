﻿using AutoFixture;
using NexCore.ContactManager.Application.Distributors.Commands.Employees.AddEmployee;
using NexCore.ContactManager.Application.Distributors.Commands.Employees.UpdateEmployee;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.DistributorEmployees;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Testing.Extensions;
using NexCore.Testing.Seeders;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers
{
    public class DistributorEmployeesControllerTest : ApiControllerTestFixture
    {
        private readonly IFixture _fixture;

        private readonly DistributorId _distributorId;
        private readonly int _organizationId;
        private readonly int _individualId;
        private readonly int _jobId;

        private readonly string _url;

        public DistributorEmployeesControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
            _fixture = new Fixture().Customize(new DistributorIdCustomization());

            _distributorId = _fixture.Create<DistributorId>();
            _organizationId = _fixture.Create<int>();
            _individualId = _fixture.Create<int>();
            _jobId = _fixture.Create<int>();

            _url = $"api/distributors/{_distributorId.Value}/employees";
        }

        [Fact]
        public async Task Post()
        {
            //Arrange
            await SeedDependencies();

            var command = _fixture.Build<AddEmployeeCommand>()
                .Without(c => c.DistributorId)
                .With(c => c.IndividualId, _individualId)
                .With(c => c.JobId, _jobId)
                .Create();

            //Act
            var response = await Client.PostCommand(_url, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        private async Task SeedDependencies()
        {
            var seeder = new DistributorSeeder(DbContext);

            await seeder.Add(_distributorId, new { organizationId = _organizationId });
            await seeder.Add<Individual>(_individualId);
            await seeder.Add<Job>(_jobId);

            await seeder.SeedAsync();
        }

        [Fact]
        public async Task Post_WhenSomething_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = _fixture.Build<AddEmployeeCommand>()
                .Without(c => c.DistributorId)
                .With(c => c.IndividualId, _individualId)
                .With(c => c.JobId, _fixture.Create<int>())
                .Create();

            //Act
            var response = await Client.PostCommand(_url, command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var employee = await Seed();

            //Act
            var response = await Client.GetAsync($"{_url}/{employee.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        private async Task<DistributorEmployee> Seed()
        {
            var seeder = new DistributorEmployeeSeeder(DbContext);
            var employee = await seeder.Add(new
            {
                individualId = _individualId,
                organizationId = _organizationId,
                distributorId = _distributorId,
                jobId = _jobId
            });

            await seeder.SeedAsync();

            return employee;
        }

        [Fact]
        public async Task Get_WhenEmployee_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{_url}/{_fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            var seeder = new DistributorEmployeeSeeder(DbContext);
            await seeder.AddRange(3);

            var distributor = seeder.Get<Distributor, DistributorId>()
                .FirstOrDefault();

            await seeder.AddRange(3, new
            {
                organizationId = distributor?.OrganizationId,
                distributorId = _distributorId
            });

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync(_url);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Put()
        {
            //Arrange
            var seeder = new EntitySeeder<Job>(DbContext);
            var job = await seeder.Add();
            var employee = await Seed();

            var command = _fixture.Build<UpdateEmployeeCommand>()
                .Without(c => c.DistributorId)
                .Without(c => c.Id)
                .With(c => c.JobId, job.Id)
                .Create();

            //Act
            var response = await Client.PutCommand($"{_url}/{employee.Id}", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Put_WhenEmployee_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = _fixture.Create<UpdateEmployeeCommand>();

            //Act
            var response = await Client.PutCommand($"{_url}/{_fixture.Create<int>()}", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
