﻿using AutoFixture;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.AddEmployee;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.UpdateEmployee;
using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Testing.Extensions;
using NexCore.Testing.Seeders;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers
{
    public class DealerEmployeesControllerTest : ApiControllerTestFixture
    {
        private readonly IFixture _fixture;

        private readonly DealerId _dealerId;
        private readonly int _organizationId;
        private readonly int _individualId;
        private readonly int _jobId;

        private readonly string _baseUrl;

        public DealerEmployeesControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
            _fixture = new Fixture().Customize(new DealerIdCustomization());

            _dealerId = _fixture.Create<DealerId>();
            _organizationId = _fixture.Create<int>();
            _individualId = _fixture.Create<int>();
            _jobId = _fixture.Create<int>();

            _baseUrl = $"api/distributors/{_dealerId.DistributorId.Value}" +
                       $"/dealers/{_dealerId.DealerCode.Value}" +
                       "/employees";
        }

        [Fact]
        public async Task Post()
        {
            //Arrange
            await SeedDependencies();

            var command = _fixture.Build<AddEmployeeCommand>()
                .Without(c => c.DistributorId)
                .Without(c => c.DealerCode)
                .With(c => c.IndividualId, _individualId)
                .With(c => c.JobId, _jobId)
                .Create();

            //Act
            var response = await Client.PostCommand(_baseUrl, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        private async Task SeedDependencies()
        {
            var seeder = new DealerSeeder(DbContext);

            await seeder.Add(_dealerId, new { organizationId = _organizationId });
            await seeder.Add<Individual>(_individualId);
            await seeder.Add<Job>(_jobId);

            await seeder.SeedAsync();
        }

        [Fact]
        public async Task Post_WhenJob_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            await SeedDependencies();

            var command = _fixture.Build<AddEmployeeCommand>()
                .Without(c => c.DistributorId)
                .Without(c => c.DealerCode)
                .With(c => c.IndividualId, _individualId)
                .With(c => c.JobId, _fixture.Create<int>())
                .Create();

            //Act
            var response = await Client.PostCommand(_baseUrl, command);


            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var employee = await Seed();

            //Act
            var response = await Client.GetAsync($"{_baseUrl}/{employee.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        private async Task<DealerEmployee> Seed()
        {
            var seeder = new DealerEmployeeSeeder(DbContext);
            var employee = await seeder.Add(new
            {
                individualId = _individualId,
                organizationId = _organizationId,
                dealerId = _dealerId,
                jobId = _jobId
            });
            await seeder.SeedAsync();

            return employee;
        }

        [Fact]
        public async Task Get_WhenEmployee_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{_baseUrl}/{_fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            var seeder = new DealerEmployeeSeeder(DbContext);
            await seeder.AddRange(5);
            await seeder.AddRange(5, new
            {
                organizationId = _organizationId,
                dealerId = _dealerId
            });
            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync(_baseUrl);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Put()
        {
            //Arrange
            var seeder = new EntitySeeder<Job>(DbContext);
            var job = await seeder.Add();
            var employee = await Seed();

            var command = _fixture.Build<UpdateEmployeeCommand>()
                .Without(c => c.DistributorId)
                .Without(c => c.DealerCode)
                .Without(c => c.EmployeeId)
                .With(c => c.JobId, job.Id)
                .Create();

            //Act
            var response = await Client.PutCommand($"{_baseUrl}/{employee.Id}", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Put_WhenEmployee_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = _fixture.Build<UpdateEmployeeCommand>()
                .Without(c => c.DistributorId)
                .Without(c => c.DealerCode)
                .Without(c => c.EmployeeId)
                .Create();

            //Act
            var response = await Client.PutCommand($"{_baseUrl}/{_fixture.Create<int>()}", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
