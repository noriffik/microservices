﻿using NexCore.ContactManager.FunctionalTests.Fixtures;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers
{
    public class CommunicationMediumsControllerTest : ApiControllerTestFixture
    {
        public CommunicationMediumsControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Get()
        {
            //Act
            var response = await Client.GetAsync("api/communication-mediums/");

            //Arrange
            response.EnsureSuccessStatusCode();
        }
    }
}
