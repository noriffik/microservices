﻿using AutoFixture;
using NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.AddDisqualificationReason;
using NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.UpdateDisqualificationReason;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.Testing.Extensions;
using NexCore.Testing.Seeders;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers.Leads
{
    public class DisqualificationReasonsControllerTest : ApiControllerTestFixture
    {
        private const string Url = "api/leads/disqualification-reasons";

        private readonly Fixture _fixture;

        public DisqualificationReasonsControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
            _fixture = new Fixture();
        }

        [Fact]
        public async Task Post()
        {
            //Arrange
            var command = _fixture.Create<AddDisqualificationReasonCommand>();

            //Act
            var response = await Client.PostCommand(Url, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetById()
        {
            //Arrange
            var reason = await Seed();

            //Act
            var response = await Client.GetAsync($"{Url}/{reason.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetById_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{Url}/{_fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync(Url);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Put()
        {
            //Arrange
            var reason = await Seed();
            var command = new UpdateDisqualificationReasonCommand
            {
                Id = reason.Id,
                Name = "Name"
            };

            //Act
            var response = await Client.PutCommand(Url, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Put_WhenReason_IsNotFound_ReturnsBadRequest()
        {
            //Arrange

            var command = _fixture.Create<UpdateDisqualificationReasonCommand>();

            //Act
            var response = await Client.PutCommand(Url, command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        private async Task<DisqualificationReason> Seed()
        {
            var seeder = new EntitySeeder<DisqualificationReason>(DbContext);

            await seeder.AddRange(3);
            await seeder.SeedAsync();

            return seeder.Get<DisqualificationReason>().FirstOrDefault();
        }
    }
}
