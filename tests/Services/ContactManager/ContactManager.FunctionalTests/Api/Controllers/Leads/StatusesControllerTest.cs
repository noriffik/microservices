﻿using NexCore.ContactManager.FunctionalTests.Fixtures;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers.Leads
{
    public class StatusesControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/leads/statuses";

        public StatusesControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task GetAll()
        {
            //Act
            var response = await Client.GetAsync(BaseUrl);

            //Assert
            response.EnsureSuccessStatusCode();
        }

    }
}
