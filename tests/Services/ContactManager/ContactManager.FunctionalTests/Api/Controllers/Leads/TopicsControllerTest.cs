﻿using AutoFixture;
using NexCore.ContactManager.Application.Leads.Commands.Topics.AddTopic;
using NexCore.ContactManager.Application.Leads.Commands.Topics.UpdateTopic;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.Testing.Extensions;
using NexCore.Testing.Seeders;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers.Leads
{
    public class TopicsControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/leads/topics";
        private static readonly IFixture Fixture = new Fixture();

        public TopicsControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Post()
        {
            //Arrange
            var command = Fixture.Create<AddTopicCommand>();

            //Act
            var response = await Client.PostCommand(BaseUrl, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Post_WhenName_IsEmpty_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Build<AddTopicCommand>()
                .Without(c => c.Name)
                .Create();

            //Act
            var response = await Client.PostCommand(BaseUrl, command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var topic = await Seed();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{topic.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        private async Task<Topic> Seed()
        {
            var seeder = new EntitySeeder<Topic>(DbContext);

            await seeder.AddRange(5);

            var topic = await seeder.Add();

            await seeder.SeedAsync();

            return topic;
        }

        [Fact]
        public async Task Get_WhenIsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync(BaseUrl);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Put()
        {
            //Arrange
            var topic = await Seed();
            var command = Fixture.Build<UpdateTopicCommand>()
                .Without(c => c.Id)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/{topic.Id}", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Put_WhenTopic_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            await Seed();
            var command = Fixture.Build<UpdateTopicCommand>()
                .Without(c => c.Id)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/{Fixture.Create<int>()}", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
