﻿using AutoFixture;
using NexCore.ContactManager.Application.Leads.Commands.InterestSources.AddInterestSource;
using NexCore.ContactManager.Application.Leads.Commands.InterestSources.UpdateInterestSource;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.Testing.Extensions;
using NexCore.Testing.Seeders;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers.Leads
{
    public class InterestSourcesControllerTest : ApiControllerTestFixture
    {
        private const string Url = "api/leads/interest-sources";

        private readonly Fixture _fixture;

        public InterestSourcesControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
            _fixture = new Fixture();
        }

        [Fact]
        public async Task Post()
        {
            //Arrange
            var command = _fixture.Create<AddInterestSourceCommand>();

            //Act
            var response = await Client.PostCommand(Url, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetById()
        {
            //Arrange
            var interest = await Seed();

            //Act
            var response = await Client.GetAsync($"{Url}/{interest.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetById_WhenNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{Url}/{_fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync(Url);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Put()
        {
            //Arrange
            var interest = await Seed();

            var command = new UpdateInterestSourceCommand
            {
                Id = interest.Id,
                Name = interest.Name
            };

            //Act
            var response = await Client.PutCommand(Url, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Put_WhenEntity_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = _fixture.Create<UpdateInterestSourceCommand>();

            //Act
            var response = await Client.PutCommand(Url, command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Delete()
        {
            //Arrange
            var interest = await Seed();

            //Act
            var response = await Client.DeleteAsync($"{Url}/{interest.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Delete_WhenEntity_IsNotFound_ReturnsBadRequest()
        {
            //Act
            var response = await Client.DeleteAsync($"{Url}/{_fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        private async Task<InterestSource> Seed()
        {
            var seeder = new EntitySeeder<InterestSource>(DbContext);
            var interest = await seeder.Add();

            await seeder.SeedAsync();

            return interest;
        }
    }
}
