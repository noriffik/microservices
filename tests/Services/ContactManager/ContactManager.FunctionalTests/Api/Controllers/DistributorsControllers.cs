﻿using AutoFixture;
using NexCore.ContactManager.Application.Distributors.Commands.AddDistributor;
using NexCore.ContactManager.Application.Distributors.Commands.UpdateDistributor;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Testing.Extensions;
using NexCore.Testing.Seeders;
using System.Globalization;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers
{
    public class DistributorsControllers : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/distributors";

        private readonly IFixture _fixture;

        public DistributorsControllers(ServiceFactory<TestStartup> factory) : base(factory)
        {
            _fixture = new Fixture().Customize(new DistributorIdCustomization());
        }

        [Fact]
        public async Task Post()
        {
            //Arrange
            var seeder = new EntitySeeder<Organization>(DbContext);
            var organization = await seeder.Add();
            await seeder.SeedAsync();

            var command = _fixture.Build<AddDistributorCommand>()
                .With(c => c.Id, _fixture.Create<DistributorId>().Value)
                .With(c => c.OrganizationId, organization.Id)
                .With(c => c.CountryCode, RegionInfo.CurrentRegion.TwoLetterISORegionName)
                .Create();

            //Act
            var response = await Client.PostCommand(BaseUrl, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Post_WhenDistributorId_IsInUse_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new DistributorSeeder(DbContext);
            var distributor = await seeder.Add();
            await seeder.SeedAsync();

            var command = _fixture.Build<AddDistributorCommand>()
                .With(c => c.Id, distributor.Id.Value)
                .With(c => c.CountryCode, RegionInfo.CurrentRegion.TwoLetterISORegionName)
                .Create();

            //Act
            var response = await Client.PostCommand(BaseUrl, command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var seeder = new DistributorSeeder(DbContext);
            var distributor = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{distributor.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenIsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{_fixture.Create<DistributorId>().Value}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            var seeder = new DistributorSeeder(DbContext);
            await seeder.AddRange(3);
            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync(BaseUrl);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Put()
        {
            //Arrange
            var seeder = new DistributorSeeder(DbContext);
            var distributor = await seeder.Add();
            await seeder.SeedAsync();

            var command = _fixture.Build<UpdateDistributorCommand>()
                .With(c => c.DistributorId, distributor.Id.Value)
                .With(c => c.CountryCode, RegionInfo.CurrentRegion.TwoLetterISORegionName)
                .Create();

            //Act
            var response = await Client.PutCommand(BaseUrl, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }
    }
}
