﻿using AutoFixture;
using NexCore.ContactManager.Application.Jobs.Commands.AddJob;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.Testing.Extensions;
using NexCore.Testing.Seeders;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers
{
    public class JobsControllerTest : ApiControllerTestFixture
    {
        private const int JobId = 10;

        private const string BaseUrl = "api/jobs";

        private static readonly IFixture Fixture = new Fixture { RepeatCount = 1 }
            .Customize(new ContactModelCustomization());
        
        public JobsControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        public static IEnumerable<object[]> FindSingleEndpoints = CommandEndpoint.Wrap($"{BaseUrl}/{JobId}");

        public static IEnumerable<object[]> FindManyEndpoints = CommandEndpoint.Wrap($"{BaseUrl}/many");

        [Theory]
        [MemberData(nameof(FindSingleEndpoints))]
        public async Task FindSingle(string point)
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync(point);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task FindSingle_WhenJob_NotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{JobId}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Theory]
        [MemberData(nameof(FindManyEndpoints))]
        public async Task FindMany(string point)
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync(point);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Create()
        {
            //Arrange
            var command = Fixture.Create<AddJobCommand>();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/create", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Create_WhenGivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PostCommand($"{BaseUrl}/create", "invalid");

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
        
        private async Task Seed()
        {
            var seeder = new EntitySeeder<Job>(DbContext);
            await seeder.Add(JobId);

            await seeder.SeedAsync();
        }
    }
}
