﻿using AutoFixture;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Leads.OpenLead;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Testing.Extensions;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers
{
    public class DealerLeadsControllerTest : ApiControllerTestFixture
    {
        private static readonly IFixture Fixture = new Fixture().Customize(new DealerIdCustomization());

        private readonly DealerId _dealerId;
        private readonly string _baseUrl;

        public DealerLeadsControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
            _dealerId = Fixture.Create<DealerId>();
            _baseUrl = $"api/distributors/{_dealerId.DistributorId.Value}/dealers/{_dealerId.DealerCode.Value}/leads";
        }

        private async Task<Lead> Seed()
        {
            var seeder = new DealerLeadSeeder(DbContext, Fixture);

            await seeder.AddRange(5);

            var lead = seeder.Create<Lead>(new { dealerId = _dealerId });
            lead.AssignOwner(seeder.Create<int>());
            await seeder.Add(lead);

            await seeder.SeedAsync();

            return lead;
        }

        [Fact]
        public async Task Post()
        {
            //Arrange
            var lead = await Seed();
            var command = Fixture.Build<OpenLeadCommand>()
                .Without(c => c.DistributorId)
                .Without(c => c.DealerCode)
                .With(c => c.IndividualId, lead.Contact.IndividualId)
                .With(c => c.TopicId, lead.TopicId)
                .With(c => c.OrganizationId, lead.Contact.OrganizationId)
                .With(c => c.OwnerEmployeeId, lead.OwnerEmployeeId)
                .Create();

            //Act
            var response = await Client.PostCommand(_baseUrl, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Post_WhenEntities_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            await Seed();
            var command = Fixture.Build<OpenLeadCommand>()
                .Without(c => c.DistributorId)
                .Without(c => c.DealerCode)
                .Create();

            //Act
            var response = await Client.PostCommand(_baseUrl, command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var lead = await Seed();

            //Act
            var response = await Client.GetAsync($"{_baseUrl}/{lead.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenLead_IsNotFound_ReturnsNotFound()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync($"{_baseUrl}/{Fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync(_baseUrl);

            //Assert
            response.EnsureSuccessStatusCode();
        }
    }
}
