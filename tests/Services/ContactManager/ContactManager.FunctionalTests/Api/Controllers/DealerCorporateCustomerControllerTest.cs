﻿using AutoFixture;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddCorporateCustomer;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders.Customers;
using NexCore.DealerNetwork;
using NexCore.Testing.Extensions;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers
{
    public class DealerCorporateCustomerControllerTest : ApiControllerTestFixture
    {
        private const int ContactableId = 3;
        private const int OrganizationId = 30;
        private const string DealerIdValue = "EKK38001";
        private const int CustomerId = 234;
        private const string Url = "api/distributors";

        private readonly Fixture _fixture;
        private readonly DealerId _dealerId;
        private readonly AddCorporateCustomerCommand _command;

        public DealerCorporateCustomerControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
            _fixture = new Fixture();
            _fixture.Customize(new DealerCodeCustomization());
            _fixture.Customize(new DistributorIdCustomization());
            _fixture.Customize(new DealerIdCustomization());

            _dealerId = _fixture.Create<DealerId>();
            _command = new AddCorporateCustomerCommand { OrganizationId = ContactableId };
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync($"{Url}/{_dealerId.DistributorId}/dealers/{_dealerId.DealerCode}/customers/corporate/{CustomerId}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenIsNotFound_ReturnsNotFound()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync($"{Url}/{_dealerId.DistributorId}/dealers/{_dealerId.DealerCode}/customers/corporate/{_fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task Post()
        {
            //Arrange
            var seeder = new DealerSeeder(DbContext);

            var dealer = await seeder.Add(DealerId.Parse(DealerIdValue), new { organizationId = OrganizationId });
            await seeder.Add<Organization>(ContactableId);

            await seeder.SeedAsync();

            //Act
            var response = await Client.PostCommand($"{Url}/{dealer.Id.DistributorId.Value}" +
                                                    $"/dealers/{dealer.Id.DealerCode.Value}/customers/corporate", _command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Post_WhenDealerIsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new DealerSeeder(DbContext);
            var distributor = await seeder.Add<Distributor, DistributorId>();
            await seeder.SeedAsync();

            //Act
            var response = await Client.PostCommand($"{Url}/{distributor.Id}" +
                                                    $"/dealers/{_fixture.Create<DealerCode>()}/customers/corporate", _command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync(Url);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        private async Task Seed()
        {
            var seeder = new DealerCorporateCustomerSeeder(DbContext);

            await seeder.Add(CustomerId, new
            {
                contactableId = ContactableId,
                organizationId = OrganizationId,
                dealerId = _dealerId
            });

            await seeder.SeedAsync();
        }
    }
}
