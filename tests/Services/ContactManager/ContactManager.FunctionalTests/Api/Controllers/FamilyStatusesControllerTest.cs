﻿using NexCore.ContactManager.FunctionalTests.Fixtures;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers
{
    public class FamilyStatusesControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/family-statuses";

        public FamilyStatusesControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Get()
        {
            //Act
            var response = await Client.GetAsync(BaseUrl);

            //Assert
            response.EnsureSuccessStatusCode();
        }
    }
}
