﻿using AutoFixture;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Testing.Extensions;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers
{
    public class DealerPrivateCustomersControllerTest : ApiControllerTestFixture
    {
        private const int ContactableId = 10;
        private const int PrivateCustomerId = 20;
        private const int OrganizationId = 30;
        private const string TelephoneCountryCode = "11";
        private const string TelephoneAreaCode = "456";
        private const string TelephoneLocalNumber = "789000";
        private const string Telephone = TelephoneCountryCode + TelephoneAreaCode + TelephoneLocalNumber;
        private readonly DealerId _dealerId;

        private readonly string _baseUrl;

        private static readonly IFixture Fixture = new Fixture { RepeatCount = 1 }
            .Customize(new ContactModelCustomization())
            .Customize(new DealerIdCustomization())
            .CustomizeConstructor<Telephone>(new
            {
                countryCode = TelephoneCountryCode,
                areaCode = TelephoneAreaCode,
                localNumber = TelephoneLocalNumber
            });

        public DealerPrivateCustomersControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
            _dealerId = Fixture.Create<DealerId>();

            _baseUrl = $"api/distributors/{_dealerId.DistributorId.Value}/dealers/{_dealerId.DealerCode.Value}/customers/private";
        }

        private async Task Seed()
        {
            var seeder = new DealerPrivateCustomerSeeder(DbContext);

            await seeder.Add(PrivateCustomerId, new
            {
                contactableId = ContactableId,
                organizationId = OrganizationId,
                dealerId = _dealerId
            });

            var individual = seeder.Find<Individual>(ContactableId);
            individual.Contact.Add(Fixture.Create<Telephone>());

            await seeder.SeedAsync();
        }

        [Fact]
        public async Task Post()
        {
            //Arrange
            var seeder = new DealerSeeder(DbContext);
            await seeder.Add(_dealerId, new { organizationId = OrganizationId });
            await seeder.Add<Individual>(ContactableId);
            await seeder.SeedAsync();

            var command = Fixture.Build<AddPrivateCustomerCommand>()
                .Without(c => c.DistributorId)
                .Without(c => c.DealerCode)
                .With(c => c.IndividualId, ContactableId)
                .Create();

            //Act
            var response = await Client.PostCommand(_baseUrl, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Post_WhenDealer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Build<AddPrivateCustomerCommand>()
                .Without(c => c.DistributorId)
                .Without(c => c.DealerCode)
                .Create();

            //Act
            var response = await Client.PostCommand(_baseUrl, command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync($"{_baseUrl}/{PrivateCustomerId}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenIsNotFound_ReturnsNotFound()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync($"{_baseUrl}/{Fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync(_baseUrl);

            //Assert
            response.EnsureSuccessStatusCode();
        }
    }
}