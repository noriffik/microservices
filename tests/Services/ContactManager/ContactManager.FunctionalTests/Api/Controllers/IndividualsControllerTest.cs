﻿using AutoFixture;
using NexCore.ContactManager.Application.Individuals.Commands.AddIndividual;
using NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.Legal;
using NexCore.Testing.Extensions;
using NexCore.Testing.Seeders;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Api.Controllers
{
    public class IndividualsControllerTest : ApiControllerTestFixture
    {
        private const int IndividualId = 10;
        private const string CountryCode = "11";
        private const string AreaCode = "456";
        private const string LocalNumber = "789000";
        private const string BaseUrl = "api/contacts/individuals";

        private readonly IFixture _fixture;
        private readonly UpdateIndividualCommand _updateCommand;
        private readonly PersonalPreferencesDto _personalPreferencesDto;

        public IndividualsControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
            _fixture = new Fixture { RepeatCount = 1 }
                .Customize(new ContactModelCustomization())
                .CustomizeConstructor<Telephone>(
                    new
                    {
                        countryCode = CountryCode,
                        areaCode = AreaCode,
                        localNumber = LocalNumber
                    });

            _personalPreferencesDto = new PersonalPreferencesDto
            {
                Language = "eng"
            };

            _updateCommand = _fixture.Build<UpdateIndividualCommand>()
                .Without(c => c.Id)
                .With(c => c.FamilyStatus, FamilyStatus.CivilMarriage.Id)
                .With(c => c.Preferences, _personalPreferencesDto)
                .Create();
        }

        [Fact]
        public async Task FindSingle()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{IndividualId}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task FindSingle_WhenPerson_NotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{IndividualId}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task Put()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/{IndividualId}", _updateCommand);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Put_WhenPerson_NotFound_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/{IndividualId}", _updateCommand);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Put_WhenGivenCommand_IsNull_ReturnsBadRequest()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/{IndividualId}", "invalid");

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            await Seed();

            //Act
            var response = await Client.GetAsync(BaseUrl);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Post()
        {
            //Arrange
            var command = _fixture.Build<AddIndividualCommand>()
                .With(c => c.FamilyStatus, FamilyStatus.Divorced.Id)
                .With(c => c.Preferences, _personalPreferencesDto)
                .Create();

            //Act
            var response = await Client.PostCommand(BaseUrl, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Post_GivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PostCommand(BaseUrl, null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        private async Task Seed()
        {
            var seeder = new EntitySeeder<Individual>(DbContext);

            var individual = await seeder.Add(IndividualId);

            individual.ChangePassport(_fixture.Create<Passport>());
            individual.ChangeTaxNumber(_fixture.Create<TaxIdentificationNumber>());
            individual.Contact.Add(_fixture.Create<Telephone>(), "Description", true);
            individual.UpdateFamilyStatus(FamilyStatus.Single);

            await DbContext.SaveChangesAsync();
        }
    }
}
;