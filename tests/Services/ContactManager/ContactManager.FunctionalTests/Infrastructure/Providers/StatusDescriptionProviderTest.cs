﻿using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.Infrastructure.Providers.Leads;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Infrastructure.Providers
{
    public class StatusDescriptionProviderTest
    {
        private readonly StatusDescriptionProvider _provider = new StatusDescriptionProvider();

        public static IEnumerable<object[]> Statuses => new[]
        {
            new object[] { Status.Open}, new object[] { Status.Contacted },
            new object[] { Status.Qualified}, new object[] { Status.Disqualified }
        };

        [Fact]
        public void Get()
        {
            //Arrange
            var expectedIds = Statuses.Select(d => (int)d.First());

            //Act
            var records = _provider.Get();

            //Assert
            Assert.All(expectedIds, id => Assert.Contains(id, records.Select(r => r.Id)));
        }

        [Theory]
        [MemberData(nameof(Statuses))]
        public void Has(int id)
        {
            //Act
            var result = _provider.Has(id);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Has_WhenNotFound_ReturnsFalse()
        {
            //Act
            var result = _provider.Has(123);

            //Assert
            Assert.False(result);
        }

        [Theory]
        [MemberData(nameof(Statuses))]
        public void SingleOrDefault(int id)
        {
            //Act
            var result = _provider.SingleOrDefault(id);

            //Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void SingleOrDefault_WhenNotFound_ReturnsNull()
        {
            //Act
            var result = _provider.SingleOrDefault(123);

            //Assert
            Assert.Null(result);
        }

        [Theory]
        [MemberData(nameof(Statuses))]
        public void Single(int id)
        {
            //Act
            var result = _provider.Single(id);

            //Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void Single_WhenNotFound_Throws()
        {
            //Arrange
            const int id = 123;

            //Act
            var e = Assert.Throws<StatusDescriptionNotFoundException>(() => _provider.Single(id));

            //Assert
            Assert.Equal(id, e.Id);
        }
    }
}
