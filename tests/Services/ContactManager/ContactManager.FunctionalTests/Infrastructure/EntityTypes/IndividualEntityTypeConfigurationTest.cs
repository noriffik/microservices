﻿using AutoFixture;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.Domain;
using NexCore.Testing;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Infrastructure.EntityTypes
{
    public class IndividualEntityTypeConfigurationTest : SpecificationFixture
    {
        private readonly IFixture _fixture;
        private readonly PropertyComparer<Individual> _comparer;

        public IndividualEntityTypeConfigurationTest() : base(nameof(IndividualEntityTypeConfigurationTest))
        {
            _fixture = new Fixture();
            _comparer = new PropertyComparer<Individual>(new PropertyComparerOptions
            {
                CollectionComparison = CollectionComparison.Unordered
            });
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var expected = _fixture.Create<Individual>();

            using (var context = CreateContext())
            {
                await context.AddAsync(expected);
                await context.SaveChangesAsync();
            }

            //Assert
            using (var context = CreateContext())
            {
                var actual = await context.Set<Individual>().FindAsync(expected.Id);

                Assert.Equal(expected, actual, _comparer);
            }
        }

        [Fact]
        public async Task Add_WithPreferences()
        {
            //Arrange
            var expected = _fixture.Create<Individual>();
            expected.Preferences.ChangeLanguage("eng");

            using (var context = CreateContext())
            {
                await context.AddAsync(expected);
                await context.SaveChangesAsync();
            }

            //Assert
            using (var context = CreateContext())
            {
                var actual = await context.Set<Individual>().FindAsync(expected.Id);

                Assert.Equal(expected, actual, _comparer);
            }
        }
    }
}
