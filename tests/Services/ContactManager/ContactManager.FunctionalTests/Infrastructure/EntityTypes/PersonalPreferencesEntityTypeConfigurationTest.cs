﻿using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.Domain;
using NexCore.Testing;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Infrastructure.EntityTypes
{
    public class PersonalPreferencesEntityTypeConfigurationTest : SpecificationFixture
    {
        private readonly PropertyComparer<PersonalPreferences> _comparer;

        public PersonalPreferencesEntityTypeConfigurationTest() : base(
            nameof(PersonalPreferencesEntityTypeConfigurationTest))
        {
            _comparer = new PropertyComparer<PersonalPreferences>(new PropertyComparerOptions
            {
                CollectionComparison = CollectionComparison.Unordered
            });
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var expected = new PersonalPreferences();

            using (var context = CreateContext())
            {
                await context.AddAsync(expected);
                await context.SaveChangesAsync();
            }

            //Assert
            using (var context = CreateContext())
            {
                var actual = await context.Set<PersonalPreferences>().FindAsync(expected.Id);

                Assert.Equal(expected, actual, _comparer);
            }
        }

        [Fact]
        public async Task Add_WithCommunications()
        {
            //Arrange
            var expected = new PersonalPreferences();
            expected.ChangeLanguage("eng");
            expected.Communications.ChangePreferredMedium(CommunicationMedium.Email);
            expected.Communications.BanMedium(CommunicationMedium.Telegram);
            expected.Communications.BanMedium(CommunicationMedium.Telephone);

            using (var context = CreateContext())
            {
                await context.AddAsync(expected);
                await context.SaveChangesAsync();
            }

            //Assert
            using (var context = CreateContext())
            {
                var actual = await context.Set<PersonalPreferences>().FindAsync(expected.Id);

                Assert.Equal(expected, actual, _comparer);
            }
        }
    }
}
