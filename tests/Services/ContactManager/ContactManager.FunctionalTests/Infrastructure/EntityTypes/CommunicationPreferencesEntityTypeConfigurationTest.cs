﻿using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.FunctionalTests.Fixtures;
using NexCore.Domain;
using NexCore.Testing;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.FunctionalTests.Infrastructure.EntityTypes
{
    public class CommunicationPreferencesEntityTypeConfigurationTest : SpecificationFixture
    {
        private readonly PropertyComparer<CommunicationPreferences> _comparer;

        public CommunicationPreferencesEntityTypeConfigurationTest() : base(
            nameof(CommunicationPreferencesEntityTypeConfigurationTest))
        {
            _comparer = new PropertyComparer<CommunicationPreferences>(new PropertyComparerOptions
            {
                CollectionComparison = CollectionComparison.Unordered
            });
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var expected = new CommunicationPreferences();

            using (var context = CreateContext())
            {
                await context.AddAsync(expected);
                await context.SaveChangesAsync();
            }

            //Assert
            using (var context = CreateContext())
            {
                var actual = await context.Set<CommunicationPreferences>().FindAsync(expected.Id);

                Assert.Equal(expected, actual, _comparer);
            }
        }

        [Fact]
        public async Task Add_WithBanMediums()
        {
            //Arrange
            var expected = new CommunicationPreferences();

            expected.BanMedium(CommunicationMedium.Email);

            using (var context = CreateContext())
            {
                await context.AddAsync(expected);
                await context.SaveChangesAsync();
            }

            //Assert
            using (var context = CreateContext())
            {
                var actual = await context.Set<CommunicationPreferences>().FindAsync(expected.Id);

                Assert.Equal(expected, actual, _comparer);
            }
        }

        [Fact]
        public async Task Add_WithPreferredMedium()
        {
            //Arrange
            var expected = new CommunicationPreferences();

            expected.ChangePreferredMedium(CommunicationMedium.Telephone);
            expected.BanMedium(CommunicationMedium.Email);

            using (var context = CreateContext())
            {
                await context.AddAsync(expected);
                await context.SaveChangesAsync();
            }

            //Assert
            using (var context = CreateContext())
            {
                var actual = await context.Set<CommunicationPreferences>().FindAsync(expected.Id);

                Assert.Equal(expected, actual, _comparer);
            }
        }

        [Fact]
        public async Task Update()
        {
            //Arrange
            var expected = new CommunicationPreferences();

            expected.ChangePreferredMedium(CommunicationMedium.Telephone);
            expected.BanMedium(CommunicationMedium.Email);

            using (var context = CreateContext())
            {
                await context.AddAsync(expected);
                await context.SaveChangesAsync();
            }

            //Act
            using (var context = CreateContext())
            {
                expected = await context.Set<CommunicationPreferences>().FindAsync(expected.Id);

                expected.ChangePreferredMedium(CommunicationMedium.Telegram);
                expected.PermitMedium(CommunicationMedium.Email);
                expected.BanMedium(CommunicationMedium.Viber);

                await context.SaveChangesAsync();
            }

            //Assert
            using (var context = CreateContext())
            {
                var actual = await context.Set<CommunicationPreferences>().FindAsync(expected.Id);

                Assert.Equal(expected, actual, _comparer);
            }
        }
    }
}