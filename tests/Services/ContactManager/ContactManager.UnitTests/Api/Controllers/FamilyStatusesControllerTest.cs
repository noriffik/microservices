﻿using NexCore.ContactManager.Api.Controllers;
using NexCore.ContactManager.Application.FamilyStatuses.Dtos;
using NexCore.ContactManager.Application.FamilyStatuses.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Controllers
{
    public class FamilyStatusesControllerTest
    {
        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(FamilyStatusesController)).HasNullGuard();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<FamilyStatusDto>>(typeof(FamilyStatusesController), typeof(FamilyStatusQuery));

            //Assert
            return action.IsJson().Run();
        }
    }
}
