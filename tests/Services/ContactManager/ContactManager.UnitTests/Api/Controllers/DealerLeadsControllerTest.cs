﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Controllers;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Leads.OpenLead;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Leads;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Controllers
{
    public class DealerLeadsControllerTest
    {
        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DealerLeadsController)).HasNullGuard();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<LeadDto>(typeof(DealerLeadsController), typeof(LeadByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Post()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(DealerLeadsController), typeof(OpenLeadCommand));

            //Assert
            return action.IsCreated(result => result.ActionName == nameof(DealerEmployeesController.Get)
                                              && result.RouteValues.ContainsKey("DistributorId")
                                              && result.RouteValues.ContainsKey("DealerCode")
                                              && result.RouteValues.ContainsKey("Id"))
                .IsBadRequestOnNull()
                .Run();
        }

        [Fact]
        public Task GetAll()
        {
            //Arrange
            var action = Assert.Action<PagedResponse<LeadDto>>(typeof(DealerLeadsController), typeof(LeadQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }
    }
}
