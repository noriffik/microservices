﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Controllers;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddCorporateCustomer;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers.Corporate;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Controllers
{
    public class DealerCorporateCustomerControllerTest
    {
        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DealerCorporateCustomerController)).HasNullGuard();
        }

        [Fact]
        public Task Post()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(DealerCorporateCustomerController), typeof(AddCorporateCustomerCommand));

            //Assert
            return action.IsCreated(result => result.ActionName == nameof(DealerCorporateCustomerController.Get)
                                              && result.RouteValues.ContainsKey("DistributorId")
                                              && result.RouteValues.ContainsKey("DealerCode")
                                              && result.RouteValues.ContainsKey("CorporateCustomerId"))
                .IsBadRequestOnNull()
                .Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<CorporateCustomerDto>(typeof(DealerCorporateCustomerController),
                typeof(CorporateCustomerByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetAll()
        {
            //Arrange
            var action = Assert.Action<PagedResponse<CorporateCustomerDto>>(typeof(DealerCorporateCustomerController),
                typeof(CorporateCustomerQuery));

            //Assert
            return action.IsJson().Run();
        }
    }
}
