﻿using NexCore.ContactManager.Api.Controllers;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.AddDealer;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.UpdateDealer;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Controllers
{
    public class DealersControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(DealersController)).HasNullGuard();
        }

        [Fact]
        public Task Post()
        {
            //Arrange
            var action = Assert.Action<string>(typeof(DealersController), typeof(AddDealerCommand));

            //Assert
            return action.IsCreated(result => result.ActionName == "Get")
                .IsBadRequestOnNull()
                .Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<DealerDto>(typeof(DealersController), typeof(DealerByCodeQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Put()
        {
            //Arrange
            var action = Assert.Action(typeof(DealersController), typeof(UpdateDealerCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}

