﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Controllers;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers;
using System.Threading.Tasks;
using Xunit;
using PrivateCustomerDto = NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers.PrivateCustomerDto;

namespace NexCore.ContactManager.UnitTests.Api.Controllers
{
    public class DealerPrivateCustomerControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(DealerPrivateCustomerController)).HasNullGuard();
        }

        [Fact]
        public Task Post()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(DealerPrivateCustomerController), typeof(AddPrivateCustomerCommand));

            //Assert
            return action.IsCreated(result => result.ActionName == nameof(DealerEmployeesController.Get)
                                              && result.RouteValues.ContainsKey("DistributorId")
                                              && result.RouteValues.ContainsKey("DealerCode")
                                              && result.RouteValues.ContainsKey("PrivateCustomerId"))
                .IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<PrivateCustomerDto>(typeof(DealerPrivateCustomerController), typeof(PrivateCustomerByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetAll()
        {
            //Arrange
            var action = Assert.Action<PagedResponse<PrivateCustomerDto>>(
                typeof(DealerPrivateCustomerController), typeof(PrivateCustomerQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }
    }
}
