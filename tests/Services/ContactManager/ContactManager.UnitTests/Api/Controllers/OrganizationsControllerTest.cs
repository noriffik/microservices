﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Controllers;
using NexCore.ContactManager.Application.Organizations.Commands;
using NexCore.ContactManager.Application.Organizations.Commands.AddOrganization;
using NexCore.ContactManager.Application.Organizations.Queries;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Controllers
{
    public class OrganizationsControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(OrganizationsController)).HasNullGuard();
        }

        [Fact]
        public Task Post()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(OrganizationsController), typeof(AddOrganizationCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }
        
        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<PagedResponse<OrganizationDto>>(typeof(OrganizationsController), typeof(OrganizationQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Put()
        {
            //Arrange
            var action = Assert.Action(typeof(OrganizationsController), typeof(UpdateOrganizationCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

    }
}
