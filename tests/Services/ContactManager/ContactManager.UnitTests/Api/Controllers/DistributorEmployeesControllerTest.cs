﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Controllers;
using NexCore.ContactManager.Application.Distributors.Commands.Employees.AddEmployee;
using NexCore.ContactManager.Application.Distributors.Commands.Employees.UpdateEmployee;
using NexCore.ContactManager.Application.Distributors.Queries.Employees;
using System.Threading.Tasks;
using Xunit;


namespace NexCore.ContactManager.UnitTests.Api.Controllers
{
    public class DistributorEmployeesControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(DistributorEmployeesController)).HasNullGuard();
        }

        [Fact]
        public Task Post()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(DistributorEmployeesController), typeof(AddEmployeeCommand));

            //Assert
            return action.IsCreated(result => result.ActionName == nameof(DistributorEmployeesController.Get)
                                              && result.RouteValues.ContainsKey("DistributorId")
                                              && result.RouteValues.ContainsKey("Id"))
                .IsBadRequestOnNull()
                .Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<EmployeeDto>(typeof(DistributorEmployeesController), typeof(EmployeeByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetAll()
        {
            //Arrange
            var action = Assert.Action<PagedResponse<EmployeeDto>>(typeof(DistributorEmployeesController), typeof(EmployeeQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Put()
        {
            //Arrange
            var action = Assert.Action(typeof(DistributorEmployeesController), typeof(UpdateEmployeeCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
