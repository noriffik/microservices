﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Controllers;
using NexCore.ContactManager.Application.Distributors.Commands.AddDistributor;
using NexCore.ContactManager.Application.Distributors.Commands.UpdateDistributor;
using NexCore.ContactManager.Application.Distributors.Queries;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Controllers
{
    public class DistributorsControllerTest
    {
        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DistributorsController)).HasNullGuard();
        }

        [Fact]
        public Task Post()
        {
            //Arrange
            var action = Assert.Action<string>(typeof(DistributorsController), typeof(AddDistributorCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<DistributorDto>(typeof(DistributorsController), typeof(DistributorByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetAll()
        {
            //Arrange
            var action = Assert.Action<PagedResponse<DistributorDto>>(
                typeof(DistributorsController), typeof(DistributorsQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Put()
        {
            //Arrange
            var action = Assert.Action(typeof(DistributorsController), typeof(UpdateDistributorCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
