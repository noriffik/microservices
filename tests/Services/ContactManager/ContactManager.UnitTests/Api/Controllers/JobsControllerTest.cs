﻿using NexCore.ContactManager.Api.Controllers;
using NexCore.ContactManager.Application.Jobs.Commands.AddJob;
using NexCore.ContactManager.Application.Jobs.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Controllers
{
    public class JobsControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(JobsController)).HasNullGuard();
        }

        [Fact]
        public Task Register()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(JobsController), typeof(AddJobCommand));

            //Asserts
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Find()
        {
            //Arrange
            var action = Assert.Action<JobDto>(typeof(JobsController), typeof(JobByIdQuery));

            //Asserts
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task FindMany()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<JobDto>>(typeof(JobsController), typeof(JobQuery));

            //Asserts
            return action.IsJson().IsBadRequestOnNull().Run();
        }
    }
}
