﻿using NexCore.ContactManager.Api.Controllers.Leads;
using NexCore.ContactManager.Application.Leads.Commands.InterestSources.AddInterestSource;
using NexCore.ContactManager.Application.Leads.Commands.InterestSources.RemoveInterestedSource;
using NexCore.ContactManager.Application.Leads.Commands.InterestSources.UpdateInterestSource;
using NexCore.ContactManager.Application.Leads.Queries.InterestSources;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Controllers.Leads
{
    public class InterestSourcesControllerTest
    {
        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(InterestSourcesController)).HasNullGuard();
        }

        [Fact]
        public Task Post()
        {
            //Act
            var action = Assert.Action<int>(typeof(InterestSourcesController), typeof(AddInterestSourceCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetById()
        {
            //Act
            var action = Assert.Action<InterestSourceDto>(typeof(InterestSourcesController),
                typeof(InterestSourceByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().Run();
        }

        [Fact]
        public Task GetAll()
        {
            //Act
            var action = Assert.Action<IEnumerable<InterestSourceDto>>(typeof(InterestSourcesController),
                typeof(InterestSourceQuery));

            //Assert
            return action.IsJson().Run();
        }

        [Fact]
        public Task Put()
        {
            //Act
            var action = Assert.Action(typeof(InterestSourcesController), typeof(UpdateInterestSourceCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Delete()
        {
            //Act
            var action = Assert.Action(typeof(InterestSourcesController), typeof(RemoveInterestSourceCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
