﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.ContactManager.Api.Controllers.Leads;
using NexCore.ContactManager.Application.Leads.Queries.Statuses;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Controllers.Leads
{
    public class StatusesControllerTest
    {
        [Fact]
        public async Task Get()
        {
            //Arrange
            var expected = new List<StatusDto>();

            var mediator = new Mock<IMediator>();
            mediator.Setup(m => m.Send(It.IsAny<StatusQuery>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(expected);

            var controller = new StatusesController(mediator.Object, new Mock<IAuthorizationService>().Object);

            //Act
            var result = await controller.Get();

            //Assert
            var jsonResult = Assert.IsType<JsonResult>(result);
            Assert.Equal(expected, jsonResult.Value);
        }
    }
}
