﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Controllers.Leads;
using NexCore.ContactManager.Application.Leads.Commands.Topics.AddTopic;
using NexCore.ContactManager.Application.Leads.Commands.Topics.UpdateTopic;
using NexCore.ContactManager.Application.Leads.Queries.Topics;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Controllers.Leads
{
    public class TopicsControllerTest
    {
        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(TopicsController)).HasNullGuard();
        }

        [Fact]
        public Task Post()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(TopicsController), typeof(AddTopicCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<TopicDto>(typeof(TopicsController), typeof(TopicByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetAll()
        {
            //Arrange
            var action = Assert.Action<PagedResponse<TopicDto>>(typeof(TopicsController),
                typeof(TopicQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Put()
        {
            //Arrange
            var action = Assert.Action(typeof(TopicsController), typeof(UpdateTopicCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
