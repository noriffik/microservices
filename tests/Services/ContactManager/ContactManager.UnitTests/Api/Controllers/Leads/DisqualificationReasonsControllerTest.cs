﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Controllers.Leads;
using NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.AddDisqualificationReason;
using NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.UpdateDisqualificationReason;
using NexCore.ContactManager.Application.Leads.Queries.DisqualificationReasons;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Controllers.Leads
{
    public class DisqualificationReasonsControllerTest
    {
        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DisqualificationReasonsController)).HasNullGuard();
        }

        [Fact]
        public Task Post()
        {
            //Act
            var action = Assert.Action<int>(typeof(DisqualificationReasonsController), typeof(AddDisqualificationReasonCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetById()
        {
            //Act
            var action = Assert.Action<DisqualificationReasonDto>(typeof(DisqualificationReasonsController),
                typeof(DisqualificationReasonByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().Run();
        }

        [Fact]
        public Task GetAll()
        {
            //Act
            var action = Assert.Action<PagedResponse<DisqualificationReasonDto>>(
                typeof(DisqualificationReasonsController), typeof(DisqualificationReasonQuery));

            //Assert
            return action.IsJson().Run();
        }

        [Fact]
        public Task Put()
        {
            //Act
            var action = Assert.Action(typeof(DisqualificationReasonsController),
                typeof(UpdateDisqualificationReasonCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
