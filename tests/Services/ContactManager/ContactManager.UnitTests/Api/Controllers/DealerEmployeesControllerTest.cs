﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Controllers;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.AddEmployee;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.UpdateEmployee;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Employees;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Controllers
{
    public class DealerEmployeesControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(DealerEmployeesController)).HasNullGuard();
        }

        [Fact]
        public Task Post()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(DealerEmployeesController), typeof(AddEmployeeCommand));

            //Assert
            return action.IsCreated(result => result.ActionName == nameof(DealerEmployeesController.Get)
                                              && result.RouteValues.ContainsKey("DistributorId")
                                              && result.RouteValues.ContainsKey("DealerCode")
                                              && result.RouteValues.ContainsKey("EmployeeId"))
                .IsBadRequestOnNull()
                .Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<EmployeeDto>(typeof(DealerEmployeesController), typeof(EmployeeByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetAll()
        {
            //Arrange
            var action = Assert.Action<PagedResponse<EmployeeDto>>(typeof(DealerEmployeesController), typeof(EmployeeQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Put()
        {
            //Arrange
            var action = Assert.Action(typeof(DealerEmployeesController), typeof(UpdateEmployeeCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
