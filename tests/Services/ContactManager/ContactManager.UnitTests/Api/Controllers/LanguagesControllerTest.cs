﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Moq;
using NexCore.ContactManager.Api.Controllers;
using NexCore.ContactManager.Application.Languages.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Controllers
{
    public class LanguagesControllerTest
    {
        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(LanguagesController)).HasNullGuard();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<LanguageDto>>(typeof(LanguagesController), typeof(LanguageQuery));

            //Assert
            return action.IsJson().Run();
        }
    }
}
