﻿using NexCore.ContactManager.Api.Controllers;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Individuals.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Controllers
{
    public class CommunicationMediumsControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(CommunicationMediumsController)).HasNullGuard();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<CommunicationMediumDto>>(typeof(CommunicationMediumsController), typeof(CommunicationMediumQuery));

            //Assert
            return action.IsJson().Run();
        }
    }
}
