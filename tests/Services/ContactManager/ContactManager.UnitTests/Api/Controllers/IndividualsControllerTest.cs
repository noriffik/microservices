﻿using AutoFixture;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Moq;
using NexCore.Application.Queries;
using NexCore.ContactManager.Api.Controllers;
using NexCore.ContactManager.Application.Individuals.Commands.AddIndividual;
using NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Individuals.Queries;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Controllers
{
    public class IndividualsControllerTest
    {
        private static readonly IFixture Fixture = new Fixture().Customize(new TelephoneNumberCustomization());
        private readonly Mock<IMediator> _mediator = new Mock<IMediator>();
        private readonly Mock<IAuthorizationService> _authorizationService = new Mock<IAuthorizationService>();
        private readonly IndividualsController _controller;

        public IndividualsControllerTest()
        {
            _controller = new IndividualsController(_mediator.Object, _authorizationService.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(IndividualsController)).HasNullGuard();
        }

        [Fact]
        public Task Post()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(IndividualsController), typeof(AddIndividualCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task FindSingle()
        {
            //Arrange
            var action = Assert.Action<IndividualDto>(typeof(IndividualsController), typeof(IndividualByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Put()
        {
            //Arrange
            var action = Assert.Action(typeof(IndividualsController), typeof(UpdateIndividualCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Get()
        {
            var action = Assert.Action<PagedResponse<IndividualDto>>(typeof(IndividualsController), typeof(IndividualQuery));

            //Assert
            return action.IsJson().Run();
        }
    }

}
