﻿using NexCore.ContactManager.Api.Setup.Seeds;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.Legal;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Setup.Seeds
{
    public class IndividualMapperTest
    {
        private const string Json =
            @"{{
                ""Firstname"": ""{0}"",
                ""Lastname"": ""{1}"",
                ""Middlename"": ""{2}"",
                ""Birthday"": ""{3}"",
                ""Gender"": ""{4}""
            }}";

        private readonly Individual[] _individuals;
        private readonly string _json;

        public IndividualMapperTest()
        {
            _individuals = SetupIndividuals(15);
            _json = SetupJson(_individuals);
        }

        private static string SetupJson(Individual[] individuals)
        {
            Debug.Assert(individuals != null);

            var items = string.Empty;
            foreach (var person in individuals)
            {
                var personName = person.Name;
                var birthday = person.Birthday.HasValue ? person.Birthday.Value.ToString("o") : string.Empty;
                var gender = $"{(person.Gender == Gender.Male ? "m" : "f")}";
                items += ", " + string.Format(Json, 
                             personName.Firstname, personName.Lastname, personName.Middlename,
                             birthday, gender);
            }

            return "[" + items.TrimStart(',') + "]";
        }

        private static Individual[] SetupIndividuals(int count)
        {
            var individuals = new List<Individual>(count);
            for (var i = 1; i <= count; i++)
            {
                individuals.Add(
                    new Individual(
                        new PersonName($"Firstname{i}", $"Lastname{i}", $"Middlename{i}"))
                    {
                        Gender = i % 2 == 0 ? Gender.Male : Gender.Female,
                        Birthday = DateTime.MinValue.AddDays(i)
                    });
            }

            return individuals.ToArray();
        }

        public class TestComparer : IEqualityComparer<Individual>
        {
            public static readonly TestComparer Instance = new TestComparer();

            public bool Equals(Individual x, Individual y)
            {
                return x == y || (x != null && y != null && 
                                  x.Name == y.Name && 
                                  x.Birthday == y.Birthday &&
                                  x.Gender == y.Gender);
            }

            public int GetHashCode(Individual obj)
            {
                throw new NotSupportedException();
            }
        }

        [Fact]
        public void MapIndividualsJsonData()
        {
            //Arrange
            var mapper = new IndividualJsonMapper();

            //Act
            var actual = mapper.Map(_json);

            //Assert
            Assert.NotNull(actual);
            Assert.Equal(_individuals, actual, TestComparer.Instance);
        }
    }
}

