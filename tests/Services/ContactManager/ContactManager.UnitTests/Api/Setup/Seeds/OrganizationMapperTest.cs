﻿using NexCore.ContactManager.Api.Setup.Seeds;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.Testing;
using System.Diagnostics;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Setup.Seeds
{
    public class OrganizationMapperTest
    {
        private const string Json = @"{{
            ""_id"": ""{6}"",
            ""_region"": 46,
            ""_name"": ""{0}"",
            ""_normalizedName"": null,
            ""_latitude"": {1},
            ""_longitude"": {2},
            ""_distance"": 0,
            ""_city"": ""{3}"",
            ""_zip"": ""{4}"",
            ""_street"": ""{5}"",
            ""_rating"": 0,
            ""_url"": ""http://autotrading.lviv.ua/"",
            ""_salesContact"": {{ ""_fax"": """", ""_mail"": ""atrading@mail.lviv.ua"", ""_mobile"": """", ""_telephone"": ""+380322949294"" }},
            ""_serviceContact"": {{ ""_fax"": """", ""_mail"": ""mykola.zlydnyk@atollholding.com.ua"", ""_mobile"": """", ""_telephone"": ""+380322590700"" }},
            ""_openingHours"": [ {{ ""_label"": ""Понеділок"", ""_value"": ""09:00 - 19:00"", ""_value2"": ""09:00 - 18:00"" }} ],
            ""_marker"": null
        }}";
        private readonly Organization[] _organizations;
        private readonly Dealer[] _dealers;
        private readonly string _json;

        public OrganizationMapperTest()
        {
            _organizations = SetupOrganizations(55);
            _dealers = SetupDealers(55);
            _json = SetupJson(_organizations, _dealers);
        }

        private string SetupJson(Organization[] organizations, Dealer[] dealers)
        {
            Debug.Assert(organizations != null && dealers != null && organizations.Length == dealers.Length);

            var items = string.Empty;
            for (var i = 0; i < organizations.Length; i++)
            {
                var address = organizations[i].Contact.PhysicalAddresses.First().Value;
                items += ", " + string.Format(
                             Json, organizations[i].Name,
                    address.Geolocation.Latitude, address.Geolocation.Longitude,
                    address.City, address.PostCode, address.Street, dealers[i].Id.Value);
            }

            return "[" + items.TrimStart(',') + "]";
        }

        private Organization[] SetupOrganizations(int count)
        {
            var organizations = new Organization[count];
            for (var i = 1; i <= count; i++)
            {
                var address = new PhysicalAddress(
                    $"Street{i}", null, $"City{i}", null, null, "UA", "Україна",
                    i, i + 1, i.ToString("D5"));

                var organization = new Organization(
                    $"Organization{i}",
                    Identity.WithEdrpou(i.ToString("D7")));

                organization.Contact.Add(address);

                organizations[i - 1] = organization;
            }

            return organizations;
        }

        private Dealer[] SetupDealers(int count)
        {
            return Enumerable.Range(1, count)
                .Select(i => new Dealer(i.ToString("D8"), i))
                .ToArray();
        }

        [Fact]
        public void MapOrganizationsJsonData()
        {
            //Arrange
            var mapper = new OrganizationJsonMapper();

            //Act
            var actual = mapper.Map(_json);

            //Assert
            Assert.NotNull(actual);
            Assert.Equal(_organizations, actual, PropertyComparer<Organization>.Instance);
        }

        [Fact]
        public void MapDealersJsonData()
        {
            //Arrange
            var mapper = new DealerJsonMapper();

            //Act
            var actual = mapper.Map(_json);

            //Assert
            Assert.NotNull(actual);
            Assert.Equal(_dealers, actual, PropertyComparer<Dealer>.Instance);
        }
    }
}
