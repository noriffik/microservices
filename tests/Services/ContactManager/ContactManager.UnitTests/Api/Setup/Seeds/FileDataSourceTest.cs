﻿using AutoFixture;
using Moq;
using NexCore.ContactManager.Api.Setup.Seeds;
using NexCore.Testing;
using System;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Setup.Seeds
{
    public class FileDataSourceTest
    {
        private const string Path = "./Api/Setup/Seeds/TestData.txt";

        private readonly Fixture _fixture = new Fixture();
        private readonly Mock<IMapper<TestAggregateRoot>> _mapper;

        public FileDataSourceTest()
        {
            _mapper = new Mock<IMapper<TestAggregateRoot>>();
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Ctor_GivenPath_IsNullOrEmpty_Throws(string path)
        {
            Assert.Throws<ArgumentException>("path", () =>
                new AggregateRootFileDataSource<TestAggregateRoot>(path, _mapper.Object));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
                new AggregateRootFileDataSource<TestAggregateRoot>(Path, null));
        }

        [Fact]
        public void Get()
        {
            //Arrange
            var expected = _fixture.CreateMany<TestAggregateRoot>(3).ToList();
            var dataSource = new AggregateRootFileDataSource<TestAggregateRoot>(Path, _mapper.Object);
            _mapper.Setup(m => m.Map("TestData"))
                .Returns(expected);

            //Act
            var actual = dataSource.Get();

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
