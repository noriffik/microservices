﻿using AutoFixture;
using Moq;
using NexCore.ContactManager.Api.Authorization;
using NexCore.ContactManager.Api.Authorization.RequestFilters.Distributors.Dealers.Leads;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Leads.OpenLead;
using NexCore.Testing;
using NexCore.WebApi.Authorization;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Authorization.RequestFilters.Distributors.Dealers.Leads
{
    public class OpenLeadCommandRequestFilterTest
    {
        private readonly int _employeeId;

        //Dependencies
        private readonly Mock<ClaimsPrincipal> _user;

        //Command and Filter
        private readonly OpenLeadCommand _actualCommand;
        private readonly OpenLeadCommand _expectedCommand;
        private readonly OpenLeadCommandRequestFilter _filter;

        public OpenLeadCommandRequestFilterTest()
        {
            var fixture = new Fixture();

            _employeeId = fixture.Create<int>();

            //Dependencies
            var userProvider = new Mock<ICurrentUserProvider>();
            _user = new Mock<ClaimsPrincipal>();
            userProvider.Setup(p => p.GetUser()).Returns(_user.Object);

            //Command and Filter
            _actualCommand = fixture.Create<OpenLeadCommand>();
            _expectedCommand = fixture.Build<OpenLeadCommand>()
                .With(c => c.DealerCode, _actualCommand.DealerCode)
                .With(c => c.DistributorId, _actualCommand.DistributorId)
                .With(c => c.IndividualId, _actualCommand.IndividualId)
                .With(c => c.OrganizationId, _actualCommand.OrganizationId)
                .With(c => c.OwnerEmployeeId, _actualCommand.OwnerEmployeeId)
                .With(c => c.TopicId, _actualCommand.TopicId)
                .Create();
            _filter = new OpenLeadCommandRequestFilter(userProvider.Object);
        }

        [Fact]
        public void Handle()
        {
            //Arrange
            _expectedCommand.OwnerEmployeeId = _employeeId;
            _actualCommand.OwnerEmployeeId = null;
            _user.Setup(u => u.FindFirst(Claims.EmployeeId))
                .Returns(new Claim(Claims.EmployeeId, _employeeId.ToString()));

            //Act
            _filter.Handle(_actualCommand, CancellationToken.None, () => Task.FromResult(1));

            //Assert
            Assert.Equal(_expectedCommand, _actualCommand, PropertyComparer<OpenLeadCommand>.Instance);
        }

        [Fact]
        public void Handle_WhenEmployeeId_IsNotFound_InClaims()
        {
            //Arrange
            _actualCommand.OwnerEmployeeId = null;
            _expectedCommand.OwnerEmployeeId = null;

            //Act
            _filter.Handle(_actualCommand, CancellationToken.None, () => Task.FromResult(1));

            //Assert
            Assert.Equal(_expectedCommand, _actualCommand, PropertyComparer<OpenLeadCommand>.Instance);
            _user.Verify(u => u.FindFirst(Claims.EmployeeId), Times.Once);
        }

        [Fact]
        public void Handle_WhenEmployeeId_IsAlreadySet_DoNothing()
        {
            //Act
            _filter.Handle(_actualCommand, CancellationToken.None, () => Task.FromResult(1));

            //Assert
            Assert.Equal(_expectedCommand, _actualCommand, PropertyComparer<OpenLeadCommand>.Instance);
            _user.Verify(u => u.FindFirst(Claims.EmployeeId), Times.Never);
        }
    }
}
