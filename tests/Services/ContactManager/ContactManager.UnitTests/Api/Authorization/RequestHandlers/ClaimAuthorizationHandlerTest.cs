﻿using Microsoft.AspNetCore.Authorization;
using Moq;
using NexCore.ContactManager.Api.Authorization.RequirementHandlers;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Authorization.RequestHandlers
{
    public class ClaimAuthorizationHandlerTest
    {
        private const string ClaimType = "MyClaimType";
        private const string ClaimValue = "MyClaimValue";

        private readonly IAuthorizationRequirement _requirement;
        private readonly Mock<ClaimsPrincipal> _user;
        private readonly Mock<AuthorizationHandlerContext> _context;

        private readonly ClaimAuthorizationHandler<IAuthorizationRequirement, object> _handler;

        public ClaimAuthorizationHandlerTest()
        {
            _requirement = new Mock<IAuthorizationRequirement>().Object;
            _user = new Mock<ClaimsPrincipal>();

            _context = new Mock<AuthorizationHandlerContext>(new[] {_requirement}, _user.Object, new Mock<object>().Object)
            {
                CallBase = true
            };

            _handler = new ClaimAuthorizationHandler<IAuthorizationRequirement, object>(ClaimType, obj => ClaimValue);
        }

        [Fact]
        public void Ctor_GivenClaimType_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("claimType", () => new ClaimAuthorizationHandler<IAuthorizationRequirement, object>(null, obj => ClaimValue));
        }

        [Fact]
        public void Ctor_GivenClaimValue_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("claimValue", () => new ClaimAuthorizationHandler<IAuthorizationRequirement, object>(ClaimType, null));
        }

        [Fact]
        public async Task Handle_WhenUser_Has_RequiredClaimValue_MarksRequirement_AsEvaluated()
        {
            //Arrange
            _user.Setup(u => u.HasClaim(ClaimType, ClaimValue)).Returns(true);

            //Act
            await _handler.HandleAsync(_context.Object);

            //Assert
            _context.Verify(c => c.Succeed(_requirement));
        }

        [Fact]
        public async Task Handle_WhenUser_DoesNotHave_RequiredClaimValue_DoesNothing()
        {
            //Act
            await _handler.HandleAsync(_context.Object);

            //Assert
            _context.Verify(c => c.Succeed(_requirement), Times.Never);
        }

        [Fact]
        public async Task Handle_WhenResourceClaimValue_IsNull_DoesNothing()
        {
            //Arrange
            var handler = new ClaimAuthorizationHandler<IAuthorizationRequirement, object>(ClaimType, obj => null);

            //Act
            await handler.HandleAsync(_context.Object);

            //Assert
            _user.Verify(u => u.HasClaim(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            _context.Verify(c => c.Succeed(_requirement), Times.Never);
        }
    }
}
