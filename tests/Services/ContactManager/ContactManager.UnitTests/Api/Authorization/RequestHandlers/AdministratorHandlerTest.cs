﻿using Microsoft.AspNetCore.Authorization;
using Moq;
using NexCore.ContactManager.Api.Authorization;
using NexCore.ContactManager.Api.Authorization.RequirementHandlers;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Api.Authorization.RequestHandlers
{
    public class AdministratorHandlerTest
    {
        private readonly IAuthorizationRequirement _requirement;
        private readonly Mock<ClaimsPrincipal> _user;
        private readonly Mock<AuthorizationHandlerContext> _context;

        private readonly AdministratorHandler<IAuthorizationRequirement> _handler;

        public AdministratorHandlerTest()
        {
            _requirement = new Mock<IAuthorizationRequirement>().Object;
            _user = new Mock<ClaimsPrincipal>();

            _context = new Mock<AuthorizationHandlerContext>(new[] {_requirement}, _user.Object, new Mock<object>().Object)
            {
                CallBase = true
            };

            _handler = new AdministratorHandler<IAuthorizationRequirement>();
        }

        [Fact]
        public async Task Handle_WhenUser_IsIn_AdministratorRole_MarksRequirement_AsEvaluated()
        {
            //Arrange
            _user.Setup(u => u.IsInRole(Roles.Administrator)).Returns(true);

            //Act
            await _handler.HandleAsync(_context.Object);

            //Assert
            _context.Verify(c => c.Succeed(_requirement));
        }

        [Fact]
        public async Task Handle_WhenUser_IsNot_InAdministratorRole_DoesNothing()
        {
            //Act
            await _handler.HandleAsync(_context.Object);

            //Assert
            _context.Verify(c => c.Succeed(_requirement), Times.Never);
        }
    }
}
