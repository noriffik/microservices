﻿using AutoFixture;
using AutoFixture.Kernel;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.UnitTests.Fixtures.Domain;
using NexCore.Testing.Fixtures.SpecimenBuilders.Strings;

namespace NexCore.ContactManager.UnitTests.Fixtures.Api.Models
{
    public class ContactModelCustomization : CompositeCustomization
    {
        public ContactModelCustomization() : base(
            new TelephoneNumberCustomization(),
            new EmailAddressModelCustomization(),
            new DealerCodeCustomization(),
            new DistributorIdCustomization(),
            new DealerIdCustomization(),
            new IndividualCustomizations())
        {
        }
    }

    public class TelephoneNumberCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            var numbers = new SpecimenContext(new NumericStringGenerator());

            fixture.Customize<TelephoneNumberDto>(c => c
                .With(p => p.CountryCode, numbers.Resolve(new NumericString(2)))
                .With(p => p.AreaCode, numbers.Resolve(new NumericString(3)))
                .With(p => p.LocalNumber, numbers.Resolve(new NumericString(7))));
        }
    }

    public class EmailAddressModelCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customize<EmailAddressDto>(c => c
                .With(p => p.Address, new SpecimenContext(new EmailStringBuilder()).Resolve(typeof(string))));
        }
    }
}
