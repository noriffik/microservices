﻿using AutoFixture.Kernel;
using NexCore.Legal;
using System;
using System.Linq;

namespace NexCore.ContactManager.UnitTests.Fixtures.Domain.Individuals
{
    public class FamilyStatusBuilder : ISpecimenBuilder
    {
        private static readonly int LastId = FamilyStatus.All.Last().Id;
        private readonly Random _random = new Random();

        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type;
            if (type == null || type != typeof(FamilyStatus))
                return new NoSpecimen();

            return FamilyStatus.Get(_random.Next(1, LastId));
        }
    }
}
