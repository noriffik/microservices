﻿using AutoFixture;
using NexCore.ContactManager.UnitTests.Fixtures.Domain.Individuals;

namespace NexCore.ContactManager.UnitTests.Fixtures.Domain
{
    public class IndividualCustomizations : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customizations.Add(new FamilyStatusBuilder());
        }
    }
}
