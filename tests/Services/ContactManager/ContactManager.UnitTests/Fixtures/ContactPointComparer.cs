﻿using NexCore.ContactManager.Domain.Contacts;
using NexCore.Domain;
using System;
using System.Collections.Generic;

namespace NexCore.ContactManager.UnitTests.Fixtures
{
    class ContactPointComparer<TValue> : IEqualityComparer<IContactPoint<TValue>> where TValue : ValueObject
    {
        private static readonly IDictionary<Type, IEqualityComparer<IContactPoint<TValue>>> _instances =
            new Dictionary<Type, IEqualityComparer<IContactPoint<TValue>>>();

        public bool Equals(IContactPoint<TValue> x, IContactPoint<TValue> y)
        {
            return ReferenceEquals(x, y) || x != null
                   && x.Value == y.Value
                   && x.IsPrimary == y.IsPrimary
                   && x.Description == y.Description;
        }

        public static IEqualityComparer<IContactPoint<TValue>> Instance
        {
            get
            {
                var type = typeof(IEqualityComparer<IContactPoint<TValue>>);
                if (!_instances.ContainsKey(type))
                    _instances[type] = new ContactPointComparer<TValue>();
            
                return _instances[type];
            }
        }

        public int GetHashCode(IContactPoint<TValue> obj)
        {
            throw new NotSupportedException();
        }
    }
}