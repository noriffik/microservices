﻿using System;
using System.Linq;
using AutoFixture.Kernel;

namespace NexCore.ContactManager.UnitTests.Fixtures
{
    class EmailStringBuilder : ISpecimenBuilder
    {
        private readonly Random _random = new Random();

        public object Create(object request, ISpecimenContext context)
        {
            if (request as Type == typeof(string))
                return $"{Generate(10)}@{Generate(8)}.com";

            return new NoSpecimen();
        }

        private string Generate(int length)
        {
            return new string(
                Enumerable.Range(0, length)
                    .Select(v => (char) _random.Next(65, 91))
                    .ToArray());
        }
    }
}