﻿using AutoMapper;
using NexCore.ContactManager.Application.Organizations.Commands.AddOrganization;
using NexCore.ContactManager.Infrastructure;

namespace NexCore.ContactManager.UnitTests.Fixtures
{
    public static class AutoMapperFactory
    {
        public static IMapper Create()
        {
            var application = typeof(AddOrganizationCommand).Assembly;
            var infrastructure = typeof(ContactContext).Assembly;

            var mapperConfig = new MapperConfiguration(
                c => c.AddProfiles(application, infrastructure));

            return mapperConfig.CreateMapper();
        }
    }
}
