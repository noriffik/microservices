﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.ContactManager.Infrastructure;
using NexCore.Testing.Extensions;

namespace NexCore.ContactManager.UnitTests.Fixtures.Infrastructure
{
    public partial class InMemoryDbContextFactory
    {
        private class TestDbContext : ContactContext
        {
            public TestDbContext(DbContextOptions<ContactContext> options, IMediator mediator)
                : base(options, mediator)
            {
            }

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                base.OnModelCreating(modelBuilder);

                modelBuilder.SetAutoIncrementStep(1000);
            }
        }
    }
}
