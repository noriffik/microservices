﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NexCore.ContactManager.Infrastructure;
using System;

namespace NexCore.ContactManager.UnitTests.Fixtures.Infrastructure
{
    public partial class InMemoryDbContextFactory
    {
        public readonly string DatabaseName;
        public readonly Mock<IMediator> Mediator = new Mock<IMediator>();

        public static InMemoryDbContextFactory Instance => new InMemoryDbContextFactory(Guid.NewGuid().ToString());

        public InMemoryDbContextFactory(Type typeUnderTest) : this(typeUnderTest.FullName)
        {
        }

        public InMemoryDbContextFactory(string databaseName)
        {
            DatabaseName = databaseName;
        }

        public ContactContext Create()
        {
            var services = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase();

            var mediator = new Mock<IMediator>().Object;

            return new TestDbContext(
                new DbContextOptionsBuilder<ContactContext>()
                    .UseInMemoryDatabase(DatabaseName)
                    .UseInternalServiceProvider(services.BuildServiceProvider())
                    .Options,
                mediator);
        }
    }
}
