﻿using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Testing.Seeders;
using System.Threading.Tasks;

namespace NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class DistributorSeeder : EntitySeeder<Distributor, DistributorId>
    {
        public DistributorSeeder(ContactContext context) : base(context)
        {
            Fixture.Customize(new DistributorIdCustomization());
            Fixture.Customize(new DealerIdCustomization());
        }

        public override async Task<Distributor> Add() => await AddDependencies(await base.Add());

        public override async Task<Distributor> Add(Distributor distributor) => await AddDependencies(await base.Add(distributor));

        public override async Task<Distributor> Add(object parameters) => await AddDependencies(await base.Add(parameters));

        public override async Task<Distributor> Add(DistributorId id, object parameters) => await AddDependencies(await base.Add(id, parameters));

        private async Task<Distributor> AddDependencies(Distributor distributor)
        {
            await FindOrAdd<Organization>(distributor.OrganizationId);

            return distributor;
        }
    }
}
