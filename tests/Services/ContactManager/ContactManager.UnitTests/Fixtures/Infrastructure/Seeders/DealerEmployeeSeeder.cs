﻿using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Testing.Seeders;
using System;
using System.Threading.Tasks;

namespace NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class DealerEmployeeSeeder : EntitySeeder<DealerEmployee>
    {
        public DealerEmployeeSeeder(ContactContext context) : base(context)
        {
            Fixture.Customize(new DealerIdCustomization());
        }

        public override async Task<DealerEmployee> Add() => await AddDependencies(await base.Add());

        public override async Task<DealerEmployee> Add(DealerEmployee employee) => await AddDependencies(await base.Add(employee));

        public override async Task<DealerEmployee> Add(object parameters) => await AddDependencies(await base.Add(parameters));

        public override async Task<DealerEmployee> Add(int id, object parameters) => await AddDependencies(await base.Add(id, parameters));

        private async Task<DealerEmployee> AddDependencies(DealerEmployee employee)
        {
            await FindOrAdd<Organization>(employee.OrganizationId);

            await FindOrAdd<Individual>(employee.IndividualId);

            await FindOrAdd<Job>(employee.JobId);

            if (Has<Dealer, DealerId>(employee.DealerId))
            {
                var dealer = Find<Dealer, DealerId>(employee.DealerId);
                if (dealer.OrganizationId != employee.OrganizationId)
                    throw new InvalidOperationException(
                        "OrganizationId of Dealer is not equal to OrganizationId of DealerEmployee");
            }
            else
                await Add<Dealer, DealerId>(employee.DealerId, new { organizationId = employee.OrganizationId });

            var distributor = await FindOrAdd<Distributor, DistributorId>(employee.DealerId.DistributorId);
            await FindOrAdd<Organization>(distributor.OrganizationId);

            return employee;
        }
    }
}
