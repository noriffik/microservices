﻿using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Testing.Seeders;
using System;
using System.Threading.Tasks;

namespace NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class DealerPrivateCustomerSeeder : EntitySeeder<DealerPrivateCustomer>
    {
        public DealerPrivateCustomerSeeder(ContactContext context) : base(context)
        {
            Fixture.Customize(new DealerIdCustomization());
        }

        public override async Task<DealerPrivateCustomer> Add() => await AddDependencies(await base.Add());

        public override async Task<DealerPrivateCustomer> Add(DealerPrivateCustomer privateCustomer) => await AddDependencies(await base.Add(privateCustomer));

        public override async Task<DealerPrivateCustomer> Add(object parameters) => await AddDependencies(await base.Add(parameters));

        public override async Task<DealerPrivateCustomer> Add(int id, object parameters) => await AddDependencies(await base.Add(id, parameters));

        private async Task<DealerPrivateCustomer> AddDependencies(DealerPrivateCustomer privateCustomer)
        {
            await FindOrAdd<Individual>(privateCustomer.ContactableId);

            await FindOrAdd<Organization>(privateCustomer.OrganizationId);

            if (Has<Dealer, DealerId>(privateCustomer.DealerId))
            {
                var dealer = Find<Dealer, DealerId>(privateCustomer.DealerId);
                if (dealer.OrganizationId != privateCustomer.OrganizationId)
                    throw new InvalidOperationException(
                        "OrganizationId of Dealer is not equal to OrganizationId of DealerPrivateCustomer");
            }
            else
                await Add<Dealer, DealerId>(privateCustomer.DealerId, new { organizationId = privateCustomer.OrganizationId });

            var distributor = await FindOrAdd<Distributor, DistributorId>(privateCustomer.DealerId.DistributorId);
            await FindOrAdd<Organization>(distributor.OrganizationId);

            return privateCustomer;
        }
    }
}
