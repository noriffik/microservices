﻿using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.DistributorEmployees;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Testing.Seeders;
using System;
using System.Threading.Tasks;

namespace NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class DistributorEmployeeSeeder : EntitySeeder<DistributorEmployee>
    {
        public DistributorEmployeeSeeder(ContactContext context) : base(context)
        {
            Fixture.Customize(new DistributorIdCustomization());
        }

        public override async Task<DistributorEmployee> Add() => await AddDependencies(await base.Add());

        public override async Task<DistributorEmployee> Add(DistributorEmployee employee) => await AddDependencies(await base.Add(employee));

        public override async Task<DistributorEmployee> Add(object parameters) => await AddDependencies(await base.Add(parameters));

        public override async Task<DistributorEmployee> Add(int id, object parameters) => await AddDependencies(await base.Add(id, parameters));

        private async Task<DistributorEmployee> AddDependencies(DistributorEmployee employee)
        {
            await FindOrAdd<Organization>(employee.OrganizationId);

            await FindOrAdd<Individual>(employee.IndividualId);

            await FindOrAdd<Job>(employee.JobId);

            if (Has<Distributor, DistributorId>(employee.DistributorId))
            {
                var distributor = Find<Distributor, DistributorId>(employee.DistributorId);

                if (distributor.OrganizationId != employee.OrganizationId)
                    throw new InvalidOperationException(
                        "OrganizationId of Distributor is not equal to OrganizationId of DistributorEmployee");
            }
            else
                await Add<Distributor, DistributorId>(employee.DistributorId, new { organizationId = employee.OrganizationId });

            return employee;
        }
    }
}
