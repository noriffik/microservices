﻿using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Testing.Seeders;
using System.Threading.Tasks;

namespace NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class DealerSeeder : EntitySeeder<Dealer, DealerId>
    {
        public DealerSeeder(ContactContext context) : base(context)
        {
            Fixture.Customize(new DealerIdCustomization());
        }

        public override async Task<Dealer> Add() => await AddDependencies(await base.Add());

        public override async Task<Dealer> Add(Dealer dealer) => await AddDependencies(await base.Add(dealer));

        public override async Task<Dealer> Add(object parameters) => await AddDependencies(await base.Add(parameters));

        public override async Task<Dealer> Add(DealerId id, object parameters) => await AddDependencies(await base.Add(id, parameters));

        private async Task<Dealer> AddDependencies(Dealer dealer)
        {
            var distributor = await FindOrAdd<Distributor, DistributorId>(dealer.DistributorId);

            await FindOrAdd<Organization>(dealer.OrganizationId);
            await FindOrAdd<Organization>(distributor.OrganizationId);

            return dealer;
        }
    }
}
