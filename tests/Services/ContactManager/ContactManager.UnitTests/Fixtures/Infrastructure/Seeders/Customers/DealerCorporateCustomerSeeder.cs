﻿using NexCore.ContactManager.Domain.DealerCorporateCustomers;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.Testing.Seeders;
using System.Threading.Tasks;

namespace NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders.Customers
{
    public class DealerCorporateCustomerSeeder : EntitySeeder<DealerCorporateCustomer>
    {
        public DealerCorporateCustomerSeeder(ContactContext context) : base(context)
        {
            Fixture.Customize(new DealerIdCustomization());
        }

        public override async Task<DealerCorporateCustomer> Add() => await AddDependencies(await base.Add());

        public override async Task<DealerCorporateCustomer> Add(DealerCorporateCustomer corporateCustomer) => await AddDependencies(await base.Add(corporateCustomer));

        public override async Task<DealerCorporateCustomer> Add(object parameters) => await AddDependencies(await base.Add(parameters));

        public override async Task<DealerCorporateCustomer> Add(int id, object parameters) => await AddDependencies(await base.Add(id, parameters));

        private async Task<DealerCorporateCustomer> AddDependencies(DealerCorporateCustomer corporateCustomer)
        {
            await FindOrAdd<Organization>(corporateCustomer.OrganizationId);
            await FindOrAdd<Organization>(corporateCustomer.ContactableId);

            return corporateCustomer;
        }
    }
}
