﻿using AutoFixture;
using Microsoft.EntityFrameworkCore;
using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Testing.Seeders;
using System;
using System.Threading.Tasks;

namespace NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class DealerLeadSeeder : EntitySeeder<Lead>
    {
        public DealerLeadSeeder(DbContext context) : base(context)
        {
            Fixture.Customize(new DealerIdCustomization());
        }

        public DealerLeadSeeder(DbContext context, IFixture fixture) : base(context, fixture)
        {
            fixture.Customize(new DealerIdCustomization());
        }

        public override async Task<Lead> Add() => await AddDependencies(await base.Add());

        public override async Task<Lead> Add(Lead privateCustomer) =>
            await AddDependencies(await base.Add(privateCustomer));

        public override async Task<Lead> Add(object parameters) => await AddDependencies(await base.Add(parameters));

        public override async Task<Lead> Add(int id, object parameters) =>
            await AddDependencies(await base.Add(id, parameters));

        private async Task<Lead> AddDependencies(Lead lead)
        {
            var dealer = await FindOrAddDealer(lead.DealerId);

            if (lead.OwnerEmployeeId.HasValue)
                await FindOrAddDealerEmployee(lead.OwnerEmployeeId.Value, dealer);

            await FindOrAdd<Topic>(lead.TopicId);

            await FindOrAdd<Individual>(lead.Contact.IndividualId);

            if (lead.Contact.OrganizationId.HasValue)
                await FindOrAdd<Organization>(lead.Contact.OrganizationId.Value);

            return lead;
        }

        private async Task<Dealer> FindOrAddDealer(DealerId dealerId)
        {
            var dealer = await FindOrAdd<Dealer, DealerId>(dealerId);
            await FindOrAdd<Organization>(dealer.OrganizationId);

            var distributor = await FindOrAdd<Distributor, DistributorId>(dealer.DistributorId);
            await FindOrAdd<Organization>(distributor.OrganizationId);

            return dealer;
        }

        private async Task<DealerEmployee> FindOrAddDealerEmployee(int employeeId, Dealer dealer)
        {
            DealerEmployee employee;
            if (!Has<DealerEmployee>(employeeId))
            {
                employee = await Add<DealerEmployee>(employeeId,
                    new { organizationId = dealer.OrganizationId, dealerId = dealer.Id });
            }
            else
            {
                employee = Find<DealerEmployee>(employeeId);

                if (dealer.Id != employee.DealerId)
                    throw new InvalidOperationException(
                        "OwnerEmployee is not employee of specified Dealer");

                if (dealer.OrganizationId != employee.OrganizationId)
                    throw new InvalidOperationException(
                        "OrganizationId of Dealer is not equal to OrganizationId of OwnerEmployee");
            }

            await FindOrAdd<Individual>(employee.IndividualId);

            return employee;
        }
    }
}
