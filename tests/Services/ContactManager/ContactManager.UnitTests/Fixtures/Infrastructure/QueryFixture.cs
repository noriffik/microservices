﻿using AutoMapper;
using MediatR;
using NexCore.ContactManager.Infrastructure;
using System;
using System.Threading.Tasks;

namespace NexCore.ContactManager.UnitTests.Fixtures.Infrastructure
{
    public abstract class QueryFixture<TQuery, TResponse> where TQuery : IRequest<TResponse>
    {
        protected bool IsReady;

        public readonly IMapper Mapper;

        protected QueryFixture()
        {
            Mapper = AutoMapperFactory.Create();
        }

        public virtual Task Setup(ContactContext context)
        {
            IsReady = true;

            return Task.CompletedTask;
        }

        public abstract TQuery GetQuery();

        public abstract TQuery GetUnpreparedQuery();

        public abstract TResponse GetResponse();

        protected void ThrowSetupIsRequired()
        {
            if (!IsReady)
                throw new InvalidOperationException("Setup must be called first.");
        }
    }
}