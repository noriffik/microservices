﻿using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Domain.Contacts;

namespace NexCore.ContactManager.UnitTests.Fixtures
{
    public static class ContactableTestHelper
    {
        public static Telephone AssignTelephone(IContactable contactable)
        {
            var telephone = new Telephone("38", "044", "1112233");
            contactable.Contact.Add(telephone);

            return telephone;
        }

        public static TelephoneNumberDto MapToTelephoneNumber(Telephone telephone)
        {
            return new TelephoneNumberDto
            {
                AreaCode = telephone.AreaCode,
                CountryCode = telephone.CountryCode,
                LocalNumber = telephone.LocalNumber
            };
        }
    }
}
