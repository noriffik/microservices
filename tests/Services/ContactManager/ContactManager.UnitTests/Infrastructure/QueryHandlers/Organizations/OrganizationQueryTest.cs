﻿using AutoFixture;
using AutoMapper;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Organizations.Dtos;
using NexCore.ContactManager.Application.Organizations.Queries;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Organizations;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.Legal;
using NexCore.Testing;
using NexCore.Testing.Seeders;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Organizations
{
    public class OrganizationQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly InMemoryDbContextFactory _factory = InMemoryDbContextFactory.Instance;

        [Fact]
        public async Task Handle()
        {
            using (var context = _factory.Create())
            {
                //Assert
                //Seed Organizations
                var seeder = new EntitySeeder<Organization>(context);
                var organizations = (await seeder.AddRange(10)).ToList();
                organizations.ForEach(o => o.ChangeRequisites(seeder.Create<OrganizationRequisites>()));
                await seeder.SeedAsync();

                //Setup query and handler
                var query = new OrganizationQuery
                {
                    PageNumber = 2,
                    PageSize = 3
                };
                var handler = new OrganizationQueryHandler(context, _mapper);

                //Expected result
                var items = organizations
                    .OrderBy(o => o.Name)
                    .Skip((query.PageNumber.Value - 1) * query.PageSize.Value)
                    .Take(query.PageSize.Value);
                var expected = new PagedResponse<OrganizationDto>(PageOptions.From(query), organizations.Count,
                    items.Select(MapOrganizationDto).ToArray());

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Arrange
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<OrganizationDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WithNameIsSet()
        {
            using (var context = _factory.Create())
            {
                //Assert

                //Seed Organizations
                var seeder = new EntitySeeder<Organization>(context);
                await seeder.AddRange(5);

                var organizations = (await seeder.AddRange(2, new { name = $"_Sample{System.Guid.NewGuid()}" }))
                    .OrderBy(c => c.Name).ToList();

                await seeder.SeedAsync();

                //Setup query and handler
                var query = new OrganizationQuery
                {
                    Name = "  sample ",
                    PageSize = organizations.Count
                };
                var handler = new OrganizationQueryHandler(context, _mapper);

                //Expected result
                var expected = new PagedResponse<OrganizationDto>(PageOptions.From(query), organizations.Count,
                    organizations.Select(MapOrganizationDto).ToArray());

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Arrange
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<OrganizationDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WithIpnIsSet()
        {
            using (var context = _factory.Create())
            {
                //Assert
                //Seed Organizations
                var seeder = new EntitySeeder<Organization>(context);
                await seeder.AddRange(5);

                 var organizations = (await seeder.AddRange(
                        new { identity = Identity.WithIpn($"_Sample{System.Guid.NewGuid()}") },
                        new { identity = Identity.WithIpn($"_Sample{System.Guid.NewGuid()}") },
                        new { identity = Identity.WithIpn($"_Sample{System.Guid.NewGuid()}") }))
                    .OrderBy(c => c.Name).ToList();

                await seeder.SeedAsync();

                //Setup query and handler
                var query = new OrganizationQuery
                {
                    Ipn = " sample  ",
                    PageSize = 3
                };
                var handler = new OrganizationQueryHandler(context, _mapper);

                //Expected result
                var expected = new PagedResponse<OrganizationDto>(PageOptions.From(query), organizations.Count,
                    organizations.Select(MapOrganizationDto).ToArray());

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Arrange
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<OrganizationDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WithEdrpouIsSet()
        {
            using (var context = _factory.Create())
            {
                //Assert
                //Seed Organizations
                var seeder = new EntitySeeder<Organization>(context);

                await seeder.AddRange(5);

                var organizationsExpected = (await seeder.AddRange(
                        new { identity = Identity.WithEdrpou($"_Sample{System.Guid.NewGuid()}") },
                        new { identity = Identity.WithEdrpou($"_Sample{System.Guid.NewGuid()}") },
                        new { identity = Identity.WithEdrpou($"_Sample{System.Guid.NewGuid()}") }))
                    .OrderBy(c => c.Name).ToList();
                organizationsExpected.ForEach(o => o.ChangeRequisites(seeder.Create<OrganizationRequisites>()));

                await seeder.SeedAsync();

                //Setup query and handler
                var query = new OrganizationQuery
                {
                    Edrpou = " sample  ",
                    PageNumber = 1,
                    PageSize = 3
                };
                var handler = new OrganizationQueryHandler(context, _mapper);

                //Expected result
                var items = organizationsExpected.OrderBy(o => o.Name);
                var expected = new PagedResponse<OrganizationDto>(PageOptions.From(query), organizationsExpected.Count,
                    items.Select(MapOrganizationDto).ToArray());

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Arrange
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<OrganizationDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsEmpty()
        {
            using (var context = _factory.Create())
            {
                //Act
                var query = new Fixture().Create<OrganizationQuery>();
                var handler = new OrganizationQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Arrange
                Assert.Equal(0, result.Total);
                Assert.Equal(0, result.Last);
                Assert.Equal(query.PageNumber, result.Current);
                Assert.Equal(query.PageSize, result.Size);
                Assert.Empty(result.Items);
            }
        }

        public static OrganizationDto MapOrganizationDto(Organization organization)
        {
            var emails = organization.Contact.EmailAddresses.Select(e => new EmailAddressDto
            {
                IsPrimary = e.IsPrimary,
                Description = e.Description,
                Address = e.Value.Address
            });

            var telephones = organization.Contact.Telephones.Select(t => new TelephoneDto
            {
                Number = new TelephoneNumberDto
                {
                    AreaCode = t.Value.AreaCode,
                    CountryCode = t.Value.CountryCode,
                    LocalNumber = t.Value.LocalNumber
                },
                Description = t.Description,
                IsPrimary = t.IsPrimary,
                Type = t.Value.Type
            });

            var address = organization.Contact.PhysicalAddresses.Select(a => new PhysicalAddressDto
            {
                City = new PhysicalAddressElementDto
                {
                    Code = a.Value.City.Code,
                    Name = a.Value.City.Value
                },
                Country = new PhysicalAddressElementDto
                {
                    Code = a.Value.Country.Code,
                    Name = a.Value.Country.Value
                },
                Description = a.Description,
                Geolocation = new GeolocationDto
                {
                    Latitude = a.Value.Geolocation.Latitude,
                    Longitude = a.Value.Geolocation.Longitude
                },
                IsPrimary = a.IsPrimary,
                PostCode = a.Value.PostCode.Value,
                Region = new PhysicalAddressElementDto
                {
                    Code = a.Value.Region.Code,
                    Name = a.Value.Region.Value
                },
                Street = a.Value.Street.Value
            });

            return new OrganizationDto
            {
                Id = organization.Id,
                Name = organization.Name,
                Identity = new IdentityDto
                {
                    Edrpou = organization.Identity.Edrpou,
                    Ipn = organization.Identity.Ipn,
                },
                Contact = new ContactDto
                {
                    Addresses = address,
                    Emails = emails,
                    Telephones = telephones
                },
                Requisites = organization.Requisites == OrganizationRequisites.Empty ? null : new OrganizationRequisitesDto
                {
                    Telephones = organization.Requisites.Telephones,
                    Address = organization.Requisites.Address,
                    BankRequisites = organization.Requisites.BankRequisites,
                    FullName = organization.Requisites.Name.FullName,
                    ShortName = organization.Requisites.Name.ShortName,
                    Director = new PersonNameDto
                    {
                        Firstname = organization.Requisites.Director.Firstname,
                        Lastname = organization.Requisites.Director.Lastname,
                        Middlename = organization.Requisites.Director.Middlename
                    }
                }
            };
        }
    }
}
