﻿using AutoMapper;
using NexCore.ContactManager.Application.Organizations.Queries;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Organizations;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Organizations
{
    public class OrganizationQueryHandlerTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly InMemoryDbContextFactory _factory = new InMemoryDbContextFactory(typeof(OrganizationQueryHandlerTest));

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new OrganizationQueryHandler(null, _mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                using (var context = _factory.Create())
                {
                    var unused = new OrganizationQueryHandler(context, null);
                }
            });
        }

        [Theory]
        [InlineData(typeof(OrganizationByIdQuery))]
        [InlineData(typeof(OrganizationQuery))]
        public async Task Handle_GivenRequest_IsNull_Throws(Type queryType)
        {
            using (var context = _factory.Create())
            {
                var handler = new OrganizationQueryHandler(context, _mapper);

                await Assert.RequestHandler(handler).ThrowsOnNull(queryType);
            }
        }
    }
}
