﻿using AutoFixture;
using AutoMapper;
using NexCore.ContactManager.Application.Organizations.Queries;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Organizations;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.Legal;
using NexCore.Testing;
using NexCore.Testing.Seeders;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Organizations
{
    public class OrganizationByIdQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Assert
                var seeder = new EntitySeeder<Organization>(context);
                await seeder.AddRange(3);

                var organization = await seeder.Add(56859);
                organization.ChangeRequisites(seeder.Create<OrganizationRequisites>());

                await seeder.SeedAsync();

                var query = new OrganizationByIdQuery { Id = organization.Id };
                var handler = new OrganizationQueryHandler(context, _mapper);
                var expected = _mapper.Map<OrganizationDto>(organization);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Arrange
                Assert.Equal(expected, actual, PropertyComparer<OrganizationDto>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Act
                var fixture = new Fixture();
                var query = fixture.Create<OrganizationByIdQuery>();
                var handler = new OrganizationQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Arrange
                Assert.Null(result);
            }
        }
    }
}
