﻿using AutoFixture;
using AutoMapper;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Employees;
using NexCore.ContactManager.Infrastructure.QueryHandlers.DealerEmployees;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.Testing;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.DealerEmployees
{
    public class EmployeeQueryHandlerTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly InMemoryDbContextFactory _factory = new InMemoryDbContextFactory(typeof(EmployeeQueryHandlerTest));

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new EmployeeQueryHandler(null, _mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = _factory.Create())
                {
                    var unused = new EmployeeQueryHandler(context, null);
                }
            });
        }

        [Fact]
        public async Task Handle_FindById_GivenRequest_IsNull_Throws()
        {
            using (var context = _factory.Create())
            {
                var handler = new EmployeeQueryHandler(context, _mapper);

                await Assert.RequestHandler(handler).ThrowsOnNull(typeof(EmployeeByIdQuery));
            }
        }

        [Fact]
        public async Task Handle_FindByDealerId_GivenRequest_IsNull_Throws()
        {
            using (var context = _factory.Create())
            {
                var handler = new EmployeeQueryHandler(context, _mapper);

                await Assert.RequestHandler(handler).ThrowsOnNull(typeof(EmployeeQuery));
            }
        }

        [Fact]
        public async Task Handle_FindByDealerId_GivenDealerId_IsInvalid_ReturnsEmpty()
        {
            using (var context = _factory.Create())
            {
                //Arrange
                var query = new Fixture().Create<EmployeeQuery>();
                var handler = new EmployeeQueryHandler(context, _mapper);
                var expected = new PagedResponse<EmployeeDto>(PageOptions.From(query), 0);

                //Acr
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<EmployeeDto>>.Instance);
            }
        }
    }
}
