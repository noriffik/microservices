﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Employees;
using NexCore.ContactManager.Infrastructure.QueryHandlers.DealerEmployees;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Testing;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.DealerEmployees
{
    public class EmployeeByIdQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new DealerEmployeeSeeder(context);

                //Seed
                await seeder.AddRange(3);
                var employee = await seeder.Add();
                await seeder.SeedAsync();

                //Map expected
                var individual = await context.Individuals.SingleAsync(c => c.Id == employee.IndividualId);
                var job = await context.Jobs.SingleAsync(j => j.Id == employee.JobId);
                var expected = _mapper.Map<EmployeeDto>(new EmployeeRecord
                {
                    Id = employee.Id,
                    DealerId = employee.DealerId,
                    Individual = individual,
                    Job = job
                });

                var query = new EmployeeByIdQuery
                {
                    DistributorId = employee.DealerId.DistributorId.Value,
                    DealerCode = employee.DealerId.DealerCode.Value,
                    EmployeeId = employee.Id
                };
                var handler = new EmployeeQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<EmployeeDto>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenEmployee_IsNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new DealerEmployeeSeeder(context);

                //Seed
                await seeder.AddRange(3);
                await seeder.SeedAsync();

                var query = new EmployeeByIdQuery
                {
                    DistributorId = seeder.Create<DistributorId>().Value,
                    DealerCode = seeder.Create<DealerCode>().Value,
                    EmployeeId = seeder.Create<int>()
                };
                var handler = new EmployeeQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(actual);
            }
        }
    }
}
