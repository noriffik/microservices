﻿using AutoFixture;
using AutoMapper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Employees;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Jobs.Queries;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Infrastructure.QueryHandlers.DealerEmployees;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Legal;
using NexCore.Testing;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.DealerEmployees
{
    public class AutoMapperProfileTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly IFixture _fixture = new Fixture().Customize(new DealerIdCustomization());

        [Fact]
        public void Map_FromEmployeeRecord_ToEmployeeDto()
        {
            //Arrange
            var record = CreateDealerEmployeeRecord();
            var expected = MapExpected(record);

            //Act
            var actual = _mapper.Map<EmployeeDto>(record);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<EmployeeDto>.Instance);
        }

        private EmployeeRecord CreateDealerEmployeeRecord()
        {
            var job = _fixture.Create<Job>();
            var individual = _fixture.Create<Individual>();

            var telephones = _fixture.CreateMany<Telephone>(3);
            foreach (var telephone in telephones)
                individual.Contact.Add(telephone);

            var emails = _fixture.CreateMany<EmailAddress>(3);
            foreach (var email in emails)
                individual.Contact.Add(email);

            var addresses = _fixture.CreateMany<PhysicalAddress>(3);
            foreach (var address in addresses)
                individual.Contact.Add(address);

            return new EmployeeRecord
            {
                Id = _fixture.Create<int>(),
                DealerId = _fixture.Create<DealerId>(),
                Job = job,
                Individual = individual
            };
        }

        private static EmployeeDto MapExpected(EmployeeRecord record)
        {
            var emails = record.Individual.Contact.EmailAddresses.Select(e => new EmailAddressDto
            {
                IsPrimary = e.IsPrimary,
                Description = e.Description,
                Address = e.Value.Address
            });

            var telephones = record.Individual.Contact.Telephones.Select(t => new TelephoneDto
            {
                Number = new TelephoneNumberDto
                {
                    AreaCode = t.Value.AreaCode,
                    CountryCode = t.Value.CountryCode,
                    LocalNumber = t.Value.LocalNumber
                },
                Description = t.Description,
                IsPrimary = t.IsPrimary,
                Type = t.Value.Type
            });

            var address = record.Individual.Contact.PhysicalAddresses.Select(a => new PhysicalAddressDto
            {
                City = new PhysicalAddressElementDto
                {
                    Code = a.Value.City.Code,
                    Name = a.Value.City.Value
                },
                Country = new PhysicalAddressElementDto
                {
                    Code = a.Value.Country.Code,
                    Name = a.Value.Country.Value
                },
                Description = a.Description,
                Geolocation = new GeolocationDto
                {
                    Latitude = a.Value.Geolocation.Latitude,
                    Longitude = a.Value.Geolocation.Longitude
                },
                IsPrimary = a.IsPrimary,
                PostCode = a.Value.PostCode.Value,
                Region = new PhysicalAddressElementDto
                {
                    Code = a.Value.Region.Code,
                    Name = a.Value.Region.Value
                },
                Street = a.Value.Street.Value
            });

            var individual = new IndividualDto
            {
                Id = record.Individual.Id,
                Passport = record.Individual.Passport == Passport.Empty
                   ? null
                   : new PassportDto
                   {
                       Series = record.Individual.Passport.Series,
                       Number = record.Individual.Passport.Number,
                       Issuer = record.Individual.Passport.Issue.Issuer,
                       IssuedOn = record.Individual.Passport.Issue.IssuedOn.GetValueOrDefault()
                   },
                Name = new PersonNameDto
                {
                    Firstname = record.Individual.Name.Firstname,
                    Lastname = record.Individual.Name.Lastname,
                    Middlename = record.Individual.Name.Middlename,
                },
                Contact = new ContactDto
                {
                    Telephones = telephones,
                    Addresses = address,
                    Emails = emails
                },
                Gender = record.Individual.Gender,
                BirthDay = record.Individual.Birthday,
                TaxNumber = record.Individual.TaxNumber == TaxIdentificationNumber.Empty
                    ? null
                    : new TaxIdentificationNumberDto
                    {
                        Issue = new IssueDto
                        {
                            Issuer = record.Individual.TaxNumber.Issue.Issuer,
                            IssuedOn = record.Individual.TaxNumber.Issue.IssuedOn.GetValueOrDefault()
                        },
                        Number = record.Individual.TaxNumber.Number,
                        RegisteredOn = record.Individual.TaxNumber.RegisteredOn
                    },
                Children = record.Individual.Children,
                FamilyStatus = record.Individual.FamilyStatus?.Id,
                Preferences = new PersonalPreferencesDto
                {
                    Language = record.Individual.Preferences.Language,
                    Communications = new CommunicationPreferencesDto
                    {
                        PreferredMedium = record.Individual.Preferences.Communications.PreferredMedium != null ? record.Individual.Preferences.Communications.PreferredMedium.Id : null,
                        BannedMediums = record.Individual.Preferences.Communications.BannedMediums.Values.Select(i => i.Id)
                    }
                }
            };

            var job = new JobDto
            {
                Id = record.Job.Id,
                Title = record.Job.Title,
            };

            return new EmployeeDto
            {
                Id = record.Id,
                DealerId = record.DealerId,
                Individual = individual,
                Job = job
            };
        }
    }
}
