﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Distributors.Queries;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Organizations.Dtos;
using NexCore.ContactManager.Application.Organizations.Queries;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.Legal;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Distributors
{
    public class DistributorsQueryFixture : QueryFixture<DistributorsQuery, PagedResponse<DistributorDto>>
    {
        private IEnumerable<Distributor> _distributors;
        private IEnumerable<Organization> _organizations;

        private const int PageNumber = 2;
        private const int PageSize = 5;

        public override async Task Setup(ContactContext context)
        {
            await base.Setup(context);

            var seeder = new DistributorSeeder(context);

            //Seed Distributors
            _distributors = await seeder.AddRange(15);
            _organizations = seeder.Get<Organization>();

            await seeder.SeedAsync();
        }

        public override DistributorsQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new DistributorsQuery()
            {
                PageNumber = PageNumber,
                PageSize = PageSize
            };
        }

        public override DistributorsQuery GetUnpreparedQuery()
        {
            return new DistributorsQuery();
        }

        public override PagedResponse<DistributorDto> GetResponse()
        {
            var items = _distributors
                .OrderBy(o => o.Id)
                .Skip((PageNumber - 1) * PageSize)
                .Take(PageSize)
                .Select(distributor =>
                {
                    var organization = _organizations.Single(c => c.Id == distributor.OrganizationId);

                    var emails = organization.Contact.EmailAddresses.Select(e => new EmailAddressDto
                    {
                        IsPrimary = e.IsPrimary,
                        Description = e.Description,
                        Address = e.Value.Address
                    });

                    var telephones = organization.Contact.Telephones.Select(t => new TelephoneDto
                    {
                        Number = new TelephoneNumberDto
                        {
                            AreaCode = t.Value.AreaCode,
                            CountryCode = t.Value.CountryCode,
                            LocalNumber = t.Value.LocalNumber
                        },
                        Description = t.Description,
                        IsPrimary = t.IsPrimary,
                        Type = t.Value.Type
                    });

                    var address = organization.Contact.PhysicalAddresses.Select(a => new PhysicalAddressDto
                    {
                        City = new PhysicalAddressElementDto
                        {
                            Code = a.Value.City.Code,
                            Name = a.Value.City.Value
                        },
                        Country = new PhysicalAddressElementDto
                        {
                            Code = a.Value.Country.Code,
                            Name = a.Value.Country.Value
                        },
                        Description = a.Description,
                        Geolocation = new GeolocationDto
                        {
                            Latitude = a.Value.Geolocation.Latitude,
                            Longitude = a.Value.Geolocation.Longitude
                        },
                        IsPrimary = a.IsPrimary,
                        PostCode = a.Value.PostCode.Value,
                        Region = new PhysicalAddressElementDto
                        {
                            Code = a.Value.Region.Code,
                            Name = a.Value.Region.Value
                        },
                        Street = a.Value.Street.Value
                    });

                    return new DistributorDto
                    {
                        Id = distributor.Id,
                        Organization = new OrganizationDto
                        {
                            Id = organization.Id,
                            Name = organization.Name,
                            Identity = new IdentityDto
                            {
                                Edrpou = organization.Identity.Edrpou,
                                Ipn = organization.Identity.Ipn,
                            },
                            Contact = new ContactDto
                            {
                                Addresses = address,
                                Emails = emails,
                                Telephones = telephones
                            },
                            Requisites = organization.Requisites == OrganizationRequisites.Empty ? null : new OrganizationRequisitesDto
                            {
                                Telephones = organization.Requisites.Telephones,
                                Address = organization.Requisites.Address,
                                BankRequisites = organization.Requisites.BankRequisites,
                                FullName = organization.Requisites.Name.FullName,
                                ShortName = organization.Requisites.Name.ShortName,
                                Director = new PersonNameDto
                                {
                                    Firstname = organization.Requisites.Director.Firstname,
                                    Lastname = organization.Requisites.Director.Lastname,
                                    Middlename = organization.Requisites.Director.Middlename
                                }
                            }
                        },
                        CountryCode = distributor.CountryCode
                    };
                })
                .ToArray();

            return new PagedResponse<DistributorDto>(
                new PageOptions(PageNumber, PageSize), _distributors.Count(), items);
        }
    }
}
