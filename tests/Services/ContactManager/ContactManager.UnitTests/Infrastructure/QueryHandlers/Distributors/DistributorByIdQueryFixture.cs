﻿using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Distributors.Queries;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Organizations.Dtos;
using NexCore.ContactManager.Application.Organizations.Queries;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Legal;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Distributors
{
    public class DistributorByIdQueryFixture : QueryFixture<DistributorByIdQuery, DistributorDto>
    {
        private Distributor _distributor;
        private Organization _organization;

        public override async Task Setup(ContactContext context)
        {
            await base.Setup(context);

            var seeder = new DistributorSeeder(context);

            //Seed not expected Distributors
            await seeder.AddRange(3);

            //Seed expected Distributor
            _distributor = await seeder.Add();
            _organization = seeder.Find<Organization>(_distributor.OrganizationId);

            await seeder.SeedAsync();
        }

        public override DistributorByIdQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new DistributorByIdQuery { Id = _distributor.Id.Value };
        }

        public override DistributorByIdQuery GetUnpreparedQuery()
        {
            return new DistributorByIdQuery { Id = DistributorId.Parse("XXX") };
        }

        public override DistributorDto GetResponse()
        {
            var emails = _organization.Contact.EmailAddresses.Select(e => new EmailAddressDto
            {
                IsPrimary = e.IsPrimary,
                Description = e.Description,
                Address = e.Value.Address
            });

            var telephones = _organization.Contact.Telephones.Select(t => new TelephoneDto
            {
                Number = new TelephoneNumberDto
                {
                    AreaCode = t.Value.AreaCode,
                    CountryCode = t.Value.CountryCode,
                    LocalNumber = t.Value.LocalNumber
                },
                Description = t.Description,
                IsPrimary = t.IsPrimary,
                Type = t.Value.Type
            });

            var address = _organization.Contact.PhysicalAddresses.Select(a => new PhysicalAddressDto
            {
                City = new PhysicalAddressElementDto
                {
                    Code = a.Value.City.Code,
                    Name = a.Value.City.Value
                },
                Country = new PhysicalAddressElementDto
                {
                    Code = a.Value.Country.Code,
                    Name = a.Value.Country.Value
                },
                Description = a.Description,
                Geolocation = new GeolocationDto
                {
                    Latitude = a.Value.Geolocation.Latitude,
                    Longitude = a.Value.Geolocation.Longitude
                },
                IsPrimary = a.IsPrimary,
                PostCode = a.Value.PostCode.Value,
                Region = new PhysicalAddressElementDto
                {
                    Code = a.Value.Region.Code,
                    Name = a.Value.Region.Value
                },
                Street = a.Value.Street.Value
            });

            return new DistributorDto
            {
                Id = _distributor.Id,
                Organization = new OrganizationDto
                {
                    Id = _organization.Id,
                    Name = _organization.Name,
                    Identity = new IdentityDto
                    {
                        Edrpou = _organization.Identity.Edrpou,
                        Ipn = _organization.Identity.Ipn,
                    },
                    Contact = new ContactDto
                    {
                        Addresses = address,
                        Emails = emails,
                        Telephones = telephones
                    },
                    Requisites = _organization.Requisites == OrganizationRequisites.Empty ? null : new OrganizationRequisitesDto
                    {
                        Telephones = _organization.Requisites.Telephones,
                        Address = _organization.Requisites.Address,
                        BankRequisites = _organization.Requisites.BankRequisites,
                        FullName = _organization.Requisites.Name.FullName,
                        ShortName = _organization.Requisites.Name.ShortName,
                        Director = new PersonNameDto
                        {
                            Firstname = _organization.Requisites.Director.Firstname,
                            Lastname = _organization.Requisites.Director.Lastname,
                            Middlename = _organization.Requisites.Director.Middlename
                        }
                    }
                },
                CountryCode = _distributor.CountryCode
            };
        }
    }
}
