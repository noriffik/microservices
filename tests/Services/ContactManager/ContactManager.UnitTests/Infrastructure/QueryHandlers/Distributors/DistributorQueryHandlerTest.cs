﻿using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Distributors;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.Testing;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Distributors
{
    public class DistributorQueryHandlerTest
    {
        private readonly DistributorByIdQueryFixture _byIdQueryFixture;
        private readonly DistributorsQueryFixture _allQueryFixture;

        public DistributorQueryHandlerTest()
        {
            _byIdQueryFixture = new DistributorByIdQueryFixture();
            _allQueryFixture = new DistributorsQueryFixture();
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new DistributorQueryHandler(null, _byIdQueryFixture.Mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = InMemoryDbContextFactory.Instance.Create())
                {
                    return new DistributorQueryHandler(context, null);
                }
            });
        }

        //DistributorByIdQuery

        [Fact]
        public async Task HandleById()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                await _byIdQueryFixture.Setup(context);

                var handler = new DistributorQueryHandler(context, _byIdQueryFixture.Mapper);

                //Act
                var actual = await handler.Handle(_byIdQueryFixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(_byIdQueryFixture.GetResponse(), actual, PropertyComparer<DistributorDto>.Instance);
            }
        }

        [Fact]
        public Task HandleById_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new DistributorQueryHandler(context, _byIdQueryFixture.Mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as DistributorByIdQuery, CancellationToken.None));
            }
        }

        [Fact]
        public async Task HandleById_WhenNothing_IsFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new DistributorQueryHandler(context, _byIdQueryFixture.Mapper);

                //Act
                var result = await handler.Handle(_byIdQueryFixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }

        //DistributorsQuery

        [Fact]
        public async Task HandleAll()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                await _allQueryFixture.Setup(context);

                var handler = new DistributorQueryHandler(context, _allQueryFixture.Mapper);

                //Act
                var actual = await handler.Handle(_allQueryFixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(_allQueryFixture.GetResponse(), actual, PropertyComparer<PagedResponse<DistributorDto>>.Instance);
            }
        }

        [Fact]
        public Task HandleAll_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new DistributorQueryHandler(context, _allQueryFixture.Mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as DistributorsQuery, CancellationToken.None));
            }
        }

        [Fact]
        public async Task HandleAll_WhenNothing_IsFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new DistributorQueryHandler(context, _allQueryFixture.Mapper);

                //Act
                var result = await handler.Handle(_allQueryFixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Empty(result.Items);
            }
        }
    }
}
