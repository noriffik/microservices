﻿using AutoFixture;
using AutoMapper;
using Moq;
using NexCore.ContactManager.Application.Languages.Queries;
using NexCore.ContactManager.Application.Providers;
using NexCore.ContactManager.Domain.Languages;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Languages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Languages
{
    public class LanguageQueryHandlerTest
    {
        private readonly LanguageQueryHandler _handler;

        private readonly Mock<ILanguageProvider> _languageProvider;
        private readonly Mock<IMapper> _mapper;

        public LanguageQueryHandlerTest()
        {
            _languageProvider = new Mock<ILanguageProvider>();
            _mapper = new Mock<IMapper>();

            _handler = new LanguageQueryHandler(_languageProvider.Object, _mapper.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(LanguageQueryHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            var fixture = new Fixture();
            var languages = fixture.CreateMany<Language>();
            var expected = fixture.CreateMany<LanguageDto>().ToList();

            _languageProvider.Setup(p => p.Get())
                .ReturnsAsync(languages);

            _mapper.Setup(m => m.Map<IEnumerable<LanguageDto>>(languages))
                .Returns(expected);

            //Act
            var actual = await _handler.Handle(new LanguageQuery(), CancellationToken.None);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public Task Handle_GivenQuery_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>(() => _handler.Handle(null, CancellationToken.None));
        }
    }
}
