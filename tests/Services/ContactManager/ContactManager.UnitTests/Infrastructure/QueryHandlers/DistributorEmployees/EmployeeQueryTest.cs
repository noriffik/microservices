﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries.Employees;
using NexCore.ContactManager.Infrastructure.QueryHandlers.DistributorEmployees;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Testing;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.DistributorEmployees
{
    public class EmployeeQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new DistributorEmployeeSeeder(context);
                const int pageNumber = 3;
                const int pageSize = 2;

                await seeder.AddRange(5);

                var distributorId = seeder.Create<DistributorId>();
                var organizationId = seeder.Create<int>();

                var employees = await seeder.AddRange(6, new
                {
                    distributorId,
                    organizationId
                });

                await seeder.SeedAsync();

                var individuals = await context.Individuals.ToDictionaryAsync(i => i.Id, i => i);
                var jobs = await context.Jobs.ToDictionaryAsync(j => j.Id, j => j);

                var pagedEmployeeRecords = employees
                    .Select(e => new EmployeeRecord
                    {
                        Id = e.Id,
                        DistributorId = e.DistributorId,
                        Individual = individuals[e.IndividualId],
                        Job = jobs[e.JobId]
                    })
                    .OrderBy(r => r.Individual.Name.Lastname)
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();


                var items = _mapper.Map<IEnumerable<EmployeeDto>>(pagedEmployeeRecords).ToArray();
                var expected = new PagedResponse<EmployeeDto>(
                    new PageOptions(pageNumber, pageSize), employees.Length, items);

                var query = new EmployeeQuery
                {
                    DistributorId = distributorId.Value,
                    PageNumber = pageNumber,
                    PageSize = pageSize,
                };

                var handler = new EmployeeQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<EmployeeDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenEmployees_AreNotFound_ReturnsEmpty()
        {
            //Arrange
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                var seeder = new DealerEmployeeSeeder(context);

                var query = new EmployeeQuery
                {
                    DistributorId = seeder.Create<DistributorId>().Value
                };
                var handler = new EmployeeQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(actual.Items);
            }
        }
    }
}
