﻿using AutoMapper;
using NexCore.ContactManager.Application.FamilyStatuses.Dtos;
using NexCore.ContactManager.Application.FamilyStatuses.Queries;
using NexCore.ContactManager.Infrastructure.QueryHandlers.FamilyStatuses;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.Legal;
using NexCore.Testing;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.FamilyStatuses
{
    public class FamilyStatusQueryHandlerTest
    {
        private readonly FamilyStatusQueryHandler _handler;
        private readonly IMapper _mapper;

        public FamilyStatusQueryHandlerTest()
        {
            _mapper = AutoMapperFactory.Create();
            _handler = new FamilyStatusQueryHandler(_mapper);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(FamilyStatusQueryHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            var query = new FamilyStatusQuery();
            var expected = _mapper.Map<IEnumerable<FamilyStatusDto>>(FamilyStatus.All);

            //Act
            var actual = await _handler.Handle(query, CancellationToken.None);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<IEnumerable<FamilyStatusDto>>.Instance);
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _handler.Handle(null, CancellationToken.None));
        }
    }
}
