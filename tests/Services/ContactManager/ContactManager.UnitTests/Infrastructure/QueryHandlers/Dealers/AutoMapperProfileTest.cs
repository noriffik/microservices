﻿using AutoFixture;
using AutoMapper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Organizations.Dtos;
using NexCore.ContactManager.Application.Organizations.Queries;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Dealers;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.Legal;
using NexCore.Testing;
using NexCore.Testing.Extensions;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Dealers
{
    public class AutoMapperProfileTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly IFixture _fixture = new Fixture().Customize(new DealerIdCustomization());

        [Fact]
        public void Map_FromDealerRecord_ToDealerDto()
        {
            //Arrange
            var record = CreateDealerRecord();
            var expected = MapExpected(record);

            //Act
            var actual = _mapper.Map<DealerDto>(record);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<DealerDto>.Instance);
        }

        private DealerRecord CreateDealerRecord()
        {
            var dealer = _fixture.Create<Dealer>();
            var organization = _fixture.Construct<Organization>(dealer.OrganizationId);

            var telephones = _fixture.CreateMany<Telephone>(3);
            foreach (var telephone in telephones)
                organization.Contact.Add(telephone);

            var emails = _fixture.CreateMany<EmailAddress>(3);
            foreach (var email in emails)
                organization.Contact.Add(email);

            var addresses = _fixture.CreateMany<PhysicalAddress>(3);
            foreach (var address in addresses)
                organization.Contact.Add(address);

            return new DealerRecord
            {
                Dealer = dealer,
                Organization = organization
            };
        }

        private static DealerDto MapExpected(DealerRecord record)
        {
            var emails = record.Organization.Contact.EmailAddresses.Select(e => new EmailAddressDto
            {
                IsPrimary = e.IsPrimary,
                Description = e.Description,
                Address = e.Value.Address
            });

            var telephones = record.Organization.Contact.Telephones.Select(t => new TelephoneDto
            {
                Number = new TelephoneNumberDto
                {
                    AreaCode = t.Value.AreaCode,
                    CountryCode = t.Value.CountryCode,
                    LocalNumber = t.Value.LocalNumber
                },
                Description = t.Description,
                IsPrimary = t.IsPrimary,
                Type = t.Value.Type
            });

            var address = record.Organization.Contact.PhysicalAddresses.Select(a => new PhysicalAddressDto
            {
                City = new PhysicalAddressElementDto
                {
                    Code = a.Value.City.Code,
                    Name = a.Value.City.Value
                },
                Country = new PhysicalAddressElementDto
                {
                    Code = a.Value.Country.Code,
                    Name = a.Value.Country.Value
                },
                Description = a.Description,
                Geolocation = new GeolocationDto
                {
                    Latitude = a.Value.Geolocation.Latitude,
                    Longitude = a.Value.Geolocation.Longitude
                },
                IsPrimary = a.IsPrimary,
                PostCode = a.Value.PostCode.Value,
                Region = new PhysicalAddressElementDto
                {
                    Code = a.Value.Region.Code,
                    Name = a.Value.Region.Value
                },
                Street = a.Value.Street.Value
            });

            var organization = new OrganizationDto
            {
                Id = record.Organization.Id,
                Name = record.Organization.Name,
                Identity = new IdentityDto
                {
                    Edrpou = record.Organization.Identity.Edrpou,
                    Ipn = record.Organization.Identity.Ipn,
                },
                Contact = new ContactDto
                {
                    Addresses = address,
                    Emails = emails,
                    Telephones = telephones
                },
                Requisites = record.Organization.Requisites == OrganizationRequisites.Empty
                    ? null
                    : new OrganizationRequisitesDto
                    {
                        Telephones = record.Organization.Requisites.Telephones,
                        Address = record.Organization.Requisites.Address,
                        BankRequisites = record.Organization.Requisites.BankRequisites,
                        FullName = record.Organization.Requisites.Name.FullName,
                        ShortName = record.Organization.Requisites.Name.ShortName,
                        Director = new PersonNameDto
                        {
                            Firstname = record.Organization.Requisites.Director.Firstname,
                            Lastname = record.Organization.Requisites.Director.Lastname,
                            Middlename = record.Organization.Requisites.Director.Middlename
                        }
                    }
            };

            return new DealerDto
            {
                Id = record.Dealer.Id.Value,
                DistributorId = record.Dealer.DistributorId.Value,
                DealerCode = record.Dealer.DealerCode.Value,
                CityCode = record.Dealer.CityCode,
                Organization = organization
            };
        }
    }
}
