﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Dealers;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Testing;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Dealers
{
    public class DealerByCodeQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new DealerSeeder(context);

                //Seed
                await seeder.AddRange(3);
                var dealer = await seeder.Add();
                await seeder.SeedAsync();

                //Map expected
                var organization = await context.Organizations.SingleAsync(c => c.Id == dealer.OrganizationId);
                var expected = _mapper.Map<DealerDto>(new DealerRecord
                {
                    Dealer = dealer,
                    Organization = organization
                });

                //Query and handler
                var query = new DealerByCodeQuery
                {
                    DistributorId = dealer.DistributorId.Value,
                    DealerCode = dealer.DealerCode.Value
                };
                var handler = new DealerQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<DealerDto>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new DistributorSeeder(context);

                //Seed
                await seeder.AddRange(3);
                await seeder.SeedAsync();

                //Query and handler
                var query = new DealerByCodeQuery
                {
                    DistributorId = seeder.Create<DistributorId>().Value,
                    DealerCode = seeder.Create<DealerCode>().Value,
                };
                var handler = new DealerQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }
    }
}
