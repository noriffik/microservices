﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Dealers;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Testing;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Dealers
{
    public class DealerQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        public static IEnumerable<object[]> IdentityFilterData =>
            new List<object[]>
            {
                new object[] { Identity.WithEdrpou("Edrpou")},
                new object[] { Identity.WithIpn("Ipn")},
            };

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new DealerSeeder(context);
                const int pageNumber = 2;
                const int pageSize = 5;

                //Seed
                await seeder.AddRange(10);

                var distributorId = seeder.Create<DistributorId>();
                var dealers = await AddDealersByDistributorId(seeder, distributorId, 15);

                await seeder.SeedAsync();

                //Map expected
                var organizations = await context.Organizations.ToDictionaryAsync(o => o.Id, o => o);
                var pagedDealerRecords = dealers
                    .OrderBy(d => d.DealerCode)
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .Select(dealer => new DealerRecord
                    {
                        Dealer = dealer,
                        Organization = organizations[dealer.OrganizationId]
                    })
                    .ToList();

                var items = _mapper.Map<IEnumerable<DealerDto>>(pagedDealerRecords).ToArray();
                var expected = new PagedResponse<DealerDto>(new PageOptions(pageNumber, pageSize), 15, items);

                //Query and handler
                var query = new DealerQuery
                {
                    DistributorId = distributorId,
                    PageNumber = pageNumber,
                    PageSize = pageSize
                };
                var handler = new DealerQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<DealerDto>>.Instance);
            }
        }

        private static async Task<IReadOnlyCollection<Dealer>> AddDealersByDistributorId(DealerSeeder seeder,
            DistributorId distributorId, int count)
        {
            var dealerIds = Enumerable.Range(0, count)
                .Select(_ => DealerId.Next(distributorId, seeder.Create<DealerCode>()))
                .ToList();

            var dealers = new List<Dealer>();
            foreach (var dealerId in dealerIds)
                dealers.Add(await seeder.Add(dealerId, new { }));

            return dealers;
        }

        [Theory]
        [MemberData(nameof(IdentityFilterData))]
        public async Task Handle_WithIdentity(Identity identity)
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new DealerSeeder(context);

                //Seed expected
                var organization = await seeder.Add<Organization>(new { identity });
                var dealer = await seeder.Add(new { organizationId = organization.Id });

                //Seed unexpected
                await seeder.AddRange(10);
                await AddDealersByDistributorId(seeder, dealer.DistributorId, 15);

                await seeder.SeedAsync();

                //Query and handler
                var query = new DealerQuery
                {
                    DistributorId = dealer.DistributorId.Value,
                    Edrpou = identity.Edrpou,
                    Ipn = identity.Ipn,
                    PageNumber = 1,
                    PageSize = 5
                };
                var handler = new DealerQueryHandler(context, _mapper);

                //Map expected
                var dealerDto = _mapper.Map<DealerDto>(new DealerRecord
                {
                    Dealer = dealer,
                    Organization = organization
                });
                var expected = new PagedResponse<DealerDto>(PageOptions.From(query), 1, dealerDto);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<DealerDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new DistributorSeeder(context);

                //Seed unexpected Dealer
                await seeder.AddRange(5);
                await seeder.SeedAsync();

                //Query and handler
                var query = new DealerQuery
                {
                    DistributorId = seeder.Create<DistributorId>().Value,
                };
                var handler = new DealerQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(result.Items);
            }
        }

    }
}
