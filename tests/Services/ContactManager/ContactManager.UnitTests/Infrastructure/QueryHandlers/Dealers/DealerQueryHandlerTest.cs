﻿using AutoMapper;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Dealers;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Dealers
{
    public class DealerQueryHandlerTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly InMemoryDbContextFactory _factory = new InMemoryDbContextFactory(typeof(DealerQueryHandlerTest));

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new DealerQueryHandler(null, _mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = _factory.Create())
                {
                    var unused = new DealerQueryHandler(context, null);
                }
            });
        }

        [Fact]
        public async Task Handle_FindByDealerCode_GivenRequest_IsNull_Throws()
        {
            using (var context = _factory.Create())
            {
                var handler = new DealerQueryHandler(context, _mapper);

                await Assert.RequestHandler(handler).ThrowsOnNull(typeof(DealerByCodeQuery));
            }
        }

        [Fact]
        public async Task Handle_FindByDistributorId_GivenRequest_IsNull_Throws()
        {
            using (var context = _factory.Create())
            {
                var handler = new DealerQueryHandler(context, _mapper);

                await Assert.RequestHandler(handler).ThrowsOnNull(typeof(DealerQuery));
            }
        }
    }
}
