﻿using AutoFixture;
using AutoMapper;
using NexCore.ContactManager.Application.Jobs.Queries;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.UnitTests.Fixtures;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Jobs
{
    public class JobDtoMapperTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly Fixture _fixture = new Fixture();
        
        [Fact]
        public void Map()
        {
            //Arrange
            var job = _fixture.Create<Job>();

            //Act
            var model = _mapper.Map<JobDto>(job);

            //Assert
            Assert.NotNull(model);
            Assert.Equal(job.Id, model.Id);
            Assert.Equal(job.Title, model.Title);
        }
    }
}
