﻿using AutoMapper;
using NexCore.ContactManager.Application.Jobs.Queries;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Jobs;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.Testing;
using NexCore.Testing.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Jobs
{
    public class JobQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new EntitySeeder<Job>(context);

                var jobs = (await seeder.AddRange(5)).OrderBy(j => j.Title);

                await seeder.SeedAsync();

                var query = new JobQuery();
                var handler = new JobQueryHandler(context, _mapper);
                var expected = _mapper.Map<IEnumerable<JobDto>>(jobs);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<JobDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new JobQuery();
                var handler = new JobQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(result);
            }
        }
    }
}
