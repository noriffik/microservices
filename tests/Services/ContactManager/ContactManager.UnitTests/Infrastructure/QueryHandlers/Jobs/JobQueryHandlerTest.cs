﻿using AutoMapper;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Jobs;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Jobs
{
    public class JobQueryHandlerTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly InMemoryDbContextFactory _factory = new InMemoryDbContextFactory(typeof(JobQueryHandlerTest));
        
        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () =>
            {
                var unused = new JobQueryHandler(null, _mapper);
            });
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = _factory.Create())
                {
                    var unused = new JobQueryHandler(context, null);
                }
            });
        }
        
        [Fact]
        public async Task Handle_GivenRequest_IsNull_Throws()
        {
            using (var context = _factory.Create())
            {
                var handler = new JobQueryHandler(context, _mapper);

                await Assert.RequestHandler(handler).ThrowsOnNull();
            }
        }
    }
}
