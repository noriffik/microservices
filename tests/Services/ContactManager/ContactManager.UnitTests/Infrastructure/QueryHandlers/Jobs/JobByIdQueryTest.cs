﻿using AutoMapper;
using NexCore.ContactManager.Application.Jobs.Queries;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Jobs;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.Testing;
using NexCore.Testing.Seeders;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Jobs
{
    public class JobByIdQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new EntitySeeder<Job>(context);
                await seeder.AddRange(3);

                var job = await seeder.Add(365867);

                await seeder.SeedAsync();

                var query = new JobByIdQuery {Id = job.Id};
                var handler = new JobByIdQueryHandler(context, _mapper);
                var expected = _mapper.Map<JobDto>(job);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<JobDto>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new EntitySeeder<Job>(context);
                await seeder.AddRange(3);

                await seeder.SeedAsync();

                var query = new JobByIdQuery {Id = 2193};
                var handler = new JobByIdQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }
    }
}
