﻿using AutoMapper;
using NexCore.ContactManager.Application.Leads.Queries.Topics;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Topics;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.Testing;
using NexCore.Testing.Seeders;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Topics
{
    public class TopicByIdQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new EntitySeeder<Topic>(context))
            {
                //Arrange
                //Seed
                await seeder.AddRange(3);
                var topic = await seeder.Add();
                await seeder.SeedAsync();

                //Map expected
                var expected = new TopicDto
                {
                    Id = topic.Id,
                    Name = topic.Name
                };

                //Query and handler
                var query = new TopicByIdQuery
                {
                    Id = topic.Id
                };
                var handler = new TopicQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<TopicDto>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenTopic_IsNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new EntitySeeder<Topic>(context))
            {
                //Arrange
                //Seed unexpected
                await seeder.AddRange(3);
                await seeder.SeedAsync();

                //Query and handler
                var query = new TopicByIdQuery
                {
                    Id = seeder.Create<int>()
                };
                var handler = new TopicQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }
    }
}
