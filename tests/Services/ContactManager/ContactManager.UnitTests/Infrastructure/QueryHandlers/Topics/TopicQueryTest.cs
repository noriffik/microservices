﻿using AutoMapper;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Leads.Queries.Topics;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Topics;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.Testing;
using NexCore.Testing.Seeders;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Topics
{
    public class TopicQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new EntitySeeder<Topic>(context))
            {
                //Arrange
                //Seed
                var topics = await seeder.AddRange(10);
                await seeder.SeedAsync();

                //Query and handler
                var query = new TopicQuery
                {
                    PageNumber = 2,
                    PageSize = 3
                };
                var handler = new TopicQueryHandler(context, _mapper);

                //Map expected
                var items = topics.OrderBy(t => t.Name)
                    .Skip((query.PageNumber.Value - 1) * query.PageSize.Value)
                    .Take(query.PageSize.Value)
                    .Select(t => new TopicDto
                    {
                        Id = t.Id,
                        Name = t.Name
                    }).ToArray();
                var expected = new PagedResponse<TopicDto>(PageOptions.From(query), topics.Length, items);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<TopicDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_FilterByName()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new EntitySeeder<Topic>(context))
            {
                //Arrange
                //Seed
                await seeder.AddRange(5);

                var name = seeder.Create<string>();
                var topics = await seeder.AddRange(10);
                foreach (var topic in topics)
                    topic.ChangeName(seeder.Create<string>() + name + seeder.Create<string>());

                await seeder.SeedAsync();

                //Query and handler
                var query = new TopicQuery
                {
                    PageNumber = 2,
                    PageSize = 3,
                    Name = name
                };
                var handler = new TopicQueryHandler(context, _mapper);

                //Map expected
                var items = topics.OrderBy(t => t.Name)
                    .Skip((query.PageNumber.Value - 1) * query.PageSize.Value)
                    .Take(query.PageSize.Value)
                    .Select(t => new TopicDto
                    {
                        Id = t.Id,
                        Name = t.Name
                    }).ToArray();
                var expected = new PagedResponse<TopicDto>(PageOptions.From(query), topics.Length, items);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<TopicDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenTopic_IsNotFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new TopicQuery();
                var handler = new TopicQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(result.Items);
            }
        }
    }
}
