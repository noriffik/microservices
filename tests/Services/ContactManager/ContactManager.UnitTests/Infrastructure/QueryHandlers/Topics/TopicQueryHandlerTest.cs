﻿using AutoMapper;
using NexCore.ContactManager.Application.Leads.Queries.Topics;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Topics;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Topics
{
    public class TopicQueryHandlerTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly InMemoryDbContextFactory _factory = new InMemoryDbContextFactory(typeof(TopicQueryHandlerTest));

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new TopicQueryHandler(null, _mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                using (var context = _factory.Create())
                {
                    var unused = new TopicQueryHandler(context, null);
                }
            });
        }

        [Theory]
        [InlineData(typeof(TopicQuery))]
        [InlineData(typeof(TopicByIdQuery))]
        public async Task Handle_GivenRequest_IsNull_Throws(Type queryType)
        {
            using (var context = _factory.Create())
            {
                var handler = new TopicQueryHandler(context, _mapper);

                await Assert.RequestHandler(handler).ThrowsOnNull(queryType);
            }
        }
    }
}
