﻿using AutoMapper;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers.Corporate;
using NexCore.ContactManager.Application.Organizations.Queries;
using NexCore.ContactManager.Domain.DealerCorporateCustomers;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure.QueryHandlers.DealerCorporateCustomers;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders.Customers;
using NexCore.DealerNetwork;
using NexCore.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.DealersCorporateCustomers
{
    public class CorporateCustomerQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        private const int PageNumber = 1;
        private const int PageSize = 3;
        private const string Code = "code";

        [Fact]
        public async Task Handle()
        {
            //Arrange
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                var seeder = new DealerCorporateCustomerSeeder(context);

                //Seed expected
                var dealerId = seeder.Create<DealerId>();
                var expected = await SeedExpectation(seeder, dealerId, null);

                //Query and handler
                var query = new CorporateCustomerQuery
                {
                    DistributorId = dealerId.DistributorId.Value,
                    DealerCode = dealerId.DealerCode.Value,
                    PageNumber = PageNumber,
                    PageSize = PageSize
                };
                var handler = new CorporateCustomerQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<CorporateCustomerDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_FilteredByEdrpou()
        {
            //Arrange
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                var seeder = new DealerCorporateCustomerSeeder(context);
                var dealerId = seeder.Create<DealerId>();

                //Seed expected
                var expected = await SeedExpectation(seeder, dealerId, () => Identity.WithEdrpou(Code + seeder.Create<string>()));

                //Query and handler
                var query = new CorporateCustomerQuery
                {
                    DistributorId = dealerId.DistributorId.Value,
                    DealerCode = dealerId.DealerCode.Value,
                    Edrpou = Code,
                    PageNumber = PageNumber,
                    PageSize = PageSize
                };
                var handler = new CorporateCustomerQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<CorporateCustomerDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_FilteredByIpn()
        {
            //Arrange
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                var seeder = new DealerCorporateCustomerSeeder(context);
                var dealerId = seeder.Create<DealerId>();

                //Seed expected
                var expected = await SeedExpectation(seeder, dealerId, () => Identity.WithIpn(Code + seeder.Create<string>()));

                //Query and handler
                var query = new CorporateCustomerQuery
                {
                    DistributorId = dealerId.DistributorId.Value,
                    DealerCode = dealerId.DealerCode.Value,
                    Ipn = Code,
                    PageNumber = PageNumber,
                    PageSize = PageSize
                };

                var handler = new CorporateCustomerQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<CorporateCustomerDto>>.Instance);
            }
        }

        private async Task<PagedResponse<CorporateCustomerDto>> SeedExpectation(DealerCorporateCustomerSeeder seeder, DealerId dealerId, Func<Identity> identityFactory)
        {
            var customers = await seeder.AddRange(4, new {dealerId});
            var organizations = seeder.Get<Organization>().ToDictionary(i => i.Id);

            //Assign identity when given
            if (identityFactory != null)
                seeder.Get<Organization>()
                    .ToList()
                    .ForEach(o => o.Identity = identityFactory());

            //Seed unexpected
            await seeder.AddRange(5);
            await seeder.SeedAsync();

            //Map expected
            var items = MapItems(customers, organizations);

           return new PagedResponse<CorporateCustomerDto>(
                new PageOptions(PageNumber, PageSize), customers.Length, items);
        }

        private CorporateCustomerDto[] MapItems(IEnumerable<DealerCorporateCustomer> customers, IDictionary<int, Organization> organizations)
        {
            return customers
                .Select(c => new CorporateCustomerRecord
                {
                    Customer = c,
                    Organization = organizations[c.ContactableId]
                })
                .OrderBy(r => r.Organization.Name)
                .Skip((PageNumber - 1) * PageSize)
                .Take(PageSize)
                .Select(r => new CorporateCustomerDto
                {
                    Id = r.Customer.Id,
                    DealerId = r.Customer.DealerId.Value,
                    Organization = _mapper.Map<OrganizationDto>(r.Organization)
                }).ToArray();
        }
    }
}
