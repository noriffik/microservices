﻿using AutoFixture;
using AutoMapper;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers.Corporate;
using NexCore.ContactManager.Infrastructure.QueryHandlers.DealerCorporateCustomers;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.DealersCorporateCustomers
{
    public class CorporateCustomerQueryHandlerTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly InMemoryDbContextFactory _factory = new InMemoryDbContextFactory(typeof(CorporateCustomerQueryHandler));

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new CorporateCustomerQueryHandler(null, _mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = _factory.Create())
                {
                    var unused = new CorporateCustomerQueryHandler(context, null);
                }
            });
        }

        [Fact]
        public async Task Handle_FindById_GivenRequest_IsNull_Throws()
        {
            using (var context = _factory.Create())
            {
                var handler = new CorporateCustomerQueryHandler(context, _mapper);

                await Assert.RequestHandler(handler).ThrowsOnNull(typeof(CorporateCustomerByIdQuery));
            }
        }

        [Fact]
        public async Task Handle_FindId_GivenDealerId_IsInvalid_ReturnsNull()
        {
            using (var context = _factory.Create())
            {
                //Arrange
                var query = new Fixture().Create<CorporateCustomerByIdQuery>();
                var handler = new CorporateCustomerQueryHandler(context, _mapper);

                //Acr
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }
    }
}
