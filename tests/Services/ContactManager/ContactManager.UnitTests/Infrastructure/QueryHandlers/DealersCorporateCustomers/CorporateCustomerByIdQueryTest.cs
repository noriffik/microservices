﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers.Corporate;
using NexCore.ContactManager.Application.Organizations.Queries;
using NexCore.ContactManager.Infrastructure.QueryHandlers.DealerCorporateCustomers;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders.Customers;
using NexCore.DealerNetwork;
using NexCore.Testing;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.DealersCorporateCustomers
{
    public class CorporateCustomerByIdQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new DealerCorporateCustomerSeeder(context);

                await seeder.AddRange(4);
                var customer = await seeder.Add();
                await seeder.SeedAsync();

                var organization = await context.Organizations.SingleAsync(c => c.Id == customer.ContactableId);

                var expected = new CorporateCustomerDto
                {
                    Id = customer.Id,
                    DealerId = customer.DealerId.Value,
                    Organization = _mapper.Map<OrganizationDto>(organization)
                };

                var query = new CorporateCustomerByIdQuery
                {
                    DistributorId = customer.DealerId.DistributorId.Value,
                    DealerCode = customer.DealerId.DealerCode.Value,
                    CorporateCustomerId = customer.Id
                };
                var handler = new CorporateCustomerQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<CorporateCustomerDto>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenEmployee_IsNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new DealerCorporateCustomerSeeder(context);

                //Seed
                await seeder.AddRange(6);
                await seeder.SeedAsync();

                var query = new CorporateCustomerByIdQuery
                {
                    DistributorId = seeder.Create<DistributorId>().Value,
                    DealerCode = seeder.Create<DealerCode>().Value,
                    CorporateCustomerId = seeder.Create<int>()
                };
                var handler = new CorporateCustomerQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(actual);
            }
        }
    }
}
