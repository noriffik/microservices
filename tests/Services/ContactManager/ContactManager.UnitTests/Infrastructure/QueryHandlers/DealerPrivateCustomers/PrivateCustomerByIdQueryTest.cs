﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Infrastructure.QueryHandlers.DealerPrivateCustomers;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Testing;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.DealerPrivateCustomers
{
    public class PrivateCustomerByIdQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new DealerPrivateCustomerSeeder(context);

                //Seed
                await seeder.AddRange(3);
                var customer = await seeder.Add();
                await seeder.SeedAsync();

                //Map expected
                var individual = await context.Individuals.SingleAsync(c => c.Id == customer.ContactableId);

                var expected = new PrivateCustomerDto
                {
                    Id = customer.Id,
                    DealerId = customer.DealerId.Value,
                    Individual = _mapper.Map<IndividualDto>(individual)
                };

                var query = new PrivateCustomerByIdQuery
                {
                    DistributorId = customer.DealerId.DistributorId.Value,
                    DealerCode = customer.DealerId.DealerCode.Value,
                    PrivateCustomerId = customer.Id
                };
                var handler = new PrivateCustomerQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PrivateCustomerDto>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenEmployee_IsNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new DealerPrivateCustomerSeeder(context);

                //Seed
                await seeder.AddRange(3);
                await seeder.SeedAsync();

                var query = new PrivateCustomerByIdQuery
                {
                    DistributorId = seeder.Create<DistributorId>().Value,
                    DealerCode = seeder.Create<DealerCode>().Value,
                    PrivateCustomerId = seeder.Create<int>()
                };
                var handler = new PrivateCustomerQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(actual);
            }
        }
    }
}
