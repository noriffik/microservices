﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Customers;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Infrastructure.QueryHandlers.DealerPrivateCustomers;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Legal;
using NexCore.Testing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.DealerPrivateCustomers
{
    public class PrivateCustomerQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public async Task Handle()
        {
            //Arrange
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                var seeder = new DealerPrivateCustomerSeeder(context);
                const int pageNumber = 2;
                const int pageSize = 5;

                //Seed
                await seeder.AddRange(10);

                var dealerId = seeder.Create<DealerId>();
                var organizationId = seeder.Create<int>();
                var customers = await seeder.AddRange(10, new { dealerId, organizationId });

                await seeder.SeedAsync();

                //Map expected
                var individuals = await context.Individuals.ToDictionaryAsync(i => i.Id, i => i);
                var items = customers
                    .Select(c => new PrivateCustomerRecord
                    {
                        PrivateCustomer = c,
                        Individual = individuals[c.ContactableId]
                    })
                    .OrderBy(r => r.Individual.Name.Lastname)
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .Select(r => new PrivateCustomerDto
                    {
                        Id = r.PrivateCustomer.Id,
                        DealerId = r.PrivateCustomer.DealerId.Value,
                        Individual = _mapper.Map<IndividualDto>(r.Individual)
                    })
                    .ToArray();
                var expected = new PagedResponse<PrivateCustomerDto>(
                    new PageOptions(pageNumber, pageSize), customers.Length, items);

                //Query and handler
                var query = new PrivateCustomerQuery
                {
                    DistributorId = dealerId.DistributorId.Value,
                    DealerCode = dealerId.DealerCode.Value,
                    PageNumber = pageNumber,
                    PageSize = pageSize
                };
                var handler = new PrivateCustomerQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<PrivateCustomerDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_FilterByTelephone()
        {
            //Arrange
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                var seeder = new DealerPrivateCustomerSeeder(context);
                const int pageNumber = 1;
                const int pageSize = 5;

                var countryCode = seeder.Create<int>().ToString();
                var areaCode = seeder.Create<int>().ToString();
                var localNumber = seeder.Create<int>().ToString();

                //Seed expected
                var dealerId = seeder.Create<DealerId>();
                var organizationId = seeder.Create<int>();
                var customers = await seeder.AddRange(10, new { dealerId, organizationId });

                foreach (var customer in customers)
                {
                    var telephone = new Telephone(countryCode, areaCode, localNumber + seeder.Create<int>());
                    customer.UpdateTelephone(telephone);
                    seeder.Find<Individual>(customer.ContactableId).Contact.Add(telephone, seeder.Create<string>(), true);
                }

                //Seed unexpected
                await seeder.AddRange(10);
                await seeder.AddRange(5, new { dealerId, organizationId });

                await seeder.SeedAsync();

                //Map expected
                var individuals = await context.Individuals.ToDictionaryAsync(i => i.Id, i => i);
                var items = customers
                    .Select(c => new PrivateCustomerRecord
                    {
                        PrivateCustomer = c,
                        Individual = individuals[c.ContactableId]
                    })
                    .OrderBy(r => r.Individual.Name.Lastname)
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .Select(r => new PrivateCustomerDto
                    {
                        Id = r.PrivateCustomer.Id,
                        DealerId = r.PrivateCustomer.DealerId.Value,
                        Individual = _mapper.Map<IndividualDto>(r.Individual)
                    })
                    .ToArray();
                var expected = new PagedResponse<PrivateCustomerDto>(
                    new PageOptions(pageNumber, pageSize), customers.Length, items);

                //Query and handler
                var query = new PrivateCustomerQuery
                {
                    DistributorId = dealerId.DistributorId.Value,
                    DealerCode = dealerId.DealerCode.Value,
                    PageNumber = pageNumber,
                    PageSize = pageSize,
                    Telephone = $"{countryCode}{areaCode}{localNumber}"
                };
                var handler = new PrivateCustomerQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<PrivateCustomerDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_FilterByName()
        {
            //Arrange
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                var seeder = new DealerPrivateCustomerSeeder(context);
                const int pageNumber = 1;
                const int pageSize = 5;

                var name = seeder.Create<PersonName>();

                //Seed expected
                var dealerId = seeder.Create<DealerId>();
                var organizationId = seeder.Create<int>();
                var customers = await seeder.AddRange(10, new { dealerId, organizationId });

                foreach (var customer in customers)
                    seeder.Find<Individual>(customer.ContactableId).Name.Assign(name);

                //Seed unexpected
                await seeder.AddRange(10);
                await seeder.AddRange(5, new { dealerId, organizationId });

                await seeder.SeedAsync();

                //Map expected
                var individuals = await context.Individuals.ToDictionaryAsync(i => i.Id, i => i);
                var items = customers
                    .Select(c => new PrivateCustomerRecord
                    {
                        PrivateCustomer = c,
                        Individual = individuals[c.ContactableId]
                    })
                    .OrderBy(r => r.Individual.Name.Lastname)
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .Select(r => new PrivateCustomerDto
                    {
                        Id = r.PrivateCustomer.Id,
                        DealerId = r.PrivateCustomer.DealerId.Value,
                        Individual = _mapper.Map<IndividualDto>(r.Individual)
                    })
                    .ToArray();
                var expected = new PagedResponse<PrivateCustomerDto>(
                    new PageOptions(pageNumber, pageSize), customers.Length, items);

                //Query and handler
                var query = new PrivateCustomerQuery
                {
                    DistributorId = dealerId.DistributorId.Value,
                    DealerCode = dealerId.DealerCode.Value,
                    PageNumber = pageNumber,
                    PageSize = pageSize,
                    Name = $"{name.Firstname}    {name.Lastname} {name.Middlename}"
                };
                var handler = new PrivateCustomerQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<PrivateCustomerDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenCustomer_IsNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new DealerPrivateCustomerSeeder(context);

                //Seed
                await seeder.AddRange(5);
                await seeder.SeedAsync();

                var query = new PrivateCustomerQuery
                {
                    DistributorId = seeder.Create<DistributorId>().Value,
                    DealerCode = seeder.Create<DealerCode>().Value,
                };
                var handler = new PrivateCustomerQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(actual.Items);
            }
        }
    }
}
