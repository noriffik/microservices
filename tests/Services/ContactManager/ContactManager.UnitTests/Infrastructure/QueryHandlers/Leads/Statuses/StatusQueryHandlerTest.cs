﻿using AutoFixture;
using AutoMapper;
using Moq;
using NexCore.ContactManager.Application.Leads.Queries.Statuses;
using NexCore.ContactManager.Infrastructure.Providers.Leads;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Leads.Statuses;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Leads.Statuses
{
    public class StatusQueryHandlerTest
    {
        //Dependencies
        private readonly Mock<IStatusDescriptionProvider> _provider;
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        //Entities
        private readonly IEnumerable<StatusRecord> _records;

        //Query and handler
        private readonly StatusQuery _query;
        private readonly StatusQueryHandler _handler;

        public StatusQueryHandlerTest()
        {
            var fixture = new Fixture();

            //Entities
            _records = fixture.CreateMany<StatusRecord>(5);

            //Dependencies
            _provider = new Mock<IStatusDescriptionProvider>();
            _provider.Setup(p => p.Get()).Returns(_records);

            //Query and handler
            _query = fixture.Create<StatusQuery>();
            _handler = new StatusQueryHandler(_provider.Object, _mapper);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(StatusQueryHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            var expected = _records
                .OrderBy(r => r.Name)
                .Select(r => new StatusDto
                {
                    Id = r.Id,
                    Name = r.Name
                });

            //Act
            var actual = await _handler.Handle(_query, CancellationToken.None);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<StatusDto>.Instance);
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
