﻿using AutoFixture;
using AutoMapper;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Leads;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Leads;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Legal;
using NexCore.Testing;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Leads
{
    public class LeadQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private const int PageNumber = 2;
        private const int PageSize = 3;

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new DealerLeadSeeder(context))
            {
                //Arrange
                //Seed
                var dealerId = seeder.Create<DealerId>();
                var records = await Seed(seeder, dealerId, 10);
                await seeder.SeedAsync();

                //Query and handler
                var query = new LeadQuery
                {
                    DealerCode = dealerId.DealerCode.Value,
                    DistributorId = dealerId.DistributorId.Value,
                    PageNumber = PageNumber,
                    PageSize = PageSize,
                    SortCriterion = SortCriterion.IndividualContactFirstname
                };
                var handler = new LeadQueryHandler(context, _mapper);

                //MapExpected
                var expectedRecords = records
                    .OrderBy(r => r.Individual.Name.Firstname)
                    .Skip((PageNumber - 1) * PageSize)
                    .Take(PageSize);
                var items = _mapper.Map<LeadDto[]>(expectedRecords);
                var expected = new PagedResponse<LeadDto>(new PageOptions(PageNumber, PageSize), records.Count, items);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<LeadDto>>.Instance);
            }
        }

        private static async Task<IReadOnlyCollection<LeadRecord>> Seed(DealerLeadSeeder seeder, DealerId dealerId, int count)
        {
            var leads = await seeder.AddRange(count, new { dealerId });
            await seeder.AddRange(5);

            return GetLeadRecords(seeder, leads).ToList();
        }

        private static IEnumerable<LeadRecord> GetLeadRecords(DealerLeadSeeder seeder, IEnumerable<Lead> leads)
        {
            var individuals = seeder.Get<Individual>().ToDictionary(i => i.Id, i => i);
            var organizations = seeder.Get<Organization>().ToDictionary(o => o.Id, o => o);
            var owners = seeder.Get<DealerEmployee>()
                .ToDictionary(e => e.Id, e => new EmployeeOwnerRecord
                {
                    Employee = e,
                    Individual = individuals[e.IndividualId],
                    Organization = organizations[e.OrganizationId]
                });
            var topics = seeder.Get<Topic>().ToDictionary(t => t.Id, t => t);

            return leads.Select(lead => new LeadRecord
            {
                Lead = lead,
                Individual = individuals[lead.Contact.IndividualId],
                Organization = lead.Contact.OrganizationId.HasValue
                    ? organizations[lead.Contact.OrganizationId.Value]
                    : null,
                EmployeeOwner = lead.OwnerEmployeeId.HasValue
                    ? owners[lead.OwnerEmployeeId.Value]
                    : null,
                Topic = topics[lead.TopicId]
            });
        }

        [Theory]
        [ClassData(typeof(FilterTestData))]
        public async Task Handle_Filter(Action<LeadRecord> addFilterData, Action<LeadQuery> addFilter)
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new DealerLeadSeeder(context))
            {
                //Arrange
                //Seed
                var dealerId = seeder.Create<DealerId>();
                var records = (await Seed(seeder, dealerId, 15))
                    .Skip(5)
                    .ToList();
                records.ForEach(addFilterData);

                await seeder.SeedAsync();

                //Query and handler
                var query = new LeadQuery
                {
                    DealerCode = dealerId.DealerCode.Value,
                    DistributorId = dealerId.DistributorId.Value,
                    PageNumber = PageNumber,
                    PageSize = PageSize,
                };
                addFilter(query);
                var handler = new LeadQueryHandler(context, _mapper);

                //MapExpected
                var expectedRecords = records
                    .OrderBy(r => r.Individual.Name.Lastname)
                    .Skip((PageNumber - 1) * PageSize)
                    .Take(PageSize);
                var items = _mapper.Map<LeadDto[]>(expectedRecords);
                var expected = new PagedResponse<LeadDto>(new PageOptions(PageNumber, PageSize), records.Count, items);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<LeadDto>>.Instance);
            }
        }

        public class FilterTestData : IEnumerable<object[]>
        {
            private static readonly IFixture Fixture = new Fixture().Customize(new TelephoneNumberCustomization());
            private static readonly PersonName PersonName = Fixture.Create<PersonName>();
            private static readonly string OrganizationName = Fixture.Create<string>();
            private static readonly Telephone Telephone = new Telephone(Fixture.Create<int>().ToString(), Fixture.Create<int>().ToString(), Fixture.Create<int>().ToString());

            private static IEnumerable<object[]> Data => new[]
                {
                    new object[] {FilterByIndividualNameChangeRecord, FilterByIndividualNameChangeQuery},
                    new object[] {FilterByOrganizationNameChangeRecord, FilterByOrganizationNameChangeQuery},
                    new object[] {FilterByStatusChangeRecord, FilterByStatusChangeQuery},
                    new object[] {FilterByIndividualTelephoneChangeRecord, FilterByIndividualTelephoneChangeQuery},
                    new object[] {FilterByOrganizationTelephoneChangeRecord, FilterByOrganizationTelephoneChangeQuery},
                };

            private static Action<LeadRecord> FilterByIndividualNameChangeRecord => record =>
                record.Individual.Name.Assign(PersonName);
            private static Action<LeadQuery> FilterByIndividualNameChangeQuery => query =>
                query.IndividualName = $"{PersonName.Firstname}  {PersonName.Lastname} {PersonName.Middlename}";

            private static Action<LeadRecord> FilterByOrganizationNameChangeRecord => record =>
                record.Organization.Name = OrganizationName;
            private static Action<LeadQuery> FilterByOrganizationNameChangeQuery => query =>
                query.OrganizationName = OrganizationName;

            private static Action<LeadRecord> FilterByStatusChangeRecord => record =>
                record.Lead.ChangeStatus(Status.Disqualified);
            private static Action<LeadQuery> FilterByStatusChangeQuery => query =>
                query.Status = (int)Status.Disqualified;

            private static Action<LeadRecord> FilterByIndividualTelephoneChangeRecord => record =>
                record.Individual.Contact.Add(new Telephone(Telephone), Fixture.Create<string>(), true);
            private static Action<LeadQuery> FilterByIndividualTelephoneChangeQuery => query =>
                query.IndividualTelephone = Telephone.Number;

            private static Action<LeadRecord> FilterByOrganizationTelephoneChangeRecord => record =>
                record.Organization.Contact.Add(new Telephone(Telephone), Fixture.Create<string>(), true);
            private static Action<LeadQuery> FilterByOrganizationTelephoneChangeQuery => query =>
                query.OrganizationTelephone = Telephone.Number;

            public IEnumerator<object[]> GetEnumerator() => Data.GetEnumerator();
            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        [Fact]
        public async Task Handle_FilterByEmployeeOwnerId()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new DealerLeadSeeder(context))
            {
                //Arrange
                //Seed
                var dealerId = seeder.Create<DealerId>();
                var employeeOwnerId = seeder.Create<int>();
                var leads = Enumerable.Range(0, 10)
                    .Select(_ => seeder.Create(new { dealerId }))
                    .ToList();
                leads.ForEach(l => l.AssignOwner(employeeOwnerId));
                await seeder.AddRange(leads.ToArray());

                await seeder.AddRange(5);
                await seeder.AddRange(5, new { dealerId });

                await seeder.SeedAsync();

                //Query and handler
                var query = new LeadQuery
                {
                    DealerCode = dealerId.DealerCode.Value,
                    DistributorId = dealerId.DistributorId.Value,
                    PageNumber = PageNumber,
                    PageSize = PageSize,
                    EmployeeOwnerId = employeeOwnerId
                };
                var handler = new LeadQueryHandler(context, _mapper);

                //MapExpected
                var expectedRecords = GetLeadRecords(seeder, leads)
                    .OrderBy(r => r.Individual.Name.Lastname)
                    .Skip((PageNumber - 1) * PageSize)
                    .Take(PageSize);
                var items = _mapper.Map<LeadDto[]>(expectedRecords);
                var expected = new PagedResponse<LeadDto>(new PageOptions(PageNumber, PageSize), leads.Count, items);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<LeadDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenLeads_AreNotFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new DealerLeadSeeder(context))
            {
                //Arrange
                //Seed unexpected
                await seeder.AddRange(3);
                await seeder.SeedAsync();

                //Query and handler
                var query = new LeadQuery
                {
                    DealerCode = seeder.Create<DealerCode>().Value,
                    DistributorId = seeder.Create<DistributorId>().Value
                };
                var handler = new LeadQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(result.Items);
            }
        }

        [Fact]
        public async Task Handle_WhenDealerId_IsInvalid_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new Fixture().Create<LeadQuery>();
                var handler = new LeadQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(result.Items);
            }
        }
    }
}
