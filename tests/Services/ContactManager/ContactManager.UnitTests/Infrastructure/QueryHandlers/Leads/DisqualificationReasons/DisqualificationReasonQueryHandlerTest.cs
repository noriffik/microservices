﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using NexCore.ContactManager.Application.Leads.Queries.DisqualificationReasons;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Leads.DisqualificationReasons;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Leads.DisqualificationReasons
{
    public class DisqualificationReasonQueryHandlerTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly InMemoryDbContextFactory _factory = new InMemoryDbContextFactory(typeof(DisqualificationReasonQueryHandlerTest));

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new DisqualificationReasonQueryHandler(null, _mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                using (var context = _factory.Create())
                {
                    var unused = new DisqualificationReasonQueryHandler(context, null);
                }
            });
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Throws()
        {
            using (var context = _factory.Create())
            {
                var handler = new DisqualificationReasonQueryHandler(context, _mapper);

                await Assert.RequestHandler(handler).ThrowsOnNull(typeof(DisqualificationReasonByIdQuery));
            }
        }
    }
}
