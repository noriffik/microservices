﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Leads.Queries.DisqualificationReasons;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Leads.DisqualificationReasons;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.Testing;
using NexCore.Testing.Seeders;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Leads.DisqualificationReasons
{
    public class DisqualificationReasonQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new EntitySeeder<DisqualificationReason>(context))
            {
                //Arrange
                var reasons = await seeder.AddRange(5);

                await seeder.SeedAsync();

                var query = new DisqualificationReasonQuery
                {
                    PageNumber = 1,
                    PageSize = 2
                };

                var handler = new DisqualificationReasonQueryHandler(context, _mapper);

                //Map expected
                var items = reasons.OrderBy(t => t.Name)
                    .Skip((query.PageNumber.Value - 1) * query.PageSize.Value)
                    .Take(query.PageSize.Value)
                    .Select(t => new DisqualificationReasonDto
                    {
                        Id = t.Id,
                        Name = t.Name
                    }).ToArray();

                var expected = new PagedResponse<DisqualificationReasonDto>(PageOptions.From(query), reasons.Length, items);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<DisqualificationReasonDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WithFilterByName()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new EntitySeeder<DisqualificationReason>(context))
            {
                //Arrange
                const int count = 2;
                var name = seeder.Create<string>();

                //Assign name
                var reasons = await seeder.AddRange(5);
                reasons.Take(count)
                    .ToList()
                    .ForEach(r => r.ChangeName($"prefix{name}postfix"));

                await seeder.SeedAsync();

                var query = new DisqualificationReasonQuery
                {
                    Name = name,
                    PageNumber = 1,
                    PageSize = 2
                };

                var handler = new DisqualificationReasonQueryHandler(context, _mapper);

                //Map expected
                var items = reasons
                    .Skip((query.PageNumber.Value - 1) * query.PageSize.Value)
                    .Take(query.PageSize.Value)
                    .Select(t => new DisqualificationReasonDto
                    {
                        Id = t.Id,
                        Name = t.Name
                    }).ToArray();

                var expected = new PagedResponse<DisqualificationReasonDto>(PageOptions.From(query), count, items[0], items[1]);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<DisqualificationReasonDto>>.Instance);
            }
        }

        [Fact]
        public Task Handle_WhenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new DisqualificationReasonQueryHandler(context, _mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request",
                    () => handler.Handle(null as DisqualificationReasonQuery, CancellationToken.None));
            }
        }
    }
}
