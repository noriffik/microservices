﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using NexCore.ContactManager.Application.Leads.Queries.DisqualificationReasons;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Leads.DisqualificationReasons;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.Testing;
using NexCore.Testing.Seeders;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Leads.DisqualificationReasons
{
    public class DisqualificationReasonByIdQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
       
        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new EntitySeeder<DisqualificationReason>(context))
            {
                //Arrange
                //Seed
                await seeder.AddRange(5);
                var reason = await seeder.Add();
                await seeder.SeedAsync();

                //Map expected
                var expected = new DisqualificationReasonDto
                {
                    Id = reason.Id,
                    Name = reason.Name
                };

                //Query and handler
                var query = new DisqualificationReasonByIdQuery
                {
                    Id = reason.Id
                };
                var handler = new DisqualificationReasonQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<DisqualificationReasonDto>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenTopic_IsNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new EntitySeeder<DisqualificationReason>(context))
            {
                //Arrange
                //Seed unexpected
                await seeder.AddRange(5);
                await seeder.SeedAsync();

                //Query and handler
                var query = new DisqualificationReasonByIdQuery
                {
                    Id = seeder.Create<int>()
                };
                var handler = new DisqualificationReasonQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }
    }
}

