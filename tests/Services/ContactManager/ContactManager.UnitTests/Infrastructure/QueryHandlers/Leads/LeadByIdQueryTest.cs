﻿using AutoFixture;
using AutoMapper;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Leads;
using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Leads;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure.Seeders;
using NexCore.DealerNetwork;
using NexCore.Testing;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Leads
{
    public class LeadByIdQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public async Task Handle_With_CorporateCustomer_And_Employee()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new DealerLeadSeeder(context))
            {
                //Arrange
                //Seed
                var lead = seeder.Create<Lead>(new { contact = new Contact(seeder.Create<int>(), seeder.Create<int>()) });
                lead.AssignOwner(seeder.Create<int>());
                await seeder.Add(lead);

                await seeder.AddRange(3, new { dealerId = lead.DealerId });

                await seeder.SeedAsync();

                //Map expected
                var record = GetLeadRecord(seeder, lead);
                var expected = _mapper.Map<LeadDto>(record);

                //Query and handler
                var query = GetQuery(lead);
                var handler = new LeadQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<LeadDto>.Instance);
            }
        }

        private static LeadByIdQuery GetQuery(Lead lead) =>
            new LeadByIdQuery
            {
                Id = lead.Id,
                DealerCode = lead.DealerCode,
                DistributorId = lead.DistributorId
            };

        private static LeadRecord GetLeadRecord(DealerLeadSeeder seeder, Lead lead)
        {
            return new LeadRecord
            {
                Lead = lead,
                Individual = seeder.Find<Individual>(lead.Contact.IndividualId),
                Organization = lead.Contact.OrganizationId.HasValue
                    ? seeder.Find<Organization>(lead.Contact.OrganizationId.GetValueOrDefault())
                    : null,
                Topic = seeder.Find<Topic>(lead.TopicId),
                EmployeeOwner = lead.OwnerEmployeeId.HasValue
                    ? GetEmployeeOwnerRecord(seeder, lead.OwnerEmployeeId.Value)
                    : null
            };
        }

        private static EmployeeOwnerRecord GetEmployeeOwnerRecord(DealerLeadSeeder seeder, int id)
        {
            var employee = seeder.Find<DealerEmployee>(id);

            return new EmployeeOwnerRecord
            {
                Employee = employee,
                Individual = seeder.Find<Individual>(employee.IndividualId),
                Organization = seeder.Find<Organization>(employee.OrganizationId)
            };
        }

        [Fact]
        public async Task Handle_Without_CorporateCustomer_And_Employee()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new DealerLeadSeeder(context))
            {
                //Arrange
                //Seed
                var lead = seeder.Create<Lead>(new { contact = new Contact(seeder.Create<int>(), null) });
                await seeder.Add(lead);
                await seeder.AddRange(3, new { dealerId = lead.DealerId });
                await seeder.SeedAsync();

                //Map expected
                var record = GetLeadRecord(seeder, lead);
                var expected = _mapper.Map<LeadDto>(record);

                //Query and handler
                var query = GetQuery(lead);
                var handler = new LeadQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<LeadDto>.Instance);
            }
        }

        [Fact]
        public async Task Handle_GivenDealerId_IsNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new DealerLeadSeeder(context))
            {
                //Arrange
                //Seed
                var lead = seeder.Create<Lead>();
                await seeder.Add(lead);
                await seeder.AddRange(3, new { dealerId = lead.DealerId });
                await seeder.SeedAsync();

                //Query and handler
                var query = new LeadByIdQuery
                {
                    Id = seeder.Create<int>(),
                    DealerCode = seeder.Create<DealerCode>().Value,
                    DistributorId = seeder.Create<DistributorId>().Value,
                };
                var handler = new LeadQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public async Task Handle_GivenDealerId_IsInvalid_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new Fixture().Create<LeadByIdQuery>();
                var handler = new LeadQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public async Task Handle_WhenLead_IsNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new DealerLeadSeeder(context))
            {
                //Arrange
                //Seed unexpected
                await seeder.AddRange(3);
                await seeder.SeedAsync();

                //Query and handler
                var query = new LeadByIdQuery
                {
                    Id = seeder.Create<int>(),
                    DealerCode = seeder.Create<DealerCode>().Value,
                    DistributorId = seeder.Create<DistributorId>().Value
                };
                var handler = new LeadQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }
    }
}
