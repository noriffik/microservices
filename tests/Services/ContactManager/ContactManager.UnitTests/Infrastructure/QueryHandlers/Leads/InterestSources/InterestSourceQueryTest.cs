﻿using AutoMapper;
using NexCore.ContactManager.Application.Leads.Queries.InterestSources;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Leads.InterestSources;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.Testing;
using NexCore.Testing.Seeders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Leads.InterestSources
{
    public class InterestSourceQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new EntitySeeder<InterestSource>(context))
            {
                //Arrange
                var interests = await seeder.AddRange(5);
                await seeder.SeedAsync();

                var expected = interests.Select(i =>
                    new InterestSourceDto
                    {
                        Id = i.Id,
                        Name = i.Name,
                        DateArchived = i.DateArchived
                        
                    }).OrderBy(i => i.Name);

                var query = new InterestSourceQuery();

                var handler = new InterestSourceQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<InterestSourceDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WithFilterByName()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            using (var seeder = new EntitySeeder<InterestSource>(context))
            {
                //Arrange
                const int count = 2;
                var name = seeder.Create<string>();
                var interests = await seeder.AddRange(5);
                interests.Take(count)
                    .ToList()
                    .ForEach(i => i.ChangeName($"prefix{name}postfix"));

                await seeder.SeedAsync();

                var expected = interests.Take(count).Select(i =>
                    new InterestSourceDto
                    {
                        Id = i.Id,
                        Name = i.Name
                    }).OrderBy(i => i.Name);

                var query = new InterestSourceQuery
                {
                    Name = name
                };

                var handler = new InterestSourceQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert

                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<InterestSourceDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenInterests_IsNotFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new InterestSourceQuery();
                var handler = new InterestSourceQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(result);
            }
        }

        [Fact]
        public Task Handle_WhenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new InterestSourceQueryHandler(context, _mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request",
                    () => handler.Handle(null as InterestSourceQuery, CancellationToken.None));
            }
        }
    }
}
