﻿using AutoMapper;
using NexCore.ContactManager.Application.Leads.Queries.InterestSources;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Leads.InterestSources;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Leads.InterestSources
{
    public class InterestSourceQueryHandlerTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly InMemoryDbContextFactory _factory = new InMemoryDbContextFactory(typeof(InterestSourceQueryHandlerTest));

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new InterestSourceQueryHandler(null, _mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                using (var context = _factory.Create())
                {
                    var unused = new InterestSourceQueryHandler(context, null);
                }
            });
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Throws()
        {
            using (var context = _factory.Create())
            {
                var handler = new InterestSourceQueryHandler(context, _mapper);

                await Assert.RequestHandler(handler).ThrowsOnNull(typeof(InterestSourceByIdQuery));
            }
        }

    }
}
