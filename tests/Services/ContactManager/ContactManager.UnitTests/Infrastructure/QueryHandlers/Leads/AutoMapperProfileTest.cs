﻿using AutoFixture;
using AutoMapper;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Leads;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Leads.Queries.Topics;
using NexCore.ContactManager.Application.Organizations.Queries;
using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Leads;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.Testing;
using NexCore.Testing.Extensions;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Leads
{
    public class AutoMapperProfileTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly IFixture _fixture = new Fixture()
            .Customize(new DealerIdCustomization());

        [Fact]
        public void Map_FromEmployeeOwnerRecord_ToEmployeeOwnerDto()
        {
            //Arrange
            var employee = _fixture.Create<DealerEmployee>();
            var individual = _fixture.Construct<Individual>(employee.IndividualId);
            var organization = _fixture.Construct<Organization>(employee.OrganizationId);
            var record = _fixture.Build<EmployeeOwnerRecord>()
                .With(r => r.Employee, employee)
                .With(r => r.Individual, individual)
                .With(r => r.Organization, organization)
                .Create();
            var expected = Map(record);

            //Act
            var actual = _mapper.Map<EmployeeOwnerDto>(record);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<EmployeeOwnerDto>.Instance);
        }

        private static EmployeeOwnerDto Map(EmployeeOwnerRecord record)
        {
            return new EmployeeOwnerDto
            {
                EmployeeId = record.Employee.Id,
                EmployeeName = new PersonNameDto
                {
                    Firstname = record.Individual.Name.Firstname,
                    Lastname = record.Individual.Name.Lastname,
                    Middlename = record.Individual.Name.Middlename
                },
                DealerId = record.Employee.DealerId.Value,
                EmployeeIndividualId = record.Individual.Id,
                DealerOrganizationId = record.Employee.OrganizationId,
                DealerName = record.Organization.Name
            };
        }

        [Fact]
        public void Map_FromLeadRecord_ToLeadDto_With_Organization_And_Employee()
        {
            //Arrange
            var lead = _fixture.Create<Lead>();
            var topic = _fixture.Construct<Topic>(lead.TopicId);
            var individual = _fixture.Construct<Individual>(lead.Contact.IndividualId);
            var organization = _fixture.Construct<Organization>(lead.Contact.OrganizationId.GetValueOrDefault());
            var record = _fixture.Build<LeadRecord>()
                .With(r => r.Lead, lead)
                .With(r => r.Topic, topic)
                .With(r => r.Individual, individual)
                .With(r => r.Organization, organization)
                .Create();
            var expected = Map(record);

            //Act
            var actual = _mapper.Map<LeadDto>(record);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<LeadDto>.Instance);
        }

        [Fact]
        public void Map_FromLeadRecord_ToLeadDto_Without_Organization_And_Employee()
        {
            //Arrange
            var lead = _fixture.Create<Lead>();
            var topic = _fixture.Construct<Topic>(lead.TopicId);
            var individual = _fixture.Construct<Individual>(lead.Contact.IndividualId);
            var record = _fixture.Build<LeadRecord>()
                .With(r => r.Lead, lead)
                .With(r => r.Topic, topic)
                .With(r => r.Individual, individual)
                .Without(r => r.Organization)
                .Without(r => r.EmployeeOwner)
                .Create();
            var expected = Map(record);

            //Act
            var actual = _mapper.Map<LeadDto>(record);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<LeadDto>.Instance);
        }

        private LeadDto Map(LeadRecord record)
        {
            return new LeadDto
            {
                Id = record.Lead.Id,
                DealerId = record.Lead.DealerId.Value,
                Topic = new TopicDto
                {
                    Id = record.Topic.Id,
                    Name = record.Topic.Name,
                },
                ContactType = record.Lead.Contact.Type,
                Individual = _mapper.Map<IndividualDto>(record.Individual),
                Organization = record.Organization == null ? null : _mapper.Map<OrganizationDto>(record.Organization),
                EmployeeOwner = record.EmployeeOwner == null ? null : _mapper.Map<EmployeeOwnerDto>(record.EmployeeOwner),
                Status = (int)record.Lead.Status,
                CreatedAt = record.Lead.CreatedAt,
                UpdatedAt = record.Lead.UpdatedAt
            };
        }
    }
}
