﻿using AutoMapper;
using NexCore.ContactManager.Application.Distributors.Queries.Dealers.Leads;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Leads;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Leads
{
    public class LeadQueryHandlerTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly InMemoryDbContextFactory _factory = new InMemoryDbContextFactory(typeof(LeadQueryHandlerTest));

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new LeadQueryHandler(null, _mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                using (var context = _factory.Create())
                {
                    var unused = new LeadQueryHandler(context, null);
                }
            });
        }

        [Theory]
        [InlineData(typeof(LeadQuery))]
        [InlineData(typeof(LeadByIdQuery))]
        public async Task Handle_GivenRequest_IsNull_Throws(Type queryType)
        {
            using (var context = _factory.Create())
            {
                var handler = new LeadQueryHandler(context, _mapper);

                await Assert.RequestHandler(handler).ThrowsOnNull(queryType);
            }
        }
    }
}
