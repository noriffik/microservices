﻿using NexCore.ContactManager.Application.Individuals.Queries;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Individuals;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using NexCore.ContactManager.Application.Individuals.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Individuals
{
    public class CommunicationMediumQueryHandlerTest
    {
        private readonly CommunicationMediumQueryHandler _handler;
        private readonly Mock<IMapper> _mapper;

        public CommunicationMediumQueryHandlerTest()
        {
            _mapper = new Mock<IMapper>();
            _handler = new CommunicationMediumQueryHandler(_mapper.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(CommunicationMediumQueryHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            _mapper.Setup(m => m.Map<IEnumerable<CommunicationMediumDto>>(It.IsAny<IEnumerable<CommunicationMedium>>()))
                .Verifiable();
            
            //Act
            await _handler.Handle(new CommunicationMediumQuery(), CancellationToken.None);

            //Assert
            _mapper.Verify();
        }

        [Fact]
        public Task Handle_GivenQuery_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request",
                () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
