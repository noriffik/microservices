﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Individuals.Queries;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Individuals
{
    public class IndividualQueryValidatorTest
    {
        private readonly IndividualQueryValidator _validator = new IndividualQueryValidator();

        [Theory]
        [InlineData("38")]
        [InlineData("3")]
        public void ShouldHaveError_WhenTelephone_IsEmpty_OrInvalid(string value)
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Telephone, value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("4830")]
        [InlineData("4830asdasdw")]
        public void ShouldNotHaveError_WhenTelephone_IsNumeric(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Telephone, value);
        }
    }
}
