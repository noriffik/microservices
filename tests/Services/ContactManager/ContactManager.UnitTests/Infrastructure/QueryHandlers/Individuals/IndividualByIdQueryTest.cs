﻿using AutoMapper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Individuals.Queries;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.Testing;
using NexCore.Testing.Seeders;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Individuals
{
    public class IndividualByIdQueryTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new EntitySeeder<Individual>(context);
                await seeder.AddRange(3);

                var individual = await seeder.Add();

                await seeder.SeedAsync();

                var query = new IndividualByIdQuery{ Id = individual.Id };
                var handler = new IndividualQueryHandler(context, _mapper);
                var expected = _mapper.Map<IndividualDto>(individual);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IndividualDto>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new EntitySeeder<Individual>(context);
                await seeder.AddRange(3);
                await seeder.SeedAsync();

                var query = new IndividualByIdQuery { Id = 2193 };
                var handler = new IndividualQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }
    }
}
