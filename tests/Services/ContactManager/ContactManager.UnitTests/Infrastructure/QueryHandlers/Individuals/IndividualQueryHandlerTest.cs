﻿using AutoFixture;
using AutoMapper;
using NexCore.Application.Queries;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Individuals.Queries;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Infrastructure.QueryHandlers.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Infrastructure;
using NexCore.Legal;
using NexCore.Testing;
using NexCore.Testing.Extensions;
using NexCore.Testing.Seeders;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.QueryHandlers.Individuals
{
    public class IndividualQueryHandlerTest
    {
        private readonly Fixture _fixture = new Fixture();
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly InMemoryDbContextFactory _factory = new InMemoryDbContextFactory(typeof(IndividualQueryHandlerTest));

        [Fact]
        public void ContactableQuery()
        {
            using (var context = _factory.Create())
            {
                var handler = new IndividualQueryHandler(context, _mapper);

                Assert.Injection
                    .OfProperty(handler, nameof(handler.ContactableQuery))
                    .HasDefault()
                    .HasNullGuard()
                    .DoesOverride();
            }
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () =>
            {
                var unused = new IndividualQueryHandler(null, _mapper);
            });
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = _factory.Create())
                {
                    var unused = new IndividualQueryHandler(context, null);
                }
            });
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Throws()
        {
            using (var context = _factory.Create())
            {
                var handler = new IndividualQueryHandler(context, _mapper);

                await Assert.RequestHandler(handler).ThrowsOnNull(typeof(IndividualByIdQuery));
            }
        }

        [Fact]
        public async Task Handle_GivenRequest_IsIndividualQuery()
        {
            using (var context = _factory.Create())
            {
                //Arrange
                var seeder = new EntitySeeder<Individual>(context);
                var individuals = await seeder.AddRange(3);

                await seeder.SeedAsync();

                var query = new IndividualQuery();
                var handler = new IndividualQueryHandler(context, _mapper);

                var expected = new PagedResponse<IndividualDto>(PageOptions.Default,
                    individuals.Length, _mapper.Map<IndividualDto[]>(individuals));

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<IndividualDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WithFilterByName()
        {
            using (var context = _factory.Create())
            {
                //Arrange
                var seeder = new EntitySeeder<Individual>(context);
                var individuals = _fixture.CreateMany<Individual>()
                    .ToList();

                var customIndividual = _fixture.Construct<Individual>(new
                {
                    name = new PersonName(individuals[0].Name.Firstname,
                        individuals[0].Name.Lastname,
                        individuals[0].Name.Middlename)
                });

                await seeder.AddRange(individuals.ToArray());
                await seeder.Add(customIndividual);
                await seeder.SeedAsync();

                var queryableName = $" {individuals[0].Name.Firstname}   {individuals[0].Name.Lastname} ";

                var query = new IndividualQuery
                {
                    Name = queryableName
                };
                var handler = new IndividualQueryHandler(context, _mapper);

                //Prepare expected
                var expectedIndividuals = new[] { individuals[0], customIndividual };
                var expected = new PagedResponse<IndividualDto>(PageOptions.Default, expectedIndividuals.Length, _mapper.Map<IndividualDto[]>(expectedIndividuals) );
                
                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.NotEmpty(result.Items);
                Assert.Equal(expected, result, PropertyComparer<PagedResponse<IndividualDto>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WithFilterByTelephone()
        {
            using (var context = _factory.Create())
            {
                //Arrange
                var individuals = _fixture.CreateMany<Individual>(4).ToArray();

                individuals[0].Contact.Add(new Telephone("380", "44", "5665544"));
                individuals[1].Contact.Add(new Telephone("380", "44", "5665545"));
                individuals[2].Contact.Add(new Telephone("380", "56", "5665546"));

                var seeder = new EntitySeeder<Individual>(context);

                await seeder.AddRange(individuals);
                await seeder.SeedAsync();

                var query = new IndividualQuery
                {
                    Telephone = "38044"
                };
                var handler = new IndividualQueryHandler(context, _mapper);

                var expected = new PagedResponse<IndividualDto>(PageOptions.Default, 2,
                    _mapper.Map<IndividualDto[]>(individuals.Take(2)
                        .Select(i => _mapper.Map<Individual, IndividualDto>(i))));

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.NotNull(actual);
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<IndividualDto>>.Instance);
            }
        }
    }
}
