﻿using Moq;
using NexCore.Infrastructure.Seeds;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.Seeds
{
    public class CompositeSeederTest
    {
        [Fact]
        public async Task SeedAsync()
        {
            //Arrange
            var innerSeeders = Enumerable.Range(0, 3)
                .Select(result => new Mock<ISeeder>())
                .ToList();
            innerSeeders.ForEach(s =>
            {
                s.Setup(i => i.Seed())
                    .Returns(Task.CompletedTask)
                    .Verifiable();
            });

            var seeder = new CompositeSeeder(innerSeeders.Select(s => s.Object).ToArray());

            //Act
            await seeder.Seed();

            //Assert
            innerSeeders.ForEach(s => s.Verify());
        }
    }
}
