﻿using NexCore.ContactManager.Application.Providers;
using NexCore.ContactManager.Domain.Languages;
using NexCore.ContactManager.Infrastructure.Providers;
using NexCore.Testing;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Infrastructure.Providers
{
    public class CultureInfoLanguageProviderTest
    {
        private readonly CultureInfoLanguageProvider _provider = new CultureInfoLanguageProvider();

        [Fact]
        public async Task Get()
        {
            //Arrange
            var expected = CultureInfo.GetCultures(CultureTypes.NeutralCultures)
                .Select(c => new Language(c.ThreeLetterISOLanguageName, c.TwoLetterISOLanguageName, c.Name));

            //Act
            var actual = await _provider.Get();

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<IEnumerable<Language>>.Instance);
        }

        [Theory]
        [InlineData("ru")]
        [InlineData("en")]
        [InlineData("uk")]
        public async Task GetByIso2(string code)
        {
            //Arrange
            var expected = Language.From(CultureInfo.GetCultureInfo(code));

            //Act
            var actual = await _provider.GetByIso2(expected.ThreeLetterIsoCode);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<Language>.Instance);
        }

        [Fact]
        public async Task GetByIso2_GivenCode_IsInvalid_Throws()
        {
            var e = await Assert.ThrowsAsync<InvalidLanguageCodeException>(() => _provider.GetByIso2("invalid"));

            Assert.Equal("invalid", e.LanguageCode);
        }

        [Fact]
        public Task GetByIso2_GivenCode_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("code", () => _provider.GetByIso2(null));
        }

        [Theory]
        [InlineData("rus", true)]
        [InlineData("eng", true)]
        [InlineData("invalid", false)]
        public async Task IsValidIso2(string code, bool expected)
        {
            //Act
            var actual = await _provider.IsValidIso2(code);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public Task IsValidIso2_GivenCode_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("code", () => _provider.IsValidIso2(null));
        }
    }
}
