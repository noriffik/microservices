﻿using AutoFixture;
using NexCore.ContactManager.Domain.DistributorEmployees;
using NexCore.ContactManager.Domain.DistributorEmployees.Specifications;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Testing.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.DistributorEmployees.Specifications
{
    public class ByDistributorIdAndEmployeeIdSpecificationTest
    {
        private readonly DistributorId _distributorId;
        private readonly int _employeeId;

        //Entities
        private readonly DistributorEmployee _expected;
        private readonly IReadOnlyList<DistributorEmployee> _unexpected;
        private readonly IReadOnlyList<DistributorEmployee> _all;

        //Specification
        private readonly ByDistributorIdAndEmployeeIdSpecification _specification;

        public ByDistributorIdAndEmployeeIdSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customize(new DistributorIdCustomization());

            _distributorId = fixture.Create<DistributorId>();
            _employeeId = fixture.Create<int>();

            //Entities
            _expected = fixture.Construct<DistributorEmployee>(_employeeId, new { distributorId = _distributorId });
            _unexpected = fixture.CreateMany<DistributorEmployee>(3).ToList();

            _all = _unexpected.Append(_expected).ToList();

            //Specification
            _specification = new ByDistributorIdAndEmployeeIdSpecification(_distributorId, _employeeId);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var specification = new ByDistributorIdAndEmployeeIdSpecification(_distributorId, _employeeId);

            //Assert
            Assert.Equal(_distributorId, specification.DistributorId);
            Assert.Equal(_employeeId, specification.EmployeeId);
        }

        [Fact]
        public void Ctor_GivenDistributor_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("distributorId", () => new ByDistributorIdAndEmployeeIdSpecification(null, _employeeId));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression).SingleOrDefault());
        }
    }
}
