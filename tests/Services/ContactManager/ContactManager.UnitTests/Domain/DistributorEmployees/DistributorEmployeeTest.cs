﻿using AutoFixture;
using NexCore.ContactManager.Domain.DistributorEmployees;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.DistributorEmployees
{
    public class DistributorEmployeeTest
    {
        private readonly int _id;
        private readonly int _individualId;
        private readonly int _organizationId;
        private readonly DistributorId _distributorId;
        private readonly int _jobId;

        public DistributorEmployeeTest()
        {
            var fixture = new Fixture()
                .Customize(new DistributorIdCustomization());

            _id = fixture.Create<int>();
            _individualId = fixture.Create<int>();
            _organizationId = fixture.Create<int>();
            _distributorId = fixture.Create<DistributorId>();
            _jobId = fixture.Create<int>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var employee = new DistributorEmployee(_individualId, _organizationId, _distributorId, _jobId);

            //Assert
            Assert.Equal(_individualId, employee.IndividualId);
            Assert.Equal(_organizationId, employee.OrganizationId);
            Assert.Equal(_distributorId, employee.DistributorId);
            Assert.Equal(_jobId, employee.JobId);
        }

        [Fact]
        public void Ctor_WithId()
        {
            //Act
            var employee = new DistributorEmployee(_id, _individualId, _organizationId, _distributorId, _jobId);

            //Assert
            Assert.Equal(_id, employee.Id);
            Assert.Equal(_individualId, employee.IndividualId);
            Assert.Equal(_organizationId, employee.OrganizationId);
            Assert.Equal(_distributorId, employee.DistributorId);
            Assert.Equal(_jobId, employee.JobId);
        }
    }
}
