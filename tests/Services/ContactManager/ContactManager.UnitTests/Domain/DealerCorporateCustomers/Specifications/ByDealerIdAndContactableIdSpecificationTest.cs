﻿using AutoFixture;
using NexCore.ContactManager.Domain.DealerCorporateCustomers;
using NexCore.ContactManager.Domain.DealerCorporateCustomers.Specifications;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Testing.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.DealerCorporateCustomers.Specifications
{
    public class ByDealerIdAndContactableIdSpecificationTest
    {
        private readonly DealerId _dealerId;
        private const int ContactableId = 1;

        //Entities
        private readonly DealerCorporateCustomer _expected;
        private readonly IReadOnlyList<DealerCorporateCustomer> _unexpected;
        private readonly IReadOnlyList<DealerCorporateCustomer> _all;

        //Specification
        private readonly ByDealerIdAndContactableIdSpecification _idSpecification;

        public ByDealerIdAndContactableIdSpecificationTest()
        {
            var fixture = new Fixture()
                .Customize(new DealerIdCustomization());

            _dealerId = fixture.Create<DealerId>();

            //Entities
            _expected = fixture.Construct<DealerCorporateCustomer>(new
                {contactableId = ContactableId, dealerId = _dealerId});

            _unexpected = fixture.CreateMany<DealerCorporateCustomer>(4).ToList();
            _all = _unexpected.Append(_expected).ToList();

            //Specification
            _idSpecification = new ByDealerIdAndContactableIdSpecification(ContactableId, _dealerId);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var specification = new ByDealerIdAndContactableIdSpecification(ContactableId, _dealerId);

            //Assert
            Assert.Equal(ContactableId, specification.ContactableId);
            Assert.Equal(_dealerId, specification.DealerId);
        }

        [Fact]
        public void Ctor_GivenDealerId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealerId", () => new ByDealerIdAndContactableIdSpecification(ContactableId, null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_idSpecification.IsSatisfiedBy(_expected));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_idSpecification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _idSpecification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _idSpecification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().SingleOrDefault(expression));
        }
    }
}
