﻿using AutoFixture;
using NexCore.ContactManager.Domain.DealerCorporateCustomers;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.DealerCorporateCustomers
{
    public class DealerCorporateCustomerTest
    {
        private readonly int _id;
        private readonly int _contactableId;
        private readonly int _organizationId;
        private readonly DealerId _dealerId;

        public DealerCorporateCustomerTest()
        {
            var fixture = new Fixture();
            fixture.Customize(new DealerIdCustomization());

            _id = fixture.Create<int>();
            _contactableId = fixture.Create<int>();
            _organizationId = fixture.Create<int>(); 
            _dealerId = fixture.Create<DealerId>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var actual = new DealerCorporateCustomer(_contactableId, _organizationId, _dealerId);

            //Assert
            Assert.Equal(_contactableId, actual.ContactableId);
            Assert.Equal(_organizationId, actual.OrganizationId);
            Assert.Equal(_dealerId, actual.DealerId);
        }

        [Fact]
        public void Ctor_WithId()
        {
            //Act
            var actual = new DealerCorporateCustomer(_id, _contactableId, _organizationId, _dealerId);

            //Assert
            Assert.Equal(_id, actual.Id);
            Assert.Equal(_contactableId, actual.ContactableId);
            Assert.Equal(_organizationId, actual.OrganizationId);
            Assert.Equal(_dealerId, actual.DealerId);
        }

        [Fact]
        public void Ctor_GivenDealerId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealerId",
                () => new DealerCorporateCustomer(_contactableId, _organizationId, null));
        }

        [Fact]
        public void Ctor_WithId_GivenDealerId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealerId",
                () => new DealerCorporateCustomer(_id, _contactableId, _organizationId, null));
        }
    }
}
