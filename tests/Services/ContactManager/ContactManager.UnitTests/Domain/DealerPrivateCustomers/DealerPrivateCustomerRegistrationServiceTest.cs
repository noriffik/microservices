﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Domain.Customers;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.DealerPrivateCustomers.Specifications;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.Testing;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.DealerPrivateCustomers
{
    public class DealerPrivateCustomerRegistrationServiceTest
    {
        //Entities
        private readonly DealerPrivateCustomer _customer;

        //Dependencies
        private readonly Mock<IEntityRepository> _repository;

        //Service
        private readonly DealerPrivateCustomerRegistrationService _service;

        public DealerPrivateCustomerRegistrationServiceTest()
        {
            _customer = new Fixture()
                .Customize(new DealerIdCustomization())
                .Create<DealerPrivateCustomer>();

            //Dependencies
            var work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Service
            _service = new DealerPrivateCustomerRegistrationService(work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DealerPrivateCustomerRegistrationService)).HasNullGuard();
        }

        [Fact]
        public async Task Register()
        {
            //Arrange
            var expectedEvent = new RegisteredEvent<DealerPrivateCustomer>(_customer);

            using (Sequence.Create())
            {
                _repository.Setup(r => r.HasRequired<Organization>(_customer.OrganizationId))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.HasRequired<Individual>(_customer.ContactableId))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.HasRequired<Dealer, DealerId>(_customer.DealerId))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.Has(It.Is<ByDealerIdAndContactableIdSpecifications>(s =>
                        s.DealerId == _customer.DealerId && s.ContactableId == _customer.ContactableId)))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Add(_customer))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _service.Register(_customer);
            }

            //Assert
            Assert.Contains(expectedEvent, _customer.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public async Task Register_GivenCustomer_IsAlreadyCustomer_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Has(It.Is<ByDealerIdAndContactableIdSpecifications>(s =>
                    s.DealerId == _customer.DealerId && s.ContactableId == _customer.ContactableId)))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.ThrowsAsync<DuplicateDealerPrivateCustomerException>(() => _service.Register(_customer));

            //Assert
            Assert.Equal(_customer.DealerId, e.DealerId);
            Assert.Equal(_customer.ContactableId, e.ContactableId);
        }
    }
}
