﻿using AutoFixture;
using Moq;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Customers;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.DealerPrivateCustomers
{
    public class RegisterHandlerTest
    {
        //Entities
        private readonly Telephone _telephone;
        private readonly Individual _individual;
        private readonly DealerPrivateCustomer _privateCustomer;

        //Dependencies
        private readonly Mock<IEntityRepository> _repository;

        //Event and Handler
        private readonly RegisteredEvent<DealerPrivateCustomer> _event;
        private readonly RegisteredHandler _handler;

        public RegisterHandlerTest()
        {
            var fixture = new Fixture()
                .Customize(new DealerIdCustomization())
                .Customize(new TelephoneNumberCustomization());

            //Entities
            _individual = fixture.Create<Individual>();
            _telephone = fixture.Create<Telephone>();

            _privateCustomer = fixture.Create<DealerPrivateCustomer>();

            //Dependencies
            var work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();

            //Event and Handler
            _event = new RegisteredEvent<DealerPrivateCustomer>(_privateCustomer);
            _handler = new RegisteredHandler(work.Object);

            //Setup dependencies
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            _repository.Setup(r => r.Require<Individual>(_event.Customer.ContactableId, It.IsAny<CancellationToken>()))
                .ReturnsAsync(_individual);
        }

        [Fact]
        public Task Handle_GivenNotification_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("notification", () =>
                _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            _individual.Contact.Add(_telephone);
            _individual.Contact.SetPrimary(_telephone);

            //Act
            await _handler.Handle(_event, CancellationToken.None);

            //Assert
            Assert.Equal(_telephone, _privateCustomer.Telephone);
        }

        [Fact]
        public async Task Handle_WhenPrimaryTelephone_IsNotFound_DoNothing()
        {
            //Act
            await _handler.Handle(_event, CancellationToken.None);

            //Assert
            _repository.Verify(r => r.Require<Individual>(_event.Customer.ContactableId, It.IsAny<CancellationToken>()), Times.Once);
            Assert.Equal(Telephone.Empty, _privateCustomer.Telephone);
        }
    }
}
