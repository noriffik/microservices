﻿using AutoFixture;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.DealerPrivateCustomers
{
    public class DealerPrivateCustomerTest
    {
        private readonly int _id;
        private readonly int _contactableId;
        private readonly int _organizationId;
        private readonly DealerId _dealerId;
        private readonly Telephone _telephone;

        private readonly DealerPrivateCustomer _customer;

        public DealerPrivateCustomerTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            _id = fixture.Create<int>();
            _contactableId = fixture.Create<int>();
            _organizationId = fixture.Create<int>();
            _dealerId = fixture.Create<DealerId>();
            _telephone = fixture.Create<Telephone>();

            _customer = new DealerPrivateCustomer(_id, _contactableId, _organizationId, _dealerId);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var customer = new DealerPrivateCustomer(_contactableId, _organizationId, _dealerId);

            //Assert
            Assert.Equal(_contactableId, customer.ContactableId);
            Assert.Equal(_organizationId, customer.OrganizationId);
            Assert.Equal(_dealerId, customer.DealerId);
            Assert.Equal(Telephone.Empty, customer.Telephone);
            Assert.Equal(PhysicalAddress.Empty, customer.PhysicalAddress);
        }

        [Fact]
        public void Ctor_WithId()
        {
            //Act
            var customer = new DealerPrivateCustomer(_id, _contactableId, _organizationId, _dealerId);

            //Assert
            Assert.Equal(_id, customer.Id);
            Assert.Equal(_contactableId, customer.ContactableId);
            Assert.Equal(_organizationId, customer.OrganizationId);
            Assert.Equal(_dealerId, customer.DealerId);
            Assert.Equal(Telephone.Empty, customer.Telephone);
            Assert.Equal(PhysicalAddress.Empty, customer.PhysicalAddress);
        }

        [Fact]
        public void UpdateTelephone()
        {
            //Act
            _customer.UpdateTelephone(_telephone);

            //Assert
            Assert.Equal(_telephone, _customer.Telephone);
            Assert.NotSame(_telephone, _customer.Telephone);
        }

        [Fact]
        public void UpdateTelephone_GivenTelephone_IsNull_AssignEmpty()
        {
            //Act
            _customer.UpdateTelephone(null);

            //Assert
            Assert.Equal(Telephone.Empty, _customer.Telephone);
        }

        [Fact]
        public void UpdatePhysicalAddress()
        {
            //Arrange
            var fixture = new Fixture();

            var physicalAddress = fixture.Create<PhysicalAddress>();

            //Act
            _customer.UpdatePhysicalAddress(physicalAddress);

            //Assert
            Assert.Equal(physicalAddress, _customer.PhysicalAddress);
        }

        [Fact]
        public void UpdatePhysicalAddress_GivenValue_IsNull_AssignEmpty()
        {
            //Act
            _customer.UpdatePhysicalAddress(null);

            //Assert
            Assert.Equal(PhysicalAddress.Empty, _customer.PhysicalAddress);
        }
    }
}
