﻿using AutoFixture;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.DealerPrivateCustomers.Specifications;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Testing.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.DealerPrivateCustomers.Specifications
{
    public class ByDealerIdSpecificationTest
    {
        private readonly DealerId _dealerId;

        //Entities
        private readonly IReadOnlyList<DealerPrivateCustomer> _expected;
        private readonly IReadOnlyList<DealerPrivateCustomer> _unexpected;
        private readonly IReadOnlyList<DealerPrivateCustomer> _all;

        //Specification
        private readonly ByDealerIdSpecification _specification;

        public ByDealerIdSpecificationTest()
        {
            var fixture = new Fixture()
                .Customize(new DealerIdCustomization());

            _dealerId = fixture.Create<DealerId>();

            //Entities
            _expected = fixture.ConstructMany<DealerPrivateCustomer>(3, new { dealerId = _dealerId }).ToList();
            _unexpected = fixture.CreateMany<DealerPrivateCustomer>(5).ToList();
            _all = _unexpected.Concat(_expected).ToList();

            //Specification
            _specification = new ByDealerIdSpecification(_dealerId);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var specification = new ByDealerIdSpecification(_dealerId);

            //Assert
            Assert.Equal(_dealerId, specification.DealerId);
        }

        [Fact]
        public void Ctor_GivenDealerId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealerId", () => new ByDealerIdSpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
