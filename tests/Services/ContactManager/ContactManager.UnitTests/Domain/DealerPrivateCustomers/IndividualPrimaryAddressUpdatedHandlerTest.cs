﻿using AutoFixture;
using Moq;
using NexCore.ContactManager.Domain.Customers.Specifications;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.DealerPrivateCustomers
{
    public class IndividualPrimaryAddressUpdatedHandlerTest
    {
        //Entities
        private readonly IEnumerable<DealerPrivateCustomer> _privateCustomers;

        //Event and Handler
        private readonly PrimaryPhysicalAddressUpdateEvent _event;
        private readonly IndividualPrimaryAddressUpdatedHandler _handler;

        public IndividualPrimaryAddressUpdatedHandlerTest()
        {
            var fixture = new Fixture()
                .Customize(new DealerIdCustomization())
                .Customize(new TelephoneNumberCustomization());

            //Entities
            _privateCustomers = fixture.CreateMany<DealerPrivateCustomer>();

            //Dependencies
            var work = new Mock<IUnitOfWork>();
            var repository = new Mock<IEntityRepository>();

            //Event and Handler
            _event = fixture.Create<PrimaryPhysicalAddressUpdateEvent>();
            _handler = new IndividualPrimaryAddressUpdatedHandler(work.Object);

            //Setup dependencies
            work.Setup(w => w.EntityRepository).Returns(repository.Object);
            repository.Setup(r => r.Find(
                    It.Is<ByContactableIdSpecification<DealerPrivateCustomer>>(s => s.ContactableId == _event.ContactableId),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(_privateCustomers);
        }

        [Fact]
        public Task Handle_GivenNotification_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("notification", () =>
                _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            //Act
            await _handler.Handle(_event, CancellationToken.None);

            //Assert
            Assert.All(_privateCustomers, c => Assert.Equal(_event.PhysicalAddress, c.PhysicalAddress));
        }
    }
}
