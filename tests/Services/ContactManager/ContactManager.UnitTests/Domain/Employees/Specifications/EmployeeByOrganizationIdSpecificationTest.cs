﻿using AutoFixture;
using Moq;
using NexCore.ContactManager.Domain.Employees;
using NexCore.ContactManager.Domain.Employees.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Employees.Specifications
{
    public class EmployeeByOrganizationIdSpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<Employee> _expected;
        private readonly IReadOnlyList<Employee> _unexpected;
        private readonly IReadOnlyList<Employee> _all;

        //Specification
        private readonly EmployeeByOrganizationIdSpecification<Employee> _specification;

        public EmployeeByOrganizationIdSpecificationTest()
        {
            var fixture = new Fixture();

            var organizationId = fixture.Create<int>();

            //Entities
            _expected = Enumerable.Range(0, 3)
                .Select(_ =>
                    new Mock<Employee>(fixture.Create<int>(), organizationId, fixture.Create<int>()) { CallBase = true }.Object)
                .ToList();
            _unexpected = Enumerable.Range(0, 3).Select(_ =>
                    new Mock<Employee>(fixture.Create<int>(), fixture.Create<int>(), fixture.Create<int>()) { CallBase = true }.Object)
                .ToList();
            _all = _unexpected.Concat(_expected).ToList();

            //Specification
            _specification = new EmployeeByOrganizationIdSpecification<Employee>(organizationId);
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
