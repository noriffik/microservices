﻿using AutoFixture;
using Moq;
using NexCore.ContactManager.Domain.Employees;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Employees
{
    public class EmployeeTest
    {
        private readonly Fixture _fixture = new Fixture();

        private readonly int _id;
        private readonly int _individualId;
        private readonly int _organizationId;
        private readonly int _jobId;

        private readonly Employee _employee;

        public EmployeeTest()
        {
            _id = _fixture.Create<int>();
            _individualId = _fixture.Create<int>();
            _organizationId = _fixture.Create<int>();
            _jobId = _fixture.Create<int>();

            _employee = new Mock<Employee>(_individualId, _organizationId, _jobId) { CallBase = true }.Object;
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var employee = new Mock<Employee>(_individualId, _organizationId, _jobId) { CallBase = true }.Object;

            //Assert
            Assert.Equal(_individualId, employee.IndividualId);
            Assert.Equal(_organizationId, employee.OrganizationId);
            Assert.Equal(_jobId, employee.JobId);
        }

        [Fact]
        public void Ctor_WithId()
        {
            //Act
            var employee = new Mock<Employee>(_id, _individualId, _organizationId, _jobId) { CallBase = true }.Object;

            //Assert
            Assert.Equal(_id, employee.Id);
            Assert.Equal(_individualId, employee.IndividualId);
            Assert.Equal(_organizationId, employee.OrganizationId);
            Assert.Equal(_jobId, employee.JobId);
        }

        [Fact]
        public void ChangeJobId()
        {
            //Arrange
            var jobId = _fixture.Create<int>();

            //Act
            _employee.ChangeJobId(jobId);

            //Assert
            Assert.Equal(jobId, _employee.JobId);
        }
    }
}
