﻿using AutoFixture;
using NexCore.ContactManager.Domain.Customers.Specifications;
using NexCore.Testing.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Customers.Specifications
{
    public class ByContactableIdSpecificationTest
    {
        //Entities
        private readonly int _contactableId;
        private readonly IReadOnlyList<TestCustomer> _expected;
        private readonly IReadOnlyList<TestCustomer> _unexpected;
        private readonly IReadOnlyList<TestCustomer> _all;

        //Specification
        private readonly ByContactableIdSpecification<TestCustomer> _specification;

        public ByContactableIdSpecificationTest()
        {
            var fixture = new Fixture();

            _contactableId = fixture.Create<int>();
            _expected = fixture.ConstructMany<TestCustomer>(3, new { contactableId = _contactableId }).ToList();
            _unexpected = fixture.CreateMany<TestCustomer>(2).ToList();
            _all = _unexpected.Concat(_expected).ToList();

            _specification = new ByContactableIdSpecification<TestCustomer>(_contactableId);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var specification = new ByContactableIdSpecification<TestCustomer>(_contactableId);

            //Assert
            Assert.Equal(_contactableId, specification.ContactableId);
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
