﻿using AutoFixture;
using NexCore.ContactManager.Domain.Customers.Specifications;
using NexCore.Testing.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Customers.Specifications
{
    public class ByOrganizationIdSpecificationTest
    {
        private readonly int _organizationId;

        //Entities
        private readonly IReadOnlyList<TestCustomer> _expected;
        private readonly IReadOnlyList<TestCustomer> _unexpected;
        private readonly IReadOnlyList<TestCustomer> _all;

        //Specification
        private readonly ByOrganizationIdSpecification<TestCustomer> _specification;

        public ByOrganizationIdSpecificationTest()
        {
            var fixture = new Fixture();

            _organizationId = fixture.Create<int>();

            //Entities
            _expected = fixture.ConstructMany<TestCustomer>(3, new { organizationId = _organizationId }).ToList();
            _unexpected = fixture.CreateMany<TestCustomer>(5).ToList();
            _all = _unexpected.Concat(_expected).ToList();

            //Specification
            _specification = new ByOrganizationIdSpecification<TestCustomer>(_organizationId);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var specification = new ByOrganizationIdSpecification<TestCustomer>(_organizationId);

            //Assert
            Assert.Equal(_organizationId, specification.OrganizationId);
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
