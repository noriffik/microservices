﻿using NexCore.ContactManager.Domain.Customers;

namespace NexCore.ContactManager.UnitTests.Domain.Customers
{
    public class TestCustomer : Customer
    {
        public TestCustomer(int id, int contactableId, int organizationId)
            : base(id, contactableId, organizationId)
        { }
    }
}
