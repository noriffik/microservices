﻿using AutoFixture;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Distributors
{
    public class DistributorTest
    {
        private readonly DistributorId _id;
        private readonly int _organizationId;
        private readonly string _countryCode;

        private readonly Distributor _distributor;

        public DistributorTest()
        {
            var fixture = new Fixture()
                .Customize(new DistributorIdCustomization());

            _id = fixture.Create<DistributorId>();
            _organizationId = fixture.Create<int>();
            _countryCode = fixture.Create<string>().ToLowerInvariant();

            _distributor = fixture.Create<Distributor>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var distributor = new Distributor(_id, _organizationId, _countryCode);

            //Assert
            Assert.Equal(_id, distributor.Id);
            Assert.Equal(_organizationId, distributor.OrganizationId);
            Assert.Equal(_countryCode.ToUpperInvariant(), distributor.CountryCode);
        }

        [Fact]
        public void Ctor_GivenId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("id", () => new Distributor(null, _organizationId, _countryCode));
        }

        [Fact]
        public void Ctor_GivenCountryCode_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("countryCode", () => new Distributor(_id, _organizationId, null));
        }

        [Fact]
        public void ChangeCountryCode()
        {
            //Act
            _distributor.ChangeCountryCode(_countryCode);

            //Assert
            Assert.Equal(_countryCode.ToUpperInvariant(), _distributor.CountryCode);
        }

        [Fact]
        public void ChangeCountryCode_GivenCode_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("countryCode", () => _distributor.ChangeCountryCode(null));
        }
    }
}
