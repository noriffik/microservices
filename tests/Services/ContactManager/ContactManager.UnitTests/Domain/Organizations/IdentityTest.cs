﻿using NexCore.ContactManager.Domain.Organizations;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Organizations
{
    public class IdentityTest
    {
        [Fact]
        public void WithEdrpou()
        {
            //Arrange 
            var code = "123456789";
            
            //Act 
            var identity = Identity.WithEdrpou(code);

            //Arrange
            Assert.NotNull(identity);
            Assert.Equal(identity.Edrpou, code);
            Assert.Null(identity.Ipn);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void WithEdrpou_GivenCode_IsNullOrEmpty_Throws(string value)
        {
            var result = Assert.Throws<ArgumentException>(() => { Identity.WithEdrpou(value); });
            Assert.Equal("code", result.ParamName);
        }

        [Fact]
        public void WithIpn()
        {
            //Arrange 
            var code = "123456789";

            //Act 
            var identity = Identity.WithIpn(code);

            //Arrange
            Assert.NotNull(identity);
            Assert.Equal(identity.Ipn, code);
            Assert.Null(identity.Edrpou);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void WithIpn_GivenCode_IsNullOrEmpty_Throws(string value)
        {
            var result = Assert.Throws<ArgumentException>(() => { Identity.WithIpn(value); });
            Assert.Equal("code", result.ParamName);
        }
    }
}
