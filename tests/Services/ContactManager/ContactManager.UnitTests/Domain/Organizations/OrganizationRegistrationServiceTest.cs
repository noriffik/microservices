﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.Domain.Organizations.Specifications;
using NexCore.Domain;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Organizations
{
    public class OrganizationRegistrationServiceTest
    {
        //Entity
        private readonly Organization _organization;

        //Dependencies
        private readonly Mock<IEntityRepository> _repository;

        //Service
        private readonly OrganizationRegistrationService _service;

        public OrganizationRegistrationServiceTest()
        {
            //Entity
            _organization = new Fixture().Create<Organization>();

            //Dependencies
            var work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Service
            _service = new OrganizationRegistrationService(work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(OrganizationRegistrationService)).HasNullGuard();
        }

        [Fact]
        public async Task Register()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Has(It.Is<ByIdentitySpecification>(s => s.Identity == _organization.Identity)))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Add(_organization))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _service.Register(_organization);
            }
        }

        [Fact]
        public async Task Register_OrganizationWithGivenIdentity_AlreadyExist_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Has(It.Is<ByIdentitySpecification>(s => s.Identity == _organization.Identity)))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.ThrowsAsync<DuplicateIdentityException>(() => _service.Register(_organization));

            //Assert
            Assert.Equal(_organization.Identity.ToString(), e.Identity);
        }
    }
}
