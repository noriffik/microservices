﻿using NexCore.ContactManager.Domain.Dictionaries;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Organizations
{
    public class JobTest
    {
        [Fact]
        public void Ctor()
        {
            //Arrange
            var title = "job";

            //Act 
            var job = new Job(title);

            //Assert
            Assert.Equal(job.Title, title);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Ctor_GivenTitle_IsNullOrEmptyString_Throws(string title)
        {
            //Assert
            var e = Assert.Throws<ArgumentException>(() =>
            {
                var unused = new Job(title);
            });

            Assert.Equal("title", e.Message);
        }
    }
}
