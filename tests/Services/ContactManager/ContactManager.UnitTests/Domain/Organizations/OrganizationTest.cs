﻿using AutoFixture;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.Legal;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Organizations
{
    public class OrganizationTest
    {
        private readonly Fixture _fixture = new Fixture();
        private readonly Organization _organization;
        private readonly string _name;
        private readonly Identity _identity;

        public OrganizationTest()
        {
            _organization = _fixture.Create<Organization>();
            _name = _fixture.Create<string>();
            _identity = _fixture.Create<Identity>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var organization = new Organization(_name, _identity);

            //Assert
            Assert.Equal(_name, organization.Name);
            Assert.Equal(_identity, organization.Identity);
            Assert.NotNull(organization.Contact);
            Assert.Equal(OrganizationRequisites.Empty, organization.Requisites);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Ctor_GivenName_IsNullOrEmpty_Throws(string name)
        {
            Assert.Throws<ArgumentException>("name", () => new Organization(name, _identity));
        }

        [Fact]
        public void Ctor_GivenIdentity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("identity", () => new Organization(_name, null));
        }

        [Fact]
        public void SetName()
        {
            //Act
            _organization.Name = "Other";

            //Assert
            Assert.Equal("Other", _organization.Name);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void SetName_GivenValue_IsNullOrEmpty_Throws(string value)
        {
            Assert.Throws<ArgumentException>("value", () => _organization.Name = value);
        }

        [Fact]
        public void SetIdentity()
        {
            //Arrange
            var value = Identity.WithIpn("code");

            //Act
            _organization.Identity = value;

            //Assert
            Assert.Equal(value, _organization.Identity);
        }

        [Fact]
        public void SetIdentity_GivenValue_IsNullOrEmpty_Throws()
        {
            Assert.Throws<ArgumentNullException>("value", () => _organization.Identity = null);
        }

        [Fact]
        public void ChangeRequisites()
        {
            //Arrange
            var requisites = _fixture.Create<OrganizationRequisites>();

            //Act
            _organization.ChangeRequisites(requisites);

            //Assert
            Assert.Equal(requisites, _organization.Requisites);
        }

        [Fact]
        public void ChangeRequisites_GivenRequisites_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("requisites", () => _organization.ChangeRequisites(null));
        }
    }
}
