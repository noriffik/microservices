﻿using NexCore.ContactManager.Domain.Contacts;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Contacts
{
    public class PhysicalAddressBuilderTest
    {
        [Fact]
        public void Build()
        {
            Assert.NotNull(PhysicalAddress.Build());
        }

        [Fact]
        public void Create()
        {
            //Arrange
            var builder = PhysicalAddress.Build()
                .WithStreet("Street")
                .WithCity("City", "CityCode")
                .WithRegion("Region", "RegionCode")
                .WithCountry("Country", "CountryCode")
                .WithGeolocation(1, 2)
                .WithPostCode("PostCode");

            //Act
            var address = builder.Create();

            //Assert
            Assert.NotNull(address);
            Assert.Equal(new PhysicalAddressElement("Street"), address.Street);
            Assert.Equal(new PhysicalAddressCodedElement("CityCode", "City"), address.City);
            Assert.Equal(new PhysicalAddressCodedElement("RegionCode", "Region"), address.Region);
            Assert.Equal(new PhysicalAddressCodedElement("CountryCode", "Country"), address.Country);
            Assert.Equal(new Geolocation(1, 2), address.Geolocation);
            Assert.Equal(new PhysicalAddressElement("PostCode"), address.PostCode);
        }

        [Fact]
        public void Create_WithoutConfiguration()
        {
            //Act
            var address = PhysicalAddress.Build().Create();

            //Assert
            Assert.NotNull(address);
            Assert.Equal(PhysicalAddressElement.Empty, address.Street);
            Assert.Equal(PhysicalAddressCodedElement.Empty, address.City);
            Assert.Equal(PhysicalAddressCodedElement.Empty, address.Region);
            Assert.Equal(PhysicalAddressCodedElement.Empty, address.Country);
            Assert.Equal(Geolocation.Empty, address.Geolocation);
            Assert.Equal(PhysicalAddressElement.Empty, address.PostCode);
        }
    }
}
