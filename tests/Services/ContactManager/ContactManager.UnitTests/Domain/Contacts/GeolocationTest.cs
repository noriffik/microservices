﻿using NexCore.ContactManager.Domain.Contacts;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Contacts
{
    public class GeolocationTest
    {
        private readonly double _latitude = 1.7;
        private readonly double _longitude = 1.2;

        [Fact]
        public void Ctor()
        {
            //Act
            var geolocation = new Geolocation(_latitude, _longitude);

            //Assert
            Assert.Equal(_latitude, geolocation.Latitude);
            Assert.Equal(_longitude, geolocation.Longitude);
        }

        [Fact]
        public void Ctor_GivenLatitude_IsNull_WhenLongitudeIsSet_Throws()
        {
            var e = Assert.Throws<InvalidOperationException>(() =>
            {
                var unused = new Geolocation(null, _longitude);
            });
            Assert.Contains("coordinates", e.Message);
        }

        [Fact]
        public void Ctor_GivenLongitude_IsNull_WhenLatitudeIsSet_Throws()
        {
            var e = Assert.Throws<InvalidOperationException>(() =>
            {
                var unused = new Geolocation(_latitude, null);
            });
            Assert.Contains("coordinates", e.Message);
        }

        [Fact]
        public void Ctor_GivenLongitude_AndLatitude_IsNull_ReturnsEmpty()
        {
            //Act
            var empty = new Geolocation(null, null);

            //Assert
            Assert.Null(empty.Latitude);
            Assert.Null(empty.Longitude);
        }

        [Fact]
        public void Empty_ReturnsEmptyGeolocation()
        {
            //Act
            var empty = Geolocation.Empty;

            //Assert
            Assert.NotNull(empty);
            Assert.Null(empty.Latitude);
            Assert.Null(empty.Longitude);
            Assert.Equal(empty, Geolocation.Empty);
        }

        [Fact]
        public void ToString_ReturnsValue()
        {
            Assert.Equal($"{_latitude}, {_longitude}", new Geolocation(_latitude, _longitude).ToString());
            Assert.Equal(string.Empty, Geolocation.Empty.ToString());
        }


        [Fact]
        public void Assign()
        {
            //Arrange
            var expected = new Geolocation(_latitude, _longitude);
            var actual = new Geolocation(1, 1);

            //Act
            actual.Assign(expected);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Assign_GivenElement_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("geolocation", () => Geolocation.Empty.Assign(null));
        }
    }
}
