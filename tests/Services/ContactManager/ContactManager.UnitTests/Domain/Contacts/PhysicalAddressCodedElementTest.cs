﻿using NexCore.ContactManager.Domain.Contacts;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Contacts
{
    public class PhysicalAddressCodedElementTest
    {
        [Fact]
        public void Ctor_WithValue()
        {
            //Act
            var element = new PhysicalAddressCodedElement("value");

            //Assert
            Assert.Null(element.Code);
            Assert.Equal("value", element.Value);
        }

        [Fact]
        public void Ctor_WithCode_AndValue()
        {
            //Act
            var element = new PhysicalAddressCodedElement("code", "value");

            //Assert
            Assert.Equal("code", element.Code);
            Assert.Equal("value", element.Value);
        }

        [Fact]
        public void Empty_ReturnsEmptyAddressElement()
        {
            //Act
            var empty = PhysicalAddressCodedElement.Empty;

            //Assert
            Assert.NotNull(empty);
            Assert.Equal(empty, PhysicalAddressCodedElement.Empty);
            Assert.Null(empty.Code);
            Assert.Null(empty.Value);
        }

        [Fact]
        public void ToString_ReturnsValue()
        {
            Assert.Equal(string.Empty, PhysicalAddressCodedElement.Empty.ToString());
            Assert.Equal("value", new PhysicalAddressCodedElement("value").ToString());
            Assert.Equal("value", new PhysicalAddressCodedElement("code", "value").ToString());
        }

        [Fact]
        public void Assign()
        {
            //Arrange
            var expected = new PhysicalAddressCodedElement("expectedCode", "expectedValue");
            var actual = new PhysicalAddressCodedElement("actualCode", "actualValue");

            //Act
            actual.Assign(expected);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Assign_GivenElement_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("element", () => PhysicalAddressCodedElement.Empty.Assign(null));
        }
    }
}
