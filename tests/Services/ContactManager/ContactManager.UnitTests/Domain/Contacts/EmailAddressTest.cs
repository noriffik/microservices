﻿using NexCore.ContactManager.Domain.Contacts;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Contacts
{
    public class EmailAddressTest
    {
        private const string Address = "address@host.com";

        [Fact]
        public void Ctor()
        {
            //Act
            var address = new EmailAddress(Address);

            //Assert
            Assert.Equal(Address, address.Address);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Ctor_GivenAddress_IsNullOrEmpty_Throws(string address)
        {
            //Assert
            Assert.Throws<ArgumentException>("address", () =>
            {
                var unused = new EmailAddress(address);
            });
        }

        [Fact]
        public void Create()
        {
            //Act
            var address = EmailAddress.Create(Address);

            //Assert
            Assert.Equal(Address, address.Address);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Create_GivenAddress_IsNullOrEmpty_Throws(string address)
        {
            //Assert
            Assert.Throws<ArgumentException>("address", () =>
            {
                EmailAddress.Create(address);
            });
        }

        [Fact]
        public void ToString_ReturnsAddress()
        {
            //Arrange
            var address = EmailAddress.Create(Address);

            //Assert
            Assert.Equal(address.Address, address.ToString());
        }
    }
}
