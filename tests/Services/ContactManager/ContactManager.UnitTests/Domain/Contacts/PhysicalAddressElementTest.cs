﻿using NexCore.ContactManager.Domain.Contacts;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Contacts
{
    public class PhysicalAddressElementTest
    {
        [Fact]
        public void Ctor()
        {
            //Act
            var element = new PhysicalAddressElement("value");

            //Assert
            Assert.Equal("value", element.Value);
        }

        [Fact]
        public void Empty_ReturnsEmptyAddressElement()
        {
            //Act
            var empty = PhysicalAddressElement.Empty;

            //Assert
            Assert.NotNull(empty);
            Assert.Equal(empty, PhysicalAddressElement.Empty);
            Assert.Null(empty.Value);
        }

        [Fact]
        public void Assign()
        {
            //Arrange
            var expected = new PhysicalAddressElement("expected");
            var actual = new PhysicalAddressElement("actual");

            //Act
            actual.Assign(expected);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Assign_GivenElement_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("element", () => PhysicalAddressElement.Empty.Assign(null));
        }
    }
}
