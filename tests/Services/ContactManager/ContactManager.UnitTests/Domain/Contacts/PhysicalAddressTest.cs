﻿using NexCore.ContactManager.Domain.Contacts;
using NexCore.Testing;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Contacts
{
    public class PhysicalAddressTest
    {
        private const string Street = "Pobedy Ave 67";
        private const string CityCode = "CityCode";
        private const string CityName = "Kiev";
        private const string RegionCode = "RegionCode";
        private const string RegionName = "Kiev City";
        private const string CountryCode = "CountryCode";
        private const string CountryName = "Ukraine";
        private const double Latitude = 1.7;
        private const double Longitude = 1.8;
        private const string PostCode = "03113";

        [Fact]
        public void Ctor()
        {
            //Act
            var address = new PhysicalAddress(
                Street,
                CityCode, CityName,
                RegionCode, RegionName,
                CountryCode, CountryName,
                Latitude, Longitude,
                PostCode);

            //Assert
            Assert.Equal(new PhysicalAddressElement(Street), address.Street);
            Assert.Equal(new PhysicalAddressCodedElement(CityCode, CityName), address.City);
            Assert.Equal(new PhysicalAddressCodedElement(RegionCode, RegionName), address.Region);
            Assert.Equal(new PhysicalAddressCodedElement(CountryCode, CountryName), address.Country);
            Assert.Equal(new Geolocation(Latitude, Longitude), address.Geolocation);
            Assert.Equal(new PhysicalAddressElement(PostCode), address.PostCode);
        }

        [Fact]
        public void Copy_Ctor()
        {
            //Arrange
            var address = new PhysicalAddress(
                Street,
                CityCode, CityName,
                RegionCode, RegionName,
                CountryCode, CountryName,
                Latitude, Longitude,
                PostCode);

            //Act
            var result = new PhysicalAddress(address);

            //Assert
            Assert.Equal(address, result, PropertyComparer<PhysicalAddress>.Instance);
            Assert.NotSame(address, result);
        }

        [Fact]
        public void Copy_Ctor_GivenPhysicalAddress_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("physicalAddress", () => new PhysicalAddress(null));
        }

        [Fact]
        public void Empty()
        {
            //Act
            var result = PhysicalAddress.Empty;

            //Assert
            Assert.NotNull(result.City);
            Assert.NotNull(result.Country);
            Assert.NotNull(result.Geolocation);
            Assert.NotNull(result.PostCode);
            Assert.NotNull(result.Region);
            Assert.NotNull(result.Street);
        }

        [Fact]
        public void Assign()
        {
            var actual = PhysicalAddress.Empty;
            var expected = new PhysicalAddress(Street,
                CityCode, CityName,
                RegionCode, RegionName,
                CountryCode, CountryName,
                Latitude, Longitude,
                PostCode);

            //Act
            actual.Assign(expected);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Assign_GivenAddress_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("address", () => PhysicalAddress.Empty.Assign(null));
        }
    }
}
