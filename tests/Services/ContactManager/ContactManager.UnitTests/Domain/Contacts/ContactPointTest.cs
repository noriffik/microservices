﻿using NexCore.ContactManager.Domain.Contacts;
using NexCore.Testing;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Contacts
{
    public class ContactPointTest
    {
        private readonly TestValueObject _value = new TestValueObject();

        [Fact]
        public void ContactPoint()
        {
            //Act
            var point = new ContactPoint<TestValueObject>(_value);

            //Assert
            Assert.Null(point.Description);
            Assert.Equal(_value, point.Value);
            Assert.False(point.IsPrimary);
        }

        [Fact]
        public void ContactPoint_GivenValue_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new ContactPoint<TestValueObject>(null);
            });
        }

        [Fact]
        public void ToString_ReturnsValues_ToStringResult()
        {
            //Arrange
            var email = new EmailAddress("mail@host.com");
            var point = new ContactPoint<EmailAddress>(email);

            //Assert
            Assert.Equal(email.ToString(), point.ToString());
        }
    }
}
