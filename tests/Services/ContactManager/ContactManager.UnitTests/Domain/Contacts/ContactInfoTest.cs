﻿using AutoFixture;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.Domain;
using NexCore.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Contacts
{
    public class ContactInfoTest
    {
        private readonly Fixture _fixture = new Fixture();

        [Fact]
        public void ContactInfo()
        {
            //Act
            var info = new ContactInfo();

            //Assert
            Assert.Empty(info.PhysicalAddresses);
            Assert.Empty(info.EmailAddresses);
            Assert.Empty(info.Telephones);
        }

        [Fact]
        public void ContactInfo_WithId()
        {
            //Act
            var info = new ContactInfo(1);

            //Assert
            Assert.Equal(1, info.Id);
            Assert.Empty(info.PhysicalAddresses);
            Assert.Empty(info.EmailAddresses);
            Assert.Empty(info.Telephones);
        }

        [Fact]
        public void Add()
        {
            //Arrange
            var address = _fixture.Create<PhysicalAddress>();
            var info = new ContactInfo();

            //Act
            info.Add(address, "description");

            //Assert
            Assert.Single(info.PhysicalAddresses);
            Assert.Equal("description", info.PhysicalAddresses.ToList()[0].Description);
            Assert.Equal(address, info.PhysicalAddresses.ToList()[0].Value);
            Assert.True(info.PhysicalAddresses.ToList()[0].IsPrimary);
        }

        [Fact]
        public void Add_GivenValue_AlreadyExists_Throws()
        {
            //Arrange
            var address = _fixture.Create<PhysicalAddress>();
            var info = new ContactInfo();
            info.Add(address, "description");

            //Assert
            Assert.Throws<DuplicateContactPointException>(() =>
            {
                info.Add(address, "another");
            });
        }

        [Fact]
        public void Add_GivenIsPrimary_IsTrue()
        {
            //Arrange
            var expected = new[] { false, false, false, true };
            var info = BuildContactInfoWith<PhysicalAddress>(3);

            //Act
            info.Add(_fixture.Create<PhysicalAddress>(), _fixture.Create<string>(), true);

            //Assert
            Assert.Equal(expected, info.GetContactPoints<PhysicalAddress>().Select(c => c.IsPrimary));
        }

        private ContactInfo BuildContactInfoWith<TValue>(int count) where TValue : ValueObject
        {
            var info = new ContactInfo();
            FillContactInfo<TValue>(info, count);

            return info;
        }

        private IList<TValue> FillContactInfo<TValue>(ContactInfo info, int count) where TValue : ValueObject
        {
            var values = _fixture.CreateMany<TValue>(count).ToList();
            values.ForEach(v => info.Add(v, _fixture.Create<string>()));

            return values;
        }

        [Fact]
        public void Add_GivenIsPrimary_IsFalse()
        {
            //Arrange
            var expected = new[] { true, false, false, false };
            var info = BuildContactInfoWith<PhysicalAddress>(3);

            //Act
            info.Add(_fixture.Create<PhysicalAddress>(), _fixture.Create<string>());

            //Assert
            Assert.Equal(expected, info.GetContactPoints<PhysicalAddress>().Select(c => c.IsPrimary));
        }

        [Fact]
        public void Add_GivenType_IsUnsupported_Throws()
        {
            //Arrange
            var info = new ContactInfo();

            //Assert
            Assert.Throws<NotSupportedException>(() => { info.Add(new TestValueObject(), "description"); });
        }

        [Fact]
        public void Add_GivenValue_IsNull_Throws()
        {
            //Arrange
            var info = new ContactInfo();

            //Assert
            Assert.Throws<ArgumentNullException>("value", () => { info.Add<Telephone>(null, "description"); });
        }

        [Fact]
        public void GetContactPoints()
        {
            //Arrange
            var address = _fixture.Create<PhysicalAddress>();
            var info = new ContactInfo();
            info.Add(address, "description");

            //Act
            var contactPoints = info.GetContactPoints<PhysicalAddress>().ToList();

            //Assert
            Assert.Single(contactPoints);
            Assert.Equal("description", contactPoints[0].Description);
            Assert.Equal(address, contactPoints[0].Value);
        }

        [Fact]
        public void GetContactPoints_GivenType_IsUnsupported_Throws()
        {
            //Arrange
            var info = new ContactInfo();

            //Assert
            Assert.Throws<NotSupportedException>(() => { info.GetContactPoints<ValueObject>(); });
        }

        [Fact]
        public void GetPhysicalAddresses()
        {
            TestContactPointsCollections(i => i.PhysicalAddresses);
        }

        [Fact]
        public void GetTelephones()
        {
            TestContactPointsCollections(i => i.Telephones);
        }

        [Fact]
        public void GetEmailAddresses()
        {
            TestContactPointsCollections(i => i.EmailAddresses);
        }

        private void TestContactPointsCollections<TValue>(
            Func<ContactInfo, IEnumerable<IContactPoint<TValue>>> property) where TValue : ValueObject
        {
            //Arrange
            var value = _fixture.Create<TValue>();
            var info = new ContactInfo();
            info.Add(value, "description");

            //Assert
            Assert.Equal(info.GetContactPoints<TValue>(), property(info));
        }

        [Fact]
        public void GetContactPoints_WhenEmpty_ForGivenType_ReturnsEmpty()
        {
            //Act
            var info = new ContactInfo();

            //Assert
            Assert.Empty(info.GetContactPoints<PhysicalAddress>());
        }

        [Fact]
        public void SetPrimary()
        {
            //Arrange
            var expected = new[]
            {
                false, false, false,
                false, false, false,
                true, false, false,
                false
            };
            var info = new ContactInfo();
            var values = FillContactInfo<PhysicalAddress>(info, 10);

            //Act
            info.SetPrimary(values[6]);

            //Assert
            Assert.Equal(expected, info.GetContactPoints<PhysicalAddress>().Select(c => c.IsPrimary));
        }

        [Fact]
        public void SetPrimary_GivenType_IsUnsupported_Throws()
        {
            //Arrange
            var info = new ContactInfo();

            //Assert
            Assert.Throws<NotSupportedException>(() => { info.SetPrimary(new TestValueObject()); });
        }

        [Fact]
        public void SetPrimary_GivenValue_DoesNotExist()
        {
            //Arrange
            var info = new ContactInfo();

            //Assert
            var e = Assert.Throws<InvalidOperationException>(() =>
            {
                info.SetPrimary(_fixture.Create<EmailAddress>());
            });
            Assert.Equal("Value does not exist.", e.Message);
        }

        [Fact]
        public void SetPrimary_GivenValue_IsNull_Throws()
        {
            //Arrange
            var info = new ContactInfo();

            //Assert
            Assert.Throws<ArgumentNullException>(() =>
            {
                info.SetPrimary<Telephone>(null);
            });
        }

        [Fact]
        public void GetPrimary()
        {
            //Arrange
            var info = new ContactInfo();
            var values = FillContactInfo<Telephone>(info, 10);
            info.SetPrimary(values[7]);
            var expected = info.Find(values[7]);

            //Assert
            Assert.Equal(expected, info.GetPrimary<Telephone>());
        }

        [Fact]
        public void GetPrimary_WhenValues_AreNotSet_ReturnsNull()
        {
            //Arrange
            var info = new ContactInfo();

            //Assert
            Assert.Null(info.GetPrimary<Telephone>());
        }

        [Fact]
        public void Find()
        {
            //Arrange
            var info = new ContactInfo();
            var values = FillContactInfo<Telephone>(info, 10);
            var expected = info.GetContactPoints<Telephone>().ToList()[3];

            //Act
            var actual = info.Find(values[3]);

            //Assert
            Assert.NotNull(actual);
            Assert.Equal(expected, actual);
        }

        
        [Fact]
        public void Find_GivenValue_DoesNotExist_ReturnsNull()
        {
            //Arrange
            var info = new ContactInfo();
            var telephone = _fixture.Create<Telephone>();

            //Act 
            var result = info.Find(telephone);

            //Assert
            Assert.Null(result);
        }
        
        [Fact]
        public void Replace()
        {
            //Arrange
            var info = new ContactInfo();
            var values = FillContactInfo<Telephone>(info, 10);
            var anotherValue = _fixture.Create<Telephone>();
            var expectedPoint = info.GetContactPoints<Telephone>().ToList()[3];
            
            //Act
            info.Replace(values[3], anotherValue);

            //Assert
            var point = info.GetContactPoints<Telephone>().ToList()[3];
            Assert.NotNull(point);
            Assert.Equal(expectedPoint.Description, point.Description);
            Assert.Equal(anotherValue, point.Value);
        }
        
        [Fact]
        public void Replace_GivenExisting_IsNull_Throws()
        {
            //Arrange
            var info = new ContactInfo();
            var value = _fixture.Create<Telephone>();
            
            //Assert
            Assert.Throws<ArgumentNullException>(() => { info.Replace(null, value); });
        }

        [Fact]
        public void Replace_GivenExisting_DoesNotExist_Throws()
        {
            //Arrange
            var info = new ContactInfo();
            var nonExisting = _fixture.Create<EmailAddress>();

            //Assert
            var e = Assert.Throws<InvalidOperationException>(() =>
            {
                info.Replace(nonExisting, nonExisting);
            });
            Assert.Equal("Value does not exist.", e.Message);
        }

        [Fact]
        public void Replace_GivenWith_IsNull_Throws()
        {
            //Arrange
            var info = new ContactInfo();
            var value = _fixture.Create<Telephone>();

            //Assert
            Assert.Throws<ArgumentNullException>(() => { info.Replace(value, null); });
        }

        [Fact]
        public void Replace_GivenWith_AlreadyExists_Throws()
        {
            //Arrange
            var info = new ContactInfo();
            var beingReplaced = _fixture.Create<EmailAddress>();
            var replaceWith = _fixture.Create<EmailAddress>();
            info.Add(beingReplaced, string.Empty);
            info.Add(replaceWith, string.Empty);

            //Assert
            var e = Assert.Throws<InvalidOperationException>(() =>
            {
                info.Replace(beingReplaced, replaceWith);
            });
            Assert.Equal("Replacing value already exists.", e.Message);
        }

        [Fact]
        public void Has()
        {
            //Arrange
            var existing = _fixture.Create<Telephone>();
            var nonExisting = _fixture.Create<Telephone>();
            var info = new ContactInfo();
            info.Add(existing, string.Empty);

            //Assert
            Assert.True(info.Has(existing));
            Assert.False(info.Has(nonExisting));
        }

        [Fact]
        public void Has_GivenType_IsUnsupported_Throws()
        {
            //Arrange
            var info = new ContactInfo();

            //Assert
            Assert.Throws<NotSupportedException>(() => { info.Has(new TestValueObject()); });
        }

        [Fact]
        public void Has_GivenValue_IsNull_Throws()
        {
            //Arrange
            var info = new ContactInfo();

            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                info.Has<Telephone>(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Fact]
        public void Remove_GivenValue_Existed_ReturnsTrue()
        {
            //Arrange
            var info = new ContactInfo();
            var values = FillContactInfo<Telephone>(info, 10);
            var expected = info.Find(values[5]);

            //Act
            var result = info.Remove(values[5]);

            //Assert
            Assert.True(result);
            Assert.DoesNotContain(expected, info.GetContactPoints<Telephone>());
        }

        [Fact]
        public void Remove_GivenValue_DidNotExist_ReturnsFalse()
        {
            //Arrange
            var info = new ContactInfo();

            //Act
            var result = info.Remove(_fixture.Create<PhysicalAddress>());

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void Remove_GivenValue_IsNull_Throws()
        {
            //Arrange
            var info = new ContactInfo();

            //Assert
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                info.Remove<EmailAddress>(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Fact]
        public void Remove_GivenValue_IsPrimary_Sets_FirstValue_AsPrimary()
        {
            //Arrange
            var expected = new[] { true, false, false, false };
            var info = new ContactInfo();
            var values = FillContactInfo<EmailAddress>(info, 5);
            info.SetPrimary(values[3]);

            //Act
            info.Remove(values[3]);

            //Assert
            Assert.Equal(expected, info.GetContactPoints<EmailAddress>().Select(c => c.IsPrimary));
        }

        [Fact]
        public void Remove_GivenValue_IsNotPrimary_DoesNotChangePrimary()
        {
            //Arrange
            var expected = new[] { false, true };
            var info = new ContactInfo();
            var values = FillContactInfo<EmailAddress>(info, 3);
            info.SetPrimary(values[1]);

            //Act
            info.Remove(values[2]);

            //Assert
            Assert.Equal(expected, info.GetContactPoints<EmailAddress>().Select(c => c.IsPrimary));
        }
    }
}
