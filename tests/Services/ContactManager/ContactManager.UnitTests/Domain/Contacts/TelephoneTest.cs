﻿using NexCore.ContactManager.Domain.Contacts;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Contacts
{
    public class TelephoneTest
    {
        public static readonly string CountryCode = "000";
        public static readonly string AreaCode = "111";
        public static readonly string LocalNumber = "1112233";

        [Fact]
        public void Ctor()
        {
            //Act
            var telephone = new Telephone(CountryCode, AreaCode, LocalNumber, TelephoneType.Mobile);

            //Assert
            Assert.Equal(CountryCode, telephone.CountryCode);
            Assert.Equal(AreaCode, telephone.AreaCode);
            Assert.Equal(LocalNumber, telephone.LocalNumber);
            Assert.Equal(TelephoneType.Mobile, telephone.Type);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Ctor_GivenCountryCode_IsNullOrEmpty_Throws(string countryCode)
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var unused = new Telephone(countryCode, AreaCode, LocalNumber);
            });
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Ctor_GivenAreaCode_IsNullOrEmpty_Throws(string areaCode)
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var unused = new Telephone(CountryCode, areaCode, LocalNumber);
            });
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Ctor_GivenLocalNumber_IsNullOrEmpty_Throws(string localNumber)
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var unused = new Telephone(CountryCode, AreaCode, localNumber);
            });
        }

        [Fact]
        public void AsNumber_ReturnsConcatenatedValues()
        {
            //Arrange
            var telephone = new Telephone("380", "44", "1112233");

            //Assert
            Assert.Equal("380441112233", telephone.Number);
        }

        [Fact]
        public void ToString_ReturnsNumber()
        {
            //Arrange
            var telephone = new Telephone("380", "44", "1112233");

            //Assert
            Assert.Equal(telephone.Number, telephone.ToString());
        }

        [Fact]
        public void AsE123()
        {
            //Arrange
            var telephone = new Telephone("380", "44", "1112233");

            //Assert
            Assert.Equal("+380 44 1112233", telephone.E123);
        }

        [Fact]
        public void ChangeType()
        {
            //Arrange
            var telephone = new Telephone(
                CountryCode, AreaCode, LocalNumber, TelephoneType.Landline);

            //Act
            var copy = telephone.ChangeType(TelephoneType.Mobile);

            //Assert
            Assert.NotNull(copy);
            Assert.NotSame(telephone, copy);
            Assert.Equal(TelephoneType.Mobile, copy.Type);
        }

        [Fact]
        public void CopyCtor()
        {
            //Arrange
            var telephone = new Telephone(
                CountryCode, AreaCode, LocalNumber, TelephoneType.Landline);

            //Act
            var result = new Telephone(telephone);

            //Assert
            Assert.Equal(telephone, result);
        }

        [Fact]
        public void CopyCtor_GivenTelephone_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("telephone", () =>
            {
                var unused = new Telephone(null);
            });
        }
    }
}
