﻿using AutoFixture;
using NexCore.ContactManager.Domain.Languages;
using NexCore.Testing;
using System;
using System.Globalization;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Languages
{
    public class LanguageTest
    {
        private Fixture _fixture = new Fixture();

        [Fact]
        public void Ctor()
        {
            //Arrange
            _fixture = new Fixture();
            var expected = _fixture.Create<Language>();

            //Act
            var actual = new Language(expected.ThreeLetterIsoCode, expected.TwoLetterIsoCode, expected.Name);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<Language>.Instance);
        }

        [Fact]
        public void Ctor_GivenThreeLetterIsoCode_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new Language(null, _fixture.Create<string>(), _fixture.Create<string>()));
        }

        [Fact]
        public void Ctor_GivenTwoLetterIsoCode_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new Language(_fixture.Create<string>(), null, _fixture.Create<string>()));
        }

        [Fact]
        public void Ctor_GivenName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new Language(_fixture.Create<string>(), _fixture.Create<string>(), null));
        }

        [Fact]
        public void From()
        {
            //Arrange
            var cultureInfo = _fixture.Create<CultureInfo>();

            //Act
            var result = Language.From(cultureInfo);

            //Assert
            Assert.Equal(result.Name, cultureInfo.Name);
            Assert.Equal(result.ThreeLetterIsoCode, cultureInfo.ThreeLetterISOLanguageName);
            Assert.Equal(result.TwoLetterIsoCode, cultureInfo.TwoLetterISOLanguageName);
        }

        [Fact]
        public void From_GivenCultureInfo_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => Language.From(null));
        }
    }
}
