﻿using NexCore.ContactManager.Domain.Dealers;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Dealers
{
    public class UnitsBuilderTest
    {
        [Fact]
        public void Create()
        {
            //Arrange
            var builder = Units.Build()?.HasService()?.HasSales();

            //Act
            var units = builder?.Create();

            //Assert
            Assert.NotNull(units);
            Assert.True(units.Service);
            Assert.True(units.Sales);
        }
    }
}
