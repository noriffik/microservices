﻿using NexCore.ContactManager.Domain.Dealers;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Dealers
{
    public class UnitsTest
    {
        [Fact]
        public void Ctor()
        {
            //Act
            var unit = new Units(true, true);

            //Assert
            Assert.True(unit.Sales);
            Assert.True(unit.Service);
        }

        [Fact]
        public void Ctor_GivenAll_Units_AreFalse_Throws()
        {
            var error = Assert.Throws<InvalidOperationException>(() =>
            {
                var unused = new Units(false, false);
            });

            Assert.Equal("At least one of unit required", error.Message);
        }

        [Fact]
        public void Empty_ReturnsEmptyUnit()
        {
            //Act
            var empty = Units.Empty;

            //Assert
            Assert.NotNull(empty);
        }

        [Fact]
        public void ToString_Returns_UnitTrueFields()
        {
            Assert.Equal("Sales, Service", new Units(true, true).ToString());
            Assert.Equal("Sales", new Units(true, false).ToString());
            Assert.Equal("Service", new Units(false, true).ToString());
        }
    }
}
