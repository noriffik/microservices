﻿using AutoFixture;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Dealers
{
    public class DealerTest
    {
        private readonly DealerId _id;
        private readonly int _organizationId;

        private readonly Dealer _dealer;

        public DealerTest()
        {
            var fixture = new Fixture()
                .Customize(new DealerIdCustomization());

            _organizationId = fixture.Create<int>();
            _id = fixture.Create<DealerId>();
            _dealer = new Dealer(_id, _organizationId);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var dealer = new Dealer(_id, _organizationId);

            //Assert
            Assert.Equal(_organizationId, dealer.OrganizationId);
            Assert.Equal(_id, dealer.Id.Value);
            Assert.Equal(_id.DistributorId, dealer.DistributorId);
            Assert.Equal(_id.DealerCode, dealer.DealerCode);
            Assert.Null(_dealer.CityCode);
        }

        [Fact]
        public void Ctor_GivenId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("id", () => new Dealer(null, _organizationId));
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("CityCode")]
        public void ChangeCityCode(string cityCode)
        {
            //Act
            _dealer.ChangeCityCode(cityCode);

            //Assert
            Assert.Equal(cityCode, _dealer.CityCode);
        }
    }
}
