﻿using AutoFixture;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Dealers.Specifications;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Testing.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Dealers.Specifications
{
    public class ByDistributorIdSpecificationTest
    {
        private readonly DistributorId _distributorId;

        //Entities
        private readonly IReadOnlyList<Dealer> _expected;
        private readonly IReadOnlyList<Dealer> _unexpected;
        private readonly IReadOnlyList<Dealer> _all;

        //Specification
        private readonly ByDistributorIdSpecification _specification;

        public ByDistributorIdSpecificationTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            _distributorId = fixture.Create<DistributorId>();

            //Entities
            _expected = fixture.CreateMany<DealerCode>()
                .Select(dealerCode => DealerId.Next(_distributorId, dealerCode))
                .Select(id => fixture.Construct<Dealer, DealerId>(id))
                .ToList();
            _unexpected = fixture.CreateMany<Dealer>(5).ToList();
            _all = _unexpected.Concat(_expected).ToList();

            //Specification
            _specification = new ByDistributorIdSpecification(_distributorId);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var specification = new ByDistributorIdSpecification(_distributorId);

            //Assert
            Assert.Equal(_distributorId, specification.DistributorId);
        }

        [Fact]
        public void Ctor_GivenDistributorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("distributorId", () => new ByDistributorIdSpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
