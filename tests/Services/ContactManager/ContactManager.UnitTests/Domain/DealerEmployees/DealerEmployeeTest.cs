﻿using AutoFixture;
using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.DealerEmployees
{
    public class DealerEmployeeTest
    {
        private readonly int _id;
        private readonly int _individualId;
        private readonly int _organizationId;
        private readonly DealerId _dealerId;
        private readonly int _jobId;

        public DealerEmployeeTest()
        {
            var fixture = new Fixture()
                .Customize(new DealerIdCustomization());

            _id = fixture.Create<int>();
            _individualId = fixture.Create<int>();
            _organizationId = fixture.Create<int>();
            _dealerId = fixture.Create<DealerId>();
            _jobId = fixture.Create<int>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var employee = new DealerEmployee(_individualId, _organizationId, _dealerId, _jobId);

            //Assert
            Assert.Equal(_individualId, employee.IndividualId);
            Assert.Equal(_organizationId, employee.OrganizationId);
            Assert.Equal(_dealerId, employee.DealerId);
            Assert.Equal(_jobId, employee.JobId);
        }

        [Fact]
        public void Ctor_WithId()
        {
            //Act
            var employee = new DealerEmployee(_id, _individualId, _organizationId, _dealerId, _jobId);

            //Assert
            Assert.Equal(_id, employee.Id);
            Assert.Equal(_individualId, employee.IndividualId);
            Assert.Equal(_organizationId, employee.OrganizationId);
            Assert.Equal(_dealerId, employee.DealerId);
            Assert.Equal(_jobId, employee.JobId);
        }
    }
}
