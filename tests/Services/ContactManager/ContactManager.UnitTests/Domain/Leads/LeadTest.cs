﻿using AutoFixture;
using NexCore.Common;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Leads
{
    public class LeadTest
    {
        private readonly int _id;
        private readonly DealerId _dealerId;
        private readonly int _topicId;
        private readonly Contact _contact;
        private readonly DateTime _dateTime;

        private readonly Lead _lead;

        public LeadTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            _id = fixture.Create<int>();
            _dealerId = fixture.Create<DealerId>();
            _topicId = fixture.Create<int>();
            _contact = fixture.Create<Contact>();
            _dateTime = fixture.Create<DateTime>();

            _lead = fixture.Create<Lead>();
        }

        [Fact]
        public void Ctor()
        {
            using (new SystemTimeContext(_dateTime))
            {
                //Act
                var lead = new Lead(_dealerId, _contact, _topicId);

                //Assert
                Assert.Equal(default, lead.Id);
                Assert.Equal(_dealerId.DistributorId, lead.DistributorId);
                Assert.Equal(_dealerId.DealerCode, lead.DealerCode);
                Assert.Equal(_dealerId, lead.DealerId);
                Assert.Equal(_topicId, lead.TopicId);
                Assert.Equal(_contact, lead.Contact);
                Assert.Equal(Status.Open, lead.Status);
                Assert.Null(lead.OwnerEmployeeId);
                Assert.Equal(_dateTime, lead.CreatedAt);
                Assert.Equal(_dateTime, lead.UpdatedAt);
            }
        }

        [Fact]
        public void Ctor_WithId()
        {
            using (new SystemTimeContext(_dateTime))
            {
                //Act
                var lead = new Lead(_id, _dealerId, _contact, _topicId);

                //Assert
                Assert.Equal(_id, lead.Id);
                Assert.Equal(_dealerId.DistributorId, lead.DistributorId);
                Assert.Equal(_dealerId.DealerCode, lead.DealerCode);
                Assert.Equal(_dealerId, lead.DealerId);
                Assert.Equal(_topicId, lead.TopicId);
                Assert.Equal(_contact, lead.Contact);
                Assert.Equal(Status.Open, lead.Status);
                Assert.Null(lead.OwnerEmployeeId);
                Assert.Equal(_dateTime, lead.CreatedAt);
                Assert.Equal(_dateTime, lead.UpdatedAt);
            }
        }

        [Fact]
        public void Ctor_GivenDealerId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealerId", () => new Lead(null, _contact, _topicId));
        }

        [Theory]
        [InlineData(10)]
        [InlineData(null)]
        public void AssignOwner(int? employeeId)
        {
            using (new SystemTimeContext(_dateTime))
            {
                //Arrange
                _lead.AssignOwner(1);

                //Act
                _lead.AssignOwner(employeeId);

                //Assert
                Assert.Equal(employeeId, _lead.OwnerEmployeeId);
                Assert.Equal(_dateTime, _lead.UpdatedAt);
            }
        }

        [Fact]
        public void ChangeStatus()
        {
            //Arrange
            const Status expected = Status.Disqualified;

            //Act
            _lead.ChangeStatus(expected);

            //Assert
            Assert.Equal(expected, _lead.Status);
        }
    }
}
