﻿using AutoFixture;
using NexCore.ContactManager.Domain.Leads;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Leads
{
    public class TopicTest
    {
        private readonly int _id;
        private readonly string _name;

        private readonly Topic _topic;

        public TopicTest()
        {
            var fixture = new Fixture();

            _id = fixture.Create<int>();
            _name = fixture.Create<string>();

            _topic = fixture.Create<Topic>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var topic = new Topic(_name);

            //Assert
            Assert.Equal(default, topic.Id);
            Assert.Equal(_name, topic.Name);
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void Ctor_GivenName_IsNullOrEmpty(string name)
        {
            //Act
            var e = Assert.Throws<ArgumentException>("name", () => new Topic(name));

            //Assert
            Assert.Contains("cannot be null or empty", e.Message);
        }

        [Fact]
        public void Ctor_WithId()
        {
            //Act
            var topic = new Topic(_id, _name);

            //Assert
            Assert.Equal(_id, topic.Id);
            Assert.Equal(_name, topic.Name);
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void Ctor_WithId_GivenName_IsNullOrEmpty(string name)
        {
            //Act
            var e = Assert.Throws<ArgumentException>("name", () => new Topic(_id, name));

            //Assert
            Assert.Contains("cannot be null or empty", e.Message);
        }

        [Fact]
        public void ChangeName()
        {
            //Act
            _topic.ChangeName(_name);

            //Assert
            Assert.Equal(_name, _topic.Name);
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void ChangeName_GivenName_IsNullOrEmpty(string name)
        {
            //Act
            var e = Assert.Throws<ArgumentException>("name", () => _topic.ChangeName(name));

            //Assert
            Assert.Contains("cannot be null or empty", e.Message);
        }
    }
}
