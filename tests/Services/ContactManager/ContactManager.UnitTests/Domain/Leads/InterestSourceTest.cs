﻿using AutoFixture;
using NexCore.Common;
using NexCore.ContactManager.Domain.Leads;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Leads
{
    public class InterestSourceTest
    {
        private readonly Fixture _fixture;
        private readonly string _name;
        private readonly InterestSource _interest;

        public InterestSourceTest()
        {
            _fixture = new Fixture();
            _name = _fixture.Create<string>();
            _interest = _fixture.Create<InterestSource>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var result = new InterestSource(_name);

            //Assert
            Assert.Equal(_name, result.Name);
            Assert.Null(result.DateArchived);
        }

        [Fact]
        public void Ctor_GivenName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("name", () => new InterestSource(null));
        }

        [Fact]
        public void Ctor_WithId()
        {
            //Arrange
            var id = _fixture.Create<int>();

            //Act
            var result = new InterestSource(id, _name);

            //Assert
            Assert.Equal(id, result.Id);
            Assert.Equal(_name, result.Name);
            Assert.Null(result.DateArchived);
        }

        [Fact]
        public void ChangeName()
        {
            //Act
            _interest.ChangeName(_name);

            //Assert
            Assert.Equal(_name, _interest.Name);
        }

        [Fact]
        public void ChangeName_GivenName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("name", () => _interest.ChangeName(null));
        }

        [Fact]
        public void Ctor_WithId_GivenName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("name", () => new InterestSource(_fixture.Create<int>(), null));
        }

        [Fact]
        public void Archive()
        {
            using (var time = new SystemTimeContext(_fixture.Create<DateTime>()))
            {
                //Act
                _interest.Archive();

                //Assert
                Assert.Equal(time.Now, _interest.DateArchived);
            }
        }

        [Fact]
        public void IsArchived()
        {
            //Arrange
            _interest.Archive();

            //Act
            var result = _interest.IsArchived;

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void IsArchived_ReturnsFalse()
        {
            //Act
            var result = _interest.IsArchived;

            //Assert
            Assert.False(result);
        }
    }
}
