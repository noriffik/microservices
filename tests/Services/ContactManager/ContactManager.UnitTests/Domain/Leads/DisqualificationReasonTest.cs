﻿using AutoFixture;
using NexCore.ContactManager.Domain.Leads;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Leads
{
    public class DisqualificationReasonTest
    {
        private readonly Fixture _fixture;
        private readonly string _name;

        public DisqualificationReasonTest()
        {
            _fixture = new Fixture();
            _name = _fixture.Create<string>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var result = new DisqualificationReason(_name);

            //Assert
            Assert.Equal(_name, result.Name);
        }

        [Fact]
        public void Ctor_GivenName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("name", () => new DisqualificationReason(null));
        }

        [Fact]
        public void Ctor_WithId()
        {
            //Arrange
            var id = _fixture.Create<int>();

            //Act
            var result = new DisqualificationReason(id, _name);

            //Assert
            Assert.Equal(id, result.Id);
            Assert.Equal(_name, result.Name);
        }

        [Fact]
        public void Ctor_WithId_GivenName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("name",
                () => new DisqualificationReason(_fixture.Create<int>(), null));
        }

        [Fact]
        public void ChangeName()
        {
            //Arrange
            var reason = _fixture.Create<DisqualificationReason>();

            //Act
            reason.ChangeName(_name);

            //Assert
            Assert.Equal(_name, reason.Name);
        }

        [Fact]
        public void ChangeName_GivenName_IsNull_Throws()
        {
            //Arrange
            var reason = _fixture.Create<DisqualificationReason>();

            //Assert
            Assert.Throws<ArgumentNullException>("name", () => reason.ChangeName(null));
        }
    }
}
