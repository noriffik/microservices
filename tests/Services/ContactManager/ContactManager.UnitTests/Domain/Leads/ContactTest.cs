﻿using NexCore.ContactManager.Domain.Leads;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Leads
{
    public class ContactTest
    {
        private const int IndividualId = 5;
        private const int OrganizationId = 10;

        [Fact]
        public void Ctor_WhenOrganizationId_IsNotSet()
        {
            //Act
            var contact = new Contact(IndividualId, null);

            //Assert
            Assert.Equal(IndividualId, contact.IndividualId);
            Assert.Null(contact.OrganizationId);
            Assert.Equal(ContactType.Private, contact.Type);
        }

        [Fact]
        public void Ctor_WhenOrganizationId_IsSet()
        {
            //Act
            var contact = new Contact(IndividualId, OrganizationId);

            //Assert
            Assert.Equal(IndividualId, contact.IndividualId);
            Assert.Equal(OrganizationId, contact.OrganizationId);
            Assert.Equal(ContactType.Corporate, contact.Type);
        }
    }
}
