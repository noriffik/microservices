﻿using NexCore.ContactManager.Domain.Individuals;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Individuals
{
    public class CommunicationMediumTest
    {
        [Fact]
        public void Get()
        {
            //Act
            var result = CommunicationMedium.Get(CommunicationMedium.Telephone.Id);

            //Assert
            Assert.Equal(CommunicationMedium.Telephone, result);
        }

        [Fact]
        public void Get_GivenId_IsInvalid_Throws()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => CommunicationMedium.Get("invalid"));
        }

        [Fact]
        public void Has()
        {
            Assert.True(CommunicationMedium.Has("TELEGRAM"));
            Assert.False(CommunicationMedium.Has("INVALID"));
        }

        [Fact]
        public void Equals()
        {
            //Arrange
            var status = CommunicationMedium.Telegram;

            //Assert
            Assert.Equal(status, CommunicationMedium.Telegram);
            Assert.False(status.Equals(null));
            Assert.False(status.Equals(CommunicationMedium.Telephone));
        }

        [Fact]
        public void Hashing()
        {
            //Act
            var hash = CommunicationMedium.Telephone.GetHashCode();

            //Assert
            Assert.Equal(hash, CommunicationMedium.Telephone.GetHashCode());
            Assert.NotEqual(hash, CommunicationMedium.Telegram.GetHashCode());
            Assert.NotEqual(hash, CommunicationMedium.Email.Id.GetHashCode());
        }
    }
}
