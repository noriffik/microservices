﻿using NexCore.ContactManager.Domain.Individuals;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Individuals
{
    public class PersonalPreferencesTest
    {
        private readonly PersonalPreferences _preferences = new PersonalPreferences();

        [Fact]
        public void Ctor()
        {
            //Act
            var preferences = new PersonalPreferences();

            //Assert
            Assert.NotNull(preferences.Communications);
        }

        [Theory]
        [InlineData("ara")]
        [InlineData("bul")]
        [InlineData("cat")]
        [InlineData("zho")]
        [InlineData("ces")]
        [InlineData("dan")]
        [InlineData("deu")]
        [InlineData("ell")]
        [InlineData("eng")]
        [InlineData("spa")]
        [InlineData("fin")]
        public void ChangeLanguage(string language)
        {
            //Act
            _preferences.ChangeLanguage(language);

            //Assert
            Assert.Equal(language, _preferences.Language);
        }

        [Fact]
        public void ChangeLanguage_GivenLanguage_IsNull_Throws()
        {
            //Assert
            Assert.Throws<ArgumentNullException>("language", () => _preferences.ChangeLanguage(null));
        }

        [Fact]
        public void ChangeLanguage_GivenLanguage_IsInvalid_IsoCode_Throws()
        {
            //Act
            var e = Assert.Throws<ArgumentException>("language", () => _preferences.ChangeLanguage("invalid"));

            //Assert
            Assert.Contains("iso", e.Message.ToLowerInvariant());
        }
    }
}
