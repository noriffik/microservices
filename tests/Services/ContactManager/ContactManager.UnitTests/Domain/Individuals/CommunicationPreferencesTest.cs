﻿using NexCore.ContactManager.Domain.Individuals;
using System;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Individuals
{
    public class CommunicationPreferencesTest
    {
        private static readonly CommunicationMedium Medium = CommunicationMedium.Telegram;

        private readonly CommunicationPreferences _preferences = new CommunicationPreferences();
        private readonly CommunicationMediumSet _banned = new CommunicationMediumSet(Medium, CommunicationMedium.Email);

        [Fact]
        public void Ctor()
        {
            //Act
            var preferences = new CommunicationPreferences();

            //Arrange
            Assert.Null(preferences.PreferredMedium);
            Assert.NotNull(preferences.BannedMediums);
        }

        [Fact]
        public void Ctor_WithBannedMediums()
        {
            //Act
            var preferences = new CommunicationPreferences(_banned);

            //Arrange
            Assert.Null(preferences.PreferredMedium);
            Assert.Equal(_banned, preferences.BannedMediums);
        }

        [Fact]
        public void Ctor_GivenBannedMediums_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("bannedMediums", () => new CommunicationPreferences(null));
        }

        [Fact]
        public void ChangePreferredMedium()
        {
            //Act
            _preferences.ChangePreferredMedium(Medium);

            //Assert
            Assert.Equal(Medium, _preferences.PreferredMedium);
        }

        [Fact]
        public void ChangePreferredMedium_GivenMedium_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("medium", () => _preferences.ChangePreferredMedium(null));
        }

        [Fact]
        public void ChangePreferredMedium_GivenMedium_IsBanned_Throws()
        {
            //Arrange
            var preferences = new CommunicationPreferences(_banned);

            //Act
            Assert.Throws<PreferredMediumCannotBeBannedException>(() =>
                preferences.ChangePreferredMedium(_banned.Values.First()));
        }

        [Fact]
        public void ClearPreferredMedium()
        {
            //Arrange
            _preferences.ChangePreferredMedium(Medium);

            //Act
            _preferences.ClearPreferredMedium();

            //Assert
            Assert.Null(_preferences.PreferredMedium);
        }

        [Fact]
        public void PermitMedium()
        {
            //Arrange
            var communication = new CommunicationPreferences(_banned);

            //Act
            communication.PermitMedium(Medium);

            //Assert
            Assert.DoesNotContain(Medium, communication.BannedMediums.Values);
        }

        [Fact]
        public void PermitMedium_GivenMedium_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("medium", () => _preferences.PermitMedium(null));
        }

        [Fact]
        public void BanMedium()
        {
            //Act
            _preferences.BanMedium(Medium);

            //Assert
            Assert.Contains(Medium, _preferences.BannedMediums.Values);
        }

        [Fact]
        public void BanMedium_GivenMedium_IsPreferred_Throws()
        {
            //Arrange
            _preferences.ChangePreferredMedium(Medium);

            Assert.Throws<PreferredMediumCannotBeBannedException>(() => _preferences.BanMedium(_preferences.PreferredMedium));
        }

        [Fact]
        public void BanMedium_GivenMedium_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("medium", () => _preferences.BanMedium(null));
        }

        [Fact]
        public void ChangeBannedMediums()
        {
            //Act
            _preferences.ChangeBannedMediums(_banned);

            //Assert
            Assert.Equal(_banned, _preferences.BannedMediums);
        }

        [Fact]
        public void ChangeBannedMediums_GivenMediums_HasOne_ThatIsPreferred_Throws()
        {
            //Arrange
            _preferences.ChangePreferredMedium(_banned.Values.First());

            //Act
            Assert.Throws<PreferredMediumCannotBeBannedException>(() => _preferences.ChangeBannedMediums(_banned));
        }

        [Fact]
        public void ChangeBannedMediums_GivenMediums_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mediums", () => _preferences.ChangeBannedMediums(null));
        }

        [Fact]
        public void ClearBannedMediums()
        {
            //Arrange
            _preferences.ChangeBannedMediums(_banned);

            //Act
            _preferences.ClearBannedMediums();

            //Assert
            Assert.Equal(CommunicationMediumSet.Empty, _preferences.BannedMediums);
        }
    }
}
