﻿using AutoFixture;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.Domain;
using NexCore.Legal;
using NexCore.Testing;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Domain.Individuals
{
    public class IndividualTest
    {
        private readonly Fixture _fixture;
        private readonly Individual _individual;
        private readonly PersonName _name;

        public IndividualTest()
        {
            _fixture = new Fixture();
            _individual = _fixture.Create<Individual>();
            _name = _fixture.Create<PersonName>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var individual = new Individual(_name);

            //Assert
            Assert.Equal(_name, individual.Name);
            Assert.Null(individual.Birthday);
            Assert.Null(individual.Gender);
            Assert.NotNull(individual.Contact);
            Assert.NotNull(individual.Preferences);
            Assert.Equal(Passport.Empty, individual.Passport);
            Assert.Equal(TaxIdentificationNumber.Empty, individual.TaxNumber);
        }

        [Fact]
        public void Ctor_WithId()
        {
            //Act
            var individual = new Individual(11, _name);

            //Assert
            Assert.Equal(11, individual.Id);
            Assert.Equal(_name, individual.Name);
            Assert.Null(individual.Birthday);
            Assert.Null(individual.Gender);
            Assert.NotNull(individual.Contact);
            Assert.NotNull(individual.Preferences);
            Assert.Equal(Passport.Empty, individual.Passport);
            Assert.Equal(TaxIdentificationNumber.Empty, individual.TaxNumber);
        }

        [Fact]
        public void Person_GivenName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new Individual(null));
        }

        [Fact]
        public void Birthday()
        {
            //Arrange
            var date = DateTime.Now;

            //Act
            _individual.Birthday = date;

            //Assert
            Assert.Equal(date, _individual.Birthday);
        }

        [Fact]
        public void Name()
        {
            //Act
            _individual.Name = _name;

            //Arrange
            Assert.Equal(_name, _individual.Name);
        }

        [Fact]
        public void Name_GivenValue_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => _individual.Name = null);
        }

        [Fact]
        public void GenderProperty()
        {
            //Act
            _individual.Gender = Gender.Male;

            //Assert
            Assert.Equal(Gender.Male, _individual.Gender);
        }

        [Fact]
        public void SetPrimaryContactPoint_WhenIsTelephone_AddEntityEvent_CustomerPrimaryTelephoneUpdatedEvent()
        {
            //Arrange
            var telephone = _fixture.Create<Telephone>();
            _individual.Contact.Add(_fixture.Create<Telephone>());
            _individual.Contact.Add(telephone);
            var expected = new PrimaryTelephoneUpdatedEvent
            {
                ContactableId = _individual.Id,
                Telephone = telephone
            };

            //Act
            _individual.Contact.SetPrimary(telephone);

            //Assert
            Assert.Contains(expected, _individual.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void SetPrimaryContactPoint_WhenIsNotTelephone_DoNotAddEntityEvent_CustomerPrimaryTelephoneUpdatedEvent()
        {
            //Arrange
            var individual = _fixture.Create<Individual>();
            var email = _fixture.Create<EmailAddress>();

            individual.Contact.Add(email);

            //Act
            individual.Contact.SetPrimary(email);

            //Assert
            Assert.Empty(_individual.EntityEvents);
        }

        [Fact]
        public void SetPrimaryContactPoint_WhenIsPhysicalAddress_AddEntityEvent_CustomerPrimaryPhysicalAddressUpdatedEvent()
        {
            //Arrange
            var physicalAddress = _fixture.Create<PhysicalAddress>();
            _individual.Contact.Add(_fixture.Create<PhysicalAddress>());
            _individual.Contact.Add(physicalAddress);
            var expected = new PrimaryPhysicalAddressUpdateEvent
            {
                ContactableId = _individual.Id,
                PhysicalAddress = physicalAddress
            };

            //Act
            _individual.Contact.SetPrimary(physicalAddress);

            //Assert
            Assert.Contains(expected, _individual.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void SetPrimaryContactPoint_WhenIsNotPhysicalAddress_DoNotAddEntityEvent_CustomerPrimaryPhysicalAddressUpdatedEvent()
        {
            //Arrange
            var individual = _fixture.Create<Individual>();
            var email = _fixture.Create<EmailAddress>();

            individual.Contact.Add(email);

            //Act
            individual.Contact.SetPrimary(email);

            //Assert
            Assert.Empty(_individual.EntityEvents);
        }

        [Fact]
        public void ChangePassport()
        {
            //Arrange
            var passport = _fixture.Create<Passport>();

            //Act
            _individual.ChangePassport(passport);

            //Assert
            Assert.Equal(passport, _individual.Passport);
        }

        [Fact]
        public void ChangePassport_GivenPassport_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("passport", () => _individual.ChangePassport(null));
        }

        [Fact]
        public void ChangeTaxNumber()
        {
            //Arrange
            var taxNumber = _fixture.Create<TaxIdentificationNumber>();

            //Act
            _individual.ChangeTaxNumber(taxNumber);

            //Assert
            Assert.Equal(taxNumber, _individual.TaxNumber);
        }

        [Fact]
        public void ChangeTaxNumber_GivenNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("number", () => _individual.ChangeTaxNumber(null));
        }

        [Fact]
        public void UpdateFamilyStatus()
        {
            //Act
            _individual.UpdateFamilyStatus(FamilyStatus.Married);

            //Assert
            Assert.Equal(_individual.FamilyStatus, FamilyStatus.Married);
        }

        [Fact]
        public void UpdateChildrenStatus()
        {
            //Act
            _individual.UpdateChildrenStatus(2);

            //Assert
            Assert.Equal(2, _individual.Children);
        }
    }
}
