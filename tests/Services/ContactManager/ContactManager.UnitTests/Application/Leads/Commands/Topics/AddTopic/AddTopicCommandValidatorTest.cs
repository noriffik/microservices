﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Leads.Commands.Topics.AddTopic;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Leads.Commands.Topics.AddTopic
{
    public class AddTopicCommandValidatorTest
    {
        private readonly AddTopicCommandValidator _validator = new AddTopicCommandValidator();

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenName_IsNullOrEmpty(string name)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Name, name);
        }

        [Fact]
        public void ShouldNotHaveError_WhenName_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Name, "Name");
        }
    }
}
