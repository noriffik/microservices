﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Leads.Commands.Topics.UpdateTopic;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Leads.Commands.Topics.UpdateTopic
{
    public class UpdateTopicCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Mock<Topic> _topic;

        //Command and handler
        private readonly UpdateTopicCommand _command;
        private readonly UpdateTopicCommandHandler _handler;

        public UpdateTopicCommandHandlerTest()
        {
            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entity
            _topic = new Mock<Topic>();

            //Command and handler
            _command = new Fixture().Create<UpdateTopicCommand>();
            _handler = new UpdateTopicCommandHandler(_work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(UpdateTopicCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Require<Topic>(_command.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_topic.Object);
                _topic.Setup(t => t.ChangeName(_command.Name))
                    .InSequence();
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
