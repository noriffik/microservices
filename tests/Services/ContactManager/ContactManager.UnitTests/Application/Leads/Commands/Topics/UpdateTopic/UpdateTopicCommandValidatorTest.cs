﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Leads.Commands.Topics.UpdateTopic;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Leads.Commands.Topics.UpdateTopic
{
    public class UpdateTopicCommandValidatorTest
    {
        private readonly UpdateTopicCommandValidator _validator = new UpdateTopicCommandValidator();

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenName_IsNullOrEmpty(string name)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Name, name);
        }

        [Fact]
        public void ShouldNotHaveError_WhenName_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Name, "Name");
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldHaveError_WhenId_IsLessThanOrEqualTo_0(int id)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Id, id);
        }

        [Fact]
        public void ShouldNotHaveError_WhenId_IsGreaterThan_0()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Id, 1);
        }
    }
}
