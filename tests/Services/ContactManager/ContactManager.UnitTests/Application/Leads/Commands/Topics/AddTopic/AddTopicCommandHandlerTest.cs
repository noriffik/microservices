﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Leads.Commands.Topics.AddTopic;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Leads.Commands.Topics.AddTopic
{
    public class AddTopicCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IMapper> _mapper;

        //Entity
        private readonly Topic _topic;

        //Command and handler
        private readonly AddTopicCommand _command;
        private readonly AddTopicCommandHandler _handler;

        public AddTopicCommandHandlerTest()
        {
            var fixture = new Fixture();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            _mapper = new Mock<IMapper>();

            //Entity
            _topic = fixture.Create<Topic>();

            //Command and handler
            _command = fixture.Create<AddTopicCommand>();
            _handler = new AddTopicCommandHandler(_work.Object, _mapper.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddTopicCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _mapper.Setup(m => m.Map<Topic>(_command))
                    .InSequence()
                    .Returns(_topic);
                _repository.Setup(r => r.Add(_topic))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_topic.Id, result);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                _handler.Handle(null, CancellationToken.None));
        }
    }
}
