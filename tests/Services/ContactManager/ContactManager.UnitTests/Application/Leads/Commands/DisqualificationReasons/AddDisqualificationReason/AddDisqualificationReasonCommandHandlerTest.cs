﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.AddDisqualificationReason;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Leads.Commands.DisqualificationReasons.AddDisqualificationReason
{
    public class AddDisqualificationReasonCommandHandlerTest
    {
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IMapper> _mapper;
        private readonly AddDisqualificationReasonCommandHandler _handler;

        public AddDisqualificationReasonCommandHandlerTest()
        {
            _repository = new Mock<IEntityRepository>();
            _work = new Mock<IUnitOfWork>();
            _mapper = new Mock<IMapper>();

            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            _handler = new AddDisqualificationReasonCommandHandler(_work.Object, _mapper.Object);
        }

        [Fact]
        public void Ctor_NullGuards()
        {
            Assert.Injection.OfConstructor(typeof(AddDisqualificationReasonCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                var fixture = new Fixture();
                var reason = fixture.Create<DisqualificationReason>();
                var command = fixture.Create<AddDisqualificationReasonCommand>();

                _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

                _mapper.Setup(m => m.Map<DisqualificationReason>(command))
                    .InSequence()
                    .Returns(reason);

                _repository.Setup(r => r.Add(reason))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(command, CancellationToken.None);

                //Assert
                Assert.NotNull(result);
            }
        }

        [Fact]
        public Task Handle_GivenCommand_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request",
                () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
