﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.UpdateDisqualificationReason;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Leads.Commands.DisqualificationReasons.UpdateDisqualificationReason
{
    public class UpdateDisqualificationReasonCommandValidatorTest
    {
        private readonly UpdateDisqualificationReasonCommandValidator _validator = new UpdateDisqualificationReasonCommandValidator();

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void ShouldHaveValidationErrorFor_Id(int id)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Id, id);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void ShouldHaveValidationErrorFor_Name(string name)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Name, name);
        }

        [Fact]
        public void ShouldNotHaveValidationError_ForId()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Id, 1);
        }

        [Fact]
        public void ShouldNotHaveValidationError_ForName()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Name, "Name");
        }
    }
}
