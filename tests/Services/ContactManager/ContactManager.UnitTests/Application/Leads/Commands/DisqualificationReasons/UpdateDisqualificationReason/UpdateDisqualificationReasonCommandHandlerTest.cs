﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.UpdateDisqualificationReason;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Leads.Commands.DisqualificationReasons.UpdateDisqualificationReason
{
    public class UpdateDisqualificationReasonCommandHandlerTest
    {
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IUnitOfWork> _work;
        private readonly UpdateDisqualificationReasonCommandHandler _handler;

        public UpdateDisqualificationReasonCommandHandlerTest()
        {
            _repository = new Mock<IEntityRepository>();
            _work = new Mock<IUnitOfWork>();

            _work.Setup(w => w.EntityRepository)
                .Returns(_repository.Object);

            _handler = new UpdateDisqualificationReasonCommandHandler(_work.Object);
        }

        [Fact]
        public void Ctor_NullGuards()
        {
            Assert.Injection.OfConstructor(typeof(UpdateDisqualificationReasonCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                var fixture = new Fixture();

                var reason = new Mock<DisqualificationReason>();
                var command = fixture.Create<UpdateDisqualificationReasonCommand>();

                _repository.Setup(r => r.Require<DisqualificationReason>(command.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(reason.Object);

                reason.Setup(r => r.ChangeName(command.Name))
                    .InSequence();

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(command, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_GivenCommand_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request",
                () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
