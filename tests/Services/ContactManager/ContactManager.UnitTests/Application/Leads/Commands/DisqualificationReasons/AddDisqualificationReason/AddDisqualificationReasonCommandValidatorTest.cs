﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Leads.Commands.DisqualificationReasons.AddDisqualificationReason;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Leads.Commands.DisqualificationReasons.AddDisqualificationReason
{
    public class AddDisqualificationReasonCommandValidatorTest
    {
        private readonly AddDisqualificationReasonCommandValidator _validator = new AddDisqualificationReasonCommandValidator();

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void ShouldHaveValidationErrorFor(string name)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Name, name);
        }

        [Fact]
        public void ShouldNotHaveValidationError()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Name, "Name");
        }
    }
}
