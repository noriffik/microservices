﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Leads.Commands.InterestSources.RemoveInterestedSource;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Leads.Commands.InterestSources.RemoveInterestSource
{
    public class RemoveInterestSourceCommandValidatorTest
    {
        private readonly RemoveInterestSourceCommandValidator _validator = new RemoveInterestSourceCommandValidator();

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void ShouldHaveValidationErrorFor_Id(int id)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Id, id);
        }

        [Fact]
        public void ShouldNotHaveValidationError_ForId()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Id, 1);
        }
    }
}
