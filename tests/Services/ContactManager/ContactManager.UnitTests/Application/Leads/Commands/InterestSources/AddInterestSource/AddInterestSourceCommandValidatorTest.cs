﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Leads.Commands.InterestSources.AddInterestSource;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Leads.Commands.InterestSources.AddInterestSource
{
    public class AddInterestSourceCommandValidatorTest
    {
        private readonly AddInterestSourceCommandValidator _validator = new AddInterestSourceCommandValidator();

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void ShouldHaveValidationError(string name)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Name, name);
        }

        [Fact]
        public void ShouldNotHaveValidationError()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Name, "Name");
        }
    }
}
