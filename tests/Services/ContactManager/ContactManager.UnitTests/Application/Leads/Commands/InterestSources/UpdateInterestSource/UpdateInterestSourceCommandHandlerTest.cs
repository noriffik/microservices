﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Leads.Commands.InterestSources.UpdateInterestSource;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Leads.Commands.InterestSources.UpdateInterestSource
{
    public class UpdateInterestSourceCommandHandlerTest
    {
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IUnitOfWork> _work;

        private readonly UpdateInterestSourceCommandHandler _handler;

        public UpdateInterestSourceCommandHandlerTest()
        {
            _repository = new Mock<IEntityRepository>();
            _work = new Mock<IUnitOfWork>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            _handler = new UpdateInterestSourceCommandHandler(_work.Object);
        }

        [Fact]
        public void Ctor_NullGuards()
        {
            Assert.Injection.OfConstructor(typeof(UpdateInterestSourceCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                var fixture = new Fixture();
                var interest = new Mock<InterestSource>(fixture.Create<string>());
                var command = fixture.Create<UpdateInterestSourceCommand>();

                _repository.Setup(r => r.Require<InterestSource>(command.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(interest.Object);

                interest.Setup(i => i.ChangeName(command.Name))
                    .InSequence();

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(command, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_givenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
