﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Leads.Commands.InterestSources.UpdateInterestSource;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Leads.Commands.InterestSources.UpdateInterestSource
{
    public class UpdateInterestSourceCommandValidatorTest
    {
        private readonly UpdateInterestSourceCommandValidator _validator = new UpdateInterestSourceCommandValidator();

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void ShouldHaveValidationErrorFor_Id(int id)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Id, id);
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void ShouldHaveValidationError(string name)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Name, name);
        }

        [Fact]
        public void ShouldNotHaveValidationError_ForId()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Id, 1);
        }

        [Fact]
        public void ShouldNotHaveValidationError()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Name, "Name");
        }
    }
}
