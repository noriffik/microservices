﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Leads.Commands.InterestSources.RemoveInterestedSource;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Leads.Commands.InterestSources.RemoveInterestSource
{
    public class RemoveInterestCommandHandlerTest
    {
        private readonly Mock<InterestSource> _interest;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IUnitOfWork> _work;

        private readonly RemoveInterestSourceCommand _command;
        private readonly RemoveInterestSourceCommandHandler _handler;

        public RemoveInterestCommandHandlerTest()
        {
            var fixture = new Fixture();

            _interest = new Mock<InterestSource>(fixture.Create<string>());

            _repository = new Mock<IEntityRepository>();
            _work = new Mock<IUnitOfWork>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            _command = fixture.Create<RemoveInterestSourceCommand>();
            _handler = new RemoveInterestSourceCommandHandler(_work.Object);
        }

        [Fact]
        public void Ctor_NullGuards()
        {
            Assert.Injection.OfConstructor(typeof(RemoveInterestSourceCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Require<InterestSource>(_command.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_interest.Object);

                _interest.Setup(i => i.Archive())
                    .InSequence();

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_WhenInterest_IsArchived_Returns()
        {
            //Arrange
            _repository.Setup(r => r.Require<InterestSource>(_command.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(_interest.Object);

            _interest.Setup(i => i.IsArchived).Returns(true);

            _interest.Setup(i => i.Archive())
                .Verifiable();

            //Act
            await _handler.Handle(_command, CancellationToken.None);

            //Assert
            _interest.Verify(i => i.Archive(), Times.Never);
        }

        [Fact]
        public Task Handle_givenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
