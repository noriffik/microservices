﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Leads.Commands.InterestSources.AddInterestSource;
using NexCore.ContactManager.Domain.Leads;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Leads.Commands.InterestSources.AddInterestSource
{
    public class AddInterestSourceCommandHandlerTest
    {
        private readonly InterestSource _interest;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IMapper> _mapper;
        private readonly AddInterestSourceCommand _command;
        private readonly AddInterestSourceCommandHandler _handler;

        public AddInterestSourceCommandHandlerTest()
        {
            var fixture = new Fixture();
            _interest = fixture.Create<InterestSource>();
            _repository = new Mock<IEntityRepository>();
            _work = new Mock<IUnitOfWork>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            _mapper = new Mock<IMapper>();

            _command = fixture.Create<AddInterestSourceCommand>();
            _handler = new AddInterestSourceCommandHandler(_work.Object, _mapper.Object);
        }

        [Fact]
        public void Ctor_NullGuards()
        {
            Assert.Injection.OfConstructor(typeof(AddInterestSourceCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _mapper.Setup(m => m.Map<InterestSource>(_command))
                    .InSequence()
                    .Returns(_interest);

                _repository.Setup(r => r.Add(_interest))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_givenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
