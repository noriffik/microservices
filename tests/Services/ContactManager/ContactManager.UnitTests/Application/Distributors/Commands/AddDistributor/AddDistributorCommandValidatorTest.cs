﻿using FluentValidation.TestHelper;
using Moq;
using NexCore.ContactManager.Application.Distributors.Commands;
using NexCore.ContactManager.Application.Distributors.Commands.AddDistributor;
using NexCore.ContactManager.Application.Providers;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.AddDistributor
{
    public class AddDistributorCommandValidatorTest
    {
        private readonly AddDistributorCommandValidator _validator =
            new AddDistributorCommandValidator(new Mock<IGeographyDescriptionProvider>().Object);

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddDistributorCommandValidator)).HasNullGuard();
        }

        [Fact]
        public void ShouldHaveError_WhenId_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Id, null as string);
        }

        [Fact]
        public void ShouldHaveChildValidator_DistributorIdValidator_ForId()
        {
            _validator.ShouldHaveChildValidator(c => c.Id, typeof(DistributorIdValidator));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldHaveError_WhenOrganizationId_IsInvalid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(m => m.OrganizationId, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenOrganizationId_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.OrganizationId, 1);
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenCountryCode_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(m => m.CountryCode, value);
        }

        [Fact]
        public void ShouldHaveChildValidator_CountryCodeValidator_ForCountryCode()
        {
            _validator.ShouldHaveChildValidator(c => c.CountryCode, typeof(CountryCodeValidator));
        }
    }
}
