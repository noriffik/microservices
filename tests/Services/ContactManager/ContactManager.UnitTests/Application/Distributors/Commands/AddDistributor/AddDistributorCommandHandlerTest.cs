﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Distributors.Commands.AddDistributor;
using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using System;
using System.Threading;
using System.Threading.Tasks;
using NexCore.EventBus.Abstract;
using Xunit;
using ByOrganizationIdSpecification = NexCore.ContactManager.Domain.Distributors.Specifications.ByOrganizationIdSpecification;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.AddDistributor
{
    public class AddDistributorCommandHandlerTest
    {
	    private readonly IFixture fixture = new Fixture();
        //Entity
        private readonly Distributor _distributor;
        private readonly Organization _organization;
        private readonly DistributorRegisteredEvent _event;

        //Dependencies
        private readonly Mock<IMapper> _mapper;
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IIntegrationEventStorage> _eventStore;

        //Handler and command
        private readonly AddDistributorCommand _command;
        private readonly AddDistributorCommandHandler _handler;

        public AddDistributorCommandHandlerTest()
        {
            fixture.Customize(new DealerIdCustomization());

            //Entity
            _distributor = fixture.Create<Distributor>();
            _organization = fixture.Construct<Organization>(_distributor.OrganizationId);
            _event = fixture.Create<DistributorRegisteredEvent>();

            //Dependencies
            _mapper = new Mock<IMapper>();
            _repository = new Mock<IEntityRepository>();
			_eventStore = new Mock<IIntegrationEventStorage>();

            _work = new Mock<IUnitOfWork>();
            var eventFactory = new Mock<IDistributorRegisteredEventFactory>();

            //Setup dependencies
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            eventFactory.Setup(f => f.Create(_distributor.Id, _organization)).Returns(_event);

            //Handler and command
            _command = fixture.Create<AddDistributorCommand>();
            _handler = new AddDistributorCommandHandler(_work.Object, _mapper.Object, _eventStore.Object)
            {
                EventFactory = eventFactory.Object
            };
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddDistributorCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, It.IsAny<CancellationToken>()));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Has<Dealer, DealerId>(
                        It.Is<ContactManager.Domain.Dealers.Specifications.ByOrganizationIdSpecification>(s => s.OrganizationId == _command.OrganizationId), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Has<Distributor, DistributorId>(
                        It.Is<ByOrganizationIdSpecification>(s => s.OrganizationId == _command.OrganizationId), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Require<Organization>(_command.OrganizationId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_organization);
                _mapper.Setup(m => m.Map<Distributor>(_command))
                    .InSequence()
                    .Returns(_distributor);
                _repository.Setup(r => r.Has<Distributor, DistributorId>(_distributor.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Add<Distributor, DistributorId>(_distributor))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Publish event
                _eventStore.Setup(e => e.Add(_event))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Commit work
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_distributor.Id.Value, result);
            }
        }

        [Fact]
        public async Task Handle_WhenDistributorId_IsInUse_Throws()
        {
            //Arrange
            _mapper.Setup(m => m.Map<Distributor>(_command))
                .Returns(_distributor);
            _repository.Setup(r => r.Has<Distributor, DistributorId>(_distributor.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.Validation
                .ThrowsAsync(nameof(_command.Id), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("distributorId is in use");
        }

        [Fact]
        public async Task Handle_WhenOrganization_IsAlreadyDealer_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Has<Dealer, DealerId>(
                    It.Is<ContactManager.Domain.Dealers.Specifications.ByOrganizationIdSpecification>(s => s.OrganizationId == _command.OrganizationId), It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.Validation
                .ThrowsAsync(nameof(_command.OrganizationId), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("organization is already a dealer");
        }

        [Fact]
        public async Task Handle_WhenOrganization_IsAlreadyDistributor_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Has<Distributor, DistributorId>(
                    It.Is<ByOrganizationIdSpecification>(s => s.OrganizationId == _command.OrganizationId), It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.Validation
                .ThrowsAsync(nameof(_command.OrganizationId), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("organization is already an distributor");
        }
    }
}
