﻿using AutoFixture;
using NexCore.ContactManager.Application.Distributors.Commands.AddDistributor;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.AddDistributor
{
    public class DistributorRegisteredEventFactoryTest
    {
        private readonly DistributorId _distributorId;
        private readonly Organization _organization;

        private readonly DistributorRegisteredEventFactory _factory;

        public DistributorRegisteredEventFactoryTest()
        {
            var fixture = new Fixture().Customize(new DistributorIdCustomization());

            _distributorId = fixture.Create<DistributorId>();
            _organization = fixture.Create<Organization>();

            _factory = new DistributorRegisteredEventFactory();
        }

        [Fact]
        public void Create()
        {
            //Act
            var result = _factory.Create(_distributorId, _organization);

            //Assert
            Assert.Equal(_distributorId.Value, result.DistributorId);
            Assert.Equal(_organization.Id, result.OrganizationId);
            Assert.Equal(_organization.Name, result.Name);
        }

        [Fact]
        public void Create_GivenDistributorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("distributorId", () => _factory.Create(null, _organization));
        }

        [Fact]
        public void Create_GivenOrganization_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("organization", () => _factory.Create(_distributorId, null));
        }
    }
}
