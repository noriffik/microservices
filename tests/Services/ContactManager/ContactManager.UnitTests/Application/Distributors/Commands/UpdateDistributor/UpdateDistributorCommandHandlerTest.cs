﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Distributors.Commands.UpdateDistributor;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.UpdateDistributor
{
    public class UpdateDistributorCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;

        //Entities
        private readonly Mock<Distributor> _distributor;

        //Command and handler
        private readonly UpdateDistributorCommand _command;
        private readonly UpdateDistributorCommandHandler _handler;

        public UpdateDistributorCommandHandlerTest()
        {
            var fixture = new Fixture()
                .Customize(new DistributorIdCustomization());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            var repository = new Mock<IEntityRepository>();

            //Entities
            var distributorId = fixture.Create<DistributorId>();
            _distributor = new Mock<Distributor>(distributorId, fixture.Create<int>(), fixture.Create<string>());

            //Setup dependencies
            _work.Setup(w => w.EntityRepository).Returns(repository.Object);
            repository.Setup(r => r.Require<Distributor, DistributorId>(distributorId, It.IsAny<CancellationToken>()))
                .ReturnsAsync(_distributor.Object);

            //Command and handler
            _command = fixture.Build<UpdateDistributorCommand>()
                .With(c => c.DistributorId, distributorId.Value)
                .Create();
            _handler = new UpdateDistributorCommandHandler(_work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(UpdateDistributorCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>(() => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _distributor.Setup(d => d.ChangeCountryCode(_command.CountryCode))
                    .InSequence();
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }
    }
}
