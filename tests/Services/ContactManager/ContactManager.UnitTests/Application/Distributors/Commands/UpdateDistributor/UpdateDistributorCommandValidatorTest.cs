﻿using FluentValidation.TestHelper;
using Moq;
using NexCore.ContactManager.Application.Distributors.Commands;
using NexCore.ContactManager.Application.Distributors.Commands.AddDistributor;
using NexCore.ContactManager.Application.Distributors.Commands.UpdateDistributor;
using NexCore.ContactManager.Application.Providers;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.UpdateDistributor
{
    public class UpdateDistributorCommandValidatorTest
    {
        private readonly UpdateDistributorCommandValidator _validator =
            new UpdateDistributorCommandValidator(new Mock<IGeographyDescriptionProvider>().Object);

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddDistributorCommandValidator)).HasNullGuard();
        }

        [Fact]
        public void ShouldHaveError_WhenDistributorId_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.DistributorId, null as string);
        }

        [Fact]
        public void ShouldHaveChildValidator_DistributorIdValidator_ForDistributorId()
        {
            _validator.ShouldHaveChildValidator(c => c.DistributorId, typeof(DistributorIdValidator));
        }

        [Fact]
        public void ShouldHaveError_WhenCountryCode_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.CountryCode, null as string);
        }

        [Fact]
        public void ShouldHaveChildValidator_CountryCodeValidator_ForCountryCode()
        {
            _validator.ShouldHaveChildValidator(c => c.CountryCode, typeof(CountryCodeValidator));
        }
    }
}
