﻿using FluentValidation.TestHelper;
using Moq;
using NexCore.ContactManager.Application.Distributors.Commands;
using NexCore.ContactManager.Application.Providers;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands
{
    public class CountryCodeValidatorTest
    {
        private const string Value = "countryCode";
        private readonly Mock<IGeographyDescriptionProvider> _geographyProvider;
        private readonly CountryCodeValidator _validator;

        public CountryCodeValidatorTest()
        {
            _geographyProvider = new Mock<IGeographyDescriptionProvider>();
            _validator = new CountryCodeValidator(_geographyProvider.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(CountryCodeValidator)).HasNullGuard();
        }

        [Fact]
        public void ShouldHaveError_WhenValue_IsInvalid()
        {
            //Arrange
            _geographyProvider.Setup(p => p.IsValidIso2(Value)).Returns(false);

            //Assert
            _validator.TestValidate(Value).ShouldHaveError();
        }

        [Fact]
        public void ShouldNotHaveError_WhenValue_IsValid()
        {
            //Arrange
            _geographyProvider.Setup(p => p.IsValidIso2(Value)).Returns(true);

            //Assert
            _validator.TestValidate(Value).ShouldNotHaveError();
        }
    }
}
