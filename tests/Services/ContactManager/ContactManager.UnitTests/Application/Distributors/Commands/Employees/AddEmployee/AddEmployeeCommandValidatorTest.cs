﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Distributors.Commands.Employees.AddEmployee;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Employees.AddEmployee
{
    public class AddEmployeeCommandValidatorTest
    {
        private readonly AddEmployeeCommandValidator _validator = new AddEmployeeCommandValidator();

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("asdasdasda")]
        [InlineData("%^^#*(#@")]
        public void ShouldHaveValidatorErrorFor_DistributorId(string distributorId)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.DistributorId, distributorId);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void ShouldHaveValidatorErrorFor_IndividualId(int individualId)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.IndividualId, individualId);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void ShouldHaveValidatorErrorFor_JobId(int jobId)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.JobId, jobId);
        }

        [Fact]
        public void ShouldNotHaveValidatorErrorFor_DistributorId()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.DistributorId, "EKK");
        }

        [Fact]
        public void ShouldNotHaveValidatorErrorFor_IndividualId()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.IndividualId, 1);
        }

        [Fact]
        public void ShouldNotHaveValidatorErrorFor_JobId()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.JobId, 1);
        }
    }
}
