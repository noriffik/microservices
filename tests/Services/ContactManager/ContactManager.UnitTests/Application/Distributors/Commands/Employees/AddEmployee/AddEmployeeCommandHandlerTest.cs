﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Distributors.Commands.Employees.AddEmployee;
using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.DistributorEmployees;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using NexCore.Testing.Extensions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Employees.AddEmployee
{
    public class AddEmployeeCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<EmployeeFactory> _employeeFactory;
        private readonly Mock<EmployeeAddedEventFactory> _eventFactory;
        private readonly Mock<IIntegrationEventStorage> _eventStorage;

        //Entities
        private readonly DistributorEmployee _employee;
        private readonly Individual _individual;
        private readonly DistributorEmployeeAddedEvent _event;

        //Command and handler
        private readonly AddEmployeeCommand _command;
        private readonly AddEmployeeCommandHandler _handler;

        public AddEmployeeCommandHandlerTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            _employeeFactory = new Mock<EmployeeFactory>();
            _eventFactory = new Mock<EmployeeAddedEventFactory>();
            _eventStorage = new Mock<IIntegrationEventStorage>();

            //Entities
            _employee = fixture.Create<DistributorEmployee>();
            _individual = fixture.Construct<Individual>(_employee.IndividualId);
            _event = fixture.Create<DistributorEmployeeAddedEvent>();

            //Command and handler
            _command = fixture.Build<AddEmployeeCommand>()
                .With(c => c.DistributorId, _employee.DistributorId.Value)
                .With(c => c.IndividualId, _employee.IndividualId)
                .With(c => c.JobId, _employee.JobId)
                .Create();
            _handler = new AddEmployeeCommandHandler(_employeeFactory.Object, _work.Object, _eventStorage.Object)
            {
                EventFactory = _eventFactory.Object
            };
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddEmployeeCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            using (Sequence.Create())
            {
                _employeeFactory.Setup(f => f.Create(_employee.DistributorId, _employee.IndividualId, _employee.JobId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_employee);
                _repository.Setup(r => r.Add(_employee))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.Require<Individual>(_employee.IndividualId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_individual);
                _eventFactory.Setup(f => f.Create(_employee, _individual.Name))
                    .InSequence()
                    .Returns(_event);
                _eventStorage.Setup(s => s.Add(_event))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_employee.Id, result);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public void EventFactory()
        {
            //Arrange
            var handler = new AddEmployeeCommandHandler(_employeeFactory.Object, _work.Object, _eventStorage.Object);

            //Assert
            Assert.Injection.OfProperty(handler, nameof(handler.EventFactory))
                .HasNullGuard()
                .DoesOverride()
                .HasDefault();
        }
    }
}
