﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Distributors.Commands.Employees.UpdateEmployee;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.DistributorEmployees;
using NexCore.ContactManager.Domain.DistributorEmployees.Specifications;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Employees.UpdateEmployee
{
    public class UpdateEmployeeCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly DistributorId _distributorId;
        private readonly Mock<DistributorEmployee> _employee;

        //Command and handler
        private readonly UpdateEmployeeCommand _command;
        private readonly UpdateEmployeeCommandHandler _handler;

        public UpdateEmployeeCommandHandlerTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entity
            _distributorId = fixture.Create<DistributorId>();

            _employee = new Mock<DistributorEmployee>(fixture.Create<int>(), fixture.Create<int>(), _distributorId, fixture.Create<int>())
            {
                CallBase = true
            };

            //Command and handler
            _command = fixture.Build<UpdateEmployeeCommand>()
                .With(c => c.DistributorId, _distributorId.Value)
                .With(c => c.Id, _employee.Object.Id)
                .Create();
            _handler = new UpdateEmployeeCommandHandler(_work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(UpdateEmployeeCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.HasRequired<Job>(_command.JobId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.Require(It.Is<ByDistributorIdAndEmployeeIdSpecification>(
                        s => s.DistributorId == _distributorId && s.EmployeeId == _employee.Object.Id),
                        It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_employee.Object);
                _employee.Setup(e => e.ChangeJobId(_command.JobId))
                    .InSequence();
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                _handler.Handle(null, CancellationToken.None));
        }
    }
}
