﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Distributors.Commands.Employees.UpdateEmployee;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Employees.UpdateEmployee
{
    public class UpdateEmployeeCommandValidatorTest
    {
        private readonly UpdateEmployeeCommandValidator _validator = new UpdateEmployeeCommandValidator();

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("asdasdasda")]
        [InlineData("%^^#*(#@")]
        public void ShouldHaveValidatorErrorFor_DistributorId(string distributorId)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.DistributorId, distributorId);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void ShouldHaveValidatorErrorFor_Id(int id)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Id, id);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void ShouldHaveValidatorErrorFor_JobId(int jobId)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.JobId, jobId);
        }

        [Fact]
        public void ShouldNotHaveValidatorErrorFor_DistributorId()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.DistributorId, "EKK");
        }

        [Fact]
        public void ShouldNotHaveValidatorErrorFor_Id()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Id, 1);
        }

        [Fact]
        public void ShouldNotHaveValidatorErrorFor_JobId()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.JobId, 1);
        }
    }
}
