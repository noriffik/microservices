﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Distributors.Commands;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands
{
    public class DistributorIdValidatorTest
    {
        private readonly DistributorIdValidator _validator = new DistributorIdValidator();

        [Theory]
        [InlineData("QQQQ")]
        [InlineData("WW")]
        [InlineData(" ")]
        [InlineData("")]
        [InlineData("#@w")]
        public void ShouldHaveError_WhenValue_IsInvalid(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }

        [Theory]
        [InlineData("WWW")]
        [InlineData("123")]
        [InlineData("F4j")]
        public void ShouldNotHaveError_WhenValue_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }
    }
}
