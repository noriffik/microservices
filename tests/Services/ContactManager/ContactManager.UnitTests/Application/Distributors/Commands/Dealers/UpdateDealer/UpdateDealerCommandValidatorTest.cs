﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Distributors.Commands;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.UpdateDealer;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.UpdateDealer
{
    public class UpdateDealerCommandValidatorTest
    {
        private readonly UpdateDealerCommandValidator _validator = new UpdateDealerCommandValidator();

        [Fact]
        public void ShouldHaveError_WhenDistributorId_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.DistributorId, null as string);
        }

        [Fact]
        public void ShouldHaveChildValidator_DistributorIdValidator_ForDistributorId()
        {
            _validator.ShouldHaveChildValidator(c => c.DistributorId, typeof(DistributorIdValidator));
        }

        [Fact]
        public void ShouldHaveError_WhenCode_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Code, null as string);
        }

        [Fact]
        public void ShouldHaveChildValidator_DealerCodeValidator_ForCode()
        {
            _validator.ShouldHaveChildValidator(c => c.Code, typeof(DealerCodeValidator));
        }
    }
}
