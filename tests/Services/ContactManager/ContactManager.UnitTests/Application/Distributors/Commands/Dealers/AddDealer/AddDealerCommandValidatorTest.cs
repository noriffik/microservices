﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Distributors.Commands;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.AddDealer;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.AddDealer
{
    public class AddDealerCommandValidatorTest
    {
        private readonly AddDealerCommandValidator _validator = new AddDealerCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldHaveError_WhenOrganizationId_IsEmpty_Or_LessThan_1(int organizationId)
        {
            _validator.ShouldHaveValidationErrorFor(cp => cp.OrganizationId, organizationId);
        }

        [Fact]
        public void ShouldNotHaveError_WhenOrganizationId_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(cp => cp.OrganizationId, 1);
        }

        [Fact]
        public void ShouldHaveError_WhenDistributorId_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.DistributorId, null as string);
        }

        [Fact]
        public void ShouldHaveChildValidator_DistributorIdValidator_ForDistributorId()
        {
            _validator.ShouldHaveChildValidator(c => c.DistributorId, typeof(DistributorIdValidator));
        }

        [Fact]
        public void ShouldHaveError_WhenCode_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Code, null as string);
        }

        [Fact]
        public void ShouldHaveChildValidator_DealerCodeValidator_ForCode()
        {
            _validator.ShouldHaveChildValidator(c => c.Code, typeof(DealerCodeValidator));
        }
    }
}
