﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.UpdateEmployee;
using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.Domain.DealerEmployees.Specifications;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.Employees.UpdateEmployee
{
    public class UpdateEmployeeCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly DealerId _dealerId;
        private readonly Mock<DealerEmployee> _employee;

        //Command and handler
        private readonly UpdateEmployeeCommand _command;
        private readonly UpdateEmployeeCommandHandler _handler;

        public UpdateEmployeeCommandHandlerTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entity
            _dealerId = fixture.Create<DealerId>();
            _employee = new Mock<DealerEmployee>(fixture.Create<int>(), fixture.Create<int>(), _dealerId, fixture.Create<int>())
            {
                CallBase = true
            };

            //Command and handler
            _command = fixture.Build<UpdateEmployeeCommand>()
                .With(c => c.DistributorId, _dealerId.DistributorId.Value)
                .With(c => c.DealerCode, _dealerId.DealerCode.Value)
                .With(c => c.EmployeeId, _employee.Object.Id)
                .Create();
            _handler = new UpdateEmployeeCommandHandler(_work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(UpdateEmployeeCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.HasRequired<Job>(_command.JobId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.Require(It.Is<ByDealerIdAndEmployeeIdSpecification>(
                        s => s.DealerId == _dealerId && s.DealerEmployeeId == _employee.Object.Id),
                        It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_employee.Object);
                _employee.Setup(e => e.ChangeJobId(_command.JobId))
                    .InSequence();
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                _handler.Handle(null, CancellationToken.None));
        }
    }
}
