﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Distributors.Commands;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Leads.OpenLead;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.Leads.OpenLead
{
    public class OpenLeadCommandValidatorTest
    {
        private readonly OpenLeadCommandValidator _validator = new OpenLeadCommandValidator();

        [Fact]
        public void ShouldHaveError_WhenDistributorId_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.DistributorId, null as string);
        }

        [Fact]
        public void ShouldHaveChildValidator_DistributorIdValidator_ForDistributorId()
        {
            _validator.ShouldHaveChildValidator(c => c.DistributorId, typeof(DistributorIdValidator));
        }

        [Fact]
        public void ShouldHaveError_WhenDealerCode_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.DealerCode, null as string);
        }

        [Fact]
        public void ShouldHaveChildValidator_DealerCodeValidator_ForCode()
        {
            _validator.ShouldHaveChildValidator(c => c.DealerCode, typeof(DealerCodeValidator));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldHaveError_WhenTopicId_IsLessThanOrEqualTo_0(int id)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.TopicId, id);
        }

        [Fact]
        public void ShouldNotHaveError_WhenTopicId_IsGreaterThan_0()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.TopicId, 1);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldHaveError_WhenIndividualId_IsLessThanOrEqualTo_0(int id)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.IndividualId, id);
        }

        [Fact]
        public void ShouldNotHaveError_WhenIndividualId_IsGreaterThan_0()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.IndividualId, 1);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldHaveError_WhenOrganizationId_IsLessThanOrEqualTo_0(int id)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.OrganizationId, id);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(null)]
        public void ShouldNotHaveError_WhenOrganizationId_IsNull_Or_GreaterThan_0(int? id)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.OrganizationId, id);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldHaveError_WhenOwnerEmployeeId_LessThanOrEqualTo_0(int id)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.OwnerEmployeeId, id);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(null)]
        public void ShouldNotHaveError_WhenOwnerEmployeeId_IsNull_Or_GreaterThan_0(int? id)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.OwnerEmployeeId, id);
        }
    }
}
