﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddCorporateCustomer;
using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.DealerCorporateCustomers;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.Customers.AddCorporateCustomer
{
    public class AddCorporateCustomerCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<DealerCorporateCustomerFactory> _customerFactory;
        private readonly Mock<IIntegrationEventStorage> _eventStorage;
        private readonly Mock<DealerCorporateCustomerAddedEventFactory> _eventFactory;

        //Entities
        private readonly DealerCorporateCustomer _customer;
        private readonly DealerCorporateCustomerAddedEvent _event;

        //Command and handler
        private readonly AddCorporateCustomerCommand _command;
        private readonly AddCorporateCustomerCommandHandler _handler;

        public AddCorporateCustomerCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customize(new DealerIdCustomization());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            _customerFactory = new Mock<DealerCorporateCustomerFactory>();
            _eventStorage = new Mock<IIntegrationEventStorage>();
            _eventFactory = new Mock<DealerCorporateCustomerAddedEventFactory>();

            //Entities
            _customer = fixture.Create<DealerCorporateCustomer>();

            _event = fixture.Create<DealerCorporateCustomerAddedEvent>();

            //Command and handler
            _command = fixture.Build<AddCorporateCustomerCommand>()
                .With(c => c.DistributorId, _customer.DealerId.DistributorId.Value)
                .With(c => c.DealerCode, _customer.DealerId.DealerCode.Value)
                .With(c => c.OrganizationId, _customer.ContactableId)
                .Create();

            _handler = new AddCorporateCustomerCommandHandler(
                 _customerFactory.Object, _eventFactory.Object, _work.Object, _eventStorage.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddCorporateCustomerCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _customerFactory.Setup(f =>
                        f.Create(_command.OrganizationId, DealerId.Parse(_command.DistributorId, _command.DealerCode), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_customer);

                _repository.Setup(r => r.Add(_customer))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _repository.Setup(r => r.Require<DealerCorporateCustomer>(_customer.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_customer);

                _eventFactory.Setup(e => e.Create(_customer))
                    .InSequence()
                    .Returns(_event);

                _eventStorage.Setup(s => s.Add(_event))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_customer.Id, result);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
