﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.AddEmployee;
using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.ContactManager.Domain.Employees;
using NexCore.ContactManager.Domain.Employees.Specifications;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.Testing;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.Employees.AddEmployee
{
    public class EmployeeFactoryTest
    {
        //Dependencies
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly Dealer _dealer;
        private readonly int _individualId;
        private readonly int _jobId;

        //Factory
        private readonly EmployeeFactory _factory;

        public EmployeeFactoryTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            //Dependencies
            _repository = new Mock<IEntityRepository>();

            //Entities
            _dealer = fixture.Create<Dealer>();
            _individualId = fixture.Create<int>();
            _jobId = fixture.Create<int>();

            //Factory
            _factory = new EmployeeFactory(_repository.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(EmployeeFactory)).HasNullGuard();
        }

        [Fact]
        public async Task Create()
        {
            using (Sequence.Create())
            {
                //Arrange
                var expectedEmployee = new DealerEmployee(_individualId, _dealer.OrganizationId, _dealer.Id, _jobId);

                _repository.Setup(r => r.Require<Dealer, DealerId>(_dealer.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_dealer);
                _repository.Setup(r => r.HasRequired<Individual>(_individualId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.HasRequired<Job>(_jobId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.Has(
                        It.Is<EmployeeByIndividualAndOrganizationSpecification<DealerEmployee>>(s =>
                            s.IndividualId == _individualId && s.OrganizationId == _dealer.OrganizationId),
                        It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(false);

                //Act
                var employee = await _factory.Create(_dealer.Id, _individualId, _jobId, CancellationToken.None);

                //Assert
                Assert.Equal(expectedEmployee, employee, PropertyComparer<DealerEmployee>.Instance);
            }
        }

        [Fact]
        public async Task Create_WhenIndividual_IsAlreadyEmployee_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Require<Dealer, DealerId>(_dealer.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(_dealer);
            _repository.Setup(r => r.Has(
                    It.Is<EmployeeByIndividualAndOrganizationSpecification<DealerEmployee>>(s =>
                        s.IndividualId == _individualId && s.OrganizationId == _dealer.OrganizationId),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.ThrowsAsync<DuplicateEmploymentException>(() =>
                _factory.Create(_dealer.Id, _individualId, _jobId, CancellationToken.None));

            //Assert
            Assert.Equal(_individualId, e.IndividualId);
            Assert.Equal(_dealer.OrganizationId, e.OrganizationId);
        }

        [Fact]
        public Task Create_GivenDealerId_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("dealerId", () =>
                _factory.Create(null, _individualId, _jobId, CancellationToken.None));
        }
    }
}
