﻿using AutoFixture;
using AutoMapper;
using Moq;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.Legal;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer
{
    public class DealerPrivateCustomerAddedEventFactoryTest
    {
        private readonly Fixture _fixture;
        private readonly PassportDto _passportDto;
        private readonly TaxIdentificationNumberDto _numberDto;
        private readonly Individual _individual;
        private readonly DealerPrivateCustomer _customer;

        private readonly DealerPrivateCustomerAddedEventFactory _factory;

        public DealerPrivateCustomerAddedEventFactoryTest()
        {
            _fixture = new Fixture();
            _fixture.Customize(new DealerIdCustomization());

            _individual = _fixture.Create<Individual>();
            _customer = _fixture.Create<DealerPrivateCustomer>();

            _passportDto = _fixture.Create<PassportDto>();
            _numberDto = _fixture.Create<TaxIdentificationNumberDto>();

            var mapper = new Mock<IMapper>();

            mapper.Setup(m => m.Map<Passport, PassportDto>(_individual.Passport)).Returns(_passportDto);
            mapper.Setup(m => m.Map<TaxIdentificationNumber, TaxIdentificationNumberDto>(_individual.TaxNumber))
                .Returns(_numberDto);

            _factory = new DealerPrivateCustomerAddedEventFactory(mapper.Object);
        }

        [Fact]
        public void Ctor_givenMapper_IsNull_Throws()
        {
            Assert.Injection.OfConstructor(typeof(DealerPrivateCustomerAddedEventFactory))
                .HasNullGuard();
        }

        [Fact]
        public void Create()
        {
            //Arrange
            var passport = _fixture.Create<Passport>();
            var taxNumber = _fixture.Create<TaxIdentificationNumber>();
            
            _individual.ChangePassport(passport);
            _individual.ChangeTaxNumber(taxNumber);

            //Act
            var result = _factory.Create(_individual, _customer);

            //Assert
            Assert.Equal(result.DealerPrivateCustomerId, _customer.Id);
            Assert.Equal(result.DealerId, _customer.DealerId);
            Assert.Equal(result.OrganizationId, _customer.OrganizationId);
            Assert.Equal(result.FirstName, _individual.Name.Firstname);
            Assert.Equal(result.LastName, _individual.Name.Lastname);
            Assert.Equal(result.MiddleName, _individual.Name.Middlename);
            Assert.Equal(result.Passport.Series, _passportDto.Series);
            Assert.Equal(result.Passport.Number, _passportDto.Number);
            Assert.Equal(result.Passport.Issuer, _passportDto.Issuer);
            Assert.Equal(result.Passport.IssuedOn, _passportDto.IssuedOn);
            Assert.Equal(result.TaxIdentificationNumber.Number, _numberDto.Number);
            Assert.Equal(result.TaxIdentificationNumber.Issue, _numberDto.Issue);
            Assert.Equal(result.TaxIdentificationNumber.RegisteredOn, _numberDto.RegisteredOn);
            Assert.Equal(result.Telephone, _customer.Telephone.E123);
            Assert.Equal(result.PhysicalAddress, _customer.PhysicalAddress.FullAddress);
        }

        [Fact]
        public void Create_WhenPassportAndTaxNumber_IsEmpty_ExpectedNulls()
        {
            //Act
            var result = _factory.Create(_individual, _customer);

            //Assert
            Assert.Null(result.Passport);
            Assert.Null(result.TaxIdentificationNumber);
        }

        [Fact]
        public void Create_givenIndividual_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("individual", () => _factory.Create(null, _customer));
        }

        [Fact]
        public void Create_givenPrivateCustomer_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("customer", () => _factory.Create(_individual, null));
        }
    }
}
