﻿using System;
using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddCorporateCustomer;
using NexCore.ContactManager.Domain.DealerCorporateCustomers;
using NexCore.ContactManager.Domain.DealerCorporateCustomers.Specifications;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.Testing;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.Customers.AddCorporateCustomer
{
    public class CorporateCustomerFactoryTest
    {
        //Dependency
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly int _contactableId;
        private readonly Dealer _dealer;

        //Factory
        private readonly DealerCorporateCustomerFactory _factory;

        public CorporateCustomerFactoryTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());
            _contactableId = fixture.Create<int>();

            //Dependency
            _repository = new Mock<IEntityRepository>();

            //Entity
            _dealer = fixture.Create<Dealer>();

            //Factory
            _factory = new DealerCorporateCustomerFactory(_repository.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DealerCorporateCustomerFactory)).HasNullGuard();
        }

        [Fact]
        public async Task Create()
        {
            using (Sequence.Create())
            {
                //Arrange
                var expected = new DealerCorporateCustomer(_contactableId, _dealer.OrganizationId, _dealer.Id);
                _repository.Setup(r => r.Require<Dealer, DealerId>(_dealer.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_dealer);
                _repository.Setup(r => r.HasRequired<Organization>(_contactableId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.Has(It.Is<ByDealerIdAndContactableIdSpecification>(
                            s => s.DealerId == _dealer.Id && s.ContactableId == _contactableId),
                        It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(false);

                //Act
                var actual = await _factory.Create(_contactableId, _dealer.Id, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<DealerCorporateCustomer>.Instance);
            }
        }

        [Fact]
        public async Task Create_WhenOrganization_IsAlreadyCustomer_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Require<Dealer, DealerId>(_dealer.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(_dealer);
            _repository.Setup(r => r.Has(It.Is<ByDealerIdAndContactableIdSpecification>(
                        s => s.DealerId == _dealer.Id && s.ContactableId == _contactableId),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.ThrowsAsync<DuplicateDealerCorporateCustomerException>(() =>
                _factory.Create(_contactableId, _dealer.Id, CancellationToken.None));

            //Assert
            Assert.Equal(_dealer.Id, e.DealerId);
            Assert.Equal(_contactableId, e.ContactableId);
        }

        [Fact]
        public Task Create_GivenDealerId_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("dealerId", () =>
                _factory.Create(_contactableId, null, CancellationToken.None));
        }
    }
}
