﻿using AutoFixture;
using NexCore.Common;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.AddEmployee;
using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.Legal;
using NexCore.Testing;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.Employees.AddEmployee
{
    public class EmployeeAddedEventFactoryTest
    {
        private readonly DealerEmployee _employee;
        private readonly PersonName _personName;
        private readonly Guid _guid;
        private readonly DateTime _dateTime;

        private readonly EmployeeAddedEventFactory _eventFactory;

        public EmployeeAddedEventFactoryTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            _employee = fixture.Create<DealerEmployee>();
            _personName = fixture.Create<PersonName>();
            _dateTime = fixture.Create<DateTime>();
            _guid = Guid.NewGuid();

            _eventFactory = new EmployeeAddedEventFactory();
        }

        [Fact]
        public void Create_GivenEmployee_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("employee", () => _eventFactory.Create(null, _personName));
        }

        [Fact]
        public void Create_GivenIndividualName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("personName", () => _eventFactory.Create(_employee, null));
        }

        [Fact]
        public void Create()
        {
            //Arrange
            var expected = new DealerEmployeeAddedEvent(_guid, _dateTime)
            {
                DealerId = _employee.DealerId.Value,
                EmployeeId = _employee.Id,
                FirstName = _personName.Firstname,
                LastName = _personName.Lastname,
                MiddleName = _personName.Middlename
            };

            using (new GuidFactoryContext(_guid))
            using (new SystemTimeContext(_dateTime))
            {
                //Act
                var actual = _eventFactory.Create(_employee, _personName);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<DealerEmployeeAddedEvent>.Instance);
            }
        }
    }
}
