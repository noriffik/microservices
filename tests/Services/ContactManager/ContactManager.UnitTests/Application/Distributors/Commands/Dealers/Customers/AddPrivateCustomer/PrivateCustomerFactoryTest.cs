﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.DealerPrivateCustomers.Specifications;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.Testing;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer
{
    public class PrivateCustomerFactoryTest
    {
        //Dependency
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Dealer _dealer;
        private readonly int _individualId;

        //Factory
        private readonly PrivateCustomerFactory _factory;

        public PrivateCustomerFactoryTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            //Dependency
            _repository = new Mock<IEntityRepository>();

            //Entity
            _dealer = fixture.Create<Dealer>();
            _individualId = fixture.Create<int>();

            //Factory
            _factory = new PrivateCustomerFactory(_repository.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(PrivateCustomerFactory)).HasNullGuard();
        }

        [Fact]
        public async Task Create()
        {
            using (Sequence.Create())
            {
                //Arrange
                var expected = new DealerPrivateCustomer(_individualId, _dealer.OrganizationId, _dealer.Id);
                _repository.Setup(r => r.Require<Dealer, DealerId>(_dealer.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_dealer);
                _repository.Setup(r => r.HasRequired<Individual>(_individualId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.Has(It.Is<ByDealerIdAndContactableIdSpecifications>(
                            s => s.DealerId == _dealer.Id && s.ContactableId == _individualId),
                        It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(false);

                //Act
                var actual = await _factory.Create(_dealer.Id, _individualId, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<DealerPrivateCustomer>.Instance);
            }
        }

        [Fact]
        public async Task Create_WhenIndividual_IsAlreadyCustomer_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Require<Dealer, DealerId>(_dealer.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(_dealer);
            _repository.Setup(r => r.Has(It.Is<ByDealerIdAndContactableIdSpecifications>(
                        s => s.DealerId == _dealer.Id && s.ContactableId == _individualId),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.ThrowsAsync<DuplicateDealerPrivateCustomerException>(() =>
                _factory.Create(_dealer.Id, _individualId, CancellationToken.None));

            //Assert
            Assert.Equal(_dealer.Id, e.DealerId);
            Assert.Equal(_individualId, e.ContactableId);
        }

        [Fact]
        public Task Create_GivenDealerId_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("dealerId", () =>
                _factory.Create(null, _individualId, CancellationToken.None));
        }
    }
}
