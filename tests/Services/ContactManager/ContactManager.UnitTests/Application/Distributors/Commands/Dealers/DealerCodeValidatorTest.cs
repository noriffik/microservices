﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers
{
    public class DealerCodeValidatorTest
    {
        private readonly DealerCodeValidator _validator = new DealerCodeValidator();

        [Theory]
        [InlineData("38001")]
        [InlineData("00000")]
        [InlineData("99999")]
        public void ShouldNotHaveError_WhenValue_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }

        [Theory]
        [InlineData("3800")]
        [InlineData("380011")]
        [InlineData("3800A")]
        [InlineData("")]
        [InlineData(" 123 ")]
        public void ShouldHaveError_WhenValue_IsInvalid(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }
    }
}
