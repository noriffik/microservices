﻿using AutoFixture;
using NexCore.Common;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.AddDealer;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Application.Organizations.Dtos;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.Legal;
using NexCore.Testing;
using NexCore.Testing.Extensions;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.AddDealer
{
    public class DealerAddedEventFactoryTest
    {
        private readonly IFixture _fixture;

        private readonly Dealer _dealer;
        private readonly Organization _organization;
        private readonly Guid _guid;
        private readonly DateTime _dateTime;

        private readonly DealerAddedEventFactory _eventFactory;

        public DealerAddedEventFactoryTest()
        {
            _fixture = new Fixture()
                .Customize(new DealerIdCustomization());

            _dealer = _fixture.Create<Dealer>();
            _organization = _fixture.Construct<Organization>(_dealer.OrganizationId);
            _dateTime = _fixture.Create<DateTime>();
            _guid = Guid.NewGuid();

            _eventFactory = new DealerAddedEventFactory(AutoMapperFactory.Create());
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Injection.OfConstructor(typeof(DealerAddedEventFactory)).HasNullGuard();
        }

        [Fact]
        public void Create_GivenDealer_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealer", () => _eventFactory.Create(null, _organization));
        }

        [Fact]
        public void Create_GivenOrganization_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("organization", () => _eventFactory.Create(_dealer, null));
        }

        [Fact]
        public void Create_WhenOrganizationRequisites_IsNotEmpty()
        {
            //Arrange
            _organization.ChangeRequisites(_fixture.Create<OrganizationRequisites>());
            var expected = new DealerAddedEvent(_guid, _dateTime)
            {
                DealerId = _dealer.Id.Value,
                DistributorId = _dealer.Id.DistributorId,
                DealerCode = _dealer.Id.DealerCode,
                OrganizationId = _dealer.OrganizationId,
                Requisites = new OrganizationRequisitesDto
                {
                    Director = new PersonNameDto
                    {
                        Firstname = _organization.Requisites.Director.Firstname,
                        Lastname = _organization.Requisites.Director.Lastname,
                        Middlename = _organization.Requisites.Director.Middlename,
                    },
                    Address = _organization.Requisites.Address,
                    Telephones = _organization.Requisites.Telephones,
                    BankRequisites = _organization.Requisites.BankRequisites,
                    FullName = _organization.Requisites.Name.FullName,
                    ShortName = _organization.Requisites.Name.ShortName,
                },
                Name = _organization.Name
            };

            using (new GuidFactoryContext(_guid))
            using (new SystemTimeContext(_dateTime))
            {
                //Act
                var actual = _eventFactory.Create(_dealer, _organization);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<DealerAddedEvent>.Instance);
            }
        }

        [Fact]
        public void Create_WhenOrganizationRequisites_IsEmpty()
        {
            //Arrange
            var expected = new DealerAddedEvent(_guid, _dateTime)
            {
                DealerId = _dealer.Id.Value,
                DistributorId = _dealer.Id.DistributorId,
                DealerCode = _dealer.Id.DealerCode,
                OrganizationId = _dealer.OrganizationId,
                Name = _organization.Name
            };

            using (new GuidFactoryContext(_guid))
            using (new SystemTimeContext(_dateTime))
            {
                //Act
                var actual = _eventFactory.Create(_dealer, _organization);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<DealerAddedEvent>.Instance);
            }
        }
    }
}
