﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Distributors.Commands;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Employees.AddEmployee;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.Employees.AddEmployee
{
    public class AddEmployeeCommandValidatorTest
    {
        private readonly AddEmployeeCommandValidator _validator = new AddEmployeeCommandValidator();

        [Fact]
        public void ShouldHaveError_WhenDistributorId_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.DistributorId, null as string);
        }

        [Fact]
        public void ShouldHaveChildValidator_DistributorIdValidator_ForDistributorId()
        {
            _validator.ShouldHaveChildValidator(c => c.DistributorId, typeof(DistributorIdValidator));
        }

        [Fact]
        public void ShouldHaveError_WhenCode_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.DealerCode, null as string);
        }

        [Fact]
        public void ShouldHaveChildValidator_DealerCodeValidator_ForCode()
        {
            _validator.ShouldHaveChildValidator(c => c.DealerCode, typeof(DealerCodeValidator));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldHaveError_WhenIndividualId_LessThan_1(int organizationId)
        {
            _validator.ShouldHaveValidationErrorFor(cp => cp.IndividualId, organizationId);
        }

        [Fact]
        public void ShouldNotHaveError_WhenIndividualId_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(cp => cp.IndividualId, 1);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldHaveError_WhenJobId_LessThan_1(int organizationId)
        {
            _validator.ShouldHaveValidationErrorFor(cp => cp.JobId, organizationId);
        }

        [Fact]
        public void ShouldNotHaveError_WhenJobId_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(cp => cp.JobId, 1);
        }
    }
}
