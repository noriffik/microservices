﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer;
using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using NexCore.Testing.Extensions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.Customers.AddPrivateCustomer
{
    public class AddPrivateCustomerCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<PrivateCustomerFactory> _customerFactory;
        private readonly Mock<IIntegrationEventStorage> _eventStorage;
        private readonly Mock<DealerPrivateCustomerAddedEventFactory> _eventFactory;

        //Entities
        private readonly DealerPrivateCustomer _customer;
        private readonly Individual _individual;
        private readonly DealerPrivateCustomerAddedEvent _event;

        //Command and handler
        private readonly AddPrivateCustomerCommand _command;
        private readonly AddPrivateCustomerCommandHandler _handler;

        public AddPrivateCustomerCommandHandlerTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            _customerFactory = new Mock<PrivateCustomerFactory>();
            _eventStorage = new Mock<IIntegrationEventStorage>();
            _eventFactory = new Mock<DealerPrivateCustomerAddedEventFactory>();

            //Entities
            _customer = fixture.Create<DealerPrivateCustomer>();
            _individual = fixture.Construct<Individual>(_customer.ContactableId);
            _event = fixture.Create<DealerPrivateCustomerAddedEvent>();

            //Command and handler
            _command = fixture.Build<AddPrivateCustomerCommand>()
                .With(c => c.DistributorId, _customer.DealerId.DistributorId.Value)
                .With(c => c.DealerCode, _customer.DealerId.DealerCode.Value)
                .With(c => c.IndividualId, _customer.ContactableId)
                .Create();
            _handler = new AddPrivateCustomerCommandHandler(
                _work.Object, _customerFactory.Object, _eventStorage.Object, _eventFactory.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddPrivateCustomerCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                //Add customer
                _customerFactory.Setup(f =>
                        f.Create(_customer.DealerId, _customer.ContactableId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_customer);
                _repository.Setup(r => r.Add(_customer))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Add event
                _repository.Setup(r => r.Require<Individual>(_customer.ContactableId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_individual);
                _eventFactory.Setup(e => e.Create(_individual, _customer))
                    .InSequence()
                    .Returns(_event);
                _eventStorage.Setup(s => s.Add(_event))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Commit
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_customer.Id, result);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
