﻿using System;
using AutoFixture;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Customers.AddCorporateCustomer;
using NexCore.ContactManager.Domain.DealerCorporateCustomers;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.Customers.AddCorporateCustomer
{
    public class DealerCorporateCustomerAddedEventFactoryTest
    {
        private readonly DealerCorporateCustomerAddedEventFactory _eventFactory;

        public DealerCorporateCustomerAddedEventFactoryTest()
        {
            _eventFactory = new DealerCorporateCustomerAddedEventFactory();   
        }

        [Fact]
        public void Create()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customize(new DealerIdCustomization());

            var corporateCustomer = fixture.Create<DealerCorporateCustomer>();

            //Act
            var result = _eventFactory.Create(corporateCustomer);

            //Assert
            Assert.Equal(corporateCustomer.Id, result.DealerCorporateCustomerId);
            Assert.Equal(corporateCustomer.DealerId, result.DealerId);
            Assert.Equal(corporateCustomer.ContactableId, result.ContactableId);
            Assert.Equal(corporateCustomer.OrganizationId, result.OrganizationId);
        }

        [Fact]
        public void Create_GivenCorporateCustomer_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("corporateCustomer", () => _eventFactory.Create(null));
        }
    }
}
