﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.UpdateDealer;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.UpdateDealer
{
    public class UpdateDealerCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;

        //Entities
        private readonly Mock<Dealer> _dealer;

        //Command and handler
        private readonly UpdateDealerCommand _command;
        private readonly UpdateDealerCommandHandler _handler;

        public UpdateDealerCommandHandlerTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            var repository = new Mock<IEntityRepository>();

            //Entities
            var dealerId = fixture.Create<DealerId>();
            _dealer = new Mock<Dealer>(dealerId, fixture.Create<int>());

            //Setup dependencies
            _work.Setup(w => w.EntityRepository).Returns(repository.Object);
            repository.Setup(r => r.Require<Dealer, DealerId>(dealerId, It.IsAny<CancellationToken>()))
                .ReturnsAsync(_dealer.Object);

            //Command and handler
            _command = fixture.Build<UpdateDealerCommand>()
                .With(c => c.DistributorId, dealerId.DistributorId.Value)
                .With(c => c.Code, dealerId.DealerCode.Value)
                .Create();
            _handler = new UpdateDealerCommandHandler(_work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(UpdateDealerCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            using (Sequence.Create())
            {
                _dealer.Setup(d => d.ChangeCityCode(_command.CityCode))
                    .InSequence();
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
