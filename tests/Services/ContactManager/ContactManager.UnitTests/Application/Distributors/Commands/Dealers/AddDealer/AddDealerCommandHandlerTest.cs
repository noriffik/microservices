﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.AddDealer;
using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Dealers.Specifications;
using NexCore.ContactManager.Domain.Distributors;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using NexCore.Testing.Extensions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using ByOrganizationIdSpecification = NexCore.ContactManager.Domain.Distributors.Specifications.ByOrganizationIdSpecification;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.AddDealer
{
    public class AddDealerCommandHandlerTest
    {
        //Entity
        private readonly Organization _organization;
        private readonly Dealer _dealer;
        private readonly DealerAddedEvent _event;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IIntegrationEventStorage> _eventStore;
        private readonly Mock<IMapper> _mapper;

        //Handler and command
        private readonly AddDealerCommand _command;
        private readonly AddDealerCommandHandler _handler;

        public AddDealerCommandHandlerTest()
        {
            var fixture = new Fixture()
                .Customize(new DealerIdCustomization());

            //Entity
            _dealer = fixture.Create<Dealer>();
            _organization = fixture.Construct<Organization>(_dealer.OrganizationId);
            _event = fixture.Create<DealerAddedEvent>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _eventStore = new Mock<IIntegrationEventStorage>();
            _mapper = new Mock<IMapper>();
            var eventFactory = new Mock<IDealerAddedEventFactory>();

            //Handler and command
            _handler = new AddDealerCommandHandler(_work.Object, _mapper.Object, _eventStore.Object)
            {
                EventFactory = eventFactory.Object
            };
            _command = fixture.Build<AddDealerCommand>()
                .With(c => c.Code, _dealer.DealerCode.Value)
                .With(c => c.DistributorId, _dealer.DistributorId.Value)
                .With(c => c.OrganizationId, _dealer.OrganizationId)
                .Create();

            //Setup dependencies
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            _mapper.Setup(m => m.Map<Dealer>(_command)).Returns(_dealer);
            eventFactory.Setup(f => f.Create(_dealer, _organization)).Returns(_event);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddDealerCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _mapper.Setup(m => m.Map<Dealer>(_command))
                    .InSequence()
                    .Returns(_dealer);
                _repository.Setup(r => r.HasRequired<Distributor, DistributorId>(_dealer.DistributorId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.Has<Distributor, DistributorId>(It.Is<ByOrganizationIdSpecification>(s => s.OrganizationId == _command.OrganizationId), It.IsAny<CancellationToken>()))
                   .InSequence()
                   .ReturnsAsync(false);
                _repository.Setup(r => r.Has<Dealer, DealerId>(_dealer.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Has<Dealer, DealerId>(
                        It.Is<ByOrganizationAndDistributorSpecification>(s => s.OrganizationId == _dealer.OrganizationId && s.DistributorId == _dealer.DistributorId),
                        It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Require<Organization>(_command.OrganizationId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_organization);
                _repository.Setup(r => r.Add<Dealer, DealerId>(_dealer))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                
                //Publish event
                _eventStore.Setup(e => e.Add(_event))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Commit work
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_dealer.Id, result);
            }
        }

        [Fact]
        public async Task Handle_WhenOrganization_IsAlreadyDistributor_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Has<Distributor, DistributorId>(
                    It.Is<ByOrganizationIdSpecification>(s => s.OrganizationId == _command.OrganizationId), It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.OrganizationId), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("organization is already an distributor");
        }

        [Fact]
        public async Task Handle_WhenDealer_AlreadyExist_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Has<Dealer, DealerId>(_dealer.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.Code), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("already has dealer with code");
        }

        [Fact]
        public async Task Handle_WhenOrganization_AlreadyIsDealer_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Has<Dealer, DealerId>(
                    It.Is<ByOrganizationAndDistributorSpecification>(s => s.OrganizationId == _dealer.OrganizationId && s.DistributorId == _dealer.DistributorId),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.OrganizationId), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("already has Dealer for Organization with id");
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, It.IsAny<CancellationToken>()));
        }

        [Fact]
        public void EventFactory()
        {
            //Arrange
            var handler = new AddDealerCommandHandler(_work.Object, new Mock<IMapper>().Object, _eventStore.Object);

            //Assert
            Assert.Injection.OfProperty(handler, nameof(handler.EventFactory))
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}
