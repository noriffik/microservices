﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers.Leads.OpenLead;
using NexCore.ContactManager.Domain.DealerEmployees.Specifications;
using NexCore.ContactManager.Domain.Dealers;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.Domain.Leads;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers.Leads.OpenLead
{
    public class OpenLeadCommandHandlerTest
    {
        private readonly IFixture _fixture;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IMapper> _mapper;

        //Command and handler
        private readonly OpenLeadCommand _command;
        private readonly OpenLeadCommandHandler _handler;

        public OpenLeadCommandHandlerTest()
        {
            _fixture = new Fixture().Customize(new DealerIdCustomization());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            _mapper = new Mock<IMapper>();

            //Command and handler
            _command = _fixture.Create<OpenLeadCommand>();
            _handler = new OpenLeadCommandHandler(_work.Object, _mapper.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(OpenLeadCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request",
                () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_WhenOrganizationId_AndEmployeeId_IsSet()
        {
            var lead = _fixture.Construct<Lead>(new { organizationId = _fixture.Create<int>() });
            lead.AssignOwner(_fixture.Create<int>());
            using (Sequence.Create())
            {
                //Map
                _mapper.Setup(m => m.Map<Lead>(_command))
                    .InSequence()
                    .Returns(lead);

                //HasRequired
                _repository.Setup(r => r.HasRequired<Dealer, DealerId>(lead.DealerId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.HasRequired<Topic>(lead.TopicId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.HasRequired<Individual>(lead.Contact.IndividualId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.HasRequired<Organization>(lead.Contact.OrganizationId.Value, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.HasRequired(It.Is<ByDealerIdAndEmployeeIdSpecification>(
                        s => s.DealerId == lead.DealerId && s.DealerEmployeeId == lead.OwnerEmployeeId), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Add lead
                _repository.Setup(r => r.Add(lead))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(lead.Id, result);
            }
        }

        [Fact]
        public async Task Handle_WhenOrganizationId_And_EmployeeId_IsNotSet()
        {
            var lead = _fixture.Construct<Lead>(new
            {
                organizationId = (int?)null
            });
            using (Sequence.Create())
            {
                //Map
                _mapper.Setup(m => m.Map<Lead>(_command))
                    .InSequence()
                    .Returns(lead);

                //HasRequired
                _repository.Setup(r => r.HasRequired<Dealer, DealerId>(lead.DealerId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.HasRequired<Topic>(lead.TopicId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.HasRequired<Individual>(lead.Contact.IndividualId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Add lead
                _repository.Setup(r => r.Add(lead))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(lead.Id, result);
            }
        }
    }
}
