﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Distributors.Commands.Dealers;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Distributors.Commands.Dealers
{
    public class DealerIdValidatorTest
    {
        private readonly DealerIdValidator _validator = new DealerIdValidator();

        [Theory]
        [InlineData("EKK38001")]
        [InlineData("00000000")]
        [InlineData("ekk99999")]
        public void ShouldNotHaveError_WhenValue_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }

        [Theory]
        [InlineData("EKK3800")]
        [InlineData("EKK380011")]
        [InlineData("EKK3800A")]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(" EKK123 ")]
        public void ShouldHaveError_WhenValue_IsInvalid(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }
    }
}
