﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Contacts.Dtos
{
    public class PhysicalAddressDtoValidatorTest
    {
        private readonly PhysicalAddressElementDto _addressElement = new PhysicalAddressElementDto();
        private readonly GeolocationDto _geolocation = new GeolocationDto();
        private readonly PhysicalAddressDtoValidator _validator = new PhysicalAddressDtoValidator();
        
        [Fact]
        public void ShouldHaveError_When_Street_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Street, string.Empty);
            _validator.ShouldHaveValidationErrorFor(m => m.Street, null as string);
        }

        [Fact]
        public void ShouldNotHaveError_When_Street_IsNotEmpty()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Street, "Street");
        }
        
        [Fact]
        public void ShouldHaveError_When_City_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.City, null as PhysicalAddressElementDto);
        }

        [Fact]
        public void ShouldNotHaveError_When_City_IsNotEmpty()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.City, _addressElement);
        }

        [Fact]
        public void ShouldHaveChildValidator_PhysicalAddressElementValidator__ForCity()
        {
            _validator.ShouldHaveChildValidator(v => v.City, typeof(PhysicalAddressElementDtoValidator));
        }

        [Fact]
        public void ShouldHaveError_When_Region_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Region, null as PhysicalAddressElementDto);
        }

        [Fact]
        public void ShouldNotHaveError_When_Region_IsNotEmpty()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Region, _addressElement);
        }

        [Fact]
        public void ShouldHaveChildValidator_PhysicalAddressElementValidator_ForRegion()
        {
            _validator.ShouldHaveChildValidator(v => v.Region, typeof(PhysicalAddressElementDtoValidator));
        }

        [Fact]
        public void ShouldHaveError_When_Country_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Country, null as PhysicalAddressElementDto);
        }

        [Fact]
        public void ShouldNotHaveError_When_Country_IsNotEmpty()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Country, _addressElement);
        }

        [Fact]
        public void ShouldHaveChildValidator_PhysicalAddressElementValidator_ForCountry()
        {
            _validator.ShouldHaveChildValidator(v => v.Country, typeof(PhysicalAddressElementDtoValidator));
        }

        [Fact]
        public void ShouldHave_GeolocationValidator_ForGeolocation()
        {
            _validator.ShouldHaveChildValidator(v => v.Geolocation, typeof(GeolocationDtoValidator));
        }

        [Fact]
        public void ShouldNotHaveError_When_Geolocation_IsNotEmpty()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Geolocation, _geolocation);
        }

        [Fact]
        public void ShouldNotHaveError_When_Geolocation_IsNull()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Geolocation, null as GeolocationDto);
        }

        [Fact]
        public void ShouldHave_AddressElementValidator_ForCountry()
        {
            _validator.ShouldHaveChildValidator(v => v.Country, typeof(PhysicalAddressElementDtoValidator));
        }

        [Fact]
        public void ShouldHaveError_When_PostCode_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.PostCode, string.Empty);
            _validator.ShouldHaveValidationErrorFor(m => m.PostCode, null as string);
        }

        [Fact]
        public void ShouldNotHaveError_When_PostCode_IsNotEmpty()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.PostCode, "PostCode");
        }

        [Fact]
        public void ShouldNotHaveError_When_Description_IsEmpty()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Description, string.Empty);
            _validator.ShouldNotHaveValidationErrorFor(m => m.Description, null as string);
        }

        [Fact]
        public void ShouldNotHaveError_When_Description_IsNotEmpty()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Description, "Street");
        }
    }
}
