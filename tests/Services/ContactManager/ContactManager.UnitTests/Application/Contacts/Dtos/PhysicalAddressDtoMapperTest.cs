﻿using AutoFixture;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.Domain;
using System.Collections.Generic;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Contacts.Dtos
{
    public class PhysicalAddressDtoMapperTest : BaseDtoMapperTest<PhysicalAddressDto, IContactPoint<PhysicalAddress>>
    {
        private readonly PhysicalAddressDtoMapper _mapper = new PhysicalAddressDtoMapper();

        protected override IMapper<PhysicalAddressDto, IContactPoint<PhysicalAddress>> Mapper => _mapper;

        protected override IEqualityComparer<IContactPoint<PhysicalAddress>> Comparer => new ContactPointComparer<PhysicalAddress>();

        [Fact]
        public void Map()
        {
            //Arrange
            var model = Fixture.Create<PhysicalAddressDto>();

            //Act
            var point = _mapper.Map(model);

            //Assert
            Assert.NotNull(point);
            Assert.Equal(point.Value, new PhysicalAddress(
                model.Street,
                model.City.Code, model.City.Name,
                model.Region.Code, model.Region.Name,
                model.Country.Code, model.Country.Name,
                model.Geolocation.Latitude, model.Geolocation.Longitude,
                model.PostCode));
            Assert.Equal(point.Description, model.Description);
            Assert.Equal(point.IsPrimary, model.IsPrimary);
        }

        [Fact]
        public virtual void Map_GivenModel_HasNo_CitySet_AssignsEmpty()
        {
            //Arrange
            var model = Fixture.Create<PhysicalAddressDto>();
            model.City = null;

            //Act
            var point = Mapper.Map(model);
            
            //Assert
            Assert.Equal(PhysicalAddressCodedElement.Empty, point.Value?.City);
        }

        [Fact]
        public virtual void Map_GivenModel_HasNo_RegionSet_AssignsEmpty()
        {
            //Arrange
            var model = Fixture.Create<PhysicalAddressDto>();
            model.Region = null;

            //Act
            var point = Mapper.Map(model);

            //Assert
            Assert.Equal(PhysicalAddressCodedElement.Empty, point.Value?.Region);
        }

        [Fact]
        public virtual void Map_GivenModel_HasNo_CountrySet_AssignsEmpty()
        {
            //Arrange
            var model = Fixture.Create<PhysicalAddressDto>();
            model.Country = null;

            //Act
            var point = Mapper.Map(model);

            //Assert
            Assert.Equal(PhysicalAddressCodedElement.Empty, point.Value?.Country);
        }

        [Fact]
        public virtual void Map_GivenModel_HasNo_GeolocationSet_AssignsEmpty()
        {
            //Arrange
            var model = Fixture.Create<PhysicalAddressDto>();
            model.Geolocation = null;

            //Act
            var point = Mapper.Map(model);

            //Assert
            Assert.Equal(Geolocation.Empty, point.Value?.Geolocation);
        }
    }
}
