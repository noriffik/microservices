﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Domain.Contacts;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Contacts.Dtos
{
    public class TelephoneDtoValidatorTest
    {
        private readonly TelephoneDtoValidator _validator = new TelephoneDtoValidator();

        [Fact]
        public void ShouldHaveChildValidator_TelephoneNumberValidator_ForNumber()
        {
            _validator.ShouldHaveChildValidator(c => c.Number, typeof(TelephoneNumberDtoValidator));
        }

        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        public void ShouldHaveError_When_Telephone_IsInvalid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Type, (TelephoneType)value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public void ShouldNotHaveError_When_Telephone_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Type, (TelephoneType)value);
        }
    }
}
