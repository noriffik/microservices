﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Contacts.Dtos
{
    public class GeolocationDtoValidatorTest
    {
        private readonly GeolocationDtoValidator _validator = new GeolocationDtoValidator();

        [Fact]
        public void ShouldNotHaveError_When_Latitude_AndLongitude_IsEmpty()
        {
            var model = new GeolocationDto
            {
                Latitude = null,
                Longitude = null
            };

            _validator.ShouldNotHaveValidationErrorFor(m => m.Latitude, model);
            _validator.ShouldNotHaveValidationErrorFor(m => m.Longitude, model);
        }

        [Fact]
        public void ShouldNotHaveError_When_Latitude_AndLongitude_IsNot_NullOrEmpty()
        {
            _validator.ShouldNotHaveValidationErrorFor(
                m => m.Latitude,
                new GeolocationDto
                {
                    Latitude = 1.5,
                    Longitude = 1.7
                });
        }

        [Fact]
        public void ShouldHaveError_When_Latitude_IsEmpty_AndLongitude_IsNotEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Latitude, new GeolocationDto
            {
                Latitude = null,
                Longitude = 1
            });
        }

        [Fact]
        public void ShouldNotHaveError_When_Latitude_IsZero()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Latitude, new GeolocationDto
            {
                Latitude = 0,
                Longitude = 33
            });
        }

        [Fact]
        public void ShouldHaveError_When_Longitude_IsEmpty_AndLatitude_IsNotEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Longitude, new GeolocationDto
            {
                Latitude = 1,
                Longitude = null
            });
        }

        [Fact]
        public void ShouldNotHaveError_When_Longitude_IsZero()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Longitude, new GeolocationDto
            {
                Latitude = 1,
                Longitude = 0
            });
        }
    }
}
