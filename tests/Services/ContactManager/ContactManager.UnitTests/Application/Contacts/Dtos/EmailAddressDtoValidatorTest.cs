﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Contacts.Dtos
{
    public class EmailAddressDtoValidatorTest
    {
        //Arrange
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("][-")]
        [InlineData("12345")]
        [InlineData("test")]
        [InlineData("test@")]
        [InlineData("test@gmail")]
        [InlineData("@gmail.com")]
        public void ShouldHaveError_When_Email_IsInvalid(string value)
        {
            //Act
            var validator = new EmailAddressDtoValidator();

            //Assert
            validator.ShouldHaveValidationErrorFor(m => m.Address, value);
        }

        [Theory]
        [InlineData("test@gmail.com")]
        [InlineData("test@i.ua")]
        public void ShouldNotHaveError_When_Email_IsValid(string value)
        {
            //Act
            var validator = new EmailAddressDtoValidator();

            //Assert
            validator.ShouldNotHaveValidationErrorFor(m => m.Address, value);
        }
    }
}
