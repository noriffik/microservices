﻿using AutoFixture;
using Moq;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.Domain;
using NexCore.Testing;
using System;
using System.Collections.Generic;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Contacts.Dtos
{
    public class ContactDtoMapperTest
    {
        private class Contactable : IContactable
        {
            public int Id { get; set; }

            public ContactInfo Contact { get; }

            public Contactable()
            {
                Contact = new ContactInfo();
            }
        }

        private readonly Fixture _fixture = new Fixture();
        private readonly Mock<IMapper<PhysicalAddressDto, IContactPoint<PhysicalAddress>>> _addressModelMapper;
        private readonly Mock<IMapper<TelephoneDto, IContactPoint<Telephone>>> _telephoneModelMapper;
        private readonly Mock<IMapper<EmailAddressDto, IContactPoint<EmailAddress>>> _emailModelMapper;
        private readonly ContactDto _model;
        private readonly Contactable _contactable;
        private readonly ContactDtoMapper _mapper;

        public ContactDtoMapperTest()
        {
            _model = SetupCommand();
            _contactable = SetupContactable();

            _addressModelMapper = SetupModelMapper<PhysicalAddress, PhysicalAddressDto>(
                _model.Addresses, _contactable.Contact);
            _telephoneModelMapper = SetupModelMapper<Telephone, TelephoneDto>(
                _model.Telephones, _contactable.Contact);
            _emailModelMapper = SetupModelMapper<EmailAddress, EmailAddressDto>(
                _model.Emails, _contactable.Contact);

            _mapper = new ContactDtoMapper();
            _mapper.SetMapper(_addressModelMapper.Object);
            _mapper.SetMapper(_telephoneModelMapper.Object);
            _mapper.SetMapper(_emailModelMapper.Object);
        }

        private ContactDto SetupCommand()
        {
            return new ContactDto
            {
                Addresses = new[] {_fixture.Create<PhysicalAddressDto>()},
                Telephones = new[] {_fixture.Create<TelephoneDto>()},
                Emails = new[] {_fixture.Create<EmailAddressDto>()},
            };
        }

        private Contactable SetupContactable()
        {
            var contactable = new Contactable();

            contactable.Contact.Add(_fixture.Create<PhysicalAddress>());
            contactable.Contact.Add(_fixture.Create<Telephone>());
            contactable.Contact.Add(_fixture.Create<EmailAddress>());

            return contactable;
        }

        private Mock<IMapper<TModel, IContactPoint<TValue>>> SetupModelMapper<TValue, TModel>(
            IEnumerable<TModel> models, ContactInfo contactInfo)
            where TValue : ValueObject
            where TModel : class
        {
            var mapper = new Mock<IMapper<TModel, IContactPoint<TValue>>>();
            mapper
                .Setup(m => m.MapMany(models))
                .Returns(contactInfo.GetContactPoints<TValue>());

            return mapper;
        }

        [Fact]
        public void MapReserve()
        {
            //Arrange
            var actual = new Contactable();

            //Act
            _mapper.Map(_model, actual);

            //Assert
            Assert.Equal(
                _contactable.Contact.PhysicalAddresses,
                actual.Contact.PhysicalAddresses,
                ContactPointComparer<PhysicalAddress>.Instance);
            Assert.Equal(
                _contactable.Contact.Telephones,
                actual.Contact.Telephones,
                ContactPointComparer<Telephone>.Instance);
            Assert.Equal(
                _contactable.Contact.EmailAddresses,
                actual.Contact.EmailAddresses,
                ContactPointComparer<EmailAddress>.Instance);
        }

        [Fact]
        public void MapReserve_GivenCommand_Addresses_IsNull()
        {
            //Arrange
            _model.Addresses = null;
            var contactable = new Contactable();

            //Act
            _mapper.Map(_model, contactable);

            //Assert
            Assert.Empty(contactable.Contact.PhysicalAddresses);
            _addressModelMapper.Verify(
                m => m.MapMany(It.IsAny<IEnumerable<PhysicalAddressDto>>()), Times.Never());
        }

        [Fact]
        public void MapReserve_GivenCommand_Telephones_IsNull()
        {
            //Arrange
            _model.Telephones = null;
            var contactable = new Contactable();

            //Act
            _mapper.Map(_model, contactable);

            //Assert
            Assert.Empty(contactable.Contact.Telephones);
            _telephoneModelMapper.Verify(
                m => m.MapMany(It.IsAny<IEnumerable<TelephoneDto>>()), Times.Never());
        }

        [Fact]
        public void MapReserve_GivenCommand_MailAddresses_IsNull()
        {
            //Arrange
            _model.Emails = null;
            var contactable = new Contactable();

            //Act
            _mapper.Map(_model, contactable);

            //Assert
            Assert.Empty(contactable.Contact.EmailAddresses);
            _emailModelMapper.Verify(
                m => m.MapMany(It.IsAny<IEnumerable<EmailAddressDto>>()), Times.Never());
        }

        [Fact]
        public void MapReserve_GivenCommand_IsNull_Throws()
        {
            //Arrange
            var mapper = new ContactDtoMapper();

            //Assert
            Assert.Throws<ArgumentNullException>("model", () =>
            {
                mapper.Map(null, new Contactable());
            });
        }

        [Fact]
        public void MapReserve_GivenContactable_IsNull_Throws()
        {
            //Arrange
            var mapper = new ContactDtoMapper();

            //Assert
            Assert.Throws<ArgumentNullException>("contactable", () =>
            {
                mapper.Map(new ContactDto(), null);
            });
        }

        [Fact]
        public void GetModelMapper_WhenIsNotSet_ReturnsDefault()
        {
            //Arrange
            var mapper = new ContactDtoMapper();

            //Assert
            Assert.IsType<PhysicalAddressDtoMapper>(mapper.GetMapper<PhysicalAddressDto, PhysicalAddress>());
            Assert.IsType<TelephoneDtoMapper>(mapper.GetMapper<TelephoneDto, Telephone>());
            Assert.IsType<EmailAddressDtoMapper>(mapper.GetMapper<EmailAddressDto, EmailAddress>());
        }

        [Fact]
        public void GetModelMapper_WhenTypeIsNotSupported_Throws()
        {
            //Arrange
            var mapper = new ContactDtoMapper();

            //Assert
            Assert.Throws<NotSupportedException>(() =>
            {
                mapper.GetMapper<string, TestValueObject>();
            });
        }

        [Fact]
        public void SetModelMapper_GivenMapper_IsNull_Throws()
        {
            //Arrange
            var mapper = new ContactDtoMapper();

            //Assert
            Assert.Throws<ArgumentNullException>(() =>
            {
                mapper.SetMapper<PhysicalAddressDto, PhysicalAddress>(null);
            });
        }
    }
}
