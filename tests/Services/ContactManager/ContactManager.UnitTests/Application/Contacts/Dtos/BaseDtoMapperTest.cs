﻿using AutoFixture;
using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Contacts.Dtos
{
    public abstract class BaseDtoMapperTest<TFrom, TTo>
        where TFrom : class
        where TTo : class
    {
        protected readonly Fixture Fixture = new Fixture();

        protected abstract IMapper<TFrom, TTo> Mapper { get; }

        protected abstract IEqualityComparer<TTo> Comparer { get; }

        [Fact]
        public virtual void Map_GivenModel_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("source", () =>
            {
                Mapper.Map(null);
            });
        }

        [Fact]
        public virtual void MapMany()
        {
            //Arrange
            var models = Fixture.CreateMany<TFrom>(3).ToList();
            var expected = models.Select(m => Mapper.Map(m));

            //Act
            var actual = Mapper.MapMany(models);

            //Assert
            Assert.Equal(expected, actual, Comparer);
        }

        [Fact]
        public virtual void MapMany_GivenModels_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("source", () =>
            {
                Mapper.MapMany(null);
            });
        }
    }
}