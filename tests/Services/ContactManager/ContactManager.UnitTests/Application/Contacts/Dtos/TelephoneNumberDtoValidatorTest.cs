﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Contacts.Dtos
{
    public class TelephoneNumberDtoValidatorTest
    {
        //CountryCode
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("][-")]
        [InlineData("Type75115c65-b216-44be-af81-c47d80057890")]
        public void ShouldHaveError_When_CountryCode_IsInvalid(string value)
        {
            //Arrange
            var validator = new TelephoneNumberDtoValidator();

            //Assert
            validator.ShouldHaveValidationErrorFor(m => m.CountryCode, value);
        }

        [Theory]
        [InlineData("0")]
        [InlineData("4830")]
        [InlineData("1234567890")]
        public void ShouldNotHaveError_When_CountryCode_IsNumeric(string value)
        {
            //Arrange
            var validator = new TelephoneNumberDtoValidator();

            //Assert
            validator.ShouldNotHaveValidationErrorFor(m => m.CountryCode, value);
        }

        //AreaCode
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("][-")]
        [InlineData("test")]
        public void ShouldHaveError_When_AreaCode_IsInvalid(string value)
        {
            //Arrange
            var validator = new TelephoneNumberDtoValidator();

            //Assert
            validator.ShouldHaveValidationErrorFor(m => m.AreaCode, value);
        }

        [Theory]
        [InlineData("0")]
        [InlineData("4830")]
        [InlineData("1234567890")]
        public void ShouldNotHaveError_When_AreaCode_IsNumeric(string value)
        {
            //Arrange
            var validator = new TelephoneNumberDtoValidator();

            //Assert
            validator.ShouldNotHaveValidationErrorFor(m => m.AreaCode, value);
        }

        //LocalNumber
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("][-")]
        [InlineData("test")]
        public void ShouldHaveError_When_LocalNumber_IsInvalid(string value)
        {
            //Arrange
            var validator = new TelephoneNumberDtoValidator();

            //Assert
            validator.ShouldHaveValidationErrorFor(m => m.LocalNumber, value);
        }

        [Theory]
        [InlineData("0")]
        [InlineData("4830")]
        [InlineData("1234567890")]
        public void ShouldNotHaveError_When_LocalNumber_IsNumeric(string value)
        {
            //Arrange
            var validator = new TelephoneNumberDtoValidator();

            //Assert
            validator.ShouldNotHaveValidationErrorFor(m => m.LocalNumber, value);
        }
    }
}
