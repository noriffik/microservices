﻿using AutoFixture;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.Domain;
using System.Collections.Generic;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Contacts.Dtos
{
    public class EmailAddressDtoMapperTest : BaseDtoMapperTest<EmailAddressDto, IContactPoint<EmailAddress>>
    {
        private readonly EmailAddressDtoMapper _mapper = new EmailAddressDtoMapper();

        protected override IMapper<EmailAddressDto, IContactPoint<EmailAddress>> Mapper => _mapper;

        protected override IEqualityComparer<IContactPoint<EmailAddress>> Comparer => new ContactPointComparer<EmailAddress>();

        [Fact]
        public void Map()
        {
            //Arrange
            var model = Fixture.Create<EmailAddressDto>();

            //Act
            var point = _mapper.Map(model);

            //Assert
            Assert.NotNull(point);
            Assert.Equal(model.Description, point.Description);
            Assert.Equal(new EmailAddress(model.Address), point.Value);
            Assert.Equal(model.IsPrimary, point.IsPrimary);
        }
    }
}
