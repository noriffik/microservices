﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Contacts.Dtos
{
    public class ContactDtoValidatorTest
    {
        private readonly ContactDtoValidator _validator = new ContactDtoValidator();

        [Fact]
        public void ShouldHaveChildValidator_TelephoneModelValidator_ForTelephones()
        {
            _validator.ShouldHaveChildValidator(c => c.Telephones, typeof(TelephoneDtoValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_EmailAddressModelValidator_ForEmails()
        {
            _validator.ShouldHaveChildValidator(c => c.Emails, typeof(EmailAddressDtoValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_PhysicalAddressModelValidator_ForAddresses()
        {
            _validator.ShouldHaveChildValidator(c => c.Addresses, typeof(PhysicalAddressDtoValidator));
        }
    }
}
