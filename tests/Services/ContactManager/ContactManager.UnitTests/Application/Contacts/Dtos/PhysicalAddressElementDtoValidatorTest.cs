﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Contacts.Dtos
{
    public class PhysicalAddressElementDtoValidatorTest
    {
        private readonly PhysicalAddressElementDtoValidator _validator = new PhysicalAddressElementDtoValidator();

        [Fact]
        public void ShouldHaveError_When_Name_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Name, string.Empty);
            _validator.ShouldHaveValidationErrorFor(m => m.Name, null as string);
        }

        [Fact]
        public void ShouldNotHaveError_When_Name_IsNotEmpty()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Name, "name");
        }

        [Fact]
        public void ShouldNotHaveError_When_Code_IsNullOrEmpty()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Code, string.Empty);
            _validator.ShouldNotHaveValidationErrorFor(m => m.Code, null as string);
        }
    }
}
