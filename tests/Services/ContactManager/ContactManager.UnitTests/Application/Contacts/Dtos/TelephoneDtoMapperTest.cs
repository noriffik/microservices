﻿using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.Domain;
using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Contacts.Dtos
{
    public class TelephoneDtoMapperTest : BaseDtoMapperTest<TelephoneDto, IContactPoint<Telephone>>
    {
        private readonly TelephoneDtoMapper _mapper = new TelephoneDtoMapper();

        protected override IMapper<TelephoneDto, IContactPoint<Telephone>> Mapper => _mapper;

        protected override IEqualityComparer<IContactPoint<Telephone>> Comparer => new ContactPointComparer<Telephone>();
        
        [Theory]
        [ClassData(typeof(TestData))]
        public void Map(TelephoneDto model, Telephone telephone)
        {
            //Act
            var point = _mapper.Map(model);

            //Assert
            Assert.NotNull(point);
            Assert.Equal(model.Description, point.Description);
            Assert.Equal(telephone, point.Value);
            Assert.Equal(model.IsPrimary, model.IsPrimary);
        }

        class TestData : IEnumerable<object[]>
        {
            private const string CountryCode = "38";
            private const string AreaCode = "044";
            private const string LocalNumber = "1112233";

            private readonly List<object[]> _data = new List<object[]>
            {
                new object[] {
                    new TelephoneDto
                    {
                        Number = new TelephoneNumberDto
                        {
                            CountryCode = CountryCode,
                            AreaCode = AreaCode,
                            LocalNumber = LocalNumber,
                        },
                        Type = TelephoneType.Mobile,
                        IsPrimary = true,
                        Description = "Description"
                    },
                    new Telephone(
                        CountryCode, AreaCode, LocalNumber, TelephoneType.Mobile)
                },
                new object[] {
                    new TelephoneDto
                    {
                        Number = new TelephoneNumberDto
                        {
                            CountryCode = CountryCode,
                            AreaCode = AreaCode,
                            LocalNumber = LocalNumber,
                        },
                        Type = TelephoneType.Landline
                    },
                    new Telephone(
                        CountryCode, AreaCode, LocalNumber, TelephoneType.Landline)
                },
                new object[] {
                    new TelephoneDto
                    {
                        Number = new TelephoneNumberDto
                        {
                            CountryCode = "38",
                            AreaCode = "050",
                            LocalNumber = "9998811"
                        }
                    },
                    new Telephone("38", "050", "9998811")
                }
            };

            public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
    }
}
