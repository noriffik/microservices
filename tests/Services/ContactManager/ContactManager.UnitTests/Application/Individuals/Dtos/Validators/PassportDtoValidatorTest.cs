﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.Validators
{
    public class PassportDtoValidatorTest
    {
        private readonly PassportDtoValidator _validator = new PassportDtoValidator();

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void ShouldHaveError_WhenSeries_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Series, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenSeries_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Series, "Series");
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void ShouldHaveError_WhenNumber_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Number, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenNumber_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Number, "Number");
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void ShouldHaveError_WhenIssuer_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Issuer, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenIssuer_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Issuer, "Issuer");
        }
    }
}
