﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.Validators
{
    public class LegalIndividualNameDtoValidatorTest
    {
        private readonly LegalIndividualNameDtoValidator _validator = new LegalIndividualNameDtoValidator();

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void ShouldHaveError_WhenFirstname_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Firstname, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenFirstname_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Firstname, "Firstname");
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void ShouldHaveError_WhenLastname_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Lastname, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenLastname_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Lastname, "Lastname");
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void ShouldHaveError_WhenMiddlename_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Middlename, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenMiddlename_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Middlename, "Middlename");
        }
    }
}
