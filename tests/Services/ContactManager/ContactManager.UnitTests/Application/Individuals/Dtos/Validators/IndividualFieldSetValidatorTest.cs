﻿using FluentValidation.TestHelper;
using Moq;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Providers;
using NexCore.ContactManager.Domain.Individuals;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.Validators
{
    public class IndividualFieldSetValidatorTest
    {
        private readonly IndividualFieldSetValidator<IndividualFieldSet> _validator;

        public IndividualFieldSetValidatorTest()
        {
            var languageProvider = new Mock<ILanguageProvider>();

            _validator = new Mock<IndividualFieldSetValidator<IndividualFieldSet>>(languageProvider.Object)
            {
                CallBase = true
            }
            .Object;
        }

        [Fact]
        public void ShouldHaveValidationError_WhenName_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Name, null as PersonNameDto);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        public void ShouldHaveError_WhenGender_IsInvalid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Gender, (Gender)value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(null)]
        public void ShouldNotHaveError_WhenGender_IsValidOrEmpty(int? value)
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Gender, (Gender?)value);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(5)]
        public void ShouldHaveValidationError_WhenInvalidFamilyStatus(int familyStatus)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.FamilyStatus, familyStatus);
        }

        [Theory]
        [InlineData(null)]
        [InlineData(1)]
        [InlineData(4)]
        public void ShouldNotHaveValidationError_ForFamilyStatus(int? familyStatus)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.FamilyStatus, familyStatus);
        }

        [Fact]
        public void ShouldNotHaveValidationError_WhenPreferences_IsNull()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Preferences, null as PersonalPreferencesDto);
        }

        [Fact]
        public void ShouldHaveValidationError_WhenChildren_IsLessThanZero()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Children, -1);
        }

        [Theory]
        [InlineData(null)]
        [InlineData(0)]
        [InlineData(4)]
        public void ShouldNotHaveValidationError_WhenChildren_IsNull_OrPositiveNumber(int? children)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Children, children);
        }

        [Fact]
        public void ShouldNotHaveValidationError_WhenPassport_IsNull()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Passport, null as PassportDto);
        }

        [Fact]
        public void ShouldNotHaveValidationError_WhenTaxNumber_IsNull()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.TaxNumber, null as TaxIdentificationNumberDto);
        }

        [Fact]
        public void ShouldHaveChildValidators()
        {
            _validator.ShouldHaveChildValidator(c => c.Passport, typeof(PassportDtoValidator));
            _validator.ShouldHaveChildValidator(c => c.TaxNumber, typeof(TaxIdentificationNumberDtoValidator));
            _validator.ShouldHaveChildValidator(c => c.Preferences, typeof(PersonalPreferencesValidator));
        }

        [Fact]
        public void ShouldHaveValidationError_WhenContact_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Contact, null as ContactDto);
        }
    }
}