﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.Validators
{
    public class TaxIdentificationNumberDtoValidatorTest
    {
        private readonly TaxIdentificationNumberDtoValidator _validator = new TaxIdentificationNumberDtoValidator();

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenNumber_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Number, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenNumber_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Number, "Number");
        }

        [Fact]
        public void ShouldNotHaveError_WhenIssue_IsNull()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Issue, null as IssueDto);
        }

        [Fact]
        public void ShouldHaveChildValidator_IssueDtoValidator_ForIssue()
        {
            _validator.ShouldHaveChildValidator(c => c.Issue, typeof(IssueDtoValidator));
        }
    }
}
