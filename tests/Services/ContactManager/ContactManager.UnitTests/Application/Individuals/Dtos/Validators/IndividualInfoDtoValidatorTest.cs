﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Domain.Individuals;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.Validators
{
    public class IndividualInfoDtoValidatorTest
    {
        private readonly IndividualInfoDtoValidator _validator = new IndividualInfoDtoValidator();

        [Fact]
        public void ShouldHaveChildValidator_PersonNameDtoValidator_ForName()
        {
            _validator.ShouldHaveChildValidator(c => c.Name, typeof(PersonNameDtoValidator));
        }

        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        public void ShouldHaveError_When_Gender_IsInvalid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Gender, (Gender)value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public void ShouldNotHaveError_When_Gender_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Gender, (Gender)value);
        }
    }
}
