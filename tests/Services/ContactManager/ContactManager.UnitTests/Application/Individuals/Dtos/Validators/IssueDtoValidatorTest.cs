﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.Validators
{
    public class IssueDtoValidatorTest
    {
        private readonly IssueDtoValidator _validator = new IssueDtoValidator();

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenIssuer_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Issuer, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenIssuer_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Issuer, "Issuer");
        }
    }
}
