﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using System.Collections.Generic;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.Validators
{
    public class PersonNameDtoValidatorTest
    {
        private readonly PersonNameDtoValidator _validator = new PersonNameDtoValidator();

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void ShouldHaveError_When_IndividualName_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Firstname, value);
            _validator.ShouldHaveValidationErrorFor(m => m.Lastname, value);
            _validator.ShouldHaveValidationErrorFor(m => m.Middlename, value);
        }

        [Theory]
        [MemberData(nameof(Models))]
        public void ShouldNotHaveError_When_IndividualName_IsSet(PersonNameDto model)
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Firstname, model);
            _validator.ShouldNotHaveValidationErrorFor(m => m.Lastname, model);
            _validator.ShouldNotHaveValidationErrorFor(m => m.Middlename, model);
        }

        public static IEnumerable<object[]> Models => new[]
        {
            new[] { new PersonNameDto { Firstname = "Firstname" } },
            new[] { new PersonNameDto { Lastname = "Lastname" } },
            new[] { new PersonNameDto { Middlename = "Middlename" } }
        };

    }
}
