﻿using FluentValidation.TestHelper;
using Moq;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Providers;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.Validators
{
    public class PersonalPreferencesValidatorTest
    {
        private const string ThreeLetterIsoCode = "code";

        private readonly Mock<ILanguageProvider> _languageProvider;
        private readonly PersonalPreferencesValidator _validator;

        public PersonalPreferencesValidatorTest()
        {
            _languageProvider = new Mock<ILanguageProvider>();
            _validator = new PersonalPreferencesValidator(_languageProvider.Object);
        }

        [Fact]
        public void ShouldHaveValidationError()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Language, ThreeLetterIsoCode);
        }

        [Fact]
        public void ShouldNotHaveValidationErrorFor()
        {
            _languageProvider.Setup(p => p.IsValidIso2(ThreeLetterIsoCode))
                .ReturnsAsync(true);

            _validator.ShouldNotHaveValidationErrorFor(c => c.Language, ThreeLetterIsoCode);
        }

        [Fact]
        public void ShouldHaveChildValidator_ForCommunicationPreferences()
        {
            _validator.ShouldHaveChildValidator(c => c.Communications, typeof(CommunicationPreferencesValidator));
        }
    }
}
