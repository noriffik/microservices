﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.Validators
{
    public class CommunicationPreferencesValidatorTest
    {
        private readonly CommunicationPreferencesValidator _validator = new CommunicationPreferencesValidator();

        [Fact]
        public void ShouldHaveValidatorError_WithInvalidPreferred()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.PreferredMedium, "INVALID");
        }

        [Theory]
        [InlineData("INVALID", "TELEGRAM")]
        [InlineData("EMAIL", "INVALID")]
        [InlineData("INVALID", "INVALID")]
        public void ShouldHaveValidatorError_WithInvalidBanned(string first, string second)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.BannedMediums, new [] { first, second});
        }

        [Theory]
        [InlineData("TELEGRAM")]
        [InlineData("EMAIL")]
        [InlineData("VIBER")]
        [InlineData("TELEPHONE")]
        [InlineData(null)]
        public void ShouldNotHaveValidationError_ForPreferred(string preferred)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.PreferredMedium, preferred);
        }

        [Fact]
        public void ShouldNotHaveValidationError_ForBanned()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.BannedMediums, new [] { "TELEGRAM", "EMAIL", "VIBER", "TELEPHONE"});
        }

        [Fact]
        public void ShouldNotHaveValidationError_ForBanned_WhenEmpty()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.BannedMediums, new CommunicationPreferencesDto());
        }
    }
}
