﻿using AutoMapper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.Testing;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.ToDto
{
    public class PersonalPreferencesMapperTest
    {
        private readonly IMapper _mapper;
        private readonly PersonalPreferences _preferences;

        public PersonalPreferencesMapperTest()
        {
            _mapper = AutoMapperFactory.Create();
            _preferences = new PersonalPreferences();
        }

        [Fact]
        public void Map()
        {
            //Act
            var actual = _mapper.Map<PersonalPreferencesDto>(_preferences);

            //Assert
            Assert.Equal(ExpectedDto(_preferences), actual, PropertyComparer<PersonalPreferencesDto>.Instance);
        }

        [Fact]
        public void Map_WithLanguage()
        {
            //Arrange
            _preferences.ChangeLanguage("eng");

            //Act
            var actual = _mapper.Map<PersonalPreferencesDto>(_preferences);

            //Assert
            Assert.Equal(ExpectedDto(_preferences), actual, PropertyComparer<PersonalPreferencesDto>.Instance);
        }

        private PersonalPreferencesDto ExpectedDto(PersonalPreferences preferences)
        {
            return new PersonalPreferencesDto
            {
                Communications = _mapper.Map<CommunicationPreferencesDto>(preferences.Communications),
                Language = preferences.Language
            };
        }
    }
}
