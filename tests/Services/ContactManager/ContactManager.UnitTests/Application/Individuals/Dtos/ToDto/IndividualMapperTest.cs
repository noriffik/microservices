﻿using AutoFixture;
using AutoMapper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Domain;
using NexCore.Legal;
using NexCore.Testing;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.ToDto
{
    public class IndividualMapperTest
    {
        private readonly IMapper _mapper;
        private readonly IFixture _fixture;
        private readonly Individual _individual;

        public IndividualMapperTest()
        {
            _mapper = AutoMapperFactory.Create();
            _fixture = new Fixture()
                .Customize(new IndividualCustomizations());

            _individual = _fixture.Create<Individual>();
        }

        [Fact]
        public void Map()
        {
            //Act
            var actual = _mapper.Map<IndividualDto>(_individual);

            //Assert
            Assert.Equal(ExpectedDto(_individual), actual, PropertyComparer<IndividualDto>.Instance);
        }

        [Fact]
        public void Map_WithPassport()
        {
            //Arrange
            _individual.ChangePassport(_fixture.Create<Passport>());

            //Act
            var actual = _mapper.Map<IndividualDto>(_individual);

            //Assert
            Assert.Equal(ExpectedDto(_individual), actual, PropertyComparer<IndividualDto>.Instance);
        }

        [Fact]
        public void Map_WithTaxNumber()
        {
            //Arrange
            _individual.ChangeTaxNumber(_fixture.Create<TaxIdentificationNumber>());

            //Act
            var actual = _mapper.Map<IndividualDto>(_individual);

            //Assert
            Assert.Equal(ExpectedDto(_individual), actual, PropertyComparer<IndividualDto>.Instance);
        }

        [Fact]
        public void Map_WithFamilyStatus()
        {
            //Arrange
            _individual.UpdateFamilyStatus(_fixture.Create<FamilyStatus>());

            //Act
            var actual = _mapper.Map<IndividualDto>(_individual);

            //Assert
            Assert.Equal(ExpectedDto(_individual), actual, PropertyComparer<IndividualDto>.Instance);
        }

        private IndividualDto ExpectedDto(Individual individual)
        {
            return new IndividualDto
            {
                Id = individual.Id,
                Name = _mapper.Map<PersonName, PersonNameDto>(individual.Name),
                Preferences = _mapper.Map<PersonalPreferences, PersonalPreferencesDto>(individual.Preferences),
                BirthDay = individual.Birthday,
                Gender = individual.Gender,
                FamilyStatus = individual.FamilyStatus?.Id,
                Children = individual.Children,
                Passport = individual.Passport == Passport.Empty
                    ? null
                    : _mapper.Map<Passport, PassportDto>(individual.Passport),
                TaxNumber = individual.TaxNumber == TaxIdentificationNumber.Empty
                    ? null
                    : _mapper.Map<TaxIdentificationNumber, TaxIdentificationNumberDto>(individual.TaxNumber),
                Contact = _mapper.Map<ContactDto>(individual.Contact),
            };
        }
    }
}
