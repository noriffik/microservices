﻿using AutoMapper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.Testing;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.ToDto
{
    public class CommunicationPreferencesMapperTest
    {
        private readonly IMapper _mapper;
        private readonly CommunicationPreferences _preferences;

        public CommunicationPreferencesMapperTest()
        {
            _mapper = AutoMapperFactory.Create();
            _preferences = new CommunicationPreferences();
        }

        [Fact]
        public void Map()
        {
            //Act
            var actual = _mapper.Map<CommunicationPreferencesDto>(_preferences);

            //Assert
            Assert.Equal(ExpectedDto(_preferences), actual, PropertyComparer<CommunicationPreferencesDto>.Instance);
        }

        [Fact]
        public void Map_WithPreferredMedium()
        {
            //Arrange
            _preferences.ChangePreferredMedium(CommunicationMedium.Viber);

            //Act
            var actual = _mapper.Map<CommunicationPreferencesDto>(_preferences);

            //Assert
            Assert.Equal(ExpectedDto(_preferences), actual, PropertyComparer<CommunicationPreferencesDto>.Instance);
        }

        [Fact]
        public void Map_BannedMediums()
        {
            //Arrange
            _preferences.BanMedium(CommunicationMedium.Email);
            _preferences.BanMedium(CommunicationMedium.Telephone);

            //Act
            var actual = _mapper.Map<CommunicationPreferencesDto>(_preferences);

            //Assert
            Assert.Equal(ExpectedDto(_preferences), actual, PropertyComparer<CommunicationPreferencesDto>.Instance);
        }

        private CommunicationPreferencesDto ExpectedDto(CommunicationPreferences preferences)
        {
            return new CommunicationPreferencesDto
            {
                PreferredMedium = preferences.PreferredMedium?.Id,
                BannedMediums = preferences.BannedMediums.Values.Select(m => m.Id)
            };
        }
    }
}