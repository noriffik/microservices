﻿using AutoFixture;
using AutoMapper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.Legal;
using NexCore.Testing;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.FromDto
{
    public class TaxIdentificationNumberDtoMapperTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly IFixture _fixture = new Fixture();

        [Fact]
        public void Map()
        {
            //Arrange
            var expected = _fixture.Create<TaxIdentificationNumber>();

            //Act
            var actual = _mapper.Map<TaxIdentificationNumber>(MapToDto(expected));

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<TaxIdentificationNumber>.Instance);
        }

        private static TaxIdentificationNumberDto MapToDto(TaxIdentificationNumber number)
        {
            return new TaxIdentificationNumberDto
            {
                Number = number.Number,
                RegisteredOn = number.RegisteredOn,
                Issue = new IssueDto
                {
                    Issuer = number.Issue.Issuer,
                    IssuedOn = number.Issue.IssuedOn.GetValueOrDefault()
                }
            };
        }

        [Fact]
        public void Map_GivenDto_IsNull_ReturnsEmpty()
        {
            //Act
            var actual = _mapper.Map<TaxIdentificationNumberDto, TaxIdentificationNumber>(null);

            //Assert
            Assert.Equal(TaxIdentificationNumber.Empty, actual);
        }
    }
}
