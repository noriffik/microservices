﻿using AutoMapper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.Domain;
using NexCore.Testing;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.FromDto
{
    public class CommunicationPreferencesDtoMapperTest
    {
        private readonly IMapper _mapper;
        private readonly PropertyComparer<CommunicationPreferences> _comparer;
        private readonly CommunicationPreferences _preferences;

        public CommunicationPreferencesDtoMapperTest()
        {
            _mapper = AutoMapperFactory.Create();
            _comparer = new PropertyComparer<CommunicationPreferences>(new PropertyComparerOptions
            {
                CollectionComparison = CollectionComparison.Unordered
            });
            _preferences = new CommunicationPreferences();
        }

        [Fact]
        public void Map()
        {
            //Act
            var actual = _mapper.Map<CommunicationPreferences>(MapToDto(_preferences));

            //Assert
            Assert.Equal(_preferences, actual, _comparer);
        }

        [Fact]
        public void Map_WithPreferredMedium()
        {
            //Arrange
            _preferences.ChangePreferredMedium(CommunicationMedium.Telegram);

            //Act
            var actual = _mapper.Map<CommunicationPreferences>(MapToDto(_preferences));

            //Assert
            Assert.Equal(_preferences, actual, _comparer);
        }

        [Fact]
        public void Map_WithBannedMediums()
        {
            //Arrange
            _preferences.BanMedium(CommunicationMedium.Telegram);
            _preferences.BanMedium(CommunicationMedium.Email);

            //Act
            var actual = _mapper.Map<CommunicationPreferences>(MapToDto(_preferences));

            //Assert
            Assert.Equal(_preferences, actual, _comparer);
        }

        private static CommunicationPreferencesDto MapToDto(CommunicationPreferences preferences)
        {
            return new CommunicationPreferencesDto
            {
                PreferredMedium = preferences.PreferredMedium?.Id,
                BannedMediums = preferences.BannedMediums.Values.Select(m => m.Id)
            };
        }
    }
}