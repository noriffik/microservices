﻿using AutoFixture;
using AutoMapper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.Legal;
using NexCore.Testing;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.FromDto
{
    public class PassportDtoMapperTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly IFixture _fixture = new Fixture();

        [Fact]
        public void Map()
        {
            //Arrange
            var expected = _fixture.Create<Passport>();

            //Act
            var actual = _mapper.Map<Passport>(MapToDto(expected));

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<Passport>.Instance);
        }

        private static PassportDto MapToDto(Passport passport)
        {
            return new PassportDto
            {
                Series = passport.Series,
                Number = passport.Number,
                Issuer = passport.Issue.Issuer,
                IssuedOn = passport.Issue.IssuedOn.GetValueOrDefault()
            };
        }

        [Fact]
        public void Map_GivenDto_IsNull_ReturnsEmpty()
        {
            //Act
            var actual = _mapper.Map<PassportDto, Passport>(null);

            //Assert
            Assert.Equal(Passport.Empty, actual);
        }
    }
}