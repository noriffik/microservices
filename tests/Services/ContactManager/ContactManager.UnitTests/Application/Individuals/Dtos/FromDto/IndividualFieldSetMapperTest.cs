﻿using AutoFixture;
using AutoMapper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Domain;
using NexCore.Domain;
using NexCore.Legal;
using NexCore.Testing;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.FromDto
{
    public class IndividualFieldSetMapperTest
    {
        private readonly IMapper _mapper;
        private readonly IFixture _fixture;
        private readonly PropertyComparer<Individual> _comparer;
        private readonly Individual _individual;

        public IndividualFieldSetMapperTest()
        {
            _mapper = AutoMapperFactory.Create();
            _fixture = new Fixture().Customize(new IndividualCustomizations());
            _comparer = new PropertyComparer<Individual>(new PropertyComparerOptions
            {
                CollectionComparison = CollectionComparison.Unordered
            });

            _individual = _fixture.Create<Individual>();
        }

        [Fact]
        public void Map()
        {
            //Act
            var actual = _mapper.Map<Individual>(MapToFieldSet(_individual));

            //Assert
            Assert.Equal(_individual, actual, _comparer);
        }

        [Fact]
        public void Map_WithPassport()
        {
            //Arrange
            _individual.ChangePassport(_fixture.Create<Passport>());

            //Act
            var actual = _mapper.Map<Individual>(MapToFieldSet(_individual));

            //Assert
            Assert.Equal(_individual, actual, _comparer);
        }

        [Fact]
        public void Map_WithTaxNumber()
        {
            //Arrange
            _individual.ChangeTaxNumber(_fixture.Create<TaxIdentificationNumber>());

            //Act
            var actual = _mapper.Map<Individual>(MapToFieldSet(_individual));

            //Assert
            Assert.Equal(_individual, actual, _comparer);
        }


        [Fact]
        public void Map_WithFamilyStatus()
        {
            //Arrange
            _individual.UpdateFamilyStatus(FamilyStatus.Married);

            //Act
            var actual = _mapper.Map<Individual>(MapToFieldSet(_individual));

            //Assert
            Assert.Equal(_individual, actual, _comparer);
        }

        [Fact]
        public void Map_WithPersonalPreferences()
        {
            //Arrange
            _individual.Preferences.ChangeLanguage("eng");

            //Act
            var actual = _mapper.Map<Individual>(MapToFieldSet(_individual));

            //Assert
            Assert.Equal(_individual, actual, _comparer);
        }

        private IndividualFieldSet MapToFieldSet(Individual individual)
        {
            return new IndividualFieldSet
            {
                Name = _mapper.Map<PersonNameDto>(individual.Name),
                Preferences = _mapper.Map<PersonalPreferencesDto>(individual.Preferences),
                BirthDay = individual.Birthday,
                Gender = individual.Gender,
                Children = individual.Children,
                FamilyStatus = individual.FamilyStatus?.Id,
                Passport = individual.Passport == Passport.Empty
                    ? null : _mapper.Map<PassportDto>(individual.Passport),
                TaxNumber = individual.TaxNumber == TaxIdentificationNumber.Empty
                    ? null : _mapper.Map<TaxIdentificationNumberDto>(individual.TaxNumber),
                Contact = _mapper.Map<ContactDto>(individual.Contact)
            };
        }
    }
}
