﻿using AutoMapper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.Domain;
using NexCore.Testing;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.FromDto
{
    public class PersonalPreferencesDtoMapperTest
    {
        private readonly IMapper _mapper;
        private readonly PropertyComparer<PersonalPreferences> _comparer;
        private readonly PersonalPreferences _preferences;

        public PersonalPreferencesDtoMapperTest()
        {
            _mapper = AutoMapperFactory.Create();
            _comparer = new PropertyComparer<PersonalPreferences>(new PropertyComparerOptions
            {
                CollectionComparison = CollectionComparison.Unordered
            });
            _preferences = new PersonalPreferences();
        }

        [Fact]
        public void Map()
        {
            //Act
            var actual = _mapper.Map<PersonalPreferences>(MapToDto(_preferences));

            //Assert
            Assert.Equal(_preferences, actual, _comparer);
        }

        [Fact]
        public void Map_GivenDto_IsEmpty()
        {
            //Act
            var actual = _mapper.Map<PersonalPreferences>(new PersonalPreferencesDto());

            //Assert
            Assert.Equal(_preferences, actual, _comparer);
        }

        [Fact]
        public void Map_WithLanguage()
        {
            //Arrange
            _preferences.ChangeLanguage("eng");

            //Act
            var actual = _mapper.Map<PersonalPreferences>(MapToDto(_preferences));

            //Assert
            Assert.Equal(_preferences, actual, _comparer);
        }

        [Fact]
        public void Map_WithCommunication()
        {
            //Arrange
            _preferences.Communications.ChangePreferredMedium(CommunicationMedium.Email);

            //Act
            var actual = _mapper.Map<PersonalPreferences>(MapToDto(_preferences));

            //Assert
            Assert.Equal(_preferences, actual, _comparer);
        }

        private PersonalPreferencesDto MapToDto(PersonalPreferences preferences)
        {
            return new PersonalPreferencesDto
            {
                Language = preferences.Language,
                Communications = new CommunicationPreferencesDto 
                {
                    PreferredMedium = preferences.Communications.PreferredMedium?.Id
                }
            };
        }
    }
}