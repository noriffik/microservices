﻿using AutoFixture;
using AutoMapper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.Legal;
using NexCore.Testing;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Dtos.FromDto
{
    public class PersonNameDtoMapperTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly IFixture _fixture = new Fixture();

        [Fact]
        public void Map()
        {
            //Arrange
            var expected = _fixture.Create<PersonName>();

            //Act
            var actual = _mapper.Map<PersonName>(MapToDto(expected));

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<PersonName>.Instance);
        }

        private static PersonNameDto MapToDto(PersonName name)
        {
            return new PersonNameDto
            {
                Firstname = name.Firstname,
                Lastname = name.Lastname,
                Middlename = name.Middlename
            };
        }
    }
}