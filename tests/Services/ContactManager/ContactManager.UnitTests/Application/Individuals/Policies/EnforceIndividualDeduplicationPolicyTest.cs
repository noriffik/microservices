﻿using AutoFixture;
using Moq;
using NexCore.ContactManager.Application.Individuals.Policies;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Contacts.Specifications;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Policies
{
    public class EnforceIndividualDeduplicationPolicyTest
    {
        //Dependency
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly Individual _individual;
        private readonly IReadOnlyCollection<Telephone> _telephones;

        //Policy
        private readonly EnforceIndividualDeduplicationPolicy _policy;

        public EnforceIndividualDeduplicationPolicyTest()
        {
            //Dependency
            _repository = new Mock<IEntityRepository>();

            //Entity
            var fixture = new Fixture();
            _individual = fixture.Create<Individual>();
            _telephones = fixture.CreateMany<Telephone>().ToList();
            foreach (var telephone in _telephones)
                _individual.Contact.Add(telephone, fixture.Create<string>());

            //Policy
            _policy = new EnforceIndividualDeduplicationPolicy(_repository.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(EnforceIndividualDeduplicationPolicy)).HasNullGuard();
        }

        [Fact]
        public Task Validate_GivenIndividual_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("individual",
                () => _policy.Apply(null, CancellationToken.None));
        }

        [Fact]
        public async Task Apply()
        {
            //Arrange
            _repository.Setup(r =>
                    r.Has(It.Is<TelephoneInUseByOtherContactableSpecification<Individual>>(s =>
                        s.ContactableId == _individual.Id &&
                        s.Telephones.SequenceEqual(_telephones)), It.IsAny<CancellationToken>()))
                .ReturnsAsync(false)
                .Verifiable();

            //Act
            await _policy.Apply(_individual, CancellationToken.None);

            //Assert
            _repository.Verify();
        }

        [Fact]
        public async Task Apply_WhenTelephone_IsInUse_Throws()
        {
            //Arrange
            _repository.Setup(r =>
                    r.Has(It.Is<TelephoneInUseByOtherContactableSpecification<Individual>>(s =>
                        s.ContactableId == _individual.Id &&
                        s.Telephones.SequenceEqual(_telephones)), It.IsAny<CancellationToken>()))
                .ReturnsAsync(true)
                .Verifiable();

            //Act
            var e = await Assert.ThrowsAsync<IndividualDeduplicationRuleViolationException>(() =>
                _policy.Apply(_individual, CancellationToken.None));

            //Assert
            Assert.Contains("is in use", e.Message);
            _repository.Verify();
        }
    }
}
