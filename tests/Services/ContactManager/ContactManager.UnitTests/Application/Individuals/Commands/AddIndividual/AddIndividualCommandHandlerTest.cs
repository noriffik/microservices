﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Individuals.Commands.AddIndividual;
using NexCore.ContactManager.Application.Individuals.Policies;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.Domain;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Commands.AddIndividual
{
    public class AddIndividualCommandHandlerTest
    {
        //Command and entity
        private readonly AddIndividualCommand _command;
        private readonly Individual _individual;

        //Dependencies
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IIndividualDeduplicationRulePolicy> _deduplicationPolicy;
        private readonly AddIndividualCommandHandler _handler;

        public AddIndividualCommandHandlerTest()
        {
            //Create command and entity
            _command = new AddIndividualCommand();
            _individual = CreateIndividual();

            //Create dependencies
            _repository = new Mock<IEntityRepository>();
            _work = new Mock<IUnitOfWork>();
            _deduplicationPolicy = new Mock<IIndividualDeduplicationRulePolicy>();
            var mapper = new Mock<IMapper>();

            //Setup dependencies
            mapper.Setup(m => m.Map<Individual>(_command))
                .Returns(_individual);
            _work.Setup(w => w.EntityRepository)
                .Returns(_repository.Object);

            //Create command handler
            _handler = new AddIndividualCommandHandler(_work.Object, mapper.Object, _deduplicationPolicy.Object);
        }

        private static Individual CreateIndividual()
        {
            var fixture = new Fixture();
            var individual = fixture.Create<Individual>();

            fixture.CreateMany<Telephone>()
                .ToList()
                .ForEach(t => individual.Contact.Add(t));

            return individual;
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddIndividualCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request",
                () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            var telephones = _individual.Contact
                .Telephones
                .Select(t => t.Value);

            using (Sequence.Create())
            {
                _deduplicationPolicy.Setup(p => p.Apply(_individual, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _repository.Setup(r => r.Add(_individual))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }
    }
}
