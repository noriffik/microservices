﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual;
using NexCore.ContactManager.Application.Individuals.Policies;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures.Domain;
using NexCore.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Commands.UpdateIndividual
{
    public class UpdateIndividualCommandHandlerTest
    {
        //Entities
        private readonly Individual _individual;

        //Dependencies
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IMapper> _mapper;
        private readonly Mock<IIndividualInfoUpdatedEventPublisher> _eventPublisher;
        private readonly Mock<IIndividualDeduplicationRulePolicy> _deduplicationPolicy;

        //Handler and command
        private readonly UpdateIndividualCommandHandler _handler;
        private readonly UpdateIndividualCommand _command;
        private readonly Mock<IUnitOfWork> _work;

        public UpdateIndividualCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customize(new IndividualCustomizations());

            //Entities
            _individual = fixture.Create<Individual>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            _mapper = new Mock<IMapper>();
            _eventPublisher = new Mock<IIndividualInfoUpdatedEventPublisher>();
            _deduplicationPolicy = new Mock<IIndividualDeduplicationRulePolicy>();

            //Handler and command
            _command = fixture.Create<UpdateIndividualCommand>();
            _handler = new UpdateIndividualCommandHandler(
                _work.Object, _mapper.Object, _eventPublisher.Object, _deduplicationPolicy.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(UpdateIndividualCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Require<Individual>(_command.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_individual);
                _mapper.Setup(m => m.Map(_command, _individual))
                    .InSequence();
                _deduplicationPolicy.Setup(p => p.Apply(_individual, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _eventPublisher.Setup(w => w.Publish(_individual, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, It.IsAny<CancellationToken>()));
        }
    }
}
