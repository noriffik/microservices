﻿using AutoFixture;
using AutoMapper;
using Moq;
using NexCore.Common;
using NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.Legal;
using NexCore.Testing;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Commands.UpdateIndividual
{
    public class DealerPrivateCustomerUpdatedEventFactoryTest
    {
        //Entities
        private readonly Individual _individual;
        private readonly int _customerId;
        private readonly Passport _passport;
        private readonly TaxIdentificationNumber _taxNumber;

        private readonly PersonNameDto _personNameDto;
        private readonly PassportDto _passportDto;
        private readonly TaxIdentificationNumberDto _taxNumberDto;

        private readonly Guid _guid;
        private readonly DateTime _dateTime;

        //Factory
        private readonly DealerPrivateCustomerInfoUpdatedEventFactory _factory;

        public DealerPrivateCustomerUpdatedEventFactoryTest()
        {
            var fixture = new Fixture();

            //Entities
            _individual = fixture.Create<Individual>();
            _passport = fixture.Create<Passport>();
            _taxNumber = fixture.Create<TaxIdentificationNumber>();

            _customerId = fixture.Create<int>();
            _personNameDto = fixture.Create<PersonNameDto>();
            _passportDto = fixture.Create<PassportDto>();
            _taxNumberDto = fixture.Create<TaxIdentificationNumberDto>();

            _guid = Guid.NewGuid();
            _dateTime = fixture.Create<DateTime>();

            //Mapper
            var mapper = new Mock<IMapper>();
            mapper.Setup(m => m.Map<PersonNameDto>(_individual.Name)).Returns(_personNameDto);
            mapper.Setup(m => m.Map<PassportDto>(_individual.Passport)).Returns(_passportDto);
            mapper.Setup(m => m.Map<TaxIdentificationNumberDto>(_individual.TaxNumber)).Returns(_taxNumberDto);

            //Factory
            _factory = new DealerPrivateCustomerInfoUpdatedEventFactory(mapper.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DealerPrivateCustomerInfoUpdatedEventFactory)).HasNullGuard();
        }

        [Fact]
        public void Create_WhenPassportAndTaxNumber_IsNotEmpty()
        {
            //Arrange
            _individual.ChangePassport(_passport);
            _individual.ChangeTaxNumber(_taxNumber);
            var expected = new DealerPrivateCustomerInfoUpdatedEvent(_guid, _dateTime)
            {
                DealerPrivateCustomerId = _customerId,
                PersonName = _personNameDto,
                Passport = _passportDto,
                TaxNumber = _taxNumberDto
            };

            using (new GuidFactoryContext(_guid))
            using (new SystemTimeContext(_dateTime))
            {
                //Act
                var actual = _factory.Create(_customerId, _individual);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<DealerPrivateCustomerInfoUpdatedEvent>.Instance);
            }
        }

        [Fact]
        public void Create_WhenPassportAndTaxNumber_IsEmpty()
        {
            //Arrange
            var expected = new DealerPrivateCustomerInfoUpdatedEvent(_guid, _dateTime)
            {
                DealerPrivateCustomerId = _customerId,
                PersonName = _personNameDto,
            };

            using (new GuidFactoryContext(_guid))
            using (new SystemTimeContext(_dateTime))
            {
                //Act
                var actual = _factory.Create(_customerId, _individual);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<DealerPrivateCustomerInfoUpdatedEvent>.Instance);
            }
        }

        [Fact]
        public void Create_GivenIndividual_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("individual", () => _factory.Create(_customerId, null));
        }
    }
}
