﻿using AutoFixture;
using Moq;
using NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual;
using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.Customers.Specifications;
using NexCore.ContactManager.Domain.DealerEmployees;
using NexCore.ContactManager.Domain.DealerPrivateCustomers;
using NexCore.ContactManager.Domain.Employees.Specifications;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.Domain;
using NexCore.EventBus.Abstract;
using NexCore.Testing.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Commands.UpdateIndividual
{
    public class IndividualInfoUpdatedEventPublisherTest
    {
        private readonly IFixture _fixture;

        //Dependencies
        private readonly Mock<IIntegrationEventStorage> _eventStorage;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IDealerPrivateCustomerInfoUpdatedEventFactory> _customerEventFactory;
        private readonly Mock<IDealerEmployeeNameUpdatedEventFactory> _employeeEventFactory;

        //Entities
        private readonly Individual _individual;
        private readonly IReadOnlyCollection<IntegrationEvent> _expectedEvents;

        //Publisher
        private readonly IndividualInfoUpdatedEventPublisher _publisher;

        public IndividualInfoUpdatedEventPublisherTest()
        {
            _fixture = new Fixture()
                .Customize(new DealerIdCustomization());

            //Dependencies
            _customerEventFactory = new Mock<IDealerPrivateCustomerInfoUpdatedEventFactory>();
            _employeeEventFactory = new Mock<IDealerEmployeeNameUpdatedEventFactory>();
            _eventStorage = new Mock<IIntegrationEventStorage>();
            _repository = new Mock<IEntityRepository>();

            //Entities
            _individual = _fixture.Construct<Individual>(_fixture.Create<int>());

            //Publisher
            _publisher = new IndividualInfoUpdatedEventPublisher(_repository.Object,
                _customerEventFactory.Object, _employeeEventFactory.Object, _eventStorage.Object);

            _expectedEvents = SetupCustomerEvents()
                .Concat(SetupEmployeeEvents())
                .ToList();
        }

        private IEnumerable<IntegrationEvent> SetupEmployeeEvents()
        {
            var employees = Enumerable.Range(10, 3)
                .Select(id => _fixture.Construct<DealerEmployee>(id))
                .ToList();

            var events = employees.ToDictionary(c => c.Id, c => _fixture.Create<DealerEmployeeNameUpdatedEvent>());

            _repository.Setup(r => r.Find(
                    It.Is<EmployeeByIndividualIdSpecification<DealerEmployee>>(s => s.IndividualId == _individual.Id),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(employees);

            foreach (var employee in employees)
                _employeeEventFactory.Setup(f => f.Create(employee.Id, _individual))
                    .Returns(events[employee.Id]);

            return events.Values;
        }

        private IEnumerable<IntegrationEvent> SetupCustomerEvents()
        {
            var customers = Enumerable.Range(1, 3)
                .Select(id => _fixture.Construct<DealerPrivateCustomer>(id))
                .ToList();

            var events = customers.ToDictionary(c => c.Id, c => _fixture.Create<DealerPrivateCustomerInfoUpdatedEvent>());

            _repository.Setup(r => r.Find(
                    It.Is<ByContactableIdSpecification<DealerPrivateCustomer>>(s => s.ContactableId == _individual.Id),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(customers);

            foreach (var customer in customers)
                _customerEventFactory.Setup(f => f.Create(customer.Id, _individual))
                    .Returns(events[customer.Id]);

            return events.Values;
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(IndividualInfoUpdatedEventPublisher)).HasNullGuard();
        }

        [Fact]
        public Task Publish_GivenIndividual_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("individual", () =>
                _publisher.Publish(null, CancellationToken.None));
        }

        [Fact]
        public async Task Publish()
        {
            //Arrange
            _eventStorage.Setup(s => s.AddRange(_expectedEvents))
                .Returns(Task.CompletedTask)
                .Verifiable();

            //Act
            await _publisher.Publish(_individual, CancellationToken.None);

            //Assert
            _eventStorage.Verify();
        }
    }
}
