﻿using FluentValidation.TestHelper;
using Moq;
using NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual;
using NexCore.ContactManager.Application.Providers;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Commands.UpdateIndividual
{
    public class UpdateIndividualCommandValidatorTest
    {
        private readonly UpdateIndividualCommandValidator _validator;

        public UpdateIndividualCommandValidatorTest()
        {
            _validator = new UpdateIndividualCommandValidator(new Mock<ILanguageProvider>().Object);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldHaveError_WhenId_IsLessThan_One(int individualId)
        {
            _validator.ShouldHaveValidationErrorFor(cp => cp.Id, individualId);
        }

        [Fact]
        public void ShouldNotHaveError_WhenId_IsGreaterThan_One()
        {
            _validator.ShouldNotHaveValidationErrorFor(cp => cp.Id, 1);
        }
    }
}
