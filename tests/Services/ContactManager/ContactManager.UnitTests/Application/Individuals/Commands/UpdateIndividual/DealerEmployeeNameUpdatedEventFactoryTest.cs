﻿using AutoFixture;
using AutoMapper;
using Moq;
using NexCore.Common;
using NexCore.ContactManager.Application.Individuals.Commands.UpdateIndividual;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Messages;
using NexCore.ContactManager.Domain.Individuals;
using NexCore.Testing;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Individuals.Commands.UpdateIndividual
{
    public class DealerEmployeeNameUpdatedEventFactoryTest
    {
        //Entities
        private readonly Individual _individual;
        private readonly int _employeeId;

        private readonly PersonNameDto _personNameDto;

        private readonly Guid _guid;
        private readonly DateTime _dateTime;

        //Factory
        private readonly DealerEmployeeNameUpdatedEventFactory _factory;

        public DealerEmployeeNameUpdatedEventFactoryTest()
        {
            var fixture = new Fixture();

            //Entities
            _individual = fixture.Create<Individual>();

            _employeeId = fixture.Create<int>();
            _personNameDto = fixture.Create<PersonNameDto>();

            _guid = Guid.NewGuid();
            _dateTime = fixture.Create<DateTime>();

            //Mapper
            var mapper = new Mock<IMapper>();
            mapper.Setup(m => m.Map<PersonNameDto>(_individual.Name)).Returns(_personNameDto);

            //Factory
            _factory = new DealerEmployeeNameUpdatedEventFactory(mapper.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DealerEmployeeNameUpdatedEventFactory)).HasNullGuard();
        }

        [Fact]
        public void Create()
        {
            //Arrange
            var expected = new DealerEmployeeNameUpdatedEvent(_guid, _dateTime)
            {
                DealerEmployeeId = _employeeId,
                PersonName = _personNameDto,
            };

            using (new GuidFactoryContext(_guid))
            using (new SystemTimeContext(_dateTime))
            {
                //Act
                var actual = _factory.Create(_employeeId, _individual);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<DealerEmployeeNameUpdatedEvent>.Instance);
            }
        }

        [Fact]
        public void Create_GivenIndividual_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("individual", () => _factory.Create(_employeeId, null));
        }
    }
}
