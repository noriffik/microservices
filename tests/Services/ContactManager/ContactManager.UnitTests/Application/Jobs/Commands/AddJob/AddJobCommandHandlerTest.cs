﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Jobs.Commands.AddJob;
using NexCore.ContactManager.Domain.Dictionaries;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Jobs.Commands.AddJob
{
    public class AddJobCommandHandlerTest
    {
        //Entity
        private readonly Job _job;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IMapper> _mapper;

        //Handler and command
        private readonly AddJobCommand _command;
        private readonly AddJobCommandHandler _handler;

        public AddJobCommandHandlerTest()
        {
            var fixture = new Fixture();

            //Entity
            _job = fixture.CustomizeConstructor<Job>(5)
                .Create<Job>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            _mapper = new Mock<IMapper>();

            //Handler and command
            _command = fixture.Create<AddJobCommand>();
            _handler = new AddJobCommandHandler(_work.Object, _mapper.Object);

            //Setup mapper
            _mapper.Setup(m => m.Map<Job>(_command)).Returns(_job);
        }

        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work", () => new AddJobCommandHandler(null, _mapper.Object));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () => new AddJobCommandHandler(_work.Object, null));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Add(_job))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_job.Id, result);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }
    }
}


