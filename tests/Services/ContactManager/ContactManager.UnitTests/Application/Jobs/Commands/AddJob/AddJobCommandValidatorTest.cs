﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Jobs.Commands.AddJob;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Jobs.Commands.AddJob
{
    public class AddJobCommandValidatorTest
    {
        private readonly AddJobCommandValidator _validator = new AddJobCommandValidator();
        
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void ShouldHaveError_WhenName_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Title, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenName_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Title, "Title");
        }
    }
}
