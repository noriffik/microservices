﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Organizations.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Organizations.Dtos
{
    public class OrganizationInfoDtoValidatorTest
    {
        private readonly OrganizationInfoDtoValidator _validator = new OrganizationInfoDtoValidator();

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void ShouldHaveError_WhenName_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Name, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenName_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Name, "name");
        }

        [Fact]
        public void ShouldHaveChildValidator_IdentityModelValidator_ForIdentity()
        {
            _validator.ShouldHaveChildValidator(c => c.Identity, typeof(IdentityDtoValidator));
        }
        
        [Fact]
        public void ShouldHaveError_WhenIdentity_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Identity, null as IdentityDto);
        }
    }
}
