﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Organizations.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Organizations.Dtos
{
    public class IdentityDtoValidatorTest
    {
        private readonly IdentityDtoValidator _validator = new IdentityDtoValidator();

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void ShouldHaveError_WhenEdrpou_IsEmpty_AndIpn_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Edrpou, new IdentityDto
            {
                Edrpou = value,
                Ipn = value
            });
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void ShouldHaveError_WhenIpn_IsEmpty_AndEdrpou_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Ipn, new IdentityDto
            {
                Edrpou = value,
                Ipn = value
            });
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void ShouldNotHaveError_WhenEdrpou_IsNotEmpty_AndIpn_IsEmpty(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Ipn, new IdentityDto
            {
                Edrpou = "code",
                Ipn = value
            });
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void ShouldNotHaveError_WhenIpn_IsNotEmpty_AndEdrpou_IsEmpty(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Edrpou, new IdentityDto
            {
                Edrpou = value,
                Ipn = "code"
            });
        }

        [Fact]
        public void ShouldHaveError_ForEdrpou_WhenEdrpou_IsNotEmpty_AndIpn_IsNotEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Edrpou, new IdentityDto
            {
                Edrpou = "code1",
                Ipn = "code2"
            });
        }

        [Fact]
        public void ShouldHaveError_ForIpn_WhenEdrpou_IsNotEmpty_AndIpn_IsNotEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Ipn, new IdentityDto
            {
                Edrpou = "code1",
                Ipn = "code2"
            });
        }
    }
}
