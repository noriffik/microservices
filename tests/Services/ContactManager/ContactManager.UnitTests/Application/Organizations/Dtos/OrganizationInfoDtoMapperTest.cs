﻿using AutoFixture;
using Moq;
using NexCore.ContactManager.Application.Organizations.Dtos;
using NexCore.ContactManager.Domain.Organizations;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Organizations.Dtos
{
    public class OrganizationInfoDtoMapperTest
    {
        private readonly OrganizationInfoDtoMapper _mapper;
        private readonly Organization _organization;
        private readonly OrganizationInfoDto _model;

        public OrganizationInfoDtoMapperTest()
        {
            var fixture = new Fixture();

            _organization = fixture.Create<Organization>();

            _model = fixture.Build<OrganizationInfoDto>()
                .With(m => m.Name, _organization.Name)
                .Create();

            _mapper = new OrganizationInfoDtoMapper
            {
                IdentityMapper = SetupIdentityMapper(_model.Identity, _organization.Identity)
            };
        }

        private IIdentityDtoMapper SetupIdentityMapper(IdentityDto model, Identity identity)
        {
            var mapper = new Mock<IIdentityDtoMapper>();
            mapper.Setup(m => m.Map(model)).Returns(identity);

            return mapper.Object;
        }

        [Fact]
        public void Map()
        {
            //Act
            var result = _mapper.Map(_model);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(_organization.Name, result.Name);
            Assert.Equal(_organization.Identity, result.Identity);
        }

        [Fact]
        public void Map_WithDestination()
        {
            //Arrange
            var fixture = new Fixture();
            var identity = fixture.Create<Identity>();
            _model.Name = "changed";
            _mapper.IdentityMapper = SetupIdentityMapper(_model.Identity, identity);

            //Act
            _mapper.Map(_model, _organization);

            //Assert
            Assert.Equal(_model.Name, _organization.Name);
            Assert.Equal(identity, _organization.Identity);
        }

        [Fact]
        public void Map_GivenDestination_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("destination", () => _mapper.Map(null as OrganizationInfoDto));
        }

        [Fact]
        public void Map_WithDestination_GivenDestination_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("destination", () => _mapper.Map(null, _organization));
        }

        [Fact]
        public void Map_WithDestination_GivenSource_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("source", () => _mapper.Map(_model, null));
        }
    }
}
