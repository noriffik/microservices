﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Organizations.Dtos;
using System.Collections.Generic;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Organizations.Dtos
{
    public class OrganizationRequisitesDtoValidatorTest
    {
        private readonly OrganizationRequisitesDtoValidator _validator = new OrganizationRequisitesDtoValidator();

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenShortName_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ShortName, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenShortName_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.ShortName, "ShortName");
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenFullName_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.FullName, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenFullName_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.FullName, "FullName");
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenAddress_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Address, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenAddress_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Address, "Address");
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenBankRequisites_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.BankRequisites, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenBankRequisites_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.BankRequisites, "BankRequisites");
        }

        [Fact]
        public void ShouldHaveError_WhenTelephones_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Telephones, null as IEnumerable<string>);
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenOneOfTelephones_IsEmpty(string telephone)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Telephones, new[] { "telephone1", telephone, "telephone2" });
        }

        [Fact]
        public void ShouldNotHaveError_WhenTelephones_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Telephones, new[] { "telephone1", "telephone2" });
        }

        [Fact]
        public void ShouldHaveError_WhenDirector_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Director, null as PersonNameDto);
        }

        [Fact]
        public void ShouldHaveChildValidator_LegalPersonNameDtoValidator_ForDirector()
        {
            _validator.ShouldHaveChildValidator(c => c.Director, typeof(LegalIndividualNameDtoValidator));
        }
    }
}
