﻿using NexCore.ContactManager.Application.Organizations.Dtos;
using System;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Organizations.Dtos
{
    public class IdentityDtoMapperTest
    {
        private readonly IdentityDtoMapper _mapper = new IdentityDtoMapper();
        
        [Fact]
        public void Map_WithEdrpou()
        {
            //Arrange
            var model = new IdentityDto {Edrpou = "Edrpou"};

            //Act
            var result = _mapper.Map(model);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(model.Edrpou, result.Edrpou);
            Assert.Null(result.Ipn);
        }

        [Fact]
        public void Map_WithIpn()
        {
            //Arrange
            var model = new IdentityDto {Ipn = "Ipn"};

            //Act
            var result = _mapper.Map(model);

            //Assert
            Assert.NotNull(result);
            Assert.Null(result.Edrpou);
            Assert.Equal(model.Ipn, result.Ipn);
        }

        [Fact]
        public void Map_GivenSource_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("source", () =>
            {
                var unused = _mapper.Map(null);
            });
        }

        [Fact]
        public void Map_GivenDestinationProperties_AreNull_Throws()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                var unused = _mapper.Map(new IdentityDto());
            });
        }

        [Fact]
        public void Map_GivenDestinationProperties_AreaBothIsSet_Throws()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                var unused = _mapper.Map(new IdentityDto {Edrpou = "Edrpou", Ipn = "Ipn"});
            });
        }
    }
}
