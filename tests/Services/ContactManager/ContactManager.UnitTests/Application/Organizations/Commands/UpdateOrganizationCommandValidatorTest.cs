﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Organizations.Commands;
using NexCore.ContactManager.Application.Organizations.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Organizations.Commands
{
    public class UpdateOrganizationCommandValidatorTest
    {
        private readonly UpdateOrganizationCommandValidator _validator = new UpdateOrganizationCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldHaveError_WhenId_IsEmpty_Or_LessThan_1(int organizationId)
        {
            _validator.ShouldHaveValidationErrorFor(cp => cp.Id, organizationId);
        }

        [Fact]
        public void ShouldNotHaveError_WhenId_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(cp => cp.Id, 1);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void ShouldHaveError_WhenName_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Name, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenName_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Name, "name");
        }

        [Fact]
        public void ShouldHaveChildValidator_IdentityDtoValidator_ForIdentity()
        {
            _validator.ShouldHaveChildValidator(c => c.Identity, typeof(IdentityDtoValidator));
        }

        [Fact]
        public void ShouldHaveError_WhenIdentity_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Identity, null as IdentityDto);
        }

        [Fact]
        public void ShouldHaveChildValidator_ContactModelValidator_ForContact()
        {
            _validator.ShouldHaveChildValidator(c => c.Contact, typeof(ContactDtoValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_OrganizationRequisitesDtoValidator_ForRequisites()
        {
            _validator.ShouldHaveChildValidator(c => c.Requisites, typeof(OrganizationRequisitesDtoValidator));
        }

        [Fact]
        public void ShouldNotHaveError_WhenRequisites_IsNull()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Requisites, null as OrganizationRequisitesDto);
        }
    }
}
