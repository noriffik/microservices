﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Organizations.Commands;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Organizations.Commands
{
    public class UpdateOrganizationCommandHandlerTest
    {
        //Entity
        private readonly Organization _organization;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IMapper> _mapper;

        //Handler and command
        private readonly UpdateOrganizationCommandHandler _handler;
        private readonly UpdateOrganizationCommand _command;

        public UpdateOrganizationCommandHandlerTest()
        {
            var fixture = new Fixture()
                .CustomizeConstructor<Organization>(175);

            //Entity
            _organization = fixture.Create<Organization>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _mapper = new Mock<IMapper>();

            //Handler and command
            _handler = new UpdateOrganizationCommandHandler(_work.Object, _mapper.Object);
            _command = fixture.Build<UpdateOrganizationCommand>()
                .With(c => c.Id, _organization.Id)
                .Create();
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection
                .OfConstructor(typeof(UpdateOrganizationCommandHandler))
                .HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange

                _work.Setup(w => w.EntityRepository.Require<Organization>(_command.Id, CancellationToken.None))
                    .InSequence()
                    .ReturnsAsync(_organization);

                _mapper.Setup(m => m.Map(_command, _organization))
                    .InSequence();
                
                _work.Setup(u => u.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, It.IsAny<CancellationToken>());
            }
        }
        
        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>(
                "request", () => _handler.Handle(null, It.IsAny<CancellationToken>()));
        }
    }
}
