﻿using AutoFixture;
using AutoMapper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Individuals.Dtos;
using NexCore.ContactManager.Application.Organizations.Commands.AddOrganization;
using NexCore.ContactManager.Application.Organizations.Dtos;
using NexCore.ContactManager.Domain.Contacts;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.ContactManager.UnitTests.Fixtures;
using NexCore.ContactManager.UnitTests.Fixtures.Api.Models;
using NexCore.Legal;
using NexCore.Testing;
using System.Linq;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Organizations.Commands.AddOrganization
{
    public class AutoMapperTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        private readonly IFixture _fixture = new Fixture()
            .Customize(new ContactModelCustomization());

        [Fact]
        public void Map_FromRegisterCommand_To_Organization()
        {
            //Arrange
            var organization = _fixture.Create<Organization>();
            organization.ChangeRequisites(_fixture.Create<OrganizationRequisites>());
            _fixture.CreateMany<Telephone>(3).ToList()
                .ForEach(t => organization.Contact.Add(t, _fixture.Create<string>()));
            _fixture.CreateMany<EmailAddress>(3).ToList()
                .ForEach(e => organization.Contact.Add(e, _fixture.Create<string>()));
            _fixture.CreateMany<PhysicalAddress>(3).ToList()
                .ForEach(a => organization.Contact.Add(a, _fixture.Create<string>()));

            var command = MapToCommand(organization);

            //Act
            var result = _mapper.Map<Organization>(command);

            //Assert
            Assert.Equal(organization, result, PropertyComparer<Organization>.Instance);
        }

        private static AddOrganizationCommand MapToCommand(Organization organization)
        {

            var requisites = organization.Requisites == OrganizationRequisites.Empty
                ? null
                : new OrganizationRequisitesDto
                {
                    Telephones = organization.Requisites.Telephones,
                    FullName = organization.Requisites.Name.FullName,
                    ShortName = organization.Requisites.Name.ShortName,
                    Address = organization.Requisites.Address,
                    BankRequisites = organization.Requisites.BankRequisites,
                    Director = new PersonNameDto
                    {
                        Firstname = organization.Requisites.Director.Firstname,
                        Lastname = organization.Requisites.Director.Lastname,
                        Middlename = organization.Requisites.Director.Middlename
                    }
                };

            var emails = organization.Contact.EmailAddresses.Select(e => new EmailAddressDto
            {
                IsPrimary = e.IsPrimary,
                Description = e.Description,
                Address = e.Value.Address
            });

            var telephones = organization.Contact.Telephones.Select(t => new TelephoneDto
            {
                Number = new TelephoneNumberDto
                {
                    AreaCode = t.Value.AreaCode,
                    CountryCode = t.Value.CountryCode,
                    LocalNumber = t.Value.LocalNumber
                },
                Description = t.Description,
                IsPrimary = t.IsPrimary,
                Type = t.Value.Type
            });

            var address = organization.Contact.PhysicalAddresses.Select(a => new PhysicalAddressDto
            {
                City = new PhysicalAddressElementDto
                {
                    Code = a.Value.City.Code,
                    Name = a.Value.City.Value
                },
                Country = new PhysicalAddressElementDto
                {
                    Code = a.Value.Country.Code,
                    Name = a.Value.Country.Value
                },
                Description = a.Description,
                Geolocation = new GeolocationDto
                {
                    Latitude = a.Value.Geolocation.Latitude,
                    Longitude = a.Value.Geolocation.Longitude
                },
                IsPrimary = a.IsPrimary,
                PostCode = a.Value.PostCode.Value,
                Region = new PhysicalAddressElementDto
                {
                    Code = a.Value.Region.Code,
                    Name = a.Value.Region.Value
                },
                Street = a.Value.Street.Value
            });

            return new AddOrganizationCommand
            {
                Identity = new IdentityDto
                {
                    Edrpou = organization.Identity.Edrpou,
                    Ipn = organization.Identity.Ipn
                },
                Name = organization.Name,
                Requisites = requisites,
                Contact = new ContactDto
                {
                    Addresses = address,
                    Emails = emails,
                    Telephones = telephones
                }
            };
        }
    }
}
