﻿using FluentValidation.TestHelper;
using NexCore.ContactManager.Application.Contacts.Dtos;
using NexCore.ContactManager.Application.Organizations.Commands.AddOrganization;
using NexCore.ContactManager.Application.Organizations.Dtos;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Organizations.Commands.AddOrganization
{
    public class AddOrganizationCommandValidatorTest
    {
        private readonly AddOrganizationCommandValidator _validator = new AddOrganizationCommandValidator();

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void ShouldHaveError_WhenName_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Name, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenName_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Name, "name");
        }

        [Fact]
        public void ShouldHaveChildValidator_IdentityDtoValidator_ForIdentity()
        {
            _validator.ShouldHaveChildValidator(c => c.Identity, typeof(IdentityDtoValidator));
        }

        [Fact]
        public void ShouldHaveError_WhenIdentity_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Identity, null as IdentityDto);
        }

        [Fact]
        public void ShouldHaveChildValidator_ContactModelValidator_ForContact()
        {
            _validator.ShouldHaveChildValidator(c => c.Contact, typeof(ContactDtoValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_OrganizationRequisitesDtoValidator_ForRequisites()
        {
            _validator.ShouldHaveChildValidator(c => c.Requisites, typeof(OrganizationRequisitesDtoValidator));
        }

        [Fact]
        public void ShouldNotHaveError_WhenRequisites_IsNull()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Requisites, null as OrganizationRequisitesDto);
        }
    }
}
