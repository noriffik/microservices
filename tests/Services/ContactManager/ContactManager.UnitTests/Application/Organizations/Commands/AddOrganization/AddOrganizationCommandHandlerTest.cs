﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.ContactManager.Application.Organizations.Commands.AddOrganization;
using NexCore.ContactManager.Domain.Organizations;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.ContactManager.UnitTests.Application.Organizations.Commands.AddOrganization
{
    public class AddOrganizationCommandHandlerTest
    {
        //Entity
        private readonly Organization _organization;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IOrganizationRegistrationService> _service;

        //Handler and command
        private readonly AddOrganizationCommandHandler _handler;
        private readonly AddOrganizationCommand _command;

        public AddOrganizationCommandHandlerTest()
        {
            var fixture = new Fixture()
                .CustomizeConstructor<Organization>(16);

            //Entity
            _organization = fixture.Create<Organization>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IOrganizationRegistrationService>();
            var mapper = new Mock<IMapper>();

            //Handler and command
            _handler = new AddOrganizationCommandHandler(_work.Object, _service.Object, mapper.Object);
            _command = fixture.Create<AddOrganizationCommand>();

            //Setup mapper
            mapper.Setup(r => r.Map<Organization>(_command)).Returns(_organization);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddOrganizationCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.Register(_organization))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_organization.Id, result);
            }
        }

        [Fact]
        public async Task Handle_WhenOrganization_WithGivenIdentity_AlreadyExists_Throws()
        {
            //Arrange
            _service
                .Setup(r => r.Register(It.IsAny<Organization>()))
                .Throws<DuplicateIdentityException>();

            //Assert
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_organization.Identity), () => _handler.Handle(_command, It.IsAny<CancellationToken>()));

            e.WithMessage("already registered");
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>(
                "request", () => _handler.Handle(null, It.IsAny<CancellationToken>()));
        }
    }
}
