﻿using AutoFixture;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Moq;
using NexCore.Identity.Api.UnitTests.Fixtures;
using NexCore.Identity.Application.Exceptions;
using NexCore.Identity.Application.UserIdentities.Commands.AddUserIdentity;
using NexCore.Identity.Common.Entities;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Application.Commands.AddUserIdentity
{
    public class AddUserIdentityCommandHandlerTest
	{
		private readonly Fixture _fixture = new Fixture();

        private readonly AddUserIdentityCommand _command;
		private readonly UserIdentity _identity;
		private readonly IdentityError _error;

		//Dependencies
		private readonly Mock<UserManager<UserIdentity>> _manager;
        private readonly Mock<IMapper> _mapper;

        //Handler
		private readonly AddUserIdentityCommandHandler _handler;

		public AddUserIdentityCommandHandlerTest()
		{
			_error = _fixture.Create<IdentityError>();
			_command = _fixture.Create<AddUserIdentityCommand>();
            _identity = _fixture.Create<UserIdentity>();

            //Dependencies
			_manager = MockHelpers.TestUserManager();
            _mapper = new Mock<IMapper>();

            //Setup dependencies
            _mapper.Setup(f => f.Map<UserIdentity>(_command))
                .Returns(_identity);

            //Setup handler
            _handler = new AddUserIdentityCommandHandler(_manager.Object, _mapper.Object);
        }

		[Fact]
		public void Ctor_GivenIdentityService_IsNull_Throws()
		{
            Assert.Throws<ArgumentNullException>("userManager", () => new AddUserIdentityCommandHandler(null, _mapper.Object));
		}

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () => new AddUserIdentityCommandHandler(_manager.Object, null));
        }

		[Fact]
		public async Task Handle()
		{
			//Arrange
			_manager.Setup(m => m.CreateAsync(_identity, _command.Password))
				.ReturnsAsync(IdentityResult.Success);

			_manager.Setup(m => m.SetLockoutEnabledAsync(_identity, true))
				.ReturnsAsync(IdentityResult.Success);
			//Act
			var result = await _handler.Handle(_command, CancellationToken.None);

			//Assert
			Assert.Equal(_identity.Id, result);
		}

		[Fact]
		public async Task Handle_WhenFailedTo_CreateUserIdentity_DueTo_UserNameInUse_Throws()
		{
			//Arrange
			_manager.Setup(m => m.FindByNameAsync(_command.UserName))
				.ReturnsAsync(_identity);

			//Act
			var e = await Assert.ThrowsAsync<IdentityOperationFailureException>(() =>
				_handler.Handle(_command, CancellationToken.None));

			//Assert
			Assert.Contains("add user", e.Message);
			Assert.Empty(e.Errors);
		}

		[Fact]
		public async Task Handle_WhenFailedTo_CreateUserIdentity_Throws()
		{
			//Arrange
			var errors = _fixture.CreateMany<IdentityError>().ToArray();
			var identityResult = IdentityResult.Failed(errors);
			
			_manager.Setup(m => m.CreateAsync(_identity, _command.Password))
				.ReturnsAsync(identityResult);

			//Act
			var e = await Assert.ThrowsAsync<IdentityOperationFailureException>(() =>
				_handler.Handle(_command, CancellationToken.None));

			//Assert
			Assert.Contains("add user", e.Message);
			Assert.Equal(errors.Select(_ => _.Description), e.Errors);
		}


		[Fact]
		public async Task Handle_WhenFailedTo_SetLockoutEnabled_Throws()
		{
			//Arrange
			_manager.Setup(m => m.CreateAsync(_identity, _command.Password))
				.ReturnsAsync(IdentityResult.Success);

			_manager.Setup(m => m.SetLockoutEnabledAsync(_identity, _command.LockEnabled))
				.ReturnsAsync(IdentityResult.Failed(_error));
			//Act
			var e = await Assert.ThrowsAsync<IdentityOperationFailureException>(() =>
				_handler.Handle(_command, CancellationToken.None));

			//Assert
			Assert.Contains("add user", e.Message);
			Assert.Equal(new[] { _error.Description }, e.Errors);
		}

		[Fact]
        public async Task Handle_GivenCommand_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("command", () => _handler.Handle(null, CancellationToken.None));
        }
	}
}