﻿using AutoFixture;
using NexCore.Identity.Api.UnitTests.Fixtures;
using NexCore.Identity.Application.UserIdentities.Commands.AddUserIdentity;
using NexCore.Identity.Application.UserIdentities.Commands.UpdateUserIdentity;
using NexCore.Identity.Common.Entities;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Application.Commands
{
    public class AutoMapperTest
    {
        [Fact]
        public void Map_AddUserIdentityCommand_To_UserIdentity()
        {
            //Arrange
            var fixture = new Fixture();
            var mapper = AutoMapperFactory.Create();
            var expected = fixture.Create<UserIdentity>();

            //Act
            var actual = mapper.Map<UserIdentity>(MapToCommand(expected));

            //Assert
            Assert.NotNull(actual);
            Assert.Equal(expected.Email, actual.Email);
            Assert.Equal(expected.LockoutEnabled, actual.LockoutEnabled);
            Assert.Equal(expected.PhoneNumber, actual.PhoneNumber);
            Assert.Equal(expected.UserName, actual.UserName);
        }

        private static AddUserIdentityCommand MapToCommand(UserIdentity user)
        {
            return new AddUserIdentityCommand
            {
                Email = user.Email,
                LockEnabled = user.LockoutEnabled,
                PhoneNumber = user.PhoneNumber,
                UserName = user.UserName
            };
        }

        [Fact]
        public void Map_UpdateUserIdentityCommand_To_UserIdentity()
        {
	        //Arrange
	        var fixture = new Fixture();
	        var mapper = AutoMapperFactory.Create();
	        var expected = fixture.Create<UserIdentity>();

	        //Act
	        var actual = mapper.Map<UserIdentity>(MapToUpdateCommand(expected));

	        //Assert
	        Assert.NotNull(actual);
	        Assert.Equal(expected.Id, actual.Id);
			Assert.Equal(expected.Email, actual.Email);
	        Assert.Equal(expected.LockoutEnabled, actual.LockoutEnabled);
	        Assert.Equal(expected.PhoneNumber, actual.PhoneNumber);
	        Assert.Equal(expected.UserName, actual.UserName);
        }

        private static UpdateUserIdentityCommand MapToUpdateCommand(UserIdentity user)
        {
	        return new UpdateUserIdentityCommand
	        {
				Id = user.Id,
		        Email = user.Email,
		        PhoneNumber = user.PhoneNumber,
		        UserName = user.UserName,
		        LockoutEnabled = user.LockoutEnabled
	        };
        }
	}
}
