﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Moq;
using Moq.Sequences;
using NexCore.Identity.Api.UnitTests.Fixtures;
using NexCore.Identity.Application.Exceptions;
using NexCore.Identity.Application.UserIdentities.Commands.UpdateUserIdentity;
using NexCore.Identity.Common.Entities;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Application.Commands.UpdateUserIdentity
{
    public class UpdateUserIdentityCommandHandlerTest
	{
		private readonly Fixture _fixture = new Fixture();

		private readonly UpdateUserIdentityCommand _command;
		private readonly UserIdentity _identity;

		//Dependencies
		private readonly Mock<UserManager<UserIdentity>> _manager;
		private readonly Mock<IMapper> _mapper;

		//Handler
		private readonly UpdateUserIdentityCommandHandler _handler;

		public UpdateUserIdentityCommandHandlerTest()
		{
			_identity = _fixture.Create<UserIdentity>();
			_command = _fixture.Create<UpdateUserIdentityCommand>();

			//Dependencies
			_manager = MockHelpers.TestUserManager();
			_mapper = new Mock<IMapper>();

			//Setup dependencies
			_mapper.Setup(f => f.Map<UserIdentity>(_command))
				.Returns(_identity);

			//Setup handler
			_handler = new UpdateUserIdentityCommandHandler(_manager.Object, _mapper.Object);
		}

		[Fact]
		public void Ctor_GivenIdentityService_IsNull_Throws()
		{
			Assert.Throws<ArgumentNullException>("userManager", () => new UpdateUserIdentityCommandHandler(null, _mapper.Object));
		}

		[Fact]
		public void Ctor_GivenMapper_IsNull_Throws()
		{
			Assert.Throws<ArgumentNullException>("mapper", () => new UpdateUserIdentityCommandHandler(_manager.Object, null));
		}

		[Fact]
		public async Task Handle()
		{
            using (Sequence.Create())
            {
                //Arrange
                _manager.Setup(m => m.FindByIdAsync(_command.Id))
                    .InSequence()
                    .ReturnsAsync(_identity);

                _mapper.Setup(m => m.Map(_command, _identity))
                    .InSequence()
                    .Returns(_identity);

                _manager.Setup(m => m.UpdateAsync(_identity))
                    .InSequence()
                    .ReturnsAsync(IdentityResult.Success);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
		}

		[Fact]
		public async Task Handle_WhenFailedTo_FindUserIdentity_Throws()
		{
			//Arrange
            _manager.Setup(m => m.FindByIdAsync(_command.Id))
                .ReturnsAsync(null as UserIdentity);

			//Act
			var e = await Assert.ThrowsAsync<IdentityOperationFailureException>(() =>
				_handler.Handle(_command, CancellationToken.None));

			//Assert
			Assert.Contains("find", e.Message);
            Assert.Empty(e.Errors);
		}

        [Fact]
        public async Task Handle_WhenFailedTo_UpdateUserIdentity_Throws()
        {
            //Arrange
            var errors = _fixture.CreateMany<IdentityError>().ToArray();

            _manager.Setup(m => m.FindByIdAsync(_command.Id))
                .ReturnsAsync(_identity);

            _mapper.Setup(m => m.Map(_command, _identity))
                .Returns(_identity);

            _manager.Setup(m => m.UpdateAsync(_identity))
                .ReturnsAsync(IdentityResult.Failed(errors));

            //Act
            var e = await Assert.ThrowsAsync<IdentityOperationFailureException>(() =>
                _handler.Handle(_command, CancellationToken.None));

            //Assert
            Assert.Contains("update", e.Message);
            Assert.Equal(errors.Select(_ => _.Description), e.Errors);
        }

		[Fact]
		public async Task Handle_GivenCommand_IsNull_Throws()
		{
			await Assert.ThrowsAsync<ArgumentNullException>("command", () => _handler.Handle(null, CancellationToken.None));
		}
	}
}