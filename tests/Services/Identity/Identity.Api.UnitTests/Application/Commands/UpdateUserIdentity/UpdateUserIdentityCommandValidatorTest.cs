﻿using FluentValidation.TestHelper;
using NexCore.Identity.Application.UserIdentities.Commands.UpdateUserIdentity;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Application.Commands.UpdateUserIdentity
{
    public class UpdateUserIdentityCommandValidatorTest
	{
		private readonly UpdateUserIdentityCommandValidator _validator = new UpdateUserIdentityCommandValidator();

		[Fact]
		public void ShouldHaveError_WhenUserId_IsEmpty()
		{
			_validator.ShouldHaveValidationErrorFor(c => c.Id, "");
		}

		[Fact]
		public void ShouldHaveError_WhenUserName_IsEmpty()
		{
			_validator.ShouldHaveValidationErrorFor(c => c.UserName, "");
		}

		[Fact]
		public void ShouldHaveError_WhenEmail_IsEmpty()
		{
			_validator.ShouldHaveValidationErrorFor(c => c.Email, "");
		}

		[Fact]
		public void ShouldHaveError_WhenEmail_IsInvalid()
		{
			_validator.ShouldHaveValidationErrorFor(c => c.Email, "user");
		}

		[Fact]
		public void ShouldNotHaveError_WhenEmail_IsValid()
		{
			_validator.ShouldNotHaveValidationErrorFor(c => c.Email, "tester@mail.com");
		}

		[Fact]
		public void ShouldHaveError_WhenPhoneNumber_IsText()
		{
			_validator.ShouldHaveValidationErrorFor(c => c.PhoneNumber, "phone-phone");
		}

		[Fact]
		public void ShouldNotHaveError_WhenPhoneNumber_IsValid()
		{
			_validator.ShouldNotHaveValidationErrorFor(c => c.PhoneNumber, "3805012345678");
		}

		[Fact]
		public void ShouldHaveError_WhenPhoneNumber_IsTooShort()
		{
			_validator.ShouldHaveValidationErrorFor(c => c.PhoneNumber, "0000");
		}
	}
}