﻿using FluentValidation.TestHelper;
using NexCore.Identity.Application.UserIdentities.Commands.DistributorEmployees.AddDistributorEmployeeIdentity;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Application.Commands.DistributorEmployees.AddDistributorEmployeeIdentity
{
    public class AddDistributorEmployeeIdentityCommandValidatorTest
	{
		private readonly AddDistributorEmployeeIdentityCommandValidator _validator = new AddDistributorEmployeeIdentityCommandValidator();

		[Fact]
		public void ShouldHaveError_WhenUserName_IsEmpty()
		{
			_validator.ShouldHaveValidationErrorFor(c => c.UserName, "");
		}

		[Fact]
		public void ShouldHaveError_WhenPassword_IsEmpty()
		{
			_validator.ShouldHaveValidationErrorFor(c => c.Password, "");
		}
	}
}