﻿using AutoFixture;
using Moq;
using NexCore.Identity.Api.UnitTests.Fixtures;
using NexCore.Identity.Application.Services;
using NexCore.Identity.Application.Services.Factories;
using NexCore.Identity.Application.UserIdentities.Commands.DistributorEmployees.AddDistributorEmployeeIdentity;
using NexCore.Identity.Common.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Application.Commands.DistributorEmployees.AddDistributorEmployeeIdentity
{
	public class AddDistributorEmployeeIdentityCommandHandlerTest
	{
		private readonly AddDistributorEmployeeIdentityCommand _command;
		private readonly UserIdentity _identity;
		private readonly UserIdentityOptions _identityOptions;

		//Dependencies
		private readonly Mock<UserIdentityService> _service;
		private readonly Mock<EmployeeClaimFactory<DistributorEmployee>> _claimFactory;

		public AddDistributorEmployeeIdentityCommandHandlerTest()
		{
			var fixture = new Fixture();

			_command = fixture.Create<AddDistributorEmployeeIdentityCommand>();
			_identity = fixture.Create<UserIdentity>();
			_identityOptions = new UserIdentityOptions(_identity, "P@ssw0rd");

			//Dependencies
			_service = new Mock<UserIdentityService>(MockHelpers.TestUserManager().Object);
			_claimFactory = new Mock<EmployeeClaimFactory<DistributorEmployee>>();
		}

		[Fact]
		public void Ctor_GivenUserService_IsNull_Throws()
		{
			using (var context = InMemoryDbContextFactory.Instance.Create())
			{
				var factory = new Mock<DistributorEmployeeIdentityOptionsFactory>(context, _claimFactory.Object);
				Assert.Throws<ArgumentNullException>("service",
					() => new AddDistributorEmployeeIdentityCommandHandler(null, factory.Object));
			}
		}

		[Fact]
		public void Ctor_GivenFactory_IsNull_Throws()
		{
			Assert.Throws<ArgumentNullException>("factory",
				() => new AddDistributorEmployeeIdentityCommandHandler(_service.Object, null));
		}

		[Fact]
		public async Task Handle_GivenCommand_IsNull_Throws()
		{
			using (var context = InMemoryDbContextFactory.Instance.Create())
			{
				var factory = new Mock<DistributorEmployeeIdentityOptionsFactory>(context, _claimFactory.Object);
				var handler = new AddDistributorEmployeeIdentityCommandHandler(_service.Object, factory.Object);
				await Assert.ThrowsAsync<ArgumentNullException>("request",
					() => handler.Handle(null, CancellationToken.None));
			}
		}

		[Fact]
		public async Task Handle()
		{
			using (var context = InMemoryDbContextFactory.Instance.Create())
			{
				var factory = new Mock<DistributorEmployeeIdentityOptionsFactory>(context, _claimFactory.Object);
				var handler = new AddDistributorEmployeeIdentityCommandHandler(_service.Object, factory.Object);

				//Arrange
				factory.Setup(f => f.Create(_command))
					.ReturnsAsync(_identityOptions);

				_service.Setup(s => s.Create(_identityOptions))
					.ReturnsAsync(_identity.Id);

				_service.Setup(s => s.AddRole(_identityOptions))
					.Returns(Task.CompletedTask);

				_service.Setup(s => s.AddClaims(_identityOptions))
					.Returns(Task.CompletedTask);

				//Act
				var result = await handler.Handle(_command, CancellationToken.None);

				//Assert
				Assert.Equal(_identity.Id, result);
			}
		}
	}
}