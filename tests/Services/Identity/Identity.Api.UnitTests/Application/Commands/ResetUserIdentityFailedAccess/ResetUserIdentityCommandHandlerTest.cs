﻿using AutoFixture;
using Microsoft.AspNetCore.Identity;
using Moq;
using Moq.Sequences;
using NexCore.Common;
using NexCore.Identity.Api.UnitTests.Fixtures;
using NexCore.Identity.Application.Exceptions;
using NexCore.Identity.Application.UserIdentities.Commands.ResetUserIdentityFailedAccessCount;
using NexCore.Identity.Common.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Application.Commands.ResetUserIdentityFailedAccess
{
    public class ResetUserIdentityCommandHandlerTest
	{
		private readonly Fixture _fixture = new Fixture();

		private readonly ResetUserIdentityFailedAccessCountCommand _command;
		private readonly UserIdentity _identity;
		private readonly IdentityError _error;

		//Dependencies
		private readonly Mock<UserManager<UserIdentity>> _manager;

		//Handler
		private readonly ResetUserIdentityFailedAccessCountCommandHandler _handler;

		public ResetUserIdentityCommandHandlerTest()
		{
			_error = _fixture.Create<IdentityError>();
			_identity = _fixture.Create<UserIdentity>();
			_command = _fixture.Create<ResetUserIdentityFailedAccessCountCommand>();

			_command.Id = _identity.Id;

			//Dependencies
			_manager = MockHelpers.TestUserManager();
			
			//Setup handler
			_handler = new ResetUserIdentityFailedAccessCountCommandHandler(_manager.Object);
		}

		[Fact]
		public void Ctor_GivenIdentityService_IsNull_Throws()
		{
			Assert.Throws<ArgumentNullException>("userManager", () => 
				new ResetUserIdentityFailedAccessCountCommandHandler(null));
		}
		
		[Fact]
		public async Task Handle_GivenCommand_IsNull_Throws()
		{
			await Assert.ThrowsAsync<ArgumentNullException>("command", 
				() => _handler.Handle(null, CancellationToken.None));
		}

		[Fact]
		public async Task Handle()
		{
			using (Sequence.Create())
			using (var time = new SystemTimeContext(DateTime.UtcNow))
			{
				//Arrange

				var endDateOffset = new DateTimeOffset(time.Now);

				_manager.Setup(m => m.FindByIdAsync(_command.Id))
					.InSequence()
					.ReturnsAsync(_identity);

				_manager.Setup(m => m.ResetAccessFailedCountAsync(_identity))
					.InSequence()
					.ReturnsAsync(IdentityResult.Success);

				_manager.Setup(m => m.SetLockoutEnabledAsync(_identity, true))
					.InSequence()
					.ReturnsAsync(IdentityResult.Success);

				_manager.Setup(m => m.SetLockoutEndDateAsync(_identity, endDateOffset))
					.InSequence()
					.ReturnsAsync(IdentityResult.Success);
				//Act
				await _handler.Handle(_command, CancellationToken.None);
			}
		}

		[Fact]
		public async Task Handle_WhenFailedTo_FindUserIdentity_Throws()
		{
			using (Sequence.Create())
			{
				//Arrange
				_manager.Setup(m => m.FindByIdAsync(_command.Id))
					.ReturnsAsync(null as UserIdentity);

				//Act
				var e = await Assert.ThrowsAsync<IdentityOperationFailureException>(() =>
					_handler.Handle(_command, CancellationToken.None));

				//Assert
				Assert.Contains("find", e.Message);
				Assert.Empty(e.Errors);
			}
		}

		[Fact]
		public async Task Handle_WhenFailedTo_ResetAccess_Throws()
		{

			//Arrange
			_manager.Setup(m => m.FindByIdAsync(_command.Id))
				.ReturnsAsync(_identity);

			_manager.Setup(m => m.ResetAccessFailedCountAsync(_identity))
				.ReturnsAsync(IdentityResult.Failed(_error));
			//Act
			var e = await Assert.ThrowsAsync<IdentityOperationFailureException>(() =>
				_handler.Handle(_command, CancellationToken.None));

			//Assert
			Assert.Contains("reset", e.Message);
			Assert.Equal(new[] {_error.Description}, e.Errors);
		}

		[Fact]
		public async Task Handle_WhenFailedTo_SetLockoutEnabled_Throws()
		{
			//Arrange
			_manager.Setup(m => m.FindByIdAsync(_command.Id))
				.ReturnsAsync(_identity);

			_manager.Setup(m => m.ResetAccessFailedCountAsync(_identity))
				.ReturnsAsync(IdentityResult.Success);

			_manager.Setup(m => m.SetLockoutEnabledAsync(_identity, true))
				.ReturnsAsync(IdentityResult.Failed(_error));
			//Act
			var e = await Assert.ThrowsAsync<IdentityOperationFailureException>(() =>
				_handler.Handle(_command, CancellationToken.None));

			//Assert
			Assert.Contains("reset", e.Message);
			Assert.Equal(new[] {_error.Description}, e.Errors);
		}

		[Fact]
		public async Task Handle_WhenFailedTo_SetLockoutEndDate_Throws()
		{
			//Arrange

			_manager.Setup(m => m.FindByIdAsync(_command.Id))
				.ReturnsAsync(_identity);

			_manager.Setup(m => m.ResetAccessFailedCountAsync(_identity))
				.ReturnsAsync(IdentityResult.Success);

			_manager.Setup(m => m.SetLockoutEnabledAsync(_identity, true))
				.ReturnsAsync(IdentityResult.Success);

			_manager.Setup(m => m.SetLockoutEndDateAsync(_identity, It.IsAny<DateTimeOffset>()))
				.ReturnsAsync(IdentityResult.Failed(_error));
			//Act
			var e = await Assert.ThrowsAsync<IdentityOperationFailureException>(() =>
				_handler.Handle(_command, CancellationToken.None));

			//Assert
			Assert.Contains("reset", e.Message);
			Assert.Equal(new[] {_error.Description}, e.Errors);
		}
	}
}