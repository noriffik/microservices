﻿using AutoFixture;
using NexCore.Identity.Api.UnitTests.Fixtures;
using NexCore.Identity.Application.Exceptions;
using NexCore.Identity.Application.Services;
using NexCore.Identity.Application.Services.Factories;
using NexCore.Identity.Application.UserIdentities.Commands.DealerEmployees.AddDealerEmployeeIdentity;
using NexCore.Identity.Common;
using NexCore.Identity.Common.Entities;
using NexCore.Testing;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Application.Commands.DealerEmployees.AddDealerEmployeeIdentity
{
	public class EmployeeIdentityOptionsFactorTest
	{
        public EmployeeIdentityOptionsFactorTest()
        {
            _fixture = new Fixture();
        }

        private readonly IFixture _fixture;

        [Fact]
        public void Ctor_GivenUserService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context",
                () => new DealerEmployeeIdentityOptionsFactory(null, new DealerEmployeeClaimFactory()));
        }
        
		[Fact]
		public async Task Create()
		{
			using (var context = InMemoryDbContextFactory.Instance.Create())
			{
				//Arrange
				var employee = _fixture.Create<DealerEmployee>();
				var command = _fixture.Build<AddDealerEmployeeIdentityCommand>()
					.With(c => c.EmployeeId, employee.Id)
					.With(c => c.DistributorId, employee.DistributorId)
					.With(c => c.DealerId, employee.DealerId)
					.Create();

				var claims = new[]
				{
					new Claim(IdentityClaimTypes.IndividualId, employee.IndividualId.ToString()),
					new Claim(IdentityClaimTypes.EmployeeId, employee.Id.ToString()),
					new Claim(IdentityClaimTypes.EmployeeJobId, employee.JobId.ToString()),
					new Claim(IdentityClaimTypes.EmployeeDealerId, employee.DealerId),
					new Claim(IdentityClaimTypes.EmployeeDealerOrganizationId, employee.DealerOrganizationId.ToString()), 
					new Claim(IdentityClaimTypes.EmployeeDealerDistributorId, employee.DistributorId),
					new Claim(IdentityClaimTypes.EmployeeDealerDistributorOrganizationId,
						employee.DistributorOrganizationId.ToString()),
					
				};
				await context.DealerEmployees.AddAsync(employee);
				await context.SaveChangesAsync(CancellationToken.None);

				var factory = new DealerEmployeeIdentityOptionsFactory(context, new DealerEmployeeClaimFactory());

				//Act
				var result = await factory.Create(command);

				//Assert
				Assert.NotNull(result);
				Assert.NotNull(result.User);
				Assert.NotNull(result.User.Employment);
				Assert.Equal(command.UserName, result.User.UserName);
				Assert.Equal(employee.Id, result.User.Employment.EmployeeId);
				Assert.Equal(employee.DistributorId, result.User.Employment.DistributorId);
				Assert.Equal(employee.DealerId, result.User.Employment.DealerId);
				Assert.Equal(employee.Name, result.User.DisplayName);
				Assert.Equal(employee.Telephone, result.User.PhoneNumber);
				Assert.Equal(employee.Email, result.User.Email);
				Assert.Equal(command.Password, result.Password);
				Assert.Equal("DealerEmployee", result.RoleName);
				Assert.Equal(command.LockEnabled, result.User.LockoutEnabled);
				Assert.Equal(claims, result.Claims, PropertyComparer<IEnumerable<Claim>>.Instance);
			}
		}

        [Fact]
        public async Task Create_WhenEmployee_IsNotFound_Throws()
        {
	        using (var context = InMemoryDbContextFactory.Instance.Create())
	        {
		        //Arrange
		        var factory = new DealerEmployeeIdentityOptionsFactory(context, new DealerEmployeeClaimFactory());

		        //Act
		        var e = await Assert.ThrowsAsync<IdentityOperationFailureException>(() =>
			        factory.Create(new AddDealerEmployeeIdentityCommand()));

		        //Assert
		        Assert.Contains("Employee not found", e.Message);
	        }
        }

		[Fact]
        public async Task Create_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var factory = new DealerEmployeeIdentityOptionsFactory(context, new DealerEmployeeClaimFactory());

                //Assert
                await Assert.ThrowsAsync<ArgumentNullException>("request", () => factory.Create(null));
            }
        }
	}
}
