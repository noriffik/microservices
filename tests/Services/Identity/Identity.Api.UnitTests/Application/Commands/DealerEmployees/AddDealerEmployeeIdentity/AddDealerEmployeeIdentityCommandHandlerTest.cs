﻿using AutoFixture;
using Moq;
using NexCore.Identity.Api.UnitTests.Fixtures;
using NexCore.Identity.Application.Services;
using NexCore.Identity.Application.Services.Factories;
using NexCore.Identity.Application.UserIdentities.Commands.DealerEmployees.AddDealerEmployeeIdentity;
using NexCore.Identity.Common.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Application.Commands.DealerEmployees.AddDealerEmployeeIdentity
{
	public class AddDealerEmployeeIdentityCommandHandlerTest
	{
		private readonly AddDealerEmployeeIdentityCommand _command;
		private readonly UserIdentity _identity;
		private readonly UserIdentityOptions _identityOptions;

		//Dependencies
		private readonly Mock<UserIdentityService> _service;
		private readonly Mock<EmployeeClaimFactory<DealerEmployee>> _claimFactory;
		
		public AddDealerEmployeeIdentityCommandHandlerTest()
		{
			var fixture = new Fixture();

			_command = fixture.Create<AddDealerEmployeeIdentityCommand>();
			_identity = fixture.Create<UserIdentity>();
			_identityOptions = new UserIdentityOptions(_identity, "P@ssw0rd");

			//Dependencies
			_service = new Mock<UserIdentityService>(MockHelpers.TestUserManager().Object);
			_claimFactory = new Mock<EmployeeClaimFactory<DealerEmployee>>();
		}

		[Fact]
		public void Ctor_GivenUserService_IsNull_Throws()
		{
			using (var context = InMemoryDbContextFactory.Instance.Create())
			{
				var factory = new Mock<DealerEmployeeIdentityOptionsFactory>(context, _claimFactory.Object);
				Assert.Throws<ArgumentNullException>("service",
					() => new AddDealerEmployeeIdentityCommandHandler(null, factory.Object));
			}
		}

		[Fact]
		public void Ctor_GivenFactory_IsNull_Throws()
		{
			Assert.Throws<ArgumentNullException>("factory",
				() => new AddDealerEmployeeIdentityCommandHandler(_service.Object, null));
		}

		[Fact]
		public async Task Handle_GivenCommand_IsNull_Throws()
		{
			using (var context = InMemoryDbContextFactory.Instance.Create())
			{
				var factory = new Mock<DealerEmployeeIdentityOptionsFactory>(context, _claimFactory.Object);
				var handler = new AddDealerEmployeeIdentityCommandHandler(_service.Object, factory.Object);
				await Assert.ThrowsAsync<ArgumentNullException>("request",
					() => handler.Handle(null, CancellationToken.None));
			}
		}

		[Fact]
		public async Task Handle()
		{
			using (var context = InMemoryDbContextFactory.Instance.Create())
			{
				var factory = new Mock<DealerEmployeeIdentityOptionsFactory>(context, _claimFactory.Object);
				
				factory.Setup(f => f.Create(_command))
					.ReturnsAsync(_identityOptions);
				
				//Arrange
				_service.Setup(s => s.Create(_identityOptions))
					.ReturnsAsync(_identity.Id);

				_service.Setup(s => s.AddRole(_identityOptions))
					.Returns(Task.CompletedTask);

				_service.Setup(s => s.AddClaims(_identityOptions))
					.Returns(Task.CompletedTask);

				var handler = new AddDealerEmployeeIdentityCommandHandler(_service.Object, factory.Object);

				//Act
				var result = await handler.Handle(_command, CancellationToken.None);

				//Assert
				Assert.Equal(_identity.Id, result);
			}
		}
	}
}