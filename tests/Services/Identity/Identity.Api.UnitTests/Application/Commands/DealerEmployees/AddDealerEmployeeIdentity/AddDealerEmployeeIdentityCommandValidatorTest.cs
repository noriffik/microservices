﻿using FluentValidation.TestHelper;
using NexCore.Identity.Application.UserIdentities.Commands.DealerEmployees.AddDealerEmployeeIdentity;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Application.Commands.DealerEmployees.AddDealerEmployeeIdentity
{
	public class AddDealerEmployeeIdentityCommandValidatorTest
	{
		private readonly AddDealerEmployeeIdentityCommandValidator _validator = new AddDealerEmployeeIdentityCommandValidator();

		[Fact]
		public void ShouldHaveError_WhenUserName_IsEmpty()
		{
			_validator.ShouldHaveValidationErrorFor(c => c.UserName, "");
		}

		[Fact]
		public void ShouldHaveError_WhenPassword_IsEmpty()
		{
			_validator.ShouldHaveValidationErrorFor(c => c.Password, "");
		}
	}
}