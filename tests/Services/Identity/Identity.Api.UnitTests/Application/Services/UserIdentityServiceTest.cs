﻿using AutoFixture;
using Microsoft.AspNetCore.Identity;
using Moq;
using NexCore.Identity.Api.UnitTests.Fixtures;
using NexCore.Identity.Application.Exceptions;
using NexCore.Identity.Application.Services;
using NexCore.Identity.Common.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Application.Services
{
    public class UserIdentityServiceTest
	{
		private readonly IFixture _fixture;

		private readonly Mock<UserManager<UserIdentity>> _manager;
		private readonly UserIdentity _identity;
		private readonly UserIdentityOptions _identityOptions;
		private readonly IdentityError _error;
		
		//Service
		private readonly UserIdentityService _service;

		public UserIdentityServiceTest()
		{
			_fixture = new Fixture();

			_error = _fixture.Create<IdentityError>();
			_identity = _fixture.Create<UserIdentity>();
			_identityOptions = new UserIdentityOptions(_identity, "P@ssw0rd");

			_manager = MockHelpers.TestUserManager();

			//Setup service
			_service = new UserIdentityService(_manager.Object);
		}

		[Fact]
		public void Ctor_GivenUserManager_IsNull_Throws()
		{
			Assert.Throws<ArgumentNullException>("userManager", () => new UserIdentityService(null));
		}

		[Fact]
		public async Task CreateUser()
		{
			//Arrange
			_manager.Setup(m => m.CreateAsync(_identityOptions.User, _identityOptions.Password))
				.ReturnsAsync(IdentityResult.Success);

			_manager.Setup(m => m.SetLockoutEnabledAsync(_identityOptions.User, _identity.LockoutEnabled))
				.ReturnsAsync(IdentityResult.Success);

			//Act
			var result = await _service.Create(_identityOptions);

			//Assert
			Assert.Equal(_identity.Id, result);
		}

		[Fact]
		public async Task Service_WhenFailedTo_CreateUserIdentity_DueTo_UserNameInUse_Throws()
		{
			//Arrange
			_manager.Setup(m => m.FindByNameAsync(_identityOptions.User.UserName))
				.ReturnsAsync(_identity);

			//Act
			var e = await Assert.ThrowsAsync<IdentityOperationFailureException>(() =>
				_service.Create(_identityOptions));

			//Assert
			Assert.Contains("add user", e.Message);
			Assert.Empty(e.Errors);
		}

		[Fact]
		public async Task Service_WhenFailedTo_CreateUserIdentity_Throws()
		{
			//Arrange
			var errors = _fixture.CreateMany<IdentityError>().ToArray();
			var identityResult = IdentityResult.Failed(errors);

			_manager.Setup(m => m.CreateAsync(_identityOptions.User, _identityOptions.Password))
				.ReturnsAsync(identityResult);

			//Act
			var e = await Assert.ThrowsAsync<IdentityOperationFailureException>(() =>
				_service.Create(_identityOptions));

			//Assert
			Assert.Contains("add user", e.Message);
			Assert.Equal(errors.Select(_ => _.Description), e.Errors);
		}

		[Fact]
		public async Task Service_WhenFailedTo_SetLockoutEnabled_Throws()
		{
			//Arrange
			_manager.Setup(m => m.CreateAsync(_identityOptions.User, _identityOptions.Password))
				.ReturnsAsync(IdentityResult.Success);

			_manager.Setup(m => m.SetLockoutEnabledAsync(_identityOptions.User, _identity.LockoutEnabled))
				.ReturnsAsync(IdentityResult.Failed(_error));

			//Act
			var e = await Assert.ThrowsAsync<IdentityOperationFailureException>(() =>
				_service.Create(_identityOptions));

			//Assert
			Assert.Contains("add user", e.Message);
			Assert.Equal(new[] { _error.Description }, e.Errors);
		}

		[Fact]
		public async Task Service_WhenFiledTo_AddToRole_Throws()
		{
			//Arrange
			_manager.Setup(m => m.AddToRoleAsync(_identityOptions.User, _identityOptions.RoleName))
				.ReturnsAsync(IdentityResult.Failed(_error));
			//Act
			var e = await Assert.ThrowsAsync<IdentityOperationFailureException>(() =>
				_service.AddRole(_identityOptions));

			//Assert
			Assert.Contains("add user", e.Message);
			Assert.Equal(new[] { _error.Description }, e.Errors);
		}

		[Fact]
		public async Task Service_WhenFiledTo_AddClaims_Throws()
		{
			//Arrange
			_manager.Setup(m => m.AddClaimsAsync(_identityOptions.User, _identityOptions.Claims))
				.ReturnsAsync(IdentityResult.Failed(_error));

			//Act
			var e = await Assert.ThrowsAsync<IdentityOperationFailureException>(() =>
				_service.AddClaims(_identityOptions));

			//Assert
			Assert.Contains("add user", e.Message);
			Assert.Equal(new[] { _error.Description }, e.Errors);
		}
	}
}