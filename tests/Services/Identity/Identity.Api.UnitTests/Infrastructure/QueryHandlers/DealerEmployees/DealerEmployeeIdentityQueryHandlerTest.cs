﻿using AutoMapper;
using Moq;
using NexCore.Identity.Api.UnitTests.Fixtures;
using NexCore.Identity.Application.UserIdentities.Queries.DealerEmployees;
using NexCore.Identity.Infrastructure.QueryHandlers.DealerEmployees;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Infrastructure.QueryHandlers.DealerEmployees
{
	public class DealerEmployeeIdentityQueryHandlerTest
	{
		[Fact]
		public void Ctor_VerifyInjection()
		{
			Assert.Injection.OfConstructor(typeof(DealerEmployeeIdentityQueryHandler)).HasNullGuard();
		}

		[Fact]
		public async Task Handle_GivenRequestById_IsNull_Throws()
		{
			using (var context = InMemoryDbContextFactory.Instance.Create())
			{
				//Arrange
				var handler = new DealerEmployeeIdentityQueryHandler(context, new Mock<IMapper>().Object);

				//Assert
				await Assert.ThrowsAsync<ArgumentNullException>("request", () => handler.Handle(null as DealerEmployeeIdentityByIdQuery, CancellationToken.None));
			}
		}

		[Fact]
		public async Task Handle_GivenRequest_IsNull_Throws()
		{
			using (var context = InMemoryDbContextFactory.Instance.Create())
			{
				//Arrange
				var handler = new DealerEmployeeIdentityQueryHandler(context, new Mock<IMapper>().Object);

				//Assert
				await Assert.ThrowsAsync<ArgumentNullException>("request", () => handler.Handle(null as DealerEmployeeIdentityQuery, CancellationToken.None));
			}
		}
	}
}