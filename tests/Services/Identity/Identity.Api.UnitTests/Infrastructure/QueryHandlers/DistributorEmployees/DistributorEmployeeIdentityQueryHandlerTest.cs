﻿using AutoMapper;
using Moq;
using NexCore.Identity.Api.UnitTests.Fixtures;
using NexCore.Identity.Application.UserIdentities.Queries.DistributorEmployees;
using NexCore.Identity.Infrastructure.QueryHandlers.DistributorEmployees;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Infrastructure.QueryHandlers.DistributorEmployees
{
    public class DistributorEmployeeIdentityQueryHandlerTest
	{
		[Fact]
		public void Ctor_VerifyInjection()
		{
			Assert.Injection.OfConstructor(typeof(DistributorEmployeeIdentityQueryHandler)).HasNullGuard();
		}

		[Fact]
		public async Task Handle_GivenRequestById_IsNull_Throws()
		{
			using (var context = InMemoryDbContextFactory.Instance.Create())
			{
				//Arrange
				var handler = new DistributorEmployeeIdentityQueryHandler(context, new Mock<IMapper>().Object);

				//Assert
				await Assert.ThrowsAsync<ArgumentNullException>("request", () => handler.Handle(null as DistributorEmployeeIdentityByIdQuery, CancellationToken.None));
			}
		}

		[Fact]
		public async Task Handle_GivenRequest_IsNull_Throws()
		{
			using (var context = InMemoryDbContextFactory.Instance.Create())
			{
				//Arrange
				var handler = new DistributorEmployeeIdentityQueryHandler(context, new Mock<IMapper>().Object);

				//Assert
				await Assert.ThrowsAsync<ArgumentNullException>("request", () => handler.Handle(null as DistributorEmployeeIdentityQuery, CancellationToken.None));
			}
		}
	}
}