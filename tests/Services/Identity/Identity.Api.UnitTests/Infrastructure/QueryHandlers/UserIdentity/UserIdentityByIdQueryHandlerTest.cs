﻿using AutoMapper;
using Moq;
using NexCore.Identity.Api.UnitTests.Fixtures;
using NexCore.Identity.Infrastructure.QueryHandlers.UserIdentities;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Infrastructure.QueryHandlers.UserIdentity
{
    public class UserIdentityByIdQueryHandlerTest
	{
		[Fact]
		public void Ctor_VerifyInjection()
		{
			Assert.Injection.OfConstructor(typeof(UserIdentityByIdQueryHandler)).HasNullGuard();
		}

		[Fact]
        public async Task Handle_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new UserIdentityByIdQueryHandler(context, new Mock<IMapper>().Object);

                //Assert
                await Assert.ThrowsAsync<ArgumentNullException>("request", () => handler.Handle(null, CancellationToken.None));
            }
        }
    }
}
