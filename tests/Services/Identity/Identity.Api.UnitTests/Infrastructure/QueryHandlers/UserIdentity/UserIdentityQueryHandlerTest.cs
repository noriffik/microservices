﻿using AutoMapper;
using Moq;
using NexCore.Identity.Api.UnitTests.Fixtures;
using NexCore.Identity.Infrastructure.QueryHandlers.UserIdentities;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Infrastructure.QueryHandlers.UserIdentity
{
    public class UserIdentityQueryHandlerTest
	{
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(UserIdentityQueryHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new UserIdentityQueryHandler(context, new Mock<IMapper>().Object);

                //Assert
                await Assert.ThrowsAsync<ArgumentNullException>("request", () => handler.Handle(null, CancellationToken.None));
            }
        }
	}
}
