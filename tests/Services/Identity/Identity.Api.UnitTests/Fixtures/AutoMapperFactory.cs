﻿using AutoMapper;
using NexCore.Identity.Application.UserIdentities.Commands.AddUserIdentity;

namespace NexCore.Identity.Api.UnitTests.Fixtures
{
    public static class AutoMapperFactory
	{
		public static IMapper Create()
		{
			var application = typeof(AddUserIdentityCommand).Assembly;

			var mapperConfig = new MapperConfiguration(c => c.AddProfiles(application));

			return mapperConfig.CreateMapper();
		}
	}
}