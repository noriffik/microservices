﻿using Microsoft.EntityFrameworkCore;
using NexCore.Identity.Common.Persistence;
using NexCore.Testing.Extensions;

namespace NexCore.Identity.Api.UnitTests.Fixtures
{
    public partial class InMemoryDbContextFactory
    {
        private class TestDbContext : AdminIdentityDbContext
        {
            public TestDbContext(DbContextOptions<AdminIdentityDbContext> options) : base(options)
            {
            }

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                base.OnModelCreating(modelBuilder);

                modelBuilder.SetAutoIncrementStep(1000);
            }
        }
    }
}
