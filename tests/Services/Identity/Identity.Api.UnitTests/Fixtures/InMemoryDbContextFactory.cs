﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Identity.Common.Persistence;
using System;

namespace NexCore.Identity.Api.UnitTests.Fixtures
{
    public partial class InMemoryDbContextFactory
	{
        public readonly string DatabaseName;
        public static InMemoryDbContextFactory Instance => new InMemoryDbContextFactory(Guid.NewGuid().ToString());

        public InMemoryDbContextFactory(Type typeUnderTest) : this(typeUnderTest.FullName)
        {
        }

        public InMemoryDbContextFactory(string databaseName)
        {
            DatabaseName = databaseName;
        }

        public AdminIdentityDbContext Create()
        {
            var services = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase();

            return new TestDbContext(
                new DbContextOptionsBuilder<AdminIdentityDbContext>()
                    .UseInMemoryDatabase(DatabaseName)
                    .UseInternalServiceProvider(services.BuildServiceProvider())
                    .Options);
        }
	}
}