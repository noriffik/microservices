﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using NexCore.Identity.Common.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NexCore.Identity.Api.UnitTests.Fixtures
{
    public static class MockHelpers
	{
        public static Mock<UserManager<UserIdentity>> TestUserManager()
        {
            var store = new Mock<IUserStore<UserIdentity>>().Object;
            var options = new Mock<IOptions<IdentityOptions>>();
            var idOptions = new IdentityOptions {Lockout = {AllowedForNewUsers = false}};
            options.Setup(o => o.Value).Returns(idOptions);
            var userValidators = new List<IUserValidator<UserIdentity>>();
            var validator = new Mock<IUserValidator<UserIdentity>>();
            userValidators.Add(validator.Object);
            var pwdValidators = new List<PasswordValidator<UserIdentity>> {new PasswordValidator<UserIdentity>()};

            var userManager = new Mock<UserManager<UserIdentity>>(store, options.Object, new PasswordHasher<UserIdentity>(),
                userValidators, pwdValidators, new UpperInvariantLookupNormalizer(),
                new IdentityErrorDescriber(), null,
                new Mock<ILogger<UserManager<UserIdentity>>>().Object);

            validator.Setup(v => v.ValidateAsync(userManager.Object, It.IsAny<UserIdentity>()))
                .Returns(Task.FromResult(IdentityResult.Success)).Verifiable();

            return userManager;
        }
	}
}