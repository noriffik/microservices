﻿using NexCore.Application.Queries;
using NexCore.Identity.Api.Controllers;
using NexCore.Identity.Application.UserIdentities.Commands.DistributorEmployees.AddDistributorEmployeeIdentity;
using NexCore.Identity.Application.UserIdentities.Queries.DistributorEmployees;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Api.Controllers
{
    public class DistributorEmployeeIdentityControllerTest
	{
		[Fact]
		public void Ctor_VerifyInjection()
		{
			Assert.Injection.OfConstructor(typeof(DistributorEmployeeIdentityController)).HasNullGuard();
		}

		[Fact]
		public Task Get_ById()
		{
			//Arrange
			var action = Assert.Action<DistributorEmployeeIdentityDto>(typeof(DistributorEmployeeIdentityController), typeof(DistributorEmployeeIdentityByIdQuery));

			//Asserts
			return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
		}

		[Fact]
		public Task Post()
		{
			//Arrange
			var action = Assert.Action<string>(typeof(DistributorEmployeeIdentityController), typeof(AddDistributorEmployeeIdentityCommand));

			//Assert
			return action.IsCreated(result => result.ActionName == "Get")
				.IsBadRequestOnNull()
				.Run();
		}

		[Fact]
		public Task Get_ByPaged()
		{
			var action = Assert.Action<PagedResponse<DistributorEmployeeIdentityDto>>(typeof(DistributorEmployeeIdentityController), typeof(DistributorEmployeeIdentityQuery));

			//Asserts
			return action.IsJson().IsBadRequestOnNull().IsNotFoundWhenFailed().Run();
		}
	}
}