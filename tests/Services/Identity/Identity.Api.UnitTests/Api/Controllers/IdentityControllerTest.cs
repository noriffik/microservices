﻿using NexCore.Application.Queries;
using NexCore.Identity.Api.Controllers;
using NexCore.Identity.Application.UserIdentities.Commands.AddUserIdentity;
using NexCore.Identity.Application.UserIdentities.Commands.ResetUserIdentityFailedAccessCount;
using NexCore.Identity.Application.UserIdentities.Commands.UpdateUserIdentity;
using NexCore.Identity.Application.UserIdentities.Queries.UserIdentity;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Api.Controllers
{
    public class IdentityControllerTest
	{
		[Fact]
		public void Ctor_VerifyInjection()
		{
			Assert.Injection.OfConstructor(typeof(IdentitiesController)).HasNullGuard();
		}
		
		[Fact]
		public Task Post()
		{
            //Arrange
			var action = Assert.Action<string>(typeof(IdentitiesController), typeof(AddUserIdentityCommand));

			//Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
		}

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<PagedResponse<UserIdentityDto>>(typeof(IdentitiesController), typeof(UserIdentityQuery));

            //Asserts
            return action.IsJson().IsBadRequestOnNull().IsNotFoundWhenFailed().Run();
        }

		[Fact]
		public Task Get_ById()
		{
			//Arrange
			var action = Assert.Action<UserIdentityDto>(typeof(IdentitiesController), typeof(UserIdentityByIdQuery));

			//Asserts
			return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
		}

		[Fact]
		public Task UpdateUserIdentity()
		{
			//Arrange
			var action = Assert.Action(typeof(IdentitiesController), typeof(UpdateUserIdentityCommand));

			//Assert
			return action.IsOk().IsBadRequestOnNull().Run();
		}

		[Fact]
		public Task ResetFailedAccessCount()
		{
			//Arrange
			var action = Assert.Action(typeof(IdentitiesController), typeof(ResetUserIdentityFailedAccessCountCommand));

			//Assert
			return action.IsOk().IsBadRequestOnNull().Run();
		}
	}
}