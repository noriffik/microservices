﻿using System.Threading.Tasks;
using NexCore.Application.Queries;
using NexCore.Identity.Api.Controllers;
using NexCore.Identity.Application.UserIdentities.Commands.DealerEmployees.AddDealerEmployeeIdentity;
using NexCore.Identity.Application.UserIdentities.Queries.DealerEmployees;
using NexCore.Identity.Application.UserIdentities.Queries.DistributorEmployees;
using Xunit;

namespace NexCore.Identity.Api.UnitTests.Api.Controllers
{
	public class DealerEmployeeIdentityControllerTest
	{
		[Fact]
		public void Ctor_VerifyInjection()
		{
			Assert.Injection.OfConstructor(typeof(DealerEmployeeIdentityController)).HasNullGuard();
		}

		[Fact]
		public Task Post()
		{
			//Arrange
			var action = Assert.Action<string>(typeof(DealerEmployeeIdentityController), typeof(AddDealerEmployeeIdentityCommand));

			//Assert
			return action.IsCreated(result => result.ActionName == "Get")
				.IsBadRequestOnNull()
				.Run();
		}

		[Fact]
		public Task Get_ById()
		{
			//Arrange
			var action = Assert.Action<DealerEmployeeIdentityDto>(typeof(DealerEmployeeIdentityController), typeof(DealerEmployeeIdentityByIdQuery));

			//Asserts
			return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
		}

		[Fact]
		public Task Get_ByPaged()
		{
			var action = Assert.Action<PagedResponse<DealerEmployeeIdentityDto>>(typeof(DealerEmployeeIdentityController), typeof(DealerEmployeeIdentityQuery));

			//Asserts
			return action.IsJson().IsBadRequestOnNull().IsNotFoundWhenFailed().Run();
		}
	}
}