﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Identity.Common.Persistence;
using System.Reflection;

namespace NexCore.Identity.Api.FunctionalTests.Fixtures
{
    public class SqlServerDbContextFactory
	{
		public AdminIdentityDbContext Create(string databaseName)
		{
			var connectionString = ConnectionStringProvider.Get(databaseName);

			var migrationAssembly = typeof(MigrationService.Startup).GetTypeInfo().Assembly.GetName().Name;

			var services = new ServiceCollection()
				.AddEntityFrameworkSqlServer();

			var options = new DbContextOptionsBuilder<AdminIdentityDbContext>()
				.UseInternalServiceProvider(services.BuildServiceProvider())
				.UseSqlServer(
					connectionString,
					sqlOptions => {
						sqlOptions
							.MigrationsAssembly(migrationAssembly)
							.EnableRetryOnFailure(10);
					})
				.Options;

			return new AdminIdentityDbContext(options);
		}
	}
}