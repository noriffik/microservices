﻿using Microsoft.EntityFrameworkCore;
using NexCore.Identity.Common.Persistence;
using System;

namespace NexCore.Identity.Api.FunctionalTests.Fixtures
{
    public abstract class SpecificationFixture : IDisposable
	{
		protected string DatabaseName { get; }

		protected readonly SqlServerDbContextFactory DbContextFactory;

		protected SpecificationFixture(string databaseName)
		{
			DatabaseName = databaseName;
			DbContextFactory = new SqlServerDbContextFactory();

			SetupDatabase();
		}

		public AdminIdentityDbContext CreateContext()
		{
			return DbContextFactory.Create(DatabaseName);
		}

		protected void SetupDatabase()
		{
			using (var context = CreateContext())
			{
				context.Database.Migrate();

				SeedDatabase(context);
			}
		}

		protected virtual void SeedDatabase(AdminIdentityDbContext context)
		{
		}

		public virtual void Dispose()
		{
			CreateContext().Database.EnsureDeleted();
		}
	}
}