﻿using AutoFixture;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Identity.Api.FunctionalTests.Api.Controllers;
using NexCore.Identity.Common.Entities;
using NexCore.Identity.Common.Persistence;
using System;
using System.Net.Http;
using Xunit;

namespace NexCore.Identity.Api.FunctionalTests.Fixtures
{
    public abstract class ApiControllerTestFixture : IClassFixture<ServiceFactory<TestStartup>>, IDisposable
	{
		protected readonly HttpClient Client;
		protected readonly AdminIdentityDbContext DbContext;
        protected readonly RoleManager<UserIdentityRole> RoleManager;
		protected readonly IFixture Fixture;

		protected readonly IServiceScope Scope;

		protected ApiControllerTestFixture(ServiceFactory<TestStartup> factory)
		{
			Client = factory.CreateClient();
			Scope = factory.CreateScope();
			DbContext = Scope.ServiceProvider.GetRequiredService<AdminIdentityDbContext>();
            RoleManager = Scope.ServiceProvider.GetRequiredService<RoleManager<UserIdentityRole>>();
			Fixture = new Fixture();
		}

		public void Dispose()
		{
			DbContext.Database.EnsureDeleted();

			Scope.Dispose();
		}
	}
}
