﻿using AutoMapper;
using NexCore.Identity.Application.UserIdentities.Commands.AddUserIdentity;
using NexCore.Identity.Infrastructure;

namespace NexCore.Identity.Api.FunctionalTests.Fixtures
{
    public static class AutoMapperFactory
	{
		public static IMapper Create()
		{
			var application = typeof(AddUserIdentityCommand).Assembly;
			var infrastructure = typeof(IdentityContext).Assembly;

			var mapperConfig = new MapperConfiguration(c => c.AddProfiles(application, infrastructure));

			return mapperConfig.CreateMapper();
		}
	}
}