﻿namespace NexCore.Identity.Api.FunctionalTests.Fixtures
{
	internal class ConnectionStringProvider
	{
		public static string Get(string name) => Configuration.Get["ConnectionString"]
			.Replace("%DATABASE%", $"NexCore.Identity.Api_{name}");
	}
}