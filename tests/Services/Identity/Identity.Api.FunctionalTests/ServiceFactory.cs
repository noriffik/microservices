﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Identity.Common.Persistence;

namespace NexCore.Identity.Api.FunctionalTests
{
    public class ServiceFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
	{
		protected override void ConfigureWebHost(IWebHostBuilder builder)
		{
			base.ConfigureWebHost(builder);

			builder.UseContentRoot(".");

			builder.ConfigureServices(services =>
			{
				var serviceProvider = new ServiceCollection()
					.AddEntityFrameworkInMemoryDatabase()
					.BuildServiceProvider();

				services.AddScoped(c =>
				{
					var context = new AdminIdentityDbContext(new DbContextOptionsBuilder<AdminIdentityDbContext>()
						.UseInMemoryDatabase(nameof(AdminIdentityDbContext))
						.UseInternalServiceProvider(serviceProvider)
						.Options);

					//Eager load dealer entities to enable InMemory database testing
					context.Users.Load();

					return context;
				});
			});
		}

		protected override IWebHostBuilder CreateWebHostBuilder()
		{
			return WebHost.CreateDefaultBuilder()
				.UseStartup<TStartup>();
		}

		public IServiceScope CreateScope()
		{
			return Server.Host.Services.CreateScope();
		}
	}
}