using Microsoft.Extensions.Configuration;

namespace NexCore.Identity.Api.FunctionalTests
{
	public class Configuration
	{
		private static IConfigurationRoot _root;

		public static IConfigurationRoot Get => _root ?? (_root = new ConfigurationBuilder()
			                                        .AddJsonFile("appsettings.json", true)
			                                        .AddUserSecrets("4FC0E032-46E3-44E6-A10E-7338A34EEC1B")
			                                        .Build());
	}
}
