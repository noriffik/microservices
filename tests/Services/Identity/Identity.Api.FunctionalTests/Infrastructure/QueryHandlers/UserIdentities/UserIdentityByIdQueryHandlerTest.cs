﻿using AutoFixture;
using Microsoft.EntityFrameworkCore;
using NexCore.Identity.Api.FunctionalTests.Fixtures;
using NexCore.Identity.Application.UserIdentities.Queries.UserIdentity;
using NexCore.Identity.Common.Entities;
using NexCore.Identity.Infrastructure.QueryHandlers.UserIdentities;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.FunctionalTests.Infrastructure.QueryHandlers.UserIdentities
{
    public class UserIdentityByIdQueryHandlerTest : SpecificationFixture
	{
		public UserIdentityByIdQueryHandlerTest() : base(Guid.NewGuid().ToString())
		{
		}

		[Fact]
		public async Task Handle()
		{
			//Arrange
			var fixture = new Fixture();
			var mapper = AutoMapperFactory.Create();
			
            var user = fixture.CreateMany<UserIdentity>(3).ElementAt(1);
			var expected = mapper.Map<UserIdentityDto>(user);

			var query = new UserIdentityByIdQuery {Id = user.Id};

			using (var context = CreateContext())
			{
				await context.Database.MigrateAsync();

				await context.AddAsync(user);
				await context.SaveChangesAsync();
            }

            using (var context = CreateContext())
            {
				var handler = new UserIdentityByIdQueryHandler(context, mapper);

				//Act
				var actual = await handler.Handle(query, CancellationToken.None);

				//Assert
				Assert.Equal(expected.UserName, actual.UserName);
			}
		}
	}
}