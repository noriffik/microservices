﻿using AutoFixture;
using Microsoft.EntityFrameworkCore;
using NexCore.Application.Queries;
using NexCore.Identity.Api.FunctionalTests.Fixtures;
using NexCore.Identity.Application.UserIdentities.Queries.UserIdentity;
using NexCore.Identity.Common.Entities;
using NexCore.Identity.Infrastructure.QueryHandlers.UserIdentities;
using NexCore.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.FunctionalTests.Infrastructure.QueryHandlers.UserIdentities
{
    public class UserIdentityQueryHandlerTest : SpecificationFixture
    {
        private const int PageNumber = 3;
        private const int PageSize = 2;

		public UserIdentityQueryHandlerTest() : base(Guid.NewGuid().ToString())
		{
		}

		[Fact]
		public async Task Handle()
		{
			//Arrange
			var fixture = new Fixture();
			var mapper = AutoMapperFactory.Create();

			var users = fixture.CreateMany<UserIdentity>(PageSize * (PageNumber + 1))
                .OrderBy(u => u.UserName)
                .ToList();

            var query = new UserIdentityQuery
            {
                PageNumber = PageNumber,
                PageSize = PageSize,
                SortBy = SortCriterion.UserName,
                SortDirection = SortDirection.Ascending
            };

            var items = mapper.Map<IEnumerable<UserIdentityDto>>(users.Skip((PageNumber - 1) * PageSize).Take(PageSize)).ToArray();
            var expected = new PagedResponse<UserIdentityDto>(new PageOptions(query.PageNumber.Value, query.PageSize.Value), users.Count, items);

			using (var context = CreateContext())
			{
				await context.Database.MigrateAsync();

				await context.AddRangeAsync(users);
				await context.SaveChangesAsync();
            }
			
            using (var context = CreateContext())
            {
				var handler = new UserIdentityQueryHandler(context, mapper);

				//Act
				var actual = await handler.Handle(query, CancellationToken.None);

				//Assert
				Assert.Equal(expected, actual, PropertyComparer<PagedResponse<UserIdentityDto>>.Instance);
			}
		}

		[Fact]
		public async Task Handle_WithUserNameFilter()
		{
			//Arrange
			var fixture = new Fixture();
			var mapper = AutoMapperFactory.Create();

			var user = fixture.CreateMany<UserIdentity>(3).ElementAt(1);
			var expected = mapper.Map<UserIdentityDto>(user);

			var query = new UserIdentityQuery { UserName = user.UserName };

			using (var context = CreateContext())
			{
				await context.Database.MigrateAsync();

				await context.AddAsync(user);
				await context.SaveChangesAsync();
			}

			using (var context = CreateContext())
			{
				var handler = new UserIdentityQueryHandler(context, mapper);

				//Act
				var actual = await handler.Handle(query, CancellationToken.None);

				//Assert
				Assert.Equal(expected.UserName, actual.Items.FirstOrDefault()?.UserName);
			}
		}

		[Fact]
		public async Task Handle_WithDisplayNameFilter()
		{
			//Arrange
			var fixture = new Fixture();
			var mapper = AutoMapperFactory.Create();

			var user = fixture.CreateMany<UserIdentity>(3).ElementAt(1);
			var expected = mapper.Map<UserIdentityDto>(user);

			var query = new UserIdentityQuery { DisplayName = user.DisplayName };

			using (var context = CreateContext())
			{
				await context.Database.MigrateAsync();

				await context.AddAsync(user);
				await context.SaveChangesAsync();
			}

			using (var context = CreateContext())
			{
				var handler = new UserIdentityQueryHandler(context, mapper);

				//Act
				var actual = await handler.Handle(query, CancellationToken.None);

				//Assert
				Assert.Equal(expected.UserName, actual.Items.FirstOrDefault()?.UserName);
			}

		}
	}
}