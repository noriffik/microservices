﻿using AutoMapper;
using NexCore.Application.Queries;
using NexCore.Identity.Api.FunctionalTests.Fixtures;
using NexCore.Identity.Application.UserIdentities.Queries;
using NexCore.Identity.Application.UserIdentities.Queries.DistributorEmployees;
using NexCore.Identity.Infrastructure.QueryHandlers;
using NexCore.Identity.Infrastructure.QueryHandlers.DistributorEmployees;
using NexCore.Testing;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.FunctionalTests.Infrastructure.QueryHandlers.DistributorEmployees
{
	public class DistributorEmployeeIdentityQueryHandlerTest : QueryFixture
	{
		private readonly IMapper _mapper;
        private readonly DistributorEmployeeIdentityQuery query = new DistributorEmployeeIdentityQuery
        {
            DistributorId = "EKK",
			PageNumber = 1,
			PageSize = 2,
			SortCriterion = SortCriterion.DisplayName,
			SortDirection = SortDirection.Ascending
        };

		public DistributorEmployeeIdentityQueryHandlerTest()
		{
			_mapper = AutoMapperFactory.Create();
		}

		[Fact]
		public async Task Handle()
		{
			//Arrange
			var expected = Map(await Seed(query.DistributorId), query.PageNumber.Value, query.PageSize.Value);

            using (var context = CreateContext())
			{
				var handler = new DistributorEmployeeIdentityQueryHandler(context, _mapper);

				//Act
				var actual = await handler.Handle(query, CancellationToken.None);

				//Assert
				Assert.Equal(expected, actual,
					PropertyComparer<PagedResponse<DistributorEmployeeIdentityDto>>.Instance);
			}
		}

		[Fact]
		public async Task Handle_WithDisplayNameFilter()
		{
            //Arrange
            var records = (await Seed(query.DistributorId)).Skip(1).Take(1);
            var expected = Map(records, query.PageNumber.Value, query.PageSize.Value);

            query.DisplayName = expected.Items.Single().DisplayName;

            using (var context = CreateContext())
			{
				var handler = new DistributorEmployeeIdentityQueryHandler(context, _mapper);

				//Act
				var actual = await handler.Handle(query, CancellationToken.None);

				//Assert
				Assert.Equal(expected, actual,
					PropertyComparer<PagedResponse<DistributorEmployeeIdentityDto>>.Instance);
			}
		}

        [Fact]
		public async Task Handle_WithUserNameFilter()
		{
            //Arrange
            var records = (await Seed(query.DistributorId)).Skip(1).Take(1);
            var expected = Map(records, query.PageNumber.Value, query.PageSize.Value);

            query.UserName = expected.Items.Single().UserName;

            using (var context = CreateContext())
			{
				var handler = new DistributorEmployeeIdentityQueryHandler(context, _mapper);

				//Act
				var actual = await handler.Handle(query, CancellationToken.None);

				//Assert
				Assert.Equal(expected, actual,
					PropertyComparer<PagedResponse<DistributorEmployeeIdentityDto>>.Instance);
			}
		}

        private PagedResponse<DistributorEmployeeIdentityDto> Map(IEnumerable<UserIdentityRecord> records, int pageNumber, int pageSize)
        {
            var items = _mapper.Map<IEnumerable<DistributorEmployeeIdentityDto>>(records
                .Skip((pageNumber - 1) * pageSize)
			    .Take(pageSize)).ToArray();

            return new PagedResponse<DistributorEmployeeIdentityDto>(
				new PageOptions(pageNumber, pageSize), records.Count(), items);
        }
	}
}