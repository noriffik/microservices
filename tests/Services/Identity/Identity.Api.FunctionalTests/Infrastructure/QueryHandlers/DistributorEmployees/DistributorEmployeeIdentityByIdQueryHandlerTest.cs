﻿using AutoFixture;
using AutoMapper;
using NexCore.Identity.Api.FunctionalTests.Fixtures;
using NexCore.Identity.Application.UserIdentities.Queries.DistributorEmployees;
using NexCore.Identity.Infrastructure.QueryHandlers;
using NexCore.Identity.Infrastructure.QueryHandlers.DistributorEmployees;
using NexCore.Testing;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.FunctionalTests.Infrastructure.QueryHandlers.DistributorEmployees
{
	public class DistributorEmployeeIdentityByIdQueryHandlerTest : QueryFixture
	{
		private readonly IMapper _mapper;

		public DistributorEmployeeIdentityByIdQueryHandlerTest()
		{
			_mapper = AutoMapperFactory.Create();
		}

		[Fact]
		public async Task Handle()
		{
			var expected = Map(await Seed("EKK"));
			
			var query = new DistributorEmployeeIdentityByIdQuery
			{
				Id = expected.Id,
				DistributorId = expected.EmployeeDistributorId
			};

			using (var context = CreateContext())
			{
				var handler = new DistributorEmployeeIdentityQueryHandler(context, _mapper);

				var actual = await handler.Handle(query, CancellationToken.None);

				Assert.Equal(expected, actual, PropertyComparer<DistributorEmployeeIdentityDto>.Instance);
			}
		}

		[Fact]
		public async Task Handle_WhenUserIdentity_IsNotFound_ReturnsNull()
		{
			using (var context = CreateContext())
			{
				//Arrange
				var query = _fixture.Create<DistributorEmployeeIdentityByIdQuery>();
				var handler = new DistributorEmployeeIdentityQueryHandler(context, _mapper);

				//Act
				var result = await handler.Handle(query, CancellationToken.None);

				//Assert
				Assert.Null(result);
			}
		}

        private DistributorEmployeeIdentityDto Map(IEnumerable<UserIdentityRecord> records)
        {
            return _mapper.Map<DistributorEmployeeIdentityDto>(new UserIdentityRecord
			{
				User = records.First().User,
				Claims = records.First().Claims
			});
        }
	}
}