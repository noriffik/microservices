﻿using AutoFixture;
using NexCore.Identity.Common;
using NexCore.Identity.Common.Entities;
using System.Collections.Generic;
using System.Linq;

namespace NexCore.Identity.Api.FunctionalTests.Infrastructure.QueryHandlers.DistributorEmployees
{
    public static class FixtureExtensions
	{
		public static UserIdentity CreateUser(this IFixture fixture, DistributorEmployee employee)
		{
			return fixture.Build<UserIdentity>()
                .With(u => u.DisplayName, employee.Name)
				.With(u => u.Employment, new UserIdentityEmployment
				{
					EmployeeId = employee.Id,
					DistributorId = employee.DistributorId
				})
				.Create();
		}

		public static IEnumerable<UserIdentityUserClaim> CreateClaims(this IFixture fixture, UserIdentity user)
		{
			return new[]
			{
				new UserIdentityUserClaim
				{
					UserId = user.Id,
					ClaimType = IdentityClaimTypes.EmployeeId,
					ClaimValue = user.Employment.EmployeeId.ToString()
				},
				new UserIdentityUserClaim
				{
					UserId = user.Id,
					ClaimType = IdentityClaimTypes.EmployeeDistributorId,
					ClaimValue = user.Employment.DistributorId
				}
			};
		}

		public static List<DistributorEmployee> CreateDistributorEmployees(this IFixture fixture)
		{
			return fixture.CreateMany<DistributorEmployee>(3)
				.OrderBy(x => x.Name)
				.ToList();
		}

        public static List<DistributorEmployee> CreateDistributorEmployees(this IFixture fixture, string distributorId)
		{
			return fixture.Build<DistributorEmployee>()
                .With(e => e.DistributorId, distributorId)
                .CreateMany(3)
				.ToList();
		}
	}
}