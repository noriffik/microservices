﻿using AutoFixture;
using NexCore.Identity.Api.FunctionalTests.Fixtures;
using NexCore.Identity.Infrastructure.QueryHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.Identity.Api.FunctionalTests.Infrastructure.QueryHandlers.DistributorEmployees
{
	public class QueryFixture : SpecificationFixture
    {
        protected readonly IFixture _fixture;

		public QueryFixture() : base(Guid.NewGuid().ToString())
		{
			_fixture = new Fixture();
		}

        public async Task<IEnumerable<UserIdentityRecord>> Seed(string distributorId)
		{
			var employees = _fixture.CreateDistributorEmployees(distributorId).OrderBy(e => e.Name);
			var users = employees.Select(_fixture.CreateUser).ToList();
			var claims = users.ToDictionary(u => u.Id, u => _fixture.CreateClaims(u));
            var unexpected = _fixture.CreateDistributorEmployees("xxx")
                .Select(_fixture.CreateUser)
                .ToList();

            using (var context = CreateContext())
			{
                await context.AddRangeAsync(unexpected);
				await context.AddRangeAsync(users);
				await context.AddRangeAsync(claims.SelectMany(c => c.Value));
				await context.SaveChangesAsync();
			}

            return users.Select(e => new UserIdentityRecord
			{
				User = e,
				Claims = claims[e.Id]
			});
		}
    }
}