﻿using AutoFixture;
using NexCore.Identity.Api.FunctionalTests.Fixtures;
using NexCore.Identity.Application.UserIdentities.Commands.DealerEmployees.AddDealerEmployeeIdentity;
using NexCore.Identity.Application.UserIdentities.Queries.DealerEmployees;
using NexCore.Identity.Common;
using NexCore.Identity.Common.Entities;
using NexCore.Testing.Extensions;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.FunctionalTests.Api.Controllers
{
	public class DealerEmployeeIdentityControllerTest : ApiControllerTestFixture
	{
		private const string BaseUrl = "api/distributors";

		private const int PageNumber = 1;
		private const int PageSize = 3;

		public DealerEmployeeIdentityControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
		{
		}

		[Fact]
		public async Task Post()
		{
			//Arrange
			var employee = await SeedDealerEmployee();

			var command = Fixture.Build<AddDealerEmployeeIdentityCommand>()
				.With(c => c.UserName, employee.Name)
				.With(c => c.DealerId, employee.DealerId)
				.With(c => c.DistributorId, employee.DistributorId)
				.With(c => c.Password, "P@ssw0rd").Create();

			//Act
			var response = await Client.PostCommand($"{BaseUrl}/{command.DistributorId}/dealers/{command.DealerId}/users", command);

			//Assert
			response.EnsureSuccessStatusCode();
		}

		private async Task<DealerEmployee> SeedDealerEmployee()
		{
			await RoleManager.CreateAsync(new UserIdentityRole
			{
				Name = "DealerEmployee"
			});

			var employee = Fixture.Create<DealerEmployee>();

			await DbContext.DealerEmployees.AddAsync(employee);
			await DbContext.SaveChangesAsync();

			return employee;
		}

		private async Task<UserIdentity> SeedUser(DealerEmployee employee)
		{
			var user = Fixture.Build<UserIdentity>()
				.With(u => u.Employment, new UserIdentityEmployment
				{
					EmployeeId = employee.Id,
					DistributorId = employee.DistributorId,
					DealerId = employee.DealerId
				})
				.Create();

			var claims = new[]
			{
				new UserIdentityUserClaim
				{
					UserId = user.Id,
					ClaimType = IdentityClaimTypes.EmployeeId,
					ClaimValue = employee.Id.ToString()
				},
				new UserIdentityUserClaim
				{
					UserId = user.Id,
					ClaimType = IdentityClaimTypes.EmployeeDealerId,
					ClaimValue = employee.DealerId
				}
			};

			await DbContext.Users.AddAsync(user);

			await DbContext.UserClaims.AddRangeAsync(claims);

			await DbContext.SaveChangesAsync();

			return user;
		}

		private async Task<UserIdentity> SeedUser()
		{
			var user = Fixture.Create<UserIdentity>();

			var claims = Fixture.Build<UserIdentityUserClaim>().Create();

			await DbContext.Users.AddAsync(user);

			await DbContext.UserClaims.AddAsync(claims);

			await DbContext.SaveChangesAsync();

			return user;
		}

		[Fact]
		public async Task Get_ById()
		{
			//Arrange
			var employee = await SeedDealerEmployee();
			var user = await SeedUser(employee);

			var query = new DealerEmployeeIdentityByIdQuery
			{
				Id = user.Id,
				DistributorId = employee.DistributorId,
				DealerId = employee.DealerId
			};

			var url = $"{BaseUrl}/{query.DistributorId}/dealers/{query.DealerId}/users/{query.Id}";
			//Act
			var response = await Client.GetAsync(url);

			//Assert
			response.EnsureSuccessStatusCode();
		}

		[Fact]
		public async Task Get_PagedList()
		{
			//Arrange
			var employee = await SeedDealerEmployee();
			var user = await SeedUser();

			var query = new DealerEmployeeIdentityQuery
			{
				DistributorId = employee.DistributorId,
				DealerId = employee.DealerId,
				UserName = user.UserName,
				DisplayName = user.DisplayName,
				PageSize = PageSize,
				PageNumber = PageNumber
			};
			var url =
				$"{BaseUrl}/{query.DistributorId}/dealers/{query.DealerId}/users/?PageNumber={query.PageNumber}&PageSize={query.PageSize}";
			var response = await Client.GetAsync(url);
			
			response.EnsureSuccessStatusCode();
		}

		[Fact]
		public async Task Get_ById_WhenNotFound_ReturnsNotFound()
		{
			var user = await SeedUser();

			var query = new DealerEmployeeIdentityByIdQuery
			{
				Id = user.Id
			};
			//Act
			var response = await Client.GetAsync($"{BaseUrl}/{query.DistributorId}/dealers/{query.DealerId}/users/{query.Id}");

			//Assert
			Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
		}

		[Fact]
		public async Task Post_GivenCommand_EmployeeNotFound_ReturnsBadRequest()
		{
			//Arrange
			var command = Fixture.Build<AddDealerEmployeeIdentityCommand>()
				.With(c => c.Password, "P@ssw0rd")
				.Create();

			//Act
			var response = await Client.PostCommand($"{BaseUrl}/{command.DistributorId}/dealers/{command.DealerId}/users", command);

			//Assert
			Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
		}

		[Fact]
		public async Task Post_GivenCommand_IsInvalid_ReturnsBadRequest()
		{
			//Arrange
			var command = new AddDealerEmployeeIdentityCommand();

			//Act
			var response = await Client.PostCommand($"{BaseUrl}/invalid/users", command);

			//Assert
			Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
		}

		[Fact]
		public async Task Post_GivenPassword_IsInvalid_ReturnsBadRequest()
		{
			//Arrange
			var command = Fixture.Build<AddDealerEmployeeIdentityCommand>()
				.With(c => c.Password, "1111")
				.Create();
			//Act
			var response = await Client.PostCommand($"{BaseUrl}/{command.DistributorId}/dealers/{command.DealerId}/users", command);

			//Assert
			Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
		}

		[Fact]
		public async Task Post_GivenCommand_IsNull_ReturnsBadRequest()
		{
			//Act
			var response = await Client.PostCommand($"{BaseUrl}/invalid/users", "invalid");

			//Assert
			Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
		}
	}
}