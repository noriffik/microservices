﻿using AutoFixture;
using NexCore.Identity.Api.FunctionalTests.Fixtures;
using NexCore.Identity.Application.UserIdentities.Commands.DistributorEmployees.AddDistributorEmployeeIdentity;
using NexCore.Identity.Application.UserIdentities.Queries.DistributorEmployees;
using NexCore.Identity.Common;
using NexCore.Identity.Common.Entities;
using NexCore.Testing.Extensions;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.FunctionalTests.Api.Controllers
{
	public class DistributorEmployeeIdentityControllerTest : ApiControllerTestFixture
	{
		private const string BaseUrl = "api/distributors";

		private const int PageNumber = 1;
		private const int PageSize = 3;

		public DistributorEmployeeIdentityControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
		{
		}

		[Fact]
		public async Task Post()
		{
			//Arrange
			var employee = await SeedDistributorEmployee();

			var command = Fixture.Build<AddDistributorEmployeeIdentityCommand>()
				.With(c => c.UserName, employee.Name)
				.With(c => c.DistributorId, employee.DistributorId)
				.With(c => c.Password, "P@ssw0rd")
				.Create();

			//Act
			var response = await Client.PostCommand($"{BaseUrl}/{command.DistributorId}/users", command);

			//Assert
			response.EnsureSuccessStatusCode();
		}

		private async Task<DistributorEmployee> SeedDistributorEmployee()
		{
            await RoleManager.CreateAsync(new UserIdentityRole {
                Name = "DistributorEmployee"
            });

			var employee = Fixture.Create<DistributorEmployee>();
			
			await DbContext.DistributorEmployees.AddAsync(employee);
			await DbContext.SaveChangesAsync();

			return employee;
		}

        private async Task<UserIdentity> SeedUser(DistributorEmployee employee)
        {
            var user = Fixture.Build<UserIdentity>()
                .With(u => u.Employment, new UserIdentityEmployment
                {
                    EmployeeId = employee.Id,
                    DistributorId = employee.DistributorId
                })
                .Create();

            var claims = new[]
            {
                new UserIdentityUserClaim
                {
                    UserId = user.Id,
                    ClaimType = IdentityClaimTypes.EmployeeId,
                    ClaimValue = employee.Id.ToString()
                },
                new UserIdentityUserClaim
                {
                    UserId = user.Id,
                    ClaimType = IdentityClaimTypes.EmployeeDistributorId,
                    ClaimValue = employee.DistributorId
                }
            };

            await DbContext.Users.AddAsync(user);

            await DbContext.UserClaims.AddRangeAsync(claims);

            await DbContext.SaveChangesAsync();

            return user;
        }

		private async Task<UserIdentity> SeedUser()
		{
			var user = Fixture.Create<UserIdentity>();

			var claims = Fixture.Build<UserIdentityUserClaim>().Create();

			await DbContext.Users.AddAsync(user);

			await DbContext.UserClaims.AddAsync(claims);

			await DbContext.SaveChangesAsync();

			return user;
		}

		[Fact]
		public async Task Get_ById()
		{
			//Arrange
			var employee = await SeedDistributorEmployee();
			var user = await SeedUser(employee);

			var query = new DistributorEmployeeIdentityByIdQuery
			{
				Id = user.Id,
				DistributorId = employee.DistributorId
			};
			//Act
			var response = await Client.GetAsync($"{BaseUrl}/{query.DistributorId}/users/{query.Id}");
			
			//Assert
			response.EnsureSuccessStatusCode();
		}

		[Fact]
		public async Task Get_PagedList()
		{
			//Arrange
			var employee = await SeedDistributorEmployee();
			var user = await SeedUser();

			var query = new DistributorEmployeeIdentityQuery
			{
				DistributorId = employee.DistributorId,
				UserName = user.UserName,
				DisplayName = user.DisplayName,
				PageSize = PageSize,
				PageNumber = PageNumber
			};

			var response = await Client.GetAsync($"{BaseUrl}/{query.DistributorId}/users/?PageNumber={query.PageNumber}&PageSize={query.PageSize}");
			response.EnsureSuccessStatusCode();
		}

		[Fact]
		public async Task Get_ById_WhenNotFound_ReturnsNotFound()
		{
			var user = await SeedUser();

			var query = new DistributorEmployeeIdentityByIdQuery
			{
				Id = user.Id
			};
			//Act
			var response = await Client.GetAsync($"{BaseUrl}/{query.DistributorId}/users/{query.Id}");

			//Assert
			Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
		}

		[Fact]
		public async Task Post_GivenCommand_EmployeeNotFound_ReturnsBadRequest()
		{
			//Arrange
			var command = Fixture.Build<AddDistributorEmployeeIdentityCommand>()
                .With(c => c.Password, "P@ssw0rd")
				.Create();

			//Act
			var response = await Client.PostCommand($"{BaseUrl}/{command.DistributorId}/users", command);

			//Assert
			Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
		}

		[Fact]
		public async Task Post_GivenCommand_IsInvalid_ReturnsBadRequest()
		{
			//Arrange
			var command = new AddDistributorEmployeeIdentityCommand();

			//Act
			var response = await Client.PostCommand($"{BaseUrl}/invalid/users", command);

			//Assert
			Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
		}

		[Fact]
		public async Task Post_GivenPassword_IsInvalid_ReturnsBadRequest()
		{
			//Arrange
			var command = Fixture.Build<AddDistributorEmployeeIdentityCommand>()
                .With(c => c.Password, "1111")
				.Create();
			//Act
			var response = await Client.PostCommand($"{BaseUrl}/{command.DistributorId}/users", command);

			//Assert
			Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
		}

		[Fact]
		public async Task Post_GivenCommand_IsNull_ReturnsBadRequest()
		{
			//Act
			var response = await Client.PostCommand($"{BaseUrl}/invalid/users", "invalid");

			//Assert
			Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
		}
	}
}