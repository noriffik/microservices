﻿using AutoFixture;
using NexCore.Identity.Api.FunctionalTests.Fixtures;
using NexCore.Identity.Application.UserIdentities.Commands.AddUserIdentity;
using NexCore.Identity.Application.UserIdentities.Commands.UpdateUserIdentity;
using NexCore.Identity.Common.Entities;
using NexCore.Testing.Extensions;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.Identity.Api.FunctionalTests.Api.Controllers
{
    public class IdentitiesControllerTest : ApiControllerTestFixture
	{
		private const string BaseUrl = "api/users";
        
        private const int PageNumber = 1;
        private const int PageSize = 3;

		public IdentitiesControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
		{
		}

        [Fact]
		public async Task Get()
		{
			var response = await Client.GetAsync($"{BaseUrl}/?PageNumber={PageNumber}&PageSize={PageSize}");

			response.EnsureSuccessStatusCode();
		}

		[Fact]
		public async Task Get_ById()
		{
			//Arrange
			var user = await SeedUser();

			//Act
			var response = await Client.GetAsync($"{BaseUrl}/{user.Id}");

			//Assert
			response.EnsureSuccessStatusCode();
		}

		private async Task<UserIdentity> SeedUser()
		{
            var user = new UserIdentity("Test");

			await DbContext.Users.AddAsync(user);
            await DbContext.SaveChangesAsync();

			return user;
		}

		[Fact]
		public async Task Get_ById_WhenNotFound_ReturnsNotFound()
		{
			//Act
			var response = await Client.GetAsync($"{BaseUrl}/896");

			//Assert
			Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
		}

		[Fact]
		public async Task Get_ByDisplayName()
		{
			//Arrange
			var user = await SeedUser();

			//Act
			var response = await Client.GetAsync($"{BaseUrl}/?DisplayName={user.DisplayName}");

			//Assert
			response.EnsureSuccessStatusCode();
		}
		

		[Fact]
		public async Task Post()
		{
			//Arrange
            var command = Fixture.Build<AddUserIdentityCommand>()
                .With(c => c.Email, "test@email.com")
                .With(c => c.PhoneNumber, "3801112345678")
                .Create();

			//Act
			var response = await Client.PostCommand($"{BaseUrl}", command);

			//Assert
			response.EnsureSuccessStatusCode();
		}

		[Fact]
		public async Task Post_GivenCommand_IsInvalid_ReturnsBadRequest()
		{
			//Arrange
			var command = new AddUserIdentityCommand();

			//Act
			var response = await Client.PostCommand($"{BaseUrl}", command);

			//Assert
			Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
		}

		[Fact]
		public async Task Post_GivenPassword_IsInvalid_ReturnsBadRequest()
		{
			//Arrange
			var command = Fixture.Build<AddUserIdentityCommand>()
				.With(c => c.Email, "test@email.com")
				.With(c=>c.Password,"1111")
				.With(c => c.PhoneNumber, "3801112345678")
				.Create();
			//Act
			var response = await Client.PostCommand($"{BaseUrl}", command);

			//Assert
			Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
		}

		[Fact]
		public async Task Post_GivenCommand_IsNull_ReturnsBadRequest()
		{
			//Act
			var response = await Client.PostCommand($"{BaseUrl}", "invalid");

			//Assert
			Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
		}
		
		[Fact]
		public async Task Get_ByName()
		{
			//Arrange
			var user = await SeedSecuredUser();

			//Act
			var response = await Client.GetAsync($"{BaseUrl}/?UserName={user.UserName}");

			//Assert
			response.EnsureSuccessStatusCode();
		}

		private async Task<UserIdentity> SeedSecuredUser()
		{
			var user = new UserIdentity("Test") {SecurityStamp = Guid.NewGuid().ToString("D")};

			await DbContext.Users.AddAsync(user);
			await DbContext.SaveChangesAsync();

			return user;
		}

		[Fact]
		public async Task Put()
		{
			//Arrange
			var user = await SeedSecuredUser();

			var command = Fixture.Build<UpdateUserIdentityCommand>()
				.With(c => c.Id, user.Id)
				.With(c => c.UserName, user.UserName)
				.With(c => c.LockoutEnabled, false)
				.With(c => c.Email, "test@email.com")
				.With(c => c.PhoneNumber, "3801112345678")
				.Create();

			//Act
			var response = await Client.PutCommand($"{BaseUrl}/{user.Id}", command);
			response.EnsureSuccessStatusCode();
			
		}

		[Fact]
		public async Task ResetFailedAccessCount()
		{
			//Arrange
			var user = await SeedSecuredUser();

			//Act
			var response = await Client.DeleteAsync($"{BaseUrl}/{user.Id}/failed-access-count");

			//Assert
			response.EnsureSuccessStatusCode();
		}

        [Fact]
        public async Task ResetFailedAccessCount_WhenUser_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.DeleteAsync($"{BaseUrl}/896/failed-access-count");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
		}
	}
}