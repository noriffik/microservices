﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Testing.Functional.Auth;

namespace NexCore.Identity.Api.FunctionalTests.Api.Controllers
{
    public class TestStartup : Startup
	{
		public TestStartup(IConfiguration configuration) : base(configuration)
		{
		}

		protected override void ConfigureAuthentication(IServiceCollection services)
		{
			services.AddAuthentication("Test")
				.AddTestAuthentication("Test");
		}
	}
}