﻿using FluentValidation.TestHelper;
using NexCore.LeadManagement.Application.Leads.Commands.Add;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Application.Leads.Commands.Add
{
    public class AddCommandValidatorTest
    {
        private readonly AddCommandValidator _validator = new AddCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenPersonId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.PersonId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenPersonId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.PersonId, value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenPrivateCustomerId_IsInValid(int? value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.PrivateCustomerId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        [InlineData(null)]
        public void ShouldNotHaveError_WhenPrivateCustomerId_IsValid(int? value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.PrivateCustomerId, value);
        }
    }
}
