﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.LeadManagement.Application.Leads.Commands.Add;
using NexCore.LeadManagement.Domain.Leads;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Application.Leads.Commands.Add
{
    public class AddCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IUnitOfWork> _work;

        //Entity
        private readonly Lead _lead;

        //Handler and command
        private readonly AddCommandHandler _handler;
        private readonly AddCommand _command;

        public AddCommandHandlerTest()
        {
            var fixture = new Fixture();

            //Dependencies
            _repository = new Mock<IEntityRepository>();
            _work = new Mock<IUnitOfWork>();
            var mapper = new Mock<IMapper>();

            //Entity
            _lead = fixture.Create<Lead>();

            //Command
            _command = fixture.Create<AddCommand>();

            //Setup
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            mapper.Setup(m => m.Map<AddCommand, Lead>(_command)).Returns(_lead);

            //Handler
            _handler = new AddCommandHandler(_work.Object, mapper.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Add(_lead))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_lead.Id, result);
            }
        }
    }
}
