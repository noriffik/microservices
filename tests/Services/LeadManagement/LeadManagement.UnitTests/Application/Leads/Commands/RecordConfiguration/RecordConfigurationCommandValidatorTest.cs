﻿using FluentValidation.TestHelper;
using NexCore.LeadManagement.Application.Leads.Commands.RecordConfiguration;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Application.Leads.Commands.RecordConfiguration
{
    public class RecordConfigurationCommandValidatorTest
    {
        private readonly RecordConfigurationCommandValidator _validator = new RecordConfigurationCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenLeadId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.LeadId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenLeadId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.LeadId, value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenConfigurationId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ConfigurationId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenConfigurationId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.ConfigurationId, value);
        }
    }
}
