﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.LeadManagement.Application.Leads.Commands.SetMainOffer;
using NexCore.LeadManagement.Domain.Leads;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Application.Leads.Commands.SetMainOffer
{
    public class SetMainOfferCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Lead _lead;
        private readonly int _offerId;

        //Handler and command
        private readonly SetMainOfferCommandHandler _handler;
        private readonly SetMainOfferCommand _command;

        public SetMainOfferCommandHandlerTest()
        {
            var fixture = new Fixture();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();

            //Entity
            _lead = fixture.Create<Lead>();
            _offerId = fixture.Create<int>();
            _lead.RecordOffer(_offerId);

            //Command
            _command = fixture.Build<SetMainOfferCommand>()
                .With(c => c.OfferId, _offerId)
                .Create();

            //Handler
            _handler = new SetMainOfferCommandHandler(_work.Object);

            //Setup dependencies
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(SetMainOfferCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Require<Lead>(_command.LeadId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_lead);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }

            //Assert
            Assert.Equal(_offerId, _lead.MainOfferId);
        }
    }
}
