﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.LeadManagement.Application.Leads.Commands.RecordModelInterest;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Application.Leads.Commands.RecordModelInterest
{
    public class RecordModelInterestCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Lead _lead;
        private readonly IEnumerable<ModelId> _modelIds;

        //Handler and command
        private readonly RecordModelInterestCommandHandler _handler;
        private readonly RecordModelInterestCommand _command;

        public RecordModelInterestCommandHandlerTest()
        {
            var fixture = new Fixture();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            var mapper = new Mock<IMapper>();

            //Setup dependencies
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entity
            _lead = fixture.Create<Lead>();
            _modelIds = fixture.CreateMany<ModelId>();

            //Command
            _command = fixture.Build<RecordModelInterestCommand>()
                .With(c => c.LeadId, _lead.Id)
                .Create();

            //Setup mapper
            mapper.Setup(m => m.Map<IEnumerable<ModelId>>(_command.ModelIds)).Returns(_modelIds);

            //Handler
            _handler = new RecordModelInterestCommandHandler(_work.Object, mapper.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(RecordModelInterestCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Require<Lead>(_lead.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_lead);
                _repository.Setup(r => r.HasManyRequired<Model, ModelId>(_modelIds, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_modelIds, _lead.ModelsInterestedIn);
            }
        }
    }
}
