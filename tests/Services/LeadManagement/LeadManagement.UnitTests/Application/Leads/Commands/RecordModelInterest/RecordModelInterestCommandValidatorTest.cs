﻿using FluentValidation.TestHelper;
using NexCore.LeadManagement.Application.Leads.Commands.RecordModelInterest;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Application.Leads.Commands.RecordModelInterest
{
    public class RecordModelInterestCommandValidatorTest
    {
        private readonly RecordModelInterestCommandValidator _validator = new RecordModelInterestCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenLeadId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.LeadId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenLeadId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.LeadId, value);
        }

        [Theory]
        [InlineData("q qw qwe qwer")]
        [InlineData("1 23 456")]
        [InlineData("q1@ qw3$%sad")]
        public void ShouldNotHaveError_WhenModelIds_IsNotNull(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.ModelIds, value);
        }

        [Fact]
        public void ShouldHaveError_WhenModelIds_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ModelIds, null as string);
        }
    }
}
