﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.LeadManagement.Application.Leads.Commands.RecordVehicleReservation;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.Testing;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Application.Leads.Commands.RecordVehicleReservation
{
    public class RecordVehicleReservationCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Lead _lead;

        //Handler and command
        private readonly RecordVehicleReservationCommandHandler _handler;
        private readonly RecordVehicleReservationCommand _command;

        public RecordVehicleReservationCommandHandlerTest()
        {
            var fixture = new Fixture();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();

            //Entity
            _lead = fixture.Create<Lead>();

            //Command
            _command = fixture.Build<RecordVehicleReservationCommand>()
                .With(c => c.LeadId, _lead.Id)
                .Create();
            
            //Handler
            _handler = new RecordVehicleReservationCommandHandler(_work.Object);

            //Setup dependencies
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(RecordVehicleReservationCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            var reservation = new VehicleReservation(_command.CommercialNumber, _command.Vin, _command.OnDate);

            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<Lead>(_command.LeadId, CancellationToken.None))
                    .InSequence()
                    .ReturnsAsync(_lead);

                _work.Setup(w => w.Commit(CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                
                //Act
                await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_lead.Reservation, reservation, PropertyComparer<VehicleReservation>.Instance);
            }
        }
    }
}
