﻿using FluentValidation.TestHelper;
using NexCore.LeadManagement.Application.Leads.Commands.RecordVehicleReservation;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Application.Leads.Commands.RecordVehicleReservation
{
    public class RecordVehicleReservationCommandValidatorTest
    {
        private readonly RecordVehicleReservationCommandValidator _validator = new RecordVehicleReservationCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenLeadId_IsInvalid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.LeadId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenLeadId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.LeadId, value);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void ShouldHaveError_WhenCommercialNumber_IsInvalid(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.CommercialNumber, value);
        }

        [Theory]
        [InlineData("1")]
        public void ShouldHaveError_WhenCommercialNumber_IsValid(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.CommercialNumber, value);
        }

        [Theory]
        [InlineData("")]
        [InlineData("1234567890123456")]
        [InlineData(null)]
        public void ShouldHaveError_WhenVin_IsInvalid(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Vin, value);
        }

        [Theory]
        [InlineData("12345678901234567")]
        [InlineData("ABCDEF01234567890")]
        public void ShouldHaveError_WhenVin_IsValid(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Vin, value);
        }
    }
}
