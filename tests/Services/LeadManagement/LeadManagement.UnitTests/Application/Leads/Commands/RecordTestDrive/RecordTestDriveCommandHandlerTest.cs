﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.LeadManagement.Application.Leads.Commands.RecordTestDrive;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Domain.Leads.Specifications;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Application.Leads.Commands.RecordTestDrive
{
    public class RecordTestDriveCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Lead _lead;

        //Handler and command
        private readonly RecordTestDriveCommandHandler _handler;
        private readonly RecordTestDriveCommand _command;

        public RecordTestDriveCommandHandlerTest()
        {
            var fixture = new Fixture();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();

            //Entity
            _lead = fixture.Create<Lead>();

            //Command
            _command = fixture.Create<RecordTestDriveCommand>();

            //Handler
            _handler = new RecordTestDriveCommandHandler(_work.Object);

            //Setup dependencies
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(RecordTestDriveCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Require<Lead>(_command.LeadId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_lead);
                _repository.Setup(r => r.Has(
                        It.Is<LeadIncludedTestDriveSpecification>(s => s.TestDriveId == _command.TestDriveId),
                        It.IsAny<CancellationToken>()))
                    .ReturnsAsync(false);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Contains(_command.TestDriveId, _lead.TestDrives.Select(c => c.Id));
            }
        }

        [Fact]
        public async Task Handle_WhenLead_WithGivenTestDrive_AlreadyExist_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Require<Lead>(_command.LeadId, It.IsAny<CancellationToken>()))
                .ReturnsAsync(_lead);
            _repository.Setup(r => r.Has(
                    It.Is<LeadIncludedTestDriveSpecification>(s => s.TestDriveId == _command.TestDriveId),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.ThrowsAsync<DuplicateTestDriveException>(() => _handler.Handle(_command, CancellationToken.None));

            //Assert
            Assert.Equal(_command.TestDriveId, e.TestDriveId);
        }
    }
}
