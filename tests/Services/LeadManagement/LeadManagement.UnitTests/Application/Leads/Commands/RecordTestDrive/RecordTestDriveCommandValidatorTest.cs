﻿using FluentValidation.TestHelper;
using NexCore.LeadManagement.Application.Leads.Commands.RecordTestDrive;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Application.Leads.Commands.RecordTestDrive
{
    public class RecordTestDriveCommandValidatorTest
    {
        private readonly RecordTestDriveCommandValidator _validator = new RecordTestDriveCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenLeadId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.LeadId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenLeadId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.LeadId, value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenTestDriveId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.TestDriveId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenTestDriveId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.TestDriveId, value);
        }
    }
}
