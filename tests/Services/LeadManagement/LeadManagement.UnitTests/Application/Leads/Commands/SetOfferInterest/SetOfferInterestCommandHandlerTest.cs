﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.LeadManagement.Application.Leads.Commands.SetOfferInterest;
using NexCore.LeadManagement.Domain.Leads;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Application.Leads.Commands.SetOfferInterest
{
    public class SetOfferInterestCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Lead _lead;
        private readonly int _offerId;

        //Handler and command
        private readonly SetOfferInterestCommandHandler _handler;
        private readonly SetOfferInterestCommand _command;

        public SetOfferInterestCommandHandlerTest()
        {
            var fixture = new Fixture();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();

            //Entity
            _lead = fixture.Create<Lead>();
            _offerId = fixture.Create<int>();
            _lead.RecordOffer(_offerId);

            //Command
            _command = fixture.Build<SetOfferInterestCommand>()
                .With(c => c.OfferId, _offerId)
                .With(c => c.Interest, 1)
                .Create();

            //Handler
            _handler = new SetOfferInterestCommandHandler(_work.Object);

            //Setup dependencies
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(SetOfferInterestCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Require<Lead>(_command.LeadId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_lead);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }

            //Assert
            Assert.Equal(1, _lead.FindOffer(_offerId).Interest);
        }
    }
}
