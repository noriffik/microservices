﻿using FluentValidation.TestHelper;
using NexCore.LeadManagement.Application.Leads.Commands.SetOfferInterest;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Application.Leads.Commands.SetOfferInterest
{
    public class SetOfferInterestCommandValidatorTest
    {
        private readonly SetOfferInterestCommandValidator _validator = new SetOfferInterestCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenLeadId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.LeadId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenLeadId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.LeadId, value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenOfferId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.OfferId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenOfferId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.OfferId, value);
        }


        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenInterest_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Interest, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenInterest_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Interest, value);
        }
    }
}
