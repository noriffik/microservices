﻿using FluentValidation.TestHelper;
using NexCore.LeadManagement.Application.Leads.Commands.SetConfigurationInterest;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Application.Leads.Commands.SetConfigurationInterest
{
    public class SetConfigurationInterestCommandValidatorTest
    {
        private readonly SetConfigurationInterestCommandValidator _validator = new SetConfigurationInterestCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenLeadId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.LeadId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenLeadId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.LeadId, value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenConfigurationId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ConfigurationId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenConfigurationId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.ConfigurationId, value);
        }


        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenInterest_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Interest, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenInterest_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Interest, value);
        }
    }
}
