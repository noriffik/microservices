﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.LeadManagement.Application.Leads.Commands.SetConfigurationInterest;
using NexCore.LeadManagement.Domain.Leads;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Application.Leads.Commands.SetConfigurationInterest
{
    public class SetConfigurationInterestCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Lead _lead;
        private readonly int _configurationId;

        //Handler and command
        private readonly SetConfigurationInterestCommandHandler _handler;
        private readonly SetConfigurationInterestCommand _command;

        public SetConfigurationInterestCommandHandlerTest()
        {
            var fixture = new Fixture();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();

            //Entity
            _lead = fixture.Create<Lead>();
            _configurationId = fixture.Create<int>();
            _lead.RecordConfiguration(_configurationId);

            //Command
            _command = fixture.Build<SetConfigurationInterestCommand>()
                .With(c => c.ConfigurationId, _configurationId)
                .With(c => c.Interest, 1)
                .Create();

            //Handler
            _handler = new SetConfigurationInterestCommandHandler(_work.Object);
            
            //Setup dependencies
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(SetConfigurationInterestCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Require<Lead>(_command.LeadId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_lead);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }

            //Assert
            Assert.Equal(1, _lead.FindConfiguration(_configurationId).Interest);
        }
    }
}
