﻿using NexCore.LeadManagement.Api.Controllers;
using NexCore.LeadManagement.Application.Leads.Commands.Add;
using NexCore.LeadManagement.Application.Leads.Commands.RecordConfiguration;
using NexCore.LeadManagement.Application.Leads.Commands.RecordModelInterest;
using NexCore.LeadManagement.Application.Leads.Commands.RecordOffer;
using NexCore.LeadManagement.Application.Leads.Commands.RecordTestDrive;
using NexCore.LeadManagement.Application.Leads.Commands.RecordVehicleReservation;
using NexCore.LeadManagement.Application.Leads.Commands.SetConfigurationInterest;
using NexCore.LeadManagement.Application.Leads.Commands.SetMainOffer;
using NexCore.LeadManagement.Application.Leads.Commands.SetOfferInterest;
using NexCore.LeadManagement.Application.Leads.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Api.Controllers
{
    public class LeadsControllerTest
    {
        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(LeadsController)).HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(LeadsController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<LeadDto>(typeof(LeadsController), typeof(ByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetMany_ByPersonId()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<LeadDto>>(typeof(LeadsController), typeof(ByPersonIdQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task RecordModelInterest()
        {
            //Arrange
            var action = Assert.Action(typeof(LeadsController), typeof(RecordModelInterestCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task RecordTestDrive()
        {
            //Arrange
            var action = Assert.Action(typeof(LeadsController), typeof(RecordTestDriveCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task RecordConfiguration()
        {
            //Arrange
            var action = Assert.Action(typeof(LeadsController), typeof(RecordConfigurationCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task SetConfigurationInterest()
        {
            //Arrange
            var action = Assert.Action(typeof(LeadsController), typeof(SetConfigurationInterestCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task RecordOffer()
        {
            //Arrange
            var action = Assert.Action(typeof(LeadsController), typeof(RecordOfferCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task SetOfferInterest()
        {
            //Arrange
            var action = Assert.Action(typeof(LeadsController), typeof(SetOfferInterestCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task SetMainOffer()
        {
            //Arrange
            var action = Assert.Action(typeof(LeadsController), typeof(SetMainOfferCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task RecordVehicleReservation()
        {
            //Arrange
            var action = Assert.Action(typeof(LeadsController), typeof(RecordVehicleReservationCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
