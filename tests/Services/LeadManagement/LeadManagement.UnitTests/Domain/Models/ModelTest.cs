﻿using AutoFixture;
using NexCore.LeadManagement.Domain.Models;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Domain.Models
{
    public class ModelTest
    {
        private readonly ModelId _modelId;
        private readonly string _name;

        private readonly Model _model;

        public ModelTest()
        {
            var fixture = new Fixture();

            _modelId = fixture.Create<ModelId>();
            _name = fixture.Create<string>();

            _model = new Model(_modelId, _name);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Equal(_modelId, _model.Id);
            Assert.Equal(_name, _model.Name);
        }

        [Fact]
        public void Name()
        {
            //Arrange
            const string name = "new name";

            //Act
            _model.Name = name;

            //Assert
            Assert.Equal(name, _model.Name);
        }
    }
}
