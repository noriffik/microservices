﻿using NexCore.LeadManagement.Domain.Leads;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Domain.Leads
{
    public class ConfigurationTest
    {
        private const int ConfigurationId = 10;

        private readonly Configuration _configuration = new Configuration(ConfigurationId);

        [Fact]
        public void Ctor()
        {
            Assert.Equal(ConfigurationId, _configuration.Id);
        }

        [Theory]
        [InlineData(20)]
        [InlineData(null)]
        public void Interest(int? interest)
        {
            //Act
            _configuration.Interest = interest;

            //Assert
            Assert.Equal(interest, _configuration.Interest);
        }
    }
}
