﻿using NexCore.LeadManagement.Domain.Leads;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Domain.Leads
{
    public class OfferTest
    {
        private const int OfferId = 10;

        private readonly Offer _offer = new Offer(OfferId);

        [Fact]
        public void Ctor()
        {
            Assert.Equal(OfferId, _offer.Id);
        }

        [Theory]
        [InlineData(20)]
        [InlineData(null)]
        public void Interest(int? interest)
        {
            //Act
            _offer.Interest = interest;

            //Assert
            Assert.Equal(interest, _offer.Interest);
        }
    }
}
