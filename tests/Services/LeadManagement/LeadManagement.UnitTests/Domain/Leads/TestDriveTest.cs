﻿using NexCore.LeadManagement.Domain.Leads;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Domain.Leads
{
    public class TestDriveTest
    {
        private const int TestDriveId = 10;

        private readonly TestDrive _testDrive = new TestDrive(TestDriveId);

        [Fact]
        public void Ctor()
        {
            Assert.Equal(TestDriveId, _testDrive.Id);
        }
    }
}
