﻿using AutoFixture;
using NexCore.Common;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Domain.Models;
using System;
using System.Linq;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Domain.Leads
{
    public class LeadTest
    {
        private readonly IFixture _fixture;

        private readonly int _personId;
        private readonly int _privateCustomerId;

        private readonly Lead _lead;

        public LeadTest()
        {
            _fixture = new Fixture();

            _personId = _fixture.Create<int>();
            _privateCustomerId = _fixture.Create<int>();

            _lead = _fixture.Create<Lead>();
        }

        [Fact]
        public void Ctor()
        {
            using (var time = new SystemTimeContext(new DateTime(1991, 11, 25)))
            {
                //Act
                var lead = new Lead(_personId, _privateCustomerId);

                //Assert
                Assert.Equal(_personId, lead.PersonId);
                Assert.Equal(_privateCustomerId, lead.PrivateCustomerId);
                Assert.Equal(LeadStatus.Open, lead.Status);
                Assert.Equal(time.Now, lead.CreatedAt);
                Assert.Equal(time.Now, lead.UpdatedAt);
                Assert.NotNull(lead.ModelsInterestedIn);
                Assert.Empty(lead.ModelsInterestedIn);
            }
        }

        [Fact]
        public void FindConfiguration()
        {
            //Arrange
            foreach (var _ in Enumerable.Range(0, 2))
                _lead.RecordConfiguration(_fixture.Create<int>());
            var configurationId = _fixture.Create<int>();
            _lead.RecordConfiguration(configurationId);

            //Act
            var result = _lead.FindConfiguration(configurationId);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(configurationId, result.Id);
        }

        [Fact]
        public void FindConfiguration_WhenNotFound_ReturnsNull()
        {
            //Act
            var result = _lead.FindConfiguration(_fixture.Create<int>());

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public void FindOffer()
        {
            //Arrange
            foreach (var _ in Enumerable.Range(0, 2))
                _lead.RecordOffer(_fixture.Create<int>());
            var offerId = _fixture.Create<int>();
            _lead.RecordOffer(offerId);

            //Act
            var result = _lead.FindOffer(offerId);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(offerId, result.Id);
        }

        [Fact]
        public void FindOffer_WhenNotFound_ReturnsNull()
        {
            //Act
            var result = _lead.FindOffer(_fixture.Create<int>());

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public void RecordModelInterest()
        {
            using (var time = new SystemTimeContext(new DateTime(1991, 11, 25)))
            {
                //Arrange
                var modelIds = _fixture.CreateMany<ModelId>().ToList();

                //Act
                _lead.RecordModelInterest(modelIds);

                //Assert
                Assert.Equal(modelIds, _lead.ModelsInterestedIn);
                Assert.Equal(time.Now, _lead.UpdatedAt);
            }
        }

        [Fact]
        public void RecordTestDrive()
        {
            using (var time = new SystemTimeContext(new DateTime(1991, 11, 25)))
            {
                //Arrange
                var testDriveId = _fixture.Create<int>();

                //Act
                _lead.RecordTestDrive(testDriveId);

                //Assert
                Assert.Contains(testDriveId, _lead.TestDrives.Select(c => c.Id));
                Assert.Equal(time.Now, _lead.UpdatedAt);
            }
        }

        [Fact]
        public void RecordTestDrive_WhenTestDrive_AlreadyIncluded_Throws()
        {
            //Arrange
            var testDriveId = _fixture.Create<int>();
            _lead.RecordTestDrive(testDriveId);

            //Act
            var e = Assert.Throws<DuplicateTestDriveException>(() => _lead.RecordTestDrive(testDriveId));

            //Assert
            Assert.Equal(testDriveId, e.TestDriveId);
        }

        [Fact]
        public void RecordConfiguration()
        {
            using (var time = new SystemTimeContext(new DateTime(1991, 11, 25)))
            {
                //Arrange
                var configurationId = _fixture.Create<int>();

                //Act
                _lead.RecordConfiguration(configurationId);

                //Assert
                Assert.Contains(configurationId, _lead.Configurations.Select(c => c.Id));
                Assert.Equal(time.Now, _lead.UpdatedAt);
            }
        }

        [Fact]
        public void RecordConfiguration_WhenConfiguration_AlreadyIncluded_Throws()
        {
            //Arrange
            var configurationId = _fixture.Create<int>();
            _lead.RecordConfiguration(configurationId);

            //Act
            var e = Assert.Throws<DuplicateConfigurationException>(() => _lead.RecordConfiguration(configurationId));

            //Assert
            Assert.Equal(configurationId, e.ConfigurationId);
        }

        [Fact]
        public void SetConfigurationInterest()
        {
            //Arrange
            var configurationId = _fixture.Create<int>();
            _lead.RecordConfiguration(configurationId);

            //Act
            _lead.SetConfigurationInterest(configurationId, 1);

            //Assert
            Assert.Equal(1, _lead.FindConfiguration(configurationId).Interest);
        }

        [Fact]
        public void RecordOffer()
        {
            using (var time = new SystemTimeContext(new DateTime(1991, 11, 25)))
            {
                //Arrange
                var offerId = _fixture.Create<int>();

                //Act
                _lead.RecordOffer(offerId);

                //Assert
                Assert.Contains(offerId, _lead.Offers.Select(c => c.Id));
                Assert.Equal(time.Now, _lead.UpdatedAt);
            }
        }

        [Fact]
        public void RecordOffer_WhenOffer_AlreadyIncluded_Throws()
        {
            //Arrange
            var offerId = _fixture.Create<int>();
            _lead.RecordOffer(offerId);

            //Act
            var e = Assert.Throws<DuplicateOfferException>(() => _lead.RecordOffer(offerId));

            //Assert
            Assert.Equal(offerId, e.OfferId);
        }

        [Fact]
        public void SetOfferInterest()
        {
            //Arrange
            var offerId = _fixture.Create<int>();
            _lead.RecordOffer(offerId);

            //Act
            _lead.SetOfferInterest(offerId, 1);

            //Assert
            Assert.Equal(1, _lead.FindOffer(offerId).Interest);
        }

        [Fact]
        public void SetMainOffer()
        {
            //Arrange
            var offerId = _fixture.Create<int>();
            _lead.RecordOffer(offerId);

            //Act
            _lead.SetMainOffer(offerId);

            //Assert
            Assert.Equal(offerId, _lead.MainOfferId);
        }

        [Fact]
        public void SetMainOffer_WhenGivenOfferId_IsNull()
        {
            //Arrange
            _lead.RecordOffer(_fixture.Create<int>());

            //Act
            _lead.SetMainOffer(null);

            //Assert
            Assert.Null(_lead.MainOfferId);
        }

        [Fact]
        public void SetMainOffer_WhenOffer_IsNotFound_Throws()
        {
            //Arrange
            var offerId = _fixture.Create<int>();

            //Act
            var e = Assert.Throws<OfferNotFoundException>(() => _lead.SetMainOffer(offerId));

            //Assert
            Assert.Equal(offerId, e.OfferId);
        }

        [Fact]
        public void MainOffer()
        {
            //Arrange
            foreach (var _ in Enumerable.Range(0, 2))
                _lead.RecordOffer(_fixture.Create<int>());
            var mainOfferId = _fixture.Create<int>();
            _lead.RecordOffer(mainOfferId);
            _lead.SetMainOffer(mainOfferId);

            //Act
            var result = _lead.MainOffer;

            //Assert
            Assert.NotNull(result);
            Assert.Equal(mainOfferId, result.Id);
        }

        [Fact]
        public void MainOffer_WhenMainOffer_IsNotSet_ReturnsNull()
        {
            //Arrange
            foreach (var _ in Enumerable.Range(0, 2))
                _lead.RecordOffer(_fixture.Create<int>());

            //Act
            var result = _lead.MainOffer;

            //Assert
            Assert.Null(result);
        }
    }
}
