﻿using NexCore.LeadManagement.Domain.Leads.InterestIn;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Domain.Leads
{
    //TODO : Refactor these tests
    public class InterestInListTest
    {
        private readonly TestInterestIn _interestIn;
        private readonly InterestInList<TestInterestIn, char> _list;

        public InterestInListTest()
        {
            _interestIn = new TestInterestIn('C');

            _list = new InterestInList<TestInterestIn, char> {
                new TestInterestIn('A'),new TestInterestIn('B'),
                _interestIn, new TestInterestIn('D'),
                new TestInterestIn('E'),new TestInterestIn('F'),
                new TestInterestIn('G'),new TestInterestIn('H') };
        }

        [Fact]
        public void SetInterest_SetInterest_WhenOtherInterests_AreNotSet()
        {
            //Act
            _list.SetInterest(_interestIn.Id, 1);

            //Assert
            Assert.Equal(1, _interestIn.Interest);
        }

        [Fact]
        public void Find()
        {
            //Act
            var result = _list.Find(_interestIn.Id);

            //Assert
            Assert.Equal(_interestIn, result);
        }

        [Fact]
        public void SetInterest_WhenConfiguration_HasNotValue()
        {
            //Arrange
            _list.Find('B').Interest = 1;
            _list.Find('D').Interest = 2;
            _list.Find('H').Interest = 3;
            _list.Find('F').Interest = 4;
            _list.Find('E').Interest = 5;

            //Act
            _list.SetInterest(_interestIn.Id, 2);

            //Arrange
            Assert.Equal(1, _list.Find('B').Interest);
            Assert.Equal(2, _interestIn.Interest);
            Assert.Equal(3, _list.Find('D').Interest);
            Assert.Equal(4, _list.Find('H').Interest);
            Assert.Equal(5, _list.Find('F').Interest);
            Assert.Equal(6, _list.Find('E').Interest);
        }

        [Fact]
        public void SetInterest_WhenConfiguration_HasOldValue_GreaterThanNew()
        {
            //Arrange
            _list.Find('B').Interest = 1;
            _list.Find('D').Interest = 2;
            _list.Find('H').Interest = 3;
            _interestIn.Interest = 4;
            _list.Find('F').Interest = 5;
            _list.Find('E').Interest = 6;

            //Act
            _list.SetInterest(_interestIn.Id, 2);

            //Arrange
            Assert.Equal(1, _list.Find('B').Interest);
            Assert.Equal(2, _interestIn.Interest);
            Assert.Equal(3, _list.Find('D').Interest);
            Assert.Equal(4, _list.Find('H').Interest);
            Assert.Equal(5, _list.Find('F').Interest);
            Assert.Equal(6, _list.Find('E').Interest);
        }

        [Fact]
        public void SetInterest_WhenConfiguration_HasOldValue_LessThanNew()
        {
            //Arrange
            _list.Find('B').Interest = 1;
            _interestIn.Interest = 2;
            _list.Find('D').Interest = 3;
            _list.Find('H').Interest = 4;
            _list.Find('F').Interest = 5;
            _list.Find('E').Interest = 6;

            //Act
            _list.SetInterest(_interestIn.Id, 4);

            //Arrange
            Assert.Equal(1, _list.Find('B').Interest);
            Assert.Equal(2, _list.Find('D').Interest);
            Assert.Equal(3, _list.Find('H').Interest);
            Assert.Equal(4, _interestIn.Interest);
            Assert.Equal(5, _list.Find('F').Interest);
            Assert.Equal(6, _list.Find('E').Interest);
        }

        [Fact]
        public void SetInterest_WhenInterestIn_NotFound_Throws()
        {
            //Arrange
            const char interestInId = 'X';

            //Act
            var e = Assert.Throws<InterestInNotFoundException>(() => _list.SetInterest(interestInId, 1));

            //Assert
            Assert.Equal(interestInId, e.Id);
            Assert.Equal(typeof(TestInterestIn), e.InterestInType);
        }

        [Fact]
        public void SetInterest_AsLast_WhenOldValue_IsNotSet()
        {
            //Arrange
            _list.Find('B').Interest = 1;
            _list.Find('A').Interest = 2;
            _list.Find('D').Interest = 3;

            //Act
            _list.SetInterest(_interestIn.Id, 4);

            //Assert
            Assert.Equal(1, _list.Find('B').Interest);
            Assert.Equal(2, _list.Find('A').Interest);
            Assert.Equal(3, _list.Find('D').Interest);
            Assert.Equal(4, _interestIn.Interest);
        }

        [Fact]
        public void SetInterest_AsLast_WhenOldValue_IsSet_Throws()
        {
            //Arrange
            _list.Find('B').Interest = 1;
            _interestIn.Interest = 2;
            _list.Find('D').Interest = 3;

            //Act
            var e = Assert.Throws<InterestOutOfCountException>(() => _list.SetInterest(_interestIn.Id, 4));

            //Assert
            Assert.Equal(4, e.Interest);
            Assert.Equal(3, e.MaxValue);
            Assert.Equal(typeof(TestInterestIn), e.InterestInType);
        }

        [Fact]
        public void SetInterest_WhenInterest_IsGreater_ThenListCount_Throws()
        {
            //Arrange
            _list.Find('B').Interest = 1;
            _list.Find('A').Interest = 2;
            _list.Find('D').Interest = 3;
            const int interest = 6;


            //Act
            var e = Assert.Throws<InterestOutOfCountException>(() => _list.SetInterest(_interestIn.Id, interest));

            //Assert
            Assert.Equal(interest, e.Interest);
            Assert.Equal(4, e.MaxValue);
            Assert.Equal(typeof(TestInterestIn), e.InterestInType);
        }

        private class TestInterestIn : IInterestIn<char>
        {
            public char Id { get; private set; }

            public int? Interest { get; set; }

            public TestInterestIn(char id)
            {
                Id = id;
            }
        }
    }
}
