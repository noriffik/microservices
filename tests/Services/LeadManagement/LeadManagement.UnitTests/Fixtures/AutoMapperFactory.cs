﻿using AutoMapper;
using NexCore.LeadManagement.Application.Leads.Commands.Add;
using NexCore.LeadManagement.Infrastructure;

namespace NexCore.LeadManagement.UnitTests.Fixtures
{
    public static class AutoMapperFactory
    {
        public static IMapper Create()
        {
            var application = typeof(AddCommand).Assembly;
            var infrastructure = typeof(LeadManagementContext).Assembly;

            var mapperConfig = new MapperConfiguration(
                c => c.AddProfiles(application, infrastructure));

            return mapperConfig.CreateMapper();
        }
    }
}
