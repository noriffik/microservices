﻿using AutoMapper;
using MediatR;
using NexCore.LeadManagement.Infrastructure;
using System;
using System.Threading.Tasks;

namespace NexCore.LeadManagement.UnitTests.Fixtures.Infrastructure.QueryHandlers
{
    public abstract class QueryFixture<TQuery, TResponse> where TQuery : IRequest<TResponse>
    {
        protected bool IsReady;
        protected readonly InMemoryDbContextFactory DbContextFactory = new InMemoryDbContextFactory(typeof(TQuery));

        public readonly IMapper Mapper = AutoMapperFactory.Create();
        
        public LeadManagementContext CreateContext()
        {
            return DbContextFactory.Create();
        }

        public virtual Task Setup(LeadManagementContext context)
        {
            IsReady = true;

            return Task.CompletedTask;
        }

        public abstract TQuery GetQuery();

        public abstract TQuery GetUnpreparedQuery();

        public abstract TResponse GetResponse();

        protected void ThrowSetupIsRequired()
        {
            if (!IsReady)
                throw new InvalidOperationException("Setup must be called first.");
        }
    }
}