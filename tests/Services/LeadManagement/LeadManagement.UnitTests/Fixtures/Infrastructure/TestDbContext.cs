﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.LeadManagement.Infrastructure;
using NexCore.Testing.Extensions;

namespace NexCore.LeadManagement.UnitTests.Fixtures.Infrastructure
{
    public partial class InMemoryDbContextFactory
    {
        private class TestDbContext : LeadManagementContext
        {
            public TestDbContext(DbContextOptions<LeadManagementContext> options, IMediator mediator) : base(
                options, mediator)
            {
            }

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                base.OnModelCreating(modelBuilder);

                modelBuilder.SetAutoIncrementStep(1000);
            }
        }
    }
}
