﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NexCore.LeadManagement.Infrastructure;
using System;

namespace NexCore.LeadManagement.UnitTests.Fixtures.Infrastructure
{
    public partial class InMemoryDbContextFactory
    {
        public readonly string DatabaseName;
        public readonly Mock<IMediator> Mediator = new Mock<IMediator>();

        public static InMemoryDbContextFactory Instance => new InMemoryDbContextFactory(Guid.NewGuid().ToString());

        public InMemoryDbContextFactory(Type typeUnderTest) : this(typeUnderTest.FullName)
        {
        }

        public InMemoryDbContextFactory(string databaseName)
        {
            DatabaseName = databaseName;
        }

        public LeadManagementContext Create()
        {
            var services = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase();

            var mediator = new Mock<IMediator>().Object;

            return new TestDbContext(
                new DbContextOptionsBuilder<LeadManagementContext>()
                    .UseInMemoryDatabase(DatabaseName)
                    .UseInternalServiceProvider(services.BuildServiceProvider())
                    .Options,
                mediator);
        }
    }
}