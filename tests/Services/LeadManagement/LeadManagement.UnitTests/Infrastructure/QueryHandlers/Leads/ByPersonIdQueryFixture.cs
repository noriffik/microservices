﻿using NexCore.LeadManagement.Application.Leads.Queries;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Domain.Models;
using NexCore.LeadManagement.Infrastructure;
using NexCore.LeadManagement.UnitTests.Fixtures.Infrastructure.QueryHandlers;
using NexCore.Testing.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.LeadManagement.UnitTests.Infrastructure.QueryHandlers.Leads
{
    public class ByPersonIdQueryFixture : QueryFixture<ByPersonIdQuery, IEnumerable<LeadDto>>
    {
        private IEnumerable<Lead> _leads;
        private int _personId;
        private List<Model> _models;
        
        public override async Task Setup(LeadManagementContext context)
        {
            await base.Setup(context);

            var seeder = new EntitySeeder<Lead>(context);

            //Seed not expected Leads
            await seeder.AddRange(5);

            //Seed expected Lead
            _personId = seeder.Create<int>();
            _leads = await seeder.AddRange(6, new { personId = _personId });

            //Seed not expected Models
            await seeder.AddRange<Model, ModelId>(3);

            //Seed expected Models and record to expected leads
            _models = new List<Model>();
            foreach (var lead in _leads.Take(3))
            {
                var models = await seeder.AddRange<Model, ModelId>(2);
                lead.RecordModelInterest(models.Select(m => m.Id));
                _models.AddRange(models);
            }
            _leads.ElementAt(3).RecordModelInterest(_models.Select(m => m.Id));

            //Record configurations to expected leads
            foreach (var lead in _leads)
                foreach (var _ in Enumerable.Range(0, 2))
                    lead.RecordConfiguration(seeder.Create<int>());

            //Record testDrives to expected leads
            foreach (var lead in _leads)
                foreach (var _ in Enumerable.Range(0, 2))
                    lead.RecordTestDrive(seeder.Create<int>());

            //Record offers to expected leads
            foreach (var lead in _leads)
                foreach (var _ in Enumerable.Range(0, 3))
                    lead.RecordOffer(seeder.Create<int>());

            //Set main offers for expected leads
            foreach (var lead in _leads)
                lead.SetMainOffer(lead.Offers.Last().Id);

            await seeder.SeedAsync();
        }

        public override ByPersonIdQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new ByPersonIdQuery
            {
                PersonId = _personId
            };
        }

        public override ByPersonIdQuery GetUnpreparedQuery()
        {
            return new ByPersonIdQuery { PersonId = 100 };
        }

        public override IEnumerable<LeadDto> GetResponse()
        {
            foreach (var lead in _leads)
            {
                var models = _models.Where(m => lead.ModelsInterestedIn.Contains(m.Id)).ToList();

                yield return new LeadDto
                {
                    Id = lead.Id,
                    PersonId = lead.PersonId,
                    PrivateCustomerId = lead.PrivateCustomerId,
                    CreatedAt = lead.CreatedAt,
                    UpdatedAt = lead.UpdatedAt,
                    ModelsInterestedInCount = models.Count,
                    ConfigurationsCount = lead.Configurations.Count(),
                    TestDrivesCount = lead.TestDrives.Count(),
                    OffersCount = lead.Offers.Count(),
                    MainOffer = new OfferDto
                    {
                        Id = lead.MainOffer.Id,
                        Interest = lead.MainOffer.Interest,
                    },
                    ModelsInterestedIn = models.Select(m => new ModelInterestedInDto
                    {
                        ModelId = m.Id,
                        Name = m.Name
                    }),
                    Configurations = lead.Configurations.Select(c => new ConfigurationDto
                    {
                        Id = c.Id,
                        Interest = c.Interest
                    }),
                    TestDrives = lead.TestDrives.Select(c => new TestDriveDto
                    {
                        Id = c.Id
                    }),
                    Offers = lead.Offers.Select(o => new OfferDto
                    {
                        Id = o.Id,
                        Interest = o.Interest
                    })
                };
            }
        }
    }
}
