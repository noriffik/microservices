﻿using NexCore.LeadManagement.Application.Leads.Queries;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Domain.Models;
using NexCore.LeadManagement.Infrastructure;
using NexCore.LeadManagement.UnitTests.Fixtures.Infrastructure.QueryHandlers;
using NexCore.Testing.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.LeadManagement.UnitTests.Infrastructure.QueryHandlers.Leads
{
    public class ByIdQueryFixture : QueryFixture<ByIdQuery, LeadDto>
    {
        private Lead _lead;
        private IEnumerable<Model> _models;
        
        public override async Task Setup(LeadManagementContext context)
        {
            await base.Setup(context);

            var seeder = new EntitySeeder<Lead>(context);

            //Seed not expected Leads
            await seeder.AddRange(5);

            //Seed expected Lead
            _lead = await seeder.Add();

            //Seed not expected Models
            await seeder.AddRange<Model, ModelId>(3);

            //Seed expected Models
            _models = await seeder.AddRange<Model, ModelId>(3);

            //Record model interest to expected lead
            _lead.RecordModelInterest(_models.Select(m => m.Id));

            //Record configurations to expected lead
            foreach (var _ in Enumerable.Range(0, 2))
                _lead.RecordConfiguration(seeder.Create<int>());

            //Record testDrives to expected lead
            foreach (var _ in Enumerable.Range(0, 2))
                _lead.RecordTestDrive(seeder.Create<int>());

            //Record offers to expected lead
            foreach (var _ in Enumerable.Range(0, 3))
                _lead.RecordOffer(seeder.Create<int>());

            //Set main offer for expected lead
            _lead.SetMainOffer(_lead.Offers.Last().Id);

            await seeder.SeedAsync();
        }

        public override ByIdQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new ByIdQuery
            {
                Id = _lead.Id
            };
        }

        public override ByIdQuery GetUnpreparedQuery()
        {
            return new ByIdQuery { Id = 100 };
        }

        public override LeadDto GetResponse()
        {
            return new LeadDto
            {
                Id = _lead.Id,
                PersonId = _lead.PersonId,
                PrivateCustomerId = _lead.PrivateCustomerId,
                CreatedAt = _lead.CreatedAt,
                UpdatedAt = _lead.UpdatedAt,
                ModelsInterestedInCount = _models.Count(),
                ConfigurationsCount = _lead.Configurations.Count(),
                TestDrivesCount = _lead.TestDrives.Count(),
                OffersCount = _lead.Offers.Count(),
                MainOffer = new OfferDto
                {
                    Id = _lead.MainOffer.Id,
                    Interest = _lead.MainOffer.Interest,
                },
                ModelsInterestedIn = _models.Select(m => new ModelInterestedInDto
                {
                    ModelId = m.Id,
                    Name = m.Name
                }),
                Configurations = _lead.Configurations.Select(c => new ConfigurationDto
                {
                    Id = c.Id,
                    Interest = c.Interest
                }),
                TestDrives = _lead.TestDrives.Select(c => new TestDriveDto
                {
                    Id = c.Id
                }),
                Offers = _lead.Offers.Select(c => new OfferDto
                {
                    Id = c.Id,
                    Interest = c.Interest
                })
            };
        }
    }
}
