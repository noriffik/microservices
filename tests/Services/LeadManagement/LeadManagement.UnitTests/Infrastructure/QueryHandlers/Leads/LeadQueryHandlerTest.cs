﻿using AutoMapper;
using NexCore.LeadManagement.Application.Leads.Queries;
using NexCore.LeadManagement.Infrastructure.QueryHandlers.Leads;
using NexCore.LeadManagement.UnitTests.Fixtures;
using NexCore.LeadManagement.UnitTests.Fixtures.Infrastructure;
using NexCore.Testing;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.LeadManagement.UnitTests.Infrastructure.QueryHandlers.Leads
{
    public class LeadQueryHandlerTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();
        
        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new LeadQueryHandler(null, _mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = InMemoryDbContextFactory.Instance.Create())
                {
                    return new LeadQueryHandler(context, null);
                }
            });
        }

        //ByIdQuery

        [Fact]
        public async Task HandleById()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByIdQueryFixture();
                await fixture.Setup(context);

                var handler = new LeadQueryHandler(context, fixture.Mapper);

                //Act
                var actual = await handler.Handle(fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(fixture.GetResponse(), actual, PropertyComparer<LeadDto>.Instance);
            }
        }

        [Fact]
        public Task HandleById_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new LeadQueryHandler(context, _mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as ByIdQuery, CancellationToken.None));
            }
        }

        [Fact]
        public async Task HandleById_WhenNothing_IsFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByIdQueryFixture();

                var handler = new LeadQueryHandler(context, _mapper);

                var query = fixture.GetUnpreparedQuery();

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }

        //ByPersonIdQuery

        [Fact]
        public async Task HandleByPersonId()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByPersonIdQueryFixture();

                await fixture.Setup(context);

                var handler = new LeadQueryHandler(context, fixture.Mapper);

                //Act
                var actual = await handler.Handle(fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(fixture.GetResponse(), actual, PropertyComparer<LeadDto>.Instance);
            }
        }

        [Fact]
        public Task HandleByPersonId_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new LeadQueryHandler(context, _mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as ByIdQuery, CancellationToken.None));
            }
        }

        [Fact]
        public async Task HandleByPersonId_WhenNothing_IsFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByPersonIdQueryFixture();

                var handler = new LeadQueryHandler(context, _mapper);

                var query = fixture.GetUnpreparedQuery();

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(result);
            }
        }
    }
}
