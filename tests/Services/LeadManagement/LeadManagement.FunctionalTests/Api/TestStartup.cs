﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexCore.LeadManagement.Api;
using NexCore.Testing.Functional.Auth;

namespace NexCore.LeadManagement.FunctionalTests.Api
{
    public class TestStartup : Startup
    {
        public TestStartup(IConfiguration configuration) : base(configuration)
        {
        }

        protected override void ConfigureAuthentication(IServiceCollection services)
        {
            services.AddAuthentication("Test")
                .AddTestAuthentication("Test");
        }
    }
}
