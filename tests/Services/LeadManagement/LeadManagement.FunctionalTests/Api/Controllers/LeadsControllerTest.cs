﻿using AutoFixture;
using NexCore.LeadManagement.Application.Leads.Commands.Add;
using NexCore.LeadManagement.Application.Leads.Commands.RecordConfiguration;
using NexCore.LeadManagement.Application.Leads.Commands.RecordModelInterest;
using NexCore.LeadManagement.Application.Leads.Commands.RecordOffer;
using NexCore.LeadManagement.Application.Leads.Commands.RecordTestDrive;
using NexCore.LeadManagement.Application.Leads.Commands.RecordVehicleReservation;
using NexCore.LeadManagement.Application.Leads.Commands.SetConfigurationInterest;
using NexCore.LeadManagement.Application.Leads.Commands.SetMainOffer;
using NexCore.LeadManagement.Application.Leads.Commands.SetOfferInterest;
using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Domain.Models;
using NexCore.LeadManagement.FunctionalTests.Fixtures;
using NexCore.Testing.Extensions;
using NexCore.Testing.Seeders;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.LeadManagement.FunctionalTests.Api.Controllers
{
    public class LeadsControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/leads";

        public LeadsControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }
        
        [Fact]
        public async Task Get()
        {
            //Arrange
            var seeder = new EntitySeeder<Lead>(DbContext);
            var lead = await seeder.Add();

            await seeder.SeedAsync();
            
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{lead.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/896");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetMany_ByPersonId()
        {
            //Arrange
            var seeder = new EntitySeeder<Lead>(DbContext);
            var personId = Fixture.Create<int>();
            await seeder.AddRange(3, new {personId});

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many/person/{personId}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetMany_ByPersonId_WhenNotFound_ReturnsSuccess()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many/person/{Fixture.Create<int>()}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var command = Fixture.Create<AddCommand>();

            //Assert
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task RecordModelInterest()
        {
            //Arrange
            var seeder = new EntitySeeder<Lead>(DbContext);
            var lead = await seeder.Add();
            var models = await seeder.AddRange<Model, ModelId>(3);

            await seeder.SeedAsync();

            var command = seeder.Build<RecordModelInterestCommand>()
                .With(c => c.LeadId, lead.Id)
                .With(c => c.ModelIds, models.Select(m => m.Id.Value).Aggregate((a, b) => $"{a} {b}"))
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/record/model-interest", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task RecordModelInterest_WhenEntities_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<RecordModelInterestCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/record/model-interest", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task RecordTestDrive()
        {
            //Arrange
            var seeder = new EntitySeeder<Lead>(DbContext);
            var lead = await seeder.Add();
            await seeder.SeedAsync();

            var command = seeder.Build<RecordTestDriveCommand>()
                .With(c => c.LeadId, lead.Id)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/record/test-drive", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task RecordTestDrive_WhenEntities_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<RecordTestDriveCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/record/test-drive", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task RecordConfiguration()
        {
            //Arrange
            var seeder = new EntitySeeder<Lead>(DbContext);

            var lead = await seeder.Add();
            await seeder.SeedAsync();

            var command = seeder.Build<RecordConfigurationCommand>()
                .With(c => c.LeadId, lead.Id)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/record/configuration", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task RecordConfiguration_WhenEntities_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<RecordConfigurationCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/record/configuration", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task SetConfigurationInterest()
        {
            //Arrange
            var seeder = new EntitySeeder<Lead>(DbContext);

            var lead = await seeder.Add();
            var configurationId = seeder.Create<int>();
            lead.RecordConfiguration(configurationId);

            await seeder.SeedAsync();

            var command = seeder.Build<SetConfigurationInterestCommand>()
                .With(c => c.LeadId, lead.Id)
                .With(c => c.ConfigurationId, configurationId)
                .With(c => c.Interest, 1)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/set/configuration-interest", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task SetConfigurationInterest_WhenEntities_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<SetConfigurationInterestCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/record/configuration-interest", command);

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task RecordOffer()
        {
            //Arrange
            var seeder = new EntitySeeder<Lead>(DbContext);
            var lead = await seeder.Add();

            await seeder.SeedAsync();

            var command = seeder.Build<RecordOfferCommand>()
                .With(c => c.LeadId, lead.Id)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/record/offer", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task RecordOffer_WhenEntities_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<RecordOfferCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/record/offer", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task SetOfferInterest()
        {
            //Arrange
            var seeder = new EntitySeeder<Lead>(DbContext);
            var lead = await seeder.Add();
            var offerId = seeder.Create<int>();
            lead.RecordOffer(offerId);

            await seeder.SeedAsync();

            var command = seeder.Build<SetOfferInterestCommand>()
                .With(c => c.LeadId, lead.Id)
                .With(c => c.OfferId, offerId)
                .With(c => c.Interest, 1)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/set/offer-interest", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task SetOfferInterest_WhenEntities_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<SetOfferInterestCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/set/offer-interest", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task SetMainOffer()
        {
            //Arrange
            var seeder = new EntitySeeder<Lead>(DbContext);
            var lead = await seeder.Add();
            var offerId = seeder.Create<int>();
            lead.RecordOffer(offerId);

            await seeder.SeedAsync();

            var command = seeder.Build<SetMainOfferCommand>()
                .With(c => c.LeadId, lead.Id)
                .With(c => c.OfferId, offerId)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/set/main-offer", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task SetMainOffer_WhenEntities_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<SetMainOfferCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/set/main-offer", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task RecordVehicleReservation()
        {
            //Arrange
            var seeder = new EntitySeeder<Lead>(DbContext);
            var lead = await seeder.Add();

            await seeder.SeedAsync();

            var command = seeder.Build<RecordVehicleReservationCommand>()
                .With(c => c.LeadId, lead.Id)
                .With(c => c.Vin, "12345678901234567")
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/record/vehicle-reservation", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task RecordVehicleReservation_WhenEntities_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<RecordVehicleReservationCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/record/vehicle-reservation", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
