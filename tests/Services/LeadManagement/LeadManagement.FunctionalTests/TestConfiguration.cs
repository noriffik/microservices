﻿using Microsoft.Extensions.Configuration;

namespace NexCore.LeadManagement.FunctionalTests
{
    static class TestConfiguration
    {
        private static IConfigurationRoot _root;

        public static IConfigurationRoot Get => _root ?? (_root = new ConfigurationBuilder()
                                                    .AddJsonFile("appsettings.json", true)
                                                    .AddUserSecrets("ADB3F668-7789-4B42-B737-67FC643B20B7")
                                                    .Build());
    }
}
