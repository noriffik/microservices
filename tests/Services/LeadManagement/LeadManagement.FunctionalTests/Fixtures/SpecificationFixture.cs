﻿using Microsoft.EntityFrameworkCore;
using NexCore.LeadManagement.Infrastructure;
using System;

namespace NexCore.LeadManagement.FunctionalTests.Fixtures
{
    public abstract class SpecificationFixture : IDisposable
    {
        protected string DatabaseName { get; }

        protected readonly SqlServerDbContextFactory DbContextFactory;

        protected SpecificationFixture(string databaseName)
        {
            DatabaseName = databaseName;
            DbContextFactory = new SqlServerDbContextFactory();

            SetupDatabase();
        }

        public LeadManagementContext CreateContext()
        {
            return DbContextFactory.Create(DatabaseName);
        }

        protected void SetupDatabase()
        {
            using (var context = CreateContext())
            {
                context.Database.Migrate();

                SeedDatabase(context);
            }
        }

        protected virtual void SeedDatabase(LeadManagementContext context)
        {
        }

        public virtual void Dispose()
        {
            CreateContext().Database.EnsureDeleted();
        }
    }
}
