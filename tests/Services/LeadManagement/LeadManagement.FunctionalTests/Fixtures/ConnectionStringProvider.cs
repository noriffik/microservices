﻿namespace NexCore.LeadManagement.FunctionalTests.Fixtures
{
    public class ConnectionStringProvider
    {
        public static string Get(string name) => TestConfiguration.Get["ConnectionString"]
            .Replace("%DATABASE%", $"NexCore.Services.LeadManagement_{name}");
    }
}
