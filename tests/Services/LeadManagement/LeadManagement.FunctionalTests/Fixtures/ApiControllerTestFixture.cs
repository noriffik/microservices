﻿using AutoFixture;
using Microsoft.Extensions.DependencyInjection;
using NexCore.LeadManagement.FunctionalTests.Api;
using NexCore.LeadManagement.Infrastructure;
using System;
using System.Net.Http;
using Xunit;

namespace NexCore.LeadManagement.FunctionalTests.Fixtures
{
    public abstract class ApiControllerTestFixture : IClassFixture<ServiceFactory<TestStartup>>, IDisposable
    {
        protected readonly HttpClient Client;
        protected readonly LeadManagementContext DbContext;
        protected readonly IFixture Fixture;

        private readonly IServiceScope _scope;

        protected ApiControllerTestFixture(ServiceFactory<TestStartup> factory)
        {
            Client = factory.CreateClient();
            _scope = factory.CreateScope();
            DbContext = _scope.ServiceProvider.GetRequiredService<LeadManagementContext>();

            Fixture = new Fixture();
        }
        
        public void Dispose()
        {
            DbContext.Database.EnsureDeleted();
            
            _scope.Dispose();
        }
    }
}
