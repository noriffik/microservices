﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NexCore.LeadManagement.Infrastructure;
using System.Reflection;

namespace NexCore.LeadManagement.FunctionalTests.Fixtures
{
    public class SqlServerDbContextFactory
    {
        public LeadManagementContext Create(string databaseName)
        {
            var connectionString = ConnectionStringProvider.Get(databaseName);

            var migrationAssembly = typeof(LeadManagementContext).GetTypeInfo().Assembly.GetName().Name;

            var services = new ServiceCollection()
                .AddEntityFrameworkSqlServer();

            var options = new DbContextOptionsBuilder<LeadManagementContext>()
                .UseInternalServiceProvider(services.BuildServiceProvider())
                .UseSqlServer(
                    connectionString,
                    sqlOptions => {sqlOptions
                        .MigrationsAssembly(migrationAssembly)
                        .EnableRetryOnFailure(10);
                    })
                .Options;

            var mediator = new Mock<IMediator>();

            return new LeadManagementContext(options, mediator.Object);
        }
    }
}
