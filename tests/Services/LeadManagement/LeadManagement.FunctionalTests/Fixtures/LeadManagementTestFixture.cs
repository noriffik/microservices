﻿using NexCore.LeadManagement.Infrastructure;
using NexCore.Testing.Functional;
using System;

namespace NexCore.LeadManagement.FunctionalTests.Fixtures
{
    public class LeadManagementTestFixture : DbContextTestFixture<LeadManagementContext>
    {
        public LeadManagementTestFixture() : base(GetConnectionString())
        {
        }

        private static string GetConnectionString()
        {
            return TestConfiguration.Get["ConnectionString"]
                .Replace("%DATABASE%", $"NexCore.Services.LeadManagement_{Guid.NewGuid()}");
        }

        public override LeadManagementContext CreateContext()
        {
            return new LeadManagementContext(Options, Mediator.Object);
        }
    }
}
