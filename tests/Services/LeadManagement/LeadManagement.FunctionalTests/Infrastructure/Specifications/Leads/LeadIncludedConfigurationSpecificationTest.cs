﻿using NexCore.LeadManagement.Domain.Leads;
using NexCore.LeadManagement.Domain.Leads.Specifications;
using NexCore.LeadManagement.FunctionalTests.Fixtures;
using NexCore.Testing.Seeders;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.LeadManagement.FunctionalTests.Infrastructure.Specifications.Leads
{
    public class LeadIncludedConfigurationSpecificationTest : SpecificationFixture
    {
        public LeadIncludedConfigurationSpecificationTest() : base(nameof(LeadIncludedConfigurationSpecification))
        {
        }

        [Fact]
        public async Task Find()
        {
            using (var context = CreateContext())
            {
                //Arrange
                var seeder = new EntitySeeder<Lead>(context);

                //Specification
                var configurationId = seeder.Create<int>();
                var specification = new LeadIncludedConfigurationSpecification(configurationId);

                //Seed unexpected Leads
                await seeder.AddRange(3);

                //Seed expected Leads
                var lead = await seeder.Add();
                lead.RecordConfiguration(configurationId);

                var expected = new[] {lead};

                await seeder.SeedAsync();
                
                //Act
                var results = await context.EntityRepository.Find(specification, CancellationToken.None);

                //Assert
                Assert.Equal(expected, results);
            }
        }
    }
}
