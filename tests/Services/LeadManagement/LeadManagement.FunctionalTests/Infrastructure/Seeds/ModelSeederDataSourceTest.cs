﻿using AutoFixture;
using AutoMapper;
using NexCore.LeadManagement.Domain.Models;
using NexCore.LeadManagement.Infrastructure.Seeds;
using Xunit;

namespace NexCore.LeadManagement.FunctionalTests.Infrastructure.Seeds
{
    public class ModelSeederDataSourceTest
    {
        private readonly IMapper _mapper;

        public ModelSeederDataSourceTest()
        {
            var mapperConfiguration = new MapperConfiguration(
                c => c.AddProfile(new AutoMapperProfile()));

            _mapper = mapperConfiguration.CreateMapper();
        }

        [Fact]
        public void Get()
        {
            //Arrange
            var fixture = new Fixture();

            var expected = fixture.Create<Model>();
            var text = $"Id;Name\n{expected.Id};{expected.Name}";
            var dataSource = new ModelSeederDataSource(_mapper, text);

            //Act
            var actual = dataSource.Get();

            //Assert
            Assert.Contains(expected, actual);
        }

        [Fact]
        public void Get_WhenUsingDefaultText()
        {
            //Arrange
            var dataSource = new ModelSeederDataSource(
                _mapper, SeederDataSourceFixture.SetupProvider("Models"));

            //Assert
            Assert.NotEmpty(dataSource.Get());
        }
    }
}
