﻿using AutoFixture;
using NexCore.TestDriveCalendar.Api.Models;
using System;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Models
{
    public class VehicleMapperTest
    {
        [Fact]
        public void Map()
        {
            //Arrange
            var vin = "12345678901234567";
            var query = new Fixture().Build<VehicleItem>().With(i => i.Id, 11).With(i => i.Vin, vin).Create();
            
            var mapper = new VehicleMapper();

            //Act
            var result = mapper.Map(query);

            //Assert
            Assert.Equal(query.Id, result.Id);
            Assert.Equal(query.Vin, result.Vin.Number);
            Assert.Equal(query.ModelId, result.ModelId);
            Assert.Equal(query.Equipment, result.Equipment);
            Assert.Equal(query.Type, result.Type);
            Assert.Equal(query.GearBox, result.GearBox);
            Assert.Equal(query.Options, result.Options);
            Assert.Equal(query.Comments, result.Comments);
        }

        [Fact]
        public void Map_GivenItem_IsNull_Throws()
        {
            //Arrange
            var mapper = new VehicleMapper();

            //Assert
            var e = Assert.Throws<ArgumentNullException>(() => { mapper.Map(null); });
            Assert.Equal("item", e.ParamName);
        }
    }
}
