﻿using Microsoft.EntityFrameworkCore;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Infrastructure;
using NexCore.TestDriveCalendar.Api.Infrastructure.Repositories;
using System;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Infrastructure.Repositories
{
    public class FleetRepositoryTest : IDisposable
    {
        private readonly TestDriveContext _context;
        private readonly FleetRepository _repository;

        public FleetRepositoryTest()
        {
            var options = new DbContextOptionsBuilder<TestDriveContext>()
                .UseInMemoryDatabase("FleetRepositoryTest")
                .Options;

            _context = new TestDriveContext(options);
            _repository = new FleetRepository(_context);
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new FleetRepository(null);
            });
            Assert.Equal("context", e.ParamName);
        }

        [Fact]
        public void UnitOfWork_Returns_Context()
        {
            //Act
            var context = _repository.UnitOfWork;

            //Assert
            Assert.Same(_context, context);
        }

        [Fact]
        public void Add_GivenEntity_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                _repository.Add(null);
            });
            Assert.Equal("entity", e.ParamName);
        }

        [Fact]
        public void Add_AddsTo_EntityManager()
        {
            //Assert
            var fleet = new Fleet(DealerCode.Parse("38001"));

            //Act
            _repository.Add(fleet);

            //Assert
            Assert.Equal(EntityState.Added, _context.Entry(fleet).State);
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}
