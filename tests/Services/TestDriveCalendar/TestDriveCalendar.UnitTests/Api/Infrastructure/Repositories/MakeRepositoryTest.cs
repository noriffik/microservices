﻿using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Infrastructure;
using NexCore.TestDriveCalendar.Api.Infrastructure.Repositories;
using System;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Infrastructure.Repositories
{
    public class MakeRepositoryTest : IDisposable
    {
        private readonly TestDriveContext _context;
        private readonly MakeRepository _makeRepository;

        public MakeRepositoryTest()
        {
            var options = new DbContextOptionsBuilder<TestDriveContext>()
                .UseInMemoryDatabase("MakeRepositoryTest")
                .Options;

            _context = new TestDriveContext(options);
            _makeRepository = new MakeRepository(_context);
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() => 
            {
                var unused = new MakeRepository(null);
            });
            Assert.Equal("context", e.ParamName);
        }
      
        [Fact]
        public void UnitOfWork_Returns_Context()
        {
            //Act
            var context = _makeRepository.UnitOfWork;

            //Assert
            Assert.Equal(_context, context);
            Assert.IsAssignableFrom<IUnitOfWork>(context);
        }

        public void Dispose()
        {

            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}
