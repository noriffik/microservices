﻿using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Domain.Notes;
using NexCore.TestDriveCalendar.Api.Infrastructure;
using NexCore.TestDriveCalendar.Api.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Infrastructure.Repositories
{
    public class NoteRepositoryTest : IDisposable
    {
        private readonly TestDriveContext _context;
        private readonly NoteRepository _notesRepository;
        private readonly Note _note;
        private readonly List<Note> _notes = new List<Note>();

        public NoteRepositoryTest()
        {
            var options = new DbContextOptionsBuilder<TestDriveContext>()
                .UseInMemoryDatabase("NoteDriveRepositoryTest")
                .Options;

            _context = new TestDriveContext(options);
            _notesRepository = new NoteRepository(_context);


            _notes.Add(new Note(1, 1, 1, "Content1"));
            _notes.Add(new Note(2, 1, 2, "Content2"));
            _notes.Add(new Note(3, 2, 3, "Content3"));
            _notes.Add(new Note(4, 2, 4, "Content4"));
            foreach (var note in _notes)
            {
                _notesRepository.Add(note);
            }

            _note = new Note(5, 3, 5, "Content5");
            _notesRepository.UnitOfWork.Commit(CancellationToken.None);
        }

        [Fact]
        public void Ctor_GivenContextEqualToNull_throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new TestDriveRepository(null);
            });
            Assert.Equal("context", e.ParamName);
        }

        [Fact]
        public void UnitOfWork_Returns_Context()
        {
            //Act
            var context = _notesRepository.UnitOfWork;

            //Assert
            Assert.Equal(_context, context);
            Assert.IsAssignableFrom<IUnitOfWork>(context);
        }

        [Fact]
        public void Add_AddsTo_EntityManager()
        {
            //Arrange
            var repository = new NoteRepository(_context);

            //Act
            repository.Add(_note);

            //Assert
            Assert.Equal(EntityState.Added, _context.Entry(_note).State);
        }

        [Fact]
        public async Task Find_Note()
        {
            //Act
            var note = await _notesRepository.Find(1);

            //Assert
            Assert.Equal(1, note.Id);
            Assert.Equal(1, note.TestDriveId);
            Assert.Equal(1, note.ManagerId);
            Assert.Equal("Content1", note.Content);
        }

        [Fact]
        public async Task FindNotesByTestDriveId()
        {
            //Arrange
            var expected = _notes.Where(n => n.TestDriveId == 2);

            //Act
            var actual = await _notesRepository.FindAllByTestDriveIdAsync(2);

            //Assert
            Assert.Equal(expected, actual);
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}
