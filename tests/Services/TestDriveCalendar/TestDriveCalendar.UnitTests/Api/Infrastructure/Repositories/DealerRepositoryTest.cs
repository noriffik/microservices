﻿using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Extensions;
using NexCore.TestDriveCalendar.Api.Infrastructure;
using NexCore.TestDriveCalendar.Api.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Infrastructure.Repositories
{
    public class DealerRepositoryTest : IDisposable
    {
        private readonly TestDriveContext _context;
        private readonly DealerRepository _dealerRepository;
        private readonly List<DailySchedule> _dailySchedules = new List<DailySchedule>();
        private readonly Time _since = new Time(36000);
        private readonly Time _till = new Time(72000);

        public DealerRepositoryTest()
        {
            var options = new DbContextOptionsBuilder<TestDriveContext>()
                .UseInMemoryDatabase("DealerRepositoryTest")
                .Options;

            _context = new TestDriveContext(options);
            _dealerRepository = new DealerRepository(_context);
        }

        private DayOfWeek IntToWeekDay(int i)
        {
            switch (i)
            {
                case 1:
                    return DayOfWeek.Monday;
                case 2:
                    return DayOfWeek.Tuesday;
                case 3:
                    return DayOfWeek.Wednesday;
                case 4:
                    return DayOfWeek.Thursday;
                case 5:
                    return DayOfWeek.Friday;
                case 6:
                    return DayOfWeek.Saturday;
                case 7:
                    return DayOfWeek.Sunday;
                default:
                    throw new InvalidOperationException();
            }
        }

        [Fact]
        public void Ctor_GivenContextEqualToNull_throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() => 
            {
                var unused = new DealerRepository(null);
            });
            Assert.Equal("context", e.ParamName);
        }

        [Fact]
        public void UnitOfWork_Returns_Context()
        {
            //Act
            var context = _dealerRepository.UnitOfWork;

            //Assert
            Assert.Equal(_context, context);
            Assert.IsAssignableFrom<IUnitOfWork>(context);
        }

        [Fact]
        public void Add_AddsTo_EntityManager()
        {
            //Arrange
            var dailySchedules = _dailySchedules;
            for (var i = 1; i <= 7; i++)
            {
                dailySchedules.Add(new DailySchedule(IntToWeekDay(i), _since, _till));
            }
            var weeklySchedule = new Schedule(dailySchedules);
            var dealer = new Dealer(DealerCode.Parse("38001"), weeklySchedule, 15);
            var repository = new DealerRepository(_context);

            //Act
            repository.Add(dealer);

            //Assert
            Assert.Equal(EntityState.Added, _context.Entry(dealer).State);
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}
