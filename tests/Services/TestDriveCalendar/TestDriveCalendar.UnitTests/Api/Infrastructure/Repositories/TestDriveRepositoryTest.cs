﻿using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Infrastructure;
using NexCore.TestDriveCalendar.Api.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Infrastructure.Repositories
{
    public class TestDriveRepositoryTest : IDisposable
    {
        private const int ClientId = 1;
        private const int VehicleId = 1;
        private const int SalesManagerId = 1;

        private readonly TestDriveContext _context;
        private readonly TestDriveRepository _testDriveRepository;
        private readonly DateTime _since = new DateTime(2018, 11, 19, 12, 0, 0);
        private readonly DateTime _till = new DateTime(2018, 11, 19, 14, 0, 0);
        private readonly DbContextOptions<TestDriveContext> _options;
        private readonly TestDrive _testDrive;

        public TestDriveRepositoryTest()
        {
            _options = new DbContextOptionsBuilder<TestDriveContext>()
                .UseInMemoryDatabase("TestDriveRepositoryTest")
                .Options;

            _context = new TestDriveContext(_options);
            _testDriveRepository = new TestDriveRepository(_context);

            _testDrive = new TestDrive(ClientId, VehicleId, _since, _till, SalesManagerId);
        }

        [Fact]
        public void Ctor_GivenContextEqualToNull_throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() => 
            {
                var unused = new TestDriveRepository(null);
            });
            Assert.Equal("context", e.ParamName);
        }

        [Fact]
        public void UnitOfWork_Returns_Context()
        {
            //Act
            var context = _testDriveRepository.UnitOfWork;

            //Assert
            Assert.Equal(_context, context);
            Assert.IsAssignableFrom<IUnitOfWork>(context);
        }

        [Fact]
        public void Add_AddsTo_EntityManager()
        {
            //Arrange
            var repository = new TestDriveRepository(_context);

            //Act
            repository.Add(_testDrive);

            //Assert
            Assert.Equal(EntityState.Added, _context.Entry(_testDrive).State);
        }

        [Theory]
        [MemberData(nameof(Acceptable))]
        public async Task CanReschedule(DateTime since, DateTime till)
        {
            using (var context = new TestDriveContext(_options))
            {
                //Arrange
                context.AddRange(_testDrive);
                await context.SaveChangesAsync();
            }

            using (var context = new TestDriveContext(_options))
            {
                var repository = new TestDriveRepository(context);

                //Act 
                var result = await repository.CanRescheduleAsync(_testDrive, since,
                    till);

                //Assert
                Assert.True(result);
            }
        }

        public static IEnumerable<object[]> Acceptable =>
            new List<object[]>
            {
                new object[] { new DateTime(2018, 11, 19, 12, 0, 0) , new DateTime(2018, 11, 19, 14, 0, 0) },
                new object[] { new DateTime(2018, 11, 19, 15, 0, 0) , new DateTime(2018, 11, 19, 16, 0, 0) },
                new object[] { new DateTime(2018, 11, 19, 13, 30, 0) , new DateTime(2018, 11, 19, 14, 0, 0) },
                new object[] { new DateTime(2018, 11, 19, 12, 20, 0) , new DateTime(2018, 11, 19, 15, 0, 0) },
            };

        [Fact]
        public async Task CanReschedule_IfTestDives_IsEmpty_ReturnsTrue()
        {  
            using (var context = new TestDriveContext(_options))
            {
                var repository = new TestDriveRepository(context);
                
                //Act 
                var result = await repository.CanRescheduleAsync(_testDrive, _since.AddHours(2),
                    _till.AddHours(2));
                //Assert
                Assert.True(result);
            }
        }

        [Theory]
        [MemberData(nameof(Unacceptable))]
        public async Task CanReschedule_IfVehicleIsBusy_ReturnsFalse(DateTime since, DateTime till)
        {
            using (var context = new TestDriveContext(_options))
            {
                //Arrange
                var anotherTestDrive = new TestDrive(4, 1, since, till, 1);
                context.AddRange(_testDrive, anotherTestDrive);
                await context.SaveChangesAsync();
            }

            using (var context = new TestDriveContext(_options))
            {
                var toRescheduleTestDrive = new TestDrive(4, 1, since, till, 1);
                var repository = new TestDriveRepository(context);
                //Act
                var result = await repository.CanRescheduleAsync(toRescheduleTestDrive, _since, _till);

                //Assert
                Assert.False(result);
            }
        }

        public static IEnumerable<object[]> Unacceptable =>
            new List<object[]>
            {
                new object[] { new DateTime(2018, 11, 19, 12, 0, 0) , new DateTime(2018, 11, 19, 14, 0, 0) },
                new object[] { new DateTime(2018, 11, 19, 13, 0, 0) , new DateTime(2018, 11, 19, 14, 0, 0) },
                new object[] { new DateTime(2018, 11, 19, 13, 30, 0) , new DateTime(2018, 11, 19, 15, 0, 0) },
                new object[] { new DateTime(2018, 11, 19, 12, 15, 0) , new DateTime(2018, 11, 19, 14, 0, 0) }

            };

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}
