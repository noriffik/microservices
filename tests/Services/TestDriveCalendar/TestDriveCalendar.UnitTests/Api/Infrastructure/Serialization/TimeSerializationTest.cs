﻿using Newtonsoft.Json;
using NexCore.TestDriveCalendar.Api.Domain.Extensions;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Infrastructure.Serialization
{
    public class TimeSerializationTest
    {
        [Fact]
        public void Serialization()
        {
            //Arrange
            var expected = new Time(10, 15, 11);

            //Act
            var json = JsonConvert.SerializeObject(expected);
            var actual = JsonConvert.DeserializeObject<Time>(json);

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
