﻿using Newtonsoft.Json;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Extensions;
using System;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Infrastructure.Serialization
{
    public class DailyScheduleSerializationTest
    {
        [Fact]
        public void Serialization()
        {
            //Arrange
            var expected = new DailySchedule(
                DayOfWeek.Wednesday, new Time(11, 15, 11), new Time(21, 10, 5));

            //Act
            var json = JsonConvert.SerializeObject(expected);
            var actual = JsonConvert.DeserializeObject<DailySchedule>(json);

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
