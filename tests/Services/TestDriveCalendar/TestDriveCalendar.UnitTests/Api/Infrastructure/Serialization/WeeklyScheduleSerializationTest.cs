﻿using Newtonsoft.Json;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Extensions;
using System;
using System.Collections.Generic;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Infrastructure.Serialization
{
    public class WeeklyScheduleSerializationTest
    {
        [Fact]
        public void Serialization()
        {
            //Arrange
            var expected = new Schedule(new List<DailySchedule>
            {
                new DailySchedule(DayOfWeek.Monday, new Time(10, 15, 11), new Time(20, 10, 5)),
                new DailySchedule(DayOfWeek.Wednesday, new Time(11, 15, 11), new Time(21, 10, 5))
            });

            //Act
            var json = JsonConvert.SerializeObject(expected);
            var actual = JsonConvert.DeserializeObject<Schedule>(json);

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
