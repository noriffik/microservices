﻿using AutoFixture;
using FluentAssertions;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Controllers;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Models;
using NexCore.TestDriveCalendar.Api.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Controllers
{
    public class FleetsControllerTest
    {
        private const int VehicleId = 22;
        private const string VinNumber = "12345678901234567";

        private readonly Fixture _fixture = new Fixture();
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IFleetRepository> _fleetRepository;
        private readonly Mock<IDealerRepository> _dealerRepository;
        private readonly FleetsController _controller;
        private readonly VehicleItem _addVehicleQuery;
        private readonly Mock<IFleetQuery> _fleetQuery;
        private readonly Mock<IUpdateVehicleCommandHandler> _updateVehicleCommandHandler = new Mock<IUpdateVehicleCommandHandler>();


        public FleetsControllerTest()
        {
            _addVehicleQuery = _fixture.Build<VehicleItem>()
                .With( i => i.Vin, VinNumber)
                .With( i => i.Id, VehicleId)
                .With(i => i.DealerId, "38001")
                .Create();
            _fleetQuery = new Mock<IFleetQuery>();
            _work = new Mock<IUnitOfWork>();
            _fleetRepository = new Mock<IFleetRepository>();
            _fleetRepository.Setup(r => r.UnitOfWork)
                .Returns(_work.Object);
            _dealerRepository = new Mock<IDealerRepository>();

            _controller = new FleetsController(
                _fleetRepository.Object, _dealerRepository.Object, _fleetQuery.Object, _updateVehicleCommandHandler.Object);
        }

        [Fact]
        public void Ctor_GivenFleetRepository_IsNull_Throws()
        {   
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new FleetsController(
                    null, _dealerRepository.Object, _fleetQuery.Object, _updateVehicleCommandHandler.Object);
            });
            Assert.Equal("fleetRepository", e.ParamName);
        }

        [Fact]
        public void Ctor_GivenDealerRepository_IsNull_Throws()
        {   
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new FleetsController(
                    _fleetRepository.Object, null, _fleetQuery.Object, _updateVehicleCommandHandler.Object);
            });
            Assert.Equal("dealerRepository", e.ParamName);
        }

        [Fact]
        public void Ctor_GivenQueryIsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new FleetsController(
                    _fleetRepository.Object, _dealerRepository.Object, null, _updateVehicleCommandHandler.Object);
            });
            Assert.Equal("fleetQuery", e.ParamName);
        }

        [Fact]
        public void Ctor_GivenUpdateVehicleCommandHandler_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new FleetsController(
                    _fleetRepository.Object, _dealerRepository.Object, _fleetQuery.Object, null);
            });
            Assert.Equal("updateVehicleCommandHandler", e.ParamName);
        }

        [Fact]
        public void Ctor_Mapper_Returns_Mapper()
        {   
            //Act
            var mapper = _controller.Mapper;

            //Assert
            Assert.IsType<VehicleMapper>(mapper);
        }

        [Fact]
        public async Task Add()
        {
            var fleet = _fixture.Create<Fleet>();
            _fleetRepository.Setup(r => r.FindByDealerIdAsync(_addVehicleQuery.DealerId))
                .Returns(Task.FromResult(fleet));
            
            _dealerRepository.Setup(r => r.Has(_addVehicleQuery.DealerId))
                .ReturnsAsync(true);

            _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);

            //Act
            var result = await _controller.Add(_addVehicleQuery);

            //Assert
            var actionResult = Assert.IsType<CreatedAtActionResult>(result);
            Assert.Equal(nameof(FleetsController.Add), actionResult.ActionName);
            Assert.True(actionResult.RouteValues.ContainsKey("id"));
            Assert.Equal(VehicleId, actionResult.RouteValues["id"]);
        }

        [Fact]
        public async Task Add_WhenDealer_HasNoFleet_CreatesFleet()
        {
            //Arrange
            _dealerRepository.Setup(r => r.Has(_addVehicleQuery.DealerId))
                .ReturnsAsync(true);

            _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);

            //Act
            var result = await _controller.Add(_addVehicleQuery);

            //Assert
            Assert.IsType<CreatedAtActionResult>(result);
            _fleetRepository.Verify(
                r => r.Add(It.Is<Fleet>(f => f.DealerId == _addVehicleQuery.DealerId)),
                Times.Once);
        }

        [Fact]
        public async Task Add_WhenVehicle_WithGivenVin_IsAlready_InFleet_Throws()
        {
            //Arrange
            _dealerRepository.Setup(r => r.Has(_addVehicleQuery.DealerId))
                .ReturnsAsync(true);

            var vehicle = new Vehicle(Vin.Parse("12345678901234567"), 1);
            var fleet = new Fleet(_addVehicleQuery.DealerId);
            fleet.Add(vehicle);

            _fleetRepository.Setup(r => r.FindByDealerIdAsync(_addVehicleQuery.DealerId))
                .Returns(Task.FromResult(fleet));
            
            //Assert
            var e = await Assert.ThrowsAsync<ValidationException>(
                async () => 
                {
                    await _controller.Add(_addVehicleQuery);
                });

            var error = e.Errors.FirstOrDefault();
            Assert.NotNull(error);
            Assert.Equal(nameof(_addVehicleQuery.Vin), error.PropertyName);
            Assert.Contains("already", error.ErrorMessage.ToLowerInvariant());
            Assert.Equal(_addVehicleQuery.Vin, error.AttemptedValue);
        }

        [Fact]
        public async Task Add_WhenDealer_DoesNotExist_Throws()
        {
            var e = await Assert.ThrowsAsync<ValidationException>(
                async () => 
                {
                    await _controller.Add(_addVehicleQuery);
                });

            var error = e.Errors.FirstOrDefault();
            Assert.NotNull(error);
            Assert.Equal(nameof(_addVehicleQuery.DealerId), error.PropertyName);
            Assert.Contains("dealer was not found", error.ErrorMessage.ToLowerInvariant());
            Assert.Equal(_addVehicleQuery.DealerId, error.AttemptedValue);
        }

        [Fact]
        public async Task SetAvailability()
        {
            //Arrange
            var vehicle = new Vehicle(11, Vin.Parse("12345678901234567"), 1);
            var fleet = _fixture.Create<Fleet>();
            fleet.Add(vehicle);
            var query = new SetUnavailability
            {
                VehicleId = vehicle.Id,
                Availability = false
            };
            
            _fleetRepository.Setup(r => r.FindByVehicleIdAsync(query.VehicleId))
                .Returns(Task.FromResult(fleet));

            _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);

            //Act
            var result = await _controller.SetAvailability(query);

            //Assert
            Assert.IsType<OkResult>(result);
            Assert.Equal(query.Availability, vehicle.Availability);
            _work.Verify(w => w.Commit(It.IsAny<CancellationToken>()));
        }

        [Fact]
        public async Task SetAvailability_GivenQuery_IsNull_ReturnsBadRequest()
        {
            //Act
            var result = await _controller.SetAvailability(null);

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task SetAvailability_WhenFleet_IsNotFound_Throws()
        {
            //Assert
            var query = new SetUnavailability { VehicleId = 112 };

            //Act
            var e = await Assert.ThrowsAsync<ValidationException>(
                async () => 
                {
                    await _controller.SetAvailability(query);
                });
            
            //Assert
            var error = e.Errors.FirstOrDefault();
            Assert.NotNull(error);
            Assert.Equal(nameof(query.VehicleId), error.PropertyName);
            Assert.Contains("vehicle was not found", error.ErrorMessage.ToLowerInvariant());
            Assert.Equal(query.VehicleId, error.AttemptedValue);
        }

        [Fact]
        public async Task VehicleList_IfDealerNotExists_ReturnsNotFound()
        {
            //Arrange
            _dealerRepository.Setup(d => d.Has("38001")).ReturnsAsync(false);
            
            //Act
            var result = await _controller.VehiclesList("38001");

            //Assert
            Assert.IsType<NotFoundResult>(result);
        }


        [Fact]
        public async Task VehicleList_ReturnsVehiclesItem()
        {
            //Arrange
            var dealerId = DealerCode.Parse("38001");
            var item = new VehicleListItem
            {
                DealerId = dealerId,
                FleetId = 1,
                VehicleId = 1,
                Vin = "Vin",
                ModelId = 1,
                Equipment = "equipment",
                Availability = true
            };
            var vehicleItems = new List<VehicleListItem> {item};
            _dealerRepository.Setup(r => r.Has(dealerId)).ReturnsAsync(true);
            _fleetQuery.Setup(q => q.ByDealerIdAsync(dealerId)).ReturnsAsync(vehicleItems);

            //Act
            var result = await _controller.VehiclesList(dealerId);

            //Assert
            Assert.IsType<JsonResult>(result);
            Assert.Equal(vehicleItems, result.As<JsonResult>().Value);
        }

        [Fact]
        public async Task Get_WhenFounded_ForGivenId_ReturnsOkResult()
        {
            //Arrange
            var vehicle = _fixture.Build<VehicleListItem>()
                .With(v => v.VehicleId, 1)
                .Create();
            _fleetQuery.Setup(q => q.ByVehicleIdAsync(1)).ReturnsAsync(vehicle);

            //Act
            var result = await _controller.Get(1);

            //Assert
            var objectResult = Assert.IsType<JsonResult>(result);
            Assert.NotNull(objectResult);
            Assert.Equal(vehicle, objectResult.Value);
        }

        [Fact]
        public async Task Get_When_IsNotFound_ReturnsNotFound()
        {
            //Act
            var result = await _controller.Get(777);

            //Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task Update_WhenGivenCommand_IsNull_ReturnsBadRequest()
        {
            //Assert
            var result = await _controller.Update(null);

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task Update_Command_HasFailed_ReturnsBadRequest()
        {
            //Arrange
            var command = _fixture.Create<UpdateVehicleCommand>();
            _updateVehicleCommandHandler.Setup(h => h.Handle(command, It.IsAny<CancellationToken>())).ReturnsAsync(false);

            //Assert
            var result = await _controller.Update(command);

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task Update_Command_HasCompletedSuccessfully_ReturnsBadRequest()
        {
            //Arrange
            var command = _fixture.Create<UpdateVehicleCommand>();
            _updateVehicleCommandHandler.Setup(h => h.Handle(command, It.IsAny<CancellationToken>())).ReturnsAsync(true);

            //Assert
            var result = await _controller.Update(command);

            //Assert
            Assert.IsType<OkResult>(result);
        }
    }
}
