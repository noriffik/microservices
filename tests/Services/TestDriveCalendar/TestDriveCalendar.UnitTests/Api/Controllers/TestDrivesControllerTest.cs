﻿using AutoFixture;
using Moq;
using NexCore.TestDriveCalendar.Api.Controllers;
using NexCore.TestDriveCalendar.Api.Models;
using NexCore.TestDriveCalendar.Api.Queries;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Controllers
{
    public class TestDrivesControllerTest
    {
        private readonly Fixture _fixture = new Fixture();
        private readonly Mock<ITestDriveQueries> _testDriveQueries;
        private readonly TestDrivesController _controller;

        private readonly TestDrive _testDrive = new TestDrive(1, 1, 1, new DateTime(2018, 12, 19, 14, 0, 0), new DateTime(2018, 12, 19, 16, 0, 0), 1);

        public TestDrivesControllerTest()
        {
            _testDriveQueries = new Mock<ITestDriveQueries>();
            _controller = new TestDrivesController(_testDriveQueries.Object, SetupTestDriveRepository(_testDrive));
        }
        
        private static ITestDriveRepository SetupTestDriveRepository(TestDrive testDrive)
        {
            var repository = new Mock<ITestDriveRepository>();
            repository.Setup(r => r.Find(testDrive.Id))
                .Returns(Task.FromResult(testDrive));

            return repository.Object;
        }
        
        [Fact]
        public void Ctor_GivenQueries_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new TestDrivesController(null, SetupTestDriveRepository(_testDrive));
            });
            Assert.Equal("queries", e.ParamName);
        }

        [Fact]
        public void Ctor_GivenTestDriveRepository_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new TestDrivesController(_testDriveQueries.Object, null);
            });
            Assert.Equal("testDriveRepository", e.ParamName);
        }

        [Fact]
        public async Task List()
        {
            //Arrange
            var testDrives = _fixture.CreateMany<TestDriveListItem>(3);
            _testDriveQueries.Setup(q => q.AllAsync()).ReturnsAsync(testDrives);

            //Act
            var result = await _controller.List();

            //Assert
            Assert.NotNull(result);
            Assert.Equal(testDrives, result.Value);
        }

        [Fact]
        public async Task Get_WhenFounded_ForGivenId_ReturnsOkResult()
        {
            //Act
            var result = await _controller.Get(_testDrive.Id);

            //Assert
            var objectResult = Assert.IsType<JsonResult>(result);
            Assert.NotNull(objectResult);
            Assert.Equal(_testDrive, objectResult.Value);
        }

        [Fact]
        public async Task Get_When_IsNotFound_ReturnsNotFound()
        {
            //Act
            var result = await _controller.Get(777);

            //Assert
            Assert.IsType<NotFoundResult>(result);
        }
    }
}
