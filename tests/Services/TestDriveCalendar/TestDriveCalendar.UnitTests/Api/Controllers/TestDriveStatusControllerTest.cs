﻿using AutoFixture;
using FluentValidation;
using Moq;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Controllers;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Controllers
{
    public class TestDriveStatusControllerTest
    {
        private readonly Fixture _fixture = new Fixture();
        private readonly TestDriveStatusCommand _command;
        private readonly TestDrive _testDrive;
        private readonly Mock<ITestDriveRepository> _testDriveWillNotFoundRepository;
        private readonly Mock<ITestDriveRepository> _testDriveWillDeclineRepository;
        

        public TestDriveStatusControllerTest()
        {
          _command = _fixture.Create<TestDriveStatusCommand>();

          _testDrive = new TestDrive(1, 1, 1, DateTime.Now, DateTime.Now.AddDays(1), 1);

            _testDriveWillNotFoundRepository = new Mock<ITestDriveRepository>();
            _testDriveWillNotFoundRepository
                .Setup(r => r.Find(_command.TestDriveId))
                   .ReturnsAsync(null as TestDrive);

            _testDriveWillDeclineRepository = new Mock<ITestDriveRepository>();
            _testDriveWillDeclineRepository
                .Setup(r => r.Find(_command.TestDriveId)).ReturnsAsync(_testDrive);
        }

        [Fact]
        public void Ctor_GivenRepositoryIsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new TestDriveStatusController(null);
            });

            Assert.Equal("testDriveRepository", e.ParamName);
        }

        [Fact]
        public async Task Begin_IfCommandIsNull_Throws()
        {
            //Arrange
            var controller = new TestDriveStatusController(_testDriveWillDeclineRepository.Object);

            //Act
            var result = await controller.Begin(null);
            
            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task Begin_IfTestDriveWasNotFound_Throws()
        {
            //Arrange
            var controller = new TestDriveStatusController(_testDriveWillNotFoundRepository.Object);

            //Assert
            var e = await Assert.ThrowsAnyAsync<ValidationException>(async () => { await controller.Begin(_command); });
           
            Assert.Contains("TestDrive does not exists!", e.Errors.First().ErrorMessage);
        }

        [Fact]
        public async Task Begin_IfBeginWasDeclined_Throws()
        {
            //Arrange
            var controller = new TestDriveStatusController(_testDriveWillDeclineRepository.Object);
            _testDrive.Begin(_command.SalesManagerId);

            //Assert
            var e = await Assert.ThrowsAnyAsync<ValidationException>(async () => { await controller.Begin(_command); });
            Assert.Contains("Attempt to assign invalid status", e.Errors.First().ErrorMessage);
        }

        [Fact]
        public async Task Complete_IfTestDriveWasNotFound_Throws()
        {
            //Arrange
            var controller = new TestDriveStatusController(_testDriveWillNotFoundRepository.Object);

            //Assert
            var e = await Assert.ThrowsAnyAsync<ValidationException>(async () => { await controller.Complete(_command); });
            Assert.Contains("TestDrive does not exists!", e.Errors.First().ErrorMessage);
        }

        [Fact]
        public async Task Complete_IfBeginWasDeclined_Throws()
        {
            //Arrange
            _testDrive.Begin(_command.SalesManagerId);
            _testDrive.Complete(_command.SalesManagerId);
            var controller = new TestDriveStatusController(_testDriveWillDeclineRepository.Object);

            //Assert
            var e = await Assert.ThrowsAnyAsync<ValidationException>(async () => { await controller.Complete(_command); });
            Assert.Contains("Attempt to assign invalid status", e.Errors.First().ErrorMessage);
        }

        [Fact]
        public async Task Cancel_IfTestDriveWasNotFound_Throws()
        {
            //Arrange
            var controller = new TestDriveStatusController(_testDriveWillNotFoundRepository.Object);

            //Assert
            var e = await Assert.ThrowsAnyAsync<ValidationException>(async () => { await controller.Cancel(_command); });
            Assert.Contains("TestDrive does not exists!", e.Errors.First().ErrorMessage);
        }

        [Fact]
        public async Task Cancel_IfBeginWasDeclined_Throws()
        {
            //Arrange
            _testDrive.Begin(_command.SalesManagerId);
            _testDrive.Complete(_command.SalesManagerId);
            var controller = new TestDriveStatusController(_testDriveWillDeclineRepository.Object);

            //Assert
            var e = await Assert.ThrowsAnyAsync<ValidationException>(async () => { await controller.Cancel(_command); });
            Assert.Contains("Attempt to assign invalid status", e.Errors.First().ErrorMessage);
        }
    }
}
