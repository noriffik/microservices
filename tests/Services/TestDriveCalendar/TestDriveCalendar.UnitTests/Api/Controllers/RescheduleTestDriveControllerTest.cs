﻿using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Controllers;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Controllers
{
    public class RescheduleTestDriveControllerTest
    {
        private const int ClientId = 1;
        private const int VehicleId = 22;
        private const int SalesManagerId = 2;
        private const int TestDriveId = 1;
        private const int FleetId = 5;
        private readonly DealerCode _dealerId = DealerCode.Parse("38001");
        private readonly DateTime _since = new DateTime(2018, 11, 5, 10, 0, 0);
        private readonly DateTime _sinceOutsideWorkingHours = new DateTime(2018, 11, 5, 7, 0, 0);
        private readonly DateTime _till = new DateTime(2018, 11, 5, 12, 0, 0);
        private readonly Vin _vin = Vin.Parse("00000000000000001");
        private readonly TestDrive _testDrive;

        private readonly Mock<IUnitOfWork> _work = new Mock<IUnitOfWork>();
        private readonly RescheduleTestDriveController _controller;
        private readonly RescheduleTestDriveCommand _command;

        private readonly Mock<IFleetRepository> _fleetRepository;
        private readonly Mock<IDealerRepository> _dealerRepository;
        private readonly Mock<ITestDriveRepository> _testDriveRepository;

        public RescheduleTestDriveControllerTest()
        {
            var vehicle = new Vehicle(VehicleId, _vin, 5);
            var dealer = new Dealer(_dealerId, Schedule.Typical, 60);
            var fleet = new Fleet(FleetId, _dealerId);
            fleet.Add(vehicle);

            _testDrive = new TestDrive(TestDriveId, ClientId, VehicleId, _since, _till, SalesManagerId);

            _fleetRepository = new Mock<IFleetRepository>();
            _fleetRepository.Setup(fr => fr.FindByVehicleIdAsync(VehicleId))
                .Returns(Task.FromResult(fleet));

            _dealerRepository = new Mock<IDealerRepository>();
            _dealerRepository.Setup(dr => dr.FindByFleetIdAsync(fleet.Id))
                .Returns(Task.FromResult(dealer));

            _testDriveRepository = SetupTestDriveRepository(_work.Object, _testDrive);

            _controller = new RescheduleTestDriveController(
                _testDriveRepository.Object,
                _dealerRepository.Object,
                _fleetRepository.Object);

            _command = new RescheduleTestDriveCommand
            {
                TestDriveId = TestDriveId,
                Since = _since,
                Till = _till
            };
        }

        private Mock<ITestDriveRepository> SetupTestDriveRepository(IUnitOfWork work, TestDrive testDrive)
        {
            var repository = new Mock<ITestDriveRepository>();
            repository.Setup(r => r.Find(testDrive.Id))
                .Returns(Task.FromResult(testDrive));
            repository.Setup(r => r.CanRescheduleAsync(testDrive, _since, _till))
                .ReturnsAsync(true);
            repository.Setup(r => r.CanRescheduleAsync(testDrive, _sinceOutsideWorkingHours, _till))
                .ReturnsAsync(true);
            repository.Setup(r => r.UnitOfWork)
                .Returns(work);

            return repository;
        }

        [Fact]
        public void Ctor_GivenTestDriveRepository_IsNull_Throws()
        {   
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new CreateTestDriveController(null, _dealerRepository.Object, _fleetRepository.Object);
            });
            Assert.Equal("testDriveRepository", e.ParamName);
        }

        [Fact]
        public void Ctor_GivenDealerRepository_IsNull_Throws()
        {   
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new CreateTestDriveController(_testDriveRepository.Object, null, _fleetRepository.Object);
            });
            Assert.Equal("dealerRepository", e.ParamName);
        }

        [Fact]
        public void Ctor_GivenFleetRepository_IsNull_Throws()
        {   
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new CreateTestDriveController(_testDriveRepository.Object, _dealerRepository.Object, null);
            });
            Assert.Equal("fleetRepository", e.ParamName);
        }

        [Fact]
        public async Task SetTestDriveDuration_WhenGivenCommand_HasCompletedSuccessfully_ReturnsOkResult()
        {
            //Arrange
            _work.Setup(u => u.Commit(CancellationToken.None))
                .Returns(Task.FromResult(true));

            //Act
            var result = await _controller.Reschedule(_command);

            //Assert
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async Task SetTestDriveDuration_WhenGivenCommand_IsNull_ReturnsBadRequestResult()
        {
            //Act
            var result = await _controller.Reschedule(null);

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task SetTestDriveDuration_WhenTestDrive_DoesNotExists_Throws()
        {
            //Arrange
            _command.TestDriveId = 777;

            //Assert
            var e = await Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _controller.Reschedule(_command);
            });
            var error = e.Errors.FirstOrDefault();
            Assert.NotNull(error);
            Assert.Equal("TestDriveId", error.PropertyName);
            Assert.Contains("testdrive not found", error.ErrorMessage.ToLowerInvariant());
            Assert.Equal(_command.TestDriveId, error.AttemptedValue);
        }
        
        [Fact]
        public async Task CreateTestDrive_WhenDealer_IsNotAvailable_Throws()
        {
            //Arrange
            _command.Since = _sinceOutsideWorkingHours;

            //Assert
            var e = await Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _controller.Reschedule(_command);
            });
            var error = e.Errors.FirstOrDefault();
            Assert.NotNull(error);
            Assert.Equal("Since", error.PropertyName);
            Assert.Equal(_command.Since, error.AttemptedValue);
            Assert.Contains("dealer is closed", error.ErrorMessage.ToLowerInvariant());
        }

        [Fact]
        public async Task CreateTestDrive_WhenStatus_IsNotAvailable_Throws()
        {
            _testDrive.Begin(1);
            _testDrive.Complete(1);

            //Assert
            var e = await Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _controller.Reschedule(_command);
            });
            var error = e.Errors.FirstOrDefault();
            Assert.NotNull(error);
            Assert.Equal("TestDriveId", error.PropertyName);
            Assert.Equal(_command.TestDriveId, error.AttemptedValue);
            Assert.Contains("cannot be rescheduled", error.ErrorMessage.ToLowerInvariant());
        }

        [Fact]
        public async Task SetTestDriveDuration_GivenRequest_IsNull_Throws()
        {
            //Act
            var result = await _controller.Reschedule(null);

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

    }
}
