﻿using AutoFixture;
using Moq;
using NexCore.TestDriveCalendar.Api.Controllers;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Models;
using NexCore.TestDriveCalendar.Api.Queries;
using System;
using System.Collections;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Controllers
{
    public class MakesControllerTest
    {
        private readonly Fixture _fixture = new Fixture();
        private readonly Mock<IMakeQueries> _makeQueries;
        private readonly Mock<IMakeRepository> _makeRepository;
        private readonly MakesController _controller;

        public MakesControllerTest()
        {
            _makeQueries = new Mock<IMakeQueries>();
            _makeRepository = new Mock<IMakeRepository>();

            _controller = new MakesController(_makeQueries.Object, _makeRepository.Object);
        }

        [Fact]
        public void Ctor_GivenQueries_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new MakesController(null, _makeRepository.Object);
            });
            Assert.Equal("queries", e.ParamName);
        }

        [Fact]
        public void Ctor_GivenRepository_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new MakesController(_makeQueries.Object, null);
            });
            Assert.Equal("repository", e.ParamName);
        }

        [Fact]
        public async Task List()
        {
            //Arrange
            var makes = _fixture.CreateMany<MakeListItem>(3);
            _makeQueries.Setup(q => q.AllAsync()).ReturnsAsync(makes);

            //Act
            var result = await _controller.List();

            //Assert
            Assert.NotNull(result);
            Assert.Equal(makes, result.Value);
        }

        [Fact]
        public async Task Models()
        {
            //Arrange
            var make = new Make(_fixture.Create<int>(), _fixture.Create<string>());
            make.AddModel(_fixture.Create<Model>());
            make.AddModel(_fixture.Create<Model>());
            _makeRepository.Setup(r => r.Find(make.Id)).ReturnsAsync(make);

            //Act
            var result = await _controller.Models(make.Id);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(make.Models, result.Value);
        }

        [Fact]
        public async Task Models_WhenMake_IsNotFound_ReturnsEmptyResponse()
        {
            //Act
            var result = await _controller.Models(313123);

            //Assert
            Assert.NotNull(result);
            Assert.Empty(result.Value as IEnumerable);
        }
    }
}
