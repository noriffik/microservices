﻿using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Application.Models;
using NexCore.TestDriveCalendar.Api.Controllers;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Controllers
{
    public class SetScheduleControllerTest
    {
        private readonly Mock<IUnitOfWork> _work = new Mock<IUnitOfWork>();
        private readonly SetScheduleCommand _command;
        private readonly SetScheduleController _controller;

        public SetScheduleControllerTest()
        {
            var dealer = new Dealer(DealerCode.Parse("38001"), Schedule.Typical, 60);

            _command = CreateCommand(dealer);
            
            _controller = new SetScheduleController(
                SetupDealerRepository(_work.Object, dealer));
        }

        private static SetScheduleCommand CreateCommand(Dealer dealer)
        {
            var dailyScheduleModels = Enumerable.Range(0, 7)
                .Select(d => new DailyScheduleModel
                {
                    WorkingDay = d,
                    WorkingHours = new WorkingHoursModel
                    {
                        Since = 5000,
                        Till = 8000
                    }
                })
                .ToList();

            return new SetScheduleCommand
            {
                DealerId = dealer.Id,
                WeeklySchedule = new List<DailyScheduleModel>(dailyScheduleModels)
            };
        }

        private static IDealerRepository SetupDealerRepository(IUnitOfWork work, Dealer dealer)
        {
            var repository = new Mock<IDealerRepository>();
            repository.Setup(r => r.Find(dealer.Id))
                .Returns(Task.FromResult(dealer));
            repository.Setup(r => r.UnitOfWork)
                .Returns(work);

            return repository.Object;
        }

        [Fact]
        public void Ctor_GivenDealerRepository_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new SetScheduleController(null);
            });
            Assert.Equal("dealerRepository", e.ParamName);
        }
        
        [Fact]
        public async Task SetSchedule_WhenGivenCommand_HasCompletedSuccessfully_ReturnsOkResult()
        {
             //Arrange
            _work.Setup(u => u.Commit(CancellationToken.None))
                .Returns(Task.FromResult(true));

            //Act
            var result = await _controller.SetSchedule(_command);

            //Assert
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async Task SetSchedule_WhenGivenCommand_IsNull_ReturnsBadRequestResult()
        {
            //Act
            var result = await _controller.SetSchedule(null);

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task SetSchedule_WhenSuccess_ReturnsOkResult()
        {
            //Arrange
            _work.Setup(u => u.Commit(CancellationToken.None))
                .Returns(Task.FromResult(true));

            //Act
            var result = await _controller.SetSchedule(_command);

            //Assert
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async Task SetSchedule_WhenGivenDealerId_DoesNotExists_Throws()
        {
            //Arrange
            _command.DealerId = "99999";

            //Assert
            var e = await Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _controller.SetSchedule(_command);
            });
            var error = e.Errors.FirstOrDefault();
            Assert.NotNull(error);
            Assert.Equal(nameof(_command.DealerId), error.PropertyName);
            Assert.Contains("dealer not found", error.ErrorMessage.ToLowerInvariant());
            Assert.Equal(_command.DealerId, error.AttemptedValue);
        }

        [Fact]
        public async Task SetSchedule_GivenRequest_IsNull_Throws()
        {
            //Act
            var result = await _controller.SetSchedule(null);

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }
    }
}
