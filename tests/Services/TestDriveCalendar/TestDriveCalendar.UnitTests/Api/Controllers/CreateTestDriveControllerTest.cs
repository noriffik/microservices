﻿using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Controllers;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Controllers
{
    public class CreateTestDriveControllerTest
    {
        private const int ClientId = 1;
        private const int VehicleId = 22;
        private const int SalesManagerId = 2;
        private const int FleetId = 5;
        private readonly DealerCode _dealerId = DealerCode.Parse("38001");
        private readonly DateTime _since = new DateTime(2016, 11, 5, 10, 0, 0);
        private readonly DateTime _till = new DateTime(2016, 11, 5, 12, 0, 0);
        private readonly Vin _vin = Vin.Parse("00000000000000001");

        private readonly Mock<IUnitOfWork> _work = new Mock<IUnitOfWork>();
        private readonly CreateTestDriveController _controller;
        private readonly CreateTestDriveCommand _command;

        private readonly Mock<IFleetRepository> _fleetRepository;
        private readonly Mock<IDealerRepository> _dealerRepository;
        private readonly Mock<ITestDriveRepository> _testDriveRepository;

        public CreateTestDriveControllerTest()
        {
            var vehicle = new Vehicle(VehicleId, _vin, 5);
            var dealer = new Dealer(_dealerId, Schedule.Typical, 60);
            var fleet = new Fleet(FleetId, _dealerId);
            fleet.Add(vehicle);

            _fleetRepository = new Mock<IFleetRepository>();
            _fleetRepository.Setup(fr => fr.FindByVehicleIdAsync(VehicleId))
                .Returns(Task.FromResult(fleet));

            _dealerRepository = new Mock<IDealerRepository>();
            _dealerRepository.Setup(dr => dr.FindByFleetIdAsync(fleet.Id))
                .Returns(Task.FromResult(dealer));

            _testDriveRepository = new Mock<ITestDriveRepository>();
            _testDriveRepository.Setup(r => r.UnitOfWork)
                .Returns(_work.Object);

            _controller = new CreateTestDriveController(
                _testDriveRepository.Object,
                _dealerRepository.Object,
                _fleetRepository.Object);

            _command = new CreateTestDriveCommand
            {
                ClientId = ClientId,
                SalesManagerId = SalesManagerId,
                Since = _since,
                Till = _till,
                VehicleId = VehicleId
            };
        }

        [Fact]
        public void Ctor_GivenTestDriveRepository_IsNull_Throws()
        {   
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new CreateTestDriveController(null, _dealerRepository.Object, _fleetRepository.Object);
            });
            Assert.Equal("testDriveRepository", e.ParamName);
        }

        [Fact]
        public void Ctor_GivenDealerRepository_IsNull_Throws()
        {   
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new CreateTestDriveController(_testDriveRepository.Object, null, _fleetRepository.Object);
            });
            Assert.Equal("dealerRepository", e.ParamName);
        }

        [Fact]
        public void Ctor_GivenFleetRepository_IsNull_Throws()
        {   
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new CreateTestDriveController(_testDriveRepository.Object, _dealerRepository.Object, null);
            });
            Assert.Equal("fleetRepository", e.ParamName);
        }

        [Fact]
        public async Task CreateTestDrive()
        {
            //Arrange
            _work.Setup(u => u.Commit(CancellationToken.None))
                .Returns(Task.FromResult(true));

            //Act
            var result = await _controller.Create(_command);

            //Assert
            var actionResult = Assert.IsType<CreatedAtActionResult>(result);
            Assert.Equal(nameof(TestDrivesController.Get), actionResult.ActionName);
            Assert.True(actionResult.RouteValues.ContainsKey("id"));
            Assert.NotNull(actionResult.RouteValues["id"]);
            Assert.NotNull(actionResult.Value);
        }

        [Fact]
        public async Task CreateTestDrive_WhenGivenCommand_IsNull_ReturnsBadRequestResult()
        {
            //Act
            var result = await _controller.Create(null);

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task CreateTestDrive_WhenFleet_ForGivenVehicleId_DoesNotExists_Throws()
        {
            //Arrange
            _command.VehicleId = 123;

            //Assert
            var e = await Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _controller.Create(_command);
            });
            var error = e.Errors.FirstOrDefault();
            Assert.NotNull(error);
            Assert.Equal("vehicleId", error.PropertyName);
            Assert.Contains("fleet not found", error.ErrorMessage.ToLowerInvariant());
            Assert.Equal(_command.VehicleId, error.AttemptedValue);
        }

        [Fact]
        public async Task CreateTestDrive_WhenDealer_IsNotAvailable_Throws()
        {
            //Arrange
            _command.Since = new DateTime(2016, 11, 5, 7, 0, 0);

            //Assert
            var e = await Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _controller.Create(_command);
            });
            var error = e.Errors.FirstOrDefault();
            Assert.NotNull(error);
            Assert.Equal("dealer", error.PropertyName);
            Assert.Contains("dealer is not available", error.ErrorMessage.ToLowerInvariant());
            Assert.IsType<Dealer>(error.AttemptedValue);
        }

        [Fact]
        public async Task CreateTestDrive_GivenRequest_IsNull_Throws()
        {
            //Act
            var result = await _controller.Create(null);

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

    }
}
