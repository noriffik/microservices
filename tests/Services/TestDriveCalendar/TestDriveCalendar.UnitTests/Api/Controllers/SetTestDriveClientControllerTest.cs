﻿using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Controllers;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Controllers
{
    public class SetTestDriveClientControllerTest
    {
        private const int ClientId = 1;
        private const int VehicleId = 22;
        private const int SalesManagerId = 2;
        private const int TestDriveId = 1;
        private readonly DateTime _since = new DateTime(2016, 11, 5, 10, 0, 0);
        private readonly DateTime _till = new DateTime(2016, 11, 5, 12, 0, 0);

        private readonly Mock<IUnitOfWork> _work = new Mock<IUnitOfWork>();
        private readonly SetTestDriveClientController _controller;
        private readonly SetTestDriveClientCommand _command;

        public SetTestDriveClientControllerTest()
        {
            var testDrive = new TestDrive(ClientId, VehicleId, _since, _till, SalesManagerId);

            _controller = new SetTestDriveClientController(
                SetupTestDriveRepository(_work.Object, testDrive));

            _command = new SetTestDriveClientCommand
            {
                TestDriveId = TestDriveId,
                ClientId = ClientId
            };
        }

        private static ITestDriveRepository SetupTestDriveRepository(IUnitOfWork work, TestDrive testDrive)
        {
            var repository = new Mock<ITestDriveRepository>();
            repository.Setup(r => r.Find(TestDriveId))
                .Returns(Task.FromResult(testDrive));
            repository.Setup(r => r.UnitOfWork)
                .Returns(work);

            return repository.Object;
        }

        [Fact]
        public void Ctor_GivenTestDriveRepository_IsNull_Throws()
        {   
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new SetTestDriveClientController(null);
            });
            Assert.Equal("testDriveRepository", e.ParamName);
        }

        [Fact]
        public async Task SetTestDriveClient_WhenGivenCommand_HasCompletedSuccessfully_ReturnsOkResult()
        {
            //Arrange
            _work.Setup(u => u.Commit(CancellationToken.None))
                .Returns(Task.FromResult(true));

            //Act
            var result = await _controller.SetClient(_command);

            //Assert
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async Task SetTestDriveClient_WhenGivenCommand_IsNull_ReturnsBadRequestResult()
        {
            //Act
            var result = await _controller.SetClient(null);

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task SetTestDriveClient_WhenTestDrive_DoesNotExists_Throws()
        {
            //Arrange
            _command.TestDriveId = 888;

            //Assert
            var e = await Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _controller.SetClient(_command);
            });
            var error = e.Errors.FirstOrDefault();
            Assert.NotNull(error);
            Assert.Equal("testDriveId", error.PropertyName);
            Assert.Contains("testdrive not found", error.ErrorMessage.ToLowerInvariant());
            Assert.Equal(_command.TestDriveId, error.AttemptedValue);
        }

        [Fact]
        public async Task SetTestDriveClient_GivenRequest_IsNull_Throws()
        {
            //Act
            var result = await _controller.SetClient(null);

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }
    }
}
