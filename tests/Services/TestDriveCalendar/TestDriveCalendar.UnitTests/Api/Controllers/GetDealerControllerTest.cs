﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.TestDriveCalendar.Api.Controllers;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Models;
using NexCore.Testing;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Controllers
{
    public class GetDealerControllerTest
    {
        private readonly Dealer _dealer = new Dealer(DealerCode.Parse("38001"), Schedule.Typical, 60);
        private readonly GetDealerController _controller;
        private readonly Mock<IMapper> _mapper;

        public GetDealerControllerTest()
        {
            _mapper = new Mock<IMapper>();

            _controller = new GetDealerController(SetupDealerRepository(_dealer), _mapper.Object);
        }

        private static IDealerRepository SetupDealerRepository(Dealer dealer)
        {
            var repository = new Mock<IDealerRepository>();
            repository.Setup(r => r.Find(dealer.Id))
                .Returns(Task.FromResult(dealer));

            return repository.Object;
        }

        [Fact]
        public void Ctor_GivenDealerRepository_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealerRepository", () =>
            {
                var unused = new GetDealerController(null, _mapper.Object);
            });
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                var unused = new GetDealerController(SetupDealerRepository(_dealer), null);
            });
        }
        
        [Fact]
        public async Task GetDealer_WhenFoundedDealer_ForGivenId_ReturnsOkResult()
        {
            //Arrange
            var expected = new DealerModel {Id = "38001"};
            _mapper.Setup(m => m.Map<DealerModel>(_dealer)).Returns(expected);

            //Act
            var result = await _controller.GetDealer(_dealer.Id);

            //Assert
            var objectResult = Assert.IsType<JsonResult>(result);
            Assert.NotNull(objectResult);
            Assert.Equal(expected, objectResult.Value);
        }

        [Fact]
        public async Task GetDealer_WhenDealer_IsNotFound_ReturnsNotFound()
        {
            //Act
            var result = await _controller.GetDealer("99999");

            //Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void Mapping()
        {
            //Arrange
            var mapperConfig = new MapperConfiguration(
                c => c.AddProfiles(typeof(DealerModel).Assembly));

            var mapper = mapperConfig.CreateMapper();

            var expected = new DealerModel
            {
                Id = _dealer.Id.Value,
                Schedule = _dealer.Schedule,
                TestDriveDuration = _dealer.TestDriveDuration
            };

            //Act
            var actual = mapper.Map<DealerModel>(_dealer);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<DealerModel>.Instance);
        }
    }
}
