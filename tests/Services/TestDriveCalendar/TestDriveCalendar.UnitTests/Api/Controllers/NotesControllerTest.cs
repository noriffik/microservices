﻿using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Controllers;
using NexCore.TestDriveCalendar.Api.Queries;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Controllers
{
    public class NotesControllerTest
    {
        private readonly NotesController _controller;
        private  readonly Fixture _fixture = new Fixture();
        private readonly Mock<ICreateNoteCommandHandler> _createCommandHandler = new Mock<ICreateNoteCommandHandler>();
        private readonly Mock<INotesQuery> _notesQuery = new Mock<INotesQuery>();
        private readonly Mock<IUpdateNoteCommandHandler> _updateNoteCommandHandler = new Mock<IUpdateNoteCommandHandler>();
        private readonly Mock<IRemoveNoteCommandHandler> _removeNoteCommandHandler = new Mock<IRemoveNoteCommandHandler>();

        public NotesControllerTest()
        {
            _controller = new NotesController(_createCommandHandler.Object, _notesQuery.Object, 
                _updateNoteCommandHandler.Object, _removeNoteCommandHandler.Object);
        }

        [Fact]
        public void Ctor_GivenCreateNoteCommandHandlerIsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new NotesController(
                    null, _notesQuery.Object, _updateNoteCommandHandler.Object, _removeNoteCommandHandler.Object);
            });

            Assert.Equal("createNoteCommandHandler", e.ParamName);
        }

        [Fact]
        public void Ctor_GivenNotesQueryIsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new NotesController(
                    _createCommandHandler.Object, null, _updateNoteCommandHandler.Object, _removeNoteCommandHandler.Object);
            });

            Assert.Equal("notesQuery", e.ParamName);
        }

        [Fact]
        public void Ctor_GivenUpdateNoteCommandHandler_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new NotesController(
                    _createCommandHandler.Object, _notesQuery.Object, null, _removeNoteCommandHandler.Object);
            });

            Assert.Equal("updateNoteCommandHandler", e.ParamName);
        }

        [Fact]
        public void Ctor_GivenRemoveNoteCommandHandler_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new NotesController(
                    _createCommandHandler.Object, _notesQuery.Object, _updateNoteCommandHandler.Object, null);
            });

            Assert.Equal("removeNoteCommandHandler", e.ParamName);
        }

        [Fact]
        public async Task Create()
        {
            //Arrange
            var command = _fixture.Create<CreateNoteCommand>();
            _createCommandHandler.Setup(h => h.Handle(command, It.IsAny<CancellationToken>())).ReturnsAsync(_fixture.Create<int>());

            //Act
            var result = await _controller.Create(command);

            //Assert
            Assert.IsType<CreatedAtActionResult>(result);
        }


        [Fact]
        public async Task Create_IfWasFailed_ReturnsBadRequest()
        {
            //Arrange
            var command = _fixture.Create<CreateNoteCommand>();

            //Act
            var result = await _controller.Create(command);

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task Create_GivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var result = await _controller.Create(null);
            //Arrange
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task GetNotesByTestDriveId()
        {
            //Arrange
            var testDriveId = _fixture.Create<int>();
            var expected = new List<dynamic>() { new object() };
            _notesQuery.Setup(q => q.GetAllNotesByTestDriveId(testDriveId))
                .ReturnsAsync(expected);
            
            //Act
            var result = await _controller.GetNotesByTestDriveId(testDriveId);

            //Assert
            Assert.Equal(expected, Assert.IsAssignableFrom<JsonResult>(result).Value);
        }

        [Fact]
        public async Task UpdateNote()
        {
            //Arrange
            var command = _fixture.Create<UpdateNoteCommand>();
            _updateNoteCommandHandler.Setup(h => h.Handle(command, It.IsAny<CancellationToken>())).ReturnsAsync(true);
            //Act
            var result = await _controller.Update(command);
            //Assert
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async Task UpdateNote_IfWasFailed_ReturnsBadRequest()
        {
            //Arrange
            var command = _fixture.Create<UpdateNoteCommand>();
            _updateNoteCommandHandler.Setup(h => h.Handle(command, It.IsAny<CancellationToken>())).ReturnsAsync(false);
            //Act
            var result = await _controller.Update(command);
            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task UpdateNote_GivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var result = await _controller.Update(null);
            //Arrange
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task UpdateNote_WhenHandlerThrow_OwnerRequiredException_ReturnsBadRequest()
        {
            //Arrange
            var command = _fixture.Create<UpdateNoteCommand>();
            _updateNoteCommandHandler.Setup(h => h.Handle(command, CancellationToken.None))
                .ThrowsAsync(new OwnerRequiredException(
                    $"Manager with Id {command.ManagerId} is not owner of note with id {command.NoteId}")
                );

            //Act
            var result = await _controller.Update(command);

            //Arrange
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task RemoveNote()
        {
            //Arrange
            var command = _fixture.Create<RemoveNoteCommand>();
            _removeNoteCommandHandler.Setup(h => h.Handle(command, CancellationToken.None)).ReturnsAsync(true);

            //Act
            var result = await _controller.Remove(command);

            //Assert
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async Task RemoveNote_IfWasFailed_ReturnsBadRequest()
        {
            //Arrange
            var command = _fixture.Create<RemoveNoteCommand>();
            _removeNoteCommandHandler.Setup(h => h.Handle(command, CancellationToken.None)).ReturnsAsync(false);

            //Act
            var result = await _controller.Remove(command);

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task RemoveNote_GivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var result = await _controller.Remove(null);

            //Arrange
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task RemoveNote_WhenHandlerThrow_OwnerRequiredException_ReturnsBadRequest()
        {
            //Arrange
            var command = _fixture.Create<RemoveNoteCommand>();
            _removeNoteCommandHandler.Setup(h => h.Handle(command, CancellationToken.None))
                .ThrowsAsync(new OwnerRequiredException(
                    $"Manager with Id {command.ManagerId} is not owner of note with id {command.NoteId}")
                    );

            //Act
            var result = await _controller.Remove(command);

            //Arrange
            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
