﻿using AutoFixture;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System;
using System.Linq;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Domain.Vehicles
{
    public class MakeTest
    {
        private readonly Fixture _fixture = new Fixture();

        [Fact]
        public void Ctor()
        {
            //Arrange
            var name = _fixture.Create<string>();

            //Act
            var make = new Make(name);

            //Assert
            Assert.Equal(make.Name, name);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Ctor_GivenNameNullOrEmpty_Throws(string name)
        {
            var e = Assert.Throws<ArgumentException>(() => 
            {
                var unused = new Make(name);
            });
            Assert.Equal("name", e.ParamName);
        }

        [Fact]
        public void Ctor_WithId()
        {
            //Arrange
            var id = _fixture.Create<int>();
            var name = _fixture.Create<string>();

            //Act
            var make = new Make(id, name);

            //Assert
            Assert.Equal(id, make.Id);
            Assert.Equal(make.Name, name);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Ctor_WithId_GivenNameNullOrEmpty_Throws(string name)
        {
            var e = Assert.Throws<ArgumentException>(() =>
            {
                var unused = new Make(33, name);
            });
            Assert.Equal("name", e.ParamName);
        }

        [Fact]
        public void AddModel()
        {
            //Arrange
            var model = _fixture.Create<Model>();
            var make = _fixture.Create<Make>();

            //Act
            make.AddModel(model);

            //Assert
            Assert.Contains(model, make.Models);
        }

        [Fact]
        public void AddModel_GivenModel_IsNull_Throws()
        {
            //Arrange
            var make = _fixture.Create<Make>();

            //Assert
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                make.AddModel(null as Model);
            });
            Assert.Equal("model", e.ParamName);
        }

        [Fact]
        public void AddModel_GivenModel_IsAlreadyAdded_Throws()
        {
            //Arrange
            var make = _fixture.Create<Make>();
            var modelA = new Model(1,"a", "a");
            var modelB = new Model(1, "b","b");
            make.AddModel(modelA);

            //Act
            var e = Assert.Throws<InvalidOperationException>(() =>
            {
                make.AddModel(modelB);
            });
            Assert.Contains("was already added", e.Message.ToLowerInvariant());
        }

        [Fact]
        public void AddModel_GivenModel_HasNoIdAssigned()
        {
            //Arrange
            var make = _fixture.Create<Make>();
            var modelA = new Model("a", "a");
            var modelB = new Model("a", "a");
            make.AddModel(modelA);

            //Act
            make.AddModel(modelB);

            //Assert
            Assert.Contains(modelA, make.Models);
            Assert.Contains(modelB, make.Models);
        }

        [Fact]
        public void AddModel_WithName()
        {
            //Arrange
            var name = _fixture.Create<string>();
            var code = _fixture.Create<string>();
            var make = _fixture.Create<Make>();

            //Act
            make.AddModel(code, name);

            //Assert
            Assert.Equal(name, make.Models.FirstOrDefault()?.Name);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void AddModel_GivenName_IsNullOrEmpty_Throws(string name)
        {
            //Arrange
            var code = _fixture.Create<string>();
            var make = _fixture.Create<Make>();

            //Assert
            var e = Assert.Throws<ArgumentException>(() =>
            {
                make.AddModel(code, name);
            });
            Assert.Contains("empty", e.Message);
            Assert.Equal("name", e.ParamName);
        }
    }
}
