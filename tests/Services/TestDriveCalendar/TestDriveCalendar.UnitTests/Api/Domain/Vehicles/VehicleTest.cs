﻿using AutoFixture;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Domain.Vehicles
{
    public class VehicleTest
    {
        private readonly Fixture _fixture = new Fixture();
        private readonly Vin _vin = Vin.Parse("12345678901234567");

        [Fact]
        public void Ctor()
        {
            //Arrange
            var modelId = _fixture.Create<int>();

            //Act 
            var vehicle = new Vehicle(_vin, modelId);

            //Assert
            Assert.Equal(modelId, vehicle.ModelId);
            Assert.Equal(_vin, vehicle.Vin);
            Assert.True(vehicle.Availability);
        }

        [Fact]
        public void Ctor_GivenModelId_IsZero_Throws()
        {
            var e = Assert.Throws<ArgumentException>(() =>
            {
                var unused = new Vehicle(_vin, 0);
            });
            Assert.Equal("modelId", e.ParamName);
            Assert.Contains("must be greater than 0", e.Message);
        }

        [Fact]
        public void Ctor_WithId()
        {
            //Arrange
            var modelId = _fixture.Create<int>();

            //Act 
            var vehicle = new Vehicle(11, _vin, modelId);

            //Assert
            Assert.Equal(11, vehicle.Id);
            Assert.Equal(modelId, vehicle.ModelId);
            Assert.Equal(_vin, vehicle.Vin);
            Assert.True(vehicle.Availability);
        }
    }
}
