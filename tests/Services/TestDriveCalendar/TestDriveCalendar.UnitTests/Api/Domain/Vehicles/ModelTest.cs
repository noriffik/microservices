﻿using AutoFixture;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Domain.Vehicles
{
    public class ModelTest
    {
        private readonly Fixture _fixture = new Fixture();
        private readonly string _code;
        private readonly string _name;

        public ModelTest()
        {
            _code = _fixture.Create<string>();
            _name = _fixture.Create<string>();
        }  

        [Fact]
        public void Ctor()
        {
            //Arrange
            var code = _fixture.Create<string>();
            var name = _fixture.Create<string>();

            //Act
            var model = new Model(code, name);

            //Assert
            Assert.Equal(0, model.Id);
            Assert.Equal(code, model.Code);
            Assert.Equal(name, model.Name);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Ctor_GivenName_IsNullOrEmpty_Throws(string name)
        {
            //Act
            var e = Assert.Throws<ArgumentException>(() => 
            {
                var unused = new Model(_code, name);
            });
            Assert.Equal("name", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Ctor_GivenCode_IsNullOrEmpty_Throws(string code)
        {
            //Act
            var e = Assert.Throws<ArgumentException>(() =>
            {
                var unused = new Model(code, _name);
            });
            Assert.Equal("code", e.ParamName);
        }

        [Fact]
        public void Ctor_WithId()
        {
            //Arrange
            var id = _fixture.Create<int>();
            var code = _fixture.Create<string>();
            var name = _fixture.Create<string>();

            //Act
            var model = new Model(id,code, name);

            //Assert
            Assert.Equal(id, model.Id);
            Assert.Equal(name, model.Name);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Ctor_WithId_GivenName_IsNullOrEmpty_Throws(string name)
        {
            //Act
            var e = Assert.Throws<ArgumentException>(() => 
            {
                var unused = new Model(33,"code", name);
            });
            Assert.Equal("name", e.ParamName);
        }
    }
}
