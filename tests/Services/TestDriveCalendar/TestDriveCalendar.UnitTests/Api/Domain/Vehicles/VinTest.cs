﻿using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Domain.Vehicles
{
    public class VinTest
    {
        const string Number = "12345678901234567";

        [Fact]
        public void Parse()
        {
            //Act
            var vin = Vin.Parse(Number);

            //Assert
            Assert.Equal(vin.Number, Number);
        }

        [Theory]
        [InlineData("xdx0000000xb0000a", "XDX0000000XB0000A")]
        public void Parse_GivenNumber_ContainsLowerCaseCharacters(string number, string expected)
        {
            //Act
            var vin = Vin.Parse(number);

            //Assert
            Assert.Equal(vin.Number, expected);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Parse_GivenNumber_IsNullOrEmpty_Throws(string number)
        {
            var e = Assert.Throws<ArgumentException>(() =>
            {
                Vin.Parse(number);
            });
            Assert.Equal("number", e.ParamName);
            Assert.Contains("null or empty", e.Message);
        }

        [Theory]
        [InlineData("XX")]
        [InlineData("00000000000000000X")]
        public void Parse_GivenNumber_IsNotRequiredLength_Throws(string number)
        {
            var e = Assert.Throws<ArgumentException>(() =>
            {
                Vin.Parse(number);
            });
            Assert.Equal("number", e.ParamName);
            Assert.Contains("characters long", e.Message.ToLowerInvariant());
        }

        [Theory]
        [InlineData("XXX!XX00000000000")]
        [InlineData("XXX[XX00000000000")]
        [InlineData("XXX/XX00000000000")]
        public void Parse_GivenNumber_ContainsInvalidCharacters_Throws(string number)
        {
            var e = Assert.Throws<ArgumentException>(() =>
            {
                Vin.Parse(number);
            });
            Assert.Equal("number", e.ParamName);
            Assert.Contains("must contain only alphanumeric characters", e.Message.ToLowerInvariant());
        }

        [Fact]
        public void TryParse()
        {
            //Act
            var result = Vin.TryParse(Number, out Vin vin);

            //Assert
            Assert.True(result);
            Assert.Equal(Number, vin.Number);
        }

        [Theory]
        [InlineData("xdx0000000xb0000a", "XDX0000000XB0000A")]
        public void TryParse_GivenNumber_ContainsLowerCaseCharacters(string number, string expected)
        {
            //Act
            var result = Vin.TryParse(number, out Vin vin);

            //Assert
            Assert.True(result);
            Assert.Equal(expected, vin.Number);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("XX")]
        [InlineData("00000000000000000X")]
        [InlineData("XXX!XX")]
        [InlineData("XXX[XX")]
        [InlineData("XXX/XX")]
        public void TryParse_GivenNumber_IsInvalid_ReturnsFalse(string number)
        {
            //Act
            var result = Vin.TryParse(number, out Vin vin);

            //Assert
            Assert.False(result);
            Assert.Null(vin);
        }
    }
}
