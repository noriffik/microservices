﻿using AutoFixture;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Domain.Vehicles
{
    public class FleetTest
    {
        private readonly DealerCode _dealerId = DealerCode.Parse("38001");
        private readonly Vin _vin = Vin.Parse("00000000000000001");
        private readonly Vin _otherVin = Vin.Parse("00000000000000002");

        private readonly Fixture _fixture = new Fixture();

        [Fact]
        public void Ctor()
        {
            //Act
            var fleet = new Fleet(_dealerId);

            //Assert
            Assert.Equal(fleet.DealerId, _dealerId);
            Assert.NotNull(fleet.Vehicles);
        }

        [Fact]
        public void Ctor_GivenDealerId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealerId", () => 
            {
                var unused = new Fleet(null);
            });
        }

        [Fact]
        public void Add()
        {
            //Arrange
            var fleet = new Fleet(_dealerId);
            var vehicle = new Vehicle(_vin, _fixture.Create<int>());

            //Act
            fleet.Add(_vin, vehicle.ModelId);

            //Assert
            Assert.Contains(vehicle, fleet.Vehicles, VehicleTestComparer.Instance);
        }

        [Fact]
        public void Add_GivenModelId_IsZero_Throws()
        {
            //Arrange
            var fleet = new Fleet(_dealerId);

            //Assert
            var e = Assert.Throws<ArgumentException>(() =>
            {
                fleet.Add(_vin, 0);
            });
            Assert.Equal("modelId", e.ParamName);
        }

        [Fact]
        public void Add_GivenVin_IsNull_Throws()
        {
            //Arrange
            var fleet = new Fleet(_dealerId);

            //Assert
            var e = Assert.Throws<ArgumentNullException>(() => 
            {
                fleet.Add(null, 1);
            });
            Assert.Equal("vin", e.ParamName);
        }

        [Fact]
        public void Add_GivenVin_IsForMemberVehicle_Throws()
        {
            //Arrange
            var fleet = new Fleet(_dealerId);
            fleet.Add(_vin, 1);
            fleet.Add(_otherVin, 1);

            //Assert
            var e = Assert.Throws<InvalidOperationException>(() =>
            {
                fleet.Add(_vin, 1);
            });
            Assert.Contains("already added with given vin", e.Message.ToLowerInvariant());
        }

        [Fact]
        public void IsMember()
        {
            //Arrange
            var fleet = new Fleet(_dealerId);
            fleet.Add(_vin, _fixture.Create<int>());
            fleet.Add(_otherVin, _fixture.Create<int>());
            
            //Assert
            Assert.True(fleet.IsMember(_vin));
            Assert.False(fleet.IsMember(Vin.Parse("12345678901234567")));
        }

        [Fact]
        public void IsMember_GivenVin_IsNull_Throws()
        {
            //Arrange
            var fleet = new Fleet(_dealerId);
            
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() => { fleet.IsMember(null); });
            Assert.Equal("vin", e.ParamName);
        }

        [Theory]
        [InlineData(true, 5, 15)]
        [InlineData(false, 3, 20)]
        public void ChangeAvailability(bool availability, int vehicleIndex, int vehicleCount)
        {
            //Arrange
            var fleet = new Fleet(_dealerId);
            Enumerable.Range(0, vehicleCount)
                .Select(n => new Vehicle(n + 1, Vin.Parse(n.ToString("D17")), _fixture.Create<int>())
                {
                    Availability = !availability
                })
                .ToList()
                .ForEach(v => fleet.Add(v));

            var vehicle = fleet.Vehicles[vehicleIndex];

            //Act
            fleet.ChangeAvailability(vehicle.Id, availability);

            //Arrange
            Assert.Equal(availability, vehicle.Availability);
            Assert.True(fleet.Vehicles.Where(v => v.Id != vehicle.Id).All(v => v.Availability != availability));
        }

        [Fact]
        public void ChangeAvailability_GivenVehicleId_IsInvalid_Throws()
        {
            //Arrange
            var fleet = new Fleet(_dealerId);

            //Assert
            var e = Assert.Throws<ArgumentException>(() => { fleet.ChangeAvailability(33, true); });
            Assert.Equal("vehicleId", e.ParamName);
            Assert.Contains("not found", e.Message.ToLowerInvariant());
        }
    }

    internal class VehicleTestComparer : IEqualityComparer<Vehicle>
    {
        public static readonly VehicleTestComparer Instance = new VehicleTestComparer();

        public bool Equals(Vehicle x, Vehicle y)
        {
            return x == y || (x != null && y != null && x.Id == y.Id && x.ModelId == y.ModelId && x.Vin == y.Vin);
        }

        public int GetHashCode(Vehicle obj)
        {
            throw new NotSupportedException();
        }
    }
}
