﻿using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Extensions;
using System;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Domain.Dealers
{
    public class DailyScheduleTest
    {
        private readonly DayOfWeek _weekDay = DayOfWeek.Monday;
        private readonly Time _since = new Time(36000);
        private readonly Time _till = new Time(72000);

        [Fact]
        public void Ctor()
        {
            //Act
            var dailySchedule = new DailySchedule(
                _weekDay,
                _since,
                _till);

            //Assert
            Assert.Equal(DayOfWeek.Monday, dailySchedule.WorkingDay);
            Assert.Equal(new WorkingHours(_since, _till), dailySchedule.WorkingHours);
        }
    }
}
