﻿using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Domain.Dealers
{
    public class ScheduleTest
    {
        public class DailyScheduleTestComparer : IEqualityComparer<DailySchedule>
        {
            public static readonly DailyScheduleTestComparer Instance = new DailyScheduleTestComparer();

            public bool Equals(DailySchedule x, DailySchedule y)
            {
                return x == y || (x != null && y != null &&
                                  x.WorkingDay == y.WorkingDay && x.WorkingHours == y.WorkingHours);
            }

            public int GetHashCode(DailySchedule obj) => throw new NotSupportedException();
        }

        private readonly Time _since = new Time(36000);
        private readonly Time _till = new Time(72000);

        public static IEnumerable<object[]> InvalidDateTimes =>
            new List<object[]>
            {
                new object[] { new DateTime(2018, 11, 5, 10, 0, 0), new DateTime(2018, 11, 5, 19, 0, 0) },
                new object[] { new DateTime(2018, 11, 5, 7, 0, 0), new DateTime(2018, 11, 5, 17, 0, 0) },
                new object[] { new DateTime(2018, 11, 5, 7, 0, 0), new DateTime(2018, 11, 5, 20, 0, 0) }
            };

        [Fact]
        public void Ctor()
        {
            //Arrange
            var dailySchedules = Enumerable.Range(0, 7)
                .Select(d => new DailySchedule((DayOfWeek) d, WorkingHours.Typical))
                .ToList();

            //Act
            var weeklySchedule = new Schedule(dailySchedules);

            //Assert

            Assert.Equal(weeklySchedule.WeekSchedule, dailySchedules, DailyScheduleTestComparer.Instance);
        }

        [Fact]
        public void Ctor_WithGiven_DuplicateDay_Throws()
        {
            //Arrange
            var dailySchedules = new List<DailySchedule>
            {
                new DailySchedule(DayOfWeek.Monday, _since, _till),
                new DailySchedule(DayOfWeek.Monday, _since, _till),
                new DailySchedule(DayOfWeek.Monday, _since, _till)
            };
            
            //Assert
            var e = Assert.Throws<InvalidOperationException>(() =>
            {
                var unused = new Schedule(dailySchedules);
            });
            Assert.Contains("duplicate", e.Message.ToLowerInvariant());
        }

        [Theory]
        [MemberData(nameof(InvalidDateTimes))]
        public void WithDateTimeInterval_IsOutOfDailyScheduleRange_ReturnsFalse(DateTime since, DateTime till)
        {
            //Act
            var result = Schedule.Typical.IsIn(since, till);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void WithDateTimeInterval_IsInDailyScheduleRange_ReturnsTrue()
        {
            //Act
            var result = Schedule.Typical.IsIn(
                new DateTime(2018, 11, 5, 10, 0, 0), new DateTime(2018, 11, 5, 12, 0, 0));

            //Assert
            Assert.True(result);
        }
    }
}
