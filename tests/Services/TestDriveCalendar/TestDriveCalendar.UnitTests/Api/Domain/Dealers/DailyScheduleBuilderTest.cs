﻿using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Extensions;
using System;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Domain.Dealers
{
    public class DailyScheduleBuilderTest
    {
        [Fact]
        public void Build()
        {
            Assert.NotNull(DailySchedule.Build());
        }

        [Fact]
        public void Create()
        {
            //Arrange
            var builder = DailySchedule.Build()
                .WithDay(DayOfWeek.Monday)
                .WithTime(new Time(64587), new Time(78546));

            //Act
            var dailySchedule = builder.Create();

            //Assert
            Assert.NotNull(dailySchedule);
            Assert.Equal(DayOfWeek.Monday, dailySchedule.WorkingDay);
            Assert.Equal(new WorkingHours(new Time(64587), new Time(78546)), dailySchedule.WorkingHours);
        }

        [Fact]
        public void Create_WithoutConfiguration()
        {
            //Act
            var dailySchedule = DailySchedule.Build().Create();

            //Assert
            Assert.NotNull(dailySchedule);
            Assert.Null(dailySchedule.WorkingDay);
            Assert.Equal(WorkingHours.Empty, dailySchedule.WorkingHours);
        }
    }
}
