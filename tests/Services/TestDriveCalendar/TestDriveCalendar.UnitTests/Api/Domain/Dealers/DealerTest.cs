﻿using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Domain.Dealers
{
    public class DealerTest
    {
        [Fact]
        public void Ctor()
        {
            //Arrange
            var id = DealerCode.Parse("38001");
            
            //Act
            var dealer = new Dealer(id, Schedule.Typical, 15);

            //Assert
            Assert.Equal(id, dealer.Id);
            Assert.Equal(Schedule.Typical, dealer.Schedule);
            Assert.Equal(15, dealer.TestDriveDuration);
        }
    }
}
