﻿using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Extensions;
using System;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Domain.Dealers
{
    public class WorkingHoursTest
    {
        private readonly Time _since = new Time(45678);
        private readonly Time _till = new Time(79456);

        [Fact]
        public void Ctor()
        {
            //Act
            var workingHours = new WorkingHours(_since, _till);

            //Assert
            Assert.Equal(_since, workingHours.Since);
            Assert.Equal(_till, workingHours.Till);
        }

        [Fact]
        public void Ctor_GivenTillValue_LessThan_SinceValue_Throws()
        {
            var e = Assert.Throws<InvalidOperationException>(() =>
            {
                var unused = new WorkingHours(_since, new Time(0));
            });
            Assert.Contains("less that till", e.Message);
        }

        [Fact]
        public void Ctor_Given_Till_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new WorkingHours(_since, null);
            });

            //Assert
            Assert.Contains("till", e.Message);
        }

        [Fact]
        public void Ctor_Given_Since_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new WorkingHours(null, _since);
            });

            //Assert
            Assert.Contains("since", e.Message);
        }

        [Fact]
        public void Empty_ReturnsEmptyWorkingHours()
        {
            //Act
            var empty = WorkingHours.Empty;

            //Assert
            Assert.NotNull(empty);
            Assert.Null(empty.Since);
            Assert.Null(empty.Till);
            Assert.Equal(empty, WorkingHours.Empty);
        }
    }
}
