﻿using AutoFixture;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Domain.TestDrives
{
    public class TestDriveTest
    {
        private readonly Fixture _fixture = new Fixture();
        private readonly DateTime _since;
        private readonly DateTime _till;
        private readonly TestDrive _testDrive;

        public TestDriveTest()
        {
            _since = new DateTime(2018, 11, 19, 12, 0, 0);
            _till = new DateTime(2018, 11, 19, 14, 0, 0);
            _testDrive = new TestDrive(1, 1, 5, _since, _till, 1);
        }
         
        [Fact]
        public void Ctor()
        {
            //Arrange
            var statusChange = new StatusChange(Status.Scheduled, 22, 1);

            //Act 
            var testDrive = new TestDrive(1, 5, _since, _till, 22);

            //Assert
            Assert.Equal(1, testDrive.ClientId);
            Assert.Equal(22, testDrive.SalesManagerId);
            Assert.Equal(_since, testDrive.Since);
            Assert.Equal(_till, testDrive.Till);
            Assert.Equal(5, testDrive.VehicleId);
            Assert.Equal(statusChange.Status, testDrive.Status);
            Assert.Equal(statusChange, testDrive.StatusChanges.SingleOrDefault(), StatusChangeComparer.Instance);
        }

        [Fact]
        public void Begin()
        {
            //Arrange
            var statusChange = new StatusChange(Status.Begun, 22, 2);

            //Act
            _testDrive.Begin(22);

            //Assert
            Assert.Equal(statusChange.Status, _testDrive.Status);
            Assert.Equal(statusChange, _testDrive.StatusChanges.FirstOrDefault(), StatusChangeComparer.Instance);
        }

        [Fact]
        public void Reschedule()
        {
            //Arrange
            var statusChange = new StatusChange(Status.Rescheduled, 5, 2);

            //Act
            _testDrive.Reschedule(5, _since, _till);

            //Assert
            Assert.Equal(statusChange.Status, _testDrive.Status);
            Assert.Equal(statusChange, _testDrive.StatusChanges.FirstOrDefault(), StatusChangeComparer.Instance);
        }

        [Fact]
        public void Reschedule_WhenDate_IsOutOfRange_Throws()
        {
            //Arrange
            var statusChange = new StatusChange(Status.Rescheduled, 5, 2);

            //Act
            var e = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                _testDrive.Reschedule(5, _till, _since);
            });

            //Assert
            Assert.Contains("Since must be less then till", e.Message);
        }
       

        [Fact]
        public void Begin_WhenCurrentStatus_DoesNotAllow_Throws()
        {
            //Arrange
            _testDrive.Begin(1);

            //Assert
            var e = Assert.Throws<InvalidStatusChangeException>(() =>
            {
                _testDrive.Begin(1);
            });
            Assert.Contains("Attempt to assign invalid status", e.Message);
        }

        [Fact]
        public void Complete()
        {
            //Arrange
            var statusChange = new StatusChange(Status.Completed, 22, 3);
            _testDrive.Begin(22);

            //Act
            _testDrive.Complete(22);

            //
            Assert.Equal(statusChange.Status, _testDrive.Status);
            Assert.Equal(statusChange, _testDrive.StatusChanges.FirstOrDefault(), StatusChangeComparer.Instance);
        }

        [Fact]
        public void Complete_WhenCurrentStatus_DoesNotAllow_Throws()
        {
            //Assert
            var e = Assert.Throws<InvalidStatusChangeException>(() =>
            {
                _testDrive.Complete(1);
            });
            Assert.Contains("Attempt to assign invalid status", e.Message);
        }

        [Fact]
        public void Cancel()
        {
            //Arrange
            var statusChange = new StatusChange(Status.Cancelled, 22, 3);
            _testDrive.Begin(22);

            //Act
            _testDrive.Cancel(22);

            //
            Assert.Equal(statusChange.Status, _testDrive.Status);
            Assert.Equal(statusChange, _testDrive.StatusChanges.FirstOrDefault(), StatusChangeComparer.Instance);
        }

        [Fact]
        public void Cancel_WhenCurrentStatus_DoesNotAllow_Throws()
        {
            _testDrive.Cancel(1);
            //Assert
            var e = Assert.Throws<InvalidStatusChangeException>(() =>
            {
                _testDrive.Cancel(1);
            });
            Assert.Contains("Attempt to assign invalid status", e.Message);
        }


        public class StatusChangeComparer : IEqualityComparer<StatusChange>
        {
            public static readonly StatusChangeComparer Instance = new StatusChangeComparer();

            public bool Equals(StatusChange x, StatusChange y)
            {
                return x == y || (x != null && y != null &&
                                  x.Status == y.Status &&
                                  x.Version == y.Version &&
                                  x.AuthorId == y.AuthorId);
            }

            public int GetHashCode(StatusChange obj)
            {
                throw new NotSupportedException();
            }
        }
    }
}
