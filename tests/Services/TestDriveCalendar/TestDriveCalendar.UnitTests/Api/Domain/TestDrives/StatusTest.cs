﻿using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Domain.TestDrives
{
    public class StatusTest
    {
        private static readonly List<Status> AllStatuses = new List<Status>
        {
            Status.Scheduled,
            Status.Rescheduled,
            Status.Begun,
            Status.Completed,
            Status.Cancelled
        };

        [Fact]
        public void All()
        {
            Assert.Equal(AllStatuses, Status.All);
        }

        [Theory]
        [MemberData(nameof(AllStatusesData))]
        public void Get(Status expected)
        {
            Assert.Equal(expected, Status.Get(expected.StatusId));
        }

        public static IEnumerable<object[]> AllStatusesData => AllStatuses.Select(s => new object[] { s });

        [Fact]
        public void Get_GivenId_IsInvalid_Throws()
        {
            var e = Assert.Throws<ArgumentException>(() => { Status.Get(11); });
            Assert.Equal("id", e.ParamName);
        }

        [Theory]
        [MemberData(nameof(AllStatusesData))]
        public void TryGet(Status expected)
        {
            var result = Status.TryGet(expected.StatusId, out var status);

            Assert.True(result);
            Assert.Equal(expected, status);
        }

        [Fact]
        public void TryGet_GivenId_IsInvalid_ReturnsFalse()
        {
            var result = Status.TryGet(124, out var status);

            Assert.False(result);
            Assert.Null(status);
        }

        [Theory]
        [MemberData(nameof(ChangeStatusData))]
        public void IsChangeInvalid_GivenStatus_IsValid_ReturnsFalse(Status from, Status to)
        {
            Assert.False(from.IsChangeInvalid(to));
        }
        
        public static IEnumerable<object[]> ChangeStatusData => new List<object[]>
        {
            new object[] { Status.Scheduled, Status.Begun },
            new object[] { Status.Scheduled, Status.Cancelled },
            new object[] { Status.Scheduled, Status.Rescheduled },
            new object[] { Status.Rescheduled, Status.Begun },
            new object[] { Status.Rescheduled, Status.Cancelled },
            new object[] { Status.Rescheduled, Status.Rescheduled },
            new object[] { Status.Begun, Status.Rescheduled },
            new object[] { Status.Begun, Status.Cancelled },
            new object[] { Status.Begun, Status.Rescheduled },
            new object[] { Status.Cancelled, Status.Rescheduled }
        };

        [Theory]
        [MemberData(nameof(Statuses))]
        public void IsChangeInvalid_GivenStatus_IsNull_Throws(Status status)
        {
            var e = Assert.Throws<ArgumentNullException>(() => { status.IsChangeInvalid(null); });
            Assert.Equal("status", e.ParamName);
        }

        public static IEnumerable<object[]> Statuses => Status.All.Select(s => new object[] {s});

        [Theory]
        [MemberData(nameof(ChangeStatusInvalidData))]
        public void IsChangeInvalid_GivenStatus_IsInvalid_ReturnsTrue(Status from, Status to)
        {
            Assert.True(from.IsChangeInvalid(to));
        }

        public static IEnumerable<object[]> ChangeStatusInvalidData => new List<object[]>
        {
            new object[] { Status.Scheduled, Status.Scheduled },
            new object[] { Status.Scheduled, Status.Completed },
            new object[] { Status.Rescheduled, Status.Scheduled },
            new object[] { Status.Rescheduled, Status.Completed },
            new object[] { Status.Begun, Status.Begun },
            new object[] { Status.Begun, Status.Scheduled },
            new object[] { Status.Completed, Status.Scheduled },
            new object[] { Status.Completed, Status.Rescheduled },
            new object[] { Status.Completed, Status.Begun },
            new object[] { Status.Completed, Status.Completed },
            new object[] { Status.Completed, Status.Cancelled },
            new object[] { Status.Cancelled, Status.Scheduled },
            new object[] { Status.Cancelled, Status.Begun },
            new object[] { Status.Cancelled, Status.Completed },
            new object[] { Status.Cancelled, Status.Cancelled }
        };
    }
}
