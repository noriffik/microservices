﻿using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using System;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Domain.TestDrives
{
    public class StatusChangeTest
    {
        [Fact]
        public void Ctor()
        {
            //Arrange
            var authorId = 1;
            var version = 2;

            //Act
            var result = new StatusChange(Status.Rescheduled, authorId, version);

            //Assert
            Assert.Equal(result.Status, Status.Rescheduled);
            Assert.Equal(result.AuthorId, authorId);
            Assert.Equal(result.Version, version);
        }
        
        [Fact]
        public void Ctor_GivenStatus_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentException>(() =>
            {
                var unused = new StatusChange(null, 1, 1);
            });
            Assert.Contains("status", e.Message);
        }
    }

}
