﻿using NexCore.TestDriveCalendar.Api.Domain.Extensions;
using System;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Domain.Extensions
{
    public class TimeTest
    {
        [Theory]
        [InlineData(22545, 6, 15, 45)]
        [InlineData(80231, 22, 17, 11)]
        [InlineData(80220, 22, 17, 0)]
        [InlineData(79202, 22, 0, 2)]
        [InlineData(1074, 0, 17, 54)]
        [InlineData(0, 0, 0, 0)]
        public void Ctor_With_Seconds(int total, int hours, int minutes, int seconds)
        {
            //Act
            var time = new Time(total);

            //Assert
            Assert.Equal(hours, time.Hours);
            Assert.Equal(minutes, time.Minutes);
            Assert.Equal(seconds, time.Seconds);
        }

        [Theory]
        [InlineData(6, 15, 45)]
        [InlineData(22, 17, 11)]
        [InlineData(14, 0, 11)]
        [InlineData(22, 17, 0)]
        [InlineData(0, 15, 45)]
        [InlineData(0, 0, 0)]
        public void Ctor(int hours, int minutes, int seconds)
        {
            //Act
            var time = new Time(hours, minutes, seconds);

            //Assert
            Assert.Equal(hours, time.Hours);
            Assert.Equal(minutes, time.Minutes);
            Assert.Equal(seconds, time.Seconds);
        }

        [Theory]
        [InlineData(24, 15, 45)]
        [InlineData(-5, 0, -1)]
        public void Ctor_GivenHours_IsOutOfRange_Throws(int hours, int minutes, int seconds)
        {
            //Assert
            var e = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var time = new Time(hours, minutes, seconds);
            });
            Assert.Contains(nameof(hours), e.Message);
        }

        [Theory]
        [InlineData(6, 60, 59)]
        [InlineData(12, -1, 59)]
        public void Ctor_GivenMinutes_IsOutOfRange_Throws(int hours, int minutes, int seconds)
        {
            //Assert
            var e = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var time = new Time(hours, minutes, seconds);
            });
            Assert.Contains(nameof(minutes), e.Message);
        }

        [Theory]
        [InlineData(6, 15, 60)]
        [InlineData(12, 0, -1)]
        public void Ctor_GivenSeconds_IsOutOfRange_Throws(int hours, int minutes, int seconds)
        {
            //Assert
            var e = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var time = new Time(hours, minutes, seconds);
            });
            Assert.Contains(nameof(seconds), e.Message);
        }

        [Theory]
        [InlineData(86400)]
        [InlineData(-5000)]
        public void Ctor_WithSeconds_GivenSeconds_IsOutOfRange_Throws(int timestamp)
        {
            //Assert
            var e = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var time = new Time(timestamp);
            });
            Assert.Contains(nameof(timestamp), e.Message);
        }

        [Fact]
        public void LessComparison_WithNull_Success()
        {
            //Assert
            var result = new Time(9, 0, 0) < null;
            
            //Arrange
            Assert.False(result);
        }

        [Fact]
        public void GreaterComparison_WithNull_Success()
        {
            //Assert
            var result = new Time(9, 0, 0) > null;
            
            //Arrange
            Assert.True(result);
        }

        [Fact]
        public void LessThanOrEqualComparison_WithNull_Success()
        {
            //Assert
            var result = new Time(9, 0, 0) <= null;
            
            //Arrange
            Assert.False(result);
        }

        [Fact]
        public void GreaterThanOrEqualComparison_WithNull_Success()
        {
            //Assert
            var result = new Time(9, 0, 0) >= null;
            
            //Arrange
            Assert.True(result);
        }
    }
}
