﻿using System;
using NexCore.TestDriveCalendar.Api.Domain.Notes;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Domain.Notes
{
    public class NoteTest
    {
        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Ctor_GivenContentIsNullOrEmpty_Throws(string content)
        {  
            var e = Assert.Throws<ArgumentException>(() =>
            {
                var unused = new Note(1, 1, content);
            });

            Assert.Equal("content", e.ParamName);
            Assert.Contains("content must", e.Message.ToLowerInvariant());
        }
    }
}
