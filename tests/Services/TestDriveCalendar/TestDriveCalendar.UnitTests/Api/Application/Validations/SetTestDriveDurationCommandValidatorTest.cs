﻿using FluentValidation.TestHelper;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Application.Validations;
using System;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Validations
{
    public class SetTestDriveDurationCommandValidatorTest
    {
        private readonly SetTestDriveDurationCommandValidator _validator = new SetTestDriveDurationCommandValidator();

        private readonly RescheduleTestDriveCommand _sinceIsLessThanTillCommand =
            new RescheduleTestDriveCommand
            {
                Since = new DateTime(2018, 10, 5, 10, 0, 0),
                Till = new DateTime(2018, 10, 5, 12, 0, 0)
            };

        private readonly RescheduleTestDriveCommand _sinceIsGreaterThanTillCommand =
            new RescheduleTestDriveCommand
            {
                Since = new DateTime(2018, 10, 5, 16, 0, 0),
                Till = new DateTime(2018, 10, 5, 12, 0, 0)
            };

        [Fact]
        public void ShouldHaveError_WhenTestDriveId_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.TestDriveId, default(int));
        }

        [Fact]
        public void ShouldNotHaveError_WhenTestDriveId_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.TestDriveId, 9);
        }

        
        [Fact]
        public void ShouldHaveError_WhenSince_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Since, default(DateTime));
        }

        [Fact]
        public void ShouldHaveError_WhenTill_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Till, default(DateTime));
        }

        [Fact]
        public void ShouldNotHaveError_WhenSince_IsLessThanTill()
        {
            _validator.ShouldNotHaveValidationErrorFor(wh => wh.Since, _sinceIsLessThanTillCommand);
        }

        [Fact]
        public void ShouldHaveError_WhenSince_IsGreaterThanTill()
        {
            _validator.ShouldHaveValidationErrorFor(wh => wh.Since, _sinceIsGreaterThanTillCommand);
        }

        [Fact]
        public void ShouldHaveError_WhenSalesManagerId_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.SalesManagerId, default(int));
        }

        [Fact]
        public void ShouldNotHaveError_WhenSalesManagerId_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.SalesManagerId, 9);
        }
    }
}
