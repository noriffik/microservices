﻿using FluentValidation.TestHelper;
using NexCore.TestDriveCalendar.Api.Application.Validations;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Validations
{
    public class TestDriveStatusCommandValidatorTest
    {
        private readonly TestDriveStatusCommandValidator _validator = new TestDriveStatusCommandValidator();

        [Fact]
        public void ShouldHaveError_WhenTestDriveId_IsZero()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.TestDriveId, default(int));
        }

        [Fact]
        public void ShouldHaveError_WhenManagerId_IsZero()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.SalesManagerId, default(int));
        }
    }
}
