﻿using FluentValidation.TestHelper;
using NexCore.TestDriveCalendar.Api.Application.Validations;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Validations
{
    public class CreateNoteCommandValidatorTest
    {
        private readonly CreateNoteCommandValidator _validator = new CreateNoteCommandValidator();

        [Fact]
        public void ShouldHaveError_WhenTestDriveEqualZero()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.TestDriveId, default(int));
        }

        [Fact]
        public void ShouldHaveError_WhenContentZero()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Content, "");
        }

        [Fact]
        public void ShouldHaveError_WhenManagerIdEqualZero()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ManagerId, default(int));
        }
    }
}
