﻿using FluentValidation.TestHelper;
using NexCore.TestDriveCalendar.Api.Application.Validations;
using NexCore.TestDriveCalendar.Api.Models;
using System;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Validations
{
    public class TestDriveByDealerDateItemValidatorTest
    {
        private readonly TestDriveByDealerDateItemValidator _validator = new TestDriveByDealerDateItemValidator();

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void ShouldHaveError_WhenDealerId_IsInvalid(string dealerId)
        {
            _validator.ShouldHaveValidationErrorFor(i => i.DealerId, new TestDriveByDealerDateItem{DealerId = dealerId, Date = new DateTime(2018, 11, 10)});
        }

        [Fact]
        public void ShouldNotHaveError_WhenDealerId_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(i => i.DealerId, new TestDriveByDealerDateItem{DealerId = "38001", Date = new DateTime(2018, 11, 10)});
        }

        [Fact]
        public void ShouldNotHaveError_WhenDate_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(i => i.Date, new TestDriveByDealerDateItem{DealerId = "38001", Date = new DateTime(2018, 11, 10)});
        }

    }
}
