﻿using FluentValidation.TestHelper;
using FluentValidation.Validators.UnitTestExtension.Composer;
using FluentValidation.Validators.UnitTestExtension.Core;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Application.Models;
using NexCore.TestDriveCalendar.Api.Application.Validations;
using System;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Validations
{
    public class CreateTestDriveCommandValidatorTest
    {
        private readonly CreateTestDriveCommandValidator _validator = new CreateTestDriveCommandValidator();
        
        private readonly CreateTestDriveCommand _sinceIsLessThanTillCommand =
            new CreateTestDriveCommand
            {
                Since = new DateTime(2018, 10, 5, 10, 0, 0),
                Till = new DateTime(2018, 10, 5, 12, 0, 0)
            };

        private readonly CreateTestDriveCommand _sinceIsGreaterThanTillCommand =
            new CreateTestDriveCommand
            {
                Since = new DateTime(2018, 10, 5, 16, 0, 0),
                Till = new DateTime(2018, 10, 5, 12, 0, 0)
            };
        
        [Fact]
        public void ShouldNotHaveError_WhenClientId_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.ClientId, 1);
        }

        [Fact]
        public void ShouldHaveError_WhenClientId_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.ClientId, default(int));
        }

        [Fact]
        public void ShouldHaveError_WhenVehicleId_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.VehicleId, default(int));
        }

        [Fact]
        public void ShouldNotHaveError_WhenVehicleId_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.VehicleId, 9);
        }

        [Fact]
        public void ShouldHaveError_WhenSince_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Since, default(DateTime));
        }

        [Fact]
        public void ShouldHaveError_WhenTill_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Till, default(DateTime));
        }

        [Fact]
        public void ShouldNotHaveError_WhenSince_IsLessThanTill()
        {
            _validator.ShouldNotHaveValidationErrorFor(wh => wh.Since, _sinceIsLessThanTillCommand);
        }

        [Fact]
        public void ShouldHaveError_WhenSince_IsGreaterThanTill()
        {
            _validator.ShouldHaveValidationErrorFor(wh => wh.Since, _sinceIsGreaterThanTillCommand);
        }

        [Fact]
        public void ShouldHaveError_WhenSalesManagerId_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.SalesManagerId, default(int));
        }

        [Fact]
        public void ShouldNotHaveError_WhenSalesManagerId_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.SalesManagerId, 9);
        }

    }
}
