﻿using FluentValidation.TestHelper;
using FluentValidation.Validators.UnitTestExtension.Composer;
using FluentValidation.Validators.UnitTestExtension.Core;
using NexCore.TestDriveCalendar.Api.Application.Models;
using NexCore.TestDriveCalendar.Api.Application.Validations;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Validations
{
    public class DailyScheduleValidatorTest
    {
        private readonly DailyScheduleValidator _validator = new DailyScheduleValidator();

        [Theory]
        [InlineData(-1)]
        [InlineData(7)]
        [InlineData(-128000)]
        [InlineData(128000)]
        public void ShouldHaveError_WhenWorkingDay_IsInvalid(int workingDay)
        {
            _validator.ShouldHaveValidationErrorFor(ds => ds.WorkingDay, new DailyScheduleModel{WorkingDay = workingDay});
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        [InlineData(5)]
        [InlineData(6)]
        public void ShouldNotHaveError_WhenWorkingDay_IsValid(int workingDay)
        {
            _validator.ShouldNotHaveValidationErrorFor(ds => ds.WorkingDay, new DailyScheduleModel{WorkingDay = workingDay});
        }

        [Fact]
        public void ShouldHaveRules_ForWorkingHours()
        {
            _validator.ShouldHaveRules(
                ds => ds.WorkingHours,
                BaseVerifiersSetComposer.Build()
                    .AddChildValidatorVerifier<WorkingHoursValidator>()
                    .Create());
        }
    }
}
