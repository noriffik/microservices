﻿using FluentValidation.TestHelper;
using NexCore.TestDriveCalendar.Api.Application.Validations;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Validations
{
    public class UpdateNoteCommandValidatorTest
    {
        private readonly UpdateNoteCommandValidator _validator = new UpdateNoteCommandValidator();

        [Fact]
        public void ShouldHaveError_WhenNoteIdEqualsZero()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.NoteId, default(int));
        }

        [Fact]
        public void ShouldHaveError_WhenManagerIdEqualsZero()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ManagerId, default(int));
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void ShouldHaveError_WhenContentIsEmpty(string content)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Content, content);
        }
    }
}
