﻿using FluentValidation.TestHelper;
using NexCore.TestDriveCalendar.Api.Application.Validations;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Validations
{
    public class RemoveNoteCommandValidatorTest
    {
        private readonly RemoveNoteCommandValidator _validator = new RemoveNoteCommandValidator();

        [Fact]
        public void ShouldHaveError_WhenNoteIdEqualsZero()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.NoteId, default(int));
        }

        [Fact]
        public void ShouldHaveError_WhenManagerIdEqualsZero()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ManagerId, default(int));
        }
    }
}
