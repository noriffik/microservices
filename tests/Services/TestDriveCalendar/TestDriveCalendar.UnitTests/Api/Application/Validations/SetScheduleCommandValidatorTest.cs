﻿using FluentValidation.TestHelper;
using FluentValidation.Validators.UnitTestExtension.Composer;
using FluentValidation.Validators.UnitTestExtension.Core;
using NexCore.TestDriveCalendar.Api.Application.Models;
using NexCore.TestDriveCalendar.Api.Application.Validations;
using System.Collections.Generic;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Validations
{
    public class SetScheduleCommandValidatorTest
    {
        private readonly SetScheduleCommandValidator _validator = new SetScheduleCommandValidator();

        [Fact]
        public void ShouldHaveError_WhenDealerId_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(m => m.DealerId, default(string));
        }

        [Fact]
        public void ShouldNotHaveError_WhenDealerId_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.DealerId, "38001");
        }

        [Fact]
        public void ShouldNotHaveError_WhenWeeklySchedule_IsSet()
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.WeeklySchedule, new List<DailyScheduleModel>());
        }

        [Fact]
        public void ShouldHaveRules_ForWeeklySchedule()
        {
            _validator.ShouldHaveRules(
                s => s.WeeklySchedule,
                BaseVerifiersSetComposer.Build()
                    .AddChildValidatorVerifier<DailyScheduleValidator>()
                    .Create());
        }
    }
}
