﻿using FluentValidation.TestHelper;
using NexCore.TestDriveCalendar.Api.Application.Validations;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Validations
{
    public class VehicleItemValidationTest
    {
        private readonly  VehicleItemValidator _validator = new VehicleItemValidator();

        [Fact]
        public void ShouldHaveError_When_ModelId_IsZero()
        {
            _validator.ShouldHaveValidationErrorFor(i => i.ModelId, 0);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("qweqh3i1213123asdasdasdeqwe")]
        [InlineData("qweqh3i1213e")]
        public void ShouldHaveError_When_Vin_HaveWrongValueOrEmpty(string vin)
        {
            _validator.ShouldHaveValidationErrorFor(i => i.Vin, vin);
        }

        [Fact]
        public void ShouldHaveError_When_DealerId_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(i => i.DealerId, default(string));
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void ShouldHaveError_When_Equipment_NullOrEmpty(string equipment)
        {
            _validator.ShouldHaveValidationErrorFor(i => i.Equipment, equipment);
        }
    }
}
