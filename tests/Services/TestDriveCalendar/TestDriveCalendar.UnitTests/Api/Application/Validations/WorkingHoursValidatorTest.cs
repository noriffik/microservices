﻿using FluentValidation.TestHelper;
using NexCore.TestDriveCalendar.Api.Application.Models;
using NexCore.TestDriveCalendar.Api.Application.Validations;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Validations
{
    public class WorkingHoursValidatorTest
    {
        private readonly WorkingHoursValidator _validator = new WorkingHoursValidator();

        [Theory]
        [InlineData(-1)]
        [InlineData(-86400)]
        [InlineData(86400)]
        [InlineData(128000)]
        public void ShouldHaveError_WhenSince_IsInvalid(int since)
        {
            _validator.ShouldHaveValidationErrorFor(wh => wh.Since, new WorkingHoursModel{Since = since, Till = 86399});
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(86398)]
        public void ShouldNotHaveError_WhenSince_IsValid(int since)
        {
            _validator.ShouldNotHaveValidationErrorFor(wh => wh.Since, new WorkingHoursModel{Since = since, Till = 86399});
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(-86400)]
        [InlineData(86400)]
        [InlineData(128000)]
        public void ShouldHaveError_WhenTill_IsInvalid(int till)
        {
            _validator.ShouldHaveValidationErrorFor(wh => wh.Till, new WorkingHoursModel{Since = 0, Till = till});
        }

        [Theory]
        [InlineData(1)]
        [InlineData(86399)]
        public void ShouldNotHaveError_WhenTill_IsValid(int till)
        {
            _validator.ShouldNotHaveValidationErrorFor(wh => wh.Till, new WorkingHoursModel{Since = 0, Till = till});
        }

        [Fact]
        public void ShouldNotHaveError_WhenSince_IsLessThanTill()
        {
            _validator.ShouldNotHaveValidationErrorFor(wh => wh.Since, new WorkingHoursModel{Since = 2000, Till = 5000});
        }

        [Fact]
        public void ShouldHaveError_WhenSince_IsGreaterThanTill()
        {
            _validator.ShouldHaveValidationErrorFor(wh => wh.Since, new WorkingHoursModel{Since = 5000, Till = 2000});
        }

        [Fact]
        public void ShouldHaveError_WhenTill_IsLessThanSince()
        {
            _validator.ShouldHaveValidationErrorFor(wh => wh.Till, new WorkingHoursModel{Since = 5000, Till = 2000});
        }

        [Fact]
        public void ShouldNotHaveError_WhenTill_IsGreaterThanSince()
        {
            _validator.ShouldNotHaveValidationErrorFor(wh => wh.Till, new WorkingHoursModel{Since = 2000, Till = 5000});
        }

        
    }
}
