﻿using FluentValidation.TestHelper;
using NexCore.TestDriveCalendar.Api.Application.Validations;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Validations
{
    public class UpdateVehicleCommandValidatorTest
    {
        private readonly UpdateVehicleCommandValidator _validator = new UpdateVehicleCommandValidator();

        [Fact]
        public void ShouldHaveError_WhenVehicleIdEqualsZero()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.VehicleId, default(int));
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void ShouldHaveError_WhenEquipmentIsEmpty(string content)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Equipment, content);
        }
    }
}
