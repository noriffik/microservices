﻿using AutoFixture;
using FluentValidation;
using Moq;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Commands
{
    public class UpdateVehicleCommandHandlerTest
    {
        private static readonly Fixture Fixture = new Fixture();
        private readonly Mock<IFleetRepository> _fleetRepository = new Mock<IFleetRepository>();
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly UpdateVehicleCommandHandler _handler;
        private readonly Vehicle _vehicle;
        private readonly UpdateVehicleCommand _command;

        public UpdateVehicleCommandHandlerTest()
        {
            _unitOfWork.Setup(w => w.Commit(It.IsAny<CancellationToken>())).Returns(Task.CompletedTask);
            _fleetRepository.Setup(r => r.UnitOfWork).Returns(_unitOfWork.Object);
            _handler = new UpdateVehicleCommandHandler(_fleetRepository.Object);
            _vehicle = new Vehicle(1, Vin.Parse("12345678901234567"), 1);
            _fleetRepository.Setup(r => r.FindVehicleByIdAsync(_vehicle.Id)).ReturnsAsync(_vehicle);
            _command = Fixture.Build<UpdateVehicleCommand>().With(c => c.VehicleId, _vehicle.Id).Create();
        }

        [Fact]
        public void Ctor_GivenFleetRepository_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new UpdateVehicleCommandHandler(null);
            });
            Assert.Equal("fleetRepository", e.ParamName);
        }

        [Fact]
        public async Task Handle_GivenCommand_IsNull_Throws()
        {
            //Assert
            var e = await Assert.ThrowsAsync<ArgumentNullException>(() => _handler.Handle(null, CancellationToken.None));
            Assert.Equal("command", e.ParamName);
        }

        [Fact]
        public async Task Handle()
        {
            //Act
            var response = await _handler.Handle(_command, CancellationToken.None);

            //Assert
            Assert.True(response);
            Assert.Equal(_command.VehicleId, _vehicle.Id);
            Assert.Equal(_command.Comments, _vehicle.Comments);
            Assert.Equal(_command.Engine, _vehicle.Engine);
            Assert.Equal(_command.Equipment, _vehicle.Equipment);
            Assert.Equal(_command.GearBox, _vehicle.GearBox);
            Assert.Equal(_command.Options, _vehicle.Options);
            Assert.Equal(_command.Type, _vehicle.Type);
            _unitOfWork.Verify(w => w.Commit(It.IsAny<CancellationToken>()));
        }

        [Fact]
        public async Task Handle_WhenVehicle_IsNotFound_Throws()
        {
            //Arrange
            _fleetRepository.Setup(r => r.FindVehicleByIdAsync(_command.VehicleId)).ReturnsAsync(null as Vehicle);

            //Assert
            var e = await Assert.ThrowsAsync<ValidationException>(() => _handler.Handle(_command, CancellationToken.None));
            var error = e.Errors.SingleOrDefault();
            Assert.NotNull(error);
            Assert.Equal("vehicleId", error.PropertyName);
            Assert.Contains("not found", error.ErrorMessage);
            Assert.Equal(_command.VehicleId, error.AttemptedValue);
        }
    }
}
