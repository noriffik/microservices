﻿using AutoFixture;
using FluentValidation;
using Moq;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.Notes;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Commands
{
    public class CreateNoteCommandHandlerTest
    {
        private static readonly Fixture Fixture = new Fixture();
        private readonly Mock<INotesRepository> _noteRepository = new Mock<INotesRepository>();
        private readonly Mock<ITestDriveRepository> _testDriveRepository = new Mock<ITestDriveRepository>();
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly CreateNoteCommandHandler _handler;
        private readonly CreateNoteCommand _command;

        public CreateNoteCommandHandlerTest()
        {
            _unitOfWork.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);
            _noteRepository.Setup(r => r.UnitOfWork)
                .Returns(_unitOfWork.Object);

            _command = Fixture.Create<CreateNoteCommand>();
            _testDriveRepository.Setup(r => r.Has(_command.TestDriveId))
                .ReturnsAsync(true);

            _handler = new CreateNoteCommandHandler(_noteRepository.Object, _testDriveRepository.Object);
        }

        [Fact]
        public void Ctor_GivenNoteRepository_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new CreateNoteCommandHandler(null, _testDriveRepository.Object);
            });

            Assert.Equal("noteRepository", e.ParamName);
        }

        [Fact]
        public void Ctor_GivenTestDriveRepository_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new CreateNoteCommandHandler(_noteRepository.Object, null);
            });

            Assert.Equal("testDriveRepository", e.ParamName);
        }

        [Fact]
        public async Task Handle_GivenCommand_IsNull_Throws()
        {
            //Assert
            var e = await Assert.ThrowsAsync<ArgumentNullException>(() => _handler.Handle(null, CancellationToken.None));
            Assert.Equal("command", e.ParamName);
        }

        [Fact]
        public async Task Handle()
        {
            //Act
            await _handler.Handle(_command, CancellationToken.None);

            //Assert
            _noteRepository.Verify(r => r.Add(
                It.Is<Note>(n => 
                        n.Content == _command.Content &&
                        n.ManagerId == _command.ManagerId &&
                        n.TestDriveId == _command.TestDriveId)), Times.Once);
        }

        [Fact]
        public async Task Handle_WhenTestDrive_DoesNotExist_Throw()
        {
            //Arrange
            _testDriveRepository.Setup(r => r.Has(_command.TestDriveId)).ReturnsAsync(false);

            //Assert
            var e = await Assert.ThrowsAsync<ValidationException>(() => _handler.Handle(_command, CancellationToken.None));
            var error = e.Errors.SingleOrDefault();
            Assert.NotNull(error);
            Assert.Equal("testDriveId", error.PropertyName);
            Assert.Contains("not exist", error.ErrorMessage);
            Assert.Equal(_command.TestDriveId, error.AttemptedValue);
        }
    }
}
