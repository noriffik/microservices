﻿using FluentValidation;
using Moq;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.Notes;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Commands
{
    public class RemoveNoteCommandHandlerTest
    {
        private readonly Mock<INotesRepository> _noteRepository = new Mock<INotesRepository>();
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly RemoveNoteCommandHandler _handler;
        private readonly Note _note = new Note(1, 1, 1, "Content1");
        private readonly RemoveNoteCommand _command;

        public RemoveNoteCommandHandlerTest()
        {
            _unitOfWork.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);
            _noteRepository.Setup(r => r.UnitOfWork).Returns(_unitOfWork.Object);
            _handler = new RemoveNoteCommandHandler(_noteRepository.Object);
            _noteRepository.Setup(r => r.Find(_note.Id)).ReturnsAsync(_note);
            _command = new RemoveNoteCommand() { NoteId = _note.Id, ManagerId = _note.ManagerId };
        }

        [Fact]
        public void Ctor_GivenNoteRepository_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new RemoveNoteCommandHandler(null);
            });
            Assert.Equal("notesRepository", e.ParamName);
        }

        [Fact]
        public async Task Handle_WhenNote_IsNotFound_Throws()
        {
            //Arrange
            _noteRepository.Setup(r => r.Find(_note.Id)).ReturnsAsync(null as Note);

            //Assert
            var e = await Assert.ThrowsAsync<ValidationException>(() => _handler.Handle(_command, CancellationToken.None));
            var error = e.Errors.SingleOrDefault();
            Assert.NotNull(error);
            Assert.Equal("noteId", error.PropertyName);
            Assert.Contains("not found", error.ErrorMessage);
            Assert.Equal(_note.Id, error.AttemptedValue);
        }

        [Fact]
        public async Task Handle()
        {
            //Act
            var response = await _handler.Handle(_command, CancellationToken.None);

            //Assert
            Assert.True(response);
            _noteRepository.Verify(r => r.Remove(_note));
            _unitOfWork.Verify(w => w.Commit(It.IsAny<CancellationToken>()));
        }

        [Fact]
        public async Task Handle_WhenGivenCommand_HasInvalidManagerId_Throws()
        {
            //Arrange
            var badCommand = new RemoveNoteCommand { ManagerId = 2, NoteId = 1 };

            //Assert
            var error = await Assert.ThrowsAsync<OwnerRequiredException>(() => _handler.Handle(badCommand, CancellationToken.None));
            Assert.Contains("is not owner", error.Message);
        }
    }
}
