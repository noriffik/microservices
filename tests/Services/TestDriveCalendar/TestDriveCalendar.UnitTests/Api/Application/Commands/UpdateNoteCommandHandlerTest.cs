﻿using AutoFixture;
using FluentValidation;
using Moq;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.Notes;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Api.Application.Commands
{
    public class UpdateNoteCommandHandlerTest
    {
        private static readonly Fixture Fixture = new Fixture();
        private readonly Mock<INotesRepository> _noteRepository = new Mock<INotesRepository>();
        private readonly Mock<IUnitOfWork> _unitOfWork = new Mock<IUnitOfWork>();
        private readonly UpdateNoteCommandHandler _handler;
        private readonly Note _note = new Note(1, 1, 1, "Content1");
        private readonly UpdateNoteCommand _command = new UpdateNoteCommand { Content = "NewContent", ManagerId = 1, NoteId = 1 };

        public UpdateNoteCommandHandlerTest()
        {
            _unitOfWork.Setup(w => w.Commit(It.IsAny<CancellationToken>())).Returns(Task.CompletedTask);
            _noteRepository.Setup(r => r.UnitOfWork).Returns(_unitOfWork.Object);
            _handler = new UpdateNoteCommandHandler(_noteRepository.Object);
            _noteRepository.Setup(r => r.Find(_note.Id)).ReturnsAsync(_note);
        }

        [Fact]
        public void Ctor_GivenNoteRepository_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() =>
                {
                    var unused = new UpdateNoteCommandHandler(null);
                });
            Assert.Equal("notesRepository", e.ParamName);
        }

        [Fact]
        public async Task Handle_GivenCommand_IsNull_Throws()
        {
            //Assert
            var e = await Assert.ThrowsAsync<ArgumentNullException>(() => _handler.Handle(null, CancellationToken.None));
            Assert.Equal("command", e.ParamName);
        }

        [Fact]
        public async Task Handle_WhenNote_IsNotFound_Throws()
        {
            //Arrange
            var badCommand = Fixture.Create<UpdateNoteCommand>();
            _noteRepository.Setup(r => r.Find(badCommand.NoteId)).ReturnsAsync(null as Note);

            //Assert
            var e = await Assert.ThrowsAsync<ValidationException>(() => _handler.Handle(badCommand, CancellationToken.None));
            var error = e.Errors.SingleOrDefault();
            Assert.NotNull(error);
            Assert.Equal("noteId", error.PropertyName);
            Assert.Contains("not found", error.ErrorMessage);
            Assert.Equal(badCommand.NoteId, error.AttemptedValue);
        }

        [Fact]
        public async Task Handle()
        {
            //Act
            var response = await _handler.Handle(_command, CancellationToken.None);

            //Assert
            Assert.True(response);
            Assert.Equal(_command.NoteId, _note.Id);
            Assert.Equal(_command.ManagerId, _note.ManagerId);
            Assert.Equal(_command.Content, _note.Content);
            _unitOfWork.Verify(w => w.Commit(It.IsAny<CancellationToken>()));
        }

        [Fact]
        public async Task Handle_WhenGivenCommand_HasInvalidManagerId_Throws()
        {
            //Arrange
            var badCommand = new UpdateNoteCommand {Content = "NewContent", ManagerId = 2, NoteId = 1};
            
            //Assert
            var error = await Assert.ThrowsAsync<OwnerRequiredException>(() => _handler.Handle(badCommand, CancellationToken.None));
            Assert.Contains("is not owner", error.Message);
        }
    }
}
