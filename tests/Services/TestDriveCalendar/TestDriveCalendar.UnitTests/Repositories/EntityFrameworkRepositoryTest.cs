﻿using AutoFixture;
using Microsoft.EntityFrameworkCore;
using Moq;
using NexCore.Domain;
using NexCore.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.UnitTests.Repositories
{
    public class EntityFrameworkRepositoryTest : IDisposable
    {
        private readonly DbContextOptions _options;
        private readonly IReadOnlyList<TestAggregateRoot> _entities;
        private readonly TestAggregateRoot _entity;

        public EntityFrameworkRepositoryTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new Int32SequenceGenerator());

            _entities = Enumerable.Range(1, 5).Select(id => new TestAggregateRoot(id)).ToList();
            _entity = _entities[2];

            _options = new DbContextOptionsBuilder<DbContext>()
                .UseInMemoryDatabase(nameof(EntityFrameworkRepositoryTest))
                .Options;
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new TestRepository(null));
        }

        [Fact]
        public void UnitOfWork()
        {
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                //Arrange
                var repository = new TestRepository(context);

                //Assert
                Assert.Equal(context, repository.UnitOfWork);
            }
        }
        
        [Fact]
        public void Add()
        {
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                //Arrange
                var repository = new TestRepository(context);

                //Act
                repository.Add(_entity);

                //Assert
                Assert.Equal(EntityState.Added, context.Entry(_entity).State);
            }
        }

        [Fact]
        public void Add_GivenEntity_IsNull_Throws()
        {
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                //Arrange
                var repository = new TestRepository(context);

                //Assert
                Assert.Throws<ArgumentNullException>("entity", () => { repository.Add(null); });
            }
        }
        
        [Fact]
        public void Remove()
        {
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                //Arrange
                var repository = new TestRepository(context);

                //Act
                repository.Remove(_entity);

                //Assert
                Assert.Equal(EntityState.Deleted, context.Entry(_entity).State);
            }
        }
        
        [Fact]
        public void Remove_GivenEntity_IsNull_Throws()
        {
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                //Arrange
                var repository = new TestRepository(context);

                //Assert
                Assert.Throws<ArgumentNullException>("entity", () => { repository.Remove(null); });
            }
        }

        [Fact]
        public async Task Find()
        {
            await PersistEntities();

            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                //Arrange
                var repository = new TestRepository(context);

                //Act
                var actual = await repository.Find(_entity.Id);

                //Assert
                Assert.Equal(_entity, actual);
            }
        }

        [Fact]
        public async Task Find_WhenEntity_IsNotFound_ReturnsNull()
        {
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                //Arrange
                var repository = new TestRepository(context);

                //Act
                var result = await repository.Find(348);

                //Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public async Task Find_WithSpecification()
        {
            //Arrange
            var specification = new Mock<Specification<TestAggregateRoot>>();
            var expected = await SetupManyEntities(specification);
            
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                var repository = new TestRepository(context);

                //Act
                var actual = await repository.Find(specification.Object, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual.OrderBy(s => s.Id));
            }
        }
        
        [Fact]
        public Task Find_WithSpecification_GivenSpecification_IsNull_Throws()
        {
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                //Arrange
                var repository = new TestRepository(context);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>(
                    "specification", () => repository.Find(null, CancellationToken.None));
            }
        }

        [Fact]
        public async Task SingleOrDefault()
        {
            //Arrange
            var specification = new Mock<Specification<TestAggregateRoot>>();
            var expected = await SetupSingleEntity(specification);
            
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                var repository = new TestRepository(context);

                //Act
                var actual = await repository.SingleOrDefault(specification.Object, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual);
            }
        }

        [Fact]
        public async Task SingleOrDefault_WhenNotFound_ReturnsNull()
        {
            //Arrange
            var specification = CreateUnsatisfiableSpecification();

            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                var repository = new TestRepository(context);

                //Act
                var actual = await repository.SingleOrDefault(specification, CancellationToken.None);

                //Assert
                Assert.Null(actual);
            }
        }

        [Fact]
        public async Task SingleOrDefault_WhenFound_Multiple_Throws()
        {
            //Arrange
            var specification = new Mock<Specification<TestAggregateRoot>>();
            await SetupManyEntities(specification);
            
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                var repository = new TestRepository(context);

                //Act
                await Assert.ThrowsAsync<InvalidOperationException>(() =>
                    repository.SingleOrDefault(specification.Object, CancellationToken.None));
            }
        }
        
        [Fact]
        public Task SingleOrDefault_GivenSpecification_IsNull_Throws()
        {
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                //Arrange
                var repository = new TestRepository(context);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>(
                    "specification", () => repository.Find(null, CancellationToken.None));
            }
        }

        [Fact]
        public async Task Has()
        {
            //Arrange
            await PersistEntities();

            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                var repository = new TestRepository(context);

                //Act
                var result = await repository.Has(_entity.Id);

                //Assert
                Assert.True(result);
            }
        }

        [Fact]
        public async Task Has_WhenEntity_IsNotFound_ReturnsFalse()
        {
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                //Arrange
                var repository = new TestRepository(context);

                //Act
                var result = await repository.Has(348);

                //Assert
                Assert.False(result);
            }
        }

        [Fact]
        public async Task Has_WithSpecification()
        {
            //Arrange
            var specification = new Mock<Specification<TestAggregateRoot>>();
            await SetupManyEntities(specification);
            
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                var repository = new TestRepository(context);

                //Act
                var result = await repository.Has(specification.Object, CancellationToken.None);

                //Assert
                Assert.True(result);
            }
        }
        
        [Fact]
        public Task Has_WithSpecification_GivenSpecification_IsNull_Throws()
        {
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                //Arrange
                var repository = new TestRepository(context);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>(
                    "specification", () => repository.Has(null, CancellationToken.None));
            }
        }

        [Fact]
        public async Task Has_WithManyIds()
        {
            //Arrange
            await PersistEntities();

            var ids = _entities.Take(3).Select(e => e.Id);

            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                var repository = new TestRepository(context);

                //Act
                var result = await repository.Has(ids);

                //Assert
                Assert.True(result);
            }
        }

        [Fact]
        public async Task Has_WithManyIds_WhenOneOf_Entities_IsNotFound_ReturnsFalse()
        {
            //Arrange
            await PersistEntities();

            var ids = _entities.Take(3).Select(e => e.Id).Concat(new[] {485});

            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                var repository = new TestRepository(context);

                //Act
                var result = await repository.Has(ids);

                //Assert
                Assert.False(result);
            }
        }

        [Fact]
        public async Task Has_WithManyIds_WhenEntity_IsNotFound_ReturnsFalse()
        {
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                //Arrange
                var repository = new TestRepository(context);

                //Act
                var result = await repository.Has(Enumerable.Range(3659, 3));

                //Assert
                Assert.False(result);
            }
        }

        [Fact]
        public async Task Count()
        {
            //Arrange
            var specification = new Mock<Specification<TestAggregateRoot>>();
            var expected = await SetupManyEntities(specification);
            
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                var repository = new TestRepository(context);

                //Act
                var actual = await repository.Count(specification.Object, CancellationToken.None);

                //Assert
                Assert.Equal(expected.Count, actual);
            }
        }
        
        [Fact]
        public Task Count_GivenSpecification_IsNull_Throws()
        {
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                //Arrange
                var repository = new TestRepository(context);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>(
                    "specification", () => repository.Count(null, CancellationToken.None));
            }
        }

        private async Task<TestAggregateRoot> SetupSingleEntity(Mock<Specification<TestAggregateRoot>> specification)
        {
            //Setup expectation
            var expected = _entities.Last();

            //Setup specification
            specification.Setup(s => s.ToExpression()).Returns(e => e == expected);

            await PersistEntities();

            return expected;
        }

        private async Task<IReadOnlyList<TestAggregateRoot>> SetupManyEntities(Mock<Specification<TestAggregateRoot>> specification)
        {
            //Setup expectation
            var expected = _entities.Take(2).ToList();

            //Setup specification
            specification.Setup(s => s.ToExpression()).Returns(e => expected.Contains(e));

            await PersistEntities();

            return expected;
        }

        private Specification<TestAggregateRoot> CreateUnsatisfiableSpecification()
        {
            var specification = new Mock<Specification<TestAggregateRoot>>();

            specification.Setup(s => s.ToExpression()).Returns(e => false);

            return specification.Object;
        }

        private async Task PersistEntities()
        {
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                context.Set<TestAggregateRoot>().AddRange(_entities);

                await context.SaveChangesAsync();
            }
        }

        public void Dispose()
        {
            using (var context = new TestUnitOfWorkDbContext(_options))
            {
                context.RemoveRange(context.Set<TestAggregateRoot>());
                context.SaveChanges();
            }
        }
    }
}
