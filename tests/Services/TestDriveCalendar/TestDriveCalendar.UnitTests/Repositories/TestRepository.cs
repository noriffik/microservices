﻿using NexCore.Infrastructure;
using NexCore.TestDriveCalendar.Api.Infrastructure.Repositories;
using NexCore.Testing;

namespace NexCore.TestDriveCalendar.UnitTests.Repositories
{
    public class TestRepository : EntityFrameworkRepository<UnitOfWorkDbContext, TestAggregateRoot>
    {
        public TestRepository(UnitOfWorkDbContext context) : base(context)
        {
        }
    }
}
