﻿using Microsoft.Extensions.Configuration;

namespace NexCore.TestDriveCalendar.FunctionalTests
{
    static class TestConfiguration
    {
        private static IConfigurationRoot _root;

        public static IConfigurationRoot Get => _root ?? (_root = new ConfigurationBuilder()
                                                    .AddJsonFile("appsettings.json", true)
                                                    .AddUserSecrets("5FEC2F62-8F29-4A3B-9FF4-CEB8745D3300")
                                                    .Build());
    }
}
