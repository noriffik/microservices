﻿using NexCore.TestDriveCalendar.Api.Infrastructure;
using NexCore.Testing.Functional;
using System;

namespace NexCore.TestDriveCalendar.FunctionalTests
{
    public class TestDriveTestFixture : DbContextTestFixture<TestDriveContext>
    {
        public TestDriveTestFixture() : base(GetConnectionString())
        {
        }

        private static string GetConnectionString()
        {
            return TestConfiguration.Get["ConnectionString"]
                .Replace("%DATABASE%", $"NexCore.Services.TestDriveCalendar_{Guid.NewGuid()}");
        }

        public override TestDriveContext CreateContext()
        {
            return new TestDriveContext(Options);
        }
    }
}
