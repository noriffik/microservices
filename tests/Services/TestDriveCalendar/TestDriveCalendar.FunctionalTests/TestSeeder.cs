﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Domain;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Infrastructure;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.TestDriveCalendar.FunctionalTests
{
    internal static class TestSeeder
    {
        public static async Task<Dealer> SeedTypicalDealer(IWebHost host)
        {
            var dealer = new Dealer(DealerCode.Parse("38001"), Schedule.Typical, 60);
            
            await Seed<Dealer, DealerCode>(host, dealer);

            return dealer;
        }

        public static async Task Seed(IWebHost host, params Entity[] entities)
        {
            using (var context = host.Services.GetRequiredService<TestDriveContext>())
            {
                foreach (var entity in entities)
                {
                    var found = await context.FindAsync(entity.GetType(), entity.Id);
                    if (found != null)
                        continue;

                    context.Add(entity);
                }

                await context.SaveChangesAsync();
            }
        }

        public static async Task Seed<TEntity, TId>(IWebHost host, params TEntity[] entities)
            where TId : EntityId
            where TEntity : Entity<TId>
        {
            using (var context = host.Services.GetRequiredService<TestDriveContext>())
            {
                foreach (var entity in entities)
                {
                    var doesExist = await context.Set<TEntity>()
                        .Where(e => Convert.ToString(e.Id) == entity.Id.Value)
                        .AnyAsync();

                    if (doesExist)
                        continue;

                    context.Add(entity);
                }

                await context.SaveChangesAsync();
            }
        }
    }
}
