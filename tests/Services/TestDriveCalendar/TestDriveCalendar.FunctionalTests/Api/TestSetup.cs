﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexCore.TestDriveCalendar.Api;
using NexCore.Testing.Functional.Auth;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api
{
    public class TestStartup : Startup
    {
        public TestStartup(IConfiguration configuration) : base(configuration)
        {
        }

        protected override void ConfigureAuthentication(IServiceCollection services)
        {
            services.AddAuthentication("Test")
                .AddTestAuthentication("Test");
        }
    }
}
