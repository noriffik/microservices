﻿using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Controllers
{
    public class TestDrivesControllerTest : IClassFixture<ApiWebApplicationFactory>
    {
        private const string ListAllEndPoint = "api/test-drives/list-all";
        private const string ListByDealerDateEndPoint = "api/test-drives/by-dealer-date/{0}/{1}";

        private readonly HttpClient _client;

        public TestDrivesControllerTest(ApiWebApplicationFactory factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task ListAll()
        {
            //Act
            var response = await _client.GetAsync(ListAllEndPoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ListByDealerDate()
        {
            //Arrange
            var url = string.Format(ListByDealerDateEndPoint, 5, "2018-12-23");

            //Act
            var response = await _client.GetAsync(url);

            //Assert
            response.EnsureSuccessStatusCode();
        }
    }
}
