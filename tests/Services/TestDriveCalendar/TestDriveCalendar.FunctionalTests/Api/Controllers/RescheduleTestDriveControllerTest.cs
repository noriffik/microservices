﻿using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Controllers
{
    public class RescheduleTestDriveControllerTest : IClassFixture<ApiWebApplicationFactory>
    {
        private const string RescheduleTestDriveEndPoint = "api/test-drives/reschedule";

        private const int ClientId = 1;
        private const int TestDriveId = 1;
        private const int SalesManagerId = 2;
        private const int VehicleId = 1;
        private const int ModelId = 2;
        private readonly DateTime _since = new DateTime(2019, 11, 5, 15, 0, 0);
        private readonly DateTime _till = new DateTime(2019, 11, 5, 16, 0, 0);

        private readonly HttpClient _client;
        private readonly IWebHost _host;
        private readonly RescheduleTestDriveCommand _command;

        public RescheduleTestDriveControllerTest(ApiWebApplicationFactory factory)
        {
            _client = factory.CreateClient();
            _host = factory.Server.Host;
            _command = new RescheduleTestDriveCommand
            {
                TestDriveId = TestDriveId,
                Since = new DateTime(2019, 11, 5, 17, 0, 0),
                Till = new DateTime(2019, 11, 5, 18, 0, 0),
                SalesManagerId = SalesManagerId
            };
        }

        [Fact]
        public async Task SetDuration_ToExistingTestDrive_ReturnsSuccess()
        {
            //Arrange
            var dealer = new Dealer(DealerCode.Parse("38001"), Schedule.Typical, 60);
            var fleet = new Fleet(dealer.Id);
            fleet.Add(new Vehicle(VehicleId, Vin.Parse("00000000000000001"), ModelId));
            var testDrive = new TestDrive(TestDriveId, ClientId, VehicleId, _since, _till, SalesManagerId);

            await TestSeeder.Seed<Dealer, DealerCode>(_host, dealer);
            await TestSeeder.Seed(_host, fleet, testDrive);
            
            //Act
            var response = await _client.PostAsync(RescheduleTestDriveEndPoint, MapToContent(_command));

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task SetClient_GivenCommand_IsInInvalidJsonFormat_ReturnsBadRequest()
        {
            //Arrange
            var invalidContent = new StringContent("invalid", Encoding.UTF8, "application/json");

            //Act
            var response = await _client.PostAsync(RescheduleTestDriveEndPoint, invalidContent);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task SetClient_GivenCommand_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var command = new RescheduleTestDriveCommand();

            //Act
            var response = await _client.PostAsync(RescheduleTestDriveEndPoint, MapToContent(command));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        private static StringContent MapToContent<T>(T command) where T : class
        {
            return new StringContent(JsonConvert.SerializeObject(command), Encoding.UTF8, "application/json");
        }
    }
}
