﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Notes;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Infrastructure;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Controllers
{
    public class NotesControllerTest : IClassFixture<ApiWebApplicationFactory>
    {
        private const string CreateEndPoint = "api/test-drive/notes/create";
        private const string GetNotesByTestDriveIdEndPoint = "api/test-drive/notes/list-by-test-drive";
        private const string UpdateNoteEndPoint = "api/test-drive/notes/update";
        private const string DeleteNoteEndPoint = "api/test-drive/notes/remove?NoteId={0}&ManagerId={1}";

        private readonly HttpClient _client;
        private readonly IWebHost _host;

        public NotesControllerTest(ApiWebApplicationFactory factory)
        {
            _client = factory.CreateClient();
            _host = factory.Server.Host;
        }

        private async Task Seed()
        {
            var hasNote = await _host.Services
                .GetRequiredService<ITestDriveRepository>()
                .Has(45);

            if (hasNote) return;
            
            using (var context = _host.Services.GetRequiredService<TestDriveContext>())
            {
                var fleet = new Fleet(1, DealerCode.Parse("38001"));
                var vehicle = new Vehicle(1, Vin.Parse("12345678901234567"), 1);
                fleet.Add(vehicle);

                var testDrive = new TestDrive(45, 1, 1, DateTime.Now, DateTime.Now.AddHours(1), 2);
                context.TestDrives.Add(testDrive);

                var note = new Note(5, 45, 1, "Content");
                context.Notes.Add(note);

                await context.SaveChangesAsync();
            }
        }

        [Fact]
        public async Task Create()
        {
            //Arrange
            await Seed();

            var command = new CreateNoteCommand
            {
                TestDriveId = 45,
                ManagerId = 99,
                Content = "content"
            };
            //Act
            var response = await _client.PostAsync(CreateEndPoint, MapToContent(command));

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Create_GivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var response = await _client.PostAsync(CreateEndPoint, MapToContent(null));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task GetNotesByTestDriveId()
        {
            //Arrange
            await Seed();
            var testDriveId = 45;
            var url = GetNotesByTestDriveIdEndPoint + "/" + testDriveId;

            //Act
            var response = await _client.GetAsync(url);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task UpdateNote()
        {
            //Arrange
            await Seed();
            var command = new UpdateNoteCommand {NoteId = 5, ManagerId = 1, Content = "NewContent"};
            
            //Act
            var response = await _client.PutAsync(UpdateNoteEndPoint, MapToContent(command));

            //Assert
            response.EnsureSuccessStatusCode();
        }
        
        [Fact]
        public async Task UpdateNote_WhenHandlerThrow_OwnerRequiredException_ReturnsBadRequest()
        {
            //Arrange
            await Seed();
            var command = new UpdateNoteCommand { NoteId = 5, ManagerId = 2, Content = "NewContent" };

            //Act
            var response = await _client.PutAsync(UpdateNoteEndPoint, MapToContent(command));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task UpdateNote_GivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var response = await _client.PutAsync(UpdateNoteEndPoint, MapToContent(null));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task RemoveNote()
        {
            //Arrange
            await Seed();
            var command = new RemoveNoteCommand { NoteId = 5, ManagerId = 1};
            var url = string.Format(DeleteNoteEndPoint, command.NoteId, command.ManagerId);
            //Act
            var response = await _client.DeleteAsync(url);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task RemoveNote_WhenHandlerThrow_OwnerRequiredException_ReturnsBadRequest()
        {
            //Arrange
            await Seed();
            var command = new RemoveNoteCommand { NoteId = 5, ManagerId = 2};
            var url = string.Format(DeleteNoteEndPoint, command.NoteId, command.ManagerId);

            //Act
            var response = await _client.DeleteAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task RemoveNote_GivenCommand_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var url = string.Format(DeleteNoteEndPoint, 0, 0);

            //Act
            var response = await _client.DeleteAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        private static HttpContent MapToContent(object value)
        {
            return new StringContent(
                JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json");
        }
    }
}
