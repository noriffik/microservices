﻿using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Application.Models;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Controllers
{
    public class SetScheduleControllerTest : IClassFixture<ApiWebApplicationFactory>
    {
        private const string SetScheduleEndPoint = "api/dealers/set-schedule";
        
        private readonly HttpClient _client;
        private readonly IWebHost _host;

        public SetScheduleControllerTest(ApiWebApplicationFactory factory)
        {
            _client = factory.CreateClient();
            _host = factory.Server.Host;
        }

        [Fact]
        public async Task SetSchedule_ToExistingDealer_ReturnsSuccess()
        {
            //Arrange
            var dealer = await TestSeeder.SeedTypicalDealer(_host);
            var command = CreateCommand(dealer.Id);

            //Act
            var response = await _client.PostAsync(SetScheduleEndPoint, MapToContent(command));

            //Assert
            response.EnsureSuccessStatusCode();
        }

        private static SetScheduleCommand CreateCommand(string dealerId)
        {
            return new SetScheduleCommand
            {
                DealerId = dealerId,
                WeeklySchedule = Enumerable.Range(0, 7)
                    .Select(d => new DailyScheduleModel
                    {
                        WorkingDay = d,
                        WorkingHours = new WorkingHoursModel
                        {
                            Since = 5000, Till = 8000
                        }
                    })
                    .ToList()
            };
        }

        [Fact]
        public async Task Create_GivenCommand_IsInInvalidJsonFormat_ReturnsBadRequest()
        {
            //Arrange
            var invalidContent = new StringContent("invalid", Encoding.UTF8, "application/json");

            //Act
            var response = await _client.PostAsync(SetScheduleEndPoint, invalidContent);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Create_GivenCommand_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var command = new SetScheduleCommand();

            //Act
            var response = await _client.PostAsync(SetScheduleEndPoint, MapToContent(command));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        private static StringContent MapToContent<T>(T command) where T : class
        {
            return new StringContent(JsonConvert.SerializeObject(command), Encoding.UTF8, "application/json");
        }
    }
}
