﻿using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Controllers
{
    public class SetTestDriveClientControllerTest: IClassFixture<ApiWebApplicationFactory>
    {
        private const string SetTestDriveClientEndPoint = "api/test-drives/set-client";

        private readonly DateTime _since = new DateTime(2016, 11, 5, 10, 0, 0);
        private readonly DateTime _till = new DateTime(2016, 11, 5, 12, 0, 0);
        private const int ClientId = 1;
        
        private readonly HttpClient _client;
        private readonly IWebHost _host;

        public SetTestDriveClientControllerTest(ApiWebApplicationFactory factory)
        {
            _client = factory.CreateClient();
            _host = factory.Server.Host;
        }

        [Fact]
        public async Task SetClient_ToExistingTestDrive_ReturnsSuccess()
        {
            //Arrange
            var dealer = new Dealer(DealerCode.Parse("38001"), Schedule.Typical, 60);
            var fleet = new Fleet(dealer.Id);
            fleet.Add(new Vehicle(1, Vin.Parse("00000000000000001"), 2));
            var testDrive = new TestDrive(ClientId, 1, _since, _till, 1);

            await TestSeeder.Seed<Dealer, DealerCode>(_host, dealer);
            await TestSeeder.Seed(_host, fleet, testDrive);

            var command = new SetTestDriveClientCommand
            {
                TestDriveId = testDrive.Id,
                ClientId = ClientId
            };

            //Act
            var response = await _client.PostAsync(SetTestDriveClientEndPoint, MapToContent(command));

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task SetClient_GivenCommand_IsInInvalidJsonFormat_ReturnsBadRequest()
        {
            //Arrange
            var invalidContent = new StringContent("invalid", Encoding.UTF8, "application/json");

            //Act
            var response = await _client.PostAsync(SetTestDriveClientEndPoint, invalidContent);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task SetClient_GivenCommand_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var command = new SetTestDriveClientCommand();

            //Act
            var response = await _client.PostAsync(SetTestDriveClientEndPoint, MapToContent(command));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }


        private static StringContent MapToContent<T>(T command) where T : class
        {
            return new StringContent(JsonConvert.SerializeObject(command), Encoding.UTF8, "application/json");
        }
    }
}
