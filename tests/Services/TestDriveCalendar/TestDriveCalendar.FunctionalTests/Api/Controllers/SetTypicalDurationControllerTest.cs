﻿using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Controllers
{
    public class SetTypicalDurationControllerTest : IClassFixture<ApiWebApplicationFactory>
    {
        private const string SetTypicalDurationEndPoint = "api/dealers/set-typical-duration";
        
        private readonly HttpClient _client;
        private readonly IWebHost _host;

        public SetTypicalDurationControllerTest(ApiWebApplicationFactory factory)
        {
            _client = factory.CreateClient();
            _host = factory.Server.Host;
        }

        [Fact]
        public async Task SetTypicalDuration_ToExistingDealer_ReturnsSuccess()
        {
            //Arrange
            var dealer = await TestSeeder.SeedTypicalDealer(_host);
            var command = new SetTestDriveTypicalDurationCommand
            {
                DealerId = dealer.Id,
                TestDriveDuration = 15
            };

            //Act
            var response = await _client.PostAsync(SetTypicalDurationEndPoint, MapToContent(command));

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Create_GivenCommand_IsInInvalidJsonFormat_ReturnsBadRequest()
        {
            //Arrange
            var invalidContent = new StringContent("invalid", Encoding.UTF8, "application/json");

            //Act
            var response = await _client.PostAsync(SetTypicalDurationEndPoint, invalidContent);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Create_GivenCommand_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var command = new SetTestDriveTypicalDurationCommand();

            //Act
            var response = await _client.PostAsync(SetTypicalDurationEndPoint, MapToContent(command));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        private static StringContent MapToContent<T>(T command) where T : class
        {
            return new StringContent(JsonConvert.SerializeObject(command), Encoding.UTF8, "application/json");
        }
    }
}
