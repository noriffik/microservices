﻿using Microsoft.AspNetCore.Hosting;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Controllers
{
    public class GetDealerControllerTest : IClassFixture<ApiWebApplicationFactory>
    {
        private const string GetDealersEndPoint = "api/dealers/{0}";

        private readonly HttpClient _client;
        private readonly IWebHost _host;

        public GetDealerControllerTest(ApiWebApplicationFactory factory)
        {
            _client = factory.CreateClient();
            _host = factory.Server.Host;
        }
        
        [Fact]
        public async Task GetDealer()
        {
            //Arrange
            var dealer = await TestSeeder.SeedTypicalDealer(_host);

            //Act
            var response = await _client.GetAsync(string.Format(GetDealersEndPoint, dealer.Id));

            //Assert
            response.EnsureSuccessStatusCode();
        }
    }
}
