﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Infrastructure;
using NexCore.TestDriveCalendar.Api.Models;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AutoFixture;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Controllers
{
    public class FleetsControllerTest : IClassFixture<ApiWebApplicationFactory>
    {
        private const string AddVehicleEndPoint = "api/fleets/vehicle/add";
        private const string SetAvailabilityEndPoint = "api/fleets/vehicle/availability";
        private const string UpdateVehicleEndPoint = "api/fleets/vehicle/update";
        private const string VehicleListByDealerIdEndPoint = "api/fleets/{0}/vehicles";
        private readonly Fixture _fixture = new Fixture();

        private readonly HttpClient _client;

        private readonly IWebHost _host;

        public FleetsControllerTest(ApiWebApplicationFactory factory)
        {
            _client = factory.CreateClient();
            _host = factory.Server.Host;
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var dealer = await TestSeeder.SeedTypicalDealer(_host);

            var query = new VehicleItem
            {
                DealerId = dealer.Id,
                Vin = "12345678901234567",
                Equipment = "Ambition",
                ModelId = 1
            };

            //Act
            var response = await _client.PostAsync(AddVehicleEndPoint, MapToContent(query));

            //Assert
            response.EnsureSuccessStatusCode();
        }

        private static HttpContent MapToContent(object value)
        {
            return new StringContent(
                JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json");
        }

        [Fact]
        public async Task Add_IfDealerDoesNotExists_Returns_BadRequest()
        {
            //Arrange
            var query = new VehicleItem();

            //Act
            var response = await _client.PostAsync(AddVehicleEndPoint, MapToContent(query));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Add_GivenQuery_IsInvalid_Returns_BadRequest()
        {
            //Arrange
            var dealer = await TestSeeder.SeedTypicalDealer(_host);
            var query = new VehicleItem { DealerId = dealer.Id };

            //Act
            var response = await _client.PostAsync(AddVehicleEndPoint, MapToContent(query));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Add_GivenQuery_VinIsInvalid_Returns_BadRequest()
        {
            //Arrange
            var dealer = await TestSeeder.SeedTypicalDealer(_host);
            var query = new VehicleItem
            {
                DealerId = dealer.Id,
                Vin = "heresy",
                Equipment = "Ambition",
                ModelId = 1
            };

            //Act
            var response = await _client.PostAsync(AddVehicleEndPoint, MapToContent(query));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task SetAvailability()
        {
            //Arrange
            var vehicle = new Vehicle(Vin.Parse("12345678901234567"), 1);
            using (var context = _host.Services.GetRequiredService<TestDriveContext>())
            {
                var fleet = new Fleet(DealerCode.Parse("38001"));
                fleet.Add(vehicle);

                context.Fleets.Add(fleet);

                await context.SaveChangesAsync();
            }
            var query = new SetUnavailability
            {
                VehicleId = vehicle.Id,
                Availability = true,
            };

            //Act
            var response = await _client.PostAsync(SetAvailabilityEndPoint, MapToContent(query));

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task SetAvailability_WhenVehicle_IsNotFound_Returns_BadRequest()
        {
            //Arrange
            var query = new SetUnavailability
            {
                VehicleId = 123
            };

            //Act
            var response = await _client.PostAsync(SetAvailabilityEndPoint, MapToContent(query));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task SetAvailability_GivenQuery_IsInvalid_Returns_BadRequest()
        {
            //Arrange
            var query = new SetUnavailability();

            //Act
            var response = await _client.PostAsync(SetAvailabilityEndPoint, MapToContent(query));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task VehicleList_ReturnsVehicles()
        {
            //Arrange 
            var dealer = await TestSeeder.SeedTypicalDealer(_host);
            var url = string.Format(VehicleListByDealerIdEndPoint, dealer.Id);

            //Act
            var response = await _client.GetAsync(url);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task UpdateVehicle()
        {
            //Arrange
            var vehicle = new Vehicle(Vin.Parse("12345678901234567"), 1);
            using (var context = _host.Services.GetRequiredService<TestDriveContext>())
            {
                var fleet = new Fleet(DealerCode.Parse("38001"));
                fleet.Add(vehicle);

                context.Fleets.Add(fleet);

                await context.SaveChangesAsync();
            }
            var command = _fixture.Build<UpdateVehicleCommand>().With(c => c.VehicleId, vehicle.Id).Create();

            //Act
            var response = await _client.PutAsync(UpdateVehicleEndPoint, MapToContent(command));

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task UpdateVehicle_WhenVehicle_NotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = _fixture.Create<UpdateVehicleCommand>();

            //Act
            var response = await _client.PutAsync(UpdateVehicleEndPoint, MapToContent(command));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task UpdateVehicle_WhenGivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var response = await _client.PutAsync(UpdateVehicleEndPoint, MapToContent(null));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task UpdateVehicle_WhenGivenCommand_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var command = _fixture.Build<UpdateVehicleCommand>().With(c => c.VehicleId, 0).Create();

            //Act
            var response = await _client.PutAsync(UpdateVehicleEndPoint, MapToContent(command));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
