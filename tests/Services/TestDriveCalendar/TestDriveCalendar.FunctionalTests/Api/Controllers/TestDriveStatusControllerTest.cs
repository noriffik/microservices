﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Infrastructure;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Controllers
{
    public class TestDriveStatusControllerTest : IClassFixture<ApiWebApplicationFactory>
    {
        private const string BeginTestDriveEndpoint = "api/test-drive/begin";
        private const string CompleteTestDriveEndpoint = "api/test-drive/complete";
        private const string CancelTestDriveEndpoint = "api/test-drive/cancel";

        private const int ClientId = 1;

        private readonly HttpClient _client;
        private readonly IWebHost _host;
        
        public TestDriveStatusControllerTest(ApiWebApplicationFactory factory)
        {
            _client = factory.CreateClient();
            _host = factory.Server.Host;

            using (var context = _host.Services.GetRequiredService<TestDriveContext>())
            {
                var fleet = new Fleet(1, DealerCode.Parse("38001"));
                var vehicle = new Vehicle(1, Vin.Parse("12345678901234567"), 1);

                fleet.Add(vehicle);
                context.SaveChanges();
            }
        }

        [Fact]
        public async Task Begin()
        {
            using (var context = _host.Services.GetRequiredService<TestDriveContext>())
            {
                var command = new TestDriveStatusCommand
                {
                    SalesManagerId = 22,
                    TestDriveId = 45
                };
                var testDrive =  new TestDrive(45, ClientId, 1, DateTime.Now, DateTime.Now.AddHours(1), 2);

                context.TestDrives.Add(testDrive);

                await context.SaveChangesAsync();

                //Act
                var response = await _client.PostAsync(BeginTestDriveEndpoint, MapToContent(command));

                //Assert
                response.EnsureSuccessStatusCode();
            }
        }

        [Fact]
        public async Task Complete()
        {   
            using (var context = _host.Services.GetRequiredService<TestDriveContext>())
            {
                var testDrive = new TestDrive(89, ClientId, 1, DateTime.Now, DateTime.Now.AddHours(1), 2);
                var command = new TestDriveStatusCommand
                {
                    SalesManagerId = 1,
                    TestDriveId = 89
                };

                testDrive.Begin(command.SalesManagerId);

                context.TestDrives.Add(testDrive);

                await context.SaveChangesAsync();

                //Act
                var response = await _client.PostAsync(CompleteTestDriveEndpoint, MapToContent(command));

                //Assert
                response.EnsureSuccessStatusCode();
           }
        }

        [Fact]
        public async Task Cancel()
        {
            using (var context = _host.Services.GetRequiredService<TestDriveContext>())
            {
                var testDrive = new TestDrive(33, ClientId, 1, DateTime.Now, DateTime.Now.AddHours(1), 2);
                var command = new TestDriveStatusCommand
                {
                    SalesManagerId = 1,
                    TestDriveId = 33
                };

                testDrive.Begin(command.SalesManagerId);

                context.TestDrives.Add(testDrive);

                await context.SaveChangesAsync();

                //Act
                var response = await _client.PostAsync(CancelTestDriveEndpoint, MapToContent(command));

                //Assert
                response.EnsureSuccessStatusCode();
            }
        }

        private static HttpContent MapToContent(object value)
        {
            return new StringContent(
                JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json");
        }
    }
}
