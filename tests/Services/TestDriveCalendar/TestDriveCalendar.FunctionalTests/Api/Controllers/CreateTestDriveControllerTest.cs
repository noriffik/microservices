﻿using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Controllers
{
    public class CreateTestDriveControllerTest: IClassFixture<ApiWebApplicationFactory>
    {
        private const string CreateTestDriveEndPoint = "api/test-drives/create";
        
        private readonly CreateTestDriveCommand _command;

        private const int ClientId = 1;
        private const int FleetId = 5;
        private const int VehicleId = 22;
        private const int SalesManagerId = 2;

        private readonly Vin _vin = Vin.Parse("00000000000000001");
        private readonly DealerCode _dealerId = DealerCode.Parse("38001");
        private readonly DateTime _since = new DateTime(2016, 11, 5, 10, 0, 0);
        private readonly DateTime _till = new DateTime(2016, 11, 5, 12, 0, 0);

        private readonly HttpClient _client;
        private readonly IWebHost _host;

        public CreateTestDriveControllerTest(ApiWebApplicationFactory factory)
        {
            _client = factory.CreateClient();
            _host = factory.Server.Host;

            _command = new CreateTestDriveCommand
            {
                ClientId = ClientId,
                SalesManagerId = SalesManagerId,
                Since = _since,
                Till = _till,
                VehicleId = VehicleId
            };
        }

        [Fact]
        public async Task CreateTestDrive_ToExistingFleet_ReturnsSuccess()
        {
            //Arrange
            var dealer = new Dealer(_dealerId, Schedule.Typical, 60);
            var vehicle = new Vehicle(VehicleId, _vin, 5);
            var fleet = new Fleet(FleetId, _dealerId);
            fleet.Add(vehicle);

            await TestSeeder.Seed<Dealer, DealerCode>(_host, dealer);
            await TestSeeder.Seed(_host, fleet);

            //Act
            var response = await _client.PostAsync(CreateTestDriveEndPoint, MapToContent(_command));

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Create_GivenCommand_IsInInvalidJsonFormat_ReturnsBadRequest()
        {
            //Arrange
            var invalidContent = new StringContent("invalid", Encoding.UTF8, "application/json");

            //Act
            var response = await _client.PostAsync(CreateTestDriveEndPoint, invalidContent);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Create_GivenCommand_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var command = new CreateTestDriveCommand();

            //Act
            var response = await _client.PostAsync(CreateTestDriveEndPoint, MapToContent(command));

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        private static StringContent MapToContent<T>(T command) where T : class
        {
            return new StringContent(JsonConvert.SerializeObject(command), Encoding.UTF8, "application/json");
        }
    }
}
