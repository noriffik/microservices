﻿using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Controllers
{
    public class MakesControllerTest : IClassFixture<ApiWebApplicationFactory>
    {
        private const string ListEndPoint = "api/makes/list";
        private const string ModelsEndPoint = "api/makes/{0}/models";

        private readonly HttpClient _client;

        public MakesControllerTest(ApiWebApplicationFactory factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task List()
        {
            //Act
            var response = await _client.GetAsync(ListEndPoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Models()
        {
            //Act
            var response = await _client.GetAsync(string.Format(ModelsEndPoint, 11));

            //Assert
            response.EnsureSuccessStatusCode();
        }
    }
}
