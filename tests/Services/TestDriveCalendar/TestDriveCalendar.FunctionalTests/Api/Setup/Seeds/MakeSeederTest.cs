﻿using NexCore.TestDriveCalendar.Api.Infrastructure.Repositories;
using NexCore.TestDriveCalendar.Api.Setup.Seeds;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Setup.Seeds
{
    public class MakeSeederTest : IClassFixture<TestDriveTestFixture>
    {
        private readonly TestDriveTestFixture _testFixture;

        public MakeSeederTest(TestDriveTestFixture testFixture)
        {
            _testFixture = testFixture;
        }

        [Fact]
        public async Task Seed()
        {
            //Act
            await PerformSeeding();

            //Assert
            using (var context = _testFixture.CreateContext())
            {
                var result = await context.Makes.FindAsync(MakeSeeder.SkodaId);

                Assert.NotNull(result);
                Assert.Equal(MakeSeeder.SkodaName, result.Name);
                Assert.NotEmpty(result.Models);
                Assert.Equal(10,result.Models.Count());
            }

        }

        private async Task PerformSeeding()
        {
            using (var context = _testFixture.CreateContext())
            {
                var seeder = new MakeSeeder(new MakeRepository(context));

                await seeder.Seed();
            }
        }

        [Fact]
        public async Task Seed_WhenEntitiesExist_DoesNothing()
        {
            //Arrange
            await PerformSeeding();

            //Act
            await PerformSeeding();

            //Assert
            Assert.True(true);
        }
    }
}
