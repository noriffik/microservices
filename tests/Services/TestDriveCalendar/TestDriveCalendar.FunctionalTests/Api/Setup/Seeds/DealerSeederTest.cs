﻿using System;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Infrastructure.Repositories;
using NexCore.TestDriveCalendar.Api.Setup.Seeds;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Setup.Seeds
{
    public class DealerSeederTest : IClassFixture<TestDriveTestFixture>
    {
        private readonly TestDriveTestFixture _testFixture;

        public DealerSeederTest(TestDriveTestFixture testFixture)
        {
            _testFixture = testFixture;
        }

        [Fact]
        public async Task Seed()
        {
            //Act
            await PerformSeeding();

            //Assert
            using (var context = _testFixture.CreateContext())
            {
                var result = await context.Dealers
                    .SingleOrDefaultAsync(d => Convert.ToString(d.Id) == DealerSeeder.Code.Value);

                Assert.NotNull(result);
                Assert.Equal(DealerSeeder.Code, result.Id);
                Assert.Equal(DealerSeeder.TypicalTestDriveDuration, result.TestDriveDuration);
                Assert.Equal(Schedule.Typical, result.Schedule);
            }
        }

        private async Task PerformSeeding()
        {
            using (var context = _testFixture.CreateContext())
            {
                var seeder = new DealerSeeder(new DealerRepository(context));

                await seeder.Seed();
            }
        }

        [Fact]
        public async Task Seed_WhenEntitiesExist_DoesNothing()
        {
            //Arrange
            await PerformSeeding();

            //Act
            await PerformSeeding();
            
            //Assert
            Assert.True(true);
        }
    }
}
