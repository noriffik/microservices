﻿using NexCore.TestDriveCalendar.Api.Application.Commands;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Infrastructure.Repositories;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Application.Commands
{
    public class CreateNoteCommandHandlerTest : IClassFixture<TestDriveTestFixture>
    {
        private readonly TestDriveTestFixture _testFixture;
        
        public CreateNoteCommandHandlerTest(TestDriveTestFixture testFixture)
        {
            _testFixture = testFixture;
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            var command = new CreateNoteCommand
            {
                Content = "content",
            };

            using (var context = _testFixture.CreateContext())
            {
                var dealer = new Dealer(DealerCode.Parse("38001"), Schedule.Empty, 60);
                context.Dealers.Add(dealer);

                var make = new Make(1, "Skoda");
                var model = new Model(1, "EDF", "Test");
                make.AddModel(model);
                context.Makes.Add(make);

                var fleet = new Fleet(dealer.Id);
                var vehicle = new Vehicle(1, Vin.Parse("76543210987654321"), model.Id);
                fleet.Add(vehicle);
                context.Fleets.Add(fleet);

                var testDrive = new TestDrive(1, 1, DateTime.Now, DateTime.Now.AddHours(1), 99);
                context.TestDrives.Add(testDrive);
                context.SaveChanges();

                command.TestDriveId = testDrive.Id;
            }

            using (var context = _testFixture.CreateContext())
            {
                var testDriveRepository = new TestDriveRepository(context);
                var noteRepository = new NoteRepository(context);

                var handler = new CreateNoteCommandHandler(noteRepository, testDriveRepository);

                //Act 
                var result = await handler.Handle(command, CancellationToken.None);

                //Assert
                Assert.InRange(result, 1, int.MaxValue);
            }
        }
    }
}
