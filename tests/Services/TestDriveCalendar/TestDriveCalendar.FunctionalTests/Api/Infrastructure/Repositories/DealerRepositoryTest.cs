﻿using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Extensions;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Infrastructure.Repositories
{
    public class DealerRepositoryTest : IClassFixture<TestDriveTestFixture>, IDisposable
    {
        private readonly TestDriveTestFixture _testFixture;
        private readonly Time _since = new Time(28800);
        private readonly Time _till = new Time(64800);

        public DealerRepositoryTest(TestDriveTestFixture testFixture)
        {
            _testFixture = testFixture;
        }

        public class DealerTestComparer : IEqualityComparer<Dealer>
        {
            public static readonly DealerTestComparer Instance = new DealerTestComparer();

            public bool Equals(Dealer x, Dealer y)
            {
                return x == y || (x != null && y != null &&
                                  x.Id == y.Id && x.Schedule == y.Schedule);
            }

            public int GetHashCode(Dealer obj)
            {
                throw new NotSupportedException();
            }
        }
        
        public Dealer SetupDealer()
        {
            var dailySchedules = new List<DailySchedule>();

            for (var i = 0; i < 7; i++)
                dailySchedules.Add(new DailySchedule((DayOfWeek) i, _since, _till));
            
            return new Dealer(DealerCode.Parse("38001"), new Schedule(dailySchedules), 60);
        }

        [Fact]
        public async Task Add_Persists_AddedDealer_WhenUnitOfWork_IsCommitted()
        {
            //Arrange
            var dealer = SetupDealer();

            //Act
            using (var context = _testFixture.CreateContext())
            {
                var repository = new DealerRepository(context);

                repository.Add(dealer);

                await repository.UnitOfWork.Commit();
            }

            //Assert
            using (var context = _testFixture.CreateContext())
            {
                var actual = await context.Dealers
                    .SingleOrDefaultAsync(d => Convert.ToString(d.Id) == dealer.Id.Value);

                Assert.Equal(dealer, actual, DealerTestComparer.Instance);
            }
        }

        [Fact]
        public async Task Add_Persists_AddedDealerWithDuration_WhenUnitOfWork_IsCommitted()
        {
            //Arrange
            var dealer = SetupDealer();
            dealer.TestDriveDuration = 60;

            //Act
            using (var context = _testFixture.CreateContext())
            {
                var repository = new DealerRepository(context);

                repository.Add(dealer);

                await repository.UnitOfWork.Commit();
            }

            //Assert
            using (var context = _testFixture.CreateContext())
            {
                var actual = await context.Dealers
                    .SingleOrDefaultAsync(d => Convert.ToString(d.Id) == dealer.Id.Value);

                Assert.Equal(dealer, actual, DealerTestComparer.Instance);
            }
        }

        [Fact]
        public async Task FindById()
        {
            //Arrange
            var dealer = SetupDealer();
           
            using (var context = _testFixture.CreateContext())
            {
                var repository = new DealerRepository(context);

                repository.Add(dealer);
                await repository.UnitOfWork.Commit();

                //Act
                var result = await repository.Find(dealer.Id);

                //Assert
                Assert.NotNull(result);
                Assert.Equal(dealer.Schedule, result.Schedule);
            }
        }

        [Fact]
        public async Task FindById_When_Entity_DoesNotExist_ReturnsNull()
        {
            using (var context = _testFixture.CreateContext())
            {
                //Arrange
                var repository = new DealerRepository(context);
                
                //Act
                var result = await repository.Find(DealerCode.Parse("99999"));

                //Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public async Task FindByFleetId_When_FleetExists_ReturnsDealer()
        {
            //Arrange
            var dealer = SetupDealer();

            const int fleetId = 55;
            var fleet = new Fleet(fleetId, dealer.Id);

            using (var context = _testFixture.CreateContext())
            {
                var repository = new DealerRepository(context);
                var fleetRepository = new FleetRepository(context);

                repository.Add(dealer);

                fleetRepository.Add(fleet);
                await fleetRepository.UnitOfWork.Commit();
            }
            
            using (var context = _testFixture.CreateContext())
            {
                var repository = new DealerRepository(context);
                
                //Act
                var result = await repository.FindByFleetIdAsync(fleetId);

                //Assert
                Assert.NotNull(result);
                Assert.Equal(dealer, result, DealerTestComparer.Instance);
            }
        }

        [Fact]
        public async Task FindByFleetId_When_Fleet_DoesNotExist_ReturnsNull()
        {
            using (var context = _testFixture.CreateContext())
            {
                //Arrange
                var repository = new DealerRepository(context);
                
                //Act
                var result = await repository.FindByFleetIdAsync(777);

                //Assert
                Assert.Null(result);
            }
        }
        
        public void Dispose()
        {
            using (var context = _testFixture.CreateContext())
            {
                context.Dealers.RemoveRange(context.Dealers);

                context.SaveChanges();
            }
        }
    }
}
