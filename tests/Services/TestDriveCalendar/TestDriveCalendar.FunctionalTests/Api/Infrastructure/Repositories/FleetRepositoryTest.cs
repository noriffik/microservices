﻿using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Infrastructure.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Infrastructure.Repositories
{
    public class FleetRepositoryTest : IClassFixture<TestDriveTestFixture>, IDisposable
    {
        private readonly TestDriveTestFixture _testFixture;
        private readonly Model _model = new Model("E5", "Octavia");

        public FleetRepositoryTest(TestDriveTestFixture testFixture)
        {
            _testFixture = testFixture;

            using (var context = _testFixture.CreateContext())
            {
                var make = new Make("Skoda");
                make.AddModel(_model);

                context.Add(make);
                context.SaveChanges();
            }
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var expected = CreateFleet(DealerCode.Parse("38001"), 2);

            //Act
            using (var context = _testFixture.CreateContext())
            {
                var repository = new FleetRepository(context);

                repository.Add(expected);

                await repository.UnitOfWork.Commit();
            }

            //Assert
            using (var context = _testFixture.CreateContext())
            {
                var actual = await context.Fleets.FindAsync(expected.Id);

                //Assert
                Assert.Equal(expected, actual);
                Assert.Equal(expected.Vehicles, actual.Vehicles);
            }
        }

        private Fleet CreateFleet(DealerCode dealerId, int vehicleCount)
        {
            var fleet = new Fleet(dealerId);
            Enumerable.Range(1, vehicleCount)
                .ToList()
                .ForEach(n => fleet.Add(Vin.Parse(n.ToString("D17")), _model.Id));

            return fleet;
        }

        [Fact]
        public async Task FindByDealerId()
        {
            //Arrange
            var fleet = CreateFleet(DealerCode.Parse("38001"), 2);

            using (var context = _testFixture.CreateContext())
            {
                context.Fleets.Add(fleet);

                await context.SaveChangesAsync();
            }

            using (var context = _testFixture.CreateContext())
            {
                var repository = new FleetRepository(context);

                //Act
                var result = await repository.FindByDealerIdAsync(fleet.DealerId);

                //Assert
                Assert.Equal(fleet, result);
                Assert.Equal(fleet.Vehicles, result.Vehicles);
            }
        }

        [Fact]
        public async Task FindByVehicleId_WhenFleet_IsNotFound_ReturnsNull()
        {
            //Arrange

            using (var context = _testFixture.CreateContext())
            {
                var repository = new FleetRepository(context);

                //Act
                var result = await repository.FindByVehicleIdAsync(123);

                //Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public async Task FindByVehicleId()
        {
            //Arrange
            var fleet = CreateFleet(DealerCode.Parse("38001"), 5);
            var otherFleet = CreateFleet(DealerCode.Parse("38002"), 2);

            using (var context = _testFixture.CreateContext())
            {
                context.Fleets.Add(fleet);
                context.Fleets.Add(otherFleet);

                await context.SaveChangesAsync();
            }

            var vehicle = fleet.Vehicles[1];

            using (var context = _testFixture.CreateContext())
            {
                var repository = new FleetRepository(context);

                //Act
                var result = await repository.FindByVehicleIdAsync(vehicle.Id);

                //Assert
                Assert.Equal(fleet, result);
                Assert.Contains(vehicle, fleet.Vehicles);
            }
        }

        public void Dispose()
        {
            using (var context = _testFixture.CreateContext())
            {
                context.Makes.RemoveRange(context.Makes);
                context.Fleets.RemoveRange(context.Fleets);

                context.SaveChanges();
            }
        }
    }
}
