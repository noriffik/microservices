﻿using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Infrastructure.Repositories
{
    public class TestDriveRepositoryTest : IClassFixture<TestDriveTestFixture>, IDisposable
    {
        public class TestDriveComparer : IEqualityComparer<TestDrive>
        {
            public static readonly TestDriveComparer Instance = new TestDriveComparer();

            public bool Equals(TestDrive x, TestDrive y)
            {
                return x == y || (x != null && y != null &&
                                  x.Id == y.Id &&
                                  x.ClientId == y.ClientId &&
                                  x.SalesManagerId == y.SalesManagerId &&
                                  x.Since == y.Since &&
                                  x.Till == y.Till &&
                                  x.VehicleId == y.VehicleId &&
                                  x.Status == y.Status);
            }

            public int GetHashCode(TestDrive obj)
            {
                throw new NotSupportedException();
            }
        }

        private readonly TestDriveTestFixture _testFixture;

        private const int ClientId = 1;
        private const int FleetId = 1;
        private const int VehicleId = 2;
        private const int ModelId = 3;
        private const int MakeId = 5;
        private readonly DealerCode _dealerId = DealerCode.Parse("38001");
        private readonly DateTime _since = new DateTime(2018, 11, 19, 12, 0, 0);
        private readonly DateTime _till = new DateTime(2018, 11, 19, 14, 0, 0);
        private readonly DateTime _anotherSince = new DateTime(2018, 11, 20, 12, 0, 0);
        private readonly DateTime _anotherTill = new DateTime(2018, 11, 20, 14, 0, 0);

        private readonly Model _model = new Model(ModelId, "E5", "Octavia");
        private readonly Make _make = new Make(MakeId, "Skoda");
        private readonly Vehicle _vehicle = new Vehicle(VehicleId, Vin.Parse("00000000000000001"), ModelId);
        private readonly Fleet _fleet;
        
        public static IEnumerable<object[]> InvalidDateTimes =>
            new List<object[]>
            {
                new object[] { new DateTime(2018, 11, 19, 11, 0, 0), new DateTime(2018, 11, 19, 13, 0, 0) },
                new object[] { new DateTime(2018, 11, 19, 13, 0, 0), new DateTime(2018, 11, 19, 15, 0, 0) },
                new object[] { new DateTime(2018, 11, 19, 12, 0, 0), new DateTime(2018, 11, 19, 14, 0, 0) },
                new object[] { new DateTime(2018, 11, 19, 11, 0, 0), new DateTime(2018, 11, 19, 15, 0, 0) },
                new object[] { new DateTime(2018, 11, 19, 12, 30, 0), new DateTime(2018, 11, 19, 13, 30, 0) }
            };

        public TestDriveRepositoryTest(TestDriveTestFixture testFixture)
        {
            _testFixture = testFixture;
            _fleet = new Fleet(FleetId, _dealerId);
        }

        private async Task SetupDealer()
        {
            var dealer = new Dealer(_dealerId, Schedule.Typical, 60);

            using (var context = _testFixture.CreateContext())
            {
                if (!context.Dealers.Any(d => d.Id == dealer.Id))
                    context.Add(dealer);

                await context.SaveChangesAsync();
            }
        }

        public async Task SetupFleet()
        {
            using (var context = _testFixture.CreateContext())
            {
                var repository = new MakeRepository(context);
               
                if (_make.Models.All(m => m.Id != ModelId))
                {
                    _make.AddModel(_model);

                    var exists = await repository.Has(MakeId);
                    if (!exists)
                    {
                        repository.Add(_make);
                        context.SaveChanges();
                    }
                }
            }

            using (var context = _testFixture.CreateContext())
            {
                var repository = new FleetRepository(context);

                if (_fleet.Vehicles.All(v => v.Id != VehicleId))
                {
                    _fleet.Add(_vehicle);

                    var exists = await repository.Has(FleetId);
                    if (!exists)
                        repository.Add(_fleet);
                }

                await repository.UnitOfWork.Commit();
            }
        }

        public async Task<TestDrive> SetupTestDrive()
        {
            await SetupDealer();
            await SetupFleet();

            var since = _since;
            var till = _till;

            return new TestDrive(ClientId, VehicleId, since, till, 1);
        }

        [Fact]
        public async Task Add_Persists_AddedTestDrive_WhenUnitOfWork_IsCommitted()
        {
            //Arrange
            var testDrive = await SetupTestDrive();

            //Act
            using (var context = _testFixture.CreateContext())
            {
                var repository = new TestDriveRepository(context);

                repository.Add(testDrive);

                await repository.UnitOfWork.Commit();
            }

            //Assert
            using (var context = _testFixture.CreateContext())
            {
                var actual = await context.TestDrives.FindAsync(testDrive.Id);

                Assert.Equal(testDrive, actual, TestDriveComparer.Instance);
            }
        }

        [Theory]
        [MemberData(nameof(InvalidDateTimes))]
        public async Task IsVehicleReserved_WhenVehicle_Has_TestDriveOnRequestedTime_ReturnsTrue(DateTime since, DateTime till)
        {
            //Arrange
            var testDrive = await SetupTestDrive();

            using (var context = _testFixture.CreateContext())
            {
                var repository = new TestDriveRepository(context);

                repository.Add(testDrive);

                await repository.UnitOfWork.Commit();
            }

            using (var context = _testFixture.CreateContext())
            {
                var repository = new TestDriveRepository(context);

                //Act
                var result = await repository.IsVehicleReservedAsync(VehicleId, since, till);
                
                //Assert
                Assert.True(result);
            }
        }

        [Fact]
        public async Task IsVehicleReserved_WhenVehicle_HasNotAny_TestDriveOnRequestedTime_ReturnsFalse()
        {
            //Arrange
            using (var context = _testFixture.CreateContext())
            {
                var repository = new TestDriveRepository(context);
                
                //Act
                var result = await repository.IsVehicleReservedAsync(VehicleId, _since, _till);
                
                //Assert
                Assert.False(result);
            }
        }

        [Theory]
        [MemberData(nameof(InvalidDateTimes))]
        public async Task CanReschedule_WhenGivenTestDrive_IsAlreadyReserved_OnRequestedTime_ReturnsFalse(DateTime since, DateTime till)
        {
            //Arrange
            var testDrive = await SetupTestDrive();

            using (var context = _testFixture.CreateContext())
            {
                var repository = new TestDriveRepository(context);

                repository.Add(testDrive);

                await repository.UnitOfWork.Commit();
            }

            using (var context = _testFixture.CreateContext())
            {
                var anotherTestDrive = new TestDrive(888, ClientId, VehicleId, _anotherSince, _anotherTill, 5 );
                var repository = new TestDriveRepository(context);

                //Act
                var result = await repository.CanRescheduleAsync(anotherTestDrive, since, till);
                
                //Assert
                Assert.False(result);
            }
        }

        [Fact]
        public async Task CanReschedule_WhenThereIsNo_TestDriveOnRequestedTime_ReturnsTrue()
        {
            //Arrange
            var testDrive = await SetupTestDrive();

            using (var context = _testFixture.CreateContext())
            {
                var repository = new TestDriveRepository(context);

                repository.Add(testDrive);

                await repository.UnitOfWork.Commit();
            }

            using (var context = _testFixture.CreateContext())
            {
                var anotherTestDrive = new TestDrive(888, ClientId, VehicleId, _anotherSince, _anotherTill, 5 );
                var repository = new TestDriveRepository(context);

                //Act
                var result = await repository.CanRescheduleAsync(anotherTestDrive, _anotherSince, _anotherTill);
                
                //Assert
                Assert.True(result);
            }
        }

        [Fact]
        public async Task CanReschedule_WhenThereIsNo_AnotherTestDrives_ReturnsTrue()
        {
            //Arrange
            var testDrive = await SetupTestDrive();

            using (var context = _testFixture.CreateContext())
            {
                var repository = new TestDriveRepository(context);

                repository.Add(testDrive);

                await repository.UnitOfWork.Commit();
            }

            using (var context = _testFixture.CreateContext())
            {
                var repository = new TestDriveRepository(context);

                //Act
                var result = await repository.CanRescheduleAsync(testDrive, _anotherSince, _anotherTill);
                
                //Assert
                Assert.True(result);
            }
        }

        public void Dispose()
        {
            using (var context = _testFixture.CreateContext())
            {
                context.TestDrives.RemoveRange(context.TestDrives);
                context.SaveChanges();
            }
        }
    }
}
