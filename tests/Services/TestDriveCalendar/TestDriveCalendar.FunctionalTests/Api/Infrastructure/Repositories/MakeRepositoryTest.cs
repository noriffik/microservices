﻿using AutoFixture;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Infrastructure.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Infrastructure.Repositories
{
    public class MakeRepositoryTest : IClassFixture<TestDriveTestFixture>, IDisposable
    {
        private readonly Fixture _fixture = new Fixture();
        private readonly TestDriveTestFixture _testFixture;
        private bool _isSecondTime;

        public MakeRepositoryTest(TestDriveTestFixture testFixture)
        {
            _testFixture = testFixture;
            _isSecondTime = false;
        }

        [Fact]
        public async Task List()
        {
            //Arrange
            var makes = (await SetupMakes()).OrderBy(m => m.Name);

            using (var context = _testFixture.CreateContext())
            {
                var repository = new MakeRepository(context);

                //Act
                var result = await repository.List();

                //Assert
                Assert.Equal(makes, result);
            }
        }

        private async Task<Make[]> SetupMakes(int number = 5)
        {
            int modelId;

            if (!_isSecondTime)
                modelId = 1;
            else
                modelId = 4;
            
            var makes = Enumerable.Range(0, number)
                .Select(n => new Make(_fixture.Create<string>()))
                .ToList();

            using (var context = _testFixture.CreateContext())
            {
                foreach (var make in makes)
                {  
                    Enumerable.Range(0, 3)
                        .Select(n => new Model(++modelId, _fixture.Create<string>(), _fixture.Create<string>()))
                        .ToList()
                        .ForEach(m => make.AddModel(m));

                    context.Makes.Add(make);
                }

                await context.SaveChangesAsync();
                _isSecondTime = true;
            }

            return makes.ToArray();
        }

        [Fact]
        public async Task Find()
        {
            //Arrange
            var makes = await SetupMakes();
            var expected = makes[2];

            using (var context = _testFixture.CreateContext())
            {
                var repository = new MakeRepository(context);

                //Act
                var result = await repository.Find(expected.Id);

                //Assert
                Assert.NotNull(result);
                Assert.Equal(expected.Id, result.Id);
                Assert.Equal(expected.Name, result.Name);
                Assert.Equal(expected.Models, result.Models);
            }
        }

        public void Dispose()
        {
            using (var context = _testFixture.CreateContext())
            {
                context.Makes.RemoveRange(context.Makes);
                context.Models.RemoveRange(context.Models);

                context.SaveChanges();
            }
        }
    }
}
