﻿using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.TestDrives;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Infrastructure;
using NexCore.TestDriveCalendar.Api.Models;
using NexCore.TestDriveCalendar.Api.Queries;
using NexCore.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Queries
{
    public class TestDriveQueryTestFixture : TestDriveTestFixture
    {
        public const int FleetId = 1;
        public const int VehicleId = 2;
        public const int ModelId = 3;
        public const int MakeId = 5;

        private readonly DealerCode _dealerId = DealerCode.Parse("38001");

        protected override void SeedDatabase(TestDriveContext context)
        {
            base.SeedDatabase(context);

            var make = new Make(MakeId, "Skoda");
            make.AddModel(new Model(ModelId, "E5", "Octavia"));
            
            var dealer = new Dealer(_dealerId, Schedule.Typical, 60);
            
            var fleet = new Fleet(FleetId, _dealerId);
            fleet.Add(new Vehicle(VehicleId, Vin.Parse("00000000000000001"), ModelId));
            
            context.Makes.Add(make);
            context.Dealers.Add(dealer);
            context.Fleets.Add(fleet);

            context.SaveChanges();
        }
    }

    public class TestDriveQueryTest : IClassFixture<TestDriveQueryTestFixture>, IDisposable
    {
        private readonly TestDriveTestFixture _testFixture;

        private const int ClientId = 1;
        private const int VehicleId = 2;

        private readonly DealerCode _dealerId = DealerCode.Parse("38001");
        private readonly DateTime _anotherSince = new DateTime(2018, 12, 13, 9, 0, 0);
        private readonly DateTime _anotherTill = new DateTime(2018, 12, 13, 10, 0, 0);

        public static List<DateTime> SinceDateTimes =
            new List<DateTime>
            {
                new DateTime(2018, 11, 19, 9, 0, 0),
                new DateTime(2018, 11, 19, 11, 0, 0),
                new DateTime(2018, 11, 19, 13, 0, 0),
                new DateTime(2018, 11, 19, 15, 0, 0),
                new DateTime(2018, 11, 19, 17, 0, 0)
            };

        public static List<DateTime> TillDateTimes =
            new List<DateTime>
            {
                new DateTime(2018, 11, 19, 10, 0, 0),
                new DateTime(2018, 11, 19, 12, 0, 0),
                new DateTime(2018, 11, 19, 14, 0, 0),
                new DateTime(2018, 11, 19, 16, 0, 0),
                new DateTime(2018, 11, 19, 18, 0, 0)
            };
        
        public TestDriveQueryTest(TestDriveQueryTestFixture testFixture)
        {
            _testFixture = testFixture;
        }
        
        public async Task<TestDriveListItem[]> SetupTestDrives(int number = 4)
        {
            var testDrives = new List<TestDrive>();
            for (var i = 0; i <= number; i++)
            {
                var testDrive = new TestDrive(ClientId, VehicleId, SinceDateTimes[i], TillDateTimes[i], 1);

                testDrives.Add(testDrive);
            }

            using (var context = _testFixture.CreateContext())
            {
                await context.TestDrives.AddRangeAsync(testDrives);
                await context.SaveChangesAsync();
            }
           
            return testDrives
                .Select(m => new TestDriveListItem
                {
                    Id = m.Id, 
                    ClientId = m.ClientId,
                    VehicleId = m.VehicleId,
                    Since = m.Since,
                    Till = m.Till,
                    SalesManagerId = m.SalesManagerId,
                    StatusId = m.Status.StatusId,
                    StatusName = Status.Get(m.Status.StatusId).Name
                })
                .OrderBy(m => m.Id)
                .ToArray();
        }

        [Fact]
        public async Task All()
        {
            //Arrange
            var expected = await SetupTestDrives();

            using (var connection = _testFixture.CreateConnection())
            {
                var query = new TestDriveQueries(connection);

                //Act
                var actual = await query.AllAsync();

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<TestDriveListItem>>.Instance);
            }
        }

        [Fact]
        public async Task ByDateAndDealerId()
        {
            //Arrange
            var expected = await SetupTestDrives();

            using (var context = _testFixture.CreateContext())
            {
                await context.TestDrives.AddAsync(
                    new TestDrive(
                        ClientId,
                        VehicleId,
                        _anotherSince,
                        _anotherTill,
                        5
                    ));

                await context.SaveChangesAsync();
            }

            using (var connection = _testFixture.CreateConnection())
            {
                var query = new TestDriveQueries(connection);
                var parameters = new TestDriveByDealerDateItem
                {
                    DealerId = _dealerId,
                    Date = SinceDateTimes[0]
                };

                //Act
                var actual = await query.ByDateAndDealerIdAsync(parameters);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<TestDriveListItem>>.Instance);
            }
        }

        public void Dispose()
        {
            using (var context = _testFixture.CreateContext())
            {
                context.RemoveRange(context.TestDrives);
                context.SaveChanges();
            }
        }
    }
}
