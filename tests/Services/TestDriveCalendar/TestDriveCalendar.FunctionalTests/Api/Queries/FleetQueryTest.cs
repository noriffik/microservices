﻿using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Models;
using NexCore.TestDriveCalendar.Api.Queries;
using NexCore.Testing;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Queries
{
    public class FleetQueryTest : IClassFixture<TestDriveTestFixture>, IDisposable
    {
        private readonly TestDriveTestFixture _testFixture;

        public FleetQueryTest(TestDriveTestFixture testFixture)
        {
            _testFixture = testFixture;
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            var dealerId = DealerCode.Parse("38001");
            var make  = new Make(1, "Skoda");
            var model = new Model(1, "FGR", "Superb");
            make.AddModel(model);

            var fleet = new Fleet(1, dealerId);
            var vin = Vin.Parse("12345678901234567");
            var vehicle = new Vehicle(1,vin, model.Id);
            fleet.Add(vehicle);

            var expected = new List<VehicleListItem>
            {
                new VehicleListItem
                {
                    Availability = true,
                    DealerId = dealerId,
                    MakeName = make.Name,
                    ModelId = model.Id,
                    ModelName = model.Name,
                    ModelCode = model.Code,
                    FleetId = fleet.Id,
                    VehicleId = vehicle.Id,
                    Vin = vin.Number
                }
            };

            var query = new FleetQuery(_testFixture.CreateConnection());

            using (var context = _testFixture.CreateContext())
            {
                context.Makes.Add(make);
                context.Fleets.Add(fleet);
                context.SaveChanges();
            }

            //Act 
            var result = await query.ByDealerIdAsync(dealerId);

            //Assert
            Assert.Equal(expected, result, PropertyComparer<IEnumerable<VehicleListItem>>.Instance);
        }

        public void Dispose()
        {
            using (var context = _testFixture.CreateContext())
            {
                context.RemoveRange(context.Makes);
                context.RemoveRange(context.Fleets);
                context.RemoveRange(context.Models);
                context.RemoveRange(context.Vehicle);
            }
        }
    }
}
