﻿using AutoFixture;
using NexCore.TestDriveCalendar.Api.Domain.Vehicles;
using NexCore.TestDriveCalendar.Api.Models;
using NexCore.TestDriveCalendar.Api.Queries;
using NexCore.Testing;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api.Queries
{
    public class MakeQueriesTest : IClassFixture<TestDriveTestFixture>
    {
        private readonly Fixture _fixture = new Fixture();
        private readonly TestDriveTestFixture _testFixture;

        public MakeQueriesTest(TestDriveTestFixture testFixture)
        {
            _testFixture = testFixture;
        }

        private async Task<MakeListItem[]> SetupMakes(int number = 5)
        {
            var makes = Enumerable.Range(0, number)
                .Select(n => new Make(_fixture.Create<string>()))
                .ToList();

            using (var context = _testFixture.CreateContext())
            {
                context.Makes.AddRange(makes);

                await context.SaveChangesAsync();
            }

            return makes
                .Select(m => new MakeListItem { Id = m.Id, Name = m.Name })
                .OrderBy(m => m.Name)
                .ToArray();
        }

        [Fact]
        public async Task All()
        {
            //Arrange
            var expected = await SetupMakes();

            using (var connection = _testFixture.CreateConnection())
            {
                var query = new MakeQueries(connection);

                //Act
                var actual = await query.AllAsync();

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<MakeListItem>>.Instance);
            }
        }
    }
}
