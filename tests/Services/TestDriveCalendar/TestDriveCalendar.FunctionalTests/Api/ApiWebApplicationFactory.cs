﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NexCore.EventBus.Abstract;
using NexCore.TestDriveCalendar.Api.Domain.Dealers;
using NexCore.TestDriveCalendar.Api.Infrastructure;
using NexCore.TestDriveCalendar.Api.Models;
using NexCore.TestDriveCalendar.Api.Queries;
using System.Collections.Generic;

namespace NexCore.TestDriveCalendar.FunctionalTests.Api
{
    public class ApiWebApplicationFactory : WebApplicationFactory<TestStartup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            base.ConfigureWebHost(builder);

            builder.UseContentRoot(".");

            builder.ConfigureServices(services =>
            {
                services
                    .AddEntityFrameworkInMemoryDatabase()
                    .AddDbContext<TestDriveContext>(
                        options =>
                        {
                            options.UseInMemoryDatabase("InMemoryDbForTesting");
                        },
                        ServiceLifetime.Transient);

                services.AddTransient(s =>
                {
                    var mock = new Mock<IMakeQueries>();
                    mock.Setup(q => q.AllAsync()).ReturnsAsync(new MakeListItem[] {});

                    return mock.Object;
                });

                services.AddTransient(s =>
                {
                    var mock = new Mock<IFleetQuery>();
                    mock.Setup(q => q.ByDealerIdAsync(DealerCode.Parse("38001"))).ReturnsAsync(new List<VehicleListItem>());

                    return mock.Object;
                });

                services.AddTransient(s =>
                {
                    var mock = new Mock<ITestDriveQueries>();
                    mock.Setup(q => q.AllAsync())
                        .ReturnsAsync(new TestDriveListItem[] {});
                    mock.Setup(q => q.ByDateAndDealerIdAsync(It.IsAny<TestDriveByDealerDateItem>()))
                        .ReturnsAsync(new TestDriveListItem[] {});

                    return mock.Object;
                });

                services.AddTransient(s =>
                {
                    var mock = new Mock<INotesQuery>();
                    mock.Setup(q => q.GetAllNotesByTestDriveId(45)).ReturnsAsync(new List<dynamic>());

                    return mock.Object;
                });
                
                services.AddSingleton(s =>
                {
                    var bus = new Mock<IEventBus>();

                    return bus.Object;
                });
            });

            builder.UseDefaultServiceProvider(options =>
            {
                options.ValidateScopes = false;
            });
        }

        protected override IWebHostBuilder CreateWebHostBuilder()
        {
            return WebHost.CreateDefaultBuilder()
                .UseStartup<TestStartup>();
        }
    }
}
