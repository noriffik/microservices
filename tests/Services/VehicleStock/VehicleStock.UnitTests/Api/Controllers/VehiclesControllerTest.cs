﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.VehicleStock.Api.Authorization;
using NexCore.VehicleStock.Api.Controllers;
using NexCore.VehicleStock.Application.Vehicles.Commands.Add;
using NexCore.VehicleStock.Application.Vehicles.Commands.ChangeUnavailability;
using NexCore.VehicleStock.Application.Vehicles.Commands.Delete;
using NexCore.VehicleStock.Application.Vehicles.Queries;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Api.Controllers
{
    public class VehiclesControllerTest
    {
        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(VehiclesController)).HasNullGuard();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<VehicleDto>(typeof(VehiclesController), typeof(ByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetByWarehouseId()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<VehicleDto>>(typeof(VehiclesController), typeof(ByWarehouseIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            var expected = new List<VehicleDto>();

            var mediator = new Mock<IMediator>();
            mediator.Setup(m => m.Send(It.IsAny<AnyQuery>(), CancellationToken.None))
                .ReturnsAsync(expected);

            var authorizationService = new Mock<IAuthorizationService>();

            authorizationService.Setup(s => s.AuthorizeAsync(ClaimsPrincipal.Current, It.IsAny<AnyQuery>(), new [] { AuthorizationRequirements.None }))
                .ReturnsAsync(AuthorizationResult.Success());
            authorizationService.Setup(s => s.AuthorizeAsync(ClaimsPrincipal.Current, expected, new [] { AuthorizationRequirements.None }))
                .ReturnsAsync(AuthorizationResult.Success());

            using (var controller = new VehiclesController(mediator.Object, authorizationService.Object))
            {
                //Act
                var result = await controller.GetAll();

                //Assert
                Assert.Equal(expected, Assert.IsType<JsonResult>(result).Value);
            }
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<string>(typeof(VehiclesController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ChangeUnavailability()
        {
            //Arrange
            var action = Assert.Action(typeof(VehiclesController), typeof(ChangeUnavailabilityCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task RemoveById()
        {
            //Arrange
            var action = Assert.Action(typeof(VehiclesController), typeof(DeleteByIdCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task RemoveByVin()
        {
            //Arrange
            var action = Assert.Action(typeof(VehiclesController), typeof(DeleteByVinCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Matching()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<MatchedVehicleDto>>(typeof(VehiclesController), typeof(MatchingQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }
    }
}
