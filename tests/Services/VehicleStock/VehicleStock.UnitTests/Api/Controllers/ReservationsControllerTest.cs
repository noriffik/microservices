﻿using NexCore.VehicleStock.Api.Controllers;
using NexCore.VehicleStock.Application.Reservations.Commands.Add;
using NexCore.VehicleStock.Application.Reservations.Commands.Cancel;
using NexCore.VehicleStock.Application.Reservations.Commands.Complete;
using NexCore.VehicleStock.Application.Reservations.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Api.Controllers
{
    public class ReservationsControllerTest
    {
        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ReservationsController)).HasNullGuard();
        }

        [Fact]
        public Task Add_ByCommercialNumber()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(ReservationsController), typeof(AddByCommercialNumberCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Add_ByVin()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(ReservationsController), typeof(AddByVinCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<ReservationDto>(typeof(ReservationsController), typeof(ByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetByVin()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<ReservationDto>>(typeof(ReservationsController), typeof(ByVinQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Cancel()
        {
            //Arrange
            var action = Assert.Action(typeof(ReservationsController), typeof(CancelCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Complete()
        {
            //Arrange
            var action = Assert.Action(typeof(ReservationsController), typeof(CompleteCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
