﻿using AutoMapper;
using NexCore.VehicleStock.Application.Vehicles.Commands.Add;
using NexCore.VehicleStock.Infrastructure;

namespace NexCore.VehicleStock.UnitTests.Fixtures
{
    public static class AutoMapperFactory
    {
        public static IMapper Create()
        {
            var application = typeof(AddCommand).Assembly;
            var infrastructure = typeof(VehicleStockContext).Assembly;

            var mapperConfig = new MapperConfiguration(
                c => c.AddProfiles(application, infrastructure));

            return mapperConfig.CreateMapper();
        }
    }
}
