﻿using AutoFixture;
using AutoFixture.Kernel;
using NexCore.DealerNetwork;
using System;

namespace NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders
{
    public class DealerIdCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customize(new DistributorIdCustomization());
            fixture.Customize(new DealerCodeCustomization());
            fixture.Customizations.Add(new DealerIdBuilder());
        }

        private class DealerIdBuilder : ISpecimenBuilder
        {
            public object Create(object request, ISpecimenContext context)
            {
                var type = request as Type;
                if (type == null || type != typeof(DealerId))
                    return new NoSpecimen();

                var distributorId = context.Create<DistributorId>();
                var dealerCode = context.Create<DealerCode>();

                return DealerId.Next(distributorId, dealerCode);
            }
        }
    }
}
