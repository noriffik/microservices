﻿using AutoFixture;
using AutoFixture.Kernel;
using NexCore.VehicleStock.Domain.Vehicles;
using System;

namespace NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders
{
    public class WarehouseDeliveryBuilder : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type;
            if (type == null || type != typeof(WarehouseDelivery))
                return new NoSpecimen();

            var assembledOn = context.Create<DateTime>().Date;
            var dayDiff = context.Create<double>();
            var deliveredOn = assembledOn.AddDays(dayDiff).Date;

            return new WarehouseDelivery(assembledOn, deliveredOn);
        }
    }
}
