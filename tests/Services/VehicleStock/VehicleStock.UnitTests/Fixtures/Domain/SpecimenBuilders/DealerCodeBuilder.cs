﻿using AutoFixture;
using NexCore.DealerNetwork;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;

namespace NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders
{
    public class DealerCodeBuilder : NumericEntityIdGenerator<DealerCode>
    {
        public DealerCodeBuilder() : base(5, DealerCode.Parse)
        {
        }
    }

    public class DealerCodeCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customizations.Add(new DealerCodeBuilder());
        }
    }
}