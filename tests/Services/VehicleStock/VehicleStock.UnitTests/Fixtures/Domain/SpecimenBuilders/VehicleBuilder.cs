﻿using AutoFixture.Kernel;
using NexCore.Vehicles.UnitTests.Fixtures;

namespace NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders
{
    public class VehicleBuilder : CompositeSpecimenBuilder
    {
        public VehicleBuilder() : base(
            new WarehouseDeliveryBuilder(),
            new VehiclesSpecimenBuilder())
        {
        }
    }
}
