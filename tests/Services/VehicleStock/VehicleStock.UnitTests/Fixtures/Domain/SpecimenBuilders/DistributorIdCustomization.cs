﻿using AutoFixture;
using NexCore.DealerNetwork;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;
using NexCore.Testing.Fixtures.SpecimenBuilders.Strings;

namespace NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders
{
    public class DistributorIdCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customizations.Add(new AlphaNumericSequentialStringGenerator());
            fixture.Customizations.Add(new DistributorIdBuilder());
        }

        private class DistributorIdBuilder : SequentialEntityIdGenerator<DistributorId>
        {
            public DistributorIdBuilder() : base(3, DistributorId.Parse)
            {
            }
        }
    }
}
