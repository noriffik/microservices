﻿using NexCore.Testing.Seeders;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Warehouses.DealerWarehouses;
using NexCore.VehicleStock.Infrastructure;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class ReservationSeeder : EntitySeeder<Reservation>
    {
        public ReservationSeeder(VehicleStockContext context) : base(context)
        {
            Fixture.Customizations.Add(new VehicleBuilder());
            Fixture.Customize(new DealerIdCustomization());
        }

        public override async Task<Reservation> Add() => await AddDependencies(await base.Add());

        public override async Task<Reservation> Add(Reservation vehicle) => await AddDependencies(await base.Add(vehicle));

        public override async Task<Reservation> Add(object parameters) => await AddDependencies(await base.Add(parameters));

        public override async Task<Reservation> Add(int id, object parameters) => await AddDependencies(await base.Add(id, parameters));

        public virtual async Task<Reservation> AddDependencies(Reservation reservation)
        {
            await FindOrAdd<DealerWarehouse>(reservation.WarehouseId);

            var vehicle = Get<Vehicle, CommercialNumber>().SingleOrDefault(v => v.Specification.Vin == reservation.Vehicle.Vin);

            if (vehicle == null)
            {
                await Add<Vehicle, CommercialNumber>(new
                {
                    specification = new VehicleSpecification(reservation.Vehicle),
                    warehouseId = reservation.WarehouseId
                });

                return reservation;
            }

            var isVehicleValid = vehicle.Specification == reservation.Vehicle
                                 && vehicle.WarehouseId == reservation.WarehouseId;
            if (isVehicleValid)
                return reservation;

            throw new InvalidOperationException("VehicleSpecification or DealerId is different in existing Vehicle and created Reservation");
        }
    }
}
