﻿using NexCore.Testing.Seeders;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Warehouses.DealerWarehouses;
using NexCore.VehicleStock.Infrastructure;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class VehicleSeeder : EntitySeeder<Vehicle, CommercialNumber>
    {
        public VehicleSeeder(VehicleStockContext context) : base(context)
        {
            Fixture.Customizations.Add(new VehicleBuilder());
            Fixture.Customize(new DealerIdCustomization());
        }

        public override async Task<Vehicle> Add() => await AddDependencies(await base.Add());

        public override async Task<Vehicle> Add(Vehicle vehicle) => await AddDependencies(await base.Add(vehicle));

        public override async Task<Vehicle> Add(object parameters) => await AddDependencies(await base.Add(parameters));

        public override async Task<Vehicle> Add(CommercialNumber id, object parameters) => await AddDependencies(await base.Add(id, parameters));

        public virtual async Task<Vehicle> AddDependencies(Vehicle vehicle)
        {
            await FindOrAdd<DealerWarehouse>(vehicle.WarehouseId);

            return vehicle;
        }
    }
}
