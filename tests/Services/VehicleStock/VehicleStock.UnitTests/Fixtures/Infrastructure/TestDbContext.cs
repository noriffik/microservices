﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Testing.Extensions;
using NexCore.VehicleStock.Infrastructure;

namespace NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure
{
    public partial class InMemoryDbContextFactory
    {
        private class TestDbContext : VehicleStockContext
        {
            public TestDbContext(DbContextOptions<VehicleStockContext> options, IMediator mediator) : base(
                options, mediator)
            {
            }

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                base.OnModelCreating(modelBuilder);

                modelBuilder.SetAutoIncrementStep(1000);
            }
        }
    }
}
