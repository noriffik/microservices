﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Common;
using NexCore.Domain;
using NexCore.Testing;
using NexCore.VehicleStock.Application.Reservations.Commands.Add;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Reservations.Specifications;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Vehicles.Specifications;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Reservations.Commands.Add
{
    public class AddCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Vehicle _vehicle;
        private readonly DateTime _date;
        private readonly Func<Reservation, bool> _isExpected;


        //Commands and handler
        private readonly AddByVinCommand _commandVin;
        private readonly AddByCommercialNumberCommand _commandNumber;
        private readonly AddCommandHandler _handler;

        public AddCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entity
            var customerId = fixture.Create<int>();
            _vehicle = fixture.Create<Vehicle>();
            _date = fixture.Create<DateTime>();
            using (new SystemTimeContext(_date))
            {
                var expectedReservation = new Reservation(_vehicle, customerId);
                _isExpected = reservation =>
                        PropertyComparer<Reservation>.Instance.Equals(reservation, expectedReservation);
            }

            //Commands
            _commandVin = fixture.Build<AddByVinCommand>()
                .With(c => c.CustomerId, customerId)
                .With(c => c.Vin, _vehicle.Specification.Vin)
                .Create();
            _commandNumber = fixture.Build<AddByCommercialNumberCommand>()
                .With(c => c.CustomerId, customerId)
                .With(c => c.CommercialNumber, _vehicle.Id.Value)
                .Create();

            //Handler
            _handler = new AddCommandHandler(_work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle_ByCommercialNumber()
        {
            using (Sequence.Create())
            using (new SystemTimeContext(_date))
            {
                //Arrange
                _repository.Setup(r => r.Require<Vehicle, CommercialNumber>(
                        It.Is<IdSpecification<Vehicle, CommercialNumber>>(s => s.Id == _vehicle.Id), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicle);
                _repository.Setup(r => r.Has(It.Is<ActualByVinSpecification>(s => s.Vin == _vehicle.Specification.Vin), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Add(It.Is<Reservation>(reservation => _isExpected(reservation))))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_commandNumber, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_ByCommercialNumber_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request",
                () => _handler.Handle(null as AddByCommercialNumberCommand, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_ByCommercialNumber_WhenVehicle_IsTemporaryUnavailable_Throws()
        {
            //Arrange
            _vehicle.ChangeUnavailability(true);
            _repository.Setup(r => r.Require<Vehicle, CommercialNumber>(
                    It.Is<IdSpecification<Vehicle, CommercialNumber>>(s => s.Id == _vehicle.Id), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_vehicle);

            //Act
            var e = await Assert.ThrowsAsync<InvalidDomainOperationException>(() => _handler.Handle(_commandNumber, CancellationToken.None));

            //Assert
            Assert.Contains("is temporary unavailable", e.Message.ToLower());
        }

        [Fact]
        public Task Handle_ByCommercialNumber_WhenVehicle_IsAlreadyReserved_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Require<Vehicle, CommercialNumber>(
                    It.Is<IdSpecification<Vehicle, CommercialNumber>>(s => s.Id == _vehicle.Id), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_vehicle);
            _repository.Setup(r => r.Has(It.Is<ActualByVinSpecification>(s => s.Vin == _vehicle.Specification.Vin), It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //Act
            return Assert.ThrowsAsync<IsReservedException>(() => _handler.Handle(_commandNumber, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_ByVin()
        {
            using (Sequence.Create())
            using (new SystemTimeContext(_date))
            {
                //Arrange
                _repository.Setup(r => r.Require<Vehicle, CommercialNumber>(
                        It.Is<VehicleByVinSpecification>(s => s.Vin == _vehicle.Specification.Vin), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicle);
                _repository.Setup(r => r.Has(It.Is<ActualByVinSpecification>(s => s.Vin == _vehicle.Specification.Vin), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Add(It.Is<Reservation>(reservation => _isExpected(reservation))))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_commandVin, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_ByVin_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request",
                () => _handler.Handle(null as AddByVinCommand, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_ByVin_WhenVehicle_IsTemporaryUnavailable_Throws()
        {
            //Arrange
            _vehicle.ChangeUnavailability(true);
            _repository.Setup(r => r.Require<Vehicle, CommercialNumber>(
                    It.Is<VehicleByVinSpecification>(s => s.Vin == _vehicle.Specification.Vin), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_vehicle);

            //Act
            var e = await Assert.ThrowsAsync<InvalidDomainOperationException>(() => _handler.Handle(_commandVin, CancellationToken.None));

            //Assert
            Assert.Contains("is temporary unavailable", e.Message.ToLower());
        }

        [Fact]
        public Task Handle_ByVin_WhenVehicle_IsAlreadyReserved_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Require<Vehicle, CommercialNumber>(
                    It.Is<VehicleByVinSpecification>(s => s.Vin == _vehicle.Specification.Vin), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_vehicle);
            _repository.Setup(r => r.Has(It.Is<ActualByVinSpecification>(s => s.Vin == _vehicle.Specification.Vin), It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //Act
            return Assert.ThrowsAsync<IsReservedException>(() => _handler.Handle(_commandVin, CancellationToken.None));
        }
    }
}
