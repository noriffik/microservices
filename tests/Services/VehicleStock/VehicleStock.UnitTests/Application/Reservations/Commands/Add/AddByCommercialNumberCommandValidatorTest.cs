﻿using FluentValidation.TestHelper;
using NexCore.VehicleStock.Application.Reservations.Commands.Add;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Reservations.Commands.Add
{
    public class AddByCommercialNumberCommandValidatorTest
    {
        private readonly AddByCommercialNumberCommandValidator _validator = new AddByCommercialNumberCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldHaveAnError_WhenCustomerId_LessThanOrEqualToZero(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.CustomerId, value);
        }

        [Fact]
        public void ShouldNotHaveAnError_WhenCustomerId_GreaterThanZero()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.CustomerId, 1);
        }

        [Fact]
        public void ShouldNotHaveAnError_WhenCustomerId_IsNull()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.CustomerId, (int?)null);
        }

        [Theory]
        [InlineData(" ")]
        [InlineData("")]
        [InlineData(null)]
        public void ShouldHaveAnError_WhenCommercialNumber_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.CommercialNumber, value);
        }

        [Fact]
        public void ShouldNotHaveAnError_WhenCommercialNumber_IsNotEmpty()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.CommercialNumber, "CommercialNumber");
        }
    }
}
