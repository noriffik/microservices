﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleStock.Application.Reservations.Commands.Complete;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Reservations.Commands.Complete
{
    public class CompleteCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Reservation _reservation;

        //Command and handler
        private readonly CompleteCommand _command;
        private readonly CompleteCommandHandler _handler;

        public CompleteCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entity
            _reservation = fixture.Create<Reservation>();

            //Command and handler
            _command = fixture.Create<CompleteCommand>();
            _handler = new CompleteCommandHandler(_work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(CompleteCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Require<Reservation>(_command.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_reservation);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(ReservationStatus.Completed, _reservation.Status);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request",
                () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
