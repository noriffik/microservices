﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleStock.Application.Reservations.Commands.Cancel;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Reservations.Commands.Cancel
{
    public class CancelCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Reservation _reservation;

        //Command and handler
        private readonly CancelCommand _command;
        private readonly CancelCommandHandler _handler;

        public CancelCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entity
            _reservation = fixture.Create<Reservation>();

            //Command and handler
            _command = fixture.Create<CancelCommand>();
            _handler = new CancelCommandHandler(_work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(CancelCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Require<Reservation>(_command.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_reservation);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(ReservationStatus.Canceled, _reservation.Status);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request",
                () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
