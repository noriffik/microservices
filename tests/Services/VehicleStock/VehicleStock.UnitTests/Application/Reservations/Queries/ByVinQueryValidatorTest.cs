﻿using FluentValidation.TestHelper;
using NexCore.VehicleStock.Application.Reservations.Queries;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Reservations.Queries
{
    public class ByVinQueryValidatorTest
    {
        private readonly ByVinQueryValidator _validator = new ByVinQueryValidator();

        [Theory]
        [InlineData(" ")]
        [InlineData("")]
        [InlineData(null)]
        public void ShouldHaveAnError_WhenVin_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Vin, value);
        }

        [Fact]
        public void ShouldNotHaveAnError_WhenVin_IsNotEmpty()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Vin, "Vin");
        }
    }
}
