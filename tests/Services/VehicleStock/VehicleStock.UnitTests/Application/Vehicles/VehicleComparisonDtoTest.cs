﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleStock.Application.Vehicles.Queries;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Linq;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Vehicles
{
    public class VehicleComparisonDtoTest
    {
        private readonly Fixture _fixture;
        private readonly Vehicle _vehicle;
        private readonly VehicleSpecification _specification;
        private readonly PrNumberSet _prNumberSet;
        private readonly ColorId _colorId;

        public VehicleComparisonDtoTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new VehicleBuilder());

            _prNumberSet = new PrNumberSet(_fixture.CreateMany<PrNumber>(10));
            _colorId = _fixture.Create<ColorId>();

            _specification = new VehicleSpecification(
                _fixture.Create<ModelKey>(), 
                _prNumberSet, "", "",
                _colorId);

            _vehicle = new Vehicle(_fixture.Create<CommercialNumber>(),
                _specification, _fixture.Create<WarehouseDelivery>(), 
                _fixture.Create<int>());
        }

        [Fact]
        public void Create_GivenVehicle_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("vehicle", () => VehicleComparisonDto.Create(null, _prNumberSet, _colorId));
        }

        [Fact]
        public void Create_GivenOptions_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("options", () => VehicleComparisonDto.Create(_vehicle, null, _colorId));
        }

        [Fact]
        public void Create_GivenColorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("colorId", () => VehicleComparisonDto.Create(_vehicle, _prNumberSet, null));
        }

        [Fact]
        public void Create_CompletelyMatching()
        {
            //Act
            var actual = VehicleComparisonDto.Create(_vehicle, _vehicle.Specification.Options, _colorId);

            //Assert
            Assert.Equal(true, actual.IsColorMatched);
            Assert.Equal(0, actual.AbsentOptionsCount);
            Assert.Equal(100, actual.MatchedOptionsPercentage);
            Assert.Equal(0, actual.AdditionalOptionCount);
            Assert.Equal(0, actual.AdditionalOptionPercentage);
        }

        [Fact]
        public void Create_PartiallyMatching()
        {
            //Arrange
            var additionalOptions = new PrNumberSet(_fixture.CreateMany<PrNumber>(10));
            var absentOptions = _specification.Options.Values.ToList().Take(5);
            var requiredOptions = _specification.Options.Shrink(absentOptions).Expand(additionalOptions);

            //Act
            var actual = VehicleComparisonDto.Create(_vehicle, requiredOptions, _fixture.Create<ColorId>());

            //Assert
            Assert.Equal(false, actual.IsColorMatched);
            Assert.Equal(10, actual.AbsentOptionsCount);
            Assert.Equal(33, actual.MatchedOptionsPercentage);
            Assert.Equal(5, actual.AdditionalOptionCount);
            Assert.Equal(33, actual.AdditionalOptionPercentage);
        }
    }
}
