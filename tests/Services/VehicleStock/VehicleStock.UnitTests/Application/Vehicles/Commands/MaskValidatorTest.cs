﻿using FluentValidation.TestHelper;
using NexCore.VehicleStock.Application.Vehicles.Commands;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Vehicles.Commands
{
    public class MaskValidatorTest
    {
        private readonly MaskValidator _validator = new MaskValidator();

        [Theory]
        [InlineData("ab1111")]
        [InlineData("1111111")]
        [InlineData("#11101")]
        [InlineData("121109")]
        [InlineData("11109")]
        [InlineData("11101")]
        [InlineData("")]
        public void ShouldHaveError_WhenMask_IsInvalidOrEmpty(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }
        
        [Theory]
        [InlineData("000009")]
        [InlineData("000018")]
        [InlineData("000107")]
        [InlineData("001006")]
        [InlineData("010005")]
        [InlineData("100004")]
        [InlineData("010103")]
        [InlineData("001012")]
        [InlineData("101011")]
        public void ShouldNotHaveError_WhenMask_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }
    }
}
