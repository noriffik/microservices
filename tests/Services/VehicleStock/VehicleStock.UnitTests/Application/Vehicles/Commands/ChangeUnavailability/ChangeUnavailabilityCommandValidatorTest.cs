﻿using FluentValidation.TestHelper;
using NexCore.VehicleStock.Application.Vehicles.Commands.ChangeUnavailability;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Vehicles.Commands.ChangeUnavailability
{
    public class ChangeUnavailabilityCommandValidatorTest
    {
        private readonly ChangeUnavailabilityCommandValidator _validator = new ChangeUnavailabilityCommandValidator();

        [Theory]
        [InlineData("123456")]
        [InlineData("q1@qw3$%sad")]
        public void ShouldNotHaveError_WhenCommercialNumber_IsNotEmpty(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.CommercialNumber, value);
        }

        [Theory]
        [InlineData("")]
        [InlineData("  ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenCommercialNumber_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.CommercialNumber, value);
        }
    }
}
