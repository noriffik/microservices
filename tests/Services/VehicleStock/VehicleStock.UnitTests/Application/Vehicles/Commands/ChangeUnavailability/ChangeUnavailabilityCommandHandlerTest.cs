﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleStock.Application.Vehicles.Commands.ChangeUnavailability;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Vehicles.Commands.ChangeUnavailability
{
    public class ChangeUnavailabilityCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Vehicle _vehicle;

        //Command and handler
        private readonly ChangeUnavailabilityCommand _command;
        private readonly ChangeUnavailabilityCommandHandler _handler;

        public ChangeUnavailabilityCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entity
            _vehicle = fixture.Create<Vehicle>();

            //Command and handler
            _command = fixture.Build<ChangeUnavailabilityCommand>()
                .With(c => c.CommercialNumber, _vehicle.Id.Value)
                .With(c => c.IsTemporaryUnavailable, false)
                .Create();
            _handler = new ChangeUnavailabilityCommandHandler(_work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ChangeUnavailabilityCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<Vehicle, CommercialNumber>(_vehicle.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicle);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }

            //Assert
            Assert.Equal(_command.IsTemporaryUnavailable, _vehicle.IsTemporaryUnavailable);
        }
    }
}
