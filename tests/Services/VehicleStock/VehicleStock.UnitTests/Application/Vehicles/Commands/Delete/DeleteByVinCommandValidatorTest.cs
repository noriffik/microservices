﻿using FluentValidation.TestHelper;
using NexCore.VehicleStock.Application.Vehicles.Commands.Delete;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Vehicles.Commands.Delete
{
    public class DeleteByVinCommandValidatorTest
    {
        private readonly DeleteByVinCommandValidator _validator = new DeleteByVinCommandValidator();

        [Theory]
        [InlineData("123456")]
        [InlineData("q1@qw3$%sad")]
        public void ShouldNotHaveError_WhenVin_IsNotEmpty(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Vin, value);
        }

        [Theory]
        [InlineData("")]
        [InlineData("  ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenVin_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Vin, value);
        }
    }
}
