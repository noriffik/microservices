﻿using FluentValidation.TestHelper;
using NexCore.VehicleStock.Application.Vehicles.Commands.Delete;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Vehicles.Commands.Delete
{
    public class DeleteByIdCommandValidatorTest
    {
        private readonly DeleteByIdCommandValidator _validator = new DeleteByIdCommandValidator();

        [Theory]
        [InlineData("123456")]
        [InlineData("q1@qw3$%sad")]
        public void ShouldNotHaveError_WhenId_IsNotEmpty(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Id, value);
        }

        [Theory]
        [InlineData("")]
        [InlineData("  ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenId_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Id, value);
        }
    }
}
