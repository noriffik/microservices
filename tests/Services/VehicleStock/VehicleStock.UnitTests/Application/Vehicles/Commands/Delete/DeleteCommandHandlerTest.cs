﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleStock.Application.Vehicles.Commands.Delete;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Reservations.Specifications;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Vehicles.Specifications;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Vehicles.Commands.Delete
{

    public class DeleteCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Vehicle _vehicle;

        //Command and handler
        private readonly DeleteByIdCommand _commandById;
        private readonly DeleteByVinCommand _commandByVin;
        private readonly DeleteCommandHandler _handler;

        public DeleteCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entity
            _vehicle = fixture.Create<Vehicle>();

            //Command and handler
            _commandById = fixture.Build<DeleteByIdCommand>()
                .With(c => c.Id, _vehicle.Id.Value)
                .Create();
            _commandByVin = fixture.Build<DeleteByVinCommand>()
                .With(c => c.Vin, _vehicle.Specification.Vin)
                .Create();
            _handler = new DeleteCommandHandler(_work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DeleteCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_ById_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request",
                () => _handler.Handle(null as DeleteByIdCommand, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_ById()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Require<Vehicle, CommercialNumber>(
                        It.Is<IdSpecification<Vehicle, CommercialNumber>>(s => s.Id == _vehicle.Id), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicle);
                _repository.Setup(r => r.Has(It.Is<ActualByVinSpecification>(s => s.Vin == _vehicle.Specification.Vin), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Remove<Vehicle, CommercialNumber>(_vehicle))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_commandById, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_ById_WhenVehicle_IsReserved_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Require<Vehicle, CommercialNumber>(
                    It.Is<IdSpecification<Vehicle, CommercialNumber>>(s => s.Id == _vehicle.Id), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_vehicle);
            _repository.Setup(r => r.Has(It.Is<ActualByVinSpecification>(s => s.Vin == _vehicle.Specification.Vin), It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //Assert
            return Assert.ThrowsAsync<IsReservedException>(() =>
                _handler.Handle(_commandById, CancellationToken.None));
        }

        [Fact]
        public Task Handle_ByVin_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request",
                () => _handler.Handle(null as DeleteByVinCommand, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_ByVin()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Require<Vehicle, CommercialNumber>(
                        It.Is<VehicleByVinSpecification>(s => s.Vin == _vehicle.Specification.Vin), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicle);
                _repository.Setup(r => r.Has(It.Is<ActualByVinSpecification>(s => s.Vin == _vehicle.Specification.Vin), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Remove<Vehicle, CommercialNumber>(_vehicle))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_commandByVin, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_ByVin_WhenVehicle_IsReserved_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Require<Vehicle, CommercialNumber>(
                    It.Is<VehicleByVinSpecification>(s => s.Vin == _vehicle.Specification.Vin), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_vehicle);
            _repository.Setup(r => r.Has(It.Is<ActualByVinSpecification>(s => s.Vin == _vehicle.Specification.Vin), It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //Assert
            return Assert.ThrowsAsync<IsReservedException>(() =>
                _handler.Handle(_commandByVin, CancellationToken.None));
        }
    }
}
