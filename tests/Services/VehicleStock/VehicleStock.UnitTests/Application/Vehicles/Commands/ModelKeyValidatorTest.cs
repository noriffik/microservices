﻿using FluentValidation.TestHelper;
using NexCore.VehicleStock.Application.Vehicles.Commands;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Vehicles.Commands
{
    public class ModelKeyValidatorTest
    {
        private readonly ModelKeyValidator _validator = new ModelKeyValidator();

        [Theory]
        [InlineData("QQQQ RRR")]
        [InlineData("WW RRR")]
        [InlineData(" ")]
        [InlineData("")]
        [InlineData("#$@GWE")]
        public void ShouldHaveError_WhenOneOfPrNumber_IsInvalid_IsEmpty(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }

        [Theory]
        [InlineData("WWWGRG")]
        [InlineData("123RHE")]
        [InlineData("123456")]
        public void ShouldNotHaveError_WhenAllId_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }
    }
}
