﻿using FluentValidation.TestHelper;
using NexCore.VehicleStock.Application.Vehicles.Commands;
using NexCore.VehicleStock.Application.Vehicles.Commands.Add;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Vehicles.Commands.Add
{
    public class AddCommandValidatorTest
    {
        private readonly AddCommandValidator _validator = new AddCommandValidator();

        [Theory]
        [InlineData("123456")]
        [InlineData("q1@qw3$%sad")]
        public void ShouldNotHaveError_WhenCommercialNumber_IsNotEmpty(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.CommercialNumber, value);
        }

        [Theory]
        [InlineData("")]
        [InlineData("  ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenCommercialNumber_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.CommercialNumber, value);
        }

        [Theory]
        [InlineData("123456")]
        [InlineData("q1@w$%sad")]
        public void ShouldNotHaveError_WhenVin_IsNotEmpty(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Vin, value);
        }

        [Theory]
        [InlineData("")]
        [InlineData("  ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenVin_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Vin, value);
        }

        [Fact]
        public void ShouldHaveChildValidator_ModelKeyValidator_ForModelKey()
        {
            _validator.ShouldHaveChildValidator(c => c.ModelKey, typeof(ModelKeyValidator));
        }

        [Fact]
        public void ShouldHaveError_WhenModelKey_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ModelKey, null as string);
        }

        [Theory]
        [InlineData("123456")]
        [InlineData("q1@w$%sad")]
        public void ShouldNotHaveError_WhenEngine_IsNotEmpty(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Engine, value);
        }

        [Theory]
        [InlineData("")]
        [InlineData("  ")]
        [InlineData(null)]
        public void ShouldHaveError_WhenEngine_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Engine, value);
        }


        [Fact]
        public void ShouldHaveChildValidator_PrNumberSetValidator_ForOptions()
        {
            _validator.ShouldHaveChildValidator(c => c.Options, typeof(PrNumberSetValidator));
        }

        [Fact]
        public void ShouldHaveError_WhenOptions_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Options, null as string);
        }
    }
}
