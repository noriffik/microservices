﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleStock.Application.Vehicles.Commands.Add;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Vehicles.Specifications;
using NexCore.VehicleStock.Domain.Warehouses;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Vehicles.Commands.Add
{
    public class AddCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Vehicle _vehicle;

        //Command and handler
        private readonly AddCommand _command;
        private readonly AddCommandHandler _handler;

        public AddCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            var mapper = new Mock<IMapper>();

            //Entity
            _vehicle = fixture.Create<Vehicle>();

            //Command
            _command = fixture.Create<AddCommand>();

            //Setup
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            mapper.Setup(m => m.Map<Vehicle>(_command)).Returns(_vehicle);

            //Handler
            _handler = new AddCommandHandler(_work.Object, mapper.Object);
        }


        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(w => w.HasRequired<Warehouse>(_vehicle.WarehouseId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.Has<Vehicle, CommercialNumber>(
                        It.Is<VehicleByIdOrVinSpecification>(s => s.Vin == _vehicle.Specification.Vin && s.CommercialNumber == _vehicle.Id),
                        It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Add<Vehicle, CommercialNumber>(_vehicle))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_vehicle.Id.Value, result);
            }
        }

        [Fact]
        public async Task Handle_WhenVehicle_AlreadyExist_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Has<Vehicle, CommercialNumber>(
                    It.Is<VehicleByIdOrVinSpecification>(s => s.Vin == _vehicle.Specification.Vin && s.CommercialNumber == _vehicle.Id),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.ThrowsAsync<DuplicateVehicleException>(() =>
                _handler.Handle(_command, CancellationToken.None));

            //Assert
            Assert.Equal(_vehicle.Id, e.CommercialNumber);
            Assert.Equal(_vehicle.Specification.Vin, e.Vin);
        }
    }
}
