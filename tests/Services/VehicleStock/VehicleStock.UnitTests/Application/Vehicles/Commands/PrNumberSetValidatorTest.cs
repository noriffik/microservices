﻿using FluentValidation.TestHelper;
using NexCore.VehicleStock.Application.Vehicles.Commands;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Application.Vehicles.Commands
{
    public class PrNumberSetValidatorTest
    {
        private readonly PrNumberSetValidator _validator = new PrNumberSetValidator();

        [Theory]
        [InlineData("QQQQ RRR")]
        [InlineData("WW RRR")]
        [InlineData("#$@ GWE")]
        public void ShouldHaveError_WhenOneOfPrNumber_IsInvalid_IsEmpty(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }

        [Theory]
        [InlineData("QQQ")]
        [InlineData("WWW")]
        [InlineData("123")]
        [InlineData("F4F")]
        [InlineData("QQQ RRR")]
        [InlineData("WWW GRG")]
        [InlineData("123 RHE")]
        [InlineData("F4F W6T")]
        public void ShouldNotHaveError_WhenAllId_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }
    }
}
