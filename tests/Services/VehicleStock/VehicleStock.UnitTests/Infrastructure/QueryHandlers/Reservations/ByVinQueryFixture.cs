﻿using NexCore.VehicleStock.Application.Reservations.Queries;
using NexCore.VehicleStock.Application.Vehicles.Queries;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Infrastructure;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.QueryHandlers;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.UnitTests.Infrastructure.QueryHandlers.Reservations
{
    public class ByVinQueryFixture : QueryFixture<ByVinQuery, IEnumerable<ReservationDto>>
    {
        private IReadOnlyList<Reservation> _reservations;
        private Vehicle _vehicle;

        public override async Task Setup(VehicleStockContext context)
        {
            await base.Setup(context);

            var seeder = new ReservationSeeder(context);

            //Seed unexpected reservations
            await seeder.AddRange(3);

            //Seed expected reservations
            _vehicle = await seeder.Add<Vehicle, CommercialNumber>();
            _reservations = await seeder.AddRange(3, new
            {
                vin = _vehicle.Specification.Vin,
                colorId = _vehicle.Specification.ColorId,
                vehicle = _vehicle.Specification,
                warehouseId = _vehicle.WarehouseId
            });

            await seeder.SeedAsync();
        }

        public override ByVinQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new ByVinQuery { Vin = _vehicle.Specification.Vin };
        }

        public override ByVinQuery GetUnpreparedQuery()
        {
            return new ByVinQuery { Vin = "VIN" };
        }

        public override IEnumerable<ReservationDto> GetResponse()
        {
            var vehicleDto = new VehicleDto
            {
                CommercialNumber = _vehicle.Id,
                Vin = _vehicle.Specification.Vin,
                ColorId = _vehicle.Specification.ColorId,
                ModelKey = _vehicle.Specification.ModelKey,
                Options = _vehicle.Specification.Options,
                Engine = _vehicle.Specification.Engine,
                AssembledOn = _vehicle.WarehouseDelivery.AssembledOn,
                DeliveredOn = _vehicle.WarehouseDelivery.DeliveredOn,
                WarehouseId = _vehicle.WarehouseId,
                IsTemporaryUnavailable = _vehicle.IsTemporaryUnavailable,
                ReservationState = new ReservationStateDto
                {
                    ReservationId = _vehicle.ReservationState.ReservationId,
                    Status = _vehicle.ReservationState.Status,
                    OnDate = _vehicle.ReservationState.OnDate
                }
            };

            return _reservations.Select(reservation => new ReservationDto
            {
                Id = reservation.Id,
                OnDate = reservation.OnDate,
                CustomerId = reservation.CustomerId,
                Vin = reservation.Vehicle.Vin,
                ColorId = reservation.Vehicle.ColorId,
                ModelKey = reservation.Vehicle.ModelKey,
                Options = reservation.Vehicle.Options,
                Engine = reservation.Vehicle.Engine,
                WarehouseId = reservation.WarehouseId,
                Status = reservation.Status,
                Vehicle = vehicleDto
            });
        }
    }
}
