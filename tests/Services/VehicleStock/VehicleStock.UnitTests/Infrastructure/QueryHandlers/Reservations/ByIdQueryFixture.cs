﻿using NexCore.VehicleStock.Application.Reservations.Queries;
using NexCore.VehicleStock.Application.Vehicles.Queries;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Infrastructure;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.QueryHandlers;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Linq;
using System.Threading.Tasks;
using ByIdQuery = NexCore.VehicleStock.Application.Reservations.Queries.ByIdQuery;

namespace NexCore.VehicleStock.UnitTests.Infrastructure.QueryHandlers.Reservations
{
    public class ByIdQueryFixture : QueryFixture<ByIdQuery, ReservationDto>
    {
        private Reservation _reservation;
        private Vehicle _vehicle;

        public override async Task Setup(VehicleStockContext context)
        {
            await base.Setup(context);

            var seeder = new ReservationSeeder(context);

            //Seed unexpected reservations
            await seeder.AddRange(3);

            //Seed expected reservation
            _reservation = await seeder.Add();
            _vehicle = seeder.Get<Vehicle, CommercialNumber>()
                .Single(v => v.Specification.Vin == _reservation.Vehicle.Vin);

            await seeder.SeedAsync();
        }

        public override ByIdQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new ByIdQuery { Id = _reservation.Id };
        }

        public override ByIdQuery GetUnpreparedQuery()
        {
            return new ByIdQuery { Id = 100 };
        }

        public override ReservationDto GetResponse()
        {
            return new ReservationDto
            {
                Id = _reservation.Id,
                OnDate = _reservation.OnDate,
                CustomerId = _reservation.CustomerId,
                Vin = _reservation.Vehicle.Vin,
                ColorId = _reservation.Vehicle.ColorId,
                ModelKey = _reservation.Vehicle.ModelKey,
                Options = _reservation.Vehicle.Options,
                Engine = _reservation.Vehicle.Engine,
                WarehouseId = _reservation.WarehouseId,
                Status = _reservation.Status,
                Vehicle = new VehicleDto
                {
                    CommercialNumber = _vehicle.Id,
                    Vin = _vehicle.Specification.Vin,
                    ColorId = _vehicle.Specification.ColorId,
                    ModelKey = _vehicle.Specification.ModelKey,
                    Options = _vehicle.Specification.Options,
                    Engine = _vehicle.Specification.Engine,
                    AssembledOn = _vehicle.WarehouseDelivery.AssembledOn,
                    DeliveredOn = _vehicle.WarehouseDelivery.DeliveredOn,
                    WarehouseId = _vehicle.WarehouseId,
                    IsTemporaryUnavailable = _vehicle.IsTemporaryUnavailable,
                    ReservationState = new ReservationStateDto
                    {
                        ReservationId = _vehicle.ReservationState.ReservationId,
                        Status = _vehicle.ReservationState.Status,
                        OnDate = _vehicle.ReservationState.OnDate
                    }
                }
            };
        }
    }
}
