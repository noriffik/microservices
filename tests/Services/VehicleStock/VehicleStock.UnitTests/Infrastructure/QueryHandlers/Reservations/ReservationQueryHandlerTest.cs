﻿using AutoMapper;
using NexCore.Testing;
using NexCore.VehicleStock.Application.Reservations.Queries;
using NexCore.VehicleStock.Infrastructure.QueryHandlers.Reservations;
using NexCore.VehicleStock.UnitTests.Fixtures;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Infrastructure.QueryHandlers.Reservations
{
    public class ReservationQueryHandlerTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new ReservationQueryHandler(null, _mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = InMemoryDbContextFactory.Instance.Create())
                {
                    return new ReservationQueryHandler(context, null);
                }
            });
        }

        //ByIdQuery

        [Fact]
        public async Task HandleById()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByIdQueryFixture();
                await fixture.Setup(context);

                var handler = new ReservationQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(fixture.GetResponse(), actual, PropertyComparer<ReservationDto>.Instance);
            }
        }

        [Fact]
        public Task HandleById_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new ReservationQueryHandler(context, _mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as ByIdQuery, CancellationToken.None));
            }
        }

        [Fact]
        public async Task HandleById_WhenNothing_IsFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByIdQueryFixture();
                var handler = new ReservationQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(fixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }

        //ByVinQuery

        [Fact]
        public async Task HandleByVin()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByVinQueryFixture();
                await fixture.Setup(context);

                var handler = new ReservationQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(fixture.GetResponse(), actual, PropertyComparer<ReservationDto>.Instance);
            }
        }

        [Fact]
        public Task HandleByVin_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new ReservationQueryHandler(context, _mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as ByVinQuery, CancellationToken.None));
            }
        }

        [Fact]
        public async Task HandleByVin_WhenNothing_IsFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByVinQueryFixture();
                var handler = new ReservationQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(fixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Empty(result);
            }
        }
    }
}
