﻿using NexCore.VehicleStock.Infrastructure.QueryHandlers.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Infrastructure.QueryHandlers.Vehicles
{
    public class MaskTest
    {
        [Theory]
        [InlineData("101012", true, false, true, false, true, 2)]
        [InlineData("111113", true, true, true, true, true, 3)]
        [InlineData("010114", false, true, false, true, true, 4)]
        public void Parse(string value, bool bodyRequires, bool equipmentRequires, bool engineRequires, bool gearboxRequires, bool colorRequires, int dispensableOptionsCount)
        {
            //Act
            var mask = ComparisonMask.Parse(value);

            //Assert
            Assert.NotNull(mask);
            Assert.Equal(bodyRequires, mask.IsBodyRequired);
            Assert.Equal(equipmentRequires, mask.IsEquipmentRequired);
            Assert.Equal(engineRequires, mask.IsEngineRequired);
            Assert.Equal(gearboxRequires, mask.IsGearboxRequired);
            Assert.Equal(colorRequires, mask.IsColorRequired);
            Assert.Equal(dispensableOptionsCount, mask.DispensableOptionsCount);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                ComparisonMask.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("NFF")]
        [InlineData("N$")]
        [InlineData("N ")]
        [InlineData("F")]
        [InlineData("NFXX")]
        [InlineData("NFXXX")]
        [InlineData("0101")]
        [InlineData("010134")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>
            {
                ComparisonMask.Parse(value);
            });
        }
    }
}
