﻿using AutoMapper;
using NexCore.Testing;
using NexCore.VehicleStock.Application.Vehicles.Queries;
using NexCore.VehicleStock.Infrastructure.QueryHandlers.Vehicles;
using NexCore.VehicleStock.UnitTests.Fixtures;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Infrastructure.QueryHandlers.Vehicles
{
    public class VehicleQueryHandlerTest
    {
        private readonly IMapper _mapper = AutoMapperFactory.Create();

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new VehicleQueryHandler(null, _mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = InMemoryDbContextFactory.Instance.Create())
                {
                    return new VehicleQueryHandler(context, null);
                }
            });
        }

        //FindByIdQuery

        [Fact]
        public async Task HandleById()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByIdQueryFixture();

                await fixture.Setup(context);

                var handler = new VehicleQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(fixture.GetResponse(), actual, PropertyComparer<VehicleDto>.Instance);
            }
        }

        [Fact]
        public Task HandleById_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new VehicleQueryHandler(context, _mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as ByIdQuery, CancellationToken.None));
            }
        }

        [Fact]
        public async Task HandleById_WhenNothing_IsFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByIdQueryFixture();
                var handler = new VehicleQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(fixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }

        //FindAll

        [Fact]
        public async Task HandleAll()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new AnyQueryFixture();

                await fixture.Setup(context);

                var handler = new VehicleQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(fixture.GetResponse(), actual, PropertyComparer<VehicleDto>.Instance);
            }
        }

        [Fact]
        public Task HandleAll_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new VehicleQueryHandler(context, _mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as AnyQuery, CancellationToken.None));
            }
        }

        //FindByWarehouseId

        [Fact]
        public async Task HandleByWarehouseId()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByWarehouseIdQueryFixture();

                await fixture.Setup(context);

                var handler = new VehicleQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(fixture.GetResponse(), actual, PropertyComparer<VehicleDto>.Instance);
            }
        }

        [Fact]
        public Task HandleByWarehouseId_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new VehicleQueryHandler(context, _mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as AnyQuery, CancellationToken.None));
            }
        }

        [Fact]
        public async Task HandleByWarehouseId_WhenNothing_IsFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByWarehouseIdQueryFixture();
                var handler = new VehicleQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(fixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Empty(result);
            }
        }

        [Fact]
        public async Task HandleMatching()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new MatchingQueryFixture();

                await fixture.Setup(context);

                var handler = new VehicleQueryHandler(context, _mapper);

                //Act
                var expected = fixture.GetResponse();
                var actual = await handler.Handle(fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<MatchedVehicleDto>.Instance);
            }
        }

        [Fact]
        public Task HandleMatching_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new VehicleQueryHandler(context, _mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as MatchingQuery, CancellationToken.None));
            }
        }

        [Fact]
        public async Task HandleMatching_WhenNothing_IsFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new MatchingQueryFixture();
                var handler = new VehicleQueryHandler(context, _mapper);

                //Act
                var result = await handler.Handle(fixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Empty(result);
            }
        }
    }
}
