﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleStock.Application.Vehicles.Queries;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Infrastructure;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.QueryHandlers;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.UnitTests.Infrastructure.QueryHandlers.Vehicles
{
    public class MatchingQueryFixture : QueryFixture<MatchingQuery, IEnumerable<MatchedVehicleDto>>
    {
        private Vehicle _completelyMatchingVehicle;
        private IEnumerable<Vehicle> _vehicles;

        private ModelKey _modelKeyRequired;

        private PrNumberSet _optionsRequired;

        private ColorId _colorIdRequired;

        private VehicleSpecification _vehicleSpecification;

        public override async Task Setup(VehicleStockContext context)
        {
            await base.Setup(context);

            var seeder = new VehicleSeeder(context);

            //Seed unexpected vehicles
            await seeder.AddRange(5);

            //Seed  completely matching vehicles
            _modelKeyRequired = seeder.Create<ModelKey>();
            _optionsRequired = new PrNumberSet(seeder.CreateMany<PrNumber>(6));
            _colorIdRequired = seeder.Create<ColorId>();
            _vehicleSpecification = new VehicleSpecification(_modelKeyRequired, _optionsRequired, "", "", _colorIdRequired);
            _completelyMatchingVehicle = await seeder.Add(new { specification = _vehicleSpecification });
            _completelyMatchingVehicle.ChangeUnavailability(true);
            _completelyMatchingVehicle.ChangeReservationState(seeder.Create<ReservationState>());

            //Seed partial matching vehicles
            var firstPartialMatchingVehicle = await seeder.Add(new
            {
                specification = new VehicleSpecification(
                    _modelKeyRequired, 
                    _optionsRequired.Shrink(_optionsRequired.Values.ToList()[0]).Expand(seeder.CreateMany<PrNumber>(3)), "", "", 
                    _colorIdRequired)
            });
            firstPartialMatchingVehicle.ChangeUnavailability(true);
            firstPartialMatchingVehicle.ChangeReservationState(seeder.Create<ReservationState>());

            var secondPartialMatchingVehicle = await seeder.Add(new
            {
                specification = new VehicleSpecification(
                    _modelKeyRequired, 
                    _optionsRequired.Shrink(new[]{_optionsRequired.Values.ToList()[0], _optionsRequired.Values.ToList()[1]}).Expand(seeder.Create<PrNumber>()), "", "", 
                    _colorIdRequired)
            });
            secondPartialMatchingVehicle.ChangeUnavailability(true);
            secondPartialMatchingVehicle.ChangeReservationState(seeder.Create<ReservationState>());

            var thirdPartialMatchingVehicle = await seeder.Add(new
            {
                specification = new VehicleSpecification(
                    _modelKeyRequired, 
                    _optionsRequired.Shrink(new[]{_optionsRequired.Values.ToList()[0], _optionsRequired.Values.ToList()[1]}).Expand(seeder.CreateMany<PrNumber>(2)), "", "", 
                    _colorIdRequired)
            });
            thirdPartialMatchingVehicle.ChangeUnavailability(true);
            thirdPartialMatchingVehicle.ChangeReservationState(seeder.Create<ReservationState>());

            var forthPartialMatchingVehicle = await seeder.Add(new
            {
                specification = new VehicleSpecification(
                    _modelKeyRequired, 
                    _optionsRequired.Shrink(new[]{_optionsRequired.Values.ToList()[0], _optionsRequired.Values.ToList()[1]}).Expand(seeder.CreateMany<PrNumber>(2)), "", "", 
                    seeder.Create<ColorId>())
            });
            forthPartialMatchingVehicle.ChangeUnavailability(true);
            forthPartialMatchingVehicle.ChangeReservationState(seeder.Create<ReservationState>());

            _vehicles = new[]
            {
                _completelyMatchingVehicle,
                firstPartialMatchingVehicle,
                secondPartialMatchingVehicle,
                thirdPartialMatchingVehicle,
                forthPartialMatchingVehicle
            };

            await seeder.SeedAsync();
        }

        public override MatchingQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new MatchingQuery
            {
                ModelKey = _modelKeyRequired.Value,
                ColorId = _colorIdRequired.Value,
                Options = _optionsRequired.AsString,
                Mask = "111103"
            };
        }

        public override MatchingQuery GetUnpreparedQuery()
        {
            return new MatchingQuery
            {
                ModelKey = "XXXXXX",
                ColorId = "XXXX",
                Options = "AAA",
                Mask = "111100"
            };
        }

        public override IEnumerable<MatchedVehicleDto> GetResponse()
        {
            var result = new List<MatchedVehicleDto>();

            //Add to result partial matching vehicles
            foreach (var vehicle in _vehicles)
            {
                result.Add(new MatchedVehicleDto
                {
                    Vehicle = new VehicleDto
                    {
                        CommercialNumber = vehicle.Id,
                        ModelKey = vehicle.Specification.ModelKey.Value,
                        Engine = vehicle.Specification.Engine,
                        Options = vehicle.Specification.Options.AsString,
                        WarehouseId = vehicle.WarehouseId,
                        Vin = vehicle.Specification.Vin,
                        ColorId = vehicle.Specification.ColorId,
                        DeliveredOn = vehicle.WarehouseDelivery.DeliveredOn,
                        AssembledOn = vehicle.WarehouseDelivery.AssembledOn,
                        IsTemporaryUnavailable = vehicle.IsTemporaryUnavailable,
                        ReservationState = new ReservationStateDto
                        {
                            ReservationId = vehicle.ReservationState.ReservationId,
                            Status = vehicle.ReservationState.Status,
                            OnDate = vehicle.ReservationState.OnDate
                        }
                    },
                    Comparison = VehicleComparisonDto.Create(vehicle, _optionsRequired, _colorIdRequired)
                });
            }

            return result;
        }
    }
}
