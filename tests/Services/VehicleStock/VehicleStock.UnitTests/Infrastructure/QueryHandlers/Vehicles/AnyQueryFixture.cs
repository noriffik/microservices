﻿using NexCore.VehicleStock.Application.Vehicles.Queries;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Infrastructure;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.QueryHandlers;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.UnitTests.Infrastructure.QueryHandlers.Vehicles
{
    public class AnyQueryFixture : QueryFixture<AnyQuery, IEnumerable<VehicleDto>>
    {
        private IEnumerable<Vehicle> _vehicles;

        public override async Task Setup(VehicleStockContext context)
        {
            await base.Setup(context);

            var seeder = new VehicleSeeder(context);

            //Seed expected vehicles
            _vehicles = await seeder.AddRange(3);
            _vehicles.Last().ChangeUnavailability(true);
            _vehicles.Last().ChangeReservationState(seeder.Create<ReservationState>());

            await seeder.SeedAsync();
        }

        public override AnyQuery GetQuery()
        {
            return new AnyQuery();
        }

        public override AnyQuery GetUnpreparedQuery()
        {
            throw new NotSupportedException();
        }

        public override IEnumerable<VehicleDto> GetResponse()
        {
            return _vehicles.Select(vehicle => new VehicleDto
            {
                CommercialNumber = vehicle.Id,
                ModelKey = vehicle.Specification.ModelKey.Value,
                Engine = vehicle.Specification.Engine,
                Options = vehicle.Specification.Options.AsString,
                WarehouseId = vehicle.WarehouseId,
                Vin = vehicle.Specification.Vin,
                ColorId = vehicle.Specification.ColorId,
                DeliveredOn = vehicle.WarehouseDelivery.DeliveredOn,
                AssembledOn = vehicle.WarehouseDelivery.AssembledOn,
                IsTemporaryUnavailable = vehicle.IsTemporaryUnavailable,
                ReservationState = new ReservationStateDto
                {
                    ReservationId = vehicle.ReservationState.ReservationId,
                    Status = vehicle.ReservationState.Status,
                    OnDate = vehicle.ReservationState.OnDate
                }
            });
        }
    }
}
