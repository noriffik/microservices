﻿using NexCore.VehicleStock.Application.Vehicles.Queries;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Infrastructure;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.QueryHandlers;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.UnitTests.Infrastructure.QueryHandlers.Vehicles
{
    public class ByIdQueryFixture : QueryFixture<ByIdQuery, VehicleDto>
    {
        private Vehicle _vehicle;

        public override async Task Setup(VehicleStockContext context)
        {
            await base.Setup(context);

            var seeder = new VehicleSeeder(context);

            //Seed unexpected vehicles
            await seeder.AddRange(5);

            //Seed expected vehicles
            _vehicle = await seeder.Add();
            _vehicle.ChangeUnavailability(true);
            _vehicle.ChangeReservationState(seeder.Create<ReservationState>());

            await seeder.SeedAsync();
        }

        public override ByIdQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new ByIdQuery
            {
                Id = _vehicle.Id.Value
            };
        }

        public override ByIdQuery GetUnpreparedQuery()
        {
            return new ByIdQuery { Id = "" };
        }

        public override VehicleDto GetResponse()
        {
            return new VehicleDto
            {
                CommercialNumber = _vehicle.Id,
                ModelKey = _vehicle.Specification.ModelKey.Value,
                Engine = _vehicle.Specification.Engine,
                Options = _vehicle.Specification.Options.AsString,
                WarehouseId = _vehicle.WarehouseId,
                Vin = _vehicle.Specification.Vin,
                ColorId = _vehicle.Specification.ColorId,
                DeliveredOn = _vehicle.WarehouseDelivery.DeliveredOn,
                AssembledOn = _vehicle.WarehouseDelivery.AssembledOn,
                IsTemporaryUnavailable = _vehicle.IsTemporaryUnavailable,
                ReservationState = new ReservationStateDto
                {
                    ReservationId = _vehicle.ReservationState.ReservationId,
                    Status = _vehicle.ReservationState.Status,
                    OnDate = _vehicle.ReservationState.OnDate
                }
            };
        }
    }
}
