﻿using NexCore.VehicleStock.Application.Vehicles.Queries;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Infrastructure;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.QueryHandlers;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleStock.UnitTests.Infrastructure.QueryHandlers.Vehicles
{
    public class ByWarehouseIdQueryFixture : QueryFixture<ByWarehouseIdQuery, IEnumerable<VehicleDto>>
    {
        private IEnumerable<Vehicle> _vehicles;

        private int _warehouseId;

        public override async Task Setup(VehicleStockContext context)
        {
            await base.Setup(context);

            var seeder = new VehicleSeeder(context);

            //Seed unexpected vehicles
            await seeder.AddRange(5);

            //Seed expected vehicles
            _warehouseId = seeder.Create<int>();
            _vehicles = await seeder.AddRange(3, new { warehouseId = _warehouseId });
            _vehicles.Last().ChangeUnavailability(true);
            _vehicles.Last().ChangeReservationState(seeder.Create<ReservationState>());

            await seeder.SeedAsync();
        }

        public override ByWarehouseIdQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new ByWarehouseIdQuery
            {
                WarehouseId = _warehouseId
            };
        }

        public override ByWarehouseIdQuery GetUnpreparedQuery()
        {
            return new ByWarehouseIdQuery { WarehouseId = 123 };
        }

        public override IEnumerable<VehicleDto> GetResponse()
        {
            return _vehicles.Select(vehicle => new VehicleDto
            {
                CommercialNumber = vehicle.Id,
                ModelKey = vehicle.Specification.ModelKey.Value,
                Engine = vehicle.Specification.Engine,
                Options = vehicle.Specification.Options.AsString,
                WarehouseId = vehicle.WarehouseId,
                Vin = vehicle.Specification.Vin,
                ColorId = vehicle.Specification.ColorId,
                DeliveredOn = vehicle.WarehouseDelivery.DeliveredOn,
                AssembledOn = vehicle.WarehouseDelivery.AssembledOn,
                IsTemporaryUnavailable = vehicle.IsTemporaryUnavailable,
                ReservationState = new ReservationStateDto
                {
                    ReservationId = vehicle.ReservationState.ReservationId,
                    Status = vehicle.ReservationState.Status,
                    OnDate = vehicle.ReservationState.OnDate
                }
            });
        }
    }
}
