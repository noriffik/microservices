﻿using AutoFixture;
using NexCore.Common;
using NexCore.Domain;
using NexCore.Testing;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Reservations
{
    public class ReservationTest
    {
        private readonly Vehicle _vehicle;
        private readonly int _customerId;
        private readonly DateTime _date;

        private readonly Reservation _reservation;

        public ReservationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());

            _vehicle = fixture.Create<Vehicle>();
            _customerId = fixture.Create<int>();
            _date = fixture.Create<DateTime>();

            _reservation = fixture.Create<Reservation>();
        }

        [Fact]
        public void Ctor()
        {
            //Arrange
            using (new SystemTimeContext(_date))
            {
                var expectedEvent = new ReservedEvent(_vehicle.Specification.Vin);

                //Act
                var reservation = new Reservation(_vehicle.Specification, _vehicle.WarehouseId, _customerId);

                //Assert
                Assert.Equal(_vehicle.Specification, reservation.Vehicle);
                Assert.NotSame(_vehicle.Specification, reservation.Vehicle);
                Assert.Equal(_vehicle.WarehouseId, reservation.WarehouseId);
                Assert.Equal(_customerId, reservation.CustomerId);
                Assert.Equal(_date, reservation.OnDate);
                Assert.Contains(expectedEvent, reservation.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
            }
        }

        [Fact]
        public void Ctor_GivenVehicleSpecification_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("vehicle", () => new Reservation(null, _vehicle.WarehouseId, _customerId));
        }

        [Fact]
        public void Ctor_Vehicle()
        {
            //Arrange
            using (new SystemTimeContext(_date))
            {
                //Act
                var reservation = new Reservation(_vehicle, _customerId);

                //Assert
                Assert.Equal(_vehicle.Specification, reservation.Vehicle);
                Assert.NotSame(_vehicle.Specification, reservation.Vehicle);
                Assert.Equal(_vehicle.WarehouseId, reservation.WarehouseId);
                Assert.Equal(_customerId, reservation.CustomerId);
                Assert.Equal(_date, reservation.OnDate);
            }
        }

        [Fact]
        public void Ctor_Vehicle_GivenVehicle_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("vehicle", () => new Reservation(null, _customerId));
        }

        [Fact]
        public void Cancel()
        {
            //Arrange 
            var expectedEvent = new ReservationCanceledEvent(_reservation.Id);

            //Act
            _reservation.Cancel();

            //Assert
            Assert.Equal(ReservationStatus.Canceled, _reservation.Status);
            Assert.Contains(expectedEvent, _reservation.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void Cancel_WhenReservation_IsNotActual()
        {
            //Arrange
            _reservation.Complete();

            //Assert
            Assert.Throws<ReservationIsNotActualOperationException>(() => _reservation.Cancel());
        }

        [Fact]
        public void Complete()
        {
            //Arrange
            var expectedEvent = new ReservationCompletedEvent(_reservation.Id);

            //Act
            _reservation.Complete();

            //Assert
            Assert.Equal(ReservationStatus.Completed, _reservation.Status);
            Assert.Contains(expectedEvent, _reservation.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void Complete_WhenReservation_IsNotActual()
        {
            //Arrange
            _reservation.Cancel();

            //Assert
            Assert.Throws<ReservationIsNotActualOperationException>(() => _reservation.Complete());
        }
    }
}
