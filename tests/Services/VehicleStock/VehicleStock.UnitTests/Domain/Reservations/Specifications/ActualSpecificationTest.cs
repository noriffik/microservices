﻿using AutoFixture;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Reservations.Specifications;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Reservations.Specifications
{
    public class ActualSpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<Reservation> _expected;
        private readonly IReadOnlyList<Reservation> _unexpected;
        private readonly IReadOnlyList<Reservation> _all;

        //Specification
        private readonly ActualSpecification _specification;

        public ActualSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());

            _expected = fixture.CreateMany<Reservation>(3).ToList();

            _unexpected = fixture.CreateMany<Reservation>(7).ToList();
            var canceled = _unexpected.Take(4).ToList();
            canceled.ForEach(r => r.Cancel());
            _unexpected.Except(canceled).ToList().ForEach(r => r.Complete());

            _all = _unexpected.Concat(_expected).ToList();

            _specification = new ActualSpecification();
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
