﻿using AutoFixture;
using NexCore.Testing.Extensions;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Reservations.Specifications;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Reservations.Specifications
{
    public class ByVinSpecificationTest
    {
        private readonly string _vin;

        //Entities
        private readonly IReadOnlyList<Reservation> _expected;
        private readonly IReadOnlyList<Reservation> _unexpected;
        private readonly IReadOnlyList<Reservation> _all;

        //Specification
        private readonly ByVinSpecification _specification;

        public ByVinSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());

            _vin = fixture.Create<string>();

            _expected = fixture.ConstructMany<VehicleSpecification>(3, new { vin = _vin })
                .Select(vehicle => fixture.Construct<Reservation>(3, new { vehicle }))
                .ToList();
            _unexpected = fixture.CreateMany<Reservation>(5).ToList();
            _all = _unexpected.Concat(_expected).ToList();

            _specification = new ByVinSpecification(_vin);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var specification = new ByVinSpecification(_vin);

            //Assert
            Assert.Equal(_vin.ToUpperInvariant(), specification.Vin);
        }

        [Fact]
        public void Ctor_WhenGivenVin_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("vin", () => new ByVinSpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
