﻿using AutoFixture;
using NexCore.DealerNetwork;
using NexCore.VehicleStock.Domain.Warehouses.DistributorWarehouses;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Warehouses.DistributorWarehouses
{
    public class DistributorWarehouseTest
    {
        private readonly int _id;
        private readonly DistributorId _distributorId;
        private readonly string _name;
        private readonly int _companyId;

        public DistributorWarehouseTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            _id = fixture.Create<int>();
            _distributorId = fixture.Create<DistributorId>();
            _name = fixture.Create<string>();
            _companyId = fixture.Create<int>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var warehouse = new DistributorWarehouse(_distributorId, _name, _companyId);

            //Assert
            Assert.Equal(_distributorId, warehouse.DistributorId);
            Assert.Equal(_name, warehouse.Name);
            Assert.Equal(_companyId, warehouse.CompanyId);
        }

        [Fact]
        public void Ctor_WithId()
        {
            //Act
            var warehouse = new DistributorWarehouse(_id, _distributorId, _name, _companyId);

            //Assert
            Assert.Equal(_id, warehouse.Id);
            Assert.Equal(_distributorId, warehouse.DistributorId);
            Assert.Equal(_name, warehouse.Name);
            Assert.Equal(_companyId, warehouse.CompanyId);
        }

        [Fact]
        public void Ctor_GivenDistributorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("distributorId", () => new DistributorWarehouse(null, _name, _companyId));
        }

        [Fact]
        public void Ctor_GivenName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("name", () => new DistributorWarehouse(_distributorId, null, _companyId));
        }
    }
}
