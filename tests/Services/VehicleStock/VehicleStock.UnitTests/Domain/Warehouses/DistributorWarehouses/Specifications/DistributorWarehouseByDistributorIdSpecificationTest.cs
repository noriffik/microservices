﻿using AutoFixture;
using NexCore.DealerNetwork;
using NexCore.Testing.Extensions;
using NexCore.VehicleStock.Domain.Warehouses.DistributorWarehouses;
using NexCore.VehicleStock.Domain.Warehouses.DistributorWarehouses.Specifications;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Warehouses.DistributorWarehouses.Specifications
{
    public class DistributorWarehouseByDistributorIdSpecificationTest
    {
        private readonly DistributorId _distributorId;

        //Entities
        private readonly IReadOnlyList<DistributorWarehouse> _expected;
        private readonly IReadOnlyList<DistributorWarehouse> _unexpected;
        private readonly IReadOnlyList<DistributorWarehouse> _all;

        //Specification
        private readonly DistributorWarehouseByDistributorIdSpecification _specification;

        public DistributorWarehouseByDistributorIdSpecificationTest()
        {
            var fixture = new Fixture()
                .Customize(new DistributorIdCustomization());

            _distributorId = fixture.Create<DistributorId>();

            _expected = fixture.ConstructMany<DistributorWarehouse>(3, new { distributorId = _distributorId }).ToList();
            _unexpected = fixture.CreateMany<DistributorWarehouse>(2).ToList();
            _all = _unexpected.Concat(_expected).ToList();

            _specification = new DistributorWarehouseByDistributorIdSpecification(_distributorId);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var specification = new DistributorWarehouseByDistributorIdSpecification(_distributorId);

            //Assert
            Assert.Equal(_distributorId, specification.DistributorId);
        }

        [Fact]
        public void Ctor_WhenGivenDistributorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("distributorId", () => new DistributorWarehouseByDistributorIdSpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
