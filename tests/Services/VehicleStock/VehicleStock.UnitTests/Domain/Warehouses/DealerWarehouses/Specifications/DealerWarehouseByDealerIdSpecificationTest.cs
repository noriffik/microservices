﻿using AutoFixture;
using NexCore.DealerNetwork;
using NexCore.Testing.Extensions;
using NexCore.VehicleStock.Domain.Warehouses.DealerWarehouses;
using NexCore.VehicleStock.Domain.Warehouses.DealerWarehouses.Specifications;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Warehouses.DealerWarehouses.Specifications
{
    public class DealerWarehouseByDealerIdSpecificationTest
    {
        private readonly DealerId _dealerId;

        //Entities
        private readonly IReadOnlyList<DealerWarehouse> _expected;
        private readonly IReadOnlyList<DealerWarehouse> _unexpected;
        private readonly IReadOnlyList<DealerWarehouse> _all;

        //Specification
        private readonly DealerWarehouseByDealerIdSpecification _specification;

        public DealerWarehouseByDealerIdSpecificationTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            _dealerId = fixture.Create<DealerId>();

            _expected = fixture.ConstructMany<DealerWarehouse>(3, new { dealerId = _dealerId }).ToList();
            _unexpected = fixture.CreateMany<DealerWarehouse>(2).ToList();
            _all = _unexpected.Concat(_expected).ToList();

            _specification = new DealerWarehouseByDealerIdSpecification(_dealerId);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var specification = new DealerWarehouseByDealerIdSpecification(_dealerId);

            //Assert
            Assert.Equal(_dealerId, specification.DealerId);
        }

        [Fact]
        public void Ctor_WhenGivenDealerId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealerId", () => new DealerWarehouseByDealerIdSpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
