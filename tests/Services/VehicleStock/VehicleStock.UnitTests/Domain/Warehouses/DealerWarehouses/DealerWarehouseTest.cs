﻿using AutoFixture;
using NexCore.DealerNetwork;
using NexCore.VehicleStock.Domain.Warehouses.DealerWarehouses;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Warehouses.DealerWarehouses
{
    public class DealerWarehouseTest
    {
        private readonly int _id;
        private readonly DealerId _dealerId;
        private readonly string _name;
        private readonly int _companyId;

        public DealerWarehouseTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            _id = fixture.Create<int>();
            _dealerId = fixture.Create<DealerId>();
            _name = fixture.Create<string>();
            _companyId = fixture.Create<int>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var warehouse = new DealerWarehouse(_dealerId, _name, _companyId);

            //Assert
            Assert.Equal(_dealerId, warehouse.DealerId);
            Assert.Equal(_dealerId.DistributorId, warehouse.DistributorId);
            Assert.Equal(_dealerId.DealerCode, warehouse.DealerCode);
            Assert.Equal(_name, warehouse.Name);
            Assert.Equal(_companyId, warehouse.CompanyId);
        }

        [Fact]
        public void Ctor_WithId()
        {
            //Act
            var warehouse = new DealerWarehouse(_id, _dealerId, _name, _companyId);

            //Assert
            Assert.Equal(_id, warehouse.Id);
            Assert.Equal(_dealerId, warehouse.DealerId);
            Assert.Equal(_dealerId.DistributorId, warehouse.DistributorId);
            Assert.Equal(_dealerId.DealerCode, warehouse.DealerCode);
            Assert.Equal(_name, warehouse.Name);
            Assert.Equal(_companyId, warehouse.CompanyId);
        }

        [Fact]
        public void Ctor_GivenDealerId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealerId", () => new DealerWarehouse(null, _name, _companyId));
        }

        [Fact]
        public void Ctor_GivenName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("name", () => new DealerWarehouse(_dealerId, null, _companyId));
        }
    }
}
