﻿using AutoFixture;
using NexCore.Testing.Extensions;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Vehicles.Specifications;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Vehicles.Specifications
{
    public class VehicleByVinSpecificationTest
    {
        private readonly string _vin;

        //Entities
        private readonly IReadOnlyList<Vehicle> _expected;
        private readonly IReadOnlyList<Vehicle> _unexpected;
        private readonly IReadOnlyList<Vehicle> _all;

        //Specification
        private readonly VehicleByVinSpecification _specification;

        public VehicleByVinSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());

            _vin = fixture.Create<string>();

            var specification = fixture.Construct<VehicleSpecification>(new { vin = _vin });
            _expected = new[] { fixture.Construct<Vehicle>(new { specification }) };
            _unexpected = fixture.CreateMany<Vehicle>(5).ToList();
            _all = _unexpected.Concat(_expected).ToList();

            _specification = new VehicleByVinSpecification(_vin);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var specification = new VehicleByVinSpecification(_vin);

            //Assert
            Assert.Equal(_vin.ToUpperInvariant(), specification.Vin);
        }

        [Fact]
        public void Ctor_WhenGivenVin_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("vin", () => new VehicleByVinSpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
