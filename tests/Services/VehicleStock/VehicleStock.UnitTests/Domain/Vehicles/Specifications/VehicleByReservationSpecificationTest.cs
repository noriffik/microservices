﻿using AutoFixture;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Vehicles.Specifications;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Vehicles.Specifications
{
    public class VehicleByReservationSpecificationTest
    {
        private readonly int _reservationId;

        //Entities
        private readonly IReadOnlyList<Vehicle> _expected;
        private readonly IReadOnlyList<Vehicle> _unexpected;
        private readonly IReadOnlyList<Vehicle> _all;

        //Specification
        private readonly VehicleByReservationSpecification _specification;

        public VehicleByReservationSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());

            var state = fixture.Create<ReservationState>();
            _reservationId = state.ReservationId.Value;
            _expected = new[] { fixture.Create<Vehicle>() };
            _expected.Single().ChangeReservationState(state);

            _unexpected = fixture.CreateMany<Vehicle>(5).ToList();
            _unexpected.Take(3).ToList().ForEach(v => v.ChangeReservationState(fixture.Create<ReservationState>()));

            _all = _unexpected.Concat(_expected).ToList();

            _specification = new VehicleByReservationSpecification(_reservationId);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var specification = new VehicleByReservationSpecification(_reservationId);

            //Assert
            Assert.Equal(_reservationId, specification.ReservationId);
        }

        [Fact]
        public void Ctor_WhenGivenVin_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("vin", () => new VehicleByVinSpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
