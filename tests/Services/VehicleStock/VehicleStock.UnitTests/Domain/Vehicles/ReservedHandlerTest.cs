﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Reservations.Specifications;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Vehicles.Specifications;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Vehicles
{
    public class ReservedHandlerTest
    {
        //Dependency
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly Reservation _reservation;
        private readonly Vehicle _vehicle;

        //Event and handler
        private readonly ReservedEvent _event;
        private readonly ReservedHandler _handler;

        public ReservedHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());

            //Dependencies
            _repository = new Mock<IEntityRepository>();
            var work = new Mock<IUnitOfWork>();
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entities
            _reservation = fixture.Create<Reservation>();
            _vehicle = fixture.Create<Vehicle>();

            //Event and Handler
            _event = new ReservedEvent(_reservation.Vehicle.Vin);
            _handler = new ReservedHandler(work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ReservedHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            var expectedState = new ReservationState(_reservation.Id, _reservation.OnDate);
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require(
                        It.Is<ActualByVinSpecification>(s => s.Vin == _reservation.Vehicle.Vin), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_reservation);
                _repository.Setup(r => r.Require<Vehicle, CommercialNumber>(
                        It.Is<VehicleByVinSpecification>(s => s.Vin == _reservation.Vehicle.Vin), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicle);

                //Act
                await _handler.Handle(_event, CancellationToken.None);

                //Assert
                Assert.Equal(expectedState, _vehicle.ReservationState);
            }
        }

        [Fact]
        public Task Handle_GivenNotification_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("notification", () =>
                _handler.Handle(null, CancellationToken.None));
        }
    }
}
