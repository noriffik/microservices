﻿using AutoFixture;
using NexCore.VehicleStock.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Vehicles
{
    public class WarehouseDeliveryTest
    {
        private readonly DateTime _assembledOn;

        public WarehouseDeliveryTest()
        {
            var fixture = new Fixture();

            _assembledOn = fixture.Create<DateTime>();
        }

        [Fact]
        public void Ctor()
        {
            //Arrange
            var deliveredOn = _assembledOn.AddDays(10);

            //Act
            var warehouseDelivery = new WarehouseDelivery(_assembledOn, deliveredOn);

            //Assert
            Assert.Equal(_assembledOn, warehouseDelivery.AssembledOn);
            Assert.Equal(deliveredOn, warehouseDelivery.DeliveredOn);
        }

        [Fact]
        public void Ctor_WhenDelivery_BeforeAssembly()
        {
            //Arrange
            var deliveredOn = _assembledOn.AddDays(-10);

            //Act
            var e = Assert.Throws<CannotDeliverBeforeAssemblyException>(() =>
                new WarehouseDelivery(_assembledOn, deliveredOn));

            //Assert
            Assert.Equal(_assembledOn, e.AssembledOn);
            Assert.Equal(deliveredOn, e.DeliveredOn);
        }
    }
}
