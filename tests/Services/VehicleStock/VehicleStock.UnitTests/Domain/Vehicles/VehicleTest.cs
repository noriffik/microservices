﻿using AutoFixture;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Vehicles
{
    public class VehicleTest
    {
        private readonly Vehicle _vehicle;
        private readonly CommercialNumber _commercialNumber;
        private readonly VehicleSpecification _vehicleSpecification;
        private readonly WarehouseDelivery _warehouseDelivery;
        private readonly int _warehouseId;
        private readonly ReservationState _reservationState;

        public VehicleTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());

            _vehicle = fixture.Create<Vehicle>();
            _commercialNumber = fixture.Create<CommercialNumber>();
            _vehicleSpecification = fixture.Create<VehicleSpecification>();
            _warehouseDelivery = fixture.Create<WarehouseDelivery>();
            _warehouseId = fixture.Create<int>();
            _reservationState = fixture.Create<ReservationState>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var vehicle = new Vehicle(_commercialNumber, _vehicleSpecification, _warehouseDelivery, _warehouseId);

            //Assert
            Assert.Equal(_commercialNumber, vehicle.Id);
            Assert.Equal(_vehicleSpecification, vehicle.Specification);
            Assert.Equal(_warehouseDelivery, vehicle.WarehouseDelivery);
            Assert.Equal(_warehouseId, vehicle.WarehouseId);
            Assert.False(vehicle.IsTemporaryUnavailable);
            Assert.Equal(ReservationState.None, vehicle.ReservationState);
        }

        [Fact]
        public void Ctor_WhenGivenWarehouseDelivery_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("specification", () =>
                new Vehicle(_commercialNumber, null, _warehouseDelivery, _warehouseId));
        }

        [Fact]
        public void Ctor_WhenGivenVehicleSpecification_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("warehouseDelivery", () =>
                new Vehicle(_commercialNumber, _vehicleSpecification, null, _warehouseId));
        }

        [Fact]
        public void ChangeUnavailability()
        {
            //Act
            _vehicle.ChangeUnavailability(true);

            //Assert
            Assert.True(_vehicle.IsTemporaryUnavailable);
        }

        [Fact]
        public void ChangeReservationState()
        {
            //Act
            _vehicle.ChangeReservationState(_reservationState);

            //Assert
            Assert.Equal(_reservationState, _vehicle.ReservationState);
        }

        [Fact]
        public void ChangeReservationState_GivenState_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("reservationState", () => _vehicle.ChangeReservationState(null));
        }
    }
}
