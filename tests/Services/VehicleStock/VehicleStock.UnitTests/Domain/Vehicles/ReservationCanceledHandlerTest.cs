﻿using AutoFixture;
using Moq;
using NexCore.Domain;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Vehicles.Specifications;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Vehicles
{
    public class ReservationCanceledHandlerTest
    {
        //Dependency
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly Vehicle _vehicle;

        //Event and handler
        private readonly ReservationCanceledEvent _event;
        private readonly ReservationCanceledHandler _handler;

        public ReservationCanceledHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());

            //Dependencies
            _repository = new Mock<IEntityRepository>();
            var work = new Mock<IUnitOfWork>();
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entities
            var reservationState = fixture.Create<ReservationState>();
            _vehicle = fixture.Create<Vehicle>();
            _vehicle.ChangeReservationState(reservationState);

            //Event and Handler
            _event = new ReservationCanceledEvent(reservationState.ReservationId.Value);
            _handler = new ReservationCanceledHandler(work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ReservationCanceledHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            _repository.Setup(r => r.SingleOrDefault<Vehicle, CommercialNumber>(
                    It.IsAny<VehicleByReservationSpecification>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_vehicle)
                .Verifiable();

            //Act
            await _handler.Handle(_event, CancellationToken.None);

            //Assert
            Assert.Equal(ReservationState.None, _vehicle.ReservationState);
            _repository.Verify();
        }

        [Fact]
        public Task Handle_GivenNotification_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("notification", () =>
                _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_WhenVehicle_IsNotFound_DoesNothing()
        {
            //Arrange
            _repository.Setup(r => r.SingleOrDefault<Vehicle, CommercialNumber>(
                    It.Is<VehicleByReservationSpecification>(s => s.ReservationId == _event.ReservationId), It.IsAny<CancellationToken>()))
                .ReturnsAsync(null as Vehicle)
                .Verifiable();

            //Act
            await _handler.Handle(_event, CancellationToken.None);

            //Assert
            _repository.Verify();
        }
    }
}
