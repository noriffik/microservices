﻿using AutoFixture;
using NexCore.VehicleStock.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Vehicles
{
    public class ReservationStateTest
    {
        private readonly int _reservationId;
        private readonly DateTime _date;

        public ReservationStateTest()
        {
            var fixture = new Fixture();

            _reservationId = fixture.Create<int>();
            _date = fixture.Create<DateTime>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var reservationState = new ReservationState(_reservationId, _date);

            //Assert
            Assert.Equal(_reservationId, reservationState.ReservationId);
            Assert.Equal(_date, reservationState.OnDate);
            Assert.Equal(ReservationStateStatus.Reserved, reservationState.Status);
        }

        [Fact]
        public void None()
        {
            //Act
            var reservationState = ReservationState.None;

            //Assert
            Assert.Null(reservationState.ReservationId);
            Assert.Null(reservationState.OnDate);
            Assert.Equal(ReservationStateStatus.NotReserved, reservationState.Status);
        }
    }
}
