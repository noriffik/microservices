﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Vehicles
{
    public class VehicleSpecificationTest
    {
        private readonly ModelKey _modelKey;
        private readonly PrNumberSet _options;
        private readonly string _engine;
        private readonly string _vin;
        private readonly ColorId _colorId;

        public VehicleSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());

            _modelKey = fixture.Create<ModelKey>();
            _options = fixture.Create<PrNumberSet>();
            _engine = fixture.Create<string>();
            _vin = fixture.Create<string>();
            _colorId = ColorId.Parse("1111");
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var vehicle = new VehicleSpecification(_modelKey, _options, _engine, _vin, _colorId);

            //Assert
            Assert.Equal(_modelKey, vehicle.ModelKey);
            Assert.Equal(_options, vehicle.Options);
            Assert.Equal(_engine.ToUpperInvariant(), vehicle.Engine);
            Assert.Equal(_vin.ToUpperInvariant(), vehicle.Vin);
            Assert.Equal(_colorId, vehicle.ColorId);
        }

        [Fact]
        public void CopyCtor()
        {
            //Arrange
            var source = new VehicleSpecification(_modelKey, _options, _engine, _vin, _colorId);

            //Act
            var vehicle = new VehicleSpecification(source);

            //Assert
            Assert.Equal(_modelKey, vehicle.ModelKey);
            Assert.Equal(_options, vehicle.Options);
            Assert.Equal(_engine.ToUpperInvariant(), vehicle.Engine);
            Assert.Equal(_vin.ToUpperInvariant(), vehicle.Vin);
            Assert.Equal(_colorId, vehicle.ColorId);
        }
    }
}
