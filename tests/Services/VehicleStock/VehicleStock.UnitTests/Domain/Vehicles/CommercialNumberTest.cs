﻿using System;
using NexCore.VehicleStock.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Domain.Vehicles
{
    public class CommercialNumberTest
    {
        [Theory]
        [InlineData("NF2")]
        [InlineData("12Nj")]
        [InlineData("ffN$")]
        [InlineData("s")]
        [InlineData("qwerty")]
        [InlineData("Fqf")]
        public void Parse(string value)
        {
            //Act
            var commercialNumber = CommercialNumber.Parse(value);

            //Assert
            Assert.NotNull(commercialNumber);
            Assert.Equal(value.ToUpperInvariant(), commercialNumber.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                CommercialNumber.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("   ")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>
            {
                CommercialNumber.Parse(value);
            });
        }

        [Theory]
        [InlineData("NF")]
        [InlineData("Nj2")]
        [InlineData("3%123")]
        [InlineData("3V2222222")]
        public void TryParse(string value)
        {
            //Act
            var result = CommercialNumber.TryParse(value, out var commercialNumber);

            //Assert
            Assert.True(result);
            Assert.NotNull(commercialNumber);
            Assert.Equal(value.ToUpperInvariant(), commercialNumber.Value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("      ")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = CommercialNumber.TryParse(value, out var commercialNumber);

            //Assert
            Assert.False(result);
            Assert.Null(commercialNumber);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var commercialNumber = CommercialNumber.Parse("NF123");

            //Assert
            Assert.Equal(commercialNumber.Value, commercialNumber.ToString());
        }
    }
}
