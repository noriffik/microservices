﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleStock.Application.Messages;
using NexCore.VehicleStock.Domain.Warehouses.DistributorWarehouses;
using NexCore.VehicleStock.Domain.Warehouses.DistributorWarehouses.Specifications;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Messages
{
    public class DistributorRegisteredEventHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly DistributorWarehouse _warehouse;

        //Event and handler
        private readonly DistributorRegisteredEvent _event;
        private readonly DistributorRegisteredEventHandler _handler;

        public DistributorRegisteredEventHandlerTest()
        {
            var fixture = new Fixture().Customize(new DistributorIdCustomization());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            var mapper = new Mock<IMapper>();

            //Entity
            _warehouse = fixture.Create<DistributorWarehouse>();

            //Event and handler
            _event = fixture.Create<DistributorRegisteredEvent>();
            _handler = new DistributorRegisteredEventHandler(_work.Object, mapper.Object);

            //Setup dependencies
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            mapper.Setup(m => m.Map<DistributorWarehouse>(_event)).Returns(_warehouse);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DistributorRegisteredEventHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenEvent_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("event", () => _handler.Handle(null));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Has(
                        It.Is<DistributorWarehouseByDistributorIdSpecification>(s => s.DistributorId == _warehouse.DistributorId)))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Add(_warehouse))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_event);
            }
        }

        [Fact]
        public async Task Handle_WhenDistributor_AlreadyExist_DoesNothing()
        {
            //Arrange
            _repository.Setup(r => r.Has(
                    It.Is<DistributorWarehouseByDistributorIdSpecification>(s => s.DistributorId == _warehouse.DistributorId)))
                .ReturnsAsync(true);

            //Act
            await _handler.Handle(_event);

            //Assert
            _work.Verify(w => w.Commit(It.IsAny<CancellationToken>()), Times.Never);
        }
    }
}
