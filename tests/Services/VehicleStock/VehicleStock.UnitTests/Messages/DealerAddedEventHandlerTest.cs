﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleStock.Application.Messages;
using NexCore.VehicleStock.Domain.Warehouses.DealerWarehouses;
using NexCore.VehicleStock.Domain.Warehouses.DealerWarehouses.Specifications;
using NexCore.VehicleStock.Domain.Warehouses.DistributorWarehouses.Specifications;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.UnitTests.Messages
{
    public class DealerAddedEventHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly DealerWarehouse _warehouse;

        //Event and handler
        private readonly DealerAddedEvent _event;
        private readonly DealerAddedEventHandler _handler;

        public DealerAddedEventHandlerTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            var mapper = new Mock<IMapper>();

            //Entity
            _warehouse = fixture.Create<DealerWarehouse>();

            //Event and handler
            _event = fixture.Create<DealerAddedEvent>();
            _handler = new DealerAddedEventHandler(_work.Object, mapper.Object);

            //Setup dependencies
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            mapper.Setup(m => m.Map<DealerWarehouse>(_event)).Returns(_warehouse);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DealerAddedEventHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenEvent_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("event", () => _handler.Handle(null));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Has(
                        It.Is<DistributorWarehouseByDistributorIdSpecification>(s => s.DistributorId == _warehouse.DistributorId)))
                    .InSequence()
                    .ReturnsAsync(true);
                _repository.Setup(r => r.Has(
                        It.Is<DealerWarehouseByDealerIdSpecification>(s => s.DealerId == _warehouse.DealerId)))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Add(_warehouse))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_event);
            }
        }

        [Fact]
        public async Task Handle_WhenDealer_AlreadyExist_DoesNothing()
        {
            //Arrange
            _repository.Setup(r => r.Has(
                    It.Is<DistributorWarehouseByDistributorIdSpecification>(s => s.DistributorId == _warehouse.DistributorId)))
                .ReturnsAsync(true);
            _repository.Setup(r => r.Has(
                    It.Is<DealerWarehouseByDealerIdSpecification>(s => s.DealerId == _warehouse.DealerId)))
                .ReturnsAsync(true);

            //Act
            await _handler.Handle(_event);

            //Assert
            _work.Verify(w => w.Commit(It.IsAny<CancellationToken>()), Times.Never);
        }

        [Fact]
        public async Task Handle_WhenDistributor_DoesNotExist_DoesNothing()
        {
            //Arrange
            _repository.Setup(r => r.Has(
                    It.Is<DistributorWarehouseByDistributorIdSpecification>(s => s.DistributorId == _warehouse.DistributorId)))
                .ReturnsAsync(false);
            _repository.Setup(r => r.Has(
                    It.Is<DealerWarehouseByDealerIdSpecification>(s => s.DealerId == _warehouse.DealerId)))
                .ReturnsAsync(false);

            //Act
            await _handler.Handle(_event);

            //Assert
            _work.Verify(w => w.Commit(It.IsAny<CancellationToken>()), Times.Never);
        }
    }
}
