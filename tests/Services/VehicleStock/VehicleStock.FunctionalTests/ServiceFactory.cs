﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NexCore.EventBus.Abstract;
using NexCore.Infrastructure.Utility;
using NexCore.Testing.Extensions.Moq;
using NexCore.VehicleStock.Infrastructure;

namespace NexCore.VehicleStock.FunctionalTests
{
    public class ServiceFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            base.ConfigureWebHost(builder);

            builder.UseContentRoot(".");

            builder.ConfigureServices(services =>
            {
                var serviceProvider = new ServiceCollection()
                    .AddEntityFrameworkInMemoryDatabase()
                    .BuildServiceProvider();

                services.AddScoped(c =>
                {
                    return new VehicleStockContext(new DbContextOptionsBuilder<VehicleStockContext>()
                        .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                        .UseInMemoryDatabase(nameof(VehicleStockContext))
                        .UseInternalServiceProvider(serviceProvider)
                        .Options)
                    {
                        ResilientTransaction = ResilientTransactionStub()
                    };
                });

                services.AddScoped(s => new Mock<IEventBus>().Object);
            });
        }

        private static IResilientTransaction ResilientTransactionStub()
        {
            var transaction = new Mock<IResilientTransaction>();

            transaction.SetupExecuteAsync();

            return transaction.Object;
        }

        protected override IWebHostBuilder CreateWebHostBuilder()
        {
            return WebHost.CreateDefaultBuilder()
                .UseStartup<TStartup>();
        }

        public IServiceScope CreateScope()
        {
            return Server.Host.Services.CreateScope();
        }
    }
}
