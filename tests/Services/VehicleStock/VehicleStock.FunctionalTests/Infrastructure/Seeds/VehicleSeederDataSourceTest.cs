﻿using AutoFixture;
using AutoMapper;
using NexCore.Infrastructure.Seeds;
using NexCore.Testing;
using NexCore.VehicleStock.Infrastructure.Seeds;
using NexCore.VehicleStock.UnitTests.Fixtures;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using Xunit;

namespace NexCore.VehicleStock.FunctionalTests.Infrastructure.Seeds
{
    public class VehicleSeederDataSourceTest
    {
        private readonly IMapper _mapper;

        public VehicleSeederDataSourceTest()
        {
            _mapper = AutoMapperFactory.Create();
        }

        [Fact]
        public void Get()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new VehicleBuilder());
            fixture.Customize(new DealerIdCustomization());

            var expected = fixture.Create<VehicleRecord>();

            var text = "CommercialNumber;Vin;ModelKey;Engine;AssembledOn;DeliveredOn;DistributorId;DealerCode;Options;ColorId\n" +
                       $"{expected.Id};{expected.Specification.Vin};{expected.Specification.ModelKey};" +
                       $"{expected.Specification.Engine};{expected.WarehouseDelivery.AssembledOn};" +
                       $"{expected.WarehouseDelivery.DeliveredOn};" +
                       $"{expected.DistributorId.Value};{expected.DealerCode.Value};" +
                       $"{expected.Specification.Options};{expected.Specification.ColorId}";
            var dataSource = new VehicleSeederDataSource(_mapper, text);

            //Act
            var actual = dataSource.Get();

            //Assert
            Assert.Contains(expected, actual, PropertyComparer<VehicleRecord>.Instance);
        }

        [Fact]
        public void Get_WhenUsingDefaultText()
        {
            //Arrange
            var dataSource = new VehicleSeederDataSource(
                _mapper, new ResourceReaderProvider("Vehicles", Properties.Resources.ResourceManager));

            //Assert
            Assert.NotEmpty(dataSource.Get());
        }
    }
}
