﻿using Microsoft.Extensions.Configuration;

namespace NexCore.VehicleStock.FunctionalTests
{
    static class TestConfiguration
    {
        private static IConfigurationRoot _root;

        public static IConfigurationRoot Get => _root ?? (_root = new ConfigurationBuilder()
                                                    .AddJsonFile("appsettings.json", true)
                                                    .AddUserSecrets("61BBF379-5210-43D7-94AA-B58783EE5950")
                                                    .Build());
    }
}
