﻿namespace NexCore.VehicleStock.FunctionalTests.Fixtures
{
    class ConnectionStringProvider
    {
        public static string Get(string name) => TestConfiguration.Get["ConnectionString"]
            .Replace("%DATABASE%", $"NexCore.Services.VehicleStock_{name}");
    }
}