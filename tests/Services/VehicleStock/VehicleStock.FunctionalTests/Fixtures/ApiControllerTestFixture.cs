﻿using AutoFixture;
using Microsoft.Extensions.DependencyInjection;
using NexCore.VehicleStock.FunctionalTests.Api;
using NexCore.VehicleStock.Infrastructure;
using NexCore.VehicleStock.UnitTests.Fixtures.Domain.SpecimenBuilders;
using System;
using System.Net.Http;
using Xunit;

namespace NexCore.VehicleStock.FunctionalTests.Fixtures
{
    public abstract class ApiControllerTestFixture : IClassFixture<ServiceFactory<TestStartup>>, IDisposable
    {
        protected readonly HttpClient Client;
        protected readonly VehicleStockContext DbContext;
        protected readonly IFixture Fixture;

        private readonly IServiceScope _scope;

        protected ApiControllerTestFixture(ServiceFactory<TestStartup> factory)
        {
            Client = factory.CreateClient();
            _scope = factory.CreateScope();
            DbContext = _scope.ServiceProvider.GetRequiredService<VehicleStockContext>();

            Fixture = new Fixture();
            Fixture.Customizations.Add(new VehicleBuilder());
        }
        
        public void Dispose()
        {
            DbContext.Database.EnsureDeleted();
            
            _scope.Dispose();
        }
    }
}
