﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NexCore.VehicleStock.Infrastructure;
using System.Reflection;

namespace NexCore.VehicleStock.FunctionalTests.Fixtures
{
    public class SqlServerDbContextFactory
    {
        public VehicleStockContext Create(string databaseName)
        {
            var connectionString = ConnectionStringProvider.Get(databaseName);
            
            var migrationAssembly = typeof(VehicleStockContext).GetTypeInfo().Assembly.GetName().Name;

            var services = new ServiceCollection()
                .AddEntityFrameworkSqlServer();
            
            var options = new DbContextOptionsBuilder<VehicleStockContext>()
                .UseInternalServiceProvider(services.BuildServiceProvider())
                .UseSqlServer(
                    connectionString,
                    sqlOptions => {sqlOptions
                        .MigrationsAssembly(migrationAssembly)
                        .EnableRetryOnFailure(10);
                    })
                .Options;

            var mediator = new Mock<IMediator>();

            return new VehicleStockContext(options, mediator.Object);
        }
    }
}
