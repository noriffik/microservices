﻿using AutoFixture;
using NexCore.Testing.Extensions;
using NexCore.VehicleStock.Application.Reservations.Commands.Add;
using NexCore.VehicleStock.Domain.Reservations;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.FunctionalTests.Fixtures;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.FunctionalTests.Api.Controllers
{
    public class ReservationsControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/reservations";

        public ReservationsControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Add_ByCommercialNumber_WhenVehicle_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Build<AddByCommercialNumberCommand>()
                .With(c => c.CommercialNumber, Fixture.Create<CommercialNumber>().Value)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add/by-commercial-number", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Add_ByCommercialNumber()
        {
            //Arrange
            var vehicle = await SeedVehicle();
            var command = Fixture.Build<AddByCommercialNumberCommand>()
                .With(c => c.CommercialNumber, vehicle.Id.Value)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add/by-commercial-number", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        private async Task<Vehicle> SeedVehicle()
        {
            var seeder = new VehicleSeeder(DbContext);
            var vehicle = await seeder.Add();

            await seeder.SeedAsync();

            return vehicle;
        }

        [Fact]
        public async Task Add_ByVin_WhenVehicle_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<AddByVinCommand>();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add/by-vin", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Add_ByVin()
        {
            //Arrange
            var vehicle = await SeedVehicle();
            var command = Fixture.Build<AddByVinCommand>()
                .With(c => c.Vin, vehicle.Specification.Vin)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add/by-vin", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var reservation = await SeedReservation();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{reservation.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        private async Task<Reservation> SeedReservation()
        {
            var seeder = new ReservationSeeder(DbContext);
            var reservation = await seeder.Add();

            await seeder.SeedAsync();

            return reservation;
        }

        [Fact]
        public async Task Get_WhenReservation_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task Get_ByVin()
        {
            //Arrange
            var reservation = await SeedReservation();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many/by-vin/{reservation.Vehicle.Vin}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Cancel()
        {
            //Arrange
            var reservation = await SeedReservation();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/{reservation.Id}/cancel", null);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Cancel_WhenReservation_IsNotFound_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/{Fixture.Create<int>()}/cancel", null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Complete()
        {
            //Arrange
            var reservation = await SeedReservation();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/{reservation.Id}/complete", null);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Complete_WhenReservation_IsNotFound_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/{Fixture.Create<int>()}/complete", null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
