﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleStock.Application.Vehicles.Commands.Add;
using NexCore.VehicleStock.Application.Vehicles.Commands.ChangeUnavailability;
using NexCore.VehicleStock.Domain.Vehicles;
using NexCore.VehicleStock.FunctionalTests.Fixtures;
using NexCore.VehicleStock.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleStock.FunctionalTests.Api.Controllers
{
    public class VehiclesControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/vehicles";

        public VehiclesControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var vehicle = await SeedVehicle();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{vehicle.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<string>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetMany()
        {
            //Arrange
            await SeedVehicle();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetMany_WhenEmpty_ReturnsSuccess()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetMany_ByWarehouseId()
        {
            var seeder = new VehicleSeeder(DbContext);
            var warehouseId = seeder.Create<int>();
            await seeder.AddRange(3, new { warehouseId });

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many/by-warehouse/{warehouseId}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetMany_ByWarehouseId_WhenNotFound_ReturnsSuccess()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many/by-warehouse/{Fixture.Create<int>()}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var seeder = new VehicleSeeder(DbContext);
            var vehicle = seeder.Create();

            await seeder.AddDependencies(vehicle);

            await seeder.SeedAsync();

            var command = seeder.Build<AddCommand>()
                .With(c => c.ModelKey, vehicle.Specification.ModelKey.Value)
                .With(c => c.Options, vehicle.Specification.Options.AsString)
                .With(c => c.WarehouseId, vehicle.WarehouseId)
                .With(c => c.AssembledOn, vehicle.WarehouseDelivery.AssembledOn)
                .With(c => c.DeliveredOn, vehicle.WarehouseDelivery.DeliveredOn)
                .With(c => c.ModelKey, "NJ33KC")
                .With(c => c.ColorId, "1122")
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add_WhenVehicle_AlreadyExist_ReturnsBadRequest()
        {
            //Arrange
            var vehicle = await SeedVehicle();

            var command = Fixture.Build<AddCommand>()
                .With(c => c.Vin, vehicle.Specification.Vin)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        private async Task<Vehicle> SeedVehicle()
        {
            var seeder = new VehicleSeeder(DbContext);
            var vehicle = await seeder.Add();

            await seeder.SeedAsync();

            return vehicle;
        }

        [Fact]
        public async Task ChangeUnavailability()
        {
            //Arrange
            var vehicle = await SeedVehicle();

            var command = Fixture.Build<ChangeUnavailabilityCommand>()
                .With(c => c.CommercialNumber, vehicle.Id.Value)
                .With(c => c.IsTemporaryUnavailable, false)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/unavailability", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ChangeUnavailability_WhenVehicle_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<ChangeUnavailabilityCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/unavailability", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task RemoveById()
        {
            //Arrange
            var vehicle = await SeedVehicle();

            //Act
            var response = await Client.DeleteAsync($"{BaseUrl}/remove/{vehicle.Id.Value}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task RemoveById_WhenVehicle_IsNotFound_ReturnsBadRequest()
        {
            //Act
            var response = await Client.DeleteAsync($"{BaseUrl}/remove/{Fixture.Create<CommercialNumber>().Value}");

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task RemoveByVin()
        {
            //Arrange
            var vehicle = await SeedVehicle();

            //Act
            var response = await Client.DeleteAsync($"{BaseUrl}/remove/by-vin/{vehicle.Specification.Vin}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task RemoveByVin_WhenVehicle_IsNotFound_ReturnsBadRequest()
        {
            //Act
            var response = await Client.DeleteAsync($"{BaseUrl}/remove/by-vin/{Fixture.Create<string>()}");

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Matching()
        {
            var seeder = new VehicleSeeder(DbContext);

            var modelKey = seeder.Create<ModelKey>();
            var options = seeder.Create<PrNumberSet>();
            var colorId = seeder.Create<ColorId>();
            var vehicleSpecification = new VehicleSpecification(modelKey, options, "", "", colorId);

            await seeder.AddRange(new { vehicleSpecification });

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/matching/{modelKey.Value}/{colorId.Value}/{options.AsString}/mask/111110");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Matching_WhenNotFound_ReturnsSuccess()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/matching/XXXXXX/XXXX/{Fixture.Create<PrNumberSet>().AsString}/mask/111111");

            //Assert
            response.EnsureSuccessStatusCode();
        }
    }
}
