﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NexCore.Testing.Functional.Auth;
using NexCore.VehicleStock.Api;

namespace NexCore.VehicleStock.FunctionalTests.Api
{
    public class TestStartup : Startup
    {
        public TestStartup(IConfiguration configuration) : base(configuration)
        {
        }

        protected override void ConfigureAuthentication(IServiceCollection services)
        {
            services.AddAuthentication("Test")
                .AddTestAuthentication("Test");
        }
    }
}
