﻿using NexCore.Domain;
using System;

namespace NexCore.VehicleConfigurator.UnitTests.Extensions.Domain
{
    public static class PeriodExtensions
    {
        public static Period Range(this DateTime date, int days)
        {
            return new Period(date.AddDays(days * -1), date.AddDays(days));
        }

        public static Period Past(this DateTime date, int days)
        {
            return new Period(date.AddDays(days * -1 - 1), date.AddDays(days * -1));
        }
    }
}
