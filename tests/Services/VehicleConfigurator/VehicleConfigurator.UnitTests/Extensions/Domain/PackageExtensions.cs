﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("NexCore.VehicleConfigurator.FunctionalTests")]
namespace NexCore.VehicleConfigurator.UnitTests.Extensions.Domain
{
    internal static class PackageExtensions
    {
        public static PrNumberSet ApplicableWith(this Package package)
        {
            return new PrNumberSet(package.Applicability.Select(s => s.PrNumbers.Values.First()));
        }
    }
}
