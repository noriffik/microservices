﻿using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures
{
    public class TestComponent : Component<TestId>
    {
        public TestComponent(TestId id) : base(id)
        {
        }
    }
}
