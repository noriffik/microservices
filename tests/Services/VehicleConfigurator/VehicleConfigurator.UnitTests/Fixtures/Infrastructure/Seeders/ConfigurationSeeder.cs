﻿using Microsoft.EntityFrameworkCore;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class ConfigurationSeeder : EntitySeeder<Configuration>
    {
        public ConfigurationSeeder(DbContext context) : base(context)
        {
            Fixture.Customizations.Add(new ModelComponentBuilder());
        }

        public override async Task<Configuration> Add() => await AddDependencies(await base.Add());

        public override async Task<Configuration> Add(Configuration vehicle) => await AddDependencies(await base.Add(vehicle));

        public override async Task<Configuration> Add(object parameters) => await AddDependencies(await base.Add(parameters));

        public override async Task<Configuration> Add(int id, object parameters) => await AddDependencies(await base.Add(id, parameters));

        public virtual async Task<Configuration> AddDependencies(Configuration configuration)
        {
            if (!Has<Model, ModelId>(configuration.ModelKey.ModelId))
                await Add<Model, ModelId>(configuration.ModelKey.ModelId);

            if (!Has<Body, BodyId>(configuration.ModelKey.BodyId))
                await Add<Body, BodyId>(configuration.ModelKey.BodyId);

            if (!Has<Equipment, EquipmentId>(configuration.ModelKey.EquipmentId))
                await Add<Equipment, EquipmentId>(configuration.ModelKey.EquipmentId);

            if (!Has<Engine, EngineId>(configuration.ModelKey.EngineId))
                await Add<Engine, EngineId>(configuration.ModelKey.EngineId);

            if (!Has<Gearbox, GearboxId>(configuration.ModelKey.GearboxId))
                await Add<Gearbox, GearboxId>(configuration.ModelKey.GearboxId);

            if (!Has<Catalog>(configuration.CatalogId))
                await Add<Catalog>(configuration.CatalogId);

            var vehicleOffer = VehicleOfferFor(configuration);
            if (vehicleOffer == null)
            {
                vehicleOffer = await Add<VehicleOffer>(Create<VehicleOffer>(new
                {
                    catalogId = configuration.CatalogId,
                    modelKey = configuration.ModelKey
                }));
            }
            
            await SetDefaultColor(vehicleOffer, configuration.ColorId);

            vehicleOffer.Publish();

            return configuration;
        }

        private VehicleOffer VehicleOfferFor(Configuration configuration)
        {
            return Get<VehicleOffer>()
                .Where(o => o.CatalogId == configuration.CatalogId)
                .SingleOrDefault(o => o.ModelKey == configuration.ModelKey);
        }

        private async Task SetDefaultColor(VehicleOffer vehicle, ColorId colorId)
        {
            var color = Has<Color, ColorId>(colorId)
                ? Find<Color, ColorId>(colorId)
                : await Add<Color, ColorId>(colorId);

            if (!Has<ColorType, ColorTypeId>(color.TypeId))
                await Add<ColorType, ColorTypeId>(color.TypeId);

            vehicle.AddColorOffer(colorId, Create<decimal>());
            vehicle.SetDefaultColor(colorId);
        }
    }
}
