﻿using Microsoft.EntityFrameworkCore;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class ModelComponentSeeder<TComponent, TId> : EntitySeeder<TComponent, TId>
        where TComponent : ModelComponent<TId>
        where TId : EntityId
    {
        public ModelComponentSeeder(DbContext context) : base(context)
        {
            Fixture.Customizations.Add(new ModelComponentBuilder());
        }

        public override async Task<TComponent> Add() => await AddDependencies(await base.Add());
        
        public override async Task<TComponent> Add(TComponent component) => await AddDependencies(await base.Add(component));

        public override async Task<TComponent> Add(object parameters) => await AddDependencies(await base.Add(parameters));

        public override async Task<TComponent> Add(TId id) => await AddDependencies(await base.Add(id));

        public override async Task<TComponent> Add(TId id, object parameters) => await AddDependencies(await base.Add(id, parameters));
        
        public virtual async Task<TComponent> AddDependencies(TComponent component)
        {
            if (!Has<Model, ModelId>(component.ModelId))
                await Add<Model, ModelId>(component.ModelId);

            return component;
        }
    }
}
