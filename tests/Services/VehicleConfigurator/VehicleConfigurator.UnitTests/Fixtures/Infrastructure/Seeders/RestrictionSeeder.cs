﻿using Microsoft.EntityFrameworkCore;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class RestrictionSeeder : EntitySeeder<Restriction>
    {
        public RestrictionSeeder(DbContext context) : base(context)
        {
            Fixture.Customizations.Add(new ModelComponentBuilder());
        }

        public override async Task<Restriction> Add() => await AddDependencies(await base.Add());

        public override async Task<Restriction> Add(Restriction restriction) => await AddDependencies(await base.Add(restriction));

        public override async Task<Restriction> Add(object parameters) => await AddDependencies(await base.Add(parameters));

        public override async Task<Restriction> Add(int id, object parameters) => await AddDependencies(await base.Add(id, parameters));

        public virtual async Task<Restriction> AddDependencies(Restriction restriction)
        {
            if (!Has<Catalog>(restriction.CatalogId))
                await Add<Catalog>(restriction.CatalogId);

            return restriction;
        }
    }
}
