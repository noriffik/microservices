﻿using Microsoft.EntityFrameworkCore;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class ColorSeeder : EntitySeeder<Color, ColorId>
    {
        public ColorSeeder(DbContext context) : base(context)
        {
            Fixture.Customizations.Add(new ModelComponentBuilder());
        }

        public override async Task<Color> Add() => await AddDependencies(await base.Add());

        public override async Task<Color> Add(Color color) => await AddDependencies(await base.Add(color));

        public override async Task<Color> Add(object parameters) => await  AddDependencies(await base.Add(parameters));

        public override async Task<Color> Add(ColorId id, object parameters) => await AddDependencies(await base.Add(id, parameters));

        public virtual async Task<Color> AddDependencies(Color color)
        {
            if (!Has<ColorType, ColorTypeId>(color.TypeId))
                await Add<ColorType, ColorTypeId>(color.TypeId);

            return color;
        }
    }
}
