﻿using System.Linq;
using AutoFixture;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Threading.Tasks;
using NexCore.Common;
using NexCore.Domain;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class VehicleOfferSeeder : EntitySeeder<VehicleOffer>
    {
        public VehicleOfferSeeder(DbContext context) : base(context)
        {
            Fixture.Customizations.Add(new ModelComponentBuilder());
        }

        public VehicleOfferSeeder(DbContext context, IFixture fixture) : base(context, fixture)
        {
        }

        public static IFixture CreateFixture()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new NumericSequenceGenerator());
            fixture.Customizations.Add(new ModelComponentBuilder());

            return fixture;
        }

        public override async Task<VehicleOffer> Add() => await AddDependencies(await base.Add());

        public override async Task<VehicleOffer> Add(VehicleOffer vehicle) => await AddDependencies(await base.Add(vehicle));

        public override async Task<VehicleOffer> Add(object parameters) => await AddDependencies(await base.Add(parameters));

        public override async Task<VehicleOffer> Add(int id, object parameters) => await AddDependencies(await base.Add(id, parameters));

        public virtual async Task<VehicleOffer> AddDependencies(VehicleOffer vehicleOffer)
        {
            if (!Has<Model, ModelId>(vehicleOffer.ModelId))
                await Add<Model, ModelId>(vehicleOffer.ModelId);

            if (!Has<Body, BodyId>(vehicleOffer.BodyId))
                await Add<Body, BodyId>(vehicleOffer.BodyId);

            if (!Has<Equipment, EquipmentId>(vehicleOffer.EquipmentId))
                await Add<Equipment, EquipmentId>(vehicleOffer.EquipmentId);

            if (!Has<Engine, EngineId>(vehicleOffer.EngineId))
                await Add<Engine, EngineId>(vehicleOffer.EngineId);

            if (!Has<Gearbox, GearboxId>(vehicleOffer.GearboxId))
                await Add<Gearbox, GearboxId>(vehicleOffer.GearboxId);

            await AddCatalog(vehicleOffer);

            return vehicleOffer;
        }

        private async Task AddCatalog(VehicleOffer vehicleOffer)
        {
            var catalog = Get<Catalog>()
                .SingleOrDefault(c => c.Id == vehicleOffer.CatalogId);

            if (catalog == null)
                catalog = await Add<Catalog>(vehicleOffer.CatalogId);
            
            catalog.Status = CatalogStatus.Published;
            catalog.ChangeRelevancePeriod(new Period(SystemTimeProvider.Instance.Get().AddDays(-1)));
        }
    }
}
