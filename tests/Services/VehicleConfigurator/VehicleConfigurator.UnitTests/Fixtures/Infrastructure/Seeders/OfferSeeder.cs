﻿using Microsoft.EntityFrameworkCore;
using NexCore.Common;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Contracts;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Dealers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class OfferSeeder : EntitySeeder<Offer>
    {
        public readonly ConfigurationSeeder Configuration;

        public OfferSeeder(DbContext context) : base(context)
        {
            Fixture.Customizations.Add(new ModelComponentBuilder());
            Fixture.Customize(new DealerIdCustomization());
            Fixture.Customize(new ContractTypeIdCustomization());

            Configuration = new ConfigurationSeeder(context);
        }

        public override async Task<Offer> Add() => await AddDependencies(await base.Add());

        public override async Task<Offer> Add(Offer offer) => await AddDependencies(await base.Add(offer));

        public override async Task<Offer> Add(object parameters) => await AddDependencies(await base.Add(parameters));

        public override async Task<Offer> Add(int id, object parameters) => await AddDependencies(await base.Add(id, parameters));

        protected virtual async Task<Offer> AddDependencies(Offer offer)
        {
            await FindOrAdd<Model, ModelId>(offer.ModelId);
            await FindOrAdd<Body, BodyId>(offer.BodyId);
            await FindOrAdd<Equipment, EquipmentId>(offer.EquipmentId);
            await FindOrAdd<Engine, EngineId>(offer.EngineId);
            await FindOrAdd<Gearbox, GearboxId>(offer.GearboxId);
            await FindOrAdd<Color, ColorId>(offer.ColorId);

            foreach (var prNumber in offer.Options.Values)
                await FindOrAdd<Option, OptionId>(OptionId.Next(offer.ModelId, prNumber));

            if (offer.PackageId != null)
                await AddPackageDependencies(offer);

            return offer;
        }

        private async Task AddPackageDependencies(Offer offer)
        {
            if (Has<Package, PackageId>(offer.PackageId))
            {
                var package = Find<Package, PackageId>(offer.PackageId);

                if (package.ModelKeySet.Values.Contains(offer.ModelKey) && package.IsApplicableTo(offer.Options))
                    return;

                throw new InvalidOperationException("Package specified in the offer is not applicable");
            }

            var catalog = await PersistCatalog(offer);


            var applicablePackage = new Package(offer.PackageId, catalog.Id, Create<string>(), new BoundModelKeySet(offer.ModelId),
                new Applicability(new RequiredSpecification(offer.Options.Values.First())));

            await Add<Package, PackageId>(applicablePackage);
        }

        private async Task<Catalog> PersistCatalog(Offer offer)
        {
            var catalog = await Add<Catalog>();
            catalog.ModelYear = offer.ModelYear;
            catalog.Status = CatalogStatus.Published;
            catalog.ChangeRelevancePeriod(new Period(SystemTimeProvider.Instance.Get().AddDays(-1)));
            return catalog;
        }

        public override async Task SeedAsync()
        {
            await Configuration.SeedAsync();

            await base.SeedAsync();
        }
    }
}
