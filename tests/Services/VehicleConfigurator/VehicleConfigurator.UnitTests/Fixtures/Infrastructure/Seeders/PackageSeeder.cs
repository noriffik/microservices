﻿using Microsoft.EntityFrameworkCore;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class PackageSeeder : EntitySeeder<Package, PackageId>
    {
        public PackageSeeder(DbContext context) : base(context)
        {
            Fixture.Customizations.Add(new ModelComponentBuilder());
        }

        public override async Task<Package> Add() => await AddDependencies(await base.Add());

        public override async Task<Package> Add(Package package) => await AddDependencies(await base.Add(package));

        public override async Task<Package> Add(object parameters) => await AddDependencies(await base.Add(parameters));

        public override async Task<Package> Add(PackageId id, object parameters) => await AddDependencies(await base.Add(id, parameters));

        public virtual async Task<Package> AddDependencies(Package package)
        {
            if (!Has<Catalog>(package.CatalogId))
                await Add<Catalog>(package.CatalogId);

            if (!Has<Model, ModelId>(package.ModelKeySet.ModelId))
                await Add<Model, ModelId>(package.ModelKeySet.ModelId);

            foreach (var modelKey in package.ModelKeySet.Values)
            {
                if (!Has<Body, BodyId>(modelKey.BodyId))
                    await Add<Body, BodyId>(modelKey.BodyId);

                if (!Has<Equipment, EquipmentId>(modelKey.EquipmentId))
                    await Add<Equipment, EquipmentId>(modelKey.EquipmentId);

                if (!Has<Engine, EngineId>(modelKey.EngineId))
                    await Add<Engine, EngineId>(modelKey.EngineId);

                if (!Has<Gearbox, GearboxId>(modelKey.GearboxId))
                    await Add<Gearbox, GearboxId>(modelKey.GearboxId);
            }

            var prNumbers = package.Applicability
                .Select(s => s.PrNumbers)
                .SelectMany(s => s.Values)
                .Distinct();

            var optionIds = OptionId.Next(package.ModelKeySet.ModelId, prNumbers);
            foreach (var optionId in optionIds)
            {
                if (Has<Option, OptionId>(optionId))
                    continue;

                var option = await Add<Option, OptionId>(optionId);

                if (option.OptionCategoryId.HasValue && !Has<OptionCategory>(option.OptionCategoryId.Value))
                    await Add<OptionCategory>(option.OptionCategoryId);
            }

            return package;
        }
    }
}
