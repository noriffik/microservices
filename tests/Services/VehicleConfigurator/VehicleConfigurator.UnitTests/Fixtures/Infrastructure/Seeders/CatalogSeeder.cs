﻿using Microsoft.EntityFrameworkCore;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders
{
    public class CatalogSeeder : EntitySeeder<Catalog>
    {
        public CatalogSeeder(DbContext context) : base(context)
        {
            Fixture.Customizations.Add(new DomainBuilder());
        }
    }
}
