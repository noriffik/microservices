﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Infrastructure;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure
{
    public partial class InMemoryDbContextFactory
    {
        private class TestDbContext : VehicleConfiguratorContext
        {
            public TestDbContext(DbContextOptions<VehicleConfiguratorContext> options, IMediator mediator) : base(
                options, mediator)
            {
            }

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                base.OnModelCreating(modelBuilder);

                modelBuilder.SetAutoIncrementStep(1000);
            }
        }
    }
}
