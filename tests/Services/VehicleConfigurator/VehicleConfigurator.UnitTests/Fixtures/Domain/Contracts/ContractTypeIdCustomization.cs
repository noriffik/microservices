﻿using AutoFixture;
using AutoFixture.Kernel;
using NexCore.Testing.Fixtures.SpecimenBuilders.Strings;
using NexCore.VehicleConfigurator.Domain.Contracts;
using System;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Contracts
{
    public class ContractTypeIdCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customizations.Add(new ContractTypeIdBuilder());
        }

        private class ContractTypeIdBuilder : ISpecimenBuilder
        {
            public object Create(object request, ISpecimenContext context)
            {
                var type = request as Type;
                if (type == null || type != typeof(ContractTypeId))
                    return new NoSpecimen();

                return ContractTypeId.Parse((string)context.Resolve(new AlphaNumericString(4)));
            }
        }
    }
}
