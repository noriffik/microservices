﻿using AutoFixture.Kernel;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Offers;
using NexCore.Vehicles.UnitTests.Fixtures;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles
{
    public class ModelComponentBuilder : CompositeSpecimenBuilder
    {
        public ModelComponentBuilder() : base(
            new VehiclesSpecimenBuilder(),
            new PackageCompositeBuilder(),
            new OfferBuilder())
        {
        }
    }
}
