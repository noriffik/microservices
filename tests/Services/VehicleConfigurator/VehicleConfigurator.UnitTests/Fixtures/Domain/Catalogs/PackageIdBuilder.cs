﻿using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Catalogs
{
    class PackageIdBuilder : SequentialEntityIdGenerator<PackageId>
    {
        public PackageIdBuilder() : base(5, PackageId.Parse)
        {
        }
    }
}
