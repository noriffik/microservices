﻿using AutoFixture;
using AutoFixture.Kernel;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using System;
using System.Linq;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Catalogs
{
    class ApplicabilityBuilder : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type;
            if (type == null || type != typeof(Applicability))
                return new NoSpecimen();

            var required = context.CreateMany<RequiredSpecification>().Cast<IApplicabilitySpecification>();
            var choices = context.CreateMany<ChoiceSpecification>();

            return new Applicability(required.Concat(choices));
        }
    }
}
