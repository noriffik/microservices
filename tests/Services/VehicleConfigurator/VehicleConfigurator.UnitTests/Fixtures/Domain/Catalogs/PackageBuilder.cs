﻿using AutoFixture;
using AutoFixture.Kernel;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Fixtures.SpecimenBuilders.Strings;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using System;
using System.Linq;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Catalogs
{
    public class PackageBuilder : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type;
            if (type == null || type != typeof(Package))
                return new NoSpecimen();

            var packageId = context.Create<PackageId>();

            return new Package(packageId,
                context.Create<int>(),
                context.Create<string>(),
                CreateModelKeySet(packageId.ModelId, context), context.Create<Applicability>());
        }

        private static BoundModelKeySet CreateModelKeySet(ModelId modelId, ISpecimenContext context)
        {
            return BoundModelKeySet.Parse(
                Enumerable.Range(0, 5)
                    .Select(n => modelId.Value + (string)context.Resolve(new AlphaNumericString(4)))
                    .Aggregate((a, b) => $"{a} {b}"));
        }
    }
}
