﻿using AutoFixture.Kernel;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Catalogs
{
    class PackageCompositeBuilder : CompositeSpecimenBuilder
    {
        public PackageCompositeBuilder() : base(
            new PackageCodeBuilder(),
            new PackageIdBuilder(),
            new PackageBuilder(),
            new ApplicabilityBuilder())
        {
        }
    }
}
