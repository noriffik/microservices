﻿using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Catalogs
{
    class PackageCodeBuilder : SequentialEntityIdGenerator<PackageCode>
    {
        public PackageCodeBuilder() : base(3, PackageCode.Parse)
        {
        }
    }
}
