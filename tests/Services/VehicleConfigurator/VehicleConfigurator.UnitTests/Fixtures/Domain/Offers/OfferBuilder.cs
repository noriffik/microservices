﻿using AutoFixture;
using AutoFixture.Kernel;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Offers;
using System;
using System.Linq;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Offers
{
    public class OfferBuilder : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            var type = request as Type;
            if (type == null || type != typeof(Offer))
                return new NoSpecimen();
            
            var price = context.Create<ConfigurationPrice>();
            var options = new PrNumberSet(price.Options.PerOption.Keys);

            var vehicleOffer = context.Create<VehicleOffer>();
            var includedOptions = new PrNumberSet(vehicleOffer.Options
                .Where(o => o.Availability.Type == AvailabilityType.Included)
                .Select(o => o.PrNumber));

            return new Offer(
                context.Create<int>(),
                context.Create<ModelKey>(),
                context.Create<PackageId>(),
                options,
                includedOptions,
                price,
                context.Create<ColorId>());
        }
    }
}
