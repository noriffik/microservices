﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures
{
    public class TestModelComponentId : EntityId
    {
        public TestModelComponentId(string value) : base(value)
        {
        }
    }

    public class TestModelComponent : ModelComponent<TestModelComponentId>
    {
        public TestModelComponent(TestModelComponentId id) : base(id)
        {
        }
    }
}
