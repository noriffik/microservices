﻿using AutoMapper;
using MediatR;
using NexCore.VehicleConfigurator.Api.Infrastructure.AutoMapper;
using NexCore.VehicleConfigurator.Infrastructure;
using System;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries
{
    public abstract class QueryFixture<TQuery, TResponse> where TQuery : IRequest<TResponse>
    {
        protected bool IsReady;

        public readonly IMapper Mapper;

        protected QueryFixture()
        {
            var mapperConfig = new MapperConfiguration(
                c => c.AddProfiles(typeof(DefaultProfile).Assembly));

            Mapper = mapperConfig.CreateMapper();
        }
        
        public virtual Task Setup(VehicleConfiguratorContext context)
        {
            IsReady = true;

            return Task.CompletedTask;
        }

        public abstract TQuery GetQuery();

        public abstract TQuery GetUnpreparedQuery();

        public abstract TResponse GetResponse();

        protected void ThrowSetupIsRequired()
        {
            if (!IsReady)
                throw new InvalidOperationException("Setup must be called first.");
        }
    }
}