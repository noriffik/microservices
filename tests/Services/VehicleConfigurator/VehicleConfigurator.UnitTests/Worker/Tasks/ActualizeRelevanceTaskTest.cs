﻿using AutoFixture;
using MediatR;
using Microsoft.Extensions.Logging;
using Moq;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using NexCore.VehicleConfigurator.Worker.Tasks;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Worker.Tasks
{
    public class ActualizeRelevanceTaskTest
    {
        //Dependencies
        private readonly Mock<IMediator> _mediator;

        //Task
        private readonly ActualizeRelevanceTask<TestAggregateRoot> _task;

        public ActualizeRelevanceTaskTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _mediator = new Mock<IMediator>();

            //Task
            _task = new ActualizeRelevanceTask<TestAggregateRoot>(
                _mediator.Object, new Mock<ILogger<ActualizeRelevanceTask<TestAggregateRoot>>>().Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ActualizeRelevanceTask<TestAggregateRoot>)).HasNullGuard();
        }

        [Fact]
        public async Task Perform()
        {
            //Act
            await _task.Perform();

            //Assert
            _mediator.Verify(m => m.Send(It.Is<ActualizeRelevanceCommand<TestAggregateRoot>>(c => c.CatalogId == null),
                It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}
