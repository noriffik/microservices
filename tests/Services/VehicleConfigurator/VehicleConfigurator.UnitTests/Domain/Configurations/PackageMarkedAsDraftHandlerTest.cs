﻿using AutoFixture;
using Moq;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations
{
    public class PackageMarkedAsDraftHandlerTest
    {
        //Dependency
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly IReadOnlyList<Configuration> _configurations;

        //Event and handler
        private readonly PackageMarkedAsDraftEvent _event;
        private readonly PackageMarkedAsDraftHandler _handler;

        public PackageMarkedAsDraftHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _repository = new Mock<IEntityRepository>();
            var work = new Mock<IUnitOfWork>();
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entity
            var packageId = fixture.Create<PackageId>();
            _configurations = fixture.CreateMany<Configuration>(5).ToList();
            foreach (var configuration in _configurations)
                configuration.IncludePackage(packageId);

            //Event and handler
            _event = new PackageMarkedAsDraftEvent(packageId);
            _handler = new PackageMarkedAsDraftHandler(work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(PackageCoverageUpdatedHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenNotification_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("notification", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            _repository.Setup(r => r.Find(It.Is<ByPackageIdSpecification>(s => s.PackageId == _event.PackageId), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_configurations)
                .Verifiable();

            //Act
            await _handler.Handle(_event, CancellationToken.None);

            //Assert
            Assert.All(_configurations, configuration => Assert.Equal(Relevance.Irrelevant, configuration.Relevance));
            _repository.Verify();
        }
    }
}
