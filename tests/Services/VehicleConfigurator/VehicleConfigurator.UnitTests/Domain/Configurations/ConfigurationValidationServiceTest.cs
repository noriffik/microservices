﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Rules;
using NexCore.VehicleConfigurator.UnitTests.Extensions.Domain;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations
{
    public class ConfigurationValidationServiceTest
    {
        private readonly Fixture _fixture = new Fixture();

        //Entity
        private readonly Configuration _configuration;
        private readonly Package _package;

        //Dependencies
        private readonly VehicleOffer _vehicleOffer;
        private readonly PrNumber _prNumber;
        private readonly Catalog _catalog;
        private readonly ModelKey _modelKey;
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _entityRepository;
        private readonly Mock<IConfigurationValidator> _configurationValidator;

        private readonly ConfigurationValidationService _service;

        public ConfigurationValidationServiceTest()
        {
            _fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _configuration = _fixture.Create<Configuration>();
            _package = _fixture.Create<Package>();
            _package.ChangeStatus(PackageStatus.Published);

            //Dependencies
            _prNumber = _fixture.Create<PrNumber>();
            _catalog = _fixture.Create<Catalog>();
            _modelKey = _fixture.Create<ModelKey>();;
            _vehicleOffer = new VehicleOffer(_modelKey, _catalog.Id);
            _work = new Mock<IUnitOfWork>();
            _configurationValidator = new Mock<IConfigurationValidator>();
            _entityRepository = new Mock<IEntityRepository>();

            //Setup mock
            _work.Setup(w => w.EntityRepository).Returns(_entityRepository.Object);
            _entityRepository.Setup(r => r.Require<Configuration>(_configuration.Id))
                .ReturnsAsync(_configuration);
            
            _service = new ConfigurationValidationService(_work.Object)
            {
                Validator = _configurationValidator.Object
            };
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ConfigurationValidationService)).HasNullGuard();
        }

        [Fact]
        public void Validator()
        {
            Assert.Injection
                .OfProperty(_service, nameof(_service.Validator))
                .HasDefault()
                .HasNullGuard()
                .HasDefault();
        }

        [Fact]
        public async Task ValidateOptionInclusion()
        {
            using (Sequence.Create())
            {
                //Arrange
                var vehicleOffer = _fixture.Create<VehicleOffer>();
                vehicleOffer.AddOptionOffer(_prNumber, _fixture.Create<Availability>());
               
                _entityRepository.Setup(r => r.Require(It.Is<PublishedVehicleOfferSpecification>(s =>
                        s.CatalogId == _configuration.CatalogId && s.ModelKey == _configuration.ModelKey)))
                    .InSequence()
                    .ReturnsAsync(vehicleOffer);

                //Act
                await _service.ValidateOptionInclusion(_configuration, _prNumber);
            }
        }

        [Fact]
        public Task ValidateOptionInclusion_GivenConfiguration_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("configuration", () =>
                _service.ValidateOptionInclusion(null, PrNumber.Parse("AAA")));
        }

        [Fact]
        public Task ValidateOptionInclusion_GivenPrNumber_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("prNumber", () =>
                _service.ValidateOptionInclusion(_configuration, null));
        }
        
        [Fact]
        public Task ValidateOptionInclusion_GivenPrNumber_IsInvalid_Throws()
        {
            //Arrange
            _entityRepository.Setup(r => r.Require(It.Is<PublishedVehicleOfferSpecification>(s =>
                    s.CatalogId == _configuration.CatalogId && s.ModelKey == _configuration.ModelKey)))
                .ReturnsAsync(_vehicleOffer);

            //Assert
            return Assert.ThrowsAsync<UnavailableOptionException>(() => _service.ValidateOptionInclusion(_configuration, _prNumber));
        }

        [Fact]
        public async Task ValidateOptionInclusion_WhenOptionOffer_IsIrrelevant_Throws()
        {
            //Arrange
            var vehicleOffer = _fixture.Create<VehicleOffer>();
            var optionOffer = vehicleOffer.AddOptionOffer(_prNumber, _fixture.Create<Availability>());
            optionOffer.ChangeRelevance(Relevance.Irrelevant);

            _entityRepository.Setup(r => r.Require(It.Is<PublishedVehicleOfferSpecification>(s =>
                    s.CatalogId == _configuration.CatalogId && s.ModelKey == _configuration.ModelKey)))
                .ReturnsAsync(vehicleOffer);

            //Act
            var e = await Assert.ThrowsAsync<ItemOfferIrrelevantException>(() => _service.ValidateOptionInclusion(_configuration, _prNumber));

            //Assert
            Assert.Equal(typeof(OptionOffer), e.ComponentType);
        }

        [Fact]
        public async Task ValidateConfiguration()
        {
            using (Sequence.Create())
            {
                //Arrange
                _entityRepository.Setup(r => r.Require(It.Is<RelevantCatalogPublishedForModelYearSpecification>(s =>
                        s.ModelYear == _catalog.ModelYear)))
                    .InSequence()
                    .ReturnsAsync(_catalog);

                _entityRepository.Setup(r => r.Require(It.Is<PublishedVehicleOfferSpecification>(s =>
                        s.CatalogId == _catalog.Id && s.ModelKey == _configuration.ModelKey)))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer);

                //Act
                await _service.ValidateConfiguration(_catalog.ModelYear, _configuration.ModelKey, PrNumberSet.Parse("AAA"));
            }
        }

        [Fact]
        public Task ValidateConfiguration_GivenModelYear_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("modelYear", () =>
                _service.ValidateConfiguration(null, _modelKey, PrNumberSet.Parse("AAA")));
        }

        [Fact]
        public Task ValidateConfiguration_GivenModelKey_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("modelKey", () =>
                _service.ValidateConfiguration(_catalog.ModelYear, null, PrNumberSet.Parse("AAA")));
        }

        [Fact]
        public Task ValidateConfiguration_GivenPrNumberSet_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("prNumbers", () =>
                _service.ValidateConfiguration(_catalog.ModelYear, _modelKey, null));
        }

        [Fact]
        public async Task ValidateThatOptionsAreAvailable()
        {
            using (Sequence.Create())
            {
                //Arrange
                var vehicleOffer = _fixture.Create<VehicleOffer>();
                vehicleOffer.AddOptionOffer(_prNumber, _fixture.Create<Availability>());
               
                _entityRepository.Setup(r => r.Require(It.Is<PublishedVehicleOfferSpecification>(s =>
                        s.CatalogId == _configuration.CatalogId && s.ModelKey == _configuration.ModelKey)))
                    .InSequence()
                    .ReturnsAsync(vehicleOffer);

                //Act
                await _service.ValidateThatOptionsAreAvailable(_configuration, PrNumberSet.Parse(_prNumber));
            }
        }
        
        [Fact]
        public Task ValidateThatOptionsAreAvailable_GivenConfiguration_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("configuration", () =>
                _service.ValidateThatOptionsAreAvailable(null, PrNumberSet.Parse("AAA")));
        }

        [Fact]
        public Task ValidateThatOptionsAreAvailable_GivenPrNumberSet_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("prNumbers", () =>
                _service.ValidateThatOptionsAreAvailable(_configuration, null));
        }

        [Fact]
        public Task ValidateThatOptionsAreAvailable_GivenPrNumberSet_IsInvalid_Throws()
        {
            //Arrange
            _entityRepository.Setup(r => r.Require(It.Is<PublishedVehicleOfferSpecification>(s =>
                    s.CatalogId == _configuration.CatalogId && s.ModelKey == _configuration.ModelKey)))
                .ReturnsAsync(_vehicleOffer);

            //Assert
            return Assert.ThrowsAsync<UnavailableOptionException>(() => _service.ValidateThatOptionsAreAvailable(_configuration, PrNumberSet.Parse(_prNumber)));
        }

        [Fact]
        public async Task ValidateThatOptionsAreAvailable_WhenOptionOffer_IsIrrelevant_Throws()
        {
            //Arrange
            var vehicleOffer = _fixture.Create<VehicleOffer>();
            var optionOffer = vehicleOffer.AddOptionOffer(_prNumber, _fixture.Create<Availability>());
            optionOffer.ChangeRelevance(Relevance.Irrelevant);

            _entityRepository.Setup(r => r.Require(It.Is<PublishedVehicleOfferSpecification>(s =>
                    s.CatalogId == _configuration.CatalogId && s.ModelKey == _configuration.ModelKey)))
                .ReturnsAsync(vehicleOffer);

            //Act
            var e = await Assert.ThrowsAsync<ItemOfferIrrelevantException>(() => _service.ValidateThatOptionsAreAvailable(_configuration, PrNumberSet.Parse(_prNumber)));

            //Assert
            Assert.Equal(typeof(OptionOffer), e.ComponentType);
        }
        
        [Fact]
        public Task VerifyThatOptionIsNotInPackage_GivenPrNumber_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("prNumber", () =>
                _service.VerifyThatOptionIsNotInPackage(null, _configuration));
        }

        [Fact]
        public Task VerifyThatOptionIsNotInPackage_GivenConfiguration_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("configuration", () =>
                _service.VerifyThatOptionIsNotInPackage(_prNumber, null));
        }

        [Fact]
        public async Task VerifyThatOptionIsNotInPackage_WhenIs_Throws()
        {
            //Arrange
            //Assign package to configuration
            var prNumbers = _package.ApplicableWith();
            _configuration.IncludeOptions(prNumbers);
            _configuration.IncludePackage(_package.Id);

            //Setup entity loading
            _entityRepository.Setup(r =>
                    r.Require<Package, PackageId>(_configuration.PackageId))
                .ReturnsAsync(_package);

            //Setup expectations
            var exclude = prNumbers.Values.First();
            var expected = new PackageRequirementViolationException(_package.Id.Value, exclude);

            //Act
            var actual = await Assert.ThrowsAsync<PackageRequirementViolationException>(
                () => _service.VerifyThatOptionIsNotInPackage(exclude, _configuration));

            //Assert
            Assert.Equal(expected.Message, actual.Message);
        }

        [Fact]
        public async Task VerifyThatOptionIsNotInPackage()
        {
            //Arrange
            var vehicleOffer = _fixture.Create<VehicleOffer>();
            vehicleOffer.AddOptionOffer(_prNumber, _fixture.Create<Availability>());

            //Assign package to configuration
            _configuration.IncludeOption(_prNumber);
            _configuration.IncludeOptions(_package.ApplicableWith());
            _configuration.IncludePackage(_package.Id);

            //Setup entity loading
            _entityRepository.Setup(r =>
                    r.Require<Package, PackageId>(_configuration.PackageId))
                .ReturnsAsync(_package);

            _entityRepository.Setup(r => r.Require(It.Is<PublishedVehicleOfferSpecification>(s =>
                    s.CatalogId == _configuration.CatalogId && s.ModelKey == _configuration.ModelKey)))
                .ReturnsAsync(vehicleOffer);

            _configurationValidator.Setup(r => r.Validate(_configuration.Options.Shrink(_prNumber), vehicleOffer));

            //Act
            await _service.VerifyThatOptionIsNotRequired(_configuration, _prNumber);
        }

        [Fact]
        public Task VerifyThatOptionIsNotRequired_GivenConfiguration_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("configuration", () =>
                _service.VerifyThatOptionIsNotRequired(null, _prNumber));
        }

        [Fact]
        public Task VerifyThatOptionIsNotRequired_GivenPrNumber_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("prNumber", () =>
                _service.VerifyThatOptionIsNotRequired(_configuration, null));
        }
        
        [Fact]
        public Task VerifyThatOptionIsNotRequired_WhenGivenPrNumber_IsNotValid_Throws()
        {
            //Arrange
            var vehicleOffer = _fixture.Create<VehicleOffer>();

            _entityRepository.Setup(r => r.Require(It.Is<PublishedVehicleOfferSpecification>(s =>
                    s.CatalogId == _configuration.CatalogId && s.ModelKey == _configuration.ModelKey)))
                .ReturnsAsync(vehicleOffer);

            //Assert
            return Assert.ThrowsAsync<UnavailableOptionException>(() =>
                _service.VerifyThatOptionIsNotRequired(_configuration, _prNumber));
        }

        [Fact]
        public async Task VerifyThatOptionIsNotRequired()
        {
            using (Sequence.Create())
            {
                //Arrange
                var vehicleOffer = _fixture.Create<VehicleOffer>();
                vehicleOffer.AddOptionOffer(_prNumber, _fixture.Create<Availability>());
               
                _entityRepository.Setup(r => r.Require(It.Is<PublishedVehicleOfferSpecification>(s =>
                        s.CatalogId == _configuration.CatalogId && s.ModelKey == _configuration.ModelKey)))
                    .InSequence()
                    .ReturnsAsync(vehicleOffer);

                //Act
                await _service.VerifyThatOptionIsNotRequired(_configuration, _prNumber);
            }
        }

        [Fact]
        public Task ValidateColorAvailability_GivenConfiguration_IsNull_Throws()
        {
            //Arrange
            var colorId = _fixture.Create<ColorId>();

            return Assert.ThrowsAsync<ArgumentNullException>("configuration", () =>
                _service.ValidateColorAvailability(null, colorId));
        }

        [Fact]
        public Task ValidateColorAvailability_GivenColorId_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("colorId", () =>
                _service.ValidateColorAvailability(_configuration, null));
        }
        
        [Fact]
        public async Task ValidateColorAvailability_WhenColorOffer_IsNotFound_Throws()
        {
            //Arrange
            var vehicleOffer = _fixture.Create<VehicleOffer>();
            var colorId = _fixture.Create<ColorId>();

            _entityRepository.Setup(r => r.Require<Configuration>(_configuration.Id))
                .ReturnsAsync(_configuration);
            _entityRepository.Setup(r => r.Require(It.Is<PublishedVehicleOfferSpecification>(s =>
                    s.CatalogId == _configuration.CatalogId && s.ModelKey == _configuration.ModelKey)))
                .ReturnsAsync(vehicleOffer);

            //Act
            var e = await Assert.ThrowsAsync<ColorOfferNotFoundException>(() =>
                _service.ValidateColorAvailability(_configuration, colorId));

            //Assert
            Assert.Equal(vehicleOffer.ModelKey, e.ModelKey);
            Assert.Equal(colorId, e.ColorId);
        }

        [Fact]
        public async Task ValidateColorAvailability_WhenColorOffer_IsIrrelevant_Throws()
        {
            //Arrange
            var vehicleOffer = _fixture.Create<VehicleOffer>();
            var colorId = _fixture.Create<ColorId>();
            var colorOffer = vehicleOffer.AddColorOffer(colorId, _fixture.Create<decimal>());
            colorOffer.ChangeRelevance(Relevance.Irrelevant);

            _entityRepository.Setup(r => r.Require<Configuration>(_configuration.Id))
                .ReturnsAsync(_configuration);
            _entityRepository.Setup(r => r.Require(It.Is<PublishedVehicleOfferSpecification>(s =>
                    s.CatalogId == _configuration.CatalogId && s.ModelKey == _configuration.ModelKey)))
                .ReturnsAsync(vehicleOffer);

            //Act
            var e = await Assert.ThrowsAsync<ItemOfferIrrelevantException>(() =>
                _service.ValidateColorAvailability(_configuration, colorId));

            //Assert
            Assert.Equal(typeof(ColorOffer), e.ComponentType);
        }
    }
}
