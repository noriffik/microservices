﻿using AutoFixture;
using Moq;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing.Packages;
using NexCore.VehicleConfigurator.Domain.Pricing;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations.Pricing
{
    public class PackagePricingServiceTest
    {
        private readonly Fixture _fixture;

        //Entities
        private readonly DateTime _date;
        private readonly Price _packagePrice;
        private readonly Package _package;
        private readonly VehicleOffer _vehicleOffer;

        //Service and dependencies
        private readonly PackagePricingService _service;
        private readonly Mock<IOptionPricingService> _pricingService;

        public PackagePricingServiceTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            //Entities
            _date = _fixture.Create<DateTime>();
            _packagePrice = _fixture.Create<Price>();
            _package = _fixture.Create<Package>();
            _vehicleOffer = CreateVehicleOffer();

            //Setup dependencies
            _pricingService = new Mock<IOptionPricingService>();
            _pricingService.Setup(s => s.CreatePrice(_package.Price, _date))
                .ReturnsAsync(_packagePrice);

            //Setup service
            _service = new PackagePricingService(_pricingService.Object);
        }

        private VehicleOffer CreateVehicleOffer()
        {
            var vehicleOffer = _fixture.Create<VehicleOffer>();

            foreach (var option in _fixture.CreateMany<PrNumber>(6))
                vehicleOffer.AddOptionOffer(option, _fixture.Create<Availability>());

            return vehicleOffer;
        }

        private IOptionApplicabilityFilter SetupFilter(OptionsPrice optionsPrice, Applicability applicability, IEnumerable<OptionOffer> options)
        {
            var filtered = _fixture.CreateMany<OptionOffer>(2).ToList();

            _pricingService.Setup(s => s.Calculate(filtered, _date))
                .ReturnsAsync(optionsPrice);

            var filter = new Mock<IOptionApplicabilityFilter>();
            filter.Setup(f => f.Apply(applicability, options))
                .Returns(filtered);

            return filter.Object;
        }

        [Fact]
        public void Ctor_GivenPricingService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("pricingService", () => new PackagePricingService(null));
        }

        [Fact]
        public async Task Calculate_WhenOptionOffers_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("optionOffers", () => _service.Calculate(null, _fixture.Create<Package>(), _fixture.Create<DateTime>()));
        }

        [Fact]
        public async Task Calculate_WhenPackage_IsNull_ReturnsEmpty()
        {
            //Act
            var price = await _service.Calculate(_fixture.CreateMany<OptionOffer>(1), null, _fixture.Create<DateTime>());

            //Assert
            Assert.Same(PackagePrice.Empty, price);
        }

        [Fact]
        public async Task Calculate()
        {
            //Arrange
            var optionsPrice = _fixture.Create<OptionsPrice>();
            var withoutPrice = _fixture.Create<OptionsPrice>();

            var service = new PackagePricingService(_pricingService.Object)
            {
                ApplicableOptionFilter = SetupFilter(optionsPrice, _package.Applicability, _vehicleOffer.Options),
                WithoutPackageOptionFilter = SetupFilter(withoutPrice, _package.Applicability, _vehicleOffer.Options)
            };

            var expected = new PackagePrice(_packagePrice, withoutPrice.Total, optionsPrice);

            //Act
            var actual = await service.Calculate(_vehicleOffer.Options, _package, _date);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<PackagePrice>.Instance);
        }

        [Fact]
        public async Task Calculate_WithOptionOffers()
        {
            //Arrange
            var optionsPrice = _fixture.Create<OptionsPrice>();
            var withoutPrice = _fixture.Create<OptionsPrice>();

            var withoutFilter = SetupFilter(withoutPrice, _package.Applicability, _vehicleOffer.Options);

            var service = new PackagePricingService(_pricingService.Object)
            {
                ApplicableOptionFilter = SetupFilter(optionsPrice, _package.Applicability, _vehicleOffer.Options)
            };

            var expected = new PackagePrice(_packagePrice, withoutPrice.Total, optionsPrice);

            //Act
            var actual = await service.Calculate(_vehicleOffer.Options, _package, withoutFilter, _date);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<PackagePrice>.Instance);
        }

        [Fact]
        public async Task CalculateMany()
        {
            //Arrange
            var serviceMock = new Mock<PackagePricingService>(_pricingService.Object)
            {
                CallBase = true
            };

            var vehicle = _fixture.Create<VehicleOffer>();

            var packages = _fixture.CreateMany<Package>(2).ToList();
            var expected = new Dictionary<PackageId, PackagePrice>();

            foreach (var package in packages)
            {
                var price = _fixture.Create<PackagePrice>();

                serviceMock.Setup(s => s.Calculate(vehicle.Options, package, _date))
                    .ReturnsAsync(price);

                expected[package.Id] = price;
            }

            //Act
            var result = await serviceMock.Object.CalculateMany(packages, vehicle.Options, _date);

            //Assert
            Assert.Equal(expected, result, PropertyComparer<IDictionary<PackageId, PackagePrice>>.Instance);
        }

        [Fact]
        public async Task CalculateMany_WhenPackages_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("packages",
                () => _service.CalculateMany(null, new List<OptionOffer>(), _date));
        }

        [Fact]
        public async Task CalculateMany_WhenOptionOffers_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("optionOffers",
                () => _service.CalculateMany(new List<Package>(), null, _date));
        }
    }
}