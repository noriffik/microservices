﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing.Packages;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations.Pricing.Packages
{
    public class ApplicableOptionFilterTest
    {
        [Fact]
        public void Apply()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var expected = fixture.CreateMany<OptionOffer>(3).ToList();
            var unexpected = fixture.CreateMany<OptionOffer>(2);
            var specification = new ChoiceSpecification(new PrNumberSet(expected.Select(e => e.PrNumber)));
            var options = expected.Concat(unexpected);

            var filter = new ApplicableOptionFilter();

            //Act
            var result = filter.Apply(specification, options);

            //Assert
            Assert.Equal(expected, result);
        }
    }
}
