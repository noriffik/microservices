﻿using AutoFixture;
using Moq;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing.Packages;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations.Pricing.Packages
{
    public class OptionApplicabilityFilterTest
    {
        [Fact]
        public void Apply()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var algorithm = new Mock<OptionApplicabilityFilter>();

            var options = fixture.CreateMany<OptionOffer>(3).ToList();
            var applicability = fixture.Create<Applicability>();

            var expected = options.Take(1).ToList();

            foreach (var specification in applicability)
            {
                algorithm.Setup(a => a.Apply(specification, options))
                    .Returns(expected)
                    .Verifiable();
            }

            //Act
            var actual = algorithm.Object.Apply(applicability, options);

            //Assert
            Assert.Equal(expected, actual);

            algorithm.Verify();
        }

        [Fact]
        public void Extract_WhenOptions_DoNotSatisfy_FilterCriteria_ReturnsEmpty()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var algorithm = new Mock<OptionApplicabilityFilter>();

            var options = fixture.CreateMany<OptionOffer>(3);
            var specification = fixture.Create<RequiredSpecification>();
            var applicability = new Applicability(specification);

            algorithm.Setup(a => a.Apply(specification, options))
                .Returns(new List<OptionOffer>());

            //Act
            var actual = algorithm.Object.Apply(applicability, options);

            //Assert
            Assert.Empty(actual);
        }
    }
}