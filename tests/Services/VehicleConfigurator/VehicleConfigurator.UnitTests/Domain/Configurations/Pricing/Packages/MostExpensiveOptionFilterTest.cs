﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing.Packages;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations.Pricing.Packages
{
    public class MostExpensiveOptionFilterTest
    {
        [Fact]
        public void Apply()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var prNumbers = PrNumberSet.Parse("XX1 XX2 XX3");
            var specification = new ChoiceSpecification(prNumbers);
            var applicability = new Applicability(specification);

            var options = Enumerable.Range(0, prNumbers.Values.Count()).Select(i =>
                    new OptionOffer(OptionId.Next(ModelId.Parse("XX"), prNumbers.Values.ToArray()[i]), Availability.Purchasable(i * 10)))
                .ToList();

            var algorithm = new MostExpensiveOptionFilter();
            var expected = options.TakeLast(1);

            //Act
            var result = algorithm.Apply(applicability, options);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Extract_WhenOptions_DoNotSatisfy_FilterCriteria_ReturnsEmpty()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var specification = fixture.Create<RequiredSpecification>();
            var applicability = new Applicability(specification);

            var algorithm = new MostExpensiveOptionFilter();

            //Act
            var result = algorithm.Apply(applicability, new List<OptionOffer>());

            //Assert
            Assert.Empty(result);
        }
    }
}