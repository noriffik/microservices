﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Pricing;
using System;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations.Pricing
{
    public class ConfigurationPriceTest
    {
        [Fact]
        public void Create()
        {
            //Arrange
            var onDate = DateTime.MaxValue;
            var vehiclePrice = new Price(10000, 10000);
            var colorPrice = new Price(100, 100);
            var optionsPrice = new Price(150, 150);
            var packageOptionsPrice = new Price(90, 90);
            var withoutPackagePrice = new Price(10250, 10250);
            var withPackagePrice = new Price(10210, 10210);
            var configurationPackageSavings = new Price(40, 40);
            var totalPrice = new Price(10210, 10210);
            var configurationPerOptionPrice = new Dictionary<PrNumber, Price>
            {
                {PrNumber.Parse("001"), new Price(10, 10)},
                {PrNumber.Parse("002"), new Price(20, 20)},
                {PrNumber.Parse("003"), new Price(30, 30)},
                {PrNumber.Parse("004"), new Price(40, 40)},
                {PrNumber.Parse("005"), new Price(50, 50)}
            };
            var packagePerOptionPrice = new Dictionary<PrNumber, Price>
            {
                {PrNumber.Parse("004"), new Price(40, 40)},
                {PrNumber.Parse("005"), new Price(50, 50)}
            };
            var packagePrice = PackagePrice.Create(new Price(50, 50), packageOptionsPrice, packagePerOptionPrice);

            //Act
            var configuration = new ConfigurationPrice(vehiclePrice, colorPrice, OptionsPrice.Create(configurationPerOptionPrice), packagePrice, onDate);

            //Assert
            Assert.Equal(vehiclePrice, configuration.Vehicle);
            Assert.Equal(optionsPrice, configuration.Options.Total);
            Assert.Equal(packageOptionsPrice, configuration.Package.Options.Total);
            Assert.Equal(withoutPackagePrice, configuration.PackageSavings.Without);
            Assert.Equal(withPackagePrice, configuration.PackageSavings.With);
            Assert.Equal(configurationPackageSavings, configuration.PackageSavings.Savings);
            Assert.Equal(totalPrice, configuration.Total);
            Assert.Equal(packagePrice, configuration.Package);
            Assert.Equal(onDate, configuration.OnDate);
            Assert.Equal(colorPrice, configuration.Color);
        }

        [Fact]
        public void Create_WithoutPackage()
        {
            var onDate = DateTime.MaxValue;
            var vehiclePrice = new Price(10000, 10000);
            var colorPrice = new Price(100, 100);
            var optionsPrice = new Price(150, 150);
            var withoutPackagePrice = new Price(10250, 10250);
            var withPackagePrice = new Price(10250, 10250);
            var configurationPackageSavings = new Price(0, 0);
            var totalPrice = new Price(10250, 10250);
            var configurationPerOptionPrice = new Dictionary<PrNumber, Price>
            {
                {PrNumber.Parse("001"), new Price(10, 10)},
                {PrNumber.Parse("002"), new Price(20, 20)},
                {PrNumber.Parse("003"), new Price(30, 30)},
                {PrNumber.Parse("004"), new Price(40, 40)},
                {PrNumber.Parse("005"), new Price(50, 50)}
            };
            var packagePrice = PackagePrice.Empty;

            //Act
            var configuration = new ConfigurationPrice(vehiclePrice, colorPrice, OptionsPrice.Create(configurationPerOptionPrice), packagePrice, onDate);

            //Assert
            Assert.Equal(vehiclePrice, configuration.Vehicle);
            Assert.Equal(optionsPrice, configuration.Options.Total);
            Assert.Equal(withoutPackagePrice, configuration.PackageSavings.Without);
            Assert.Equal(withPackagePrice, configuration.PackageSavings.With);
            Assert.Equal(configurationPackageSavings, configuration.PackageSavings.Savings);
            Assert.Equal(totalPrice, configuration.Total);
            Assert.Equal(packagePrice, configuration.Package);
            Assert.Equal(onDate, configuration.OnDate);
            Assert.Equal(colorPrice, configuration.Color);
        }
    }
}
