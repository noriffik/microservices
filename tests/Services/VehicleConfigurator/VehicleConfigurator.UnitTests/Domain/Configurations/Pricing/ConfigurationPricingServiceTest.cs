﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.CurrencyExchange.Abstract;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Pricing;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations.Pricing
{
    public class ConfigurationPricingServiceTest
    {
        private readonly Price _vehiclePrice = new Price(10000, 10000 * 2);
        private readonly Price _optionsPrice = new Price(150, 150 * 2);
        private readonly Price _withoutPackagePrice = new Price(10150, 10150 * 2);
        private readonly Price _withPackagePrice = new Price(10110, 10110 * 2);
        private readonly Price _configurationPackageSavings = new Price(40, 40 * 2);
        private readonly Price _totalPrice = new Price(10110, 10110 * 2);
        private readonly Price _defaultColorPrice = new Price(0, 0);
        private readonly PackagePrice _packagePrice;

        private readonly IDictionary<PrNumber, Price> _configurationPerOptionPrice;
        private readonly DateTime _date;

        //Entities
        private readonly VehicleOffer _vehicleOffer;
        private readonly Configuration _configuration;
        private readonly List<OptionOffer> _configurationOptionOffers;
        private readonly Package _package;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IPackagePricingService> _packagePricingService;

        //Service
        private readonly ConfigurationPricingService _service;
        private readonly Mock<OptionPricingService> _pricingService;

        public ConfigurationPricingServiceTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var packageOptions = new OptionsPrice(new Dictionary<PrNumber, Price>(), new Price(90, 90 * 2));
            _date = fixture.Create<DateTime>();
            _packagePrice = new PackagePrice(
                new Price(50, 50 * 2),
                packageOptions.Total,
                packageOptions);

            //Setup entities
            var defaultColorId = fixture.Create<ColorId>();
            _vehicleOffer = CreateVehicleOffer(_vehiclePrice, defaultColorId);
            _configuration = CreateConfiguration(_vehicleOffer, defaultColorId);
            _package = fixture.Create<Package>();

            //Prices per options for configuration
            _configurationOptionOffers = _configuration.Options
                .Values
                .Select(prNumber => _vehicleOffer.Options.Single(o => o.PrNumber == prNumber))
                .ToList();

            _configurationPerOptionPrice = _configurationOptionOffers
                .Select(o => new { o.PrNumber, Price = new Price(o.Availability.Price, o.Availability.Price * 2) })
                .ToDictionary(k => k.PrNumber, v => v.Price);

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            _pricingService = new Mock<OptionPricingService>(new Mock<ICurrencyExchangeService>().Object);
            _pricingService.Setup(s => s.CalculateRetail(It.IsAny<decimal>(), _date))
                .Returns<decimal, DateTime>((p, d) => Task.FromResult(p * 2));

            _packagePricingService = new Mock<IPackagePricingService>();

            //Service
            _service = new ConfigurationPricingService(_work.Object, _pricingService.Object, _packagePricingService.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ConfigurationPricingService)).HasNullGuard();
        }

        [Fact]
        public async Task Calculate_WithConfigurationId()
        {
            //Arrange
            _configuration.IncludePackage(_package.Id);

            _packagePricingService.Setup(s => s.Calculate(_configurationOptionOffers, _package, _date))
                .ReturnsAsync(_packagePrice);

            using (Sequence.Create())
            {
                //Setup repositories
                _repository.Setup(r => r.Require<Configuration>(_configuration.Id))
                    .InSequence()
                    .ReturnsAsync(_configuration);

                _repository.Setup(r => r.SingleOrDefault(It.Is<VehicleOfferForVehicleSpecification>(s =>
                        s.ModelKey == _configuration.ModelKey && s.CatalogId == _configuration.CatalogId)))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer);

                _repository.Setup(r => r.SingleOrDefault<Package, PackageId>(_configuration.PackageId))
                    .InSequence()
                    .ReturnsAsync(_package);

                //Act
                var price = await _service.Calculate(_configuration.Id, _date);

                //Assert
                Assert.NotNull(price);
                Assert.Equal(_vehiclePrice, price.Vehicle, PriceComparer.Instance);
                Assert.Equal(_configurationPerOptionPrice, price.Options.PerOption);
                Assert.Equal(_optionsPrice, price.Options.Total, PriceComparer.Instance);
                Assert.Equal(_withoutPackagePrice, price.PackageSavings.Without, PriceComparer.Instance);
                Assert.Equal(_withPackagePrice, price.PackageSavings.With, PriceComparer.Instance);
                Assert.Equal(_configurationPackageSavings, price.PackageSavings.Savings, PriceComparer.Instance);
                Assert.Equal(_totalPrice, price.Total, PriceComparer.Instance);
                Assert.Same(_packagePrice, price.Package);
                Assert.Equal(_defaultColorPrice, price.Color);
            }
        }

        [Fact]
        public async Task Calculate_WithConfigurationId_WhenPackageNotIncluded()
        {
            //Arrange
            var vehiclePrice = new Price(10000, 10000 * 2);
            var optionsPrice = new Price(150, 150 * 2);
            var withoutPackagePrice = new Price(10150, 10150 * 2);
            var withPackagePrice = new Price(10150, 10150 * 2);
            var configurationPackageSavings = Price.Empty;
            var totalPrice = new Price(10150, 10150 * 2);

            _packagePricingService.Setup(s => s.Calculate(_configurationOptionOffers, null, _date))
                .ReturnsAsync(PackagePrice.Empty);

            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<Configuration>(_configuration.Id))
                    .InSequence()
                    .ReturnsAsync(_configuration);

                _repository.Setup(r => r.SingleOrDefault(It.Is<VehicleOfferForVehicleSpecification>(s =>
                        s.ModelKey == _configuration.ModelKey && s.CatalogId == _configuration.CatalogId)))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer);

                //Act
                var price = await _service.Calculate(_configuration.Id, _date);

                //Assert
                Assert.NotNull(price);
                Assert.Equal(vehiclePrice, price.Vehicle, PriceComparer.Instance);
                Assert.Equal(_configurationPerOptionPrice, price.Options.PerOption);
                Assert.Equal(optionsPrice, price.Options.Total, PriceComparer.Instance);
                Assert.Equal(withoutPackagePrice, price.PackageSavings.Without, PriceComparer.Instance);
                Assert.Equal(withPackagePrice, price.PackageSavings.With, PriceComparer.Instance);
                Assert.Equal(configurationPackageSavings, price.PackageSavings.Savings, PriceComparer.Instance);
                Assert.Equal(totalPrice, price.Total, PriceComparer.Instance);
                Assert.Equal(_defaultColorPrice, price.Color);
            }
        }

        [Fact]
        public async Task Calculate_WithItems()
        {
            //Arrange
            _packagePricingService.Setup(s => s.Calculate(_configurationOptionOffers, _package, _date))
                .ReturnsAsync(_packagePrice);

            //Act
            var price = await _service.Calculate(_vehicleOffer, _configuration.Options, _configuration.ColorId, _package, _date);

            //Assert
            Assert.NotNull(price);
            Assert.Equal(_vehiclePrice, price.Vehicle, PriceComparer.Instance);
            Assert.Equal(_configurationPerOptionPrice, price.Options.PerOption);
            Assert.Equal(_optionsPrice, price.Options.Total, PriceComparer.Instance);
            Assert.Equal(_withoutPackagePrice, price.PackageSavings.Without, PriceComparer.Instance);
            Assert.Equal(_withPackagePrice, price.PackageSavings.With, PriceComparer.Instance);
            Assert.Equal(_configurationPackageSavings, price.PackageSavings.Savings, PriceComparer.Instance);
            Assert.Equal(_totalPrice, price.Total, PriceComparer.Instance);
            Assert.Same(_packagePrice, price.Package);
            Assert.Equal(_defaultColorPrice, price.Color);
        }

        [Fact]
        public async Task Calculate_WithItems_WhenPackageNotIncluded()
        {
            //Arrange
            var vehiclePrice = new Price(10000, 10000 * 2);
            var optionsPrice = new Price(150, 150 * 2);
            var withoutPackagePrice = new Price(10150, 10150 * 2);
            var withPackagePrice = new Price(10150, 10150 * 2);
            var configurationPackageSavings = Price.Empty;
            var totalPrice = new Price(10150, 10150 * 2);

            _packagePricingService.Setup(s => s.Calculate(_configurationOptionOffers, null, _date))
                .ReturnsAsync(PackagePrice.Empty);

            //Act
            var price = await _service.Calculate(_vehicleOffer, _configuration.Options, _configuration.ColorId, null, _date);

            //Assert
            Assert.Equal(vehiclePrice, price.Vehicle, PriceComparer.Instance);
            Assert.Equal(_configurationPerOptionPrice, price.Options.PerOption);
            Assert.Equal(optionsPrice, price.Options.Total, PriceComparer.Instance);
            Assert.Equal(withoutPackagePrice, price.PackageSavings.Without, PriceComparer.Instance);
            Assert.Equal(withPackagePrice, price.PackageSavings.With, PriceComparer.Instance);
            Assert.Equal(configurationPackageSavings, price.PackageSavings.Savings, PriceComparer.Instance);
            Assert.Equal(totalPrice, price.Total, PriceComparer.Instance);
            Assert.Same(PackagePrice.Empty, price.Package);
            Assert.Equal(_defaultColorPrice, price.Color);
        }

        [Fact]
        public Task Calculate_WithItems_GivenVehicleOffer_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("vehicleOffer", () =>
                _service.Calculate(null, _configuration.Options, _configuration.ColorId, null, _date));
        }

        [Fact]
        public Task Calculate_WithItems_GivenOptions_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("options", () =>
                _service.Calculate(_vehicleOffer, null, _configuration.ColorId, null, _date));
        }

        [Fact]
        public Task Calculate_WithItems_GivenColorId_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("colorId", () =>
                _service.Calculate(_vehicleOffer, _configuration.Options, null, null, _date));
        }

        private static Configuration CreateConfiguration(VehicleOffer vehicleOffer, ColorId colorId)
        {
            var configuration = new Configuration(vehicleOffer.CatalogId, vehicleOffer.ModelKey, colorId);

            configuration.IncludeOptions(
                new PrNumberSet(vehicleOffer.Options.Select(o => o.PrNumber).Take(5)));

            return configuration;
        }

        private static VehicleOffer CreateVehicleOffer(Price vehiclePrice, ColorId defaultColorId)
        {
            var vehicleOffer = new VehicleOffer(ModelKey.Parse("000000"), 1)
            {
                Price = vehiclePrice.Base
            };

            vehicleOffer.AddColorOffer(defaultColorId, 0);
            vehicleOffer.SetDefaultColor(defaultColorId);
            vehicleOffer.Publish();

            Enumerable.Range(1, 6)
                .ToList()
                .ForEach(n => vehicleOffer.AddOptionOffer(PrNumber.Parse($"PR{n}"), Availability.Purchasable(n * 10)));

            return vehicleOffer;
        }
    }

    public class PriceComparer : IEqualityComparer<Price>
    {
        public static readonly PriceComparer Instance = new PriceComparer();

        public bool Equals(Price x, Price y)
        {
            return ReferenceEquals(x, y) || x != null && y != null && x.Base == y.Base && x.Retail == y.Retail;
        }

        public int GetHashCode(Price obj)
        {
            throw new NotSupportedException();
        }
    }
}