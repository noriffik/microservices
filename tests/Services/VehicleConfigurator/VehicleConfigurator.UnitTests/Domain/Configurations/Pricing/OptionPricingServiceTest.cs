﻿using AutoFixture;
using Moq;
using NexCore.CurrencyExchange.Abstract;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Pricing;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations.Pricing
{
    public class OptionPricingServiceTest
    {
        private readonly OptionPricingService _service;
        private readonly Fixture _fixture;
        private readonly DateTime _date;

        public OptionPricingServiceTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            _date = _fixture.Create<DateTime>();

            var exchangeService = new Mock<ICurrencyExchangeService>();
            exchangeService.Setup(s => s.GetRate(It.IsAny<Currency>(), It.IsAny<Currency>(), _date))
                .ReturnsAsync(2);

            _service = new OptionPricingService(exchangeService.Object);
        }

        [Fact]
        public async Task CreatePrice_BasePrice()
        {
            //Arrange
            const decimal number = 10;

            //Act
            var result = await _service.CreatePrice(number, _date);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(10, result.Base);
            Assert.Equal(20, result.Retail);
        }

        [Fact]
        public async Task CreatePrice_WhenBasePrices_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("basePrices", () => _service.CreatePrice(null, _date));
        }

        [Fact]
        public async Task CreatePrice()
        {
            //Arrange
            var basePrices = PrNumberSet.Parse("000 001").Values
                .ToDictionary(k => k, v => _fixture.Create<decimal>());

            var expected = basePrices.Select(e => new { PrNumber = e.Key, Price = new Price(e.Value, e.Value * 2) })
                .ToDictionary(k => k.PrNumber, v => v.Price);

            //Act
            var result = await _service.CreatePrice(basePrices, _date);

            //Act
            Assert.Equal(expected, result);
        }

        [Fact]
        public async Task Calculate()
        {
            //Arrange
            var optionOffers = Enumerable.Range(1, 2)
                .Select(n => _fixture.Construct<OptionOffer>(new { availability = Availability.Purchasable(n * 10) }))
                .ToList();

            var basePrices = optionOffers.ToDictionary(k => k.PrNumber, v => v.Availability.Price);

            var perOptionPrice = optionOffers
                .Select(e => new { e.PrNumber, Price = new Price(e.Availability.Price, e.Availability.Price) })
                .ToDictionary(k => k.PrNumber, v => v.Price);

            var pricingService = new Mock<IOptionPricingService>();

            pricingService.Setup(s => s.CreatePrice(basePrices, _date))
                .ReturnsAsync(perOptionPrice);

            //Act
            var price = await _service.Calculate(optionOffers, _date);

            //Assert
            Assert.NotNull(price);
            Assert.Equal(perOptionPrice.Values.Aggregate((a, b) => a + b), price.Total);
            Assert.Equal(perOptionPrice, price.PerOption);
        }
    }
}
