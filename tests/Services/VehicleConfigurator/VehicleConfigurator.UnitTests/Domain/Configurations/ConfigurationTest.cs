﻿using AutoFixture;
using NexCore.Common;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations
{
    public class ConfigurationTest
    {
        private readonly Fixture _fixture;

        private readonly DateTime _now = new DateTime(2012, 1, 23);
        private readonly Package _package;
        private readonly Configuration _configuration;

        public ConfigurationTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            using (new SystemTimeContext(_now))
            {
                _configuration = _fixture.Create<Configuration>();
                _package = _fixture.Construct<Package>(
                    PackageId.Next(_configuration.ModelKey.ModelId, _fixture.Create<PackageCode>()));
            }
        }

        [Fact]
        public void Ctor()
        {
            //Arrange
            var catalogId = _fixture.Create<int>();
            var modelKey = _fixture.Create<ModelKey>();
            var colorId = _fixture.Create<ColorId>();

            using (var time = new SystemTimeContext(_now))
            {
                //Act
                var result = new Configuration(catalogId, modelKey, colorId);

                //Assert
                Assert.Equal(catalogId, result.CatalogId);
                Assert.Equal(modelKey, result.ModelKey);
                Assert.Equal(time.Now, result.CreatedAt);
                Assert.Equal(time.Now, result.UpdatedAt);
                Assert.Equal(ConfigurationStatus.Draft, result.Status);
                Assert.NotNull(result.Options);
            }
        }

        [Fact]
        public void Ctor_GivenModelKey_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelKey", () => new Configuration(_fixture.Create<int>(), null, _fixture.Create<ColorId>()));
        }

        [Fact]
        public void Ctor_GivenColor_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("colorId", () => new Configuration(_fixture.Create<int>(), _fixture.Create<ModelKey>(), null));
        }

        [Fact]
        public void ChangeStatus_AsCompleted()
        {
            using (var time = new SystemTimeContext(_now))
            {
                //Act
                _configuration.ChangeStatus(ConfigurationStatus.Completed);

                //Assert
                Assert.Equal(ConfigurationStatus.Completed, _configuration.Status);
                Assert.Equal(time.Now, _configuration.UpdatedAt);
            }
        }

        [Fact]
        public void ChangeStatus_AsDraft()
        {
            //Arrange
            using (new SystemTimeContext(_now.AddDays(1)))
            {
                _configuration.ChangeStatus(ConfigurationStatus.Completed);
            }

            using (var time = new SystemTimeContext(_configuration.CreatedAt.AddDays(1)))
            {
                //Act
                _configuration.ChangeStatus(ConfigurationStatus.Draft);

                //Assert
                Assert.Equal(ConfigurationStatus.Draft, _configuration.Status);
                Assert.Equal(time.Now.AddDays(-1), _configuration.CreatedAt);
                Assert.Equal(time.Now, _configuration.UpdatedAt);
            }
        }

        [Fact]
        public void ChangeStatus_AsSameStatus_DoNothing()
        {
            //Act
            using (new SystemTimeContext(_now.AddDays(1)))
            {
                _configuration.ChangeStatus(ConfigurationStatus.Draft);
            }

            //Assert
            Assert.Equal(ConfigurationStatus.Draft, _configuration.Status);
            Assert.Equal(_now, _configuration.CreatedAt);
            Assert.Equal(_now, _configuration.UpdatedAt);
        }

        [Fact]
        public void IncludeOption()
        {
            //Arrange
            var prNumber = _fixture.Create<PrNumber>();

            using (var time = new SystemTimeContext(_now.AddDays(1)))
            {
                //Act
                _configuration.IncludeOption(prNumber);

                //Assert
                Assert.True(_configuration.Options.Has(prNumber));
                Assert.Equal(time.Now, _configuration.UpdatedAt);
            }
        }

        [Fact]
        public void IncludeOption_Preserves_PreviouslyIncluded_Options()
        {
            //Arrange
            var prNumbers = _fixture.CreateMany<PrNumber>(3).ToList();
            var toInclude = prNumbers.TakeLast(1).Single();

            //Populate with previously included
            prNumbers.SkipLast(1)
                .ToList()
                .ForEach(_configuration.IncludeOption);

            //Act
            _configuration.IncludeOption(toInclude);

            //Assert
            Assert.Equal(new PrNumberSet(prNumbers), _configuration.Options);
        }

        [Fact]
        public void IncludeOption_GivenPrNumber_IsAlreadyIncluded_DoesNothing()
        {
            //Arrange
            var prNumber = _fixture.Create<PrNumber>();

            //Include option
            using (new SystemTimeContext(_now))
            {
                _configuration.IncludeOption(prNumber);
            }

            //Act
            using (new SystemTimeContext(_now.AddDays(1)))
            {
                _configuration.IncludeOption(prNumber);
            }

            //Assert
            Assert.Equal(_now, _configuration.UpdatedAt);
        }

        [Fact]
        public void IncludeOption_GivenPrNumber_IsNotIncluded_DoNothing()
        {
            //Arrange
            var prNumber = _fixture.Create<PrNumber>();

            //Act
            using (new SystemTimeContext(_now.AddDays(1)))
            {
                _configuration.ExcludeOption(prNumber);
            }

            //Assert
            Assert.Equal(_now, _configuration.UpdatedAt);
        }

        [Fact]
        public void IncludeOption_GivenPrNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumber", () => _configuration.IncludeOption(null));
        }

        [Fact]
        public void IncludeOptions()
        {
            //Arrange
            var prNumbers = _fixture.CreateMany<PrNumber>(6).ToList();
            var toInclude = new PrNumberSet(prNumbers.TakeLast(3));
            var expected = new PrNumberSet(prNumbers);

            //Populate with previously included
            prNumbers.SkipLast(3)
                .ToList()
                .ForEach(_configuration.IncludeOption);

            using (var time = new SystemTimeContext(_now.AddDays(1)))
            {
                //Act
                _configuration.IncludeOptions(toInclude);

                //Assert
                Assert.Equal(expected, _configuration.Options);
                Assert.Equal(time.Now, _configuration.UpdatedAt);
            }
        }

        [Fact]
        public void IncludeOptions_GivenPrNumbers_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumbers", () => _configuration.IncludeOptions(null));
        }

        [Fact]
        public void ExcludeOption()
        {
            //Arrange
            var prNumber = _fixture.Create<PrNumber>();
            _configuration.IncludeOption(prNumber);

            //Act
            _configuration.ExcludeOption(prNumber);

            //Assert
            Assert.DoesNotContain(prNumber, _configuration.Options.Values);
        }

        [Fact]
        public void ExcludeOption_GivenPrNumber_IsNotIncluded_DoesNothing()
        {
            //Arrange
            var prNumber = _fixture.Create<PrNumber>();

            using (new SystemTimeContext(_now.AddDays(1)))
            {
                //Act
                _configuration.ExcludeOption(prNumber);

                //Assert
                Assert.Equal(_now, _configuration.UpdatedAt);
            }
        }

        [Fact]
        public void ExcludeOption_GivenPrNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumber", () => _configuration.ExcludeOption(null));
        }

        [Fact]
        public void IncludePackage()
        {
            using (var time = new SystemTimeContext(_now.AddDays(1)))
            {
                //Act
                _configuration.IncludePackage(_package.Id);

                //Assert
                Assert.Equal(_package.Id, _configuration.PackageId);
                Assert.Equal(time.Now, _configuration.UpdatedAt);
            }
        }

        [Fact]
        public void IncludePackage_WithPackage_WhenPackage_AlreadyIncluded_DoesNothing()
        {
            //Arrange
            using (new SystemTimeContext(_now))
            {
                _configuration.IncludePackage(_package.Id);
            }

            //Act
            using (new SystemTimeContext(_now.AddDays(1)))
            {
                _configuration.IncludePackage(_package.Id);
            }

            //Assert
            Assert.Equal(_package.Id, _configuration.PackageId);
            Assert.Equal(_now, _configuration.UpdatedAt);
        }

        [Fact]
        public void GetIsPackageIncluded_WhenIs_ReturnsTrue()
        {
            //Arrange
            _configuration.IncludePackage(_package.Id);

            //Assert
            Assert.True(_configuration.IsPackageIncluded);
        }

        [Fact]
        public void GetIsPackageIncluded_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_configuration.IsPackageIncluded);
        }

        [Fact]
        public void ExcludePackage()
        {
            //Arrange
            using (new SystemTimeContext(_now))
            {
                _configuration.IncludePackage(_package.Id);
            }

            using (var time = new SystemTimeContext(_now.AddDays(1)))
            {
                //Act
                _configuration.ExcludePackage();

                //Assert
                Assert.Null(_configuration.PackageId);
                Assert.Equal(time.Now, _configuration.UpdatedAt);
            }
        }

        [Fact]
        public void ExcludePackage_WhenPackage_IsNotIncluded_DoesNothing()
        {
            using (new SystemTimeContext(_now.AddDays(1)))
            {
                //Act
                _configuration.ExcludePackage();

                //Assert
                Assert.Null(_configuration.PackageId);
                Assert.Equal(_now, _configuration.UpdatedAt);
            }
        }

        [Fact]
        public void MakeCopy()
        {
            //Arrange
            _configuration.IncludeOptions(_fixture.Create<PrNumberSet>());
            _configuration.ChangeStatus(ConfigurationStatus.Completed);

            //Act
            var copy = _configuration.MakeCopy();

            //Assert
            Assert.Equal(_configuration.CatalogId, copy.CatalogId);
            Assert.Equal(_configuration.ModelKey, copy.ModelKey);
            Assert.Equal(_configuration.Options, copy.Options);
            Assert.Equal(_configuration.PackageId, copy.PackageId);
            Assert.Equal(ConfigurationStatus.Draft, copy.Status);
        }

        [Fact]
        public void ChangeColor()
        {
            //Arrange
            var colorId = _fixture.Create<ColorId>();
            var updatedDate = _now.AddDays(1);

            //Act
            using (new SystemTimeContext(updatedDate))
            {
                _configuration.ChangeColor(colorId);
            }

            //Assert
            Assert.Equal(colorId, _configuration.ColorId);
            Assert.Equal(updatedDate, _configuration.UpdatedAt);
        }

        [Fact]
        public void ChangeColor_WhenGivenColorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("colorId", () => _configuration.ChangeColor(null));
        }

        [Fact]
        public void ChangeColor_WhenGivenColor_IsSame_DoNothing()
        {
            //Act
            using (new SystemTimeContext(_now.AddDays(1)))
            {
                _configuration.ChangeColor(_configuration.ColorId);
            }

            //Assert
            Assert.Equal(_now, _configuration.UpdatedAt);
        }

        [Fact]
        public void ChangeRelevance()
        {
            //Arrange
            var updatedDate = _now.AddDays(1);

            //Act
            using (new SystemTimeContext(updatedDate))
            {
                _configuration.ChangeRelevance(Relevance.Irrelevant);
            }

            //Assert
            Assert.Equal(Relevance.Irrelevant, _configuration.Relevance);
            Assert.Equal(updatedDate, _configuration.UpdatedAt);
        }

        [Fact]
        public void ChangeRelevance_WhenGivenRelevance_IsSame_DoNothing()
        {
            //Act
            using (new SystemTimeContext(_now.AddDays(1)))
            {
                _configuration.ChangeRelevance(Relevance.Relevant);
            }

            //Assert
            Assert.Equal(_now, _configuration.UpdatedAt);
        }

        [Fact]
        public void EnforceRestriction()
        {
            //Act
            _configuration.EnforceRestriction();

            //Assert
            Assert.True(_configuration.IsRestricted);
        }

        [Fact]
        public void LiftRestriction()
        {
            //Act
            _configuration.LiftRestriction();

            //Assert
            Assert.False(_configuration.IsRestricted);
        }

        [Theory]
        [InlineData(true, true)]
        [InlineData(false, false)]
        public void ToggleRestriction(bool isEnforced, bool expected)
        {
            //Act
            _configuration.ToggleRestriction(isEnforced);

            //Assert
            Assert.Equal(expected, _configuration.IsRestricted);
        }
    }
}
