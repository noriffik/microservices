﻿using AutoFixture;
using Moq;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations
{
    public class VehicleOfferMarkedAsIrrelevantHandlerTest
    {
        //Dependency
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly IReadOnlyList<Configuration> _configurations;

        //Event and handler
        private readonly VehicleOfferUnpublishedEvent _unpublishedEvent;
        private readonly VehicleOfferMarkedAsIrrelevantEvent _markedAsIrrelevantEvent;
        private readonly MarkConfigurationAsIrrelevantWhenVehicleOfferIsUnavailableHandler _handler;

        public VehicleOfferMarkedAsIrrelevantHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependency
            _repository = new Mock<IEntityRepository>();
            var work = new Mock<IUnitOfWork>();
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entities
            var vehicleOffer = fixture.Create<VehicleOffer>();
            _configurations = fixture.ConstructMany<Configuration>(3, new
            {
                catalogId = vehicleOffer.CatalogId,
                modelKey = vehicleOffer.ModelKey
            }).ToList();

            //Setup repository
            _repository.Setup(r => r.Require<VehicleOffer>(vehicleOffer.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(vehicleOffer)
                .Verifiable();
            _repository.Setup(r => r.Find(It.Is<ByVehicleOfferSpecification>(s =>
                    s.CatalogId == vehicleOffer.CatalogId && s.ModelKey == vehicleOffer.ModelKey), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_configurations)
                .Verifiable();

            //Event and handler
            _unpublishedEvent = new VehicleOfferUnpublishedEvent(vehicleOffer.Id);
            _markedAsIrrelevantEvent = new VehicleOfferMarkedAsIrrelevantEvent(vehicleOffer.Id);
            _handler = new MarkConfigurationAsIrrelevantWhenVehicleOfferIsUnavailableHandler(work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(MarkConfigurationAsIrrelevantWhenVehicleOfferIsUnavailableHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_Unpublished_GivenNotification_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("notification", () =>
                _handler.Handle(null as VehicleOfferUnpublishedEvent, CancellationToken.None));
        }

        [Fact]
        public Task Handle_MarkedAsIrrelevant_GivenNotification_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("notification", () =>
                _handler.Handle(null as VehicleOfferMarkedAsIrrelevantEvent, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_Unpublished()
        {
            //Act
            await _handler.Handle(_unpublishedEvent, CancellationToken.None);

            //Assert
            Assert.All(_configurations, configuration => Assert.Equal(Relevance.Irrelevant, configuration.Relevance));
            _repository.Verify();
        }

        [Fact]
        public async Task Handle_MarkedAsIrrelevant()
        {
            //Act
            await _handler.Handle(_markedAsIrrelevantEvent, CancellationToken.None);

            //Assert
            Assert.All(_configurations, configuration => Assert.Equal(Relevance.Irrelevant, configuration.Relevance));
            _repository.Verify();
        }
    }
}
