﻿using AutoFixture;
using Newtonsoft.Json;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations
{
    public class ConfigurationPriceSerializationTest
    {
        class DecimalConverter : JsonConverter<decimal>
        {
            public override void WriteJson(JsonWriter writer, decimal value, JsonSerializer serializer)
            {
                writer.WriteValue(value.ToString());
            }

            public override decimal ReadJson(JsonReader reader, Type objectType, decimal existingValue, bool hasExistingValue,
                JsonSerializer serializer)
            {
                var s = (string) reader.Value;

                return decimal.Parse(s);
            }
        }

        private readonly ConfigurationPrice _price;

        public ConfigurationPriceSerializationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _price = fixture.Create<ConfigurationPrice>();
        }

        [Fact]
        public void Serialize()
        {
            //Act
            var json = JsonConvert.SerializeObject(_price, new DecimalConverter());

            var deserialized = JsonConvert.DeserializeObject<ConfigurationPrice>(json, new DecimalConverter());
            
            //Assert
            Assert.NotNull(json);
            Assert.Equal(_price, deserialized, PropertyComparer<ConfigurationPrice>.Instance);
        }

        [Fact]
        public void PackagePriceSerialize()
        {
            //Act
            var json = JsonConvert.SerializeObject(_price.Package, new DecimalConverter());

            var deserialized = JsonConvert.DeserializeObject<PackagePrice>(json, new DecimalConverter());
            
            //Assert
            Assert.NotNull(json);
            Assert.Equal(_price.Package, deserialized, PropertyComparer<PackagePrice>.Instance);
        }

        [Fact]
        public void OptionsPriceSerialize()
        {
            //Act
            var json = JsonConvert.SerializeObject(_price.Options, new DecimalConverter());

            var deserialized = JsonConvert.DeserializeObject<OptionsPrice>(json, new DecimalConverter());
            
            //Assert
            Assert.NotNull(json);
            Assert.Equal(_price.Options, deserialized, PropertyComparer<OptionsPrice>.Instance);
        }
    }
}
