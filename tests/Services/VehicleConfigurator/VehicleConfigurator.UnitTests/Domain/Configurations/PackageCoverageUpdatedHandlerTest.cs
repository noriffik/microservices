﻿using AutoFixture;
using Moq;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations
{
    public class PackageCoverageUpdatedHandlerTest
    {
        //Dependency
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Package _package;
        private readonly IReadOnlyList<Configuration> _relevantConfigurations;
        private readonly IReadOnlyList<Configuration> _irrelevantConfigurations;

        //Event and handler
        private readonly PackageCoverageUpdatedEvent _event;
        private readonly PackageCoverageUpdatedHandler _handler;

        public PackageCoverageUpdatedHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _repository = new Mock<IEntityRepository>();
            var work = new Mock<IUnitOfWork>();
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entity
            _package = fixture.Create<Package>();
            _irrelevantConfigurations = fixture.CreateMany<Configuration>(3).ToList();
            _relevantConfigurations = _package.ModelKeySet.Values
                .Select(modelKey => fixture.Construct<Configuration>(new { modelKey }))
                .ToList();

            //Event and handler
            _event = new PackageCoverageUpdatedEvent(_package.Id);
            _handler = new PackageCoverageUpdatedHandler(work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(PackageCoverageUpdatedHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenNotification_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("notification", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            _repository.Setup(r => r.Require<Package, PackageId>(_package.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(_package)
                .Verifiable();
            _repository.Setup(r => r.Find(It.Is<ByPackageIdSpecification>(s => s.PackageId == _package.Id), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_relevantConfigurations.Concat(_irrelevantConfigurations))
                .Verifiable();

            //Act
            await _handler.Handle(_event, CancellationToken.None);

            //Assert
            Assert.All(_relevantConfigurations, configuration => Assert.Equal(Relevance.Relevant, configuration.Relevance));
            Assert.All(_irrelevantConfigurations, configuration => Assert.Equal(Relevance.Irrelevant, configuration.Relevance));
            _repository.Verify();
        }
    }
}
