﻿using AutoFixture;
using Moq;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations
{
    public class PackageApplicabilityUpdatedHandlerTest
    {
        //Dependencies
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly Package _package;

        private readonly IReadOnlyCollection<Configuration> _relevantConfigurations;
        private readonly IReadOnlyCollection<Configuration> _irrelevantConfigurations;

        //Event and handler
        private readonly PackageApplicabilityUpdatedEvent _event;
        private readonly PackageApplicabilityUpdatedHandler _handler;

        public PackageApplicabilityUpdatedHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            var work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entities
            _package = fixture.Create<Package>();

            _relevantConfigurations = fixture.CreateMany<Configuration>().ToList();

            foreach (var configuration in _relevantConfigurations)
            {
                var prNumbers = new PrNumberSet(_package.Applicability.Select(s => s.PrNumbers.Values.First()));
                configuration.IncludeOptions(prNumbers);
                configuration.IncludePackage(_package.Id);
            }

            _irrelevantConfigurations = fixture.CreateMany<Configuration>().ToList();

            foreach (var configuration in _irrelevantConfigurations)
                configuration.IncludePackage(_package.Id);

            //Event and handler
            _event = new PackageApplicabilityUpdatedEvent(_package.Id);
            _handler = new PackageApplicabilityUpdatedHandler(work.Object);
        }
        [Fact]

        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(PackageApplicabilityUpdatedHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenEvent_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("notification", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            _repository.Setup(r => r.Find(It.Is<ByPackageIdSpecification>(s => s.PackageId == _package.Id), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_relevantConfigurations.Concat(_irrelevantConfigurations))
                .Verifiable();
            _repository.Setup(r => r.Require<Package, PackageId>(_package.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(_package)
                .Verifiable();

            //Act
            await _handler.Handle(_event, CancellationToken.None);

            //Assert
            Assert.All(_relevantConfigurations, configuration => Assert.Equal(Relevance.Relevant, configuration.Relevance));
            Assert.All(_irrelevantConfigurations, configuration => Assert.Equal(Relevance.Irrelevant, configuration.Relevance));
            _repository.Verify();
        }
    }
}
