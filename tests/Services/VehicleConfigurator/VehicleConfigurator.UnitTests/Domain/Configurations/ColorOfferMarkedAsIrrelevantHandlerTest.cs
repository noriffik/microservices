﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations
{
    public class ColorOfferMarkedAsIrrelevantHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly VehicleOffer _vehicleOffer;
        private readonly ColorId _colorId;
        private readonly IEnumerable<Configuration> _configurations;

        //Event and handler
        private readonly ColorOfferMarkedAsIrrelevantEvent _event;
        private readonly ColorOfferMarkedAsIrrelevantHandler _handler;

        public ColorOfferMarkedAsIrrelevantHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entities
            _vehicleOffer = fixture.Create<VehicleOffer>();
            _colorId = fixture.Create<ColorId>();
            _configurations = fixture.CreateMany<Configuration>(3);

            //Event and handler
            _event = fixture.Construct<ColorOfferMarkedAsIrrelevantEvent>(new { colorId = _colorId });
            _handler = new ColorOfferMarkedAsIrrelevantHandler(_work.Object);

        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ColorOfferMarkedAsIrrelevantHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenEvent_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("notification", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<VehicleOffer>(_event.VehicleOfferId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer);
                _repository.Setup(r => r.Find(It.Is<ByColorOfferSpecification>(s =>
                        s.CatalogId == _vehicleOffer.CatalogId &&
                        s.ModelKey == _vehicleOffer.ModelKey &&
                        s.ColorId == _colorId), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_configurations);

                //Act
                await _handler.Handle(_event, CancellationToken.None);
            }

            //Assert
            Assert.All(_configurations, c => Assert.Equal(Relevance.Irrelevant, c.Relevance));
        }
    }
}