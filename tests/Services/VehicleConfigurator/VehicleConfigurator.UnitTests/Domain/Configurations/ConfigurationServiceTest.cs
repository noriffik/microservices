﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Rules;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Extensions.Domain;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations
{
    public class ConfigurationServiceTest
    {
        private readonly Fixture _fixture = new Fixture();

        //Entity
        private readonly Configuration _configuration;
        private readonly Package _package;

        //Dependencies
        private readonly PrNumber _prNumber;
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _entityRepository;
        private readonly Mock<IConfigurationValidationService> _validator;

        //Service
        private readonly ConfigurationService _service;

        public ConfigurationServiceTest()
        {
            _fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _configuration = _fixture.Create<Configuration>();
            _package = _fixture.Create<Package>();
            _package.ChangeStatus(PackageStatus.Published);

            //Dependencies
            _prNumber = _fixture.Create<PrNumber>();
            _validator = new Mock<IConfigurationValidationService>();
            _work = new Mock<IUnitOfWork>();
            _entityRepository = new Mock<IEntityRepository>();

            //Setup mock
            _work.Setup(w => w.EntityRepository).Returns(_entityRepository.Object);
            _entityRepository.Setup(r => r.Require<Configuration>(_configuration.Id))
                .ReturnsAsync(_configuration);

            //Service
            _service = new ConfigurationService(_work.Object)
            {
                Validator = _validator.Object
            };
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ConfigurationService)).HasNullGuard();
        }

        [Fact]
        public void Validator()
        {
            Assert.Injection
                .OfProperty(_service, nameof(_service.Validator))
                .HasDefault()
                .HasNullGuard()
                .HasDefault();
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var vehicleOffer = _fixture.Construct<VehicleOffer>(new
            {
                modelKey = _configuration.ModelKey,
                catalogId = _configuration.CatalogId,
            });
            vehicleOffer.AddColorOffer(_configuration.ColorId, _fixture.Create<decimal>());
            vehicleOffer.SetDefaultColor(_configuration.ColorId);
            vehicleOffer.Publish();

            using (Sequence.Create())
            {
                _entityRepository.Setup(r => r.HasRequired<Catalog>(_configuration.CatalogId))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _entityRepository.Setup(r => r.Require(It.Is<PublishedVehicleOfferSpecification>(s =>
                        s.CatalogId == _configuration.CatalogId && s.ModelKey == _configuration.ModelKey)))
                    .InSequence()
                    .ReturnsAsync(vehicleOffer);

                _entityRepository.Setup(r => r.Add(It.Is<Configuration>(c =>
                        c.CatalogId == _configuration.CatalogId &&
                        c.ModelKey == _configuration.ModelKey &&
                        c.ColorId == _configuration.ColorId)))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var configuration = await _service.Add(_configuration.CatalogId, _configuration.ModelKey);

                //Assert
                Assert.Equal(_configuration.CatalogId, configuration.CatalogId);
                Assert.Equal(_configuration.ModelKey, configuration.ModelKey);
                Assert.Equal(_configuration.ColorId, configuration.ColorId);
            }
        }

        [Fact]
        public Task Add_GivenModelKey_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("modelKey", () =>
                _service.Add(_fixture.Create<int>(), null));
        }

        [Fact]
        public async Task ChangeStatus()
        {
            //Act
            await _service.ChangeStatus(_configuration.Id, ConfigurationStatus.Completed);

            //Assert
            Assert.Equal(_configuration.Status, ConfigurationStatus.Completed);
        }

        [Fact]
        public async Task IncludeOption()
        {
            using (Sequence.Create())
            {
                //Arrange
                var prNumberToInclude = _fixture.Create<PrNumber>();
                
                _validator.Setup(r => r.ValidateOptionInclusion(_configuration, prNumberToInclude))
                    .InSequence().Returns(Task.CompletedTask);

                //Act
                await _service.IncludeOption(_configuration.Id, prNumberToInclude);
            }
        }

        [Fact]
        public async Task IncludePackage()
        {
            //Arrange
            //Include package's options
            _configuration.IncludeOptions(_package.ApplicableWith());

            using (Sequence.Create())
            {
                _entityRepository.Setup(r => r.Require<Configuration>(_configuration.Id))
                    .InSequence()
                    .ReturnsAsync(_configuration);

                _entityRepository.Setup(r => r.Require<Package, PackageId>(
                        It.Is<CompatiblePackageCodeSpecification>(
                            s => s.Code == _package.Id.PackageCode && s.Configuration == _configuration)))
                    .InSequence()
                    .ReturnsAsync(_package);

                //Act
                await _service.IncludePackage(_configuration.Id, _package.Id.PackageCode);
            }

            //Assert
            Assert.Equal(_package.Id, _configuration.PackageId);
        }

        [Fact]
        public async Task ExcludePackage()
        {
            //Arrange
            _configuration.IncludeOptions(_package.ApplicableWith());
            _configuration.IncludePackage(_package.Id);

            //Act
            await _service.ExcludePackage(_configuration.Id);

            //Assert
            _entityRepository.Verify(r => r.Require<Configuration>(_configuration.Id),
                Times.Once);
            Assert.Null(_configuration.PackageId);
        }

        [Fact]
        public async Task ExcludeOption()
        {
            using (Sequence.Create())
            {
                //Arrange
                _validator.Setup(r => r.VerifyThatOptionIsNotInPackage(_prNumber,_configuration))
                    .InSequence().Returns(Task.CompletedTask);

                _validator.Setup(r => r.VerifyThatOptionIsNotRequired(_configuration, _prNumber))
                    .InSequence().Returns(Task.CompletedTask);

                //Act
                await _service.ExcludeOption(_configuration.Id, _prNumber);
            }
        }

        [Fact]
        public Task ExcludeOption_GivenPrNumber_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("prNumber", () => _service.ExcludeOption(_configuration.Id, null));
        }

        [Fact]
        public async Task Copy()
        {
            //Arrange
            var copy = _fixture.Create<Configuration>();
            var configurationId = _fixture.Create<int>();
            var configuration = new Mock<Configuration>(configurationId, _fixture.Create<ModelKey>(), _fixture.Create<ColorId>());
            configuration.Setup(c => c.MakeCopy())
                .Returns(copy);
            _entityRepository.Setup(r => r.Require<Configuration>(configurationId))
                .ReturnsAsync(configuration.Object);

            //Act
            var result = await _service.Copy(configurationId);

            //Assert
            Assert.Equal(copy, result);
            _entityRepository.Verify(r => r.Require<Configuration>(configurationId), Times.Once);
            _entityRepository.Verify(r => r.Add(copy), Times.Once);
        }

        [Fact]
        public async Task Build()
        {
            //Arrange
            var modelYear = _fixture.Create<Year>();
            var specification = _fixture.Create<ConfigurationSpecification>();

            //Setup relevant catalog
            var catalog = _fixture.Construct<Catalog>(33);
            _entityRepository.Setup(r => r.Require(It.Is<RelevantCatalogPublishedForModelYearSpecification>(s => s.ModelYear == modelYear)))
                .ReturnsAsync(catalog);

            //Setup vehicle offer for option inclusion validation purposes
            var vehicleOffer = _fixture.Construct<VehicleOffer>(new { catalogId = catalog.Id });
            foreach (var prNumber in specification.PrNumbers.Values)
                vehicleOffer.AddOptionOffer(prNumber, _fixture.Create<Availability>());

            _entityRepository.Setup(r => r.Require(It.Is<PublishedVehicleOfferSpecification>(s =>
                    s.CatalogId == catalog.Id && s.ModelKey == specification.ModelKey)))
                .ReturnsAsync(vehicleOffer);

            //Setup defaultColor
            var colorId = _fixture.Create<ColorId>();
            vehicleOffer.AddColorOffer(colorId, 0);
            vehicleOffer.SetDefaultColor(colorId);
            vehicleOffer.Publish();

            //Act
            var configuration = await _service.Build(modelYear, specification);

            //Assert
            Assert.NotNull(configuration);
            Assert.Equal(catalog.Id, configuration.CatalogId);
            Assert.Equal(specification.ModelKey, configuration.ModelKey);
            Assert.Equal(specification.PrNumbers, configuration.Options);
            Assert.Equal(colorId, configuration.ColorId);
        }
        
        [Fact]
        public async Task Remove()
        {
            //Arrange
            var configuration = _fixture.Construct<Configuration>(123);

            _entityRepository.Setup(r => r.SingleOrDefault<Configuration>(configuration.Id))
                .ReturnsAsync(configuration);

            _entityRepository.Setup(r => r.Remove(configuration))
                .Returns(Task.CompletedTask)
                .Verifiable();

            //Act
            await _service.Remove(configuration.Id);

            //Assert
            _work.Verify();
        }

        [Fact]
        public async Task Remove_WhenConfiguration_IsNotFound_Exists()
        {
            //Act
            await _service.Remove(124);

            //Assert
            _work.Verify(s => s.EntityRepository.Remove(It.IsAny<Configuration>()), Times.Never);
        }

        [Fact]
        public async Task ChangeColor()
        {
            //Arrange
            var colorId = _fixture.Create<ColorId>();

            using (Sequence.Create())
            {
                _validator.Setup(r => r.ValidateColorAvailability(_configuration, colorId))
                    .InSequence().Returns(Task.CompletedTask);

                //Act
                await _service.ChangeColor(_configuration.Id, colorId);
            }

            //Assert
            Assert.Equal(colorId, _configuration.ColorId);
        }
    }
}
