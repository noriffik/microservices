﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Rules;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations
{
    public class OptionOfferRuleSetUpdatedHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IConfigurationValidator> _configurationValidator;

        //Entities
        private readonly VehicleOffer _vehicleOffer;
        private readonly PrNumber _prNumber;
        private readonly Configuration _configurationRelevant;
        private readonly Configuration _configurationIrrelevant;
        private readonly Configuration _configurationRelevantToIrrelevant;
        private readonly Configuration _configurationIrrelevantToRelevant;

        //Event and handler
        private readonly OptionOfferRuleSetUpdatedEvent _event;
        private readonly OptionOfferRuleSetUpdatedHandler _handler;

        public OptionOfferRuleSetUpdatedHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entities
            _vehicleOffer = fixture.Create<VehicleOffer>();
            _prNumber = fixture.Create<PrNumber>();

            _configurationRelevant = fixture.Create<Configuration>();
            _configurationRelevant.IncludeOptions(fixture.Create<PrNumberSet>());
            _configurationRelevant.ChangeRelevance(Relevance.Relevant);

            _configurationIrrelevant = fixture.Create<Configuration>();
            _configurationIrrelevant.IncludeOptions(fixture.Create<PrNumberSet>());
            _configurationIrrelevant.ChangeRelevance(Relevance.Irrelevant);

            _configurationRelevantToIrrelevant = fixture.Create<Configuration>();
            _configurationRelevantToIrrelevant.IncludeOptions(fixture.Create<PrNumberSet>());
            _configurationRelevantToIrrelevant.ChangeRelevance(Relevance.Relevant);

            _configurationIrrelevantToRelevant = fixture.Create<Configuration>();
            _configurationIrrelevantToRelevant.IncludeOptions(fixture.Create<PrNumberSet>());
            _configurationIrrelevantToRelevant.ChangeRelevance(Relevance.Irrelevant);

            _configurationValidator = new Mock<IConfigurationValidator>();

            //Event and handler
            _event = fixture.Construct<OptionOfferRuleSetUpdatedEvent>(new { prNumber = _prNumber });
            _handler = new OptionOfferRuleSetUpdatedHandler(_work.Object)
            {
                Validator = _configurationValidator.Object
            };
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(OptionOfferRuleSetUpdatedHandler)).HasNullGuard();
        }

        [Fact]
        public void Validator()
        {
            Assert.Injection
                .OfProperty(_handler, nameof(_handler.Validator))
                .HasDefault()
                .HasNullGuard()
                .HasDefault();
        }

        [Fact]
        public Task Handle_WhenGivenEvent_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("notification", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                var configurations = new List<Configuration>
                {
                    _configurationRelevant,
                    _configurationIrrelevant,
                    _configurationRelevantToIrrelevant,
                    _configurationIrrelevantToRelevant
                };

                _repository.Setup(r => r.Require<VehicleOffer>(_event.VehicleOfferId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer);
                _repository.Setup(r => r.Find(It.Is<ByVehicleOfferWithIncludedOptionSpecification>(s =>
                        s.CatalogId == _vehicleOffer.CatalogId &&
                        s.ModelKey == _vehicleOffer.ModelKey &&
                        s.PrNumberSet.Has(_prNumber)), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(configurations);

                _configurationValidator.Setup(r => r.IsValid(_configurationRelevant.Options, _vehicleOffer)).Returns(true);
                _configurationValidator.Setup(r => r.IsValid(_configurationIrrelevantToRelevant.Options, _vehicleOffer)).Returns(true);
                _configurationValidator.Setup(r => r.IsValid(_configurationIrrelevant.Options, _vehicleOffer)).Returns(false);
                _configurationValidator.Setup(r => r.IsValid(_configurationRelevantToIrrelevant.Options, _vehicleOffer)).Returns(false);

                //Act
                await _handler.Handle(_event, CancellationToken.None);
            }

            //Assert
            Assert.Equal(Relevance.Relevant, _configurationRelevant.Relevance);
            Assert.Equal(Relevance.Relevant, _configurationIrrelevantToRelevant.Relevance);
            Assert.Equal(Relevance.Irrelevant, _configurationIrrelevant.Relevance);
            Assert.Equal(Relevance.Irrelevant, _configurationRelevantToIrrelevant.Relevance);
        }
    }
}
