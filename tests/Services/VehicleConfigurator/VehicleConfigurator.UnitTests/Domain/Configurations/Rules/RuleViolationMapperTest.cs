﻿using AutoFixture;
using Moq;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Configurations.Rules;
using NexCore.VehicleConfigurator.Domain.Rules;
using NexCore.VehicleConfigurator.Domain.Rules.Expressions;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations.Rules
{
    public class RuleViolationMapperTest
    {
        private readonly Fixture _fixture;

        //Data
        private readonly string _message;
        private readonly PrNumberSetRule _rule;
        
        //Mapper
        private readonly RuleViolationMapper _mapper;

        public RuleViolationMapperTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            //Setup data
            _rule = new OnlyWithAll(_fixture.Create<PrNumberSet>(), null);
            _message = _fixture.Create<string>();

            //Setup message composer
            
            var messageComposer = new Mock<IRuleMessageComposer>();
            messageComposer.Setup(c => c.Compose(_rule.RuleName, _rule.PrNumbers.AsString))
                .Returns(_message);

            //Setup mapper
            _mapper = new RuleViolationMapper
            {
                MessageComposer = messageComposer.Object
            };
        }

        [Fact]
        public void Map()
        {
            //Arrange
            var prNumber = _fixture.Create<PrNumber>();
            var source = new Tuple<PrNumber, PrNumberSetRule>(prNumber, _rule);
            var expected = new RuleViolation
            {
                ForPrNumber = prNumber.Value,
                Message = _message,
                PrNumbers = _rule.PrNumbers.AsString,
                RuleName = _rule.RuleName,
                RuleString = _rule.ToString()
            };

            //Act
            var actual = _mapper.Map(source);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<RuleViolation>.Instance);
        }

        [Fact]
        public void Map_GivenSource_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("source", () => _mapper.Map(null));
        }

        [Fact]
        public void PropertyInjection()
        {
            Assert.Injection.OfProperty(new RuleViolationMapper(), nameof(RuleViolationMapper.MessageComposer))
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}