﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations.Rules;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations.Rules
{
    public class ConfigurationValidatorTest
    {
        private readonly Fixture _fixture = new Fixture();
        private const string RuleSet = "[VA: BBB+[ZA: CCC+[ZO: DDD EEE FFF+[]]]][ZA: ZZZ XXX+[]]";
        private const string RuleSet2 = "[ZA: XXX+[]]";
        private readonly PrNumberSet _configurationPrNumberSet = PrNumberSet.Parse("AAA EEE CCC ZZZ XXX");
        private readonly ConfigurationValidator _validator = new ConfigurationValidator();

        private readonly VehicleOffer _vehicleOffer;

        public ConfigurationValidatorTest()
        {
            _fixture.Customizations.Add(new ModelComponentBuilder());

            _vehicleOffer = _fixture.Create<VehicleOffer>();
            _vehicleOffer.AddOptionOffer(PrNumber.Parse("EEE"), Availability.Purchasable(2));
            _vehicleOffer.AddOptionOffer(PrNumber.Parse("CCC"), Availability.Purchasable(1)).AssignRules(RuleSet2);
            _vehicleOffer.AddOptionOffer(PrNumber.Parse("ZZZ"), Availability.Purchasable(2));
            _vehicleOffer.AddOptionOffer(PrNumber.Parse("XXX"), Availability.Purchasable(2));
            _vehicleOffer.AddOptionOffer(PrNumber.Parse("AAA"), Availability.Purchasable(2)).AssignRules(RuleSet);
        }

        [Fact]
        public void Validate()
        {
            //Arrange
            var configuration = _configurationPrNumberSet.Shrink(PrNumber.Parse("EEE"));

            //Act
            _validator.Validate(configuration, _vehicleOffer);
        }

        [Fact]
        public void Validate_WhenOptionsRequiredByRule_AlreadyIncluded()
        {
            //Arrange
            var vehicleOffer = _fixture.Create<VehicleOffer>();
            var prNumberToInclude = PrNumber.Parse("AAA");
            var ruleSet = "[ZA: BBB CCC+[]]";

            vehicleOffer.AddOptionOffer(PrNumber.Parse("BBB"), Availability.Included(ReasonType.NationalStandard), Inclusion.Automatic);
            vehicleOffer.AddOptionOffer(PrNumber.Parse("CCC"), Availability.Included(ReasonType.SerialEquipment), Inclusion.Automatic);
            vehicleOffer.AddOptionOffer(prNumberToInclude, Availability.Purchasable(100));
            vehicleOffer.AddOptionOfferRuleSet(prNumberToInclude, ruleSet);

            //Act
            _validator.Validate(PrNumberSet.Parse(prNumberToInclude), vehicleOffer);
        }

        [Fact]
        public void Validate_WhenOption_IsNotAvailable_ForVehicle_Throws()
        {
            //Arrange
            var configuration = _configurationPrNumberSet.Expand(PrNumber.Parse("000"));

            //Act
            Assert.Throws<UnavailableOptionException>(() => _validator.Validate(configuration, _vehicleOffer));
        }

        [Fact]
        public void Validate_WhenOption_IsRequired_Throws()
        {
            //Arrange
            var configuration = _configurationPrNumberSet.Shrink(PrNumber.Parse("ZZZ"));

            //Assert
            Assert.Throws<UnavailableOptionException>(() => _validator.Validate(configuration, _vehicleOffer));
        }

        [Fact]
        public void Validate_WhenMultipleOptions_Rules_AreViolated_Throws()
        {
            //Arrange
            var configuration = _configurationPrNumberSet.Shrink(PrNumber.Parse("XXX"));
            const string expected = "XXX ZZZ XXX";

            //Act
            var e = Assert.Throws<UnavailableOptionException>(() => _validator.Validate(configuration, _vehicleOffer));

            //Assert
            Assert.NotEmpty(e.Details);
            var actual = e.Details.Select(d => d.PrNumbers).Aggregate((a, b) => a + " " + b);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void PerOptionValidator()
        {
            //Assert
            Assert.Injection.OfProperty(new ConfigurationValidator(), nameof(ConfigurationValidator.PerOptionValidator))
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}
