﻿using AutoFixture;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations.Specifications
{
    public class BasedOnCatalogSpecificationTest
    {
        private const int CatalogId = 988;

        //Entities
        private readonly IReadOnlyList<Configuration> _expected;
        private readonly IReadOnlyList<Configuration> _unexpected;
        private readonly IReadOnlyList<Configuration> _all;
        private readonly Catalog _catalog;

        //Specification
        private readonly BasedOnCatalogSpecification _specification;

        public BasedOnCatalogSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());
            
            _expected = fixture.ConstructMany<Configuration>(3, new {catalogId = CatalogId}).ToList();
            _unexpected = fixture.CreateMany<Configuration>(2).ToList();
            _all = _unexpected.Concat(_expected).ToList();
            _catalog = fixture.Construct<Catalog>(CatalogId);

            _specification = new BasedOnCatalogSpecification(CatalogId);
        }

        [Fact]
        public void Ctor_WithCatalog()
        {
            //Act
            var specification = new BasedOnCatalogSpecification(_catalog);

            //Assert
            Assert.Equal(CatalogId, specification.CatalogId);
        }

        [Fact]
        public void Ctor_WithCatalog_GivenCatalog_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("catalog", () => new BasedOnCatalogSpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }
        
        [Fact]
        public void ToExpression()
        {   
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
