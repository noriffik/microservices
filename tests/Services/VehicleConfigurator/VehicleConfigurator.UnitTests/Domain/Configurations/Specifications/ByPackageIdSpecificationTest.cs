﻿using AutoFixture;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations.Specifications
{
    public class ByPackageIdSpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<Configuration> _expected;
        private readonly IReadOnlyList<Configuration> _unexpected;
        private readonly IReadOnlyList<Configuration> _all;
        private readonly PackageId _packageId;

        //Specification
        private readonly ByPackageIdSpecification _specification;

        public ByPackageIdSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _packageId = fixture.Create<PackageId>();

            _expected = fixture.CreateMany<Configuration>(3).ToList();
            foreach (var configuration in _expected)
                configuration.IncludePackage(_packageId);

            _unexpected = fixture.CreateMany<Configuration>(3).ToList();
            _all = _unexpected.Concat(_expected).ToList();

            _specification = new ByPackageIdSpecification(_packageId);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Equal(_packageId, _specification.PackageId);
        }

        [Fact]
        public void Ctor_GivenPackageId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("packageId", () => new ByPackageIdSpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
