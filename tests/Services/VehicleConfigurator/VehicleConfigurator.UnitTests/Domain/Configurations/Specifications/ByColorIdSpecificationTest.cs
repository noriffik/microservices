﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations.Specifications
{
    public class ByColorIdSpecificationTest
    {
        private readonly ColorId _colorId;

        //Entities
        private readonly IReadOnlyList<Configuration> _expected;
        private readonly IReadOnlyList<Configuration> _unexpected;
        private readonly IReadOnlyList<Configuration> _all;

        //Specification
        private readonly ByColorIdSpecification _specification;

        public ByColorIdSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _colorId = fixture.Create<ColorId>();

            _expected = fixture.ConstructMany<Configuration>(3, new { colorId = _colorId }).ToList();
            _unexpected = fixture.CreateMany<Configuration>(2).ToList();
            _all = _unexpected.Concat(_expected).ToList();

            _specification = new ByColorIdSpecification(_colorId);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var specification = new ByColorIdSpecification(_colorId);

            //Assert
            Assert.Equal(_colorId, specification.ColorId);
        }

        [Fact]
        public void Ctor_WhenGivenColorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("colorId", () => new ByColorIdSpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
