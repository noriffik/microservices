﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations.Specifications
{
    public class ByModelKeySpecificationTest
    {
        private readonly ModelKey _modelKey;

        //Entities
        private readonly IReadOnlyList<Configuration> _expected;
        private readonly IReadOnlyList<Configuration> _unexpected;
        private readonly IReadOnlyList<Configuration> _all;

        //Specification
        private readonly ByModelKeySpecification _specification;

        public ByModelKeySpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _modelKey = fixture.Create<ModelKey>();

            _expected = fixture.ConstructMany<Configuration>(3, new { modelKey = _modelKey }).ToList();
            _unexpected = fixture.CreateMany<Configuration>(2).ToList();
            _all = _unexpected.Concat(_expected).ToList();

            _specification = new ByModelKeySpecification(_modelKey);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Equal(_modelKey, _specification.ModelKey);
        }

        [Fact]
        public void Ctor_WhenGivenModelKey_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelKey", () => new ByModelKeySpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
