﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations.Specifications
{
    public class IncludedOptionSpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<Configuration> _expected;
        private readonly IReadOnlyList<Configuration> _unexpected;
        private readonly IReadOnlyList<Configuration> _all;
        private readonly PrNumber _prNumber;

        //Specification
        private readonly IncludedOptionSpecification _specification;

        public IncludedOptionSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _prNumber = fixture.Create<PrNumber>();

            _expected = fixture.CreateMany<Configuration>(2).ToList();
            foreach (var configuration in _expected)
            {
                configuration.IncludeOption(_prNumber);
                configuration.IncludeOptions(fixture.Create<PrNumberSet>());
            }

            _unexpected = fixture.CreateMany<Configuration>(2).ToList();
            foreach (var configuration in _unexpected)
                configuration.IncludeOptions(fixture.Create<PrNumberSet>());

            _all = _unexpected.Concat(_expected).ToList();

            _specification = new IncludedOptionSpecification(_prNumber);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Contains(_prNumber, _specification.PrNumbers);
        }

        [Fact]
        public void Ctor_GivenPrNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumber", () => new IncludedOptionSpecification(null as PrNumber));
        }

        [Fact]
        public void Ctor_GivenPrNumbers_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumberSet", () => new IncludedOptionSpecification(null as PrNumberSet));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
