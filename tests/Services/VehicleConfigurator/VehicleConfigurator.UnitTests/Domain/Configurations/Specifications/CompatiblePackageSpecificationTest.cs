﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations.Specifications
{
    public class CompatiblePackageSpecificationTest
    {
        private const int CatalogId = 988;

        //Entities
        private readonly IReadOnlyList<Package> _expected;
        private readonly IReadOnlyList<Package> _unexpected;
        private readonly IReadOnlyList<Package> _all;

        //Specification
        private readonly CompatiblePackageSpecification _specification;

        public CompatiblePackageSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Create configuration
            var configuration = fixture.Construct<Configuration>(new { catalogId = CatalogId });

            //Define expected package's vehicle coverage
            var modelKeySet = fixture.Create<BoundModelKeySet>()
                .Expand(new[] { configuration.ModelKey });

            var modelId = modelKeySet.ModelId;

            //Create entities
            var perfectMatch = fixture.Construct<Package, PackageId>(
                PackageId.Next(modelId, fixture.Create<PackageCode>()),
                new { modelKeySet, catalogId = CatalogId });
            perfectMatch.ChangeStatus(PackageStatus.Published);

            var anotherPerfectMatch = fixture.Construct<Package, PackageId>(
                PackageId.Next(modelId, fixture.Create<PackageCode>()),
                new { modelKeySet, catalogId = CatalogId });
            anotherPerfectMatch.ChangeStatus(PackageStatus.Published);

            var matchAnotherCatalog = fixture.Construct<Package, PackageId>(
                PackageId.Next(modelId, fixture.Create<PackageCode>()),
                new { modelKeySet });
            matchAnotherCatalog.ChangeStatus(PackageStatus.Published);

            var matchUnexpectedStatus = fixture.Construct<Package, PackageId>(
                PackageId.Next(modelId, fixture.Create<PackageCode>()),
                new { modelKeySet, catalogId = CatalogId });

            matchAnotherCatalog.ChangeStatus(PackageStatus.Draft);

            var nonMatching = fixture.Create<Package>();

            //Setup expectations
            _expected = new[]
                {
                    perfectMatch,
                    anotherPerfectMatch
                }
                .ToList();
            _unexpected = new[]
                {
                    matchAnotherCatalog,
                    matchUnexpectedStatus,
                    nonMatching
                }
                .ToList();
            _all = _unexpected.Concat(_expected)
                .ToList();

            _specification = new CompatiblePackageSpecification(configuration);
        }

        [Fact]
        public void Ctor_GivenConfiguration_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("configuration", () => new CompatiblePackageSpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
