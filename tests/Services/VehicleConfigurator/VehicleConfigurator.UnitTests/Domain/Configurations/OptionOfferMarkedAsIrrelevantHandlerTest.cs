﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Configurations
{
    public class OptionOfferMarkedAsIrrelevantHandlerTest
    {
        //Dependencies
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly VehicleOffer _vehicleOffer;
        private readonly PrNumber _prNumber;
        private readonly IReadOnlyCollection<Configuration> _configurations;

        //Event and handler
        private readonly OptionOfferMarkedAsIrrelevantEvent _event;
        private readonly OptionOfferMarkedAsIrrelevantHandler _handler;

        public OptionOfferMarkedAsIrrelevantHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            var work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entities
            _vehicleOffer = fixture.Create<VehicleOffer>();
            _prNumber = fixture.Create<PrNumber>();
            _configurations = fixture.CreateMany<Configuration>(3).ToList();

            //Event and handler
            _event = fixture.Construct<OptionOfferMarkedAsIrrelevantEvent>(new { prNumber = _prNumber });
            _handler = new OptionOfferMarkedAsIrrelevantHandler(work.Object);

        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(OptionOfferMarkedAsIrrelevantHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenEvent_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("notification", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<VehicleOffer>(_event.VehicleOfferId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer);
                _repository.Setup(r => r.Find(It.Is<ByVehicleOfferWithIncludedOptionSpecification>(s =>
                        s.CatalogId == _vehicleOffer.CatalogId &&
                        s.ModelKey == _vehicleOffer.ModelKey &&
                        s.PrNumberSet.Has(_prNumber)), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_configurations);

                //Act
                await _handler.Handle(_event, CancellationToken.None);
            }

            //Assert
            Assert.All(_configurations, c => Assert.Equal(Relevance.Irrelevant, c.Relevance));
        }
    }
}