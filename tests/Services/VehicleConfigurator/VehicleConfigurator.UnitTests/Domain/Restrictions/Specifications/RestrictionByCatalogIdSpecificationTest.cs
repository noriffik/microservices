﻿using AutoFixture;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.Domain.Restrictions.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Restrictions.Specifications
{
    public class RestrictionByCatalogIdSpecificationTest
    {
        private readonly List<Restriction> _expected;
        private readonly List<Restriction> _unexpected;
        private readonly List<Restriction> _all;
        
        private readonly RestrictionByCatalogIdSpecification _specification;

        public RestrictionByCatalogIdSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Create entities
            var catalogId = fixture.Create<int>();
            _expected = Enumerable.Range(1, 3)
                .Select(i => fixture.Construct<Restriction>(new { catalogId }))
                .ToList();
            
            _unexpected = fixture.CreateMany<Restriction>(2).ToList();

            _all = _expected.Concat(_unexpected).ToList();

            //Create specification
            _specification = new RestrictionByCatalogIdSpecification(catalogId);
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
