﻿using AutoFixture;
using NexCore.Common;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Restrictions
{
    public class RestrictionTest
    {
        private readonly int _catalogId;
        private readonly ModelKey _modelKey;
        private readonly PrNumberSet _prNumbers;
        private readonly Period _relevancePeriod;
        private readonly Fixture _fixture;
        private readonly string _description;
        private readonly Restriction _restriction;

        public RestrictionTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            _catalogId = _fixture.Create<int>();
            _modelKey = _fixture.Create<ModelKey>();
            _prNumbers = _fixture.Create<PrNumberSet>();
            _relevancePeriod = _fixture.Create<Period>();
            _description = _fixture.Create<string>();
            _restriction = new Restriction(_catalogId, _modelKey, _prNumbers, _relevancePeriod);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var result = new Restriction(_catalogId, _modelKey, _prNumbers, _relevancePeriod)
            {
                Description = _description
            };

            //Assert
            Assert.Equal(_catalogId, result.CatalogId);
            Assert.Equal(_modelKey, result.ModelKey);
            Assert.Equal(_prNumbers, result.PrNumbers);
            Assert.Equal(_relevancePeriod, result.RelevancePeriod);
            Assert.Equal(_description, result.Description);
        }

        [Fact]
        public void Ctor_GivenModelKey_IsNull_Throws()
        {

            Assert.Throws<ArgumentNullException>("modelKey",
                () => new Restriction(_catalogId, null, _prNumbers, _relevancePeriod));
        }

        [Fact]
        public void Ctor_PrNumbers_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumbers",
                () => new Restriction(_catalogId, _modelKey, null, _relevancePeriod));
        }

        [Fact]
        public void Description_DefaultValue()
        {
            //Arrange
            var restriction = _fixture.Build<Restriction>()
                .Without(r => r.Description)
                .Create();

            //Assert
            Assert.Contains(restriction.PrNumbers.AsString, restriction.Description);
            Assert.Contains(restriction.ModelKey, restriction.Description);
            Assert.Contains(restriction.RelevancePeriod.ToString(), restriction.Description);
        }

        [Fact]
        public void ChangeRelevancePeriod()
        {
            //Arrange
            var period = _fixture.Create<Period>();

            //Act
            _restriction.ChangeRelevancePeriod(period);

            //Assert
            Assert.Equal(period, _restriction.RelevancePeriod);
        }

        [Fact]
        public void ChangeRelevancePeriod_WhenRestriction_IsNoLongerRelevant_PostsEvent()
        {
            using (var time = new SystemTimeContext(DateTime.Now))
            {
                //Arrange
                var period = new Period(time.Now.AddDays(-2), time.Now.AddDays(-1));
                var restriction = _fixture.Construct<Restriction>(123, new { relevancePeriod = new Period(time.Now) });
                restriction.Enable();

                var expected = new RestrictionMarkedAsIrrelevantEvent(restriction.Id);

                //Act
                restriction.ChangeRelevancePeriod(period);

                //Assert
                Assert.Contains(expected, restriction.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
            }
        }

        [Fact]
        public void ChangeRelevancePeriod_WhenRestriction_RelevanceStatusUnchanged_DoesNothing()
        {
            using (var time = new SystemTimeContext(DateTime.Now))
            {
                //Arrange
                var period = new Period(time.Now, time.Now.AddDays(1));
                var restriction = _fixture.Construct<Restriction>(123, new { relevancePeriod = new Period(time.Now) });
                restriction.Enable();
                
                //Act
                restriction.ChangeRelevancePeriod(period);

                //Assert
                Assert.Equal(1, restriction.EntityEvents.Count());
            }
        }

        [Fact]
        public void ChangeRelevancePeriod_WhenRestriction_IsNoLongerIrrelevant_PostsEvent()
        {
            using (var time = new SystemTimeContext(DateTime.Now))
            {
                //Arrange
                var period = new Period(time.Now);
                var restriction = _fixture.Construct<Restriction>(
                    123, new { relevancePeriod = new Period(time.Now.AddDays(-2), time.Now.AddDays(-1)) });
                restriction.Enable();

                var expected = new RestrictionMarkedAsRelevantEvent(restriction.Id);

                //Act
                restriction.ChangeRelevancePeriod(period);

                //Assert
                Assert.Contains(expected, restriction.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
            }
        }

        [Fact]
        public void Enable()
        {
            //Arrange
            _restriction.Disable();

            //Act
            _restriction.Enable();

            //Assert
            Assert.True(_restriction.IsEnabled);
        }

        [Fact]
        public void Enable_When_IsNoLongerIrrelevant_PostsEvent()
        {
            using (var time = new SystemTimeContext(DateTime.Now))
            {
                //Arrange
                var restriction = _fixture.Construct<Restriction>(123, new { relevancePeriod = new Period(time.Now) });
                restriction.Disable();

                var expected = new RestrictionMarkedAsRelevantEvent(restriction.Id);

                //Act
                restriction.Enable();

                //Assert
                Assert.Contains(expected, restriction.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
            }
        }

        [Fact]
        public void Enable_When_RelevanceStatusUnchanged_DoesNothing()
        {
            using (var time = new SystemTimeContext(DateTime.Now))
            {
                //Arrange
                var restriction = _fixture.Construct<Restriction>(123, new { relevancePeriod = new Period(time.Now) });
                restriction.Enable();

                //Act
                restriction.Enable();

                //Assert
                Assert.Equal(1, restriction.EntityEvents.Count());
            }
        }

        [Fact]
        public void Disable()
        {
            //Arrange
            _restriction.Enable();

            //Act
            _restriction.Disable();

            //Assert
            Assert.False(_restriction.IsEnabled);
        }

        [Fact]
        public void Disable_When_IsNoLongerRelevant_PostsEvent()
        {
            using (var time = new SystemTimeContext(DateTime.Now))
            {
                //Arrange
                var restriction = _fixture.Construct<Restriction>(123, new { relevancePeriod = new Period(time.Now) });
                restriction.Enable();

                var expected = new RestrictionMarkedAsIrrelevantEvent(restriction.Id);

                //Act
                restriction.Disable();

                //Assert
                Assert.Contains(expected, restriction.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
            }
        }

        [Fact]
        public void Disable_When_RelevanceStatusUnchanged_DoesNothing()
        {
            using (var time = new SystemTimeContext(DateTime.Now))
            {
                //Arrange
                var restriction = _fixture.Construct<Restriction>(123, new { relevancePeriod = new Period(time.Now) });
                restriction.Disable();

                //Act
                restriction.Disable();

                //Assert
                Assert.Empty(restriction.EntityEvents);
            }
        }
        
        [Theory]
        [InlineData(true, true)]
        [InlineData(true, false)]
        [InlineData(false, true)]
        [InlineData(false, false)]
        public void IsRelevant(bool isInPeriod, bool isEnabled)
        {
            using (var time = new SystemTimeContext(DateTime.Now))
            {
                //Arrange
                var expected = isInPeriod && isEnabled;
                var period = new Period(isInPeriod ? time.Now : time.Now.AddDays(1));
                var restriction = _fixture.Construct<Restriction>(new { relevancePeriod = period });

                if (isEnabled)
                    restriction.Enable();

                //Act
                var actual = restriction.IsRelevant();

                //Assert
                Assert.Equal(expected, actual);
            }
        }
    }
}
