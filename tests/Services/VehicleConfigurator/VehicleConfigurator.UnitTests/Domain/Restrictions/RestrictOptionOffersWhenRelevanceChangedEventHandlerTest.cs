﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Common;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Restrictions
{
    public class RestrictOptionOffersWhenRelevanceChangedEventHandlerTest
    {
        private readonly Fixture _fixture;

        //Dependency
        private readonly Mock<IEntityRepository> _repository;

        //Handler
        private readonly RestrictOptionOffersWhenRelevanceChangedEventHandler _handler;

        public RestrictOptionOffersWhenRelevanceChangedEventHandlerTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            var work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Handler
            _handler = new RestrictOptionOffersWhenRelevanceChangedEventHandler(work.Object);
        }

        [Fact]
        public async Task Handle_MarkedIsIrrelevant()
        {
            using (Sequence.Create())
            using (var time = new SystemTimeContext(DateTime.Now))
            {
                //Arrange
                var notification = _fixture.Create<RestrictionMarkedAsRelevantEvent>();

                var restriction = _fixture.Construct<Restriction>(new { relevancePeriod = new Period(time.Now) });
                restriction.Enable();

                var vehicleOffer = new Mock<VehicleOffer>();

                _repository.Setup(r => r.Require<Restriction>(notification.RestrictionId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(restriction);

                _repository.Setup(r =>
                        r.SingleOrDefault(
                            It.Is<VehicleOfferModelKeySpecification>(rr => rr.ModelKeySet.AsString.Contains(restriction.ModelKey.Value)),
                            It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(vehicleOffer.Object);

                vehicleOffer
                    .Setup(o => o.EnforceOptionRestriction(restriction.PrNumbers))
                    .InSequence();

                //Act
                await _handler.Handle(notification, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_MarkedIsIrrelevant_WhenVehicleOffer_IsNotFound_DoesNothing()
        {
            using (Sequence.Create())
            {
                //Arrange
                var notification = _fixture.Create<RestrictionMarkedAsRelevantEvent>();

                var relevancePeriod = new Period(DateTime.MinValue, DateTime.MaxValue);
                var restriction = _fixture.Construct<Restriction>(new { relevancePeriod });
                restriction.Enable();

                _repository.Setup(r => r.Require<Restriction>(notification.RestrictionId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(restriction);

                _repository.Setup(r =>
                        r.SingleOrDefault(
                            It.Is<VehicleOfferModelKeySpecification>(rr => rr.ModelKeySet.AsString.Contains(restriction.ModelKey.Value)),
                            It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(null as VehicleOffer);

                //Act
                await _handler.Handle(notification, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_GivenEvent_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("notification", () => _handler.Handle(null as RestrictionMarkedAsRelevantEvent, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_MarkedIsRelevant()
        {
            using (Sequence.Create())
            using (var time = new SystemTimeContext(DateTime.Now))
            {
                //Arrange
                var notification = _fixture.Create<RestrictionMarkedAsIrrelevantEvent>();

                var restriction = _fixture.Construct<Restriction>(new { relevancePeriod = new Period(time.Now) });
                restriction.Enable();

                var vehicleOffer = new Mock<VehicleOffer>();

                _repository.Setup(r => r.Require<Restriction>(notification.RestrictionId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(restriction);

                _repository.Setup(r =>
                        r.SingleOrDefault(
                            It.Is<VehicleOfferModelKeySpecification>(rr => rr.ModelKeySet.AsString.Contains(restriction.ModelKey.Value)),
                            It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(vehicleOffer.Object);

                vehicleOffer
                    .Setup(o => o.LiftOptionRestriction(restriction.PrNumbers))
                    .InSequence();

                //Act
                await _handler.Handle(notification, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_MarkedIsRelevant_WhenVehicleOffer_IsNotFound_DoesNothing()
        {
            using (Sequence.Create())
            {
                //Arrange
                var notification = _fixture.Create<RestrictionMarkedAsIrrelevantEvent>();

                var relevancePeriod = new Period(DateTime.MinValue, DateTime.MaxValue);
                var restriction = _fixture.Construct<Restriction>(new { relevancePeriod });
                restriction.Enable();

                _repository.Setup(r => r.Require<Restriction>(notification.RestrictionId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(restriction);

                _repository.Setup(r =>
                        r.SingleOrDefault(
                            It.Is<VehicleOfferModelKeySpecification>(rr => rr.ModelKeySet.AsString.Contains(restriction.ModelKey.Value)),
                            It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(null as VehicleOffer);

                //Act
                await _handler.Handle(notification, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_MarkedIsRelevant_GivenEvent_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("notification", () => _handler.Handle(null as RestrictionMarkedAsIrrelevantEvent, CancellationToken.None));
        }
    }
}
