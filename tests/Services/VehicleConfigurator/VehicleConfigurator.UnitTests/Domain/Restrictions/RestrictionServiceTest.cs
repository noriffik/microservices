﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Restrictions
{
    public class RestrictionServiceTest
    {
        private readonly RestrictionService _service;
        private readonly Mock<IEntityRepository> _entityRepository;
        private readonly Restriction _restriction;
        private readonly Fixture _fixture;

        public RestrictionServiceTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            var work = new Mock<IUnitOfWork>();
            _entityRepository = new Mock<IEntityRepository>();

            work.Setup(w => w.EntityRepository)
                .Returns(_entityRepository.Object);

            _restriction = _fixture.Create<Restriction>();

            _service = new RestrictionService(work.Object);
        }
        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work", () => new RestrictionService(null));
        }

        [Fact]
        public async Task Add_GivenRestriction_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("restriction",
                () => _service.Add(null));
        }

        [Fact]
        public async Task Add()
        {
            using (Sequence.Create())
            {
                //Arrange
                _entityRepository.Setup(r => r.HasRequired<Catalog>(_restriction.CatalogId))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _entityRepository.Setup(r => r.Add(It.Is<Restriction>(restriction =>
                        PropertyComparer<Restriction>.Instance.Equals(restriction, _restriction))))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _service.Add(_restriction);
            }
        }

        [Fact]
        public async Task ChangeRelevancePeriod()
        {
            using (Sequence.Create())
            {
                //Arrange
                var period = _fixture.Create<Period>();

                _entityRepository.Setup(r => r.Require<Restriction>(_restriction.Id))
                    .InSequence()
                    .ReturnsAsync(_restriction);

                //Act
                await _service.ChangeRelevancePeriod(_restriction.Id, period);

                //Assert
                Assert.Equal(period, _restriction.RelevancePeriod);
            }
        }

        [Fact]
        public async Task Enable()
        {
            using (Sequence.Create())
            {
                //Arrange
                _entityRepository.Setup(r => r.Require<Restriction>(_restriction.Id))
                    .InSequence()
                    .ReturnsAsync(_restriction);

                //Act
                await _service.Enable(_restriction.Id);

                //Assert
                Assert.True(_restriction.IsEnabled);
            }
        }

        [Fact]
        public async Task Disable()
        {
            using (Sequence.Create())
            {
                //Arrange
                _entityRepository.Setup(r => r.Require<Restriction>(_restriction.Id))
                    .InSequence()
                    .ReturnsAsync(_restriction);

                //Act
                await _service.Disable(_restriction.Id);

                //Assert
                Assert.False(_restriction.IsEnabled);
            }
        }
    }
}
