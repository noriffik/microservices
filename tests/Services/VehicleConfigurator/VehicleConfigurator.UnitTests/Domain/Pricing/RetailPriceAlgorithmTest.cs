﻿using NexCore.VehicleConfigurator.Domain.Pricing;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Pricing
{
    public class RetailPriceAlgorithmTest
    {
        [Theory]
        [InlineData(0, 20, 0, 30)]
        [InlineData(3600, 20, 100, 30)]
        [InlineData(4500, 50, 100, 30)]
        [InlineData(3000, 0, 100, 30)]
        [InlineData(6000, 0, 100, 60)]
        [InlineData(50, 0, 100, 0.5)]
        public void CalculateRetail(decimal expected, decimal margin, decimal basedOn, decimal rate)
        {
            //Arrange
            var algorithm = new RetailPriceAlgorithm
            {
                Rate = rate,
                Margin = margin
            };
            
            //Act
            var actual = algorithm.Calculate(basedOn);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(-33)]
        [InlineData(0)]
        public void Calculate_WhenRate_IsLessThanOne_Throws(decimal rate)
        {
            //Arrange
            var algorithm = new RetailPriceAlgorithm
            {
                Rate = rate
            };

            //Act
            var e = Assert.Throws<InvalidOperationException>(() => algorithm.Calculate(330));

            //Assert
            Assert.Contains("rate", e.Message.ToLowerInvariant());
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-55)]
        public void CalculateRetail_GivenBasedOn_IsNegative_Throws(decimal basedOn)
        {
            //Arrange
            var algorithm = new RetailPriceAlgorithm
            {
                Rate = 30,
                Margin = 10
            };

            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(
                "basedOn", () => algorithm.Calculate(basedOn));
        }
    }
}
