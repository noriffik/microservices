﻿using NexCore.VehicleConfigurator.Domain.Pricing;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Pricing
{
    public class PriceTest
    {
        public static IEnumerable<object[]> Failed() => new List<object[]>
        {
            new object[]{ new Price(1, 10), new Price(2, 20)},
            new object[]{ new Price(2, 20), new Price(2, 20)}
        };

        [Theory]
        [MemberData(nameof(Equality))]
        public void Comparison_Equal(Price less, Price more, bool expected)
        {
            //Act
            var result = less == more;
            
            //Assert
            Assert.Equal(result, expected);
        }

        public static IEnumerable<object[]> Equality() => new List<object[]>
        {
            new object[]{new Price(1, 10), new Price(2, 20 ), false},
            new object[]{new Price(1, 10), new Price(1, 10), true},
            new object[]{null, null, true},
            new object[]{new Price(1, 10), null, false},
            new object[]{null, new Price(1, 10), false}
        };

        [Theory]
        [MemberData(nameof(LessThan))]
        public void Comparison_LessThen(Price less, Price more, bool expected)
        {
            //Act
            var result = less < more;

            //Assert
            Assert.Equal(result, expected);
        }

        public static IEnumerable<object[]> LessThan() => new List<object[]>
        {
            new object[]{new Price(1, 10), new Price(2, 20 ), true},
            new object[]{null, new Price(1, 10), true},
            new object[]{new Price(1, 10), new Price(1, 10), false},
            new object[]{new Price(50, 500), new Price(1, 10), false}
        };

        [Theory]
        [MemberData(nameof(MoreThan))]
        public void Comparison_MoreThen(Price less, Price more, bool expected)
        {
            //Act
            var result = less > more;

            //Assert
            Assert.Equal(result, expected);
        }

        public static IEnumerable<object[]> MoreThan() => new List<object[]>
        {   
            new object[]{new Price(2, 20), new Price(1, 5), true},
            new object[]{new Price(1, 10), null, true},
            new object[]{new Price(1, 10), new Price(1, 10), false},
            new object[]{new Price(1, 10), new Price(2, 20), false},
        };

        [Theory]
        [MemberData(nameof(Compare))]
        public void CompareTo(Price a, Price b, int expected)
        {
            //Act
            var result = a.CompareTo(b);

            //Assert
            Assert.Equal(expected, result);
        }

        public static IEnumerable<object[]> Compare() => new List<object[]>
        {   
            new object[]{new Price(2, 20), new Price(1, 5), 1},
            new object[]{new Price(1, 10), null, 1},
            new object[]{new Price(1, 10), new Price(1, 10), 0},
            new object[]{new Price(1, 10), new Price(2, 20), -1}
        };

        [Theory]
        [MemberData(nameof(Addition))]
        public void Summary(Price a, Price b, Price expected)
        {
            //Act
            var result = a + b;

            //Assert
            Assert.NotNull(result);
            Assert.Equal(expected, result);
        }

        public static IEnumerable<object[]> Addition() => new List<object[]>
        {
            new object[]{ new Price(1, 10),  new Price(2, 20 ), new Price(3, 30)},
            new object[]{ new Price(2, 20), null, new Price(2, 20)},
            new object[]{ null, new Price(2, 20), new Price(2, 20)}
        };

        [Fact]
        public void Subtraction()
        {
            //Arrange
            var a = new Price(2, 20);
            var b = new Price(1, 10);
            var expected = new Price(1, 10);
            
            //Act
            var result = a - b;

            //Assert
            Assert.NotNull(expected);
            Assert.Equal(expected, result);
        }
    }
}
