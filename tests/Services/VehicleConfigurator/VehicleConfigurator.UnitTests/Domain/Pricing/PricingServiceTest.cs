﻿using AutoFixture;
using Moq;
using NexCore.CurrencyExchange.Abstract;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Pricing;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Pricing
{
    public class PricingServiceTest
    {
       private readonly Mock<ICurrencyExchangeService> _currencyExchange = new Mock<ICurrencyExchangeService>();

        [Fact]
        public void Ctor_GivenService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("exchangeService", 
                () => new PricingService(null));
        }

        [Theory]
        [InlineData(0, 20, 0)]
        [InlineData(3600, 20, 100)]
        [InlineData(4500, 50, 100)]
        [InlineData(3000, 0, 100)]
        public async Task CalculateRetail(decimal expected, decimal margin, decimal basedOn)
        {
            //Arrange
             var fixture = new Fixture();
             var onDate = fixture.Create<DateTime>();
            _currencyExchange.Setup(s => s.Convert(Currency.Usd, Currency.Uah, basedOn, onDate))
                .ReturnsAsync(basedOn * 30);
            _currencyExchange.Setup(s => s.GetRate(Currency.Usd, Currency.Uah, onDate))
                .ReturnsAsync(30);

            var service = new PricingService(_currencyExchange.Object)
            {
                Margin = margin
            };
            
            //Act
            var actual = await service.CalculateRetail(basedOn, onDate);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-55)]
        public Task CalculateRetail_GivenBasedOn_IsNegative_Throws(decimal basedOn)
        {
            //Arrange
            var service = new PricingService(_currencyExchange.Object);

            //Assert
            return Assert.ThrowsAsync<ArgumentOutOfRangeException>(
                "basedOn", () => service.CalculateRetail(basedOn, DateTime.MinValue));
        }

        [Fact]
        public async Task CalculateRetail_WhenFailed_ToConvertCurrency_Throws()
        {
            //Arrange
            var inner = new CurrencyExchangeException();
            _currencyExchange.Setup(s => s.GetRate(
                    It.IsAny<Currency>(), It.IsAny<Currency>(), It.IsAny<DateTime>()))
                .ThrowsAsync(inner);

            var service = new PricingService(_currencyExchange.Object);

            //Act
            var e = await Assert.ThrowsAsync<PricingUnavailableException>(
                () => service.CalculateRetail(10, DateTime.MinValue));

            //Assert
            Assert.Equal(inner, e.InnerException);
        }
    }
}
