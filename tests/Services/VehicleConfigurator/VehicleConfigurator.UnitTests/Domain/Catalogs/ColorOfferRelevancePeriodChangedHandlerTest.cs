﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Common;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class ColorOfferRelevancePeriodChangedHandlerTest
    {
        //Dependencies
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly Mock<VehicleOffer> _vehicleOffer;
        private readonly int _vehicleOfferId;
        private readonly ColorOffer _colorOffer;
        private readonly DateTime _from;


        //Event and handler
        private readonly ColorOfferRelevancePeriodChangedEvent _event;
        private readonly ColorOfferRelevancePeriodChangedHandler _handler;

        public ColorOfferRelevancePeriodChangedHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entities
            _vehicleOfferId = fixture.Create<int>();
            _vehicleOffer = new Mock<VehicleOffer>(fixture.Create<ModelKey>(), fixture.Create<int>());

            _colorOffer = fixture.Create<ColorOffer>();
            _from = fixture.Create<DateTime>();

            //Dependencies
            var work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Event and handler
            _event = new ColorOfferRelevancePeriodChangedEvent(_vehicleOfferId, _colorOffer.ColorId);
            _handler = new ColorOfferRelevancePeriodChangedHandler(work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ColorOfferRelevancePeriodChangedHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle_WhenNow_IsInPeriod()
        {
            //Arrange
            _colorOffer.ChangeRelevancePeriod(new RelevancePeriod(_from, _from.AddDays(10)));

            using (new SystemTimeContext(_from.AddDays(1)))
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOfferId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer.Object);
                _vehicleOffer.Setup(v => v.FindColorOfferOrThrow(_colorOffer.ColorId))
                    .InSequence()
                    .Returns(_colorOffer);
                _vehicleOffer.Setup(v => v.ChangeColorOfferRelevance(_colorOffer.ColorId, Relevance.Relevant))
                    .InSequence();

                await _handler.Handle(_event, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_WhenNow_IsNotInPeriod()
        {
            //Arrange
            _colorOffer.ChangeRelevancePeriod(new RelevancePeriod(_from, _from.AddDays(10)));

            using (new SystemTimeContext(_from.AddDays(-1)))
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOfferId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer.Object);
                _vehicleOffer.Setup(v => v.FindColorOfferOrThrow(_colorOffer.ColorId))
                    .InSequence()
                    .Returns(_colorOffer);
                _vehicleOffer.Setup(v => v.ChangeColorOfferRelevance(_colorOffer.ColorId, Relevance.Irrelevant))
                    .InSequence();

                await _handler.Handle(_event, CancellationToken.None);
            }
        }
    }
}
