﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class ChangedOptionOfferStatusRestrictionEventHandlerTest
    {
        private readonly Fixture _fixture;

        //Entities
        private readonly List<Configuration> _configurations;

        //Dependencies
        private readonly Mock<IEntityRepository> _repository;

        //Handler
        private readonly ChangedOptionOfferStatusRestrictionEventHandler _handler;

        public ChangedOptionOfferStatusRestrictionEventHandlerTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            //Entities
            _configurations = _fixture.CreateMany<Configuration>().ToList();

            //Dependencies
            _repository = new Mock<IEntityRepository>();
            var work = new Mock<IUnitOfWork>();

            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Handler
            _handler = new ChangedOptionOfferStatusRestrictionEventHandler(work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ChangedOptionOfferStatusRestrictionEventHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle_GivenRestrictionEnforcedUponOptionOfferEvent_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("notification",
                () => _handler.Handle(null as RestrictionEnforcedUponOptionOfferEvent, It.IsAny<CancellationToken>()));
        }

        [Fact]
        public async Task Handle_GivenRestrictionLiftedFromOptionOfferEvent_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("notification",
                () => _handler.Handle(null as RestrictionLiftedFromOptionOfferEvent, It.IsAny<CancellationToken>()));
        }

        [Fact]
        public async Task Handle_WhenEnforceRestriction()
        {
            using (Sequence.Create())
            {
                //Arrange
                var notification = _fixture.Create<RestrictionEnforcedUponOptionOfferEvent>();

                _repository.Setup(r => r.Find(It.Is<ByVehicleOfferWithIncludedOptionSpecification>(s =>
                    s.ModelKey == notification.Affected.ModelKey && s.CatalogId == notification.Affected.CatalogId &&
                    s.PrNumberSet == notification.Affected.PrNumbers), It.IsAny<CancellationToken>()))
                    .ReturnsAsync(_configurations);

                //Act
                await _handler.Handle(notification, CancellationToken.None);

                //Assert
                Assert.True(_configurations.All(c => c.IsRestricted));
            }
        }

        [Fact]
        public async Task Handle_WhenLiftRestriction()
        {
            using (Sequence.Create())
            {
                //Arrange
                var notification = _fixture.Create<RestrictionLiftedFromOptionOfferEvent>();

                _configurations.ForEach(c => c.EnforceRestriction());

                _repository.Setup(r => r.Find(It.Is<ByVehicleOfferWithIncludedOptionSpecification>(s =>
                    s.ModelKey == notification.Affected.ModelKey && s.CatalogId == notification.Affected.CatalogId &&
                    s.PrNumberSet == notification.Affected.PrNumbers), It.IsAny<CancellationToken>()))
                    .ReturnsAsync(_configurations);

                //Act
                await _handler.Handle(notification, CancellationToken.None);

                //Assert
                Assert.False(_configurations.All(c => c.IsRestricted));
            }
        }
    }
}
