﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using NexCore.Common;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class VehicleOfferRelevancePeriodChangedEventHandlerTest
    {
        private readonly DateTime _date;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly VehicleOffer _vehicleOffer;

        //Event and handler
        private readonly VehicleOfferRelevancePeriodChangedEvent _event;
        private readonly VehicleOfferRelevancePeriodChangedEventHandler _handler;

        public VehicleOfferRelevancePeriodChangedEventHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _date = fixture.Create<DateTime>();

            //Entity
            _vehicleOffer = fixture.Create<VehicleOffer>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();

            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Event and handler
            _event = new VehicleOfferRelevancePeriodChangedEvent(_vehicleOffer.Id);
            _handler = new VehicleOfferRelevancePeriodChangedEventHandler(_work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(VehicleOfferRelevancePeriodChangedEventHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle_GivenEvent_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("notification",
                () => _handler.Handle(null, It.IsAny<CancellationToken>()));
        }

        [Fact]
        public async Task Handle_WhenIrrelevant()
        {
            using (new SystemTimeContext(_date))
            using (Sequence.Create())
            {
                //Arrange
                var irrelevantPeriod = new RelevancePeriod(_date.AddDays(-2), _date.AddDays(-1));
                _vehicleOffer.ChangeRelevancePeriod(irrelevantPeriod);

                _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOffer.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer);

                //Act
                await _handler.Handle(_event, CancellationToken.None);

                //Assert
                Assert.Equal(Relevance.Irrelevant, _vehicleOffer.Relevance);
            }
        }

        [Fact]
        public async Task Handle_WhenRelevant()
        {
            using (new SystemTimeContext(_date))
            using (Sequence.Create())
            {
                //Arrange
                var relevantPeriod = new RelevancePeriod(_date.AddDays(-2), _date.AddDays(1));
                _vehicleOffer.ChangeRelevancePeriod(relevantPeriod);

                _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOffer.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer);

                //Act
                await _handler.Handle(_event, CancellationToken.None);

                //Assert
                Assert.Equal(Relevance.Relevant, _vehicleOffer.Relevance);
            }
        }
    }
}
