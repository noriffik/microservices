﻿using AutoFixture;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.Catalogs
{
    public class PublishedCatalogSpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<Catalog> _expected;
        private readonly IReadOnlyList<Catalog> _unexpected;
        private readonly IReadOnlyList<Catalog> _all;

        //Specification
        private readonly PublishedCatalogSpecification _specification;

        public PublishedCatalogSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());
            
            //Create entities
            _expected = fixture.Build<Catalog>()
                .With(c => c.Status, CatalogStatus.Published)
                .CreateMany(2)
                .ToList();
            _unexpected = fixture.Build<Catalog>()
                .With(c => c.Status, CatalogStatus.Draft)
                .CreateMany(3)
                .ToList();
            _all = _unexpected.Concat(_expected).ToList();
            
            //Create specification
            _specification = new PublishedCatalogSpecification();
        }
        
        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }
        
        [Fact]
        public void ToExpression()
        {   
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
