﻿using AutoFixture;
using NexCore.Common;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.Catalogs
{
    public class RelevantCatalogSpecificationTest
    {
        private readonly DateTime _now;

        //Entities
        private readonly IReadOnlyList<Catalog> _expected;
        private readonly IReadOnlyList<Catalog> _unexpected;
        private readonly IReadOnlyList<Catalog> _all;

        //Specification
        private readonly RelevantCatalogSpecification _specification;

        public RelevantCatalogSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _now = new DateTime(3, 3, 3);

            //Create entities
            _expected = new[]
                {
                    fixture.Construct<Catalog>(new { period = new Period(_now)}),
                    fixture.Construct<Catalog>(new { period = new Period(_now, _now.AddDays(1))}),
                    fixture.Construct<Catalog>(new { period = new Period(_now.AddDays(-1))}),
                    fixture.Construct<Catalog>(new { period = new Period(_now.AddDays(-1), _now.AddDays(1))}),
                }
                .ToList();

            _unexpected = new[]
                {
                    fixture.Construct<Catalog>(new { period = new Period(_now.AddDays(1), _now.AddDays(2))}),
                    fixture.Construct<Catalog>(new { period = new Period(_now.AddDays(-2), _now.AddDays(-1))}),
                    fixture.Construct<Catalog>(new { period = new Period(_now.AddDays(1))}),
                    fixture.Construct<Catalog>(new { period = new Period(_now.AddDays(-1), _now)})
                }
                .ToList();

            _all = _unexpected.Concat(_expected).ToList();

            //Create specification
            _specification = new RelevantCatalogSpecification();
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            using (new SystemTimeContext(_now))
            {
                Assert.True(_specification.IsSatisfiedBy(_expected[0]));
            }
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            using (new SystemTimeContext(_now))
            {
                Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
            }
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            using (new SystemTimeContext(_now))
            {
                //Act
                var expression = _specification.ToExpression();
                var actual = _all.AsQueryable().Where(expression);

                //Assert
                Assert.NotNull(expression);
                Assert.Equal(_expected, actual);
            }
        }
    }
}
