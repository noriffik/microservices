﻿using AutoFixture;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Packages;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.Packages
{
    public class PackageStatusSpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<Package> _expected;
        private readonly IReadOnlyList<Package> _unexpected;
        private readonly IReadOnlyList<Package> _all;

        //Specification
        private readonly PackageStatusSpecification _specification;

        public PackageStatusSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Create entities
            _expected = fixture.CreateMany<Package>(3).ToList();
            _unexpected = fixture.CreateMany<Package>(2).ToList();
            _all = _unexpected.Concat(_expected).ToList();

            //Setup entities
            _expected.ToList().ForEach(e => e.ChangeStatus(PackageStatus.Published));
            _unexpected.ToList().ForEach(e => e.ChangeStatus(PackageStatus.Draft));

            //Create specification
            _specification = new PackageStatusSpecification(PackageStatus.Published);
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }
        
        [Fact]
        public void ToExpression()
        {   
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
