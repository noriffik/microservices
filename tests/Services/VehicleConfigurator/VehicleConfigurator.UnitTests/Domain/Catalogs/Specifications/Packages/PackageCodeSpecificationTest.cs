﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Packages;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.Packages
{
    public class PackageCodeSpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<Package> _expected;
        private readonly IReadOnlyList<Package> _unexpected;
        private readonly IReadOnlyList<Package> _all;
        private readonly PackageCode _code;

        //Specification
        private readonly PackageCodeSpecification _specification;

        public PackageCodeSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _code = PackageCode.Parse("PKG");

            //Create entities
            _expected = Enumerable.Range(0, 3)
                .Select(n => fixture.Construct<Package, PackageId>(
                    PackageId.Next(ModelId.Parse($"M{n}"), _code), new { modelKeySet = BoundModelKeySet.Parse($"M{n}0000") }))
                .ToList();

            _unexpected = fixture.CreateMany<Package>(2).ToList();
            _all = _unexpected.Concat(_expected).ToList();

            //Create specification
            _specification = new PackageCodeSpecification(_code);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var specification = new PackageCodeSpecification(_code);

            //Assert
            Assert.Equal(_code, specification.Code);
        }

        [Fact]
        public void Ctor_GivenCode_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("code", () => new PackageCodeSpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }
        
        [Fact]
        public void ToExpression()
        {   
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
