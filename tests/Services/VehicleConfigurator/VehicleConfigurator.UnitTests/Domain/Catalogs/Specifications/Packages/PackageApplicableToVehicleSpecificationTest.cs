﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Packages;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.Packages
{
    public class PackageApplicableToVehicleSpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<Package> _expected;
        private readonly IReadOnlyList<Package> _unexpected;
        private readonly IReadOnlyList<Package> _all;

        //Specification
        private readonly PackageApplicableToVehicleSpecification _specification;

        public PackageApplicableToVehicleSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Define expected package's vehicle coverage
            var modelKeySet = fixture.Create<BoundModelKeySet>();

            //Define criteria
            var modelKey = modelKeySet.Values.ElementAt(1);
            var modelId = modelKey.ModelId;
            var packageId = PackageId.Next(modelId, fixture.Create<PackageCode>());

            //Create entities
            _expected = new []
                {
                    fixture.Construct<Package, PackageId>(packageId, new { modelKeySet })
                }
                .ToList();

            _unexpected = fixture.CreateMany<Package>(3).ToList();
            _all = _unexpected.Concat(_expected).ToList();
            
            //Create specification
            _specification = new PackageApplicableToVehicleSpecification(modelKey);
        }

        [Fact]
        public void Ctor_GivenModelKey_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelKey", () => new PackageApplicableToVehicleSpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }
        
        [Fact]
        public void ToExpression()
        {   
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
