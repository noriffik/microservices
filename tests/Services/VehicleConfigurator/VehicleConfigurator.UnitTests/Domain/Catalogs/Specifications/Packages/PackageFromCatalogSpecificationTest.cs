﻿using AutoFixture;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Packages;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.Packages
{
    public class PackageFromCatalogSpecificationTest
    {
        private const int CatalogId = 988;

        //Entities
        private readonly IReadOnlyList<Package> _expected;
        private readonly IReadOnlyList<Package> _unexpected;
        private readonly IReadOnlyList<Package> _all;
        private readonly Catalog _catalog;

        //Specification
        private readonly PackageFromCatalogSpecification _specification;

        public PackageFromCatalogSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Create entities
            _expected = Enumerable.Repeat(fixture.Create<PackageId>(), 2)
                .Select(id => fixture.Construct<Package, PackageId>(id, new {catalogId = CatalogId}))
                .ToList();

            _unexpected = fixture.CreateMany<Package>(2).ToList();
            _all = _unexpected.Concat(_expected).ToList();
            _catalog = fixture.Construct<Catalog>(CatalogId);

            //Create specification
            _specification = new PackageFromCatalogSpecification(CatalogId);
        }

        [Fact]
        public void Ctor_WithCatalog()
        {
            //Act
            var specification = new PackageFromCatalogSpecification(_catalog);

            //Assert
            Assert.Equal(CatalogId, specification.CatalogId);
        }

        [Fact]
        public void Ctor_WithCatalog_GivenCatalog_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("catalog", () => new PackageFromCatalogSpecification(null));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }
        
        [Fact]
        public void ToExpression()
        {   
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
