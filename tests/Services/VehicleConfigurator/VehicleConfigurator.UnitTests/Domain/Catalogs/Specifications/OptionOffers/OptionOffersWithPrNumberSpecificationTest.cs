﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.OptionOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.OptionOffers
{
    public class OptionOffersWithPrNumberSpecificationTest
    {
        private readonly OptionOffer[] _sequence;
        private readonly OptionOffersWithPrNumberSpecification _specification;
        private readonly OptionOffer[] _expected;
        private readonly OptionOffer[] _unexpected;

        public OptionOffersWithPrNumberSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var prNumbers = fixture.Create<PrNumberSet>();

            _expected = prNumbers.Values
                .Select(i => fixture.Construct<OptionOffer>(new { optionId = OptionId.Parse($"00{i.Value}") }))
                .ToArray();

            _unexpected = Enumerable.Range(0, 4)
                .Select(i => fixture.Construct<OptionOffer>(new { optionId = OptionId.Parse($"00{i}XX") }))
                .ToArray();

            _sequence = _expected.Concat(_unexpected).ToArray();

            _specification = new OptionOffersWithPrNumberSpecification(prNumbers);
        }

        [Fact]
        public void Ctor_GivenPrNumbers_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumbers", () => new OptionOffersWithPrNumberSpecification(null));
        }

        [Fact]
        public void IsSatisfied_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _sequence.AsQueryable().Where(expression));
        }
    }
}
