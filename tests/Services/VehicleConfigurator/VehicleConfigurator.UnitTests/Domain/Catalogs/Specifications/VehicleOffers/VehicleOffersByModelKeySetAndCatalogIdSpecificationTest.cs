﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOffersByModelKeySetAndCatalogIdSpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<VehicleOffer> _expected;
        private readonly IReadOnlyList<VehicleOffer> _unexpected;
        private readonly IReadOnlyList<VehicleOffer> _all;

        //Specification
        private readonly VehicleOffersByModelKeySetAndCatalogIdSpecification _specification;

        public VehicleOffersByModelKeySetAndCatalogIdSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var catalogId = fixture.Create<int>();
            var modelKeySet = ModelKeySet.Parse("000003 000004 000005");

            var t = modelKeySet.Values.ToArray();

            //Entities
            _expected = Enumerable.Range(0, 2).Select(i =>
                fixture.Construct<VehicleOffer>(new { modelKey = t[i], catalogId })).ToArray();

            _unexpected = Enumerable.Range(0, 1).Select(i => fixture.Construct<VehicleOffer>(new { catalogId })).ToArray();

            _all = _expected.Concat(_unexpected).ToList();

            //Specification
            _specification = new VehicleOffersByModelKeySetAndCatalogIdSpecification(catalogId, modelKeySet);
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
