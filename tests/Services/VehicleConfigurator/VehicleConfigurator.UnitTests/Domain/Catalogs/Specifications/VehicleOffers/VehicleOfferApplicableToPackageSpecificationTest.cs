﻿using AutoFixture;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferApplicableToPackageSpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<VehicleOffer> _expected;
        private readonly IReadOnlyList<VehicleOffer> _unexpected;
        private readonly IReadOnlyList<VehicleOffer> _all;

        //Specification
        private readonly VehicleOfferApplicableToPackageSpecification _specification;

        public VehicleOfferApplicableToPackageSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var package = fixture.Create<Package>();
            var vehicleOffer = fixture.Construct<VehicleOffer>(new {modelKey = package.ModelKeySet.Values.First()});
            foreach (var prNumber in package.Applicability.PrNumbers.Values)
            {
                vehicleOffer.AddOptionOffer(prNumber, fixture.Create<Availability>());
            }
            
            _expected = new[] {vehicleOffer};
            _unexpected = fixture.CreateMany<VehicleOffer>(2).ToList();
            _all = _unexpected.Concat(_expected).ToList();
            
            _specification = new VehicleOfferApplicableToPackageSpecification(package.Applicability);
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }
        
        [Fact]
        public void ToExpression()
        {   
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
