﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferEngineIdSpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<VehicleOffer> _expected;
        private readonly IReadOnlyList<VehicleOffer> _unexpected;
        private readonly IReadOnlyList<VehicleOffer> _all;

        //Specification
        private readonly VehicleOfferEngineIdSpecification _specification;

        public VehicleOfferEngineIdSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var expectedEngineId = fixture.Create<EngineId>();
            var unexpectedEngineId = fixture.CreateMany<EngineId>(3);
            var modelKeys = new[] { "34#6", "45#7", "qw#7", "e0#6" };

            //Entities
            _expected = modelKeys.Select(s =>
                fixture.Construct<VehicleOffer>(new
                {
                    modelKey = ModelKey.Parse(expectedEngineId.Value.Substring(0, 2) + s.Replace('#', expectedEngineId.Value.Last()))
                }))
                .ToList();

            _unexpected = unexpectedEngineId.SelectMany(engineId => modelKeys.Select(s =>
                fixture.Construct<VehicleOffer>(new
                {
                    modelKey = ModelKey.Parse(engineId.Value.Substring(0, 2) + s.Replace('#', engineId.Value.Last()))

                })))
                .ToList();

            _all = _unexpected.Concat(_expected).ToList();

            //Specification
            _specification = new VehicleOfferEngineIdSpecification(expectedEngineId);
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
