﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferBodyIdSpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<VehicleOffer> _expected;
        private readonly IReadOnlyList<VehicleOffer> _unexpected;
        private readonly IReadOnlyList<VehicleOffer> _all;

        //Specification
        private readonly VehicleOfferBodyIdSpecification _specification;

        public VehicleOfferBodyIdSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var expectedBodyId = fixture.Create<BodyId>();
            var unexpectedBodyId = fixture.CreateMany<BodyId>(3);
            var modelKeys = new[] { "#346", "#457", "#qw7", "#e06" };

            //Entities
            _expected = modelKeys.Select(s =>
                fixture.Construct<VehicleOffer>(new
                {
                    modelKey = ModelKey.Parse(expectedBodyId.Value.Substring(0, 2) + s.Replace('#', expectedBodyId.Value.Last()))
                }))
                .ToList();

            _unexpected = unexpectedBodyId.SelectMany(bodyId => modelKeys.Select(s =>
                fixture.Construct<VehicleOffer>(new
                {
                    modelKey = ModelKey.Parse(bodyId.Value.Substring(0, 2) + s.Replace('#', bodyId.Value.Last()))

                })))
                .ToList();

            _all = _unexpected.Concat(_expected).ToList();

            //Specification
            _specification = new VehicleOfferBodyIdSpecification(expectedBodyId);
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
