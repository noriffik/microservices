﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferEquipmentIdSpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<VehicleOffer> _expected;
        private readonly IReadOnlyList<VehicleOffer> _unexpected;
        private readonly IReadOnlyList<VehicleOffer> _all;

        //Specification
        private readonly VehicleOfferEquipmentIdSpecification _specification;

        public VehicleOfferEquipmentIdSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var expectedEquipmentId = fixture.Create<EquipmentId>();
            var unexpectedEquipmentIds = fixture.CreateMany<EquipmentId>(3);
            var modelKeys = new[] { "3#46", "4#57", "q#w7", "e#06" };

            //Entities
            _expected = modelKeys.Select(s =>
                fixture.Construct<VehicleOffer>(new
                {
                    modelKey = ModelKey.Parse(expectedEquipmentId.Value.Substring(0, 2) + s.Replace('#', expectedEquipmentId.Value.Last()))
                }))
                .ToList();

            _unexpected = unexpectedEquipmentIds.SelectMany(equipmentId => modelKeys.Select(s =>
                fixture.Construct<VehicleOffer>(new
                {
                    modelKey = ModelKey.Parse(equipmentId.Value.Substring(0, 2) + s.Replace('#', equipmentId.Value.Last()))

                })))
                .ToList();

            _all = _unexpected.Concat(_expected).ToList();

            //Specification
            _specification = new VehicleOfferEquipmentIdSpecification(expectedEquipmentId);
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
