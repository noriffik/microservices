﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferByPrNumberSetSpecificationTest
    {
        private readonly VehicleOffer[] _sequence;
        private readonly VehicleOfferByPrNumberSetSpecification _specification;

        public VehicleOfferByPrNumberSetSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var prNumber = PrNumberSet.Parse("XX1 XX2 XX3");
            _sequence = fixture.CreateMany<VehicleOffer>(3).ToArray();

            prNumber.Values
                .ToList().ForEach(n => _sequence[0].AddOptionOffer(n, fixture.Create<Availability>()));

            _specification = new VehicleOfferByPrNumberSetSpecification(prNumber);
        }

        [Fact]
        public void Ctor_GivenPrNumber_IsNull_Trows()
        {
            Assert.Throws<ArgumentNullException>("prNumberSet", () => new VehicleOfferByPrNumberSetSpecification(null));
        }

        [Fact]
        public void IsSatisfied_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_sequence[0]));
        }

        [Fact]
        public void IsSatisfied_ReturnFalse()
        {
            //Arrange
            var specification = new VehicleOfferByPrNumberSetSpecification(PrNumberSet.Parse("CC1 CC2"));

            //Assert
            Assert.False(specification.IsSatisfiedBy(_sequence[0]));
        }

        [Fact]
        public void ToExpression()
        {
            //Arrange
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.True(_sequence.AsQueryable().Any(expression));
        }
    }
}
