﻿using AutoFixture;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOffersViolatingPackageSpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<VehicleOffer> _expected;
        private readonly IReadOnlyList<VehicleOffer> _unexpected;
        private readonly IReadOnlyList<VehicleOffer> _all;

        private readonly Package _package;
        private readonly VehicleOffer _vehicleOffer;

        private readonly Fixture _fixture;

        //Specification
        private readonly VehicleOffersViolatingPackageSpecification _specification;

        public VehicleOffersViolatingPackageSpecificationTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            _package = _fixture.Construct<Package>(new { catalogId = 12 });
            _vehicleOffer = _fixture.Construct<VehicleOffer>(
                new {modelKey = _package.ModelKeySet.Values.First(), catalogId = _package.CatalogId });

            var fromAnotherCatalog = _fixture.Construct<VehicleOffer>(
                new {modelKey = _package.ModelKeySet.Values.First(), catalogId = 99 });

            _expected = new[] {_vehicleOffer};
            _unexpected = _fixture.CreateMany<VehicleOffer>(2)
                .Concat(new[] {fromAnotherCatalog})
                .ToList();
            _all = _unexpected.Concat(_expected).ToList();

            _specification = new VehicleOffersViolatingPackageSpecification(_package);
        }

         [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenVehicleOffer_DoesNotCoveredByPackage_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenVehicleOffer_IsApplicableToPackage_ReturnsFalse()
        {
            //Arrange
            foreach ( var prNumber in _package.Applicability.PrNumbers.Values)
            {
                _vehicleOffer.AddOptionOffer(prNumber, _fixture.Create<Availability>());
            }

            Assert.False(_specification.IsSatisfiedBy(_vehicleOffer));
        }
        
        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }
        
        [Fact]
        public void ToExpression()
        {   
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
