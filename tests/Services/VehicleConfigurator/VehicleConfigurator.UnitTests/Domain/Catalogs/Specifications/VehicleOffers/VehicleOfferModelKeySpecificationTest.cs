﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferModelKeySpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<VehicleOffer> _expected;
        private readonly IReadOnlyList<VehicleOffer> _unexpected;
        private readonly IReadOnlyList<VehicleOffer> _all;

        //Specification
        private readonly VehicleOfferModelKeySpecification _specification;

        public VehicleOfferModelKeySpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var modelKey = fixture.Create<ModelKey>();

            //Create entities
            _expected = fixture.ConstructMany<VehicleOffer>(3, new { modelKey }).ToList();
            _unexpected = fixture.CreateMany<VehicleOffer>(2).ToList();
            _all = _unexpected.Concat(_expected).ToList();

            //Create specification
            _specification = new VehicleOfferModelKeySpecification(modelKey);
        }

        [Fact]
        public void Ctor_Given_ModelKey_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelKey", () => new VehicleOfferModelKeySpecification(null as ModelKey));
        }

        [Fact]
        public void Ctor_Given_ModelKeySet_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelKeySet", () => new VehicleOfferModelKeySpecification(null as ModelKeySet));
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
