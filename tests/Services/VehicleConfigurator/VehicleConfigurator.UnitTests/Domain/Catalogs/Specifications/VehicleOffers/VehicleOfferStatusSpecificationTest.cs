﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.Specifications.VehicleOffers
{
    public class VehicleOfferStatusSpecificationTest
    {
        //Entities
        private readonly IReadOnlyList<VehicleOffer> _expected;
        private readonly IReadOnlyList<VehicleOffer> _unexpected;
        private readonly IReadOnlyList<VehicleOffer> _all;

        //Specification
        private readonly VehicleOfferStatusSpecification _specification;

        public VehicleOfferStatusSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Create entities
            _expected = fixture.CreateMany<VehicleOffer>(3).ToList();
            _unexpected = fixture.CreateMany<VehicleOffer>(2).ToList();
            _all = _unexpected.Concat(_expected).ToList();

            //Setup entities
            foreach (var vehicleOffer in _expected)
            {
                var colorId = fixture.Create<ColorId>();
                vehicleOffer.AddColorOffer(colorId, fixture.Create<decimal>());
                vehicleOffer.SetDefaultColor(colorId);
                vehicleOffer.Publish();
            }
            _unexpected.ToList().ForEach(e => e.Unpublish());

            //Create specification
            _specification = new VehicleOfferStatusSpecification(VehicleOfferStatus.Published);
        }

        [Fact]
        public void IsSatisfied_WhenIs_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_WhenIsNot_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void IsSatisfied_GivenEntity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("entity", () => _specification.IsSatisfiedBy(null));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _all.AsQueryable().Where(expression));
        }
    }
}
