﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class PackageServiceTest
    {
        private readonly IFixture _fixture;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IModelKeyValidator> _modelKeyValidator;

        //Service
        private readonly IPackageService _packageService;

        //Entity
        private readonly Package _package;
        private readonly ModelKeySet _coverage;
        private readonly decimal _price;

        public PackageServiceTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _modelKeyValidator = new Mock<IModelKeyValidator>();

            //Service

            _packageService = new PackageService(_work.Object, _modelKeyValidator.Object);

            //Entity
            _package = _fixture.Create<Package>();
            _package.ChangeStatus(PackageStatus.Draft);

            _coverage = new ModelKeySet(Enumerable.Range(1000, 1004)
                .Select(n => ModelKey.Parse($"{_package.ModelKeySet.ModelId}{n}")));

            _price = _fixture.Create<decimal>();

            //Setup dependencies
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            _repository.Setup(r => r.Require<Package, PackageId>(_package.Id))
                .ReturnsAsync(_package)
                .Verifiable();
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(PackageService)).HasNullGuard();
        }

        [Fact]
        public Task Add_WhenGivenPackage_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("package", () => _packageService.Add(null));
        }

        [Fact]
        public async Task Add()
        {
            var prNumbers = _package.Applicability
                .SelectMany(s => s.PrNumbers.Values);
            var optionIds = OptionId.Next(_package.ModelKeySet.ModelId, prNumbers);
            using (Sequence.Create())
            {
                //Arrange
                _modelKeyValidator.Setup(v => v.ValidateMany(It.Is<IEnumerable<ModelKey>>(m => m.SequenceEqual(_package.ModelKeySet.Values))))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _repository.Setup(r => r.HasManyRequired<Option, OptionId>(It.Is<IEnumerable<OptionId>>(o => o.SequenceEqual(optionIds))))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _repository.Setup(r => r.HasRequired<Catalog>(_package.CatalogId))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _repository.Setup(r => r.Has<Package, PackageId>(_package.Id))
                    .InSequence()
                    .ReturnsAsync(false);

                _repository.Setup(r => r.Add<Package, PackageId>(_package))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _packageService.Add(_package);
            }
        }

        [Fact]
        public async Task Add_WhenPackage_AlreadyExist_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Has<Package, PackageId>(_package.Id))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.ThrowsAsync<DuplicateEntityException>(() =>
                _packageService.Add(_package));

            //Assert
            Assert.Equal(typeof(Package), e.EntityType);
            Assert.Equal(_package.Id, e.Id);
        }

        //Change Status

        [Fact]
        public async Task ChangeStatus()
        {
            //Arrange
            _repository.Setup(r => r.Require<Package, PackageId>(_package.Id))
                .ReturnsAsync(_package)
                .Verifiable();

            Expression<Func<VehicleOffersViolatingPackageSpecification, bool>> specification = o =>
                o.Package == _package;

            _repository.Setup(s => s.Has(It.Is(specification), CancellationToken.None)).ReturnsAsync(false);

            //Act
            await _packageService.ChangeStatus(_package.Id, PackageStatus.Published);

            //Assert
            _repository.Verify();
            Assert.Equal(PackageStatus.Published, _package.Status);
        }
        
        [Fact]
        public async Task ChangeStatus_ToPublished_WhenOffers_DoesNotContains_PackageOptions_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Require<Package, PackageId>(_package.Id))
                .ReturnsAsync(_package)
                .Verifiable();

            Expression<Func<VehicleOffersViolatingPackageSpecification, bool>> specification = o =>
                o.Package == _package;

            _repository.Setup(s => s.Has(It.Is(specification), CancellationToken.None)).ReturnsAsync(true);

            //Assert
            var e = await Assert.ThrowsAsync<PackageRequirementViolationException>(() =>
                _packageService.ChangeStatus(_package.Id, PackageStatus.Published));

            Assert.Contains("option requirement was violated", e.Message);
        }

        //Update Price

        [Fact]
        public async Task UpdatePrice()
        {
            //Arrange
            _repository.Setup(r => r.Require<Package, PackageId>(_package.Id))
                .ReturnsAsync(_package)
                .Verifiable();

            //Act
            await _packageService.UpdatePrice(_package.Id, _price);

            //Assert
            _repository.Verify();
            Assert.Equal(_price, _package.Price);
        }

        //Update Coverage

        [Fact]
        public async Task UpdateCoverage()
        {
            //Arrange
            var newModelKeys = _package.ModelKeySet.Values.SkipLast(1)
                .Concat(_coverage.Values)
                .Distinct()
                .ToList();
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<Package, PackageId>(_package.Id))
                    .InSequence()
                    .ReturnsAsync(_package);

                _modelKeyValidator.Setup(v => v.ValidateMany(
                        It.Is<IEnumerable<ModelKey>>(m => m.SequenceEqual(_coverage.Values))))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _packageService.UpdateCoverage(_package.Id, new BoundModelKeySet(newModelKeys));
            }

            //Assert
            Assert.Equal(newModelKeys.OrderBy(m => m.Value), _package.ModelKeySet.Values);
        }

        [Fact]
        public async Task UpdateCoverage_WhenNewCoverage_IsSubsetOfOldCoverage_ModelKeyValidator_DoNotCall()
        {
            //Arrange
            var coverage = _package.ModelKeySet.Values
                .SkipLast(1)
                .ToList();

            //Act
            await _packageService.UpdateCoverage(_package.Id, new BoundModelKeySet(coverage));

            //Assert
            Assert.Equal(coverage, _package.ModelKeySet.Values);
            _modelKeyValidator.Verify(v => v.ValidateMany(It.IsAny<IEnumerable<ModelKey>>()), Times.Never);
        }

        [Fact]
        public Task UpdateCoverage_WhenPackageId_IsNull()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("packageId", () =>
                _packageService.UpdateCoverage(null, _coverage));
        }

        [Fact]
        public Task UpdateCoverage_WhenWith_IsNull()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("with", () =>
                _packageService.UpdateCoverage(_package.Id, null));
        }

        [Fact]
        public Task UpdateApplicability_WhenApplicability_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("applicability", () =>
                _packageService.UpdateApplicability(_package.Id, null));
        }

        [Fact]
        public async Task UpdateApplicability()
        {
            //Arrange
            var applicability = _fixture.Create<Applicability>();
            var optionIds = OptionId.Next(_package.ModelKeySet.ModelId, applicability.PrNumbers);

            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<Package, PackageId>(_package.Id))
                    .InSequence()
                    .ReturnsAsync(_package);

                _repository.Setup(r => r.HasManyRequired<Option, OptionId>(It.Is<IEnumerable<OptionId>>(o => o.SequenceEqual(optionIds))))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _packageService.UpdateApplicability(_package.Id, applicability);
            }

            //Assert
            Assert.Equal(_package.Applicability, applicability);
        }
    }
}
