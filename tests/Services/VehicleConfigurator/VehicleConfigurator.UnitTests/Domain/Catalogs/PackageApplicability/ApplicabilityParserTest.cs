﻿using AutoFixture;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.PackageApplicability
{
    public class ApplicabilityParserTest
    {
        private readonly Applicability _applicability;
        private readonly ApplicabilityParser _parser;

        public static IEnumerable<object[]> Invalid => new[]
            {
                "asd q1w2e3",
                "WW aaq/",
                " ",
                "#$@",
                "WWs aw1/",
                "WWs aw1/hh",
                "/WWs das/hhs",
            }
            .Select(s => new object[] {s});

        public ApplicabilityParserTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var required = fixture.CreateMany<RequiredSpecification>();
            var choices = fixture.CreateMany<ChoiceSpecification>().Cast<IApplicabilitySpecification>();

            _applicability = new Applicability(required.Concat(choices));

            _parser = new ApplicabilityParser();
        }
        
        [Fact]
        public void Parse()
        {
            //Act
            var result = _parser.Parse(_applicability.AsString);

            //Assert
            Assert.Equal(_applicability, result);
        }

        [Fact]
        public void Parse_GivenS_IsEmpty_ReturnsEmpty()
        {
            //Act
            var result = _parser.Parse(string.Empty);

            //Assert
            Assert.Empty(result);
        }

        [Fact]
        public void Parse_GivenS_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("s", () => _parser.Parse(null));
        }

        [Theory]
        [MemberData(nameof(Invalid))]
        public void Parse_GivenS_IsInvalid_Throws(string s)
        {
            Assert.Throws<FormatException>(() => _parser.Parse(s));
        }

        [Fact]
        public void TryParse()
        {
            //Act
            var result = _parser.TryParse(_applicability.AsString, out var expected);

            //Assert
            Assert.True(result);
            Assert.Equal(_applicability, expected);
        }

        [Fact]
        public void TryParse_GivenS_IsEmpty_ReturnsTrue()
        {
            //Act
            var result = _parser.TryParse(string.Empty, out var expected);

            //Assert
            Assert.True(result);
            Assert.Equal(new Applicability(), expected);
        }

        [Theory]
        [MemberData(nameof(Invalid))]
        public void TryParse_GivenS_IsInvalid_ReturnsFalse(string s)
        {
            //Act
            var result = _parser.TryParse(s, out var expected);

            //Assert
            Assert.False(result);
            Assert.Null(expected);
        }
    }
}
