﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.PackageApplicability
{
    public class RequiredSpecificationTest
    {
        private readonly IFixture _fixture;

        private readonly RequiredSpecification _specification;
        private readonly PrNumberSet _prNumberSet;

        public RequiredSpecificationTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            _prNumberSet = _fixture.Create<PrNumberSet>();
            _specification = _fixture.Create<RequiredSpecification>();
        }

        [Fact]
        public void Ctor()
        {
            //Arrange
            var prNumber = _fixture.Create<PrNumber>();

            //Act
            var specification = new RequiredSpecification(prNumber);

            //Assert
            Assert.Equal(ApplicabilitySpecificationType.Required, specification.Type);
            Assert.Equal(prNumber.Value, specification.AsString);
        }

        [Fact]
        public void IsSatisfied_WhenGivenPrNumberSet_ContainsRequiredPrNumber_ReturnsTrue()
        {
            //Arrange
            var prNumberSet = _prNumberSet.Expand(_specification.AsString);

            //Act
            var result = _specification.IsSatisfied(prNumberSet);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void IsSatisfied_WhenGivenPrNumberSet_NotContainsRequiredPrNumber_ReturnsFalse()
        {
            //Act
            var result = _specification.IsSatisfied(_prNumberSet);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var prNumber = _fixture.Create<PrNumber>();
            var specification = new RequiredSpecification(prNumber);

            //Assert
            Assert.Equal(prNumber.Value, specification.ToString());
        }
    }
}
