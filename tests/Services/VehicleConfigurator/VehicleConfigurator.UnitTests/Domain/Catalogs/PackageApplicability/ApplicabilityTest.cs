﻿using AutoFixture;
using Moq;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.PackageApplicability
{
    public class ApplicabilityTest
    {
        private readonly Fixture _fixture;

        public ApplicabilityTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());
        }

        [Fact]
        public void Ctor()
        {
            //Arrange
            var specifications = SetupSpecifications(true, true);

            //Act
            var applicability = new Applicability(specifications);

            //Assert
            Assert.Equal(specifications, applicability);
        }

        [Theory]
        [InlineData(true, true, true)]
        [InlineData(false, true, true)]
        [InlineData(true, false, true)]
        [InlineData(true, true, false)]
        public void IsSatisfied_GivenPrNumbers_SatisfyEachSpecification_ReturnsTrue(params bool[] areSatisfied)
        {
            //Arrange
            var expected = areSatisfied.All(s => s);
            var specifications = SetupSpecifications(areSatisfied);
            var prNumbers = new PrNumberSet(specifications.SelectMany(s => s.PrNumbers.Values));
            var applicability = new Applicability(specifications);

            //Act
            var actual = applicability.IsSatisfied(prNumbers);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void IsSatisfied_WhenSpecifications_AreNot_Set_ReturnsTrue()
        {
            //Arrange
            var prNumbers = _fixture.Create<PrNumberSet>();
            var applicability = new Applicability();

            //Act
            var result = applicability.IsSatisfied(prNumbers);

            //Assert
            Assert.True(result);
        }

        [Theory]
        [InlineData(true, true, true)]
        [InlineData(false, true, true)]
        [InlineData(true, false, true)]
        [InlineData(true, true, false)]
        [InlineData(true, false, false)]
        public void IsSatisfied_WithViolated_GivenPrNumbers_SatisfyEachSpecification_ReturnsTrue(params bool[] areSatisfied)
        {
            //Arrange
            var expected = areSatisfied.All(s => s);
            var specifications = SetupSpecifications(areSatisfied);
            var expectedViolated = Enumerable.Range(0, areSatisfied.Length)
                .Where(i => !areSatisfied[i])
                .Select(i => specifications[i]);

            var prNumbers = new PrNumberSet(specifications.SelectMany(s => s.PrNumbers.Values));
            var applicability = new Applicability(specifications);

            //Act
            var actual = applicability.IsSatisfied(prNumbers, out var violated);

            //Assert
            Assert.Equal(expected, actual);
            Assert.Equal(expectedViolated, violated);
        }

        [Fact]
        public void IsSatisfied_WithViolated_WhenSpecifications_AreNot_Set_ReturnsTrue()
        {
            //Arrange
            var prNumbers = _fixture.Create<PrNumberSet>();
            var applicability = new Applicability();

            //Act
            var result = applicability.IsSatisfied(prNumbers, out var violated);

            //Assert
            Assert.True(result);
            Assert.Empty(violated);
        }
        
        [Fact]
        public void Formatting()
        {
            //Arrange
            var specifications = SetupSpecifications(true, true);
            var expected = specifications.Select(s => s.AsString).Aggregate((a, b) => $"{a} {b}");
            var applicability = new Applicability(specifications);

            //Assert
            Assert.Equal(expected, applicability.ToString());
        }


        [Fact]
        public void Formatting_WhenSpecifications_AreNot_Set_ReturnsEmpty()
        {
            //Arrange
            var applicability = new Applicability();

            //Assert
            Assert.Empty(applicability.ToString());
        }

        [Fact]
        public void GetPrNumbers()
        {
            //Arrange
            var specifications = SetupSpecifications(true, true);
            var expected = new PrNumberSet(specifications.SelectMany(s => s.PrNumbers.Values));
            var applicability = new Applicability(specifications);

            //Assert
            Assert.Equal(expected, applicability.PrNumbers);
        }

        [Fact]
        public void GetPrNumbers_Returns_WithoutDuplicates()
        {
            //Arrange
            var prNumbers = _fixture.Create<PrNumberSet>();
            var unique = SetupSpecification(prNumbers, true);
            var copy = SetupSpecification(prNumbers, true);
            var applicability = new Applicability(unique, copy);

            //Assert
            Assert.Equal(prNumbers, applicability.PrNumbers);
        }

        private List<IApplicabilitySpecification> SetupSpecifications(params bool[] areSatisfied)
        {
            return areSatisfied
                .Select((t, i) => SetupSpecification(_fixture.Create<PrNumberSet>(), areSatisfied[i]))
                .ToList();
        }

        private IApplicabilitySpecification SetupSpecification(PrNumberSet prNumbers, bool isSatisfied)
        {
            var mock = new Mock<IApplicabilitySpecification>();

            mock.Setup(m => m.AsString)
                .Returns(prNumbers.ToString);
            mock.Setup(m => m.PrNumbers)
                .Returns(prNumbers);
            mock.Setup(m => m.IsSatisfied(It.Is<PrNumberSet>(n => n.Has(prNumbers))))
                .Returns(isSatisfied);

            return mock.Object;
        }
    }
}
