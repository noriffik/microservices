﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs.PackageApplicability
{
    public class ChoiceSpecificationTest
    {
        private readonly IFixture _fixture;

        private readonly PrNumberSet _specificationPrNumbers;

        public ChoiceSpecificationTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            _specificationPrNumbers = _fixture.Create<PrNumberSet>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var specification = new ChoiceSpecification(_specificationPrNumbers);

            //Assert
            Assert.Equal(ApplicabilitySpecificationType.Choice, specification.Type);
            Assert.Equal(_specificationPrNumbers.Values.Select(v => v.Value).Aggregate((a, b) => $"{a}/{b}"), specification.AsString);
        }

        [Fact]
        public void IsSatisfied_WhenGivenPrNumberSet_ContainsRequiredPrNumber_ReturnsTrue()
        {
            //Arrange
            var prNumber = _fixture.Create<PrNumber>();
            var specification = new ChoiceSpecification(_specificationPrNumbers.Expand(prNumber));
            var prNumberSet = _fixture.Create<PrNumberSet>().Expand(prNumber);

            //Act
            var result = specification.IsSatisfied(prNumberSet);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void IsSatisfied_WhenGivenPrNumberSet_NotContainsRequiredPrNumber_ReturnsFalse()
        {
            //Arrange
            var specification = new ChoiceSpecification(_specificationPrNumbers);
            var prNumberSet = _fixture.Create<PrNumberSet>();

            //Act
            var result = specification.IsSatisfied(prNumberSet);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var specification = new ChoiceSpecification(_specificationPrNumbers);
            var expected = _specificationPrNumbers.Values.Select(v => v.Value).Aggregate((a, b) => $"{a}/{b}");

            //Assert
            Assert.Equal(expected, specification.ToString());
        }
    }
}
