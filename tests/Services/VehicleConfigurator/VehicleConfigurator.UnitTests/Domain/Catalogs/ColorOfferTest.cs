﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class ColorOfferTest
    {
        private readonly IFixture _fixture;

        private readonly ColorId _colorId;
        private readonly decimal _price;
        private readonly ColorOffer _colorOffer;

        public ColorOfferTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            _colorId = _fixture.Create<ColorId>();
            _price = _fixture.Create<decimal>();
            _colorOffer = new ColorOffer(_colorId, _price);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Equal(_colorId, _colorOffer.ColorId);
            Assert.Equal(_price, _colorOffer.Price);
            Assert.Equal(Relevance.Relevant, _colorOffer.Relevance);
        }

        [Fact]
        public void ChangeRelevance()
        {
            //Act
            _colorOffer.ChangeRelevance(Relevance.Irrelevant);

            //Assert
            Assert.Equal(Relevance.Irrelevant, _colorOffer.Relevance);
        }

        [Fact]
        public void ChangeRelevancePeriod()
        {
            //Arrange
            var period = new RelevancePeriod(new DateTime(2019, 5, 5), new DateTime(2019, 6, 6));

            //Act
            _colorOffer.ChangeRelevancePeriod(period);

            //Assert
            Assert.Equal(period.From, _colorOffer.RelevancePeriodFrom);
            Assert.Equal(period.To, _colorOffer.RelevancePeriodTo);
            Assert.Equal(period, _colorOffer.RelevancePeriod);
        }

        [Fact]
        public void ChangeRelevancePeriod_GivenPeriod_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("relevancePeriod", () => _colorOffer.ChangeRelevancePeriod(null));
        }

        [Fact]
        public void ChangePrice()
        {
            //Arrange
            var price = _fixture.Create<decimal>();

            //Act
            _colorOffer.ChangePrice(price);

            //Assert
            Assert.Equal(price, _colorOffer.Price);
        }
    }
}
