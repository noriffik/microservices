﻿using AutoFixture;
using Moq;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.UnitTests.Extensions.Domain;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class PackageTest
    {
        private readonly BoundModelKeySet _modelKeys;
        private readonly Applicability _applicability;
        private readonly string _name;
        private readonly int _catalogId;
        private readonly PackageId _id;
        private readonly Package _package;

        private readonly IFixture _fixture;

        public PackageTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            _name = _fixture.Create<string>();
            _catalogId = _fixture.Create<int>();
            _modelKeys = _fixture.Create<BoundModelKeySet>();
            _id = PackageId.Next(_modelKeys.ModelId, _fixture.Create<PackageCode>());

            //Setup applicability
            _applicability = _fixture.Create<Applicability>();

            _package = new Package(_id, _catalogId, _name, _modelKeys, _applicability);
        }

        [Fact]
        public void CtorEmpty()
        {
            //Act
            var package = new Package(_id, _catalogId, _name);

            //Assert
            Assert.Equal(_id, package.Id);
            Assert.Equal(_catalogId, package.CatalogId);
            Assert.Equal(_name, package.Name);
            Assert.NotNull(package.ModelKeySet);
            Assert.Empty(package.ModelKeySet.Values);
            Assert.Equal(_id.ModelId, package.ModelKeySet.ModelId);
            Assert.Empty(package.Applicability);
        }

        [Fact]
        public void CtorEmpty_GivenPackageId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("id", () => new Package(null, _catalogId, _name));
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var package = new Package(_id, _catalogId, _name, _modelKeys, _applicability);

            //Assert
            Assert.Equal(_id, package.Id);
            Assert.Equal(_catalogId, package.CatalogId);
            Assert.Equal(_name, package.Name);
            Assert.Equal(_modelKeys, package.ModelKeySet);
            Assert.Equal(_applicability, package.Applicability);
        }

        [Fact]
        public void Ctor_GivenId_IsDifferentModel_ToModelKeySet_Throws()
        {
            //Arrange
            var id = PackageId.Parse("AA000");
            var modelKeySet = BoundModelKeySet.Parse("BB0000");

            //Act
            var e = Assert.Throws<SingleModelViolationException>(
                () => new Package(id, _catalogId, _name, modelKeySet, _applicability));

            //Assert
            Assert.Contains("model keys are in conflict", e.Message.ToLowerInvariant());
        }

        [Fact]
        public void Ctor_GivenModelKeySet_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelKeySet",
                () => new Package(_id, _catalogId, _name, null, _applicability));
        }

        [Fact]
        public void Ctor_GivenApplicability_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("applicability",
                () => new Package(_id, _catalogId, _name, _modelKeys, null));
        }

        [Fact]
        public void Price()
        {
            //Arrange
            var price = _fixture.Create<decimal>();

            //Act
            _package.Price = price;

            //Assert
            Assert.Equal(price, _package.Price);
        }

        [Fact]
        public void IsApplicableTo()
        {
            //Act
            var result = _package.IsApplicableTo(_package.ApplicableWith());

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void IsApplicableTo_WhenGivenPrNumbers_IsNotApplicable_ReturnsFalse()
        {
            //Arrange
            var prNumbers = _fixture.Create<PrNumberSet>();

            //Act
            var result = _package.IsApplicableTo(prNumbers);

            //Assert
            Assert.False(result);
        }

        //Update ModelKeySet

        [Fact]
        public void UpdateCoverage_GivenWith_IsModelKeySet()
        {
            //Arrange
            var modelKeySet = new Mock<IModelKeySet>();
            var modelKey = ModelKey.Parse(_package.ModelKeySet.Values.First().ModelId + "9876");
            modelKeySet.Setup(m => m.Values).Returns(new[] { modelKey });
            modelKeySet.Setup(m => m.AsString).Returns(modelKey.Value);
            var expectedEvent = new PackageCoverageUpdatedEvent(_package.Id);

            //Act
            _package.UpdateCoverage(modelKeySet.Object);

            //Assert
            Assert.Equal(modelKey, _package.ModelKeySet.Values.Single());
            Assert.Contains(expectedEvent, _package.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void UpdateCoverage_GivenWith_HaveDifferentModelId_Throws()
        {
            //Arrange
            var modelKeySet = new Mock<IModelKeySet>();
            var modelKey = ModelKey.Parse("1X2X3X");
            modelKeySet.Setup(m => m.Values).Returns(new[] { modelKey });
            modelKeySet.Setup(m => m.AsString).Returns(modelKey.Value);

            //Assert
            var e = Assert.Throws<SingleModelViolationException>(() =>
                _package.UpdateCoverage(modelKeySet.Object));
            Assert.Contains($"for {_package.ModelKeySet.Values.First().ModelId} model", e.Message.ToLowerInvariant());
        }

        [Fact]
        public void UpdateCoverage_GivenWith_IsString()
        {
            //Arrange
            var expectedEvent = new PackageCoverageUpdatedEvent(_package.Id);
            var modelKey = ModelKey.Parse(_package.ModelKeySet.Values.First().ModelId + "9876");

            //Act
            _package.UpdateCoverage(modelKey.Value);

            //Assert
            Assert.Equal(modelKey, _package.ModelKeySet.Values.Single());
            Assert.Contains(expectedEvent, _package.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void UpdateCoverage_GivenWith_IsIEnumerable()
        {
            //Arrange
            var expectedEvent = new PackageCoverageUpdatedEvent(_package.Id);
            var modelKey = ModelKey.Parse(_package.ModelKeySet.Values.First().ModelId + "9876");

            //Act
            _package.UpdateCoverage(new[] { modelKey });

            //Assert
            Assert.Equal(modelKey, _package.ModelKeySet.Values.Single());
            Assert.Contains(expectedEvent, _package.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void UpdateApplicability()
        {
            //Arrange
            var applicability = new Mock<Applicability>().Object;
            var expectedEvent = new PackageApplicabilityUpdatedEvent(_package.Id);

            //Act
            _package.UpdateApplicability(applicability);

            //Assert
            Assert.Equal(_package.Applicability, applicability);
            Assert.Contains(expectedEvent, _package.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void UpdateApplicability_WhenGivenApplicability_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("applicability", () => _package.UpdateApplicability(null));
        }

        [Fact]
        public void ChangeStatus_ToPublish()
        {
            //Act
            _package.ChangeStatus(PackageStatus.Published);

            //Assert
            Assert.Equal(PackageStatus.Published, _package.Status);
        }

        [Fact]
        public void ChangeStatus_ToDraft()
        {
            //Arrange
            var expectedEvent = new PackageMarkedAsDraftEvent(_package.Id);
            _package.ChangeStatus(PackageStatus.Published);

            //Act
            _package.ChangeStatus(PackageStatus.Draft);

            //Assert
            Assert.Equal(PackageStatus.Draft, _package.Status);
            Assert.Contains(expectedEvent, _package.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void ChangeStatus_ToDraft_WhenPackage_IsAlreadyDraftState_DoesNothing()
        {
            //Arrange
            var notExpectedEvent = new PackageMarkedAsDraftEvent(_package.Id);

            //Act
            _package.ChangeStatus(PackageStatus.Draft);

            //Assert
            Assert.Equal(PackageStatus.Draft, _package.Status);
            Assert.DoesNotContain(notExpectedEvent, _package.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }
    }
}
