﻿using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class PackageCodeTest
    {
        [Theory]
        [InlineData("NF1")]
        [InlineData("3V3")]
        [InlineData("NHX")]
        [InlineData("3v5")]
        public void Parse(string value)
        {
            //Act
            var packageCode = PackageCode.Parse(value);

            //Assert
            Assert.NotNull(packageCode);
            Assert.Equal(value.ToUpperInvariant(), packageCode.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                PackageCode.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("/N1")]
        [InlineData("F%1")]
        [InlineData("NF1X")]
        [InlineData("NF")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>
            {
                PackageCode.Parse(value);
            });
        }

        [Theory]
        [InlineData("NF1")]
        [InlineData("3V3")]
        [InlineData("NHX")]
        [InlineData("3v5")]
        public void TryParse(string value)
        {
            //Act
            var result = PackageCode.TryParse(value, out var packageCode);

            //Assert
            Assert.True(result);
            Assert.NotNull(packageCode);
            Assert.Equal(value.ToUpperInvariant(), packageCode.Value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("/N1")]
        [InlineData("F%1")]
        [InlineData("NF1X")]
        [InlineData("NF")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = PackageCode.TryParse(value, out var packageCode);

            //Assert
            Assert.False(result);
            Assert.Null(packageCode);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var packageId = PackageCode.Parse("WU1");

            //Assert
            Assert.Equal(packageId.Value, packageId.ToString());
        }
    }
}
