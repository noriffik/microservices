﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class RelevancePeriodTest
    {
        public static IEnumerable<object[]> ValidCtorParameters => new List<object[]>
        {
            new object[]{ new DateTime(2019, 05, 19, 5, 5, 5), new DateTime(2019, 05, 25, 5, 5, 5) },
            new object[]{ new DateTime(2019, 05, 19, 5, 5, 5), null }
        };

        [Theory]
        [MemberData(nameof(ValidCtorParameters))]
        public void Ctor_WithFromAndTo_WithValidParameters(DateTime from, DateTime? to)
        {
            //Act
            var result = new RelevancePeriod(from, to);

            //Assert
            Assert.Equal(from.Date, result.From.GetValueOrDefault());
            Assert.Equal(to?.Date, result.To?.Date);
        }

        public static IEnumerable<object[]> InvalidCtorParameters => new List<object[]>
        {
            new object[]{ new DateTime(2019, 05, 19, 5, 5, 5), new DateTime(2019, 05, 19, 5, 5, 5) },
            new object[]{ new DateTime(2019, 05, 19, 5, 5, 5), new DateTime(2019, 05, 1, 5, 5, 5) }
        };

        [Theory]
        [MemberData(nameof(InvalidCtorParameters))]
        public void Ctor_WithFromAndTo_WithInvalidParameters_Throws(DateTime from, DateTime? to)
        {
            Assert.Throws<InvalidPeriodException>(() => new RelevancePeriod(from, to));
        }

        [Fact]
        public void Ctor_WithFrom()
        {
            //Arrange
            var from = new DateTime(2019, 05, 22, 5, 5, 5);

            //Act
            var result = new RelevancePeriod(from);

            //Assert
            Assert.Equal(from.Date, result.From.GetValueOrDefault());
            Assert.Null(result.To);
        }

        [Fact]
        public void Empty()
        {
            //Act
            var result = RelevancePeriod.Empty;

            //Assert
            Assert.Null(result.From);
            Assert.Null(result.To);
        }

        [Fact]
        public void IsIn_WhenPeriod_IsEmpty_ReturnsTrue()
        {
            //Arrange
            var date = new DateTime(2019, 05, 22, 5, 5, 5);
            var period = RelevancePeriod.Empty;

            //Act
            var result = period.IsIn(date);

            //Assert
            Assert.True(result);
        }

        public static IEnumerable<object[]> IsInTestData => new List<object[]>
        {
            new object[]{ new DateTime(2019, 05, 10, 5, 5, 5), false},
            new object[]{ new DateTime(2019, 05, 22, 5, 5, 5), true},
            new object[]{ new DateTime(2019, 05, 25, 5, 5, 5), true}
        };

        [Theory]
        [MemberData(nameof(IsInTestData))]
        public void IsIn_WhenToDate_IsNull(DateTime date, bool expected)
        {
            //Arrange
            var from = new DateTime(2019, 05, 22, 5, 5, 5);
            var period = new RelevancePeriod(from);

            //Act
            var result = period.IsIn(date);

            //Assert
            Assert.Equal(expected, result);
        }

        [Theory]
        [MemberData(nameof(IsInTestData))]
        public void IsIn_WhenToDate_HasValue(DateTime date, bool expected)
        {
            //Arrange
            var from = new DateTime(2019, 05, 22, 5, 5, 5);
            var to = new DateTime(2019, 05, 30, 5, 5, 5);
            var period = new RelevancePeriod(from, to);

            //Act
            var result = period.IsIn(date);

            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void IsIn_GivenDate_IsAfter_ToDate()
        {
            //Arrange
            var from = new DateTime(2019, 05, 22, 5, 5, 5);
            var to = new DateTime(2019, 05, 30, 5, 5, 5);
            var period = new RelevancePeriod(from, to);
            var date = new DateTime(2019, 06, 30, 5, 5, 5);

            //Act
            var result = period.IsIn(date);

            //Assert
            Assert.False(result);
        }

        public class TestPeriod : PeriodBase
        {
            private TestPeriod() : base(PeriodValidator.NoneRequired)
            {
            }

            public TestPeriod(DateTime? from, DateTime? to) : base(PeriodValidator.NoneRequired, from, to)
            {
            }
        }

        public static IEnumerable<object[]> IsInBoundsTestData => new List<object[]>
        {
            new object[]{ new RelevancePeriod(new DateTime(2019, 05, 22)),
                new TestPeriod(null, null), true},
            new object[]{ new RelevancePeriod(new DateTime(2019, 05, 22)),
                new TestPeriod(new DateTime(2019, 05, 20), null), true},

            new object[]{ new RelevancePeriod(new DateTime(2019, 05, 22), new DateTime(2019, 05, 28)),
                new TestPeriod(new DateTime(2019, 05, 20), null), true},
            new object[]{ new RelevancePeriod(new DateTime(2019, 05, 22), new DateTime(2019, 05, 28)),
                new TestPeriod(new DateTime(2019, 05, 20), new DateTime(2019, 05, 30)), true},

            new object[]{ new RelevancePeriod(new DateTime(2019, 05, 20)),
                new TestPeriod(new DateTime(2019, 05, 20), null), true},
            new object[]{ new RelevancePeriod(new DateTime(2019, 05, 22), new DateTime(2019, 05, 28)),
                new TestPeriod(new DateTime(2019, 05, 20), new DateTime(2019, 05, 28)), true},

            new object[]{ new RelevancePeriod(new DateTime(2019, 05, 20)),
                new TestPeriod(new DateTime(2019, 05, 22), null), false},
            new object[]{ new RelevancePeriod(new DateTime(2019, 05, 22), new DateTime(2019, 05, 28)),
                new TestPeriod(new DateTime(2019, 05, 20), new DateTime(2019, 05, 27)), false}
        };

        [Theory]
        [MemberData(nameof(IsInBoundsTestData))]
        public void IsInBounds(RelevancePeriod relevancePeriod, TestPeriod periodBase, bool expected)
        {
            //Act
            var actual = relevancePeriod.IsInBounds(periodBase);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void VerifyThatIsInBounds()
        {
            //Arrange
            var year = new Year(2019);
            var catalog = new Catalog(year, new Period(year));
            var relevancePeriod = new RelevancePeriod(catalog.RelevancePeriodFrom.AddDays(5));

            //Act
            relevancePeriod.VerifyThatIsInBounds(catalog);

            //Assert
            Assert.True(true);
        }

        [Fact]
        public void VerifyThatIsInBounds_WhenIsNot_Throws()
        {
            //Arrange
            var year = new Year(2019);
            var catalog = new Catalog(year, new Period(year));
            var relevancePeriod = new RelevancePeriod(catalog.RelevancePeriodFrom.AddDays(-5));

            //Act
            Assert.Throws<RelevancePeriodExceedsCatalogLifetimeException>(() =>
                relevancePeriod.VerifyThatIsInBounds(catalog));
        }
    }
}
