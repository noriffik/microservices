﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class VehicleOfferServiceTest
    {
        private const int VehicleOfferId = 1;

        //Entity
        private readonly VehicleOffer _vehicleOffer;
        private readonly ColorId _colorId;
        private readonly decimal _colorPrice;
        private readonly Catalog _catalog;

        //Dependencies
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IModelKeyValidator> _modelKeyValidator;
        private readonly Mock<IUnitOfWork> _work;

        //Service
        private readonly IVehicleOfferService _service;

        public VehicleOfferServiceTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _vehicleOffer = fixture.Create<VehicleOffer>();
            _colorId = fixture.Create<ColorId>();
            _colorPrice = fixture.Create<decimal>();
            _catalog = fixture.Construct<Catalog>(_vehicleOffer.CatalogId);

            //Dependencies
            _modelKeyValidator = new Mock<IModelKeyValidator>();
            _repository = new Mock<IEntityRepository>();
            _work = new Mock<IUnitOfWork>();

            //Service
            _service = new VehicleOfferService(_work.Object, _modelKeyValidator.Object);

            //Setup dependencies
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOffer.Id)).ReturnsAsync(_vehicleOffer);
        }

        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(VehicleOfferService));
        }

        [Fact]
        public async Task Add()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Has(It.Is<VehicleOfferForVehicleSpecification>(s =>
                        s.ModelKey == _vehicleOffer.ModelKey && s.CatalogId == _vehicleOffer.CatalogId)))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.HasRequired<Catalog>(_vehicleOffer.CatalogId))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _modelKeyValidator.Setup(v => v.Validate(_vehicleOffer.ModelKey))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.Add(_vehicleOffer))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _service.Add(_vehicleOffer);
            }
        }

        [Fact]
        public Task Add_GivenVehicleOffer_IsNull()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("vehicleOffer", () =>
                _service.Add(null));
        }

        [Fact]
        public async Task Add_GivenModelKey_IsAlreadyInCatalog_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Has(It.Is<VehicleOfferForVehicleSpecification>(s =>
                s.ModelKey == _vehicleOffer.ModelKey && s.CatalogId == _vehicleOffer.CatalogId)))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.ThrowsAsync<DuplicateVehicleOfferException>(() => _service.Add(_vehicleOffer));

            //Assert
            Assert.Equal(_vehicleOffer.CatalogId, e.CatalogId);
            Assert.Equal(_vehicleOffer.ModelKey, e.ModelKey);
        }

        [Fact]
        public async Task AddColorOffer()
        {
            //Arrange
            _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOffer.Id))
                .ReturnsAsync(_vehicleOffer)
                .Verifiable();

            //Act
            await _service.AddColorOffer(_vehicleOffer.Id, _colorId, _colorPrice);

            //Assert
            _vehicleOffer.HasColorOffer(_colorId);
            _repository.Verify();
        }

        [Fact]
        public void ChangeStatus_WhenGivenStatus_IsPublished()
        {
            //Arrange
            var mock = new Mock<VehicleOffer>(
                _vehicleOffer.ModelKey, _vehicleOffer.CatalogId);

            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<VehicleOffer>(VehicleOfferId))
                    .InSequence()
                    .ReturnsAsync(mock.Object);
                mock.Setup(m => m.Publish())
                    .InSequence();

                //Act
                _service.ChangeStatus(VehicleOfferId, VehicleOfferStatus.Published);
            }
        }

        [Fact]
        public void ChangeStatus_WhenGivenStatus_IsDraft()
        {
            //Arrange
            var mock = new Mock<VehicleOffer>(
                _vehicleOffer.ModelKey, _vehicleOffer.CatalogId);

            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<VehicleOffer>(VehicleOfferId))
                    .InSequence()
                    .ReturnsAsync(mock.Object);
                mock.Setup(m => m.Unpublish())
                    .InSequence();

                //Act
                _service.ChangeStatus(VehicleOfferId, VehicleOfferStatus.Draft);
            }
        }

        [Fact]
        public void SetDefaultColor()
        {
            //Arrange
            var mock = new Mock<VehicleOffer>(_vehicleOffer.ModelKey, _vehicleOffer.CatalogId);

            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOffer.Id))
                    .InSequence()
                    .ReturnsAsync(mock.Object);
                mock.Setup(m => m.SetDefaultColor(_colorId))
                    .InSequence();

                //Act
                _service.SetDefaultColor(_vehicleOffer.Id, _colorId);
            }
        }

        [Fact]
        public Task ChangeColorOfferRelevancePeriod_GivenColorId_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("colorId",
                () => _service.ChangeColorOfferRelevancePeriod(_vehicleOffer.Id, null, RelevancePeriod.Empty));
        }

        [Fact]
        public Task ChangeColorOfferRelevancePeriod_GivenRelevancePeriod_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("relevancePeriod",
                () => _service.ChangeColorOfferRelevancePeriod(_vehicleOffer.Id, _colorId, null));
        }

        [Fact]
        public async Task ChangeColorRelevancePeriod()
        {
            //Arrange
            var vehicleOffer = new Mock<VehicleOffer>(_vehicleOffer.ModelKey, _vehicleOffer.CatalogId);
            var relevancePeriod = new RelevancePeriod(_catalog.RelevancePeriodFrom.AddDays(1));

            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOffer.Id))
                    .InSequence()
                    .ReturnsAsync(vehicleOffer.Object);
                _repository.Setup(r => r.Require<Catalog>(_catalog.Id))
                    .InSequence()
                    .ReturnsAsync(_catalog);
                vehicleOffer.Setup(v => v.ChangeColorOfferRelevancePeriod(_colorId, relevancePeriod))
                    .InSequence();

                //Act
                await _service.ChangeColorOfferRelevancePeriod(_vehicleOffer.Id, _colorId, relevancePeriod);
            }
        }

        [Fact]
        public Task ChangeColorRelevancePeriod_WhenOptionOfferPeriod_ExceedsCatalogLifetime_Throws()
        {
            //Arrange
            var vehicleOffer = new Mock<VehicleOffer>(_vehicleOffer.ModelKey, _vehicleOffer.CatalogId);
            var relevancePeriod = new RelevancePeriod(_catalog.RelevancePeriodFrom.AddDays(-1));

            _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOffer.Id))
                .ReturnsAsync(vehicleOffer.Object);
            _repository.Setup(r => r.Require<Catalog>(_catalog.Id))
                .ReturnsAsync(_catalog);

            //Act
            return Assert.ThrowsAsync<RelevancePeriodExceedsCatalogLifetimeException>(() =>
                _service.ChangeColorOfferRelevancePeriod(_vehicleOffer.Id, _colorId, relevancePeriod));
        }

        [Fact]
        public async Task ChangeColorOfferPrice()
        {
            //Arrange
            var vehicleOffer = new Mock<VehicleOffer>(_vehicleOffer.ModelKey, _vehicleOffer.CatalogId);

            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOffer.Id))
                    .InSequence()
                    .ReturnsAsync(vehicleOffer.Object);
                vehicleOffer.Setup(v => v.ChangeColorOfferPrice(_colorId, _colorPrice))
                    .InSequence();

                //Act
                await _service.ChangeColorOfferPrice(_vehicleOffer.Id, _colorId, _colorPrice);
            }
        }

        [Fact]
        public Task ChangeColorOfferPrice_GivenColorId_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("colorId", () =>
                _service.ChangeColorOfferPrice(_vehicleOffer.Id, null, _colorPrice));
        }

        [Fact]
        public async Task ChangeRelevancePeriod()
        {
            using (Sequence.Create())
            {
                //Arrange
                var from = _catalog.RelevancePeriodFrom.AddDays(1);
                var to = from.AddDays(2);
                var period = new RelevancePeriod(from, to);

                _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOffer.Id))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer);
                _repository.Setup(r => r.Require(It.IsAny<RelevantPublishedCatalogSpecification>()))
                    .InSequence()
                    .ReturnsAsync(_catalog);

                //Act
                await _service.ChangeRelevancePeriod(_vehicleOffer.Id, period);

                //Assert
                Assert.Equal(period, _vehicleOffer.RelevancePeriod, PropertyComparer<RelevancePeriod>.Instance);
            }
        }
    }
}
