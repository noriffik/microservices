﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class OptionOffersAffectedByRestrictionTest
    {
        private readonly int _catalogId;
        private readonly ModelKey _modelKey;
        private readonly PrNumberSet _prNumbers;

        public OptionOffersAffectedByRestrictionTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _catalogId = fixture.Create<int>();
            _modelKey = fixture.Create<ModelKey>();
            _prNumbers = fixture.Create<PrNumberSet>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var result = new OptionOffersAffectedByRestriction(_catalogId, _modelKey, _prNumbers);

            //Assert
            Assert.Equal(_catalogId, result.CatalogId);
            Assert.Equal(_modelKey, result.ModelKey);
            Assert.Equal(_prNumbers, result.PrNumbers);
        }

        [Fact]
        public void Ctor_GivenModelKey_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelKey",
                () => new OptionOffersAffectedByRestriction(_catalogId, null, _prNumbers));
        }

        [Fact]
        public void Ctor_GivenPrNumbers_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumbers",
                () => new OptionOffersAffectedByRestriction(_catalogId, _modelKey, null));
        }
    }
}
