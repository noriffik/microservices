﻿using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class AvailabilityTest
    {
        private readonly decimal _price = new decimal(5.15);
        
        [Theory]
        [InlineData(ReasonType.NationalStandard)]
        [InlineData(ReasonType.SerialEquipment)]
        public void Included(ReasonType reasonType)
        {
            //Act
            var availability = Availability.Included(reasonType);

            //Arrange
            Assert.Equal(reasonType, availability.Reason);
            Assert.Equal(AvailabilityType.Included, availability.Type);
            Assert.Equal(0, availability.Price);
        }

        [Fact]
        public void Purchasable()
        {
            //Act
            var availability = Availability.Purchasable(_price);

            //Arrange
            Assert.Null(availability.Reason);
            Assert.Equal(AvailabilityType.Purchasable, availability.Type);
            Assert.Equal(_price, availability.Price);
        }

        [Fact]
        public void Ctor_GivenPrice_IsNegative_Throws()
        {
            var e = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var unused = Availability.Purchasable(-5);
            });
            Assert.Equal("price", e.ParamName);
        }
    }
}
