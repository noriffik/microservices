﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class CatalogServiceTest
    {
        //Entity
        private readonly Catalog _catalog;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Service
        private readonly ICatalogService _service;

        public CatalogServiceTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new DomainBuilder());

            //Entity
            _catalog = fixture.Create<Catalog>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Service
            _service = new CatalogService(_work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(CatalogService)).HasNullGuard();
        }

        [Fact]
        public void Add()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Has(It.Is<CatalogOverlappedPeriodAndModelYearSpecification>(s =>
                        s.ModelYear == _catalog.ModelYear && s.Period == _catalog.RelevancePeriod)))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Add(_catalog))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                _service.Add(_catalog);
            }
        }

        [Fact]
        public async Task Add_WhenPeriod_IsInvalid_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Has(It.Is<CatalogOverlappedPeriodAndModelYearSpecification>(s =>
                    s.ModelYear == _catalog.ModelYear && s.Period == _catalog.RelevancePeriod)))
                .ReturnsAsync(true);

            //Assert
            await Assert.ThrowsAsync<DuplicateCatalogException>(() => _service.Add(_catalog));
        }

        [Fact]
        public void ChangeStatus()
        {
            //Arrange
            _repository.Setup(r => r.Require<Catalog>(_catalog.Id))
                .ReturnsAsync(_catalog)
                .Verifiable();

            //Act
            _service.ChangeStatus(_catalog.Id, CatalogStatus.Published);

            //Assert
            Assert.Equal(_catalog.Status, CatalogStatus.Published);
            _repository.Verify();
        }

        [Fact]
        public void ChangeRelevancePeriodEnding()
        {
            //Arrange
            var periodEnd = DateTime.MaxValue.Date;
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<Catalog>(_catalog.Id))
                    .InSequence()
                    .ReturnsAsync(_catalog);
                _repository.Setup(r => r.Has(It.Is<CatalogOverlappedPeriodAndModelYearSpecification>(s =>
                        s.ModelYear == _catalog.ModelYear && s.Period == new Period(_catalog.RelevancePeriodTo.Value.AddDays(1), periodEnd))))
                    .InSequence()
                    .ReturnsAsync(false);

                //Act
                _service.ChangeRelevancePeriodEnding(_catalog.Id, DateTime.MaxValue);

                //Assert
                Assert.Equal(periodEnd, _catalog.RelevancePeriod.To);
            }
        }

        [Fact]
        public async Task ChangeRelevancePeriodEnding_WhenPeriod_IsInvalid_Trows()
        {
            //Arrange
            _repository.Setup(r => r.Require<Catalog>(_catalog.Id))
                .ReturnsAsync(_catalog);
            _repository.Setup(r => r.Has(It.IsAny<CatalogOverlappedPeriodAndModelYearSpecification>()))
                .ReturnsAsync(true);

            //Act
            var result = await Assert.ThrowsAsync<DuplicateCatalogException>(() =>
                _service.ChangeRelevancePeriodEnding(_catalog.Id, DateTime.MaxValue));

            //Assert
            Assert.Contains("for given model year and relevance period", result.Message);
        }

        [Fact]
        public void ChangeRelevancePeriod()
        {
            //Arrange
            var periodStart = DateTime.MinValue.Date;
            var periodEnd = DateTime.MaxValue.Date;
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<Catalog>(_catalog.Id))
                    .InSequence()
                    .ReturnsAsync(_catalog);
                _repository.Setup(r => r.Has(It.Is<CatalogOverlappedPeriodAndModelYearSpecification>(s =>
                        s.ModelYear == _catalog.ModelYear && s.Period == new Period(_catalog.RelevancePeriodTo.Value.AddDays(1), periodEnd))))
                    .InSequence()
                    .ReturnsAsync(false);

                //Act
                _service.ChangeRelevancePeriod(_catalog.Id, new RelevancePeriod(periodStart, periodEnd));

                //Assert
                Assert.Equal(periodStart, _catalog.RelevancePeriod.From);
                Assert.Equal(periodEnd, _catalog.RelevancePeriod.To);
            }
        }

        [Fact]
        public async Task ChangeRelevancePeriod_WhenPeriod_IsInvalid_Trows()
        {
            //Arrange
            _repository.Setup(r => r.Require<Catalog>(_catalog.Id))
                .ReturnsAsync(_catalog);
            _repository.Setup(r => r.Has(It.IsAny<CatalogOverlappedPeriodAndModelYearSpecification>()))
                .ReturnsAsync(true);

            //Act
            var result = await Assert.ThrowsAsync<DuplicateCatalogException>(() =>
                _service.ChangeRelevancePeriod(_catalog.Id, new RelevancePeriod(DateTime.MinValue, DateTime.MaxValue)));

            //Assert
            Assert.Contains("for given model year and relevance period", result.Message);
        }
    }
}
