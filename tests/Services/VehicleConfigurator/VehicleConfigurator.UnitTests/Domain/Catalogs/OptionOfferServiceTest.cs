﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Rules;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class OptionOfferServiceTest
    {
        private const int VehicleOfferId = 1;
        private const string RuleSet = "valid";

        //Entity
        private readonly VehicleOffer _vehicleOffer;

        private readonly OptionId _optionId;
        private readonly Availability _availability;
        private readonly IList<VehicleOffer> _vehicleOffers;
        private readonly BoundModelKeyDictionary<Availability> _optionOffers;
        private readonly Catalog _catalog;

        //Dependencies
        private readonly Mock<IModelKeyValidator> _modelKeyValidator;
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IRuleParser> _ruleParser;
        private readonly Mock<IRuleValidationService> _ruleValidationService;

        //Service
        private readonly IOptionOfferService _service;

        public OptionOfferServiceTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _vehicleOffer = fixture.Create<VehicleOffer>();
            _catalog = fixture.Construct<Catalog>(_vehicleOffer.CatalogId);

            _optionId = OptionId.Next(_vehicleOffer.ModelId, PrNumber.Parse("A11"));
            _availability = fixture.Create<Availability>();

            //Create model's option offers
            var modelKeys = Enumerable.Range(0, 4)
                .Select(n => ModelKey.Parse($"{_vehicleOffer.ModelId}000{n}"))
                .ToList();

            _optionOffers = new BoundModelKeyDictionary<Availability>(
                modelKeys.Take(1)
                    .Concat(modelKeys.Skip(2))
                    .ToDictionary(k => k, k => _availability));
            _vehicleOffers = modelKeys
                .Select(k => new VehicleOffer(k, _vehicleOffer.CatalogId))
                .ToList();

            //Dependencies
            _modelKeyValidator = new Mock<IModelKeyValidator>();
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();

            //Service
            _ruleParser = new Mock<IRuleParser>();
            _ruleValidationService = new Mock<IRuleValidationService>();
            _service = new OptionOfferService(_work.Object, _modelKeyValidator.Object)
            {
                Parser = _ruleParser.Object,
                RuleValidationService = _ruleValidationService.Object
            };

            //Setup dependencies
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOffer.Id)).ReturnsAsync(_vehicleOffer);

            _ruleParser.Setup(p => p.IsValid(RuleSet))
                .Returns(true);
        }

        [Fact]
        public async Task Add()
        {
            var inclusion = Inclusion.Automatic;
            var vehicleOffer = new Mock<VehicleOffer>(_vehicleOffer.ModelKey, _catalog.Id);

            //Arrange
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOffer.Id))
                    .InSequence()
                    .ReturnsAsync(vehicleOffer.Object);
                _repository.Setup(r => r.HasRequired<Option, OptionId>(_optionId))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                vehicleOffer.Setup(v => v.AddOptionOffer(_optionId.PrNumber, _availability, inclusion))
                    .InSequence();

                //Act
                await _service.Add(_vehicleOffer.Id, _optionId.PrNumber, _availability, inclusion);
            }
        }

        [Fact]
        public async Task UpdateAvailability()
        {
            //Arrange
            var mock = new Mock<VehicleOffer>(
                _vehicleOffer.ModelKey, _vehicleOffer.CatalogId);
            _repository.Setup(r => r.Require<VehicleOffer>(VehicleOfferId))
                .ReturnsAsync(mock.Object);
            mock.Setup(v => v.UpdateOptionOfferAvailability(_optionId.PrNumber, _availability))
                .Verifiable();

            //Act
            await _service.UpdateAvailability(VehicleOfferId, _optionId.PrNumber, _availability);

            //Assert
            mock.Verify();
        }

        [Fact]
        public async Task UpdateInclusion()
        {
            //Arrange
            var inclusion = Inclusion.Automatic;
            var mock = new Mock<VehicleOffer>(
                _vehicleOffer.ModelKey, _vehicleOffer.CatalogId);
            _repository.Setup(r => r.Require<VehicleOffer>(VehicleOfferId))
                .ReturnsAsync(mock.Object);
            mock.Setup(v => v.UpdateOptionOfferInclusion(_optionId.PrNumber, inclusion))
                .Verifiable();

            //Act
            await _service.UpdateInclusion(VehicleOfferId, _optionId.PrNumber, inclusion);

            //Assert
            mock.Verify();
        }

        [Fact]
        public async Task UpdateAvailabilities()
        {
            //Arrange
            var toUpdate = new Mock<VehicleOffer>(_vehicleOffers[0].ModelKey, _catalog.Id);
            toUpdate.Setup(v => v.HasOptionOffer(_optionId.PrNumber))
                .Returns(true);

            var toDelete = new Mock<VehicleOffer>(_vehicleOffers[1].ModelKey, _catalog.Id);
            toDelete.Setup(v => v.HasOptionOffer(_optionId.PrNumber))
                .Returns(true);

            var toAdd = new Mock<VehicleOffer>(_vehicleOffers[2].ModelKey, _catalog.Id);
            toAdd.Setup(v => v.HasOptionOffer(_optionId.PrNumber))
                .Returns(false);

            using (Sequence.Create())
            {
                //Validate option id
                _repository.Setup(r => r.HasRequired<Option, OptionId>(_optionId))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Validate model keys
                _modelKeyValidator.Setup(v => v.ValidateMany(
                        It.Is<IEnumerable<ModelKey>>(
                            m => _optionOffers.Keys.SequenceEqual(m))))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Query vehicle offers for given model
                _repository.Setup(r => r.Find(It.Is<VehicleOfferModelIdSpecification>(s => s.ModelId == _vehicleOffer.ModelId)))
                    .InSequence()
                    .ReturnsAsync(new[]
                    {
                        toUpdate.Object,
                        toAdd.Object,
                        toDelete.Object
                    });

                //Perform expected actions with option offers for each vehicle offer.
                toUpdate.Setup(v => v.UpdateOptionOfferAvailability(_optionId.PrNumber, _availability))
                    .InSequence();
                toDelete.Setup(v => v.DeleteOptionOffer(_optionId.PrNumber))
                    .InSequence();
                toAdd.Setup(v => v.AddOptionOffer(_optionId.PrNumber, _availability, default(Inclusion)))
                    .InSequence();

                //Act
                await _service.UpdateAvailabilities(_optionId.PrNumber, _optionOffers);
            }
        }

        [Fact]
        public async Task UpdateAvailabilities_GivenVehicleOffers_IsNotFound_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Find(It.Is<VehicleOfferModelIdSpecification>(s => s.ModelId == _vehicleOffer.GearboxId)))
                .ReturnsAsync(Array.Empty<VehicleOffer>());

            //Act
            var e = await Assert.ThrowsAsync<InvalidEntityIdException>(
                () => _service.UpdateAvailabilities(_optionId.PrNumber, _optionOffers));

            //Assert
            Assert.Equal(_vehicleOffer.ModelId, e.Id);
            Assert.Equal(typeof(VehicleOffer), e.EntityType);
        }

        [Fact]
        public async Task Delete()
        {
            //Arrange
            var mock = new Mock<VehicleOffer>(
                _vehicleOffer.ModelKey, _vehicleOffer.CatalogId);
            _repository.Setup(r => r.Require<VehicleOffer>(VehicleOfferId))
                .ReturnsAsync(mock.Object);
            mock.Setup(v => v.DeleteOptionOffer(_optionId.PrNumber))
                .Verifiable();

            //Act
            await _service.Delete(VehicleOfferId, _optionId.PrNumber);

            //Assert
            mock.Verify();
        }

        [Fact]
        public async Task RemoveRule()
        {
            //Arrange
            var mock = new Mock<VehicleOffer>(
                _vehicleOffer.ModelKey, _vehicleOffer.CatalogId);
            _repository.Setup(r => r.Require<VehicleOffer>(VehicleOfferId))
                .ReturnsAsync(mock.Object);
            mock.Setup(v => v.AddOptionOfferRuleSet(_optionId.PrNumber, ""))
                .Verifiable();

            //Act
            await _service.RemoveRule(VehicleOfferId, _optionId.PrNumber);

            //Assert
            mock.Verify();
        }

        [Fact]
        public async Task AddRuleSet()
        {
            //Arrange
            var mock = new Mock<VehicleOffer>(
                _vehicleOffer.ModelKey, _vehicleOffer.CatalogId);
            _repository.Setup(r => r.Require<VehicleOffer>(VehicleOfferId))
                .ReturnsAsync(mock.Object);
            mock.Setup(v => v.AddOptionOfferRuleSet(_optionId.PrNumber, RuleSet))
                .Verifiable();

            //Act
            await _service.AddRuleSet(VehicleOfferId, _optionId.PrNumber, RuleSet);

            //Assert
            mock.Verify();
        }

        [Fact]
        public Task AddRuleSet_WhenRuleSet_IsInvalid_Throws()
        {
            return Assert.ThrowsAsync<FormatException>(
                () => _service.AddRuleSet(_vehicleOffer.Id, _optionId.PrNumber, "invalid"));
        }

        [Fact]
        public Task AddRuleSet_WhenRuleSet_HasConflict_Throws()
        {
            _ruleValidationService.Setup(p => p.HasConflict(RuleSet, _optionId.PrNumber, _vehicleOffer))
                .Returns(true);

            return Assert.ThrowsAsync<RuleConflictException>(
                () => _service.AddRuleSet(_vehicleOffer.Id, _optionId.PrNumber, RuleSet));
        }

        [Fact]
        public Task AddRuleSet_GivenRuleSet_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("ruleSet",
                () => _service.AddRuleSet(VehicleOfferId, _optionId.PrNumber, null));
        }

        [Fact]
        public async Task AddRuleSet_Overload()
        {
            //Arrange
            var vehiclesMocks = Enumerable.Range(0, 4)
                .Select(i => new Mock<VehicleOffer>(_vehicleOffers[i].ModelKey, _catalog.Id))
                .ToList();

            vehiclesMocks.ForEach(m =>
                m.Setup(v => v.AddOptionOfferRuleSet(_optionId.PrNumber, RuleSet))
                    .Verifiable());

            var vehicles = vehiclesMocks.Select(m => m.Object);

            var modelKeySet =
                ModelKeySet.Parse(_vehicleOffers.Select(v => v.ModelKey.Value).Aggregate((a, b) => a + " " + b));

            _repository.Setup(r => r.Find(It.IsAny<VehicleOffersByModelKeySetAndCatalogIdSpecification>())).ReturnsAsync(vehicles);

            //Act
            await _service.AddRuleSet(_catalog.Id, modelKeySet, _optionId, RuleSet);

            //Assert
            vehiclesMocks.ForEach(m => m.Verify());
        }

        [Fact]
        public Task AddRuleSet_Overload_GivenModelKeySet_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("set",
                () => _service.AddRuleSet(_catalog.Id, null, OptionId.Parse("00000"), RuleSet));
        }

        [Fact]
        public Task AddRuleSet_Overload_OptionId_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("optionId",
                () => _service.AddRuleSet(_catalog.Id, ModelKeySet.Parse("000000"), null, RuleSet));
        }

        [Fact]
        public Task ChangeRelevancePeriod_GivenPrNumber_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("prNumber",
                () => _service.ChangeRelevancePeriod(_vehicleOffer.Id, null, RelevancePeriod.Empty));
        }

        [Fact]
        public Task ChangeRelevancePeriod_GivenRelevancePeriod_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("relevancePeriod",
                () => _service.ChangeRelevancePeriod(_vehicleOffer.Id, _optionId.PrNumber, null));
        }

        [Fact]
        public async Task ChangeRelevancePeriod()
        {
            //Arrange
            var vehicleOffer = new Mock<VehicleOffer>(_vehicleOffer.ModelKey, _vehicleOffer.CatalogId);
            var relevancePeriod = new RelevancePeriod(_catalog.RelevancePeriodFrom.AddDays(1));

            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOffer.Id))
                    .InSequence()
                    .ReturnsAsync(vehicleOffer.Object);
                _repository.Setup(r => r.Require<Catalog>(_catalog.Id))
                    .InSequence()
                    .ReturnsAsync(_catalog);
                vehicleOffer.Setup(v => v.ChangeOptionOfferRelevancePeriod(_optionId.PrNumber, relevancePeriod))
                    .InSequence();

                //Act
                await _service.ChangeRelevancePeriod(_vehicleOffer.Id, _optionId.PrNumber, relevancePeriod);
            }
        }

        [Fact]
        public Task ChangeRelevancePeriod_WhenOptionOfferPeriodStart_IsBefore_CatalogPeriod_Throws()
        {
            //Arrange
            var vehicleOffer = new Mock<VehicleOffer>(_vehicleOffer.ModelKey, _vehicleOffer.CatalogId);
            var relevancePeriod = new RelevancePeriod(_catalog.RelevancePeriodFrom.AddDays(-1));

            _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOffer.Id))
                .ReturnsAsync(vehicleOffer.Object);
            _repository.Setup(r => r.Require<Catalog>(_catalog.Id))
                .ReturnsAsync(_catalog);

            //Act
            return Assert.ThrowsAsync<RelevancePeriodExceedsCatalogLifetimeException>(() =>
                _service.ChangeRelevancePeriod(_vehicleOffer.Id, _optionId.PrNumber, relevancePeriod));
        }

        [Fact]
        public Task ChangeRelevancePeriod_WhenOptionOfferPeriodEnd_IsAfter_CatalogPeriod_Throws()
        {
            //Arrange
            var vehicleOffer = new Mock<VehicleOffer>(_vehicleOffer.ModelKey, _vehicleOffer.CatalogId);

            var fromDate = _catalog.RelevancePeriodFrom;
            _catalog.ChangeRelevancePeriodEnding(fromDate.AddDays(5));
            var relevancePeriod = new RelevancePeriod(fromDate, fromDate.AddDays(10));

            _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOffer.Id))
                .ReturnsAsync(vehicleOffer.Object);
            _repository.Setup(r => r.Require<Catalog>(_catalog.Id))
                .ReturnsAsync(_catalog);

            //Act
            return Assert.ThrowsAsync<RelevancePeriodExceedsCatalogLifetimeException>(() =>
                _service.ChangeRelevancePeriod(_vehicleOffer.Id, _optionId.PrNumber, relevancePeriod));
        }
    }
}