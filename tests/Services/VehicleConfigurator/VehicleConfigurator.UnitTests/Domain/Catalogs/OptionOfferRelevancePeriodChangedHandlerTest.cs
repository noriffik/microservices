﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Common;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class OptionOfferRelevancePeriodChangedHandlerTest
    {
        //Dependencies
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly Mock<VehicleOffer> _vehicleOffer;
        private readonly int _vehicleOfferId;
        private readonly OptionOffer _optionOffer;
        private readonly DateTime _from;


        //Event and handler
        private readonly OptionOfferRelevancePeriodChangedEvent _event;
        private readonly OptionOfferRelevancePeriodChangedHandler _handler;

        public OptionOfferRelevancePeriodChangedHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entities
            _vehicleOfferId = fixture.Create<int>();
            _vehicleOffer = new Mock<VehicleOffer>(fixture.Create<ModelKey>(), fixture.Create<int>());

            _optionOffer = fixture.Create<OptionOffer>();
            _from = fixture.Create<DateTime>();

            //Dependencies
            var work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Event and handler
            _event = new OptionOfferRelevancePeriodChangedEvent(_vehicleOfferId, _optionOffer.PrNumber);
            _handler = new OptionOfferRelevancePeriodChangedHandler(work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(OptionOfferRelevancePeriodChangedHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle_WhenNow_IsInPeriod()
        {
            //Arrange
            _optionOffer.ChangeRelevancePeriod(new RelevancePeriod(_from, _from.AddDays(10)));

            using (new SystemTimeContext(_from.AddDays(1)))
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOfferId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer.Object);
                _vehicleOffer.Setup(v => v.FindOptionOfferOrThrow(_optionOffer.PrNumber))
                    .InSequence()
                    .Returns(_optionOffer);
                _vehicleOffer.Setup(v => v.ChangeOptionOfferRelevance(_optionOffer.PrNumber, Relevance.Relevant))
                    .InSequence();

                await _handler.Handle(_event, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_WhenNow_IsNotInPeriod()
        {
            //Arrange
            _optionOffer.ChangeRelevancePeriod(new RelevancePeriod(_from, _from.AddDays(10)));

            using (new SystemTimeContext(_from.AddDays(-1)))
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<VehicleOffer>(_vehicleOfferId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer.Object);
                _vehicleOffer.Setup(v => v.FindOptionOfferOrThrow(_optionOffer.PrNumber))
                    .InSequence()
                    .Returns(_optionOffer);
                _vehicleOffer.Setup(v => v.ChangeOptionOfferRelevance(_optionOffer.PrNumber, Relevance.Irrelevant))
                    .InSequence();

                await _handler.Handle(_event, CancellationToken.None);
            }
        }
    }
}
