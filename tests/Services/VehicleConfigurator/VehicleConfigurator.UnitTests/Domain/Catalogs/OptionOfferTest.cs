﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class OptionOfferTest
    {
        private const Inclusion Inclusion = VehicleConfigurator.Domain.Catalogs.Inclusion.Automatic;
        private readonly OptionId _optionId;
        private readonly OptionOffer _optionOffer;
        private readonly Availability _availability = Availability.Included(ReasonType.NationalStandard);
        private readonly Fixture _fixture;

        public OptionOfferTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            _optionId = _fixture.Create<OptionId>();

            _optionOffer = new OptionOffer(_optionId, Availability.Included(ReasonType.SerialEquipment));
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var offer = new OptionOffer(_optionId, _availability, Inclusion);

            //Arrange
            Assert.Equal(_optionId, offer.OptionId);
            Assert.Equal(_optionId.PrNumber, offer.PrNumber);
            Assert.Equal(_availability, offer.Availability);
            Assert.Equal(string.Empty, offer.RuleSet);
            Assert.Equal(Inclusion, offer.Inclusion);
            Assert.Equal(Relevance.Relevant, offer.Relevance);
            Assert.Null(offer.RelevancePeriodFrom);
            Assert.Null(offer.RelevancePeriodTo);
            Assert.Equal(RelevancePeriod.Empty, offer.RelevancePeriod);
            Assert.False(offer.IsRestricted);
        }

        [Fact]
        public void Ctor_GivenId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("optionId", () => new OptionOffer(null, _availability));
        }

        [Fact]
        public void Ctor_GivenAvailability_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("availability", () => new OptionOffer(_optionId, null));
        }

        [Fact]
        public void AssignRules_GivenRuleSet_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("ruleSet", () => _optionOffer.AssignRules(null));
        }

        [Fact]
        public void AssignRules_WhenRule_ContainsSelfPrNumber_Throws()
        {
            //Arrange
            var ruleSet = $"[ZA: {_optionId.PrNumber} BBB+[VO: ZZZ+[]]][ZO: CCC DDD+[]]";

            //Act
            Assert.Throws<SelfReferenceRuleException>(() => _optionOffer.AssignRules(ruleSet));
        }

        [Fact]
        public void ChangeRelevance()
        {
            //Act
            _optionOffer.ChangeRelevance(Relevance.Irrelevant);

            //Assert
            Assert.Equal(Relevance.Irrelevant, _optionOffer.Relevance);
        }

        [Fact]
        public void ChangeRelevancePeriod()
        {
            //Arrange
            var period = _fixture.Create<RelevancePeriod>();

            //Act
            _optionOffer.ChangeRelevancePeriod(period);

            //Assert
            Assert.Equal(period, _optionOffer.RelevancePeriod);
        }

        [Fact]
        public void ChangeRelevancePeriod_GivenPeriod_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("relevancePeriod", () => _optionOffer.ChangeRelevancePeriod(null));
        }

        [Fact]
        public void EnforceRestriction()
        {
            //Act
            _optionOffer.EnforceRestriction();

            //Assert
            Assert.True(_optionOffer.IsRestricted);
        }

        [Fact]
        public void LiftRestriction()
        {
            //Act
            _optionOffer.LiftRestriction();

            //Assert
            Assert.False(_optionOffer.IsRestricted);
        }
    }
}
