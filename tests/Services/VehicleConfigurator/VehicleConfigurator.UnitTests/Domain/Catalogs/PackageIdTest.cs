﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class PackageIdTest
    {
        private readonly ModelId _modelId;
        private readonly PackageCode _packageCode;

        public PackageIdTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _modelId = fixture.Create<ModelId>();
            _packageCode = fixture.Create<PackageCode>();
        }

        [Theory]
        [InlineData("NFPBZ")]
        [InlineData("3VP01")]
        [InlineData("NHSV1")]
        [InlineData("3vs7P")]
        public void Parse(string value)
        {
            //Act
            var packageId = PackageId.Parse(value);

            //Assert
            Assert.NotNull(packageId);
            Assert.Equal(value.ToUpperInvariant(), packageId.Value);
        }

        [Fact]
        public void Parse_GivenValue_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                PackageId.Parse(null);
            });
            Assert.Equal("value", e.ParamName);
        }

        [Theory]
        [InlineData("")]
        [InlineData("/N1")]
        [InlineData("F%1X2")]
        [InlineData("NF1X")]
        [InlineData("NF1")]
        [InlineData("NF")]
        public void Parse_GivenValue_IsInvalid_Throws(string value)
        {
            Assert.Throws<FormatException>(() =>
            {
                PackageId.Parse(value);
            });
        }

        [Theory]
        [InlineData("NFPBZ")]
        [InlineData("3VP01")]
        [InlineData("NHSV1")]
        [InlineData("3vs7P")]
        public void TryParse(string value)
        {
            //Act
            var result = PackageId.TryParse(value, out var packageId);

            //Assert
            Assert.True(result);
            Assert.NotNull(packageId);
            Assert.Equal(value.ToUpperInvariant(), packageId.Value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("/N1")]
        [InlineData("F%1X2")]
        [InlineData("NF1X")]
        [InlineData("NF1")]
        [InlineData("NF")]
        public void TryParse_GivenValue_IsNullOrInvalid_ReturnsFalse(string value)
        {
            //Act
            var result = PackageId.TryParse(value, out var packageId);

            //Assert
            Assert.False(result);
            Assert.Null(packageId);
        }

        [Fact]
        public void Next()
        {
            //Act
            var packageId = PackageId.Next(_modelId, _packageCode);

            //Assert
            Assert.NotNull(packageId);
            Assert.Equal(_modelId.Value + _packageCode.Value, packageId.Value);
            Assert.Equal(_packageCode, packageId.PackageCode);
            Assert.Equal(_modelId, packageId.ModelId);
        }

        [Fact]
        public void Next_GivenModelId_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                PackageId.Next(null, _packageCode);
            });
            Assert.Equal("modelId", e.ParamName);
        }

        [Fact]
        public void Next_GivenPrNumber_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                PackageId.Next(_modelId, null);
            });
            Assert.Equal("packageCode", e.ParamName);
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var packageId = PackageId.Parse(_modelId.Value + _packageCode.Value);

            //Assert
            Assert.Equal(packageId.Value, packageId.ToString());
        }
    }
}
