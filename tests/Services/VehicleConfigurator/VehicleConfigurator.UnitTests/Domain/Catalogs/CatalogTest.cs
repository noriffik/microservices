﻿using AutoFixture;
using NexCore.Domain;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class CatalogTest
    {
        private readonly Year _year; 
        private readonly Catalog _catalog;
        private readonly Period _period;

        public CatalogTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new DomainBuilder());

            _year = fixture.Create<Year>();
            _period = fixture.Create<Period>();

            _catalog = new Catalog(_year, _period);        
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var catalog = new Catalog(_year, _period);

            //Assert
            Assert.Equal(_year, catalog.ModelYear);
            Assert.Equal(_period, catalog.RelevancePeriod);
            Assert.Equal(CatalogStatus.Draft, catalog.Status);
        }

        [Fact]
        public void Ctor_GivenPeriod_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("period", () => new Catalog(_year, null));
        }

        [Fact]
        public void Ctor_GivenYear_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelYear", () => new Catalog(null, _period));
        }

        [Fact]
        public void ChangeRelevancePeriod()
        {
            //Arrange
            var period = new Period(new DateTime(2019, 2, 2), new DateTime(2019, 3, 3));

            //Act
            _catalog.ChangeRelevancePeriod(period);

            //Assert
            Assert.Equal(period, _catalog.RelevancePeriod);
        }

        [Fact]
        public void ChangeRelevancePeriod_GivenPeriod_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("period", () => _catalog.ChangeRelevancePeriod(null));
        }

        [Fact]
        public void ChangeStatus_GivenStatus_IsPublished()
        {  
            //Act
            _catalog.Status = CatalogStatus.Published;

            //Assert
            Assert.Equal(CatalogStatus.Published, _catalog.Status);
        }

        [Fact]
        public void ChangeStatus_GivenStatus_IsDraft()
        { 
            //Act
            _catalog.Status = CatalogStatus.Draft;

            //Assert
            Assert.Equal(CatalogStatus.Draft, _catalog.Status);
        }

        [Fact]
        public void ChangeRelevancePeriodEnding()
        {
            //Act
            _catalog.ChangeRelevancePeriodEnding(DateTime.MaxValue);
            
            //Assert
            Assert.Equal(DateTime.MaxValue.Date, _catalog.RelevancePeriod.To);
        }
    }
}
