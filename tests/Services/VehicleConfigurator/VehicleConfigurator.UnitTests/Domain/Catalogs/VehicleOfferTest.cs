﻿using AutoFixture;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Catalogs
{
    public class VehicleOfferTest
    {
        private readonly IFixture _fixture;

        //Entities
        private readonly VehicleOffer _vehicleOffer;
        private readonly PrNumber _prNumber;
        private readonly Availability _availability;
        private readonly Inclusion _inclusion;
        private readonly ColorId _colorId;
        private readonly decimal _price;

        public VehicleOfferTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            //Entities
            _vehicleOffer = _fixture.Create<VehicleOffer>();
            _prNumber = _fixture.Create<PrNumber>();
            _availability = _fixture.Create<Availability>();
            _inclusion = _fixture.Create<Inclusion>();
            _colorId = _fixture.Create<ColorId>();
            _price = _fixture.Create<decimal>();
        }

        [Fact]
        public void Ctor()
        {
            //Arrange
            var modelKey = _fixture.Create<ModelKey>();
            var catalogId = _fixture.Create<int>();
            var id = _fixture.Create<int>();

            //Act
            var vehicleOffer = new VehicleOffer(id, modelKey, catalogId);

            //Assert
            Assert.Equal(id, vehicleOffer.Id);
            Assert.Equal(catalogId, vehicleOffer.CatalogId);
            Assert.Equal(modelKey, vehicleOffer.ModelKey);
            Assert.Equal(modelKey.ModelId, vehicleOffer.ModelKey.ModelId);
            Assert.Equal(modelKey.BodyId, vehicleOffer.ModelKey.BodyId);
            Assert.Equal(modelKey.EquipmentId, vehicleOffer.ModelKey.EquipmentId);
            Assert.Equal(modelKey.EngineId, vehicleOffer.ModelKey.EngineId);
            Assert.Equal(modelKey.GearboxId, vehicleOffer.ModelKey.GearboxId);
            Assert.Equal(Relevance.Relevant, vehicleOffer.Relevance);
            Assert.NotNull(vehicleOffer.PrNumbers);
            Assert.Empty(vehicleOffer.PrNumbers.Values);
        }

        [Fact]
        public void Ctor_GivenModelKey_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelKey", () => new VehicleOffer(null, _fixture.Create<int>()));
        }

        [Fact]
        public void Price()
        {
            //Arrange
            var price = _fixture.Create<decimal>();

            //Act
            _vehicleOffer.Price = price;

            //Assert
            Assert.Equal(price, _vehicleOffer.Price);
        }

        [Fact]
        public void AddOptionOffer()
        {
            //Arrange
            var expected = new OptionOffer(OptionId.Next(_vehicleOffer.ModelId, _prNumber), _availability, _inclusion);

            //Act
            var actual = _vehicleOffer.AddOptionOffer(_prNumber, _availability, _inclusion);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<OptionOffer>.Instance);
            Assert.Contains(expected, _vehicleOffer.Options, PropertyComparer<OptionOffer>.Instance);
            Assert.Contains(_prNumber, _vehicleOffer.PrNumbers.Values);
        }

        [Fact]
        public void AddOptionOffer_WhenHasOne_ForGivenPrNumber_Throws()
        {
            //Arrange
            _vehicleOffer.AddOptionOffer(_prNumber, _availability);

            //Act
            var e = Assert.Throws<DuplicateOptionOfferException>(() => _vehicleOffer.AddOptionOffer(_prNumber, _availability));

            //Assert
            Assert.Equal(_vehicleOffer.ModelKey, e.ModelKey);
            Assert.Equal(_prNumber, e.PrNumber);
        }

        [Fact]
        public void AddOptionOffer_GivenPrNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumber", () => _vehicleOffer.AddOptionOffer(null, _availability));
        }

        [Fact]
        public void AddOptionOffer_GivenAvailability_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("availability", () => _vehicleOffer.AddOptionOffer(_prNumber, null));
        }

        [Fact]
        public void FindOptionOffer()
        {
            //Arrange
            _vehicleOffer.AddOptionOffer(_fixture.Create<PrNumber>(), _availability);

            var expected = _vehicleOffer.AddOptionOffer(_prNumber, _availability);

            //Act
            var actual = _vehicleOffer.FindOptionOffer(_prNumber);

            //Assert
            Assert.Same(expected, actual);
        }

        [Fact]
        public void FindOptionOffer_WhenHasNone_ReturnsFalse()
        {
            //Act
            var result = _vehicleOffer.FindOptionOffer(_prNumber);

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public void FindOptionOffer_GivenPrNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumber", () => _vehicleOffer.FindOptionOffer(null));
        }

        [Fact]
        public void FindOptionOfferOrThrow()
        {
            //Arrange
            _vehicleOffer.AddOptionOffer(_fixture.Create<PrNumber>(), _availability);

            var expected = _vehicleOffer.AddOptionOffer(_prNumber, _availability);

            //Act
            var actual = _vehicleOffer.FindOptionOfferOrThrow(_prNumber);

            //Assert
            Assert.Same(expected, actual);
        }

        [Fact]
        public void FindOptionOfferOrThrow_WhenOptionOffer_IsNotFound_Throws()
        {
            //Act
            var e = Assert.Throws<OptionOfferNotFoundException>(() => _vehicleOffer.FindOptionOfferOrThrow(_prNumber));

            //Assert
            Assert.Equal(_vehicleOffer.ModelKey, e.ModelKey);
            Assert.Equal(_prNumber, e.PrNumber);
        }

        [Fact]
        public void FindOptionOfferOrThrow_GivenPrNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumber", () => _vehicleOffer.FindOptionOffer(null));
        }

        [Fact]
        public void HasOptionOffer()
        {
            //Arrange
            _vehicleOffer.AddOptionOffer(_prNumber, _availability);

            //Assert
            Assert.True(_vehicleOffer.HasOptionOffer(_prNumber));
        }

        [Fact]
        public void HasOptionOffer_WhenDoesNot_ReturnsFalse()
        {
            Assert.False(_vehicleOffer.HasOptionOffer(_prNumber));
        }

        [Fact]
        public void HasOptionOffer_GivenPrNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumber", () => _vehicleOffer.HasOptionOffer(null));
        }

        [Fact]
        public void UpdateOptionOfferAvailability()
        {
            //Arrange
            var expected = _vehicleOffer.AddOptionOffer(_prNumber, _availability);
            var availability = _fixture.Create<Availability>();

            //Act
            var actual = _vehicleOffer.UpdateOptionOfferAvailability(_prNumber, availability);

            //Assert
            Assert.Same(expected, actual);
            Assert.Equal(availability, actual.Availability);
        }

        [Fact]
        public void UpdateOptionOfferAvailability_WhenOptionOffer_IsNotFound_Throws()
        {
            //Act
            var e = Assert.Throws<OptionOfferNotFoundException>(() => _vehicleOffer.UpdateOptionOfferAvailability(_prNumber, _availability));

            //Assert
            Assert.Equal(_vehicleOffer.ModelKey, e.ModelKey);
            Assert.Equal(_prNumber, e.PrNumber);
        }

        [Fact]
        public void UpdateOptionOfferAvailability_GivenPrNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumber", () => _vehicleOffer.UpdateOptionOfferAvailability(null, _availability));
        }

        [Fact]
        public void UpdateOptionOfferAvailability_GivenAvailability_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("availability", () => _vehicleOffer.UpdateOptionOfferAvailability(_prNumber, null));
        }

        [Fact]
        public void UpdateOptionOfferInclusion()
        {
            //Arrange
            var expected = _vehicleOffer.AddOptionOffer(_prNumber, _availability, _inclusion);
            var inclusion = _fixture.Create<Inclusion>();

            //Act
            var actual = _vehicleOffer.UpdateOptionOfferInclusion(_prNumber, inclusion);

            //Assert
            Assert.Same(expected, actual);
            Assert.Equal(inclusion, actual.Inclusion);
        }

        [Fact]
        public void UpdateOptionOfferInclusion_WhenOptionOffer_IsNotFound_Throws()
        {
            //Act
            var e = Assert.Throws<OptionOfferNotFoundException>(() => _vehicleOffer.UpdateOptionOfferInclusion(_prNumber, _inclusion));

            //Assert
            Assert.Equal(_vehicleOffer.ModelKey, e.ModelKey);
            Assert.Equal(_prNumber, e.PrNumber);
        }

        [Fact]
        public void UpdateOptionOfferInclusion_GivenPrNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumber", () => _vehicleOffer.UpdateOptionOfferInclusion(null, _inclusion));
        }

        [Fact]
        public void DeleteOptionOffer()
        {
            //Arrange
            var untouched = _vehicleOffer.AddOptionOffer(_fixture.Create<PrNumber>(), _availability);
            var deleted = _vehicleOffer.AddOptionOffer(_prNumber, _availability);

            //Act
            _vehicleOffer.DeleteOptionOffer(_prNumber);

            //Assert
            Assert.DoesNotContain(deleted, _vehicleOffer.Options);
            Assert.Contains(untouched, _vehicleOffer.Options);
            Assert.DoesNotContain(_prNumber, _vehicleOffer.PrNumbers.Values);
        }

        [Fact]
        public void DeleteOptionOffer_WhenOptionOffer_IsNotFound_Throws()
        {
            //Act
            var e = Assert.Throws<OptionOfferNotFoundException>(() => _vehicleOffer.DeleteOptionOffer(_prNumber));

            //Assert
            Assert.Equal(_vehicleOffer.ModelKey, e.ModelKey);
            Assert.Equal(_prNumber, e.PrNumber);
        }

        [Fact]
        public void DeleteOptionOffer_GivenPrNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumber", () => _vehicleOffer.DeleteOptionOffer(null));
        }

        [Fact]
        public void AddOptionOfferRuleSet()
        {
            //Arrange
            var expected = _vehicleOffer.AddOptionOffer(_prNumber, _fixture.Create<Availability>());
            var ruleSet = "[ZA: AAA BBB+[VO: ZZZ+[]]];[ZO: CCC DDD+[]]";

            //Act
            var actual = _vehicleOffer.AddOptionOfferRuleSet(_prNumber, ruleSet);

            //Assert
            Assert.Same(expected, actual);
            Assert.Equal(ruleSet, actual.RuleSet);
        }

        [Fact]
        public void AddOptionOfferRuleSet_GivenPrNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumber", () => _vehicleOffer.AddOptionOfferRuleSet(null, "rule"));
        }

        [Fact]
        public void AddOptionOfferRuleSet_WhenOptionOffer_IsNotFound_Throws()
        {
            //Act
            var e = Assert.Throws<OptionOfferNotFoundException>(() => _vehicleOffer.AddOptionOfferRuleSet(_prNumber, ""));

            //Assert
            Assert.Equal(_vehicleOffer.ModelKey, e.ModelKey);
            Assert.Equal(_prNumber, e.PrNumber);
        }

        [Fact]
        public void ChangeOptionOfferRelevancePeriod()
        {
            //Arrange
            var optionOffer = _vehicleOffer.AddOptionOffer(_prNumber, _availability);
            var relevancePeriod = new RelevancePeriod(_fixture.Create<DateTime>());
            var expectedEvent = new OptionOfferRelevancePeriodChangedEvent(_vehicleOffer.Id, optionOffer.PrNumber);

            //Act
            _vehicleOffer.ChangeOptionOfferRelevancePeriod(_prNumber, relevancePeriod);

            //Assert
            Assert.Equal(relevancePeriod, optionOffer.RelevancePeriod);
            Assert.Contains(expectedEvent, _vehicleOffer.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void ChangeOptionOfferRelevancePeriod_WhenOptionOffer_IsNotFound_Throws()
        {
            //Act
            var e = Assert.Throws<OptionOfferNotFoundException>(() =>
                _vehicleOffer.ChangeOptionOfferRelevancePeriod(_prNumber, RelevancePeriod.Empty));

            //Assert
            Assert.Equal(_prNumber, e.PrNumber);
            Assert.Equal(_vehicleOffer.ModelKey, e.ModelKey);
        }

        [Fact]
        public void ChangeOptionOfferRelevancePeriod_GivenPrNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumber", () =>
                _vehicleOffer.ChangeOptionOfferRelevancePeriod(null, RelevancePeriod.Empty));
        }

        [Fact]
        public void ChangeOptionOfferRelevancePeriod_GivenRelevancePeriod_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("relevancePeriod", () =>
                _vehicleOffer.ChangeOptionOfferRelevancePeriod(_prNumber, null));
        }

        [Fact]
        public void ChangeOptionOfferRelevance()
        {
            //Arrange
            var optionOffer = _vehicleOffer.AddOptionOffer(_prNumber, _availability);
            var expectedEvent = new OptionOfferMarkedAsIrrelevantEvent(_vehicleOffer.Id, _prNumber);

            //Act
            _vehicleOffer.ChangeOptionOfferRelevance(_prNumber, Relevance.Irrelevant);

            //Assert
            Assert.Equal(Relevance.Irrelevant, optionOffer.Relevance);
            Assert.Contains(expectedEvent, _vehicleOffer.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void ChangeOptionOfferRelevance_GivenRelevance_IsSame_DoesNothing()
        {
            //Arrange
            var optionOffer = _vehicleOffer.AddOptionOffer(_prNumber, _availability);
            optionOffer.ChangeRelevance(Relevance.Irrelevant);
            var expectedEvent = new OptionOfferMarkedAsIrrelevantEvent(_vehicleOffer.Id, _prNumber);

            //Act
            _vehicleOffer.ChangeOptionOfferRelevance(_prNumber, Relevance.Irrelevant);

            //Assert
            Assert.Equal(Relevance.Irrelevant, optionOffer.Relevance);
            Assert.DoesNotContain(expectedEvent, _vehicleOffer.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void ChangeOptionOfferRelevance_WhenOptionOffer_IsNotFound_Throws()
        {
            //Act
            var e = Assert.Throws<OptionOfferNotFoundException>(() =>
                _vehicleOffer.ChangeOptionOfferRelevance(_prNumber, Relevance.Irrelevant));

            //Assert
            Assert.Equal(_prNumber, e.PrNumber);
            Assert.Equal(_vehicleOffer.ModelKey, e.ModelKey);
        }

        [Fact]
        public void ChangeOptionOfferRelevance_GivenPrNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumber", () =>
                _vehicleOffer.ChangeOptionOfferRelevance(null, Relevance.Irrelevant));
        }

        [Fact]
        public void AddColorOffer()
        {
            //Arrange
            var expected = new ColorOffer(_colorId, _price);

            //Act
            var actual = _vehicleOffer.AddColorOffer(_colorId, _price);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<ColorOffer>.Instance);
            Assert.Contains(expected, _vehicleOffer.Colors, PropertyComparer<ColorOffer>.Instance);
        }

        [Fact]
        public void AddColorOffer_WhenHasOne_ForGivenColorId_Throws()
        {
            //Arrange
            _vehicleOffer.AddColorOffer(_colorId, _price);

            //Act
            var e = Assert.Throws<DuplicateColorOfferException>(() => _vehicleOffer.AddColorOffer(_colorId, _price));

            //Assert
            Assert.Equal(_vehicleOffer.ModelKey, e.ModelKey);
            Assert.Equal(_colorId, e.ColorId);
        }

        [Fact]
        public void AddColorOffer_GivenColorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("colorId", () => _vehicleOffer.AddColorOffer(null, _price));
        }

        [Fact]
        public void HasColorOffer_WhenHasOne_ForGivenColorId_ReturnsTrue()
        {
            //Arrange
            _vehicleOffer.AddColorOffer(_colorId, _price);

            //Act
            var result = _vehicleOffer.HasColorOffer(_colorId);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void HasColorOffer_WhenHasNotOne_ForGivenColorId_ReturnsFalse()
        {
            //Act
            var result = _vehicleOffer.HasColorOffer(_colorId);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void HasColorOffer_GivenColorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("colorId", () => _vehicleOffer.HasColorOffer(null));
        }

        [Fact]
        public void HasRelevantColorOffer()
        {
            //Arrange
            _vehicleOffer.AddColorOffer(_colorId, _price);

            //Act
            var result = _vehicleOffer.HasRelevantColorOffer(_colorId);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void HasRelevantColorOffer_WhenHasNotOne_ForGivenColorId_ReturnsFalse()
        {
            //Arrange
            var colorOffer = _vehicleOffer.AddColorOffer(_colorId, _price);
            colorOffer.ChangeRelevance(Relevance.Irrelevant);

            //Act
            var result = _vehicleOffer.HasRelevantColorOffer(_colorId);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void HasRelevantColorOffer_GivenColorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("colorId", () => _vehicleOffer.HasRelevantColorOffer(null));
        }

        [Fact]
        public void FindColorOffer()
        {
            //Arrange
            var expected = _vehicleOffer.AddColorOffer(_colorId, _price);

            //Act
            var result = _vehicleOffer.FindColorOffer(_colorId);

            //Assert
            Assert.Same(expected, result);
        }

        [Fact]
        public void FindColorOffer_WhenHasNotOne_ForGivenColorId_ReturnsNull()
        {
            //Act
            var result = _vehicleOffer.FindColorOffer(_colorId);

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public void FindColorOffer_GivenColorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("colorId", () => _vehicleOffer.FindColorOffer(null));
        }

        [Fact]
        public void FindColorOfferOrThrow()
        {
            //Arrange
            _vehicleOffer.AddColorOffer(_fixture.Create<ColorId>(), _fixture.Create<decimal>());

            var expected = _vehicleOffer.AddColorOffer(_colorId, _price);

            //Act
            var actual = _vehicleOffer.FindColorOfferOrThrow(_colorId);

            //Assert
            Assert.Same(expected, actual);
        }

        [Fact]
        public void FindColorOfferOrThrow_WhenColorOffer_IsNotFound_Throws()
        {
            //Act
            var e = Assert.Throws<ColorOfferNotFoundException>(() => _vehicleOffer.FindColorOfferOrThrow(_colorId));

            //Assert
            Assert.Equal(_vehicleOffer.ModelKey, e.ModelKey);
            Assert.Equal(_colorId, e.ColorId);
        }

        [Fact]
        public void FindColorOfferOrThrows_GivenColorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("colorId", () => _vehicleOffer.FindColorOfferOrThrow(null));
        }

        [Fact]
        public void ChangeColorOfferRelevancePeriod()
        {
            //Arrange
            var colorOffer = _vehicleOffer.AddColorOffer(_colorId, _price);
            var relevancePeriod = _fixture.Create<RelevancePeriod>();
            var expectedEvent = new ColorOfferRelevancePeriodChangedEvent(_vehicleOffer.Id, _colorId);

            //Act
            _vehicleOffer.ChangeColorOfferRelevancePeriod(_colorId, relevancePeriod);

            //Assert
            Assert.Equal(relevancePeriod, colorOffer.RelevancePeriod);
            Assert.Contains(expectedEvent, _vehicleOffer.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void ChangeColorOfferRelevancePeriod_WhenColorOffer_IsNotFound_Throws()
        {
            //Act
            var e = Assert.Throws<ColorOfferNotFoundException>(() =>
                _vehicleOffer.ChangeColorOfferRelevancePeriod(_colorId, RelevancePeriod.Empty));

            //Assert
            Assert.Equal(_colorId, e.ColorId);
            Assert.Equal(_vehicleOffer.ModelKey, e.ModelKey);
        }

        [Fact]
        public void ChangeColorOfferRelevancePeriod_GivenColorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("colorId", () =>
                _vehicleOffer.ChangeColorOfferRelevancePeriod(null, RelevancePeriod.Empty));
        }

        [Fact]
        public void ChangeColorOfferRelevancePeriod_GivenRelevancePeriod_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("relevancePeriod", () =>
                _vehicleOffer.ChangeColorOfferRelevancePeriod(_colorId, null));
        }

        [Fact]
        public void ChangeColorOfferRelevancePeriod_GivenColorId_IsDefaultColorId_Throws()
        {
            _vehicleOffer.AddColorOffer(_colorId, _price);
            _vehicleOffer.SetDefaultColor(_colorId);

            //Act
            var e = Assert.Throws<InvalidDomainOperationException>(() =>
                _vehicleOffer.ChangeColorOfferRelevancePeriod(_colorId, _fixture.Create<RelevancePeriod>()));

            //Assert
            Assert.Contains("cannot be changed", e.Message);
        }

        [Fact]
        public void ChangeColorOfferRelevance()
        {
            //Arrange
            var colorOffer = _vehicleOffer.AddColorOffer(_colorId, _price);
            var expectedEvent = new ColorOfferMarkedAsIrrelevantEvent(_vehicleOffer.Id, _colorId);

            //Act
            _vehicleOffer.ChangeColorOfferRelevance(_colorId, Relevance.Irrelevant);

            //Assert
            Assert.Equal(Relevance.Irrelevant, colorOffer.Relevance);
            Assert.Contains(expectedEvent, _vehicleOffer.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void ChangeColorOfferRelevance_GivenRelevance_IsSame_DoesNothing()
        {
            //Arrange
            var colorOffer = _vehicleOffer.AddColorOffer(_colorId, _price);
            colorOffer.ChangeRelevance(Relevance.Irrelevant);
            var notExpectedEvent = new ColorOfferMarkedAsIrrelevantEvent(_vehicleOffer.Id, _colorId);

            //Act
            _vehicleOffer.ChangeColorOfferRelevance(_colorId, Relevance.Irrelevant);

            //Assert
            Assert.Equal(Relevance.Irrelevant, colorOffer.Relevance);
            Assert.DoesNotContain(notExpectedEvent, _vehicleOffer.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void ChangeColorOfferRelevance_WhenColorOffer_IsNotFound_Throws()
        {
            //Act
            var e = Assert.Throws<ColorOfferNotFoundException>(() => _vehicleOffer.ChangeColorOfferRelevance(_colorId, Relevance.Irrelevant));

            //Assert
            Assert.Equal(_colorId, e.ColorId);
            Assert.Equal(_vehicleOffer.ModelKey, e.ModelKey);
        }

        [Fact]
        public void ChangeColorOfferRelevance_GivenColorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("colorId",
                () => _vehicleOffer.ChangeColorOfferRelevance(null, Relevance.Relevant));
        }

        [Fact]
        public void ChangeColorOfferRelevance_GivenColorId_IsDefaultColorId_Throws()
        {
            _vehicleOffer.AddColorOffer(_colorId, _price);
            _vehicleOffer.SetDefaultColor(_colorId);

            //Act
            var e = Assert.Throws<InvalidDomainOperationException>(() =>
                _vehicleOffer.ChangeColorOfferRelevance(_colorId, Relevance.Irrelevant));

            //Assert
            Assert.Contains("cannot be changed", e.Message);
        }

        [Fact]
        public void ChangeColorOfferPrice()
        {
            //Arrange
            var price = _fixture.Create<decimal>();
            var colorOffer = _vehicleOffer.AddColorOffer(_colorId, _price);

            //Act
            _vehicleOffer.ChangeColorOfferPrice(_colorId, price);

            //Assert
            Assert.Equal(price, colorOffer.Price);
        }

        [Fact]
        public void ChangeColorOfferPrice_WhenColorOffer_IsNotFound_Throws()
        {
            //Act
            var e = Assert.Throws<ColorOfferNotFoundException>(() =>
                _vehicleOffer.ChangeColorOfferPrice(_colorId, _fixture.Create<decimal>()));

            //Assert
            Assert.Equal(_colorId, e.ColorId);
            Assert.Equal(_vehicleOffer.ModelKey, e.ModelKey);
        }

        [Fact]
        public void ChangeColorOfferPrice_GivenColorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("colorId",
                () => _vehicleOffer.ChangeColorOfferPrice(null, _fixture.Create<decimal>()));
        }

        [Fact]
        public void Publish()
        {
            //Arrange
            _vehicleOffer.AddColorOffer(_colorId, _price);
            _vehicleOffer.SetDefaultColor(_colorId);

            //Act
            _vehicleOffer.Publish();

            //Assert
            Assert.Equal(VehicleOfferStatus.Published, _vehicleOffer.Status);
        }

        [Fact]
        public void Unpublish()
        {
            //Assert
            _vehicleOffer.AddColorOffer(_colorId, _price);
            _vehicleOffer.SetDefaultColor(_colorId);
            _vehicleOffer.Publish();
            var expectedEvent = new VehicleOfferUnpublishedEvent(_vehicleOffer.Id);

            //Act
            _vehicleOffer.Unpublish();

            //Assert
            Assert.Equal(VehicleOfferStatus.Draft, _vehicleOffer.Status);
            Assert.Contains(expectedEvent, _vehicleOffer.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void Unpublish_WhenVehicleOffer_IsAlreadyUnpublished_DoesNothing()
        {
            //Assert
            var notExpectedEvent = new VehicleOfferUnpublishedEvent(_vehicleOffer.Id);

            //Act
            _vehicleOffer.Unpublish();

            //Assert
            Assert.Equal(VehicleOfferStatus.Draft, _vehicleOffer.Status);
            Assert.DoesNotContain(notExpectedEvent, _vehicleOffer.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void SetDefaultColor()
        {
            //Arrange
            _vehicleOffer.AddColorOffer(_colorId, _price);

            //Act
            _vehicleOffer.SetDefaultColor(_colorId);

            //Assert
            Assert.Equal(_colorId, _vehicleOffer.DefaultColorId);
        }

        [Fact]
        public void SetDefaultColor_ColorOffer_NotFound_Throws()
        {
            //Act
            var e = Assert.Throws<DefaultColorViolationException>(() => _vehicleOffer.SetDefaultColor(_colorId));

            //Assert
            Assert.Equal(_vehicleOffer.ModelKey, e.ModelKey);
            Assert.Equal(_colorId, e.ColorId);
        }

        [Fact]
        public void GetDefaultColor_WhenDefaultColor_IsNotSet_ReturnsNull()
        {
            //Act
            var colorOffer = _vehicleOffer.DefaultColor;

            //Assert
            Assert.Null(colorOffer);
        }

        [Fact]
        public void GetDefaultColor_WhenDefaultColor_IsSet_ReturnsColorOffer()
        {
            //Arrange
            _vehicleOffer.AddColorOffer(_colorId, _price);
            _vehicleOffer.SetDefaultColor(_colorId);

            //Act
            var colorOffer = _vehicleOffer.DefaultColor;

            //Assert
            Assert.NotNull(colorOffer);
            Assert.Equal(_colorId, colorOffer.ColorId);
        }

        [Fact]
        public void GetDefaultColor_WhenDefaultColor_IsReset_ReturnsColorOffer()
        {
            //Arrange
            _vehicleOffer.AddColorOffer(_colorId, _price);
            _vehicleOffer.SetDefaultColor(_colorId);
            var defaultColorId = _fixture.Create<ColorId>();
            _vehicleOffer.AddColorOffer(defaultColorId, _price);
            _vehicleOffer.SetDefaultColor(defaultColorId);

            //Act
            var colorOffer = _vehicleOffer.DefaultColor;

            //Assert
            Assert.NotNull(colorOffer);
            Assert.Equal(defaultColorId, colorOffer.ColorId);
        }

        [Fact]
        public void EnforceOptionRestriction()
        {
            //Arrange
            var toRestrict = _fixture.Create<PrNumberSet>();

            var availability = _fixture.Create<Availability>();
            var unexpected = _fixture.CreateMany<PrNumber>(3)
                .Select(n => _vehicleOffer.AddOptionOffer(n, availability))
                .ToList();
            var expected = toRestrict.Values
                .Select(n => _vehicleOffer.AddOptionOffer(n, availability))
                .ToList();

            var expectedEvent = new RestrictionEnforcedUponOptionOfferEvent(
                new OptionOffersAffectedByRestriction(_vehicleOffer.CatalogId, _vehicleOffer.ModelKey,
                    toRestrict));

            //Act
            _vehicleOffer.EnforceOptionRestriction(toRestrict);

            //Assert
            Assert.All(expected, o => Assert.True(o.IsRestricted));
            Assert.All(unexpected, o => Assert.False(o.IsRestricted));
            Assert.Contains(expectedEvent, _vehicleOffer.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void LiftOptionRestriction()
        {
            //Arrange
            var unRestrict = _fixture.Create<PrNumberSet>();

            var availability = _fixture.Create<Availability>();
            var unexpected = _fixture.CreateMany<PrNumber>(3)
                .Select(n => _vehicleOffer.AddOptionOffer(n, availability))
                .ToList();
            unexpected.ForEach(o => o.EnforceRestriction());

            var expected = unRestrict.Values
                .Select(n => _vehicleOffer.AddOptionOffer(n, availability))
                .ToList();
            expected.ForEach(o => o.EnforceRestriction());

            var expectedEvent = new RestrictionLiftedFromOptionOfferEvent(
                new OptionOffersAffectedByRestriction(_vehicleOffer.CatalogId, _vehicleOffer.ModelKey,
                    unRestrict));

            //Act
            _vehicleOffer.LiftOptionRestriction(unRestrict);

            //Assert
            Assert.All(expected, o => Assert.False(o.IsRestricted));
            Assert.All(unexpected, o => Assert.True(o.IsRestricted));
            Assert.Contains(expectedEvent, _vehicleOffer.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void ChangeRelevancePeriod()
        {
            //Arrange
            var relevancePeriod = _fixture.Create<RelevancePeriod>();

            //Act
            _vehicleOffer.ChangeRelevancePeriod(relevancePeriod);

            //Assert
            Assert.Equal(relevancePeriod, _vehicleOffer.RelevancePeriod);
            Assert.Contains(new VehicleOfferRelevancePeriodChangedEvent(_vehicleOffer.Id), _vehicleOffer.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void ChangeRelevancePeriod_GivenRelevancePeriod_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("relevancePeriod", () => _vehicleOffer.ChangeRelevancePeriod(null));
        }

        [Fact]
        public void ChangeRelevance_ToIrrelevant()
        {
            //Arrange
            var expectedEvent = new VehicleOfferMarkedAsIrrelevantEvent(_vehicleOffer.Id);

            //Act
            _vehicleOffer.ChangeRelevance(Relevance.Irrelevant);

            //Assert
            Assert.Equal(Relevance.Irrelevant, _vehicleOffer.Relevance);
            Assert.Contains(expectedEvent, _vehicleOffer.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void ChangeRelevance_ToIrrelevant_WhenVehicleOffer_IsAlreadyIrrelevant_DoesNothing()
        {
            //Arrange
            _vehicleOffer.ChangeRelevance(Relevance.Irrelevant);
            _vehicleOffer.ClearEntityEvents();
            var notExpectedEvent = new VehicleOfferMarkedAsIrrelevantEvent(_vehicleOffer.Id);

            //Act
            _vehicleOffer.ChangeRelevance(Relevance.Irrelevant);

            //Assert
            Assert.Equal(Relevance.Irrelevant, _vehicleOffer.Relevance);
            Assert.DoesNotContain(notExpectedEvent, _vehicleOffer.EntityEvents, PropertyComparer<IEntityEvent>.Instance);
        }

        [Fact]
        public void ChangeRelevance_ToRelevant()
        {
            //Arrange
            _vehicleOffer.ChangeRelevance(Relevance.Irrelevant);

            //Act
            _vehicleOffer.ChangeRelevance(Relevance.Relevant);

            //Assert
            Assert.Equal(Relevance.Relevant, _vehicleOffer.Relevance);
        }
    }
}
