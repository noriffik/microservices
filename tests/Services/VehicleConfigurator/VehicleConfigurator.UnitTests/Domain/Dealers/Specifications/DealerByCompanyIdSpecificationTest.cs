﻿using AutoFixture;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.Domain.Dealers.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Dealers;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Dealers.Specifications
{
    public class DealerByCompanyIdSpecificationTest
    {
        private readonly Dealer _expected;
        private readonly Dealer _unexpected;
        private readonly IReadOnlyList<Dealer> _all;
        private readonly IFixture _fixture;

        public DealerByCompanyIdSpecificationTest()
        {
            _fixture = new Fixture().Customize(new DealerIdCustomization());

            _expected = _fixture.Create<Dealer>();
            _unexpected = _fixture.Create<Dealer>();
            _all = new[] { _expected, _unexpected };
        }

        [Fact]
        public void IsSatisfied_ReturnsTrue()
        {
            //Arrange
            var specification = new DealerByCompanyIdSpecification(_expected.CompanyId);

            //Assert
            Assert.True(specification.IsSatisfiedBy(_expected));
        }

        [Fact]
        public void IsSatisfied_ReturnsFalse()
        {
            //Arrange
            var specification = new DealerByCompanyIdSpecification(_fixture.Create<int>());

            //Assert
            Assert.False(specification.IsSatisfiedBy(_unexpected));
        }

        [Fact]
        public void ToExpression()
        {
            //Arrange
            var specification = new DealerByCompanyIdSpecification(_expected.CompanyId);
            var expression = specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(new[] { _expected }, _all.AsQueryable().Where(expression));
        }
    }
}
