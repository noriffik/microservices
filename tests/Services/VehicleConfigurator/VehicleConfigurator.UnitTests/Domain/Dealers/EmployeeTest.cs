﻿using AutoFixture;
using NexCore.DealerNetwork;
using NexCore.Legal;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Dealers;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Dealers
{
    public class EmployeeTest
    {
        private readonly DealerId _dealerId;
        private readonly PersonName _personName;
        private readonly int _employeeId;

        private readonly Employee _employee;

        public EmployeeTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            _dealerId = fixture.Create<DealerId>();
            _employeeId = fixture.Create<int>();
            _personName = fixture.Create<PersonName>();

            _employee = fixture.Create<Employee>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var employee = new Employee(_employeeId, _personName, _dealerId);

            //Assert
            Assert.Equal(_employeeId, employee.Id);
            Assert.Equal(_personName, employee.PersonName, PropertyComparer<PersonName>.Instance);
            Assert.Equal(_dealerId, employee.DealerId);
        }

        [Fact]
        public void Ctor_GivenPersonName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("personName", () =>
                new Employee(1, null, _dealerId));
        }

        [Fact]
        public void Ctor_GivenDealerId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealerId", () =>
                new Employee(1, _personName, null));
        }

        [Fact]
        public void ChangeName()
        {
            //Act
            _employee.ChangeName(_personName);

            //Assert
            Assert.Equal(_personName, _employee.PersonName);
        }

        [Fact]
        public void ChangeName_GivenName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("name", () => _employee.ChangeName(null));
        }
    }
}
