﻿using AutoFixture;
using NexCore.DealerNetwork;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Dealers;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Dealers
{
    public class DealerTest
    {
        private readonly IFixture _fixture;

        private readonly DealerId _id;
        private readonly int _companyId;
        private readonly OrganizationRequisites _requisites;

        private readonly Dealer _dealer;

        public DealerTest()
        {
            _fixture = new Fixture().Customize(new DealerIdCustomization());

            _id = _fixture.Create<DealerId>();
            _companyId = _fixture.Create<int>();
            _requisites = _fixture.Create<OrganizationRequisites>();

            _dealer = new Dealer(_id, _companyId, _requisites);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var dealer = new Dealer(_id, _companyId, _requisites);

            //Assert
            Assert.Equal(_id, dealer.Id);
            Assert.Equal(_companyId, dealer.CompanyId);
            Assert.Equal(_id.DistributorId, dealer.DistributorId);
            Assert.Equal(_id.DealerCode, dealer.DealerCode);
            Assert.Equal(_requisites, dealer.Requisites);
        }

        [Fact]
        public void Ctor_GivenId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("id", () =>
                new Dealer(null, _companyId, _requisites));
        }

        [Fact]
        public void Ctor_GivenRequisites_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("requisites", () =>
                new Dealer(_id, _companyId, null));
        }

        [Fact]
        public void ChangeRequisites()
        {
            //Arrange
            var requisites = _fixture.Create<OrganizationRequisites>();

            //Act
            _dealer.ChangeRequisites(requisites);

            //Assert
            Assert.Equal(requisites, _dealer.Requisites);
        }

        [Fact]
        public void ChangeRequisites_GivenRequisites_IsNull_AssignEmpty()
        {
            //Act
            _dealer.ChangeRequisites(null);

            //Assert
            Assert.Equal(OrganizationRequisites.Empty, _dealer.Requisites);
        }
    }
}
