﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Rules;
using NexCore.VehicleConfigurator.Domain.Rules.Expressions;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Rules.Expressions
{
    public class OnlyWithOneTest
    {
        private readonly PropertyComparer<IEnumerable<PrNumberCombination>> _prNumberCombinationComparer =
            new PropertyComparer<IEnumerable<PrNumberCombination>>(new PropertyComparerOptions
                {CollectionComparison = CollectionComparison.Unordered});

        [Fact]
        public void Ctor_VerifyInjection()
        {
            //Assert
            Assert.Injection.OfConstructor(typeof(OnlyWithOne));
        }
        
        [Fact]
        public void Formatting()
        {
            //Arrange
            var prNumbers = "AAA BBB CCC";
            var prNumberSet = PrNumberSet.Parse(prNumbers);
            var prNumbers2 = "DDD EEE";
            var prNumberSet2 = PrNumberSet.Parse(prNumbers2);
            var onlyWithOneExpression = new OnlyWithOne(prNumberSet, new []{new OnlyWithOne(prNumberSet2, null)});
            var expected = "[ZO: AAA BBB CCC+[[ZO: DDD EEE+[]]]]";

            //Act
            var result = onlyWithOneExpression.ToString();

            //Assert
            Assert.Equal(expected, result);
        }
        
        [Fact]
        public void Check()
        {
            //Arrange
            var configuration = "AAA EEE BBB";
            var onlyWithOneExpression = new OnlyWithOne(PrNumberSet.Parse("AAA CCC FFF"), null);
            var context = new RuleContext("", configuration);

            //Act
            var result = onlyWithOneExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Check_When_Failed()
        {
            //Arrange
            var configuration = "AAA EEE BBB";
            var onlyWithOneExpression = new OnlyWithOne(PrNumberSet.Parse("EEE BBB"), null);

            var context = new RuleContext("", configuration);

            //Act
            var result = onlyWithOneExpression.Check(context);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void Check_WithNested()
        {
            //Arrange
            var configuration = "BBB ZZZ";
            var nestedExpression = new OnlyWithOne(PrNumberSet.Parse("ZZZ XXX"), null);
            var onlyWithOneExpression = new OnlyWithOne(PrNumberSet.Parse("BBB CCC"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = onlyWithOneExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Check_When_Failed_WithNested()
        {
            //Arrange
            var configuration = "CCC BBB ZZZ";
            var nestedExpression = new OnlyWithOne(PrNumberSet.Parse("ZZZ XXX"), null);
            var onlyWithOneExpression = new OnlyWithOne(PrNumberSet.Parse("BBB CCC"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = onlyWithOneExpression.Check(context);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void Check_WhenNested_Failed()
        {
            //Arrange
            var configuration = "CCC BBB ZZZ XXX";
            var nestedExpression = new OnlyWithOne(PrNumberSet.Parse("ZZZ XXX"), null);
            var onlyWithOneExpression = new OnlyWithOne(PrNumberSet.Parse("BBB CCC"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = onlyWithOneExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Check_WithNested_When_Failed()
        {
            //Arrange
            var configuration = "AAA";
            var nestedExpression = new OnlyWithOne(PrNumberSet.Parse("ZZZ XXX"), null);
            var onlyWithOneExpression = new OnlyWithOne(PrNumberSet.Parse("BBB CCC"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = onlyWithOneExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void GetAllPossibleOptions()
        {
            //Arrange
            var prNumberSet = PrNumberSet.Parse("AAA BBB CCC");
            var prNumberSet2 = PrNumberSet.Parse("DDD EEE");
            var prNumberSet3 = PrNumberSet.Parse("CCC YYY");
            var onlyWithOneExpression = new OnlyWithOne(prNumberSet, new []
            {
                new OnlyWithOne(prNumberSet2, null), new OnlyWithOne(prNumberSet3, null)
            });
            var satisfying = new List<PrNumberCombination>
            {
                PrNumberCombination.Both("AAA YYY DDD", "BBB CCC EEE"),
                PrNumberCombination.Both("AAA YYY EEE", "BBB CCC DDD"),
                PrNumberCombination.Both("BBB DDD YYY", "AAA CCC EEE"),
                PrNumberCombination.Both("BBB EEE YYY", "AAA CCC DDD"),
                PrNumberCombination.Both("CCC EEE", "AAA BBB DDD YYY"),
                PrNumberCombination.Both("CCC DDD", "AAA BBB EEE YYY"),
                PrNumberCombination.IncludedOnly("DDD EEE"),
                PrNumberCombination.ExcludedOnly("DDD EEE"),
                PrNumberCombination.IncludedOnly("CCC YYY"),
                PrNumberCombination.ExcludedOnly("CCC YYY")
            };
            var unsatisfying = new List<PrNumberCombination>
            {
                PrNumberCombination.IncludedOnly("AAA BBB CCC"),
                PrNumberCombination.ExcludedOnly("AAA BBB CCC"),
                PrNumberCombination.Both("BBB CCC", "AAA"),
                PrNumberCombination.Both("AAA CCC", "BBB"),
                PrNumberCombination.Both("AAA BBB", "CCC")
            };
            
            //Act
            var result = onlyWithOneExpression.GetAllCombinations();

            //Assert
            Assert.NotNull(result);
            Assert.Equal(satisfying, result.Satisfying, _prNumberCombinationComparer);
            Assert.Equal(unsatisfying, result.Unsatisfying, _prNumberCombinationComparer);
        }
    }
}
