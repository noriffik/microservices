﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Rules;
using NexCore.VehicleConfigurator.Domain.Rules.Expressions;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Rules.Expressions
{
    public class OnlyWithAllTest
    {
        private readonly PropertyComparer<IEnumerable<PrNumberCombination>> _prNumberCombinationComparer =
            new PropertyComparer<IEnumerable<PrNumberCombination>>(new PropertyComparerOptions
                {CollectionComparison = CollectionComparison.Unordered});

        [Fact]
        public void Ctor_VerifyInjection()
        {
            //Assert
            Assert.Injection.OfConstructor(typeof(OnlyWithAll));
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var prNumberSet = PrNumberSet.Parse("AAA BBB CCC");
            var prNumberSet2 = PrNumberSet.Parse("DDD EEE");
            var prNumberSet3 = PrNumberSet.Parse("XXX YYY");
            var onlyWithAllExpression = new OnlyWithAll(prNumberSet, new []{new OnlyWithAll(prNumberSet2, null), new OnlyWithAll(prNumberSet3, null)});
            var expected = "[ZA: AAA BBB CCC+[[ZA: DDD EEE+[]][ZA: XXX YYY+[]]]]";

            //Act
            var result = onlyWithAllExpression.ToString();

            //Assert
            Assert.Equal(expected, result);
        }
        
        [Fact]
        public void Check()
        {
            //Arrange
            var configuration = "AAA EEE BBB";
            var onlyWithAllExpression = new OnlyWithAll(PrNumberSet.Parse("AAA EEE"), null);
            
            var context = new RuleContext("", configuration);

            //Act
            var result = onlyWithAllExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Check_When_Failed()
        {
            //Arrange
            var configuration = "AAA EEE BBB";
            var onlyWithAllExpression = new OnlyWithAll(PrNumberSet.Parse("XXX DDD"), null);
            var context = new RuleContext("", configuration);

            //Act
            var result = onlyWithAllExpression.Check(context);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void Check_WithNested()
        {
            //Arrange
            var configuration = "CCC BBB ZZZ";
            var nestedExpression = new OnlyWithAll(PrNumberSet.Parse("ZZZ"), null);
            var onlyWithAllExpression = new OnlyWithAll(PrNumberSet.Parse("BBB CCC"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = onlyWithAllExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Check_When_Failed_WithNested()
        {
            //Arrange
            var configuration = "CCC ZZZ";
            var nestedExpression = new OnlyWithAll(PrNumberSet.Parse("ZZZ"), null);
            var onlyWithAllExpression = new OnlyWithAll(PrNumberSet.Parse("BBB CCC"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = onlyWithAllExpression.Check(context);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void Check_WhenNested_Failed()
        {
            //Arrange
            var configuration = "CCC BBB";
            var nestedExpression = new OnlyWithAll(PrNumberSet.Parse("ZZZ"), null);
            var onlyWithAllExpression = new OnlyWithAll(PrNumberSet.Parse("BBB CCC"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = onlyWithAllExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Check_WithNested_When_Failed()
        {
            //Arrange
            var configuration = "AAA";
            var nestedExpression = new OnlyWithAll(PrNumberSet.Parse("ZZZ"), null);
            var onlyWithAllExpression = new OnlyWithAll(PrNumberSet.Parse("BBB CCC"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = onlyWithAllExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void GetAllPossibleOptions()
        {
            //Arrange
            var prNumberSet = PrNumberSet.Parse("AAA BBB CCC");
            var prNumberSet2 = PrNumberSet.Parse("DDD EEE");
            var prNumberSet3 = PrNumberSet.Parse("EEE FFF");
            var onlyWithAllExpression = new OnlyWithAll(prNumberSet, new []
            {
                new OnlyWithAll(prNumberSet2, null), new OnlyWithAll(prNumberSet3, null)
            });
            var satisfying = new List<PrNumberCombination>
            {
                PrNumberCombination.IncludedOnly("AAA BBB CCC DDD EEE FFF"),
                PrNumberCombination.ExcludedOnly("DDD"),
                PrNumberCombination.ExcludedOnly("EEE"),
                PrNumberCombination.ExcludedOnly("FFF")
            };
            var unsatisfying = new List<PrNumberCombination>
            {
                PrNumberCombination.ExcludedOnly("AAA"),
                PrNumberCombination.ExcludedOnly("BBB"),
                PrNumberCombination.ExcludedOnly("CCC")
            };

            //Act
            var result = onlyWithAllExpression.GetAllCombinations();

            //Assert
            Assert.NotNull(result);
            Assert.Equal(satisfying, result.Satisfying, _prNumberCombinationComparer);
            Assert.Equal(unsatisfying, result.Unsatisfying, _prNumberCombinationComparer);
        }
    }
}
