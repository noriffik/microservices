﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Rules;
using NexCore.VehicleConfigurator.Domain.Rules.Expressions;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Rules.Expressions
{
    public class ProhibitedOneTest
    {
        private readonly PropertyComparer<IEnumerable<PrNumberCombination>> _prNumberCombinationComparer =
            new PropertyComparer<IEnumerable<PrNumberCombination>>(new PropertyComparerOptions
                {CollectionComparison = CollectionComparison.Unordered});

        [Fact]
        public void Ctor_VerifyInjection()
        {
            //Assert
            Assert.Injection.OfConstructor(typeof(ProhibitedOne));
        }
        
        [Fact]
        public void Formatting()
        {
            //Arrange
            var prNumbers = "AAA BBB CCC";
            var prNumberSet = PrNumberSet.Parse(prNumbers);
            var prNumbers2 = "DDD EEE";
            var prNumberSet2 = PrNumberSet.Parse(prNumbers2);
            var prohibitedOneExpression = new ProhibitedOne(prNumberSet, new []{new ProhibitedOne(prNumberSet2, null)});
            var expected = "[VO: AAA BBB CCC+[[VO: DDD EEE+[]]]]";

            //Act
            var result = prohibitedOneExpression.ToString();

            //Assert
            Assert.Equal(expected, result);
        }
        
        [Fact]
        public void Check()
        {
            //Arrange
            var configuration = "AAA EEE BBB";
            var prohibitedOneExpression = new ProhibitedOne(PrNumberSet.Parse("XXX ZZZ DDD"), null);
            var context = new RuleContext("", configuration);

            //Act
            var result = prohibitedOneExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Check_With_All()
        {
            //Arrange
            var configuration = "AAA EEE BBB";
            var prohibitedOneExpression = new ProhibitedOne(PrNumberSet.Parse("AAA EEE BBB"), null);
            var context = new RuleContext("", configuration);

            //Act
            var result = prohibitedOneExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Check_When_Failed()
        {
            //Arrange
            var configuration = "AAA EEE BBB";
            var prohibitedOneExpression = new ProhibitedOne(PrNumberSet.Parse("AAA XXX YYY"), null);
            var context = new RuleContext("", configuration);

            //Act
            var result = prohibitedOneExpression.Check(context);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void Check_WithNested()
        {
            //Arrange
            var configuration = "BBB ZZZ";
            var nestedExpression = new ProhibitedOne(PrNumberSet.Parse("ZZZ XXX"), null);
            var prohibitedOneExpression = new ProhibitedOne(PrNumberSet.Parse("BBB CCC"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = prohibitedOneExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Check_When_Failed_WithNested()
        {
            //Arrange
            var configuration = "CCC YYY EEE";
            var nestedExpression = new ProhibitedOne(PrNumberSet.Parse("ZZZ XXX"), null);
            var prohibitedOneExpression = new ProhibitedOne(PrNumberSet.Parse("BBB CCC DDD"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = prohibitedOneExpression.Check(context);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void Check_WhenNested_Failed()
        {
            //Arrange
            var configuration = "CCC BBB ZZZ";
            var nestedExpression = new ProhibitedOne(PrNumberSet.Parse("ZZZ XXX"), null);
            var prohibitedOneExpression = new ProhibitedOne(PrNumberSet.Parse("BBB CCC"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = prohibitedOneExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Check_WithNested_When_Failed()
        {
            //Arrange
            var configuration = "ZZZ BBB";
            var nestedExpression = new ProhibitedOne(PrNumberSet.Parse("ZZZ XXX"), null);
            var prohibitedOneExpression = new ProhibitedOne(PrNumberSet.Parse("BBB CCC"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = prohibitedOneExpression.Check(context);

            //Assert
            Assert.True(result);
        }

         [Fact]
        public void GetAllPossibleOptions()
        {
            //Arrange
            var prNumberSet = PrNumberSet.Parse("AAA BBB CCC");
            var prNumberSet2 = PrNumberSet.Parse("DDD EEE");
            var prNumberSet3 = PrNumberSet.Parse("CCC YYY");
            var prohibitedOneExpression = new ProhibitedOne(prNumberSet, new []
            {
                new ProhibitedOne(prNumberSet2, null), new ProhibitedOne(prNumberSet3, null)
            });
            var satisfying = new List<PrNumberCombination>
            {
                PrNumberCombination.IncludedOnly("AAA BBB CCC DDD EEE YYY"),
                PrNumberCombination.ExcludedOnly("AAA BBB CCC DDD EEE YYY"),
                PrNumberCombination.Both("AAA BBB CCC YYY", "DDD EEE"),
                PrNumberCombination.Both("DDD EEE", "AAA BBB CCC YYY"),
                PrNumberCombination.Both("BBB CCC DDD EEE YYY", "AAA"),
                PrNumberCombination.Both("AAA CCC DDD EEE YYY", "BBB"),
                PrNumberCombination.Both("BBB CCC YYY", "AAA DDD EEE"),
                PrNumberCombination.Both("AAA CCC YYY", "BBB DDD EEE"),
                PrNumberCombination.Both("AAA BBB DDD EEE", "CCC YYY"),
                PrNumberCombination.Both("AAA BBB", "CCC DDD EEE YYY"),
                PrNumberCombination.Both("CCC", "YYY"),
                PrNumberCombination.Both("YYY", "CCC"),
                PrNumberCombination.Both("EEE", "DDD"),
                PrNumberCombination.Both("DDD", "EEE")
            };
            var unsatisfying = new List<PrNumberCombination>
            {
                PrNumberCombination.Both("CCC", "AAA BBB"),
                PrNumberCombination.Both("BBB", "AAA CCC"),
                PrNumberCombination.Both("AAA", "BBB CCC")
            };
            
            //Act
            var result = prohibitedOneExpression.GetAllCombinations();
            
            //Assert
            Assert.NotNull(result);

            Assert.Equal(satisfying, result.Satisfying, _prNumberCombinationComparer);
            Assert.Equal(unsatisfying, result.Unsatisfying, _prNumberCombinationComparer);
        }
    }
}
