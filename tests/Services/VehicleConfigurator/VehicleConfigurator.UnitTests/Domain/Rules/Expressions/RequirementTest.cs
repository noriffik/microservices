﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Rules;
using NexCore.VehicleConfigurator.Domain.Rules.Expressions;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Rules.Expressions
{
    public class RequirementTest
    {
        private readonly PrNumberSet _prNumberSet = PrNumberSet.Parse("DDD EEE");
        private readonly PrNumberSet _prNumberSet2 = PrNumberSet.Parse("XXX YYY");
        private readonly Requirement _requirement;

        public RequirementTest()
        {
            _requirement = new Requirement(new PrNumberSetRule[]
            {
                new OnlyWithAll(_prNumberSet, null), 
                new OnlyWithOne(_prNumberSet2, null)
            });
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var expected = "[[ZA: DDD EEE+[]][ZO: XXX YYY+[]]]";

            //Act
            var result = _requirement.ToString();

            //Assert
            Assert.Equal(expected, result);
        }
        
        [Fact]
        public void Check()
        {
            //Arrange
            var context = new RuleContext("", "DDD EEE XXX");

            //Act
            var result = _requirement.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Check_When_Failed()
        {
            //Arrange
            var context = new RuleContext("", "DDD EEE XXX YYY");

            //Act
            var result = _requirement.Check(context);

            //Assert
            Assert.False(result);
        }
    }
}
