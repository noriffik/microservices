﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Rules;
using NexCore.VehicleConfigurator.Domain.Rules.Expressions;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Rules.Expressions
{
    public class ProhibitedAllTest
    {
        private readonly PropertyComparer<IEnumerable<PrNumberCombination>> _prNumberCombinationComparer =
            new PropertyComparer<IEnumerable<PrNumberCombination>>(new PropertyComparerOptions
                {CollectionComparison = CollectionComparison.Unordered});

        [Fact]
        public void Ctor_VerifyInjection()
        {
            //Assert
            Assert.Injection.OfConstructor(typeof(ProhibitedAll));
        }

        [Fact]
        public void Formatting()
        {
            //Arrange
            var prNumbers = "AAA BBB CCC";
            var prNumberSet = PrNumberSet.Parse(prNumbers);
            var prNumbers2 = "DDD EEE";
            var prNumberSet2 = PrNumberSet.Parse(prNumbers2);
            var prohibitedAllExpression = new ProhibitedAll(prNumberSet, new []{new ProhibitedAll(prNumberSet2, null)});
            var expected = "[VA: AAA BBB CCC+[[VA: DDD EEE+[]]]]";

            //Act
            var result = prohibitedAllExpression.ToString();

            //Assert
            Assert.Equal(expected, result);
        }
        
        [Fact]
        public void Check()
        {
            //Arrange
            var configuration = "AAA EEE BBB";
            var prohibitedAllExpression = new ProhibitedAll(PrNumberSet.Parse("AAA EEE CCC"), null);
            var context = new RuleContext("", configuration);

            //Act
            var result = prohibitedAllExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Check_When_Failed()
        {
            //Arrange
            var configuration = "AAA EEE BBB";
            var prohibitedAllExpression = new ProhibitedAll(PrNumberSet.Parse("AAA EEE BBB"), null);
            var context = new RuleContext("", configuration);

            //Act
            var result = prohibitedAllExpression.Check(context);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void Check_WithNested()
        {
            //Arrange
            var configuration = "CCC BBB ZZZ";
            var nestedExpression = new ProhibitedAll(PrNumberSet.Parse("ZZZ XXX"), null);
            var prohibitedAllExpression = new ProhibitedAll(PrNumberSet.Parse("BBB CCC DDD"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = prohibitedAllExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Check_When_Failed_WithNested()
        {
            //Arrange
            var configuration = "BBB CCC ZZZ";
            var nestedExpression = new ProhibitedAll(PrNumberSet.Parse("ZZZ XXX"), null);
            var prohibitedAllExpression = new ProhibitedAll(PrNumberSet.Parse("BBB CCC"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = prohibitedAllExpression.Check(context);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void Check_WhenNested_Failed()
        {
            //Arrange
            var configuration = "BBB ZZZ XXX";
            var nestedExpression = new ProhibitedAll(PrNumberSet.Parse("ZZZ XXX"), null);
            var prohibitedAllExpression = new ProhibitedAll(PrNumberSet.Parse("BBB CCC"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = prohibitedAllExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Check_WithNested_When_Failed()
        {
            //Arrange
            var configuration = "BBB CCC ZZZ XXX";
            var nestedExpression = new ProhibitedAll(PrNumberSet.Parse("ZZZ XXX"), null);
            var prohibitedAllExpression = new ProhibitedAll(PrNumberSet.Parse("BBB CCC"), new []{nestedExpression});
            
            var context = new RuleContext("", configuration);

            //Act
            var result = prohibitedAllExpression.Check(context);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void GetAllPossibleOptions()
        {
            //Arrange
            var prNumberSet = PrNumberSet.Parse("AAA BBB CCC");
            var prNumberSet2 = PrNumberSet.Parse("DDD EEE");
            var prNumberSet3 = PrNumberSet.Parse("EEE FFF");
            var prohibitedAllExpression = new ProhibitedAll(prNumberSet, new []
            {
                new ProhibitedAll(prNumberSet2, null), new ProhibitedAll(prNumberSet3, null)
            });
            var satisfying = new List<PrNumberCombination>
            {
                PrNumberCombination.ExcludedOnly("AAA BBB CCC DDD EEE FFF"),
                PrNumberCombination.IncludedOnly("DDD"),
                PrNumberCombination.IncludedOnly("EEE"),
                PrNumberCombination.IncludedOnly("FFF")
            };
            var unsatisfying = new List<PrNumberCombination>
            {
                PrNumberCombination.IncludedOnly("AAA"),
                PrNumberCombination.IncludedOnly("BBB"),
                PrNumberCombination.IncludedOnly("CCC")
            };

            //Act
            var result = prohibitedAllExpression.GetAllCombinations();

            //Assert
            Assert.NotNull(result);
            Assert.Equal(satisfying, result.Satisfying, _prNumberCombinationComparer);
            Assert.Equal(unsatisfying, result.Unsatisfying, _prNumberCombinationComparer);
        }
    }
}
