﻿using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Rules;
using System;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Rules
{
    public class OptionsTest
    {
        [Fact]
        public void CutConflicts()
        {
            //Arrange
            var optionsList = new List<PrNumberCombination>
            {
                PrNumberCombination.Both(PrNumberSet.Parse("AAA BBB"),PrNumberSet.Parse("CCC DDD")),
                PrNumberCombination.Both(PrNumberSet.Parse("EEE FFF"),PrNumberSet.Parse("CCC DDD")),
                PrNumberCombination.Both(PrNumberSet.Parse("AAA BBB"),PrNumberSet.Parse("CCC DDD")),
                PrNumberCombination.Both(PrNumberSet.Parse("AAA BBB CCC"),PrNumberSet.Parse("CCC DDD"))
            }; 

            var expected = new List<PrNumberCombination>
            {
                PrNumberCombination.Both(PrNumberSet.Parse("AAA BBB"), PrNumberSet.Parse("CCC DDD")),
                PrNumberCombination.Both(PrNumberSet.Parse("EEE FFF"), PrNumberSet.Parse("CCC DDD"))
            };

            //Act
            var result = optionsList.WithoutConflicts();

            //Assert
            Assert.Equal(expected, result, PropertyComparer<PrNumberCombination>.Instance);
        }

        [Fact]
        public void CutConflicts_WithNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("source", () => PrNumberCombinationExtensions.WithoutConflicts(null));
        }

        [Fact]
        public void CutConflicts_GivenList_IsEmpty_ReturnsEmpty()
        {
            //Act
            var result = new List<PrNumberCombination>().WithoutConflicts();

            //Assert
            Assert.Empty(result);
        }

        [Fact]
        public void MergeList()
        {
            //Arrange
            var firstList = new List<PrNumberCombination>
            {
                PrNumberCombination.IncludedOnly(PrNumberSet.Parse("AAA BBB"))
            }; 

            var secondList = new List<PrNumberCombination>
            {
                PrNumberCombination.Both(PrNumberSet.Parse("CCC"), PrNumberSet.Parse("DDD")),
                PrNumberCombination.Both(PrNumberSet.Parse("DDD"), PrNumberSet.Parse("CCC"))
            };

            var expected = new[]
            {
                PrNumberCombination.Both(PrNumberSet.Parse("AAA BBB CCC"), PrNumberSet.Parse("DDD")),
                PrNumberCombination.Both(PrNumberSet.Parse("AAA BBB DDD"), PrNumberSet.Parse("CCC"))
            };
            
            //Act
            var result = firstList.Merge(secondList);

            //Assert
            Assert.Equal(expected, result, PropertyComparer<IEnumerable<PrNumberCombination>>.Instance);
        }

        [Fact]
        public void MergeList_WithConflicts()
        {
            //Arrange
            var firstList = new List<PrNumberCombination>
            {
                PrNumberCombination.IncludedOnly(PrNumberSet.Parse("AAA BBB"))
            }; 

            var secondList = new List<PrNumberCombination>
            {
                PrNumberCombination.Both(PrNumberSet.Parse("CCC"),PrNumberSet.Parse("BBB")),
                PrNumberCombination.Both(PrNumberSet.Parse("BBB"),PrNumberSet.Parse("CCC"))
            };

            var expected = new[]
            {
                PrNumberCombination.Both(PrNumberSet.Parse("AAA BBB"), PrNumberSet.Parse("CCC"))
            };

            //Act
            var result = firstList.Merge(secondList).WithoutConflicts();
                
            //Assert
            Assert.Equal(expected, result, PropertyComparer<IEnumerable<PrNumberCombination>>.Instance);
        }

        [Fact]
        public void MergeList_WhenFirst_IsEmpty_ReturnsSecond()
        {
            //Arrange
            var firstList = new List<PrNumberCombination>(); 

            var secondList = new List<PrNumberCombination>
            {
                PrNumberCombination.Both(PrNumberSet.Parse("CCC"),PrNumberSet.Parse("BBB"))
            };

            var expected = new[]
            {
                PrNumberCombination.Both(PrNumberSet.Parse("CCC"), PrNumberSet.Parse("BBB"))
            };

            //Act
            var result = firstList.Merge(secondList).WithoutConflicts();
                
            //Assert
            Assert.Equal(expected, result, PropertyComparer<IEnumerable<PrNumberCombination>>.Instance);
        }

        [Fact]
        public void MergeList_WhenSecond_IsEmpty_ReturnsFirst()
        {
            //Arrange
            var firstList = new List<PrNumberCombination>
            {
                PrNumberCombination.Both(PrNumberSet.Parse("CCC"),PrNumberSet.Parse("BBB"))
            };

            var secondList = new List<PrNumberCombination>();

            var expected = new[]
            {
                PrNumberCombination.Both(PrNumberSet.Parse("CCC"), PrNumberSet.Parse("BBB"))
            };

            //Act
            var result = firstList.Merge(secondList).WithoutConflicts();
                
            //Assert
            Assert.Equal(expected, result, PropertyComparer<IEnumerable<PrNumberCombination>>.Instance);
        }

        [Fact]
        public void MergeList_WhenBoth_IsEmpty_ReturnsEmptyList()
        {
            //Arrange
            var firstList = new List<PrNumberCombination>(); 

            var secondList = new List<PrNumberCombination>(); 

            var expected = new List<PrNumberCombination>(); 

            //Act
            var result = firstList.Merge(secondList).WithoutConflicts();
                
            //Assert
            Assert.Equal(expected, result);
        }
    }

}
