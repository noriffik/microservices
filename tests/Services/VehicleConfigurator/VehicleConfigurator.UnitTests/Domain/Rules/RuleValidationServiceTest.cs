﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Rules;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Rules
{
    public class RuleValidationServiceTest
    {
        private const int VehicleOfferId = 1;

        //Entity
        private readonly VehicleOffer _vehicleOffer;

        //Service
        private readonly RuleValidationService _validator = new RuleValidationService();

        public RuleValidationServiceTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _vehicleOffer = fixture.Create<VehicleOffer>();
        }

        [Fact]
        public void HasConflict()
        {
            //Arrange
            var destinationOption = PrNumber.Parse("XXX");
            var inputRule = "[ZO: AAA BBB CCC+[]][VA: AAA BBB+[]][VA: CCC+[]]";

            //Act
            var result = _validator.HasConflict(inputRule, destinationOption, _vehicleOffer);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void HasConflict_WithSelfDependency()
        {
            //Arrange

            var inputRule = "[ZO: AAA BBB CCC+[]][VA: CCC+[]]";

            //Act
            var result = _validator.HasConflict(inputRule, PrNumber.Parse("CCC"), _vehicleOffer);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void HasConflict_WithDeepSelfDependency()
        {
            //Arrange
            var destinationOption = PrNumber.Parse("CCC");
            var inputRule = "[ZA: CCC+[]]";

            var optionA = PrNumber.Parse("AAA");
            var ruleA = "[ZA: BBB+[]]";
            var optionB = PrNumber.Parse("BBB");
            var ruleB = "[ZA: CCC+[]]";

            _vehicleOffer.AddOptionOffer(optionA, Availability.Purchasable(1));
            _vehicleOffer.AddOptionOffer(optionB, Availability.Purchasable(2));
            _vehicleOffer.AddOptionOffer(destinationOption, Availability.Purchasable(3));
            _vehicleOffer.AddOptionOfferRuleSet(optionA, ruleA);
            _vehicleOffer.AddOptionOfferRuleSet(optionB, ruleB);

            //Act
            var result = _validator.HasConflict(inputRule, destinationOption, _vehicleOffer);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void HasConflict_WhenNot_ReturnsFalse()
        {
            //Arrange
            var destinationOption = PrNumber.Parse("XXX");
            var inputRule = "[ZO: AAA BBB CCC+[[ZA: DDD EEE+[]][ZA: XXX YYY+[]]]][VA: AAA+[]]";

            //Act
            var result = _validator.HasConflict(inputRule, destinationOption, _vehicleOffer);

            //Assert
            Assert.False(result);
        }
    }
}
