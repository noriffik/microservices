﻿using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Rules;
using NexCore.VehicleConfigurator.Domain.Rules.Expressions;
using System;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Rules
{
    public class RuleParserTest
    {
        private readonly RuleParser _parser = new RuleParser();
        
        [Fact]
        public void OnlyWithAll_VerifyInjection()
        {
            //Assert
            Assert.Injection.OfProperty(typeof(RuleParser), "OnlyWithAll");
        }

        [Fact]
        public void OnlyWithOne_VerifyInjection()
        {
            //Assert
            Assert.Injection.OfProperty(typeof(RuleParser), "OnlyWithOne");
        }

        [Fact]
        public void ProhibitedAll_VerifyInjection()
        {
            //Assert
            Assert.Injection.OfProperty(typeof(RuleParser), "ProhibitedAll");
        }

        [Fact]
        public void ProhibitedOne_VerifyInjection()
        {
            //Assert
            Assert.Injection.OfProperty(typeof(RuleParser), "ProhibitedOne");
        }

        [Fact]
        public void IsValid()
        {
            //Arrange
            var inputRule = "[VA: AXC+[]][VA: AXC+[]]";

            //Act
            var result = _parser.IsValid(inputRule);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void IsValid_WhenNot_ReturnsFalse()
        {
            //Arrange
            var inputRule = "[VA: AXC+[]]VA: AXC+[]]";

            //Act
            var result = _parser.IsValid(inputRule);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void Parse()
        {
            //Arrange
            var input = "[ZA: DDD EEE FFF+[[ZO: AAA BBB+[]][ZO: XXX YYY+[]]]][VA: ZZZ+[]]";
            var expressions = new List<IExpression>
            {
                new OnlyWithAll(PrNumberSet.Parse("DDD EEE FFF"), 
                    new []
                    {
                        new OnlyWithOne(PrNumberSet.Parse("AAA BBB"), null), 
                        new OnlyWithOne(PrNumberSet.Parse("XXX YYY"), null)
                    }), 
                new ProhibitedAll(PrNumberSet.Parse("ZZZ"), null)
            };

            //Act
            var result = _parser.Parse(input);

            //Assert
            Assert.Equal(expressions, result, PropertyComparer<IExpression>.Instance);
        }

        [Theory]
        [InlineData("[ZA: DDDD+[]]")]
        [InlineData("[ZA: DD+[]]")]
        [InlineData("[ZA: D+[]]")]
        [InlineData("[ZO: DDD DD+[]]")]
        [InlineData("[ZO: DDD ADD+[]")]
        [InlineData("ZO: DDD ADD+[]]")]
        [InlineData("[ZO: ###+[]]")]
        [InlineData("[V: DDD AAA+[]]")]
        [InlineData("[Z: DDD AAA+[]]")]
        [InlineData("[VA= DDD+[]]")]
        [InlineData("[VO= DDD+[]]")]
        public void Parse_WithInvalidRule(string input)
        {
            //Act
            var e = Assert.Throws<FormatException>(() => _parser.Parse(input));

            //Assert
            Assert.Contains("invalid format", e.Message);
        }
    }
}
