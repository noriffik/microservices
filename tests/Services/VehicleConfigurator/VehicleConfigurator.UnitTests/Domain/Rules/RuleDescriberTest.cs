﻿using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Rules;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Rules
{
    public class RuleDescriberTest
    {
        private readonly Regex _empty = new Regex("");

        [Fact]
        public void GetRules()
        {
            //Arrange
            var ruleSet = "[1[A]][2[B[2BA]]][3[C]]";
            var expected = new[]
            {
                "[1[A]]",
                "[2[B[2BA]]]",
                "[3[C]]"
            };
            var describer = new RuleDescriber('[', ']', _empty);

            //Act
            var actual = describer.GetRules(ruleSet);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("[")]
        [InlineData("]")]
        [InlineData("[1[]]2[]]")]
        public void GetRules_WhenRuleIs_NotEnded_Throws(string ruleSet)
        {
            //Arrange
            var describer = new RuleDescriber('[', ']', _empty);

            //Assert
            Assert.Throws<FormatException>(() => describer.GetRules(ruleSet));
        }
        
        [Fact]
        public void GetRules_WhenRule_IsNotFound_ReturnsEmpty()
        {
            //Arrange
            var describer = new RuleDescriber('[', ']', _empty);

            //Act
            var result = describer.GetRules("asdkj");

            //Assert
            Assert.Empty(result);
        }

        [Fact]
        public void GetRules_WhenRule_ValidationFailed_ReturnsNull()
        {
            //Arrange
            var ruleSet = "[1][23][3]";
            var validator = new Regex("[[].{1}[]]");
            var describer = new RuleDescriber('[', ']', validator);

            //Assert
            Assert.Throws<FormatException>(() => describer.GetRules(ruleSet));
        }

        [Fact]
        public void GetRules_Using_DefaultSettings()
        {
            //Arrange
            var ruleSet = "[ZA: AAA+[[ZA: BBB+[]][ZO: CCC DDD+[[VO: FFF DDD+[]][VA: GGG+[]]]]]][VA: EEE+[]][ZO: XXX YYY+[]]";
            var expected = new[]
            {
                "[ZA: AAA+[[ZA: BBB+[]][ZO: CCC DDD+[[VO: FFF DDD+[]][VA: GGG+[]]]]]]",
                "[VA: EEE+[]]",
                "[ZO: XXX YYY+[]]"
            };
            var describer = new RuleDescriber();

            //Act
            var result = describer.GetRules(ruleSet);

            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Describe()
        {
            //Arrange
            var rule = "[ZO: XXX YYY+[]]";
            var expected = new RuleDescription(rule);
            var describer = new RuleDescriber();

            //Act
            var actual = describer.Describe(rule);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<IRuleDescription>.Instance);
        }

        [Fact]
        public void DescribeMany()
        {
            //Arrange
            var ruleSet = "[VA: EEE+[]][ZO: XXX YYY+[]]";
            var rules = new[]
            {
                "[VA: EEE+[]]",
                "[ZO: XXX YYY+[]]"
            };
            var expected = rules.Select(r => new RuleDescription(r)).ToList();
            var describer = new RuleDescriber();

            //Act
            var actual = describer.DescribeMany(ruleSet);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<IRuleDescription>.Instance);
        }
    }
}
