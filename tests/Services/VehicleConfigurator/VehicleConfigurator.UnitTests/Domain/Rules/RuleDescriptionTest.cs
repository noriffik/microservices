﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Rules;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Rules
{
    public class RuleDescriptionTest
    {
        private const string Rule = "[ZA: DDD EEE FFF+[[ZO: AAA BBB+[]][ZO: XXX YYY+[]]]]";

        private readonly RuleDescription _description = new RuleDescription(Rule);

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Ctor_GivenRule_IsNull_Throws(string rule)
        {
            Assert.Throws<ArgumentException>("rule", () => new RuleDescription(rule));
        }

        [Fact]
        public void GetRule()
        {
            Assert.Equal(Rule, _description.Rule);
        }
        
        [Fact]
        public void GetName()
        {
            Assert.Equal("ZA", _description.Name);
        }

        [Fact]
        public void GetPrNumbers()
        {
            Assert.Equal(PrNumberSet.Parse("DDD EEE FFF"), _description.PrNumbers);
        }

        [Fact]
        public void GetRequirement()
        {
            Assert.Equal("[ZO: AAA BBB+[]][ZO: XXX YYY+[]]", _description.Requirement);
        }
    }
}
