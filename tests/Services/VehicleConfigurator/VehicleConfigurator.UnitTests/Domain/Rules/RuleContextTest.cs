﻿using NexCore.VehicleConfigurator.Domain.Rules;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Rules
{
    public class RuleContextTest
    {
        private readonly IRuleParser _parser = new RuleParser();

        [Fact]
        public void Ctor_VerifyInjection()
        {
            //Assert
            Assert.Injection.OfConstructor(typeof(RuleContext));
        }

        [Fact]
        public void Ctor_CheckRuleSet()
        {
            //Arrange
            var input = "[VA: AAA BBB+[[ZA: CCC+[[ZO: DDD EEE FFF+[]]]]]][ZA: ZZZ+[]]";
            var prNumbers = "AAA EEE CCC ZZZ";
            
            var context = new RuleContext(input, prNumbers);

            //Act
            var results = new List<bool>();
            foreach (var expression in _parser.Parse(input))
            {
                results.Add(expression.Check(context));
            }

            //Assert
            Assert.All(results, Assert.True);
        }

        [Fact]
        public void Ctor_CheсkRuleSet_WhenRuleFailed_AndConditionIsTrue()
        {
            //Arrange
            var input = "[VA: AAA BBB+[[ZA: CCC+[[ZO: DDD EEE FFF+[]]]]]]";
            var prNumbers = "AAA BBB EEE CCC ZZZ";
            
            var context = new RuleContext(input, prNumbers);

            //Act
            var results = new List<bool>();
            foreach (var expression in _parser.Parse(input))
            {
                results.Add(expression.Check(context));
            }

            //Assert
            Assert.All(results, Assert.False);
        }

        [Fact]
        public void Ctor_CheсkRuleSet_WhenConditionFailed()
        {
            //Arrange
            var input = "[VA: AAA BBB+[[ZA: CCC+[[ZO: DDD EEE FFF+[[VO: GGG+[]]]]]]]]";
            var prNumbers = "AAA EEE CCC GGG";
            
            var context = new RuleContext(input, prNumbers);

            //Act
            var results = new List<bool>();
            foreach (var expression in _parser.Parse(input))
            {
                results.Add(expression.Check(context));
            }

            //Assert
            Assert.All(results, Assert.True);
        }
    }
}
