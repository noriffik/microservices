﻿using NexCore.Domain;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Rules;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Rules
{
    public class PrNumberSetRuleCombinationsTest
    {
        [Fact]
        public void WithoutConflicts()
        {
            //Arrange
            var satisfying = new List<PrNumberCombination>
            {
                PrNumberCombination.Both("AAA BBB", "CCC DDD"),
                PrNumberCombination.Both("EEE FFF", "CCC DDD"),
                PrNumberCombination.Both("AAA BBB", "CCC DDD"),
                PrNumberCombination.Both("AAA BBB CCC", "CCC DDD")
            }; 
            var unsatisfying =  new List<PrNumberCombination>
            {
                PrNumberCombination.Both("AAA BBB", "CCC DDD"),
                PrNumberCombination.Both("XXX YYY", "CCC DDD"),
                PrNumberCombination.Both("AAA BBB", "CCC DDD"),
                PrNumberCombination.Both("AAA BBB CCC", "CCC DDD")
            };

            var expected = new PrNumberSetRuleCombinations(
                new List<PrNumberCombination>
                {
                    PrNumberCombination.Both("AAA BBB", "CCC DDD"),
                    PrNumberCombination.Both("EEE FFF", "CCC DDD")
                },
                new List<PrNumberCombination>
                {
                    PrNumberCombination.Both("AAA BBB", "CCC DDD"),
                    PrNumberCombination.Both("XXX YYY", "CCC DDD")
                });

            var combinations = new PrNumberSetRuleCombinations(satisfying, unsatisfying);

            //Act
            var actual = combinations.WithoutConflicts();

            //Assert
            Assert.Equal(expected, actual, new PropertyComparer<PrNumberSetRuleCombinations>(new PropertyComparerOptions
            {
                CollectionComparison = CollectionComparison.Unordered
            }));
        }
    }
}
