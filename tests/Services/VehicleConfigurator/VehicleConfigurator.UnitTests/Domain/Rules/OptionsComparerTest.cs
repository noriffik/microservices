﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Rules;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Rules
{
    public class OptionsComparerTest
    {
        //public OptionsComparer OptionsComparer = OptionsComparer.Instance;

        [Fact]
        public void Equals()
        {
            //Arrange
            var first = PrNumberCombination.Both(PrNumberSet.Parse("AAA BBB"), PrNumberSet.Parse("CCC DDD"));
            var second = PrNumberCombination.Both(PrNumberSet.Parse("AAA BBB"), PrNumberSet.Parse("CCC DDD"));

            //Assert
            Assert.Equal(first, second);
        }

        [Fact]
        public void Equals_WhenNot()
        {
            //Arrange
            var first = PrNumberCombination.Both(PrNumberSet.Parse("AAA BBB"), PrNumberSet.Parse("CCC DDD"));
            var second = PrNumberCombination.Both(PrNumberSet.Parse("AAA BBB"), PrNumberSet.Parse("CCC EEE"));

            //Assert
            Assert.NotEqual(first, second);
        }
    }
}
