﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Vehicles
{
    public class OptionTest
    {
        private readonly OptionId _id = OptionId.Parse("NJPZB");
        private readonly PrNumber _prNumber = PrNumber.Parse("PZB");

        [Fact]
        public void Ctor()
        {
            //Act
            var option = new Option(_id);

            //Arrange
            Assert.Equal(_id, option.Id);
            Assert.Equal(_prNumber, option.PrNumber);
        }

        [Fact]
        public void Ctor_GivenId_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new Option(null);
            });
            Assert.Equal("id", e.ParamName);
        }

        [Fact]
        public void Formatting()
        {
            Assert.Equal(_id.ToString(), new Option(_id).ToString());
            Assert.Equal($"{_id}, name", new Option(_id) { Name = "name" }.ToString());
        }
    }
}
