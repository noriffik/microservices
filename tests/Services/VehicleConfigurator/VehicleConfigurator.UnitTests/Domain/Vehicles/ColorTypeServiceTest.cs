﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Vehicles
{
    public class ColorTypeServiceTest
    {
        //Dependencies
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IUnitOfWork> _work;

        //Service
        private readonly ColorTypeService _service;

        //Entity
        private readonly ColorType _colorType;

        public ColorTypeServiceTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _repository = new Mock<IEntityRepository>();
            _work = new Mock<IUnitOfWork>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Service
            _service = new ColorTypeService(_work.Object);

            //Entity
            _colorType = fixture.Create<ColorType>();
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ColorTypeService)).HasNullGuard();
        }

        [Fact]
        public Task Add_WhenGivenPackage_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("colorType", () => _service.Add(null));
        }

        [Fact]
        public async Task Add()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Has<ColorType, ColorTypeId>(_colorType.Id))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Add<ColorType, ColorTypeId>(_colorType))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _service.Add(_colorType);
            }
        }

        [Fact]
        public async Task Add_WhenColorType_AlreadyExist_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Has<ColorType, ColorTypeId>(_colorType.Id))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.ThrowsAsync<DuplicateEntityException>(() =>
                _service.Add(_colorType));

            //Assert
            Assert.Equal(typeof(ColorType), e.EntityType);
            Assert.Equal(_colorType.Id, e.Id);
        }
    }
}
