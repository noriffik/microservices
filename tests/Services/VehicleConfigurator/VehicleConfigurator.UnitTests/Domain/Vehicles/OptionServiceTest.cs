﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Vehicles
{
    public class OptionServiceTest
    {
        //Entities
        private readonly Option _option;
        private readonly int _categoryId;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Service
        private readonly OptionService _service;

        public OptionServiceTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entities
            _option = fixture.Create<Option>();
            _categoryId = fixture.Create<int>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Service
            _service = new OptionService(_work.Object);
        }

        [Fact]
        public void Ctor_VerifyInjections()
        {
            Assert.Injection.OfConstructor(typeof(OptionService)).HasNullGuard();
        }

        [Fact]
        public async Task AssignToCategory()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.HasRequired<OptionCategory>(_categoryId))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.Require<Option, OptionId>(_option.Id))
                    .InSequence()
                    .ReturnsAsync(_option);

                //Act
                await _service.AssignToCategory(_option.Id, _categoryId);
            }

            //Assert
            Assert.Equal(_categoryId, _option.OptionCategoryId);
        }

        [Fact]
        public Task AssignToCategory_GivenOptionId_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>(() => _service.AssignToCategory(null, _categoryId));
        }

        [Fact]
        public async Task Delete()
        {
            //Arrange
            using (Sequence.Create())
            {
                _repository.Setup(r => r.SingleOrDefault<Option, OptionId>(_option.Id))
                    .InSequence()
                    .ReturnsAsync(_option);
                _repository.Setup(r => r.Has(It.Is<VehicleOfferOptionIdSpecification>(s => s.OptionId == _option.Id)))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Remove<Option, OptionId>(_option))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _service.Delete(_option.Id);
            }
        }

        [Fact]
        public async Task Delete_WhenOption_IsInUse_Throws()
        {
            //Arrange
            _repository.Setup(r => r.SingleOrDefault<Option, OptionId>(_option.Id))
                .ReturnsAsync(_option);
            _repository.Setup(r => r.Has(It.Is<VehicleOfferOptionIdSpecification>(s => s.OptionId == _option.Id)))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.ThrowsAsync<ComponentInUseException>(() => _service.Delete(_option.Id));

            //Assert
            Assert.Equal(typeof(Option), e.ComponentType);
            Assert.Equal(_option.Id, e.Id);
        }
    }
}
