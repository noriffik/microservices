﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Vehicles
{
    public class GearboxServiceTest
    {
        //Entities
        private readonly Gearbox _component;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Service
        private readonly GearboxService _service;

        public GearboxServiceTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entities
            _component = fixture.Create<Gearbox>();


            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Service
            _service = new GearboxService(_work.Object);
        }

        [Fact]
        public async Task Delete()
        {
            //Arrange
            using (Sequence.Create())
            {
                _repository.Setup(r => r.SingleOrDefault<Gearbox, GearboxId>(_component.Id))
                    .InSequence()
                    .ReturnsAsync(_component);
                _repository.Setup(r => r.Has(It.Is<VehicleOfferGearboxIdSpecification>(s => s.GearboxId == _component.Id)))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Remove<Gearbox, GearboxId>(_component))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _service.Delete(_component.Id);
            }
        }

        [Fact]
        public async Task Delete_WhenComponent_IsInUse_Throws()
        {
            //Arrange
            _repository.Setup(r => r.SingleOrDefault<Gearbox, GearboxId>(_component.Id))
                .ReturnsAsync(_component);
            _repository.Setup(r => r.Has(It.Is<VehicleOfferGearboxIdSpecification>(s => s.GearboxId == _component.Id)))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.ThrowsAsync<ComponentInUseException>(() => _service.Delete(_component.Id));

            //Assert
            Assert.Equal(typeof(Gearbox), e.ComponentType);
            Assert.Equal(_component.Id, e.Id);
        }
    }
}
