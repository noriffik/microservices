﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Vehicles
{
    public class BodyTest
    {
        private readonly BodyId _id = BodyId.Parse("NF1");

        [Fact]
        public void Ctor()
        {
            //Act
            var body = new Body(_id);

            //Arrange
            Assert.Equal(_id, body.Id);
            Assert.Equal(ModelId.Parse("NF"), body.ModelId);
        }

        [Fact]
        public void Ctor_GivenId_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new Body(null);
            });
            Assert.Equal("id", e.ParamName);
        }

        [Fact]
        public void Formatting()
        {
            Assert.Equal(_id.ToString(), new Body(_id).ToString());
            Assert.Equal($"{_id}, name", new Body(_id) { Name = "name" }.ToString());
        }
    }
}
