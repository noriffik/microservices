﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Vehicles.Specifications
{
    public class OptionWithPrNumberSpecificationTest
    {
        private readonly OptionWithPrNumberSpecification _specification;
        private readonly ModelId _modelId;
        private readonly PrNumberSet _prNumbers;

        private readonly Option[] _sequence;        
        private readonly Option[] _expected;
        private readonly Option[] _unexpected;

        public OptionWithPrNumberSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _modelId = fixture.Create<ModelId>();
            _prNumbers = fixture.Create<PrNumberSet>();

            _expected = _prNumbers.Values
                .Select(i => fixture.Construct<Option>(new { optionId = OptionId.Parse($"{_modelId.Value}{i.Value}") }))
                .ToArray();

            _unexpected = Enumerable.Range(0, 4)
                .Select(i => fixture.Construct<Option>(new { optionId = OptionId.Parse($"{_modelId.Value}{i}XX") }))
                .ToArray();

            _sequence = _expected.Concat(_unexpected).ToArray();

            _specification = new OptionWithPrNumberSpecification(_prNumbers, _modelId);
        }

        [Fact]
        public void Ctor_GivenPrNumbers_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prNumbers", () => new OptionWithPrNumberSpecification(null, _modelId));
        }

         [Fact]
        public void Ctor_GivenModelId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelId", () => new OptionWithPrNumberSpecification(_prNumbers, null));
        }

        [Fact]
        public void IsSatisfied_ReturnsTrue()
        {
            Assert.True(_specification.IsSatisfiedBy(_expected[0]));
        }

        [Fact]
        public void IsSatisfied_ReturnsFalse()
        {
            Assert.False(_specification.IsSatisfiedBy(_unexpected[0]));
        }

        [Fact]
        public void ToExpression()
        {
            //Act
            var expression = _specification.ToExpression();

            //Assert
            Assert.NotNull(expression);
            Assert.Equal(_expected, _sequence.AsQueryable().Where(expression));
        }
    }
}
