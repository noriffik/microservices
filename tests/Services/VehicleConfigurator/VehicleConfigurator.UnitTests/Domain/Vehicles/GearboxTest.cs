﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Vehicles
{
    public class GearboxTest
    {
        private readonly GearboxId _id = GearboxId.Parse("NF1");
        private readonly GearboxType _type = GearboxType.Parse("7DSG 4X4");

        [Fact]
        public void Ctor()
        {
            //Act
            var gearbox = new Gearbox(_id, _type);

            //Arrange
            Assert.Equal(_id, gearbox.Id);
            Assert.Equal(_type, gearbox.Type);
            Assert.Equal(ModelId.Parse("NF"), gearbox.ModelId);
        }

        [Fact]
        public void Ctor_GivenId_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new Gearbox(null, _type);
            });
            Assert.Equal("id", e.ParamName);
        }

        [Fact]
        public void Formatting()
        {
            Assert.Equal(_type.ToString(), new Gearbox(_id, _type).ToString());
            Assert.Equal($"{_type}, name", new Gearbox(_id, _type) { Name = "name" }.ToString());
        }
    }
}
