﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Vehicles
{
    public class ColorServiceTest
    {
        //Dependencies
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IUnitOfWork> _work;

        //Service
        private readonly ColorService _service;

        //Entity
        private readonly Color _color;

        public ColorServiceTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _repository = new Mock<IEntityRepository>();
            _work = new Mock<IUnitOfWork>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Service
            _service = new ColorService(_work.Object);

            //Entity
            _color = fixture.Create<Color>();
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ColorTypeService)).HasNullGuard();
        }

        [Fact]
        public Task Add_WhenGivenPackage_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("color", () => _service.Add(null));
        }

        [Fact]
        public async Task Add()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.HasRequired<ColorType, ColorTypeId>(_color.TypeId))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.Has<Color, ColorId>(_color.Id))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Add<Color, ColorId>(_color))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _service.Add(_color);
            }
        }

        [Fact]
        public async Task Add_WhenColor_AlreadyExist_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Has<Color, ColorId>(_color.Id))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.ThrowsAsync<DuplicateEntityException>(() =>
                _service.Add(_color));

            //Assert
            Assert.Equal(typeof(Color), e.EntityType);
            Assert.Equal(_color.Id, e.Id);
        }

    }
}
