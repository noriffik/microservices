﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Vehicles
{
    public class EquipmentTest
    {
        private readonly EquipmentId _id = EquipmentId.Parse("NF3");

        [Fact]
        public void Ctor()
        {
            //Act
            var equipment = new Equipment(_id);

            //Arrange
            Assert.Equal(_id, equipment.Id);
            Assert.Equal(ModelId.Parse("NF"), equipment.ModelId);
        }

        [Fact]
        public void Ctor_GivenId_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new Equipment(null);
            });
            Assert.Equal("id", e.ParamName);
        }

        [Fact]
        public void Formatting()
        {
            Assert.Equal(_id.ToString(), new Equipment(_id).ToString());
            Assert.Equal($"{_id}, name", new Equipment(_id) { Name = "name" }.ToString());
        }
    }
}
