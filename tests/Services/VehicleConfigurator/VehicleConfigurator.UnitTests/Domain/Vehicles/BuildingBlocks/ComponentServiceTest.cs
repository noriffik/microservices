﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Testing;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.VehicleConfigurator.UnitTests.Fixtures;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Vehicles.BuildingBlocks
{
    public class ComponentServiceTest
    {
        //Test class
        private class TestComponentService : ComponentService<TestComponent, TestId>
        {
            public TestComponentService(IUnitOfWork work) : base(work)
            {
            }
        }

        //Entities
        private readonly TestComponent _component;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Service
        private readonly TestComponentService _service;

        public ComponentServiceTest()
        {
            var fixture = new Fixture()
                .CustomizeConstructor<TestComponent, TestId>(new TestId("AAA"));

            //Entities
            _component = fixture.Create<TestComponent>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Service
            _service = new TestComponentService(_work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(TestComponentService)).HasNullGuard();
        }

        [Fact]
        public Task Add_GivenComponent_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("component", () =>
                _service.Add(null));
        }

        [Fact]
        public async Task Add_GivenComponentId_IsInUse_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Has<TestComponent, TestId>(_component.Id)).ReturnsAsync(true);

            //Act
            var e = await Assert.ThrowsAsync<DuplicateEntityException>(() =>
                _service.Add(_component));

            //Assert
            Assert.Equal(_component.GetType(), e.EntityType);
            Assert.Equal(_component.Id, e.Id);
        }

        [Fact]
        public async Task Add()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Has<TestComponent, TestId>(_component.Id))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository
                    .Setup(r => r.Add<TestComponent, TestId>(_component))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _service.Add(_component);
            }
        }

        [Fact]
        public async Task Delete()
        {
            //Arrange
            using (Sequence.Create())
            {
                _repository.Setup(r => r.SingleOrDefault<TestComponent, TestId>(_component.Id))
                    .InSequence()
                    .ReturnsAsync(_component);
                _repository.Setup(r => r.Remove<TestComponent, TestId>(_component))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _service.Delete(_component.Id);
            }
        }

        [Fact]
        public async Task Delete_WhenComponentId_NotFound_Throws()
        {
            //Arrange
            _repository.Setup(r => r.SingleOrDefault<TestComponent, TestId>(_component.Id))
                .ReturnsAsync(null as TestComponent);

            //Act
            var e = await Assert.ThrowsAsync<InvalidEntityIdException>(() =>
                _service.Delete(_component.Id));

            //Assert
            Assert.Contains("does not exist", e.Message);
        }

        [Fact]
        public Task Delete_GivenComponentId_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("id", () =>
                _service.Delete(null));
        }
    }
}
