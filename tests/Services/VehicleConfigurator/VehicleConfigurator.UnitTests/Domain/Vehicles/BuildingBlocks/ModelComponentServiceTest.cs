﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.VehicleConfigurator.UnitTests.Fixtures;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Vehicles.BuildingBlocks
{
    public class ModelComponentServiceTest
    {
        //Test class
        private class TestModelComponentService : ModelComponentService<TestModelComponent, TestModelComponentId>
        {
            public TestModelComponentService(IUnitOfWork work) : base(work)
            {
            }
        }
        //Entities
        private readonly TestModelComponent _component;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Service
        private readonly TestModelComponentService _service;

        public ModelComponentServiceTest()
        {
            var fixture = new Fixture()
                .CustomizeConstructor<TestModelComponent, TestModelComponentId>(
                    new TestModelComponentId("AAA"));

            //Entities
            _component = fixture.Create<TestModelComponent>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Service
            _service = new TestModelComponentService(_work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(TestModelComponentService)).HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Has<TestModelComponent, TestModelComponentId>(_component.Id))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.HasRequired<Model, ModelId>(_component.ModelId))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.Add<TestModelComponent, TestModelComponentId>(_component))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                return _service.Add(_component);
            }
        }

        [Fact]
        public async Task Add_GivenComponentId_IsInUse_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Has<TestModelComponent, TestModelComponentId>(_component.Id))
                .ReturnsAsync(true);

            //Act
            var e = await Assert.ThrowsAsync<DuplicateEntityException>(() => _service.Add(_component));

            //Assert
            Assert.Equal(_component.GetType(), e.EntityType);
            Assert.Equal(_component.Id, e.Id);
        }

        [Fact]
        public Task Add_GivenComponent_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("component", () => _service.Add(null));
        }
    }
}
