﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Vehicles
{
    public class EngineTest
    {
        private readonly EngineId _id = EngineId.Parse("NFA");

        [Fact]
        public void Ctor()
        {
            //Act
            var engine = new Engine(_id);

            //Arrange
            Assert.Equal(_id, engine.Id);
            Assert.Equal(ModelId.Parse("NF"), engine.ModelId);
            Assert.Null(engine.FuelType);
        }

        [Fact]
        public void Ctor_GivenId_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new Engine(null);
            });
            Assert.Equal("id", e.ParamName);
        }

        [Fact]
        public void FuelTypeProperty()
        {
            //Arrange
            var engine = new Engine(_id);

            //Act
            engine.FuelType = FuelType.Petrol;

            //Assert
            Assert.Equal(FuelType.Petrol, engine.FuelType);
        }

        [Fact]
        public void Formatting()
        {
            Assert.Equal(_id.ToString(), new Engine(_id).ToString());
            Assert.Equal($"{_id}, name", new Engine(_id) { Name = "name" }.ToString());
        }
    }
}
