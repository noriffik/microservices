﻿using AutoFixture;
using Moq;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Vehicles
{
    public class ModelKeyValidatorTest
    {
        //Entity
        private readonly IEnumerable<ModelKey> _modelKeys;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Validator
        private readonly IModelKeyValidator _validator;

        public ModelKeyValidatorTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _modelKeys = fixture.CreateMany<ModelKey>(5);

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Validator
            _validator = new ModelKeyValidator(_work.Object);
        }

        [Fact]
        public async Task Validate()
        {
            //Arrange
            var modelKey = _modelKeys.First();

            _repository.Setup(r => r.HasRequired<Model, ModelId>(modelKey.ModelId, It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask)
                .Verifiable();
            _repository.Setup(r => r.HasRequired<Body, BodyId>(modelKey.BodyId, It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask)
                .Verifiable();
            _repository.Setup(r => r.HasRequired<Equipment, EquipmentId>(modelKey.EquipmentId, It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask)
                .Verifiable();
            _repository.Setup(r => r.HasRequired<Engine, EngineId>(modelKey.EngineId, It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask)
                .Verifiable();
            _repository.Setup(r => r.HasRequired<Gearbox, GearboxId>(modelKey.GearboxId, It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask)
                .Verifiable();

            //Act
            await _validator.Validate(modelKey);

            //Assert
            _repository.Verify();
        }

        [Fact]
        public Task Validate_WhenGivenModelKey_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("modelKey", () => _validator.Validate(null));
        }

        [Fact]
        public async Task ValidateMany()
        {
            //Arrange
            var modelIds = _modelKeys.Select(k => k.ModelId).Distinct();
            var bodyIds = _modelKeys.Select(k => k.BodyId).Distinct();
            var equipmentIds = _modelKeys.Select(k => k.EquipmentId).Distinct();
            var engineIds = _modelKeys.Select(k => k.EngineId).Distinct();
            var gearboxIds = _modelKeys.Select(k => k.GearboxId).Distinct();

            _repository.Setup(r => r.HasManyRequired<Model, ModelId>(modelIds, It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask)
                .Verifiable();
            _repository.Setup(r => r.HasManyRequired<Body, BodyId>(bodyIds, It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask)
                .Verifiable();
            _repository.Setup(r => r.HasManyRequired<Equipment, EquipmentId>(equipmentIds, It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask)
                .Verifiable();
            _repository.Setup(r => r.HasManyRequired<Engine, EngineId>(engineIds, It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask)
                .Verifiable();
            _repository.Setup(r => r.HasManyRequired<Gearbox, GearboxId>(gearboxIds, It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask)
                .Verifiable();

            //Act
            await _validator.ValidateMany(_modelKeys);

            //Assert
            _repository.Verify();
        }


        [Fact]
        public Task ValidateMany_WhenGivenModelKeys_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("modelKeys", () => _validator.ValidateMany(null));
        }

    }
}
