﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Vehicles
{
    public class ModelTest
    {
        private readonly ModelId _id = ModelId.Parse("NF");

        [Fact]
        public void Ctor()
        {
            //Act
            var model = new Model(_id);

            //Arrange
            Assert.Equal(_id, model.Id);
        }

        [Fact]
        public void Ctor_GivenId_IsNull_Throws()
        {
            var e = Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new Model(null);
            });
            Assert.Equal("id", e.ParamName);
        }

        [Fact]
        public void Formatting()
        {
            Assert.Equal(_id.ToString(), new Model(_id).ToString());
            Assert.Equal($"{_id}, Rapid", new Model(_id) { Name = "Rapid" }.ToString());
        }
    }
}
