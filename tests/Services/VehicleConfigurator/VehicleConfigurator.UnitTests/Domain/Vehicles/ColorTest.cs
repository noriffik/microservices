﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Vehicles
{
    public class ColorTest
    {
        private readonly IFixture _fixture;

        private readonly ColorId _id;
        private readonly System.Drawing.Color _argb;
        private readonly ColorTypeId _typeId;
        private readonly string _name;
        private readonly Color _color;

        public ColorTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            _id = _fixture.Create<ColorId>();
            _argb = _fixture.Create<System.Drawing.Color>();
            _typeId = _fixture.Create<ColorTypeId>();
            _name = _fixture.Create<string>();

            _color = new Color(_id, _argb, _typeId, _name);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Equal(_id, _color.Id);
            Assert.Equal(_argb, _color.Argb);
            Assert.Equal(_typeId, _color.TypeId);
            Assert.Equal(_name, _color.Name);
        }

        [Fact]
        public void Ctor_GivenId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("id", () => new Color(null, _argb, _typeId, _name));
        }

        [Fact]
        public void Ctor_GivenColorTypeId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("typeId", () => new Color(_id, _argb, null, _name));
        }

        [Fact]
        public void Name()
        {
            //Arrange
            var name = _fixture.Create<string>();

            //Act
            _color.Name = name;

            //Assert
            Assert.Equal(name, _color.Name);
        }

        [Fact]
        public void Argb()
        {
            //Arrange
            var argb = _fixture.Create<System.Drawing.Color>();

            //Act
            _color.Argb = argb;

            //Assert
            Assert.Equal(argb, _color.Argb);
        }

        [Fact]
        public void TypeId()
        {
            //Arrange
            var typeId = _fixture.Create<ColorTypeId>();

            //Act
            _color.TypeId = typeId;

            //Assert
            Assert.Equal(typeId, _color.TypeId);
        }
    }
}
