﻿using AutoFixture;
using NexCore.Common;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Offers
{
    public class OfferTest
    {
        private readonly Fixture _fixture;
        private readonly Offer _offer;
        private readonly Configuration _configuration;
        private readonly PrNumberSet _includedOptions;
        private readonly VehicleOffer _vehicleOffer;
        private readonly Year _modelYear;
        private readonly ConfigurationPrice _price;
        private readonly DateTime _testDate = new DateTime(2019, 1, 1);

        public OfferTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            _configuration = _fixture.Construct<Configuration>(1234);
            _configuration.IncludePackage(_fixture.Create<PackageId>());
            _includedOptions = PrNumberSet.Parse("AAA");
            _vehicleOffer = new VehicleOffer(_configuration.ModelKey, _configuration.CatalogId);
            _vehicleOffer.AddOptionOffer(PrNumber.Parse("AAA"), Availability.Included(ReasonType.SerialEquipment),
                Inclusion.Automatic);
            _modelYear = _fixture.Create<Year>();
            _price = _fixture.Create<ConfigurationPrice>();
            _offer = _fixture.Create<Offer>();
        }

        [Fact]
        public void Ctor()
        {
            using (new SystemTimeContext(_testDate))
            {
                //Act
                var result = new Offer(_modelYear, _configuration.ModelKey, _configuration.Options, _includedOptions, _price, _configuration.ColorId);

                //Assert
                Assert.Equal(_modelYear, result.ModelYear);
                Assert.Null(result.PackageId);
                Assert.Equal(_configuration.Options, result.Options);
                Assert.Equal(_includedOptions, result.IncludedOptions);
                Assert.Equal(_testDate, result.CreatedAt);
                Assert.Equal(_price, result.Price);
                Assert.Equal("38001", result.DealerId);
                Assert.Equal(_configuration.ColorId, result.ColorId);
                Assert.Equal(result.ModelKey, _configuration.ModelKey);
                Assert.Equal(result.ModelId, _configuration.ModelKey.ModelId);
                Assert.Equal(result.BodyId, _configuration.ModelKey.BodyId);
                Assert.Equal(result.EquipmentId, _configuration.ModelKey.EquipmentId);
                Assert.Equal(result.EngineId, _configuration.ModelKey.EngineId);
                Assert.Equal(result.GearboxId, _configuration.ModelKey.GearboxId);
            }
        }

        [Fact]
        public void Ctor_GivenIncludedOptions_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("includedOptions", () => new Offer(_fixture.Create<Year>(), _configuration.ModelKey, _configuration.Options, null, _price, _configuration.ColorId));
        }

        [Fact]
        public void Ctor_GivenOptions_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("options", () => new Offer(_fixture.Create<Year>(), _configuration.ModelKey, null, _includedOptions, _price, _configuration.ColorId));
        }

        [Fact]
        public void Ctor_GivenPrice_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("price", () => new Offer(_fixture.Create<Year>(), _configuration.ModelKey, _configuration.Options, _includedOptions, null, _configuration.ColorId));
        }

        [Fact]
        public void Ctor_GivenModelKey_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelKey", () => new Offer(_fixture.Create<Year>(), null, _configuration.Options, _includedOptions, _price, _configuration.ColorId));
        }

        [Fact]
        public void Ctor_GivenColorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("colorId", () => new Offer(_fixture.Create<Year>(), _configuration.ModelKey, _configuration.Options, _includedOptions, _price, null));
        }

        [Fact]
        public void Ctor_GivenYear_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelYear", () => new Offer(null, _configuration.ModelKey, _configuration.Options, _includedOptions, _price, _configuration.ColorId));
        }

        [Fact]
        public void Ctor_WithPackageId()
        {
            using (new SystemTimeContext(_testDate))
            {
                //Act
                var result = new Offer(_modelYear, _configuration.ModelKey, _configuration.PackageId, _configuration.Options, _includedOptions, _price, _configuration.ColorId);

                //Assert
                Assert.Equal(_modelYear, result.ModelYear);
                Assert.Equal(_configuration.PackageId, result.PackageId);
                Assert.Equal(_configuration.Options, result.Options);
                Assert.Equal(_includedOptions, result.IncludedOptions);
                Assert.Equal(_testDate, result.CreatedAt);
                Assert.Equal(_price, result.Price);
                Assert.Equal(_configuration.ColorId, result.ColorId);
                Assert.Equal("38001", result.DealerId);
                Assert.Equal(result.ModelKey, _configuration.ModelKey);
                Assert.Equal(result.ModelId, _configuration.ModelKey.ModelId);
                Assert.Equal(result.BodyId, _configuration.ModelKey.BodyId);
                Assert.Equal(result.EquipmentId, _configuration.ModelKey.EquipmentId);
                Assert.Equal(result.EngineId, _configuration.ModelKey.EngineId);
                Assert.Equal(result.GearboxId, _configuration.ModelKey.GearboxId);
            }
        }

        [Fact]
        public void Ctor_WithPackageId_GivenIncludedOptions_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("includedOptions",
                () => new Offer(_fixture.Create<Year>(), _configuration.ModelKey, _configuration.PackageId, _configuration.Options, null, _price, _configuration.ColorId));
        }

        [Fact]
        public void Ctor_WithPackageId_GivenOptions_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("options",
                () => new Offer(_fixture.Create<Year>(), _configuration.ModelKey, _configuration.PackageId, null, _includedOptions, _price, _configuration.ColorId));
        }

        [Fact]
        public void Ctor_WithPackageId_GivenPackageId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("packageId",
                () => new Offer(_fixture.Create<Year>(), _configuration.ModelKey, null, _configuration.Options, _includedOptions, _price, _configuration.ColorId));
        }

        [Fact]
        public void Ctor_WithPackageId_GivenPrice_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("price",
                () => new Offer(_fixture.Create<Year>(), _configuration.ModelKey, _configuration.PackageId, _configuration.Options, _includedOptions, null, _configuration.ColorId));
        }

        [Fact]
        public void Ctor_WithPackageId_GivenModelKey_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelKey",
                () => new Offer(_fixture.Create<Year>(), null, _configuration.PackageId, _configuration.Options, _includedOptions, _price, _configuration.ColorId));
        }

        [Fact]
        public void Ctor_WithPackageId_GivenColorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("colorId",
                () => new Offer(_fixture.Create<Year>(), _configuration.ModelKey, _configuration.PackageId, _configuration.Options, _includedOptions, _price, null));
        }

        [Fact]
        public void Ctor_WithPackageId_GivenYear_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelYear",
                () => new Offer(null, _configuration.ModelKey, _configuration.PackageId, _configuration.Options, _includedOptions, _price, _configuration.ColorId));
        }

        [Fact]
        public void From()
        {
            using (new SystemTimeContext(_testDate))
            {
                //Arrange
                var expected = new Offer(_modelYear, _configuration.ModelKey, _configuration.PackageId, _configuration.Options, _includedOptions, _price, _configuration.ColorId);

                //Assert
                Assert.Equal(expected, Offer.From(_modelYear, _vehicleOffer, _configuration, _price), PropertyComparer<Offer>.Instance);
            }
        }

        [Fact]
        public void From_GivenYear_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelYear", () => Offer.From(null, _vehicleOffer, _configuration, _price));
        }

        [Fact]
        public void From_GivenConfiguration_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("configuration", () => Offer.From(_modelYear, _vehicleOffer, null, _price));
        }

        [Fact]
        public void From_GivenVehicleOffer_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("vehicleOffer", () => Offer.From(_modelYear, null, _configuration, _price));
        }

        [Fact]
        public void From_GivenPrice_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("price", () => Offer.From(_modelYear, _vehicleOffer, _configuration, null));
        }

        [Fact]
        public void From_WhenConfiguration_HasNoPackage_Selected()
        {
            using (new SystemTimeContext(_testDate))
            {
                //Arrange
                var configuration = _fixture.Construct<Configuration>(3250);
                var expected = new Offer(_modelYear, configuration.ModelKey, configuration.Options, _includedOptions, _price, configuration.ColorId);

                //Assert
                Assert.Equal(expected, Offer.From(_modelYear, _vehicleOffer, configuration, _price), PropertyComparer<Offer>.Instance);
            }
        }

        [Fact]
        public void ChangeStatus_AsNone()
        {
            //Act
            _offer.ChangeStatus(OfferStatus.None);

            //Assert
            Assert.Equal(_offer.Status, OfferStatus.None);
        }

        [Fact]
        public void ChangeStatus_AsAccepted()
        {
            //Act
            _offer.ChangeStatus(OfferStatus.Accepted);

            //Assert
            Assert.Equal(_offer.Status, OfferStatus.Accepted);
        }
    }
}
