﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Common;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Offers
{
    public class OfferServiceTest
    {
        private readonly Fixture _fixture = new Fixture();
        private readonly DateTime _testDate = new DateTime(2019, 1, 1);
        private readonly int _privateCustomerId;

        //Entity
        private readonly Configuration _configuration;
        private readonly Offer _offer;
        private readonly ConfigurationPrice _price;
        private readonly Catalog _catalog;
        private readonly VehicleOffer _vehicleOffer;
        private readonly Package _package;

        //Dependencies
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IConfigurationPricingService> _configurationPricingService;

        //Service
        private readonly OfferService _service;

        public OfferServiceTest()
        {
            _fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _privateCustomerId = _fixture.Create<int>();
            _offer = _fixture.Construct<Offer>(new { includedOptions = PrNumberSet.Parse("AAA") });
            _price = _offer.Price;
            _package = _fixture.Create<Package>();
            _configuration = _fixture.Construct<Configuration>(new { options = _offer.Options });
            _configuration.IncludePackage(_package.Id);
            _catalog = _fixture.Create<Catalog>();
            _vehicleOffer = _fixture.Create<VehicleOffer>();
            _vehicleOffer.AddOptionOffer(PrNumber.Parse("AAA"), Availability.Included(ReasonType.NationalStandard),
                Inclusion.Automatic);

            //Dependencies
            var work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            _configurationPricingService = new Mock<IConfigurationPricingService>();
            _configurationPricingService.Setup(s => s.Calculate(_configuration.Id, _testDate))
                .ReturnsAsync(_price);

            //Service
            _service = new OfferService(work.Object, _configurationPricingService.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(OfferService)).HasNullGuard();
        }

        [Fact]
        public async Task SetPrivateCustomer()
        {
            //Arrange
            _repository.Setup(r => r.Require<Offer>(_offer.Id))
                .ReturnsAsync(_offer)
                .Verifiable();

            //Act
            await _service.SetPrivateCustomer(_offer.Id, _privateCustomerId);

            //Assert
            Assert.Equal(_privateCustomerId, _offer.PrivateCustomerId);
        }

        [Fact]
        public async Task Add()
        {
            using (new SystemTimeContext(_testDate))
            using (Sequence.Create())
            {
                //Arrange
                Expression<Func<Offer, bool>> expected = o =>
                    o.ModelYear == _catalog.ModelYear
                    && o.PackageId == _configuration.PackageId
                    && o.CreatedAt == _testDate
                    && o.Options == _configuration.Options
                    && o.Price == _price
                    && o.ColorId == _configuration.ColorId;

                _configuration.ChangeStatus(ConfigurationStatus.Completed);

                _repository.Setup(r => r.Require<Configuration>(_configuration.Id))
                    .InSequence()
                    .ReturnsAsync(_configuration);

                _repository.Setup(r => r.Require<Catalog>(It.IsAny<int>()))
                    .InSequence()
                    .ReturnsAsync(_catalog);

                _repository.Setup(r => r.Require(It.Is<VehicleOfferForVehicleSpecification>(s =>
                    s.CatalogId == _catalog.Id && s.ModelKey == _configuration.ModelKey)))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer);

                _repository.Setup(r => r.Add(It.Is(expected)))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _service.Add(_configuration.Id);

                //Assert
                Assert.NotNull(result);
            }
        }

        [Fact]
        public async Task Add_WhenConfigurationIsNotCompleted_Throws()
        {
            //Arrange
            _repository.Setup(r => r.Require<Configuration>(_configuration.Id))
                .ReturnsAsync(_configuration);

            //Act
            var e = await Assert.ThrowsAsync<InvalidEntityIdException>(() => _service.Add(_configuration.Id));

            //Assert
            Assert.Equal(typeof(Configuration), e.EntityType);
            Assert.Equal(_configuration.Id, e.Id);
            Assert.Contains("Configuration is not completed", e.Message);
        }

        [Fact]
        public async Task ChangeStatus()
        {
            //Arrange
            _repository.Setup(r => r.Require<Offer>(_offer.Id))
                .ReturnsAsync(_offer);

            //Act
            await _service.ChangeStatus(_offer.Id, OfferStatus.Accepted);

            //Assert
            Assert.Equal(_offer.Status, OfferStatus.Accepted);
        }

        [Fact]
        public async Task Actualize_WhenPackage_IsIncluded()
        {
            //Arrange
            var offer = new Offer(_fixture.Create<int>(), _offer.ModelYear, _offer.ModelKey, _package.Id, _offer.Options, _offer.IncludedOptions, _price, _offer.ColorId);

            using (new SystemTimeContext(_testDate))
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<Offer>(offer.Id))
                    .InSequence()
                    .ReturnsAsync(offer);
                _repository.Setup(r => r.Require(It.Is<RelevantCatalogPublishedForModelYearSpecification>(s =>
                        s.ModelYear == offer.ModelYear)))
                    .InSequence()
                    .ReturnsAsync(_catalog);
                _repository.Setup(r => r.Require(It.Is<VehicleOfferForVehicleSpecification>(s =>
                        s.CatalogId == _catalog.Id && s.ModelKey == offer.ModelKey)))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer);
                _repository.Setup(r => r.Require<Package, PackageId>(offer.PackageId))
                    .InSequence()
                    .ReturnsAsync(_package);
                _configurationPricingService.Setup(s => s.Calculate(_vehicleOffer, offer.Options, offer.ColorId, _package, _testDate))
                    .InSequence()
                    .ReturnsAsync(_price);
                _repository.Setup(r => r.Add(It.Is<Offer>(o => OfferComparer(o, offer))))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _service.Actualize(offer.Id);

                //Assert
                Assert.NotEqual(offer.Id, result);
            }
        }

        private static readonly Func<Offer, Offer, bool> OfferComparer = (a, b) =>
            a.ModelYear == b.ModelYear
            && a.ModelKey == b.ModelKey
            && a.Options == b.Options
            && a.PackageId == b.PackageId
            && a.Price == b.Price
            && a.ColorId == b.ColorId;

        [Fact]
        public async Task Actualize_WhenPackage_IsNotIncluded()
        {
            //Arrange
            var offer = new Offer(_fixture.Create<int>(), _offer.ModelYear, _offer.ModelKey, _offer.Options, _offer.IncludedOptions, _price, _offer.ColorId);

            using (new SystemTimeContext(_testDate))
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<Offer>(offer.Id))
                    .InSequence()
                    .ReturnsAsync(offer);
                _repository.Setup(r => r.Require(It.Is<RelevantCatalogPublishedForModelYearSpecification>(s =>
                        s.ModelYear == offer.ModelYear)))
                    .InSequence()
                    .ReturnsAsync(_catalog);
                _repository.Setup(r => r.Require(It.Is<VehicleOfferForVehicleSpecification>(s =>
                        s.CatalogId == _catalog.Id && s.ModelKey == offer.ModelKey)))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffer);
                _configurationPricingService.Setup(s => s.Calculate(_vehicleOffer, offer.Options, offer.ColorId, null, _testDate))
                    .InSequence()
                    .ReturnsAsync(_price);
                _repository.Setup(r => r.Add(It.Is<Offer>(o => OfferComparer(o, offer))))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _service.Actualize(offer.Id);

                //Assert
                Assert.NotEqual(offer.Id, result);
            }
        }
    }
}
