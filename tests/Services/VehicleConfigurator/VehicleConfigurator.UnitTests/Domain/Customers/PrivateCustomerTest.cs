﻿using AutoFixture;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Domain.Customers;
using System;
using NexCore.Testing;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Customers
{
    public class PrivateCustomerTest
    {
        private readonly int _id;
        private readonly PersonName _personName;
        private readonly string _physicalAddress;
        private readonly Passport _passport;
        private readonly TaxIdentificationNumber _taxIdentificationNumber;
        private readonly string _telephone;
        private readonly PrivateCustomer _privateCustomer;

        public PrivateCustomerTest()
        {
            var fixture = new Fixture();

            _id = fixture.Create<int>();
            _personName = fixture.Create<PersonName>();
            _physicalAddress = fixture.Create<string>();
            _passport = fixture.Create<Passport>();
            _taxIdentificationNumber = fixture.Create<TaxIdentificationNumber>();
            _telephone = fixture.Create<string>();
            _privateCustomer = fixture.Create<PrivateCustomer>();
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var result = new PrivateCustomer(_id, _personName, _physicalAddress, _passport, _taxIdentificationNumber,
                _telephone);

            //Assert
            Assert.Equal(_id, result.Id);
            Assert.Equal(_personName, result.PersonName);
            Assert.Equal(_physicalAddress, result.PhysicalAddress);
            Assert.Equal(_passport, result.Passport);
            Assert.Equal(_taxIdentificationNumber, result.TaxIdentificationNumber);
            Assert.Equal(_telephone, result.Telephone);
        }

        [Fact]
        public void AssignName()
        {
            //Act
            _privateCustomer.AssignName(_personName);

            //Assert
            Assert.Equal(_privateCustomer.PersonName, _personName, PropertyComparer<PersonName>.Instance);
        }

        [Fact]
        public void AssignName_GivenName_IsNull_AssignEmpty()
        {
            //Act
            _privateCustomer.AssignName(null);

            //Assert
            Assert.Equal(_privateCustomer.PersonName, PersonName.Empty);
        }

        [Fact]
        public void AssignPassport()
        {
            //Act
            _privateCustomer.AssignPassport(_passport);

            //Assert
            Assert.Equal(_privateCustomer.Passport, _passport, PropertyComparer<Passport>.Instance);
        }

        [Fact]
        public void AssignPassport_GivenPassport_IsNull_AssignEmpty()
        {
            //Act
            _privateCustomer.AssignPassport(null);

            //Assert
            Assert.Equal(_privateCustomer.Passport, Passport.Empty);
        }

        [Fact]
        public void AssignTaxIdentificationNumber()
        {
            //Act
            _privateCustomer.AssignTaxIdentificationNumber(_taxIdentificationNumber);

            //Assert
            Assert.Equal(_privateCustomer.TaxIdentificationNumber, _taxIdentificationNumber, PropertyComparer<TaxIdentificationNumber>.Instance);
        }

        [Fact]
        public void AssignTaxIdentificationNumber_GivenTaxIdentificationNumber_IsNull_AssignEmpty()
        {
            //Act
            _privateCustomer.AssignTaxIdentificationNumber(null);

            //Assert
            Assert.Equal(_privateCustomer.TaxIdentificationNumber, TaxIdentificationNumber.Empty);
        }

        [Fact]
        public void Ctor_GivenPersonName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("personName", () => new PrivateCustomer(_id, null,
                _physicalAddress, _passport, _taxIdentificationNumber,
                _telephone));
        }

        [Fact]
        public void Ctor_GivenPhysicalAddress_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("physicalAddress", () => new PrivateCustomer(_id, _personName,
                null, _passport, _taxIdentificationNumber,
                _telephone));
        }

        [Fact]
        public void Ctor_GivenPassport_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("passport", () => new PrivateCustomer(_id, _personName,
                _physicalAddress, null, _taxIdentificationNumber,
                _telephone));
        }

        [Fact]
        public void Ctor_GivenTaxIdentificationNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("taxIdentificationNumber", () => new PrivateCustomer(_id, _personName,
                _physicalAddress, _passport, null,
                _telephone));
        }

        [Fact]
        public void Ctor_GivenTelephone_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("telephone", () => new PrivateCustomer(_id, _personName,
                _physicalAddress, _passport, _taxIdentificationNumber,
                null));
        }
    }
}
