﻿using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Contracts;
using NexCore.VehicleConfigurator.Domain.Pricing;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Contracts
{
    public class TotalPriceTest
    {
        private readonly Price _withoutTax;
        private readonly Price _withTax;
        private readonly Price _tax;

        private readonly decimal _priceWithoutTaxBase;
        private readonly decimal _priceWithTaxBase;
        private readonly decimal _taxBase;
        private readonly decimal _priceWithoutTaxRetail;
        private readonly decimal _priceWithTaxRetail;
        private readonly decimal _taxRetail;

        public TotalPriceTest()
        {
            _priceWithTaxBase = 10;
            _taxBase = 2;
            _priceWithoutTaxBase = 8;
            
            _priceWithTaxRetail = 250;
            _taxRetail = 50;
            _priceWithoutTaxRetail = 200;

            _withoutTax = new Price(_priceWithoutTaxBase, _priceWithoutTaxRetail);
            _withTax = new Price(_priceWithTaxBase, _priceWithTaxRetail);
            _tax = new Price(_taxBase, _taxRetail);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var result = new TotalPrice(_priceWithoutTaxBase, _priceWithTaxBase, _taxBase, _priceWithoutTaxRetail, _priceWithTaxRetail, _taxRetail);

            //Assert
            Assert.Equal(_withoutTax, result.WithoutTax, PropertyComparer<Price>.Instance);
            Assert.Equal(_withTax, result.WithTax, PropertyComparer<Price>.Instance);
            Assert.Equal(_tax, result.Tax, PropertyComparer<Price>.Instance);
        }

        [Fact]
        public void Ctor_GivenWithoutTax_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("withoutTax", () => new TotalPrice(null, _withTax, _tax));
        }

        [Fact]
        public void Ctor_GivenWithTax_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("withTax", () => new TotalPrice(_withoutTax, null, _tax));
        }

        [Fact]
        public void Ctor_GivenTax_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("tax", () => new TotalPrice(_withoutTax, _withTax, null));
        }

        [Fact]
        public void Ctor_WithAllPrices()
        {
            //Act
            var result = new TotalPrice(_withoutTax, _withTax, _tax);

            //Assert
            Assert.Equal(_withoutTax, result.WithoutTax, PropertyComparer<Price>.Instance);
            Assert.Equal(_withTax, result.WithTax, PropertyComparer<Price>.Instance);
            Assert.Equal(_tax, result.Tax, PropertyComparer<Price>.Instance);
        }

        [Fact]
        public void Ctor_WithTotalPrice()
        {
            //Act
            var result = new TotalPrice(_withTax);

            //Assert
            Assert.Equal(_withoutTax, result.WithoutTax, PropertyComparer<Price>.Instance);
            Assert.Equal(_withTax, result.WithTax, PropertyComparer<Price>.Instance);
            Assert.Equal(_tax, result.Tax, PropertyComparer<Price>.Instance);
        }
    }
}
