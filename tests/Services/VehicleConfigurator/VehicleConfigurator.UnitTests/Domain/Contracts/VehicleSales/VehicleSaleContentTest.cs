﻿using AutoFixture;
using NexCore.Common;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Domain.Contracts;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Domain.Customers;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.Domain.Pricing;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Dealers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Contracts.VehicleSales
{
    public class VehicleSaleContentTest
    {
        private readonly string _number;
        private readonly PrivateCustomer _privateCustomer;
        private readonly Dealer _dealer;
        private readonly string _dealersCity;
        private readonly string _dealersHeadPosition;
        private readonly TotalPrice _totalPrice;
        private readonly IEnumerable<Price>  _payment;
        private readonly VehicleSpecification _vehicle;
        private readonly string _vehicleModelName;
        private readonly string _vehicleColorName;
        private readonly string _vehicleStockAddress;
        private readonly int _vehicleDeliveryDays;
        private readonly LegalPersonName _salesManagerName;
        private readonly DateTime _testDate = new DateTime(2019, 1, 1);

        public VehicleSaleContentTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());
            fixture.Customize(new DealerIdCustomization());

            _number = fixture.Create<string>();
            _privateCustomer = fixture.Create<PrivateCustomer>();
            _dealer = fixture.Create<Dealer>();
            _dealersCity = fixture.Create<string>();
            _dealersHeadPosition = fixture.Create<string>();
            _totalPrice = fixture.Create<TotalPrice>();
            _payment = fixture.CreateMany<Price>(2);
            _vehicle = fixture.Create<VehicleSpecification>();
            _vehicleModelName = fixture.Create<string>();
            _vehicleColorName = fixture.Create<string>();
            _vehicleStockAddress = fixture.Create<string>();
            _vehicleDeliveryDays = fixture.Create<int>();
            _salesManagerName = fixture.Create<LegalPersonName>();
        }

        [Fact]
        public void Ctor()
        {
            using (new SystemTimeContext(_testDate))
            {
                //Act
                var result = new VehicleSaleContent(_number, _privateCustomer, _dealer, _dealersCity, _dealersHeadPosition, 
                    _totalPrice, _payment, _vehicle, _vehicleModelName, _vehicleColorName, _vehicleStockAddress, _vehicleDeliveryDays,
                    _salesManagerName);

                //Assert
                Assert.Equal(_number, result.Number);
                Assert.Equal(_testDate, result.Date);
                Assert.Equal(_totalPrice, result.Price);
                Assert.Equal(_payment, result.Payment);
                Assert.Equal(_vehicle, result.Vehicle);
                Assert.Equal(_privateCustomer, result.PrivateCustomer);
                Assert.Equal(_dealer, result.Dealer);
                Assert.Equal(_salesManagerName, result.SalesManagerName);
            }
        }
        
        [Fact]
        public void Ctor_GivenNumber_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("number", () => new VehicleSaleContent(
                null, _privateCustomer, _dealer, _dealersCity, _dealersHeadPosition,
                _totalPrice, _payment, _vehicle, _vehicleModelName, _vehicleColorName, _vehicleStockAddress, _vehicleDeliveryDays,
                _salesManagerName));
        }

        [Fact]
        public void Ctor_GivenPrivateCustomer_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("privateCustomer", () => new VehicleSaleContent(
                _number, null, _dealer, _dealersCity, _dealersHeadPosition,
                _totalPrice, _payment, _vehicle, _vehicleModelName, _vehicleColorName, _vehicleStockAddress, _vehicleDeliveryDays,
                _salesManagerName));
        }


        [Fact]
        public void Ctor_GivenDealer_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealer", () => new VehicleSaleContent(
                _number, _privateCustomer, null, _dealersCity, _dealersHeadPosition,
                _totalPrice, _payment, _vehicle, _vehicleModelName, _vehicleColorName, _vehicleStockAddress, _vehicleDeliveryDays,
                _salesManagerName));
        }

        [Fact]
        public void Ctor_GivenDealersCity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealerCity", () => new VehicleSaleContent(
                _number, _privateCustomer, _dealer, null, _dealersHeadPosition,
                _totalPrice, _payment, _vehicle, _vehicleModelName, _vehicleColorName, _vehicleStockAddress, _vehicleDeliveryDays,
                _salesManagerName));
        }

        [Fact]
        public void Ctor_GivenDealersHeadPosition_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealersHeadPosition", () => new VehicleSaleContent(
                _number, _privateCustomer, _dealer, _dealersCity, null,
                _totalPrice, _payment, _vehicle, _vehicleModelName, _vehicleColorName, _vehicleStockAddress, _vehicleDeliveryDays,
                _salesManagerName));
        }

        [Fact]
        public void Ctor_GivenPrice_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("price", () => new VehicleSaleContent(
                _number, _privateCustomer, _dealer, _dealersCity, _dealersHeadPosition,
                null, _payment, _vehicle, _vehicleModelName, _vehicleColorName, _vehicleStockAddress, _vehicleDeliveryDays,
                _salesManagerName));
        }


        [Fact]
        public void Ctor_GivenPayment_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("payment", () => new VehicleSaleContent(
                _number, _privateCustomer, _dealer, _dealersCity, _dealersHeadPosition,
                _totalPrice, null, _vehicle, _vehicleModelName, _vehicleColorName, _vehicleStockAddress, _vehicleDeliveryDays,
                _salesManagerName));
        }

        [Fact]
        public void Ctor_GivenVehicle_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("vehicle", () => new VehicleSaleContent(
                _number, _privateCustomer, _dealer, _dealersCity, _dealersHeadPosition,
                _totalPrice, _payment, null, _vehicleModelName, _vehicleColorName, _vehicleStockAddress, _vehicleDeliveryDays,
                _salesManagerName));
        }

        [Fact]
        public void Ctor_GivenVehicleModelName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("vehicleModelName", () => new VehicleSaleContent(
                _number, _privateCustomer, _dealer, _dealersCity, _dealersHeadPosition,
                _totalPrice, _payment, _vehicle, null, _vehicleColorName, _vehicleStockAddress, _vehicleDeliveryDays,
                _salesManagerName));
        }

        [Fact]
        public void Ctor_GivenVehicleColorName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("vehicleColorName", () => new VehicleSaleContent(
                _number, _privateCustomer, _dealer, _dealersCity, _dealersHeadPosition,
                _totalPrice, _payment, _vehicle, _vehicleModelName, null, _vehicleStockAddress, _vehicleDeliveryDays,
                _salesManagerName));
        }

        [Fact]
        public void Ctor_GivenVehicleStockAddress_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("vehicleStockAddress", () => new VehicleSaleContent(
                _number, _privateCustomer, _dealer, _dealersCity, _dealersHeadPosition,
                _totalPrice, _payment, _vehicle, _vehicleModelName, _vehicleColorName, null, _vehicleDeliveryDays,
                _salesManagerName));
        }

        [Fact]
        public void Ctor_GivenSalesManagerName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("salesManagerName", () => new VehicleSaleContent(
                _number, _privateCustomer, _dealer, _dealersCity, _dealersHeadPosition,
                _totalPrice, _payment, _vehicle, _vehicleModelName, _vehicleColorName, _vehicleStockAddress, _vehicleDeliveryDays,
                null));
        }
    }
}
