﻿using AutoFixture;
using NexCore.Common;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Contracts;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Contracts;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Dealers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Contracts.VehicleSales
{
    public class VehicleSaleContractTest
    {
        private readonly VehicleSaleContent _content;
        private readonly ContractTypeId _typeId;
        private readonly DateTime _testDate = new DateTime(2019, 1, 1);

        public VehicleSaleContractTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());
            fixture.Customize(new DealerIdCustomization()); 
            fixture.Customize(new ContractTypeIdCustomization()); 

            _content = fixture.Create<VehicleSaleContent>();
            _typeId = fixture.Create<ContractTypeId>();
        }

        [Fact]
        public void Ctor()
        {
            using (new SystemTimeContext(_testDate))
            {
                //Act
                var result = new VehicleSaleContract(_typeId, _content, 2);

                //Assert
                Assert.Equal(_content.Number, result.Number);
                Assert.Equal(_typeId, result.TypeId);
                Assert.Equal(_content.Dealer.Id, result.DealerId);
                Assert.Equal(_content.PrivateCustomer.Id, result.PrivateCustomerId);
                Assert.Equal(2, result.SalesManagerId);
                Assert.Equal(_content, result.Content, PropertyComparer<VehicleSaleContent>.Instance);
                Assert.Equal(_content.Date, result.Date);
            }
        }

        [Fact]
        public void Ctor_GivenTypeId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("typeId", () => new VehicleSaleContract(null, _content, 2));
        }

        [Fact]
        public void Ctor_GivenContent_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("content", () => new VehicleSaleContract(_typeId, null, 2));
        }
    }
}
