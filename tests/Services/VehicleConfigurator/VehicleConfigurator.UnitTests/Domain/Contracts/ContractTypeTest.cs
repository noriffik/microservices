﻿using NexCore.VehicleConfigurator.Domain.Contracts;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Contracts
{
    public class ContractTypeTest
    {
        private readonly ContractTypeId _id;
        private readonly string _name;

        public ContractTypeTest()
        {
            _id = ContractTypeId.Parse("code");
            _name = "name";
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var result = new ContractType(_id, _name);

            //Assert
            Assert.Equal(_name, result.Name);
            Assert.Equal(_id, result.Id);
        }

        [Fact]
        public void Ctor_GivenName_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("name", () => new ContractType(_id, null));
        }
    }
}
