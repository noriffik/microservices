﻿using NexCore.VehicleConfigurator.Domain.Contracts;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Contracts
{
    public class ContractTemplateTest
    {
        private readonly ContractTypeId _typeId;
        private readonly string _dealerId;
        private readonly byte[] _content;

        public ContractTemplateTest()
        {
            _typeId = ContractTypeId.Parse("PSCD");
            _dealerId = "dealerId";
            _content = new byte[2];
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var result = new ContractTemplate(_typeId, _dealerId, _content);

            //Assert
            Assert.Equal(_typeId, result.TypeId);
            Assert.Equal(_dealerId, result.DealerId);
            Assert.Equal(_content, result.Content);
        }

        [Fact]
        public void Ctor_GivenTypeId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("typeId", () => new ContractTemplate(null, _dealerId, _content));
        }

        [Fact]
        public void Ctor_GivenDealerId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealerId", () => new ContractTemplate(_typeId, null, _content));
        }

        [Fact]
        public void Ctor_GivenContent_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("content", () => new ContractTemplate(_typeId, _dealerId, null));
        }
    }
}
