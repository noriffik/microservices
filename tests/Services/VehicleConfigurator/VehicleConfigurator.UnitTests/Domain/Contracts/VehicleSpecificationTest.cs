﻿using AutoFixture;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Contracts;
using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Domain.Contracts
{
    public class VehicleSpecificationTest
    {
        private readonly Offer _offer;
        private readonly Year _modelYear;
        private readonly ModelKey _modelKey;
        private readonly ColorId _colorId;

        public VehicleSpecificationTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _modelYear = fixture.Create<Year>();
            _modelKey = fixture.Create<ModelKey>();
            _colorId = fixture.Create<ColorId>();

            var packageId = fixture.Create<PackageId>();
            var includedOptions = PrNumberSet.Parse("AAA");
            var additionalOptions = PrNumberSet.Parse("BBB");
            var configurationPrice = fixture.Create<ConfigurationPrice>();

            _offer = new Offer(_modelYear, _modelKey, packageId, additionalOptions, includedOptions, configurationPrice, _colorId);
        }

        [Fact]
        public void Ctor()
        {
            //Act
            var result = new VehicleSpecification(_modelYear, _modelKey, _colorId);

            //Assert
            Assert.Equal(_modelYear, result.ModelYear);
            Assert.Equal(_modelKey, result.ModelKey);
            Assert.Equal(_colorId, result.ColorId);
            Assert.Null(result.PackageId);
            Assert.Equal(PrNumberSet.Empty, result.AdditionalOptions);
            Assert.Equal(PrNumberSet.Empty, result.IncludedOptions);
        }

        [Fact]
        public void Ctor_GivenModelYear_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelYear", () => new VehicleSpecification(null, _modelKey, _colorId));
        }

        [Fact]
        public void Ctor_GivenModelKey_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("modelKey", () => new VehicleSpecification(_modelYear, null, _colorId));
        }

        [Fact]
        public void Ctor_GivenColorId_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("colorId", () => new VehicleSpecification(_modelYear, _modelKey, null));
        }

        [Fact]
        public void Ctor_WithOffer()
        {
            //Act
            var result = new VehicleSpecification(_offer);

            //Assert
            Assert.Equal(_offer.ModelYear, result.ModelYear);
            Assert.Equal(_offer.ModelKey, result.ModelKey);
            Assert.Equal(_offer.ColorId, result.ColorId);
            Assert.Equal(_offer.PackageId, result.PackageId);
            Assert.Equal(_offer.Options, result.AdditionalOptions);
            Assert.Equal(_offer.IncludedOptions, result.IncludedOptions);
        }

        [Fact]
        public void Ctor_WithOffer_GivenOffer_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("offer", () => new VehicleSpecification(null));
        }
    }
}
