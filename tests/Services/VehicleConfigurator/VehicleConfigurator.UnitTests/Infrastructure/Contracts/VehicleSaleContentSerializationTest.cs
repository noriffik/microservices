﻿using AutoFixture;
using AutoMapper;
using Newtonsoft.Json;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.Infrastructure.Dtos.Contracts;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Dealers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Infrastructure.Contracts
{
    public class VehicleSaleContentSerializationTest
    {
        private readonly Fixture _fixture = new Fixture();
        private readonly IMapper _mapper;
        private readonly VehicleSaleContent _content;

        private readonly VehicleConfiguratorContext _context;
        
        public VehicleSaleContentSerializationTest()
        {
            _context = InMemoryDbContextFactory.Instance.Create();

            _fixture.Customizations.Add(new ModelComponentBuilder());
            _fixture.Customize(new DealerIdCustomization());

            _mapper = _context.Mapper;

            _content = _fixture.Create<VehicleSaleContent>();
        }

        [Fact]
        public void Serialize()
        {
            //Act
            var jsonContentDto = JsonConvert.SerializeObject(_mapper.Map<VehicleSaleContentDto>(_content));
            var deserializedContent = _mapper.Map<VehicleSaleContent>(JsonConvert.DeserializeObject<VehicleSaleContentDto>(jsonContentDto));

            //Assert
            Assert.NotNull(jsonContentDto);
            Assert.Equal(_content, deserializedContent, PropertyComparer<VehicleSaleContent>.Instance);
        }
    }
}
