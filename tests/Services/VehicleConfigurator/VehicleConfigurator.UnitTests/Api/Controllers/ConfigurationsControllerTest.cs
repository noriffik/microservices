﻿using AutoFixture;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations;
using NexCore.VehicleConfigurator.Api.Controllers;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers
{
    public class ConfigurationsControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(ConfigurationsController)).HasNullGuard();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<QueryResponse>(typeof(ConfigurationsController), typeof(ByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetAll()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<QueryResponse>>(typeof(ConfigurationsController), typeof(UnboundQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetDetailed()
        {
            //Arrange
            var action = Assert.Action<DetailedQueryResponse>(typeof(ConfigurationsController), typeof(ByIdDetailedQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Preview()
        {
            //Arrange
            var action = Assert.Action<DetailedQueryResponse>(typeof(ConfigurationsController), typeof(BySpecificationQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public async Task GetByCatalogId()
        {
            //Arrange
            var fixture = new Fixture();

            var query = fixture.Create<ByCatalogIdQuery>();
            var expected = fixture.CreateMany<QueryResponse>();

            var mediator = new Mock<IMediator>();
            mediator.Setup(m => m.Send(query, CancellationToken.None))
                .ReturnsAsync(expected);

            var authorizationService = new Mock<IAuthorizationService>();

            using (var controller = new ConfigurationsController(mediator.Object, authorizationService.Object))
            {
                //Act
                var result = await controller.GetByCatalogId(query);

                //Assert
                Assert.Equal(expected, Assert.IsType<JsonResult>(result).Value);
            }
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(ConfigurationsController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ChangeStatus()
        {
            //Arrange
            var action = Assert.Action(typeof(ConfigurationsController), typeof(ChangeStatusCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task IncludeOption()
        {
            //Arrange
            var action = Assert.Action(typeof(ConfigurationsController), typeof(IncludeOptionCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ExcludeOption()
        {
            //Arrange
            var action = Assert.Action(typeof(ConfigurationsController), typeof(ExcludeOptionCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task IncludePackage()
        {
            //Arrange
            var action = Assert.Action(typeof(ConfigurationsController), typeof(IncludePackageCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ExcludePackage()
        {
            //Arrange
            var action = Assert.Action(typeof(ConfigurationsController), typeof(ExcludePackageCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ChangeColor()
        {
            //Arrange
            var action = Assert.Action(typeof(ConfigurationsController), typeof(ChangeColorCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Copy()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(ConfigurationsController), typeof(CopyCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Validate()
        {
            //Arrange
            var action = Assert.Action<ValidationResponse>(typeof(ConfigurationsController), typeof(ValidateCommand));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Remove()
        {
            //Arrange
            var action = Assert.Action(typeof(ConfigurationsController), typeof(DeleteCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
