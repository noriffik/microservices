﻿using NexCore.VehicleConfigurator.Api.Application.Queries.Vehicles;
using NexCore.VehicleConfigurator.Api.Controllers;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers
{
    public class VehiclesControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(VehiclesController)).HasNullGuard();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<VehicleDescriptionModel>(typeof(VehiclesController), typeof(VehicleDescriptionQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }
    }
}
