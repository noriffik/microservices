﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.VehicleConfigurator.Api.Application.Commands.ColorTypes;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;
using NexCore.VehicleConfigurator.Api.Controllers;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers
{
    public class ColorTypesControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(ColorTypesController)).HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<string>(typeof(ColorTypesController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<ColorTypeModel>(typeof(ColorTypesController), typeof(FindByIdQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().IsNotFoundWhenFailed().Run();
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            var expected = new List<ColorTypeModel>();

            var mediator = new Mock<IMediator>();
            mediator.Setup(m => m.Send(It.IsAny<FindAllQuery>(), CancellationToken.None))
                .ReturnsAsync(expected);

            var authorizationService = new Mock<IAuthorizationService>();

            using (var controller = new ColorTypesController(mediator.Object, authorizationService.Object))
            {
                //Act
                var result = await controller.GetAll();

                //Assert
                Assert.Equal(expected, Assert.IsType<JsonResult>(result).Value);
            }
        }

        [Fact]
        public Task Rename()
        {
            //Arrange
            var action = Assert.Action(typeof(ColorTypesController), typeof(RenameCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
