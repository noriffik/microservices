﻿using NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs;
using NexCore.VehicleConfigurator.Api.Controllers;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers
{
    public class CatalogsControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(CatalogsController)).HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(CatalogsController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<CatalogModel>(typeof(CatalogsController), typeof(FindByIdQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().IsNotFoundWhenFailed().Run();
        }

        [Fact]
        public Task GetAll()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<CatalogModel>>(typeof(CatalogsController), typeof(FindAllQuery));

            //Assert
            return action.IsJson().Run();
        }

        [Fact]
        public Task ChangeStatus()
        {
            //Arrange
            var action = Assert.Action(typeof(CatalogsController), typeof(ChangeStatusCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ChangePeriodTo()
        {
            //Arrange
            var action = Assert.Action(typeof(CatalogsController), typeof(ChangeRelevancePeriodCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Actual()
        {
            //Arrange
            var action = Assert.Action<CatalogModel>(typeof(CatalogsController), typeof(FindActualQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().Run();
        }

        [Fact]
        public Task Outdated()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<CatalogModel>>(typeof(CatalogsController), typeof(FindOutdatedQuery));

            //Assert
            return action.IsJson().Run();
        }
    }
}
