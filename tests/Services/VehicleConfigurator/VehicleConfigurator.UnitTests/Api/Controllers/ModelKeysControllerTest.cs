﻿using NexCore.VehicleConfigurator.Api.Application.Queries.ModelKeys;
using NexCore.VehicleConfigurator.Api.Controllers;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers
{
    public class ModelKeysControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(ModelKeysController)).HasNullGuard();
        }

        [Fact]
        public Task FindMany()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<string>>(typeof(ModelKeysController), typeof(FindByOptionIdQuery));

            //Assert
            return action.IsJson().Run();
        }
    }
}
