﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Gearboxes;
using NexCore.VehicleConfigurator.Api.Controllers.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers.ModelComponents
{
    class GearboxesControllerTest
    {
        [Fact]
        public void Ctor_GivenMediatr_IsNull_Throws()
        {
            Assert.Injection
                .OfConstructor(typeof(GearboxesController))
                .HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<string>(typeof(GearboxesController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Delete()
        {
            //Arrange
            var action = Assert.Action(typeof(GearboxesController), typeof(DeleteCommand<Gearbox, GearboxId>));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Update()
        {
            //Arrange
            var action = Assert.Action(typeof(GearboxesController), typeof(UpdateCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
