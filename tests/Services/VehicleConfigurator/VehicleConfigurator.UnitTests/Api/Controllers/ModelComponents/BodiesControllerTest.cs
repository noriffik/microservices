﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Bodies;
using NexCore.VehicleConfigurator.Api.Controllers.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers.ModelComponents
{
    public class BodiesControllerTest
    {
        [Fact]
        public void Ctor_GivenMediatr_IsNull_Throws()
        {
            Assert.Injection
                .OfConstructor(typeof(BodiesController))
                .HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<string>(typeof(BodiesController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Delete()
        {
            //Arrange
            var action = Assert.Action(typeof(BodiesController), typeof(DeleteCommand<Body, BodyId>));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Update()
        {
            //Arrange
            var action = Assert.Action(typeof(BodiesController), typeof(UpdateCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
