﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Equipments;
using NexCore.VehicleConfigurator.Api.Controllers.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers.ModelComponents
{
    public class EquipmentsControllerTest
    {
        [Fact]
        public void Ctor_GivenMediatr_IsNull_Throws()
        {
            Assert.Injection
                .OfConstructor(typeof(EquipmentsController))
                .HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<string>(typeof(EquipmentsController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Delete()
        {
            //Arrange
            var action = Assert.Action(typeof(EquipmentsController), typeof(DeleteCommand<Equipment, EquipmentId>));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Update()
        {
            //Arrange
            var action = Assert.Action(typeof(EquipmentsController), typeof(UpdateCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
