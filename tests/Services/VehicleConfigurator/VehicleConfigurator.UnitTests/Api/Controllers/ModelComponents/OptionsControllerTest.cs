﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Api.Controllers.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers.ModelComponents
{
    public class OptionsControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(OptionsController)).HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<string>(typeof(OptionsController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetByPrNumberSet()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<OptionModel>>(typeof(OptionsController), typeof(FindByPrNumberSetQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Update()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionsController), typeof(UpdateCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public async Task GetGroupedByCategories()
        {
            //Arrange
            var expected = new List<OptionModel>();

            var mediator = new Mock<IMediator>();
            mediator.Setup(m => m.Send(It.IsAny<FindAllQuery>(), CancellationToken.None))
                .ReturnsAsync(expected);

            var authorizationService = new Mock<IAuthorizationService>();

            using (var controller = new OptionsController(mediator.Object, authorizationService.Object))
            {
                //Act
                var result = await controller.GetAll();

                //Assert
                Assert.Equal(expected, Assert.IsType<JsonResult>(result).Value);
            }
        }

        [Fact]
        public Task Delete()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionsController), typeof(DeleteCommand<Option, OptionId>));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
