﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Moq;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.Components;
using NexCore.VehicleConfigurator.Api.Application.Commands.Components.Models;
using NexCore.VehicleConfigurator.Api.Controllers.Components;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers.Components
{
    public class ModelsControllerTest
    {
        private readonly IMediator _mediator = new Mock<IMediator>().Object;
        private readonly IAuthorizationService _authorizationService = new Mock<IAuthorizationService>().Object;

        [Fact]
        public void Ctor_GivenMediator_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mediator", () => new ModelsController(null, _authorizationService));
        }

        [Fact]
        public void Ctor_GivenAuthorizationService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("authorizationService", () => new ModelsController(_mediator, null));
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<string>(typeof(ModelsController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Delete()
        {
            //Arrange
            var action = Assert.Action(typeof(ModelsController), typeof(DeleteCommand<Model, ModelId>));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Update()
        {
            //Arrange
            var action = Assert.Action(typeof(ModelsController), typeof(UpdateCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
