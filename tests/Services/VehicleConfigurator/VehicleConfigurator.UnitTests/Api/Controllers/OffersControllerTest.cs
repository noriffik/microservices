﻿using NexCore.VehicleConfigurator.Api.Application.Commands.Offers;
using NexCore.VehicleConfigurator.Api.Controllers;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers
{
    public class OffersControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(OffersController)).HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(OffersController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Actualize()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(OffersController), typeof(ActualizeCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task SetPrivateCustomer()
        {
            //Arrange
            var action = Assert.Action(typeof(OffersController), typeof(SetPrivateCustomerCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ChangeStatus()
        {
            //Arrange
            var action = Assert.Action(typeof(OffersController), typeof(ChangeStatusCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}

