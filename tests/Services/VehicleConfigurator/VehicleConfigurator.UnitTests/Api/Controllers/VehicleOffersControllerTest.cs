﻿using AutoFixture;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.Application.Queries;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.AvailableColors;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.AvailableOptions;
using NexCore.VehicleConfigurator.Api.Controllers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers
{
    public class VehicleOffersControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(VehicleOffersController)).HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(VehicleOffersController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<VehicleOfferModel>(typeof(VehicleOffersController), typeof(FindByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            var fixture = new Fixture();
            var expected = fixture.Create<PagedResponse<VehicleOfferModel>>();

            var mediator = new Mock<IMediator>();
            mediator.Setup(m => m.Send(It.IsAny<FindAllQuery>(), CancellationToken.None))
                .ReturnsAsync(expected);

            var authorizationService = new Mock<IAuthorizationService>();

            using (var controller = new VehicleOffersController(mediator.Object, authorizationService.Object))
            {
                //Act
                var result = await controller.GetAll(new FindAllQuery());

                //Assert
                Assert.Equal(expected, Assert.IsType<JsonResult>(result).Value);
            }
        }

        [Fact]
        public Task GetByCatalogId()
        {
            //Arrange
            var action = Assert.Action<PagedResponse<VehicleOfferModel>>(
                typeof(VehicleOffersController), typeof(FindByCatalogIdQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetAvailableOptions()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<AvailableOptionModel>>(
                typeof(VehicleOffersController), typeof(AvailableOptionsQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetAvailableColors()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<AvailableColorModel>>(
                typeof(VehicleOffersController), typeof(AvailableColorsQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task UpdatePrice()
        {
            //Arrange
            var action = Assert.Action(typeof(VehicleOffersController), typeof(UpdatePriceCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ChangeStatus()
        {
            //Arrange
            var action = Assert.Action(typeof(VehicleOffersController), typeof(ChangeStatusCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ChangeRelevancePeriod()
        {
            //Arrange
            var action = Assert.Action(typeof(VehicleOffersController), typeof(ChangeVehicleOfferRelevancePeriodCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ResetRelevancePeriod()
        {
            //Arrange
            var action = Assert.Action(typeof(VehicleOffersController), typeof(ResetVehicleOfferRelevancePeriodCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Actualize()
        {
            //Arrange
            var action = Assert.Action(typeof(VehicleOffersController), typeof(ActualizeRelevanceCommand<VehicleOffer>));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
