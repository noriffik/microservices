﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.VehicleConfigurator.Api.Application.Commands.Colors;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Api.Controllers;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers
{
    public class ColorsControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(ColorsController)).HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<string>(typeof(ColorsController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<ColorModel>(typeof(ColorsController), typeof(FindByIdQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().IsNotFoundWhenFailed().Run();
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            var expected = new List<ColorModel>();

            var mediator = new Mock<IMediator>();
            mediator.Setup(m => m.Send(It.IsAny<FindAllQuery>(), CancellationToken.None))
                .ReturnsAsync(expected);

            var authorizationService = new Mock<IAuthorizationService>();

            using (var controller = new ColorsController(mediator.Object, authorizationService.Object))
            {
                //Act
                var result = await controller.GetAll();

                //Assert
                Assert.Equal(expected, Assert.IsType<JsonResult>(result).Value);
            }
        }

        [Fact]
        public Task Update()
        {
            //Arrange
            var action = Assert.Action(typeof(ColorsController), typeof(UpdateCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
