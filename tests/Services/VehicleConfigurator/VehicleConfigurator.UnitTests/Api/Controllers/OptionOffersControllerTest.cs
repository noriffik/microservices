﻿using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers.ByComponents;
using NexCore.VehicleConfigurator.Api.Controllers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using AddCommand = NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers.AddCommand;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers
{
    public class OptionOffersControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(OptionOffersController)).HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionOffersController), typeof(AddCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetManyByVehicleOffer()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<OptionOfferModel>>(typeof(OptionOffersController), typeof(FindByVehicleOfferIdQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetByVehicleOfferAndPrNumber()
        {
            //Arrange
            var action = Assert.Action<OptionOfferModel>(typeof(OptionOffersController), typeof(FindByVehicleOfferIdAndPrNumberQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().IsNotFoundWhenFailed().Run();
        }

        [Fact]
        public Task GetManyByModelId()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<OptionOfferByComponentModel>>(typeof(OptionOffersController), typeof(FindByModelIdQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task UpdateAvailability()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionOffersController), typeof(UpdateAvailabilityCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task UpdateInclusion()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionOffersController), typeof(UpdateInclusionCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task AddRuleSet()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionOffersController), typeof(AddRuleSetCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task AddRuleSetForManyVehicles()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionOffersController), typeof(AddRuleSetForManyCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task RemoveRule()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionOffersController), typeof(RemoveRuleCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Remove()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionOffersController), typeof(DeleteCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public void UpdateAvailabilities()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionOffersController), typeof(UpdateAvailabilitiesCommand));

            //Assert
            action.IsOk().IsBadRequestOnNull();
        }

        [Fact]
        public Task ChangeRelevancePeriod()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionOffersController), typeof(ChangeOptionOfferRelevancePeriodCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ResetRelevancePeriod()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionOffersController), typeof(ResetOptionOfferRelevancePeriodCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ActualizeRelevanceInCatalog()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionOffersController), typeof(ActualizeRelevanceCommand<OptionOffer>));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
