﻿using NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions;
using NexCore.VehicleConfigurator.Api.Application.Queries.Restrictions;
using NexCore.VehicleConfigurator.Api.Controllers;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers
{
    public class RestrictionsControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(RestrictionsController)).HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(RestrictionsController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<RestrictionModel>(typeof(RestrictionsController), typeof(FindByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetByCatalogId()
        {
            //Arrange
            var action =
                Assert.Action<IEnumerable<RestrictionModel>>(typeof(RestrictionsController),
                    typeof(FindByCatalogIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ChangePeriod()
        {
            //Arrange
            var action = Assert.Action(typeof(RestrictionsController), typeof(ChangeRelevancePeriodCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Enable()
        {
            //Arrange
            var action = Assert.Action(typeof(RestrictionsController), typeof(EnableCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Disable()
        {
            //Arrange
            var action = Assert.Action(typeof(RestrictionsController), typeof(DisableCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Actualize()
        {
            //Arrange
            var action = Assert.Action(typeof(RestrictionsController), typeof(ActualizeRelevanceCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
