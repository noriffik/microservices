﻿using NexCore.VehicleConfigurator.Api.Application.Commands.Packages;
using NexCore.VehicleConfigurator.Api.Application.Queries.Packages;
using NexCore.VehicleConfigurator.Api.Controllers;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers
{
    public class PackagesControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(PackagesController)).HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<string>(typeof(PackagesController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<PackageModel>(typeof(PackagesController), typeof(FindByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetByCatalogId()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<PackageModel>>(typeof(PackagesController), typeof(ByCatalogIdQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task UpdateCoverage()
        {
            //Arrange
            var action = Assert.Action(typeof(PackagesController), typeof(UpdateCoverageCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ChangeStatus()
        {
            //Arrange
            var action = Assert.Action(typeof(PackagesController), typeof(ChangeStatusCommand));

            //Act
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task UpdatePrice()
        {
            //Arrange
            var action = Assert.Action(typeof(PackagesController), typeof(UpdatePriceCommand));

            //Act
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task UpdateApplicability()
        {
            //Arrange
            var action = Assert.Action(typeof(PackagesController), typeof(UpdateApplicabilityCommand));

            //Act
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
