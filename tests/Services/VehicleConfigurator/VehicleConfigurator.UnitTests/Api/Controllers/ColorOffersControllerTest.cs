﻿using NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers;
using NexCore.VehicleConfigurator.Api.Controllers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using AddCommand = NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers.AddCommand;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers
{
    public class ColorOffersControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(ColorOffersController)).HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action(typeof(ColorOffersController), typeof(AddCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task SetDefaultColor()
        {
            //Arrange
            var action = Assert.Action(typeof(ColorOffersController), typeof(SetDefaultColorCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ChangeRelevancePeriod()
        {
            //Arrange
            var action = Assert.Action(typeof(ColorOffersController), typeof(ChangeColorOfferRelevancePeriodCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ResetRelevancePeriod()
        {
            //Arrange
            var action = Assert.Action(typeof(ColorOffersController), typeof(ResetColorOfferRelevancePeriodCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetByVehicleOfferIdAndColorId()
        {
            //Arrange
            var action = Assert.Action<ColorOfferModel>(typeof(ColorOffersController), typeof(FindByVehicleOfferIdAndColorIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task GetByVehicleOfferId()
        {
            //Arrange
            var action = Assert.Action<IEnumerable<ColorOfferModel>>(typeof(ColorOffersController), typeof(FindByVehicleOfferIdQuery));

            //Assert
            return action.IsJson().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ActualizeRelevanceInCatalog()
        {
            //Arrange
            var action = Assert.Action(typeof(ColorOffersController), typeof(ActualizeRelevanceCommand<ColorOffer>));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task ChangePrice()
        {
            //Arrange
            var action = Assert.Action(typeof(ColorOffersController), typeof(ChangePriceCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
