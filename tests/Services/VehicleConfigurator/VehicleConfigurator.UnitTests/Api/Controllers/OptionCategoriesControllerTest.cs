﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories;
using NexCore.VehicleConfigurator.Api.Controllers;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers
{
    public class OptionCategoriesControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(OptionCategoriesController)).HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(OptionCategoriesController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<OptionCategoryModel>(typeof(OptionCategoriesController), typeof(FindByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Assign()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionCategoriesController), typeof(AssignCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Unassign()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionCategoriesController), typeof(UnassignCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            var expected = new List<OptionCategoryModel>();

            var mediator = new Mock<IMediator>();
            mediator.Setup(m => m.Send(It.IsAny<FindAllQuery>(), CancellationToken.None))
                .ReturnsAsync(expected);

            var authorizationService = new Mock<IAuthorizationService>();

            using (var controller = new OptionCategoriesController(mediator.Object, authorizationService.Object))
            {
                //Act
                var result = await controller.GetAll();

                //Assert
                Assert.Equal(expected, Assert.IsType<JsonResult>(result).Value);
            }
        }

        [Fact]
        public Task Update()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionCategoriesController), typeof(UpdateCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Delete()
        {
            //Arrange
            var action = Assert.Action(typeof(OptionCategoriesController), typeof(DeleteCommand));

            //Assert
            return action.IsOk().IsBadRequestOnNull().Run();
        }
    }
}
