﻿using AutoFixture;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NexCore.Application.Queries;
using NexCore.VehicleConfigurator.Api.Application.Commands.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Api.Application.Queries.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Api.Controllers;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers
{
    public class VehicleSalesContractsControllerTest
    {
        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(CatalogsController)).HasNullGuard();
        }

        [Fact]
        public Task Add()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(VehicleSalesContractsController), typeof(AddCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task AddWithOffer()
        {
            //Arrange
            var action = Assert.Action<int>(typeof(VehicleSalesContractsController), typeof(AddWithOfferCommand));

            //Assert
            return action.IsCreated().IsBadRequestOnNull().Run();
        }

        [Fact]
        public Task Get()
        {
            //Arrange
            var action = Assert.Action<VehicleSaleContractModel>(typeof(VehicleSalesContractsController), typeof(ByIdQuery));

            //Assert
            return action.IsJson().IsNotFoundWhenFailed().IsBadRequestOnNull().Run();
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            var fixture = new Fixture();

            var query = fixture.Create<FindAllQuery>();
            var expected = fixture.Create<PagedResponse<VehicleSaleContractModel>>();

            var mediator = new Mock<IMediator>();
            mediator.Setup(m => m.Send(query, CancellationToken.None))
                .ReturnsAsync(expected);

            var authorizationService = new Mock<IAuthorizationService>();

            using (var controller = new VehicleSalesContractsController(mediator.Object, authorizationService.Object))
            {
                //Act
                var result = await controller.GetAll(query);

                //Assert
                Assert.Equal(expected, Assert.IsType<JsonResult>(result).Value);
            }
        }
    }
}
