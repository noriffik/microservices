﻿using NexCore.VehicleConfigurator.Api.Controllers;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Controllers
{
    public class SeedsControllerTest
    {
        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(SeedsController)).HasNullGuard();
        }

        [Fact]
        public Task Run()
        {
            //Arrange
            var action = Assert.Action(typeof(SeedsController), nameof(SeedsController.Run));

            //Assert
            return action.IsOk().Run();
        }
    }
}
