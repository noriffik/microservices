﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Components.Models;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.ModelComponents.Options
{
    public class FindByPrNumberSetValidatorTest
    {
        private readonly FindByPrNumberSetQueryValidator _validator = new FindByPrNumberSetQueryValidator();

        [Fact]
        public void ShouldHaveChildValidator_ModelIdValidator_ForModelId()
        {
            _validator.ShouldHaveChildValidator(c => c.ModelId, typeof(ModelIdValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_PrNumberValidator_ForPrNumberSet()
        {
            _validator.ShouldHaveChildValidator(c => c.PrNumberSet, typeof(PrNumberSetValidator));
        }
    }
}
