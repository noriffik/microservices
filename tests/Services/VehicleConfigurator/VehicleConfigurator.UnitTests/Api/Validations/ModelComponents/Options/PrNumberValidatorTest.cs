﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.ModelComponents.Options
{
    public class PrNumberValidatorTest
    {
        private readonly PrNumberValidator _validator = new PrNumberValidator();

        [Theory]
        [InlineData("QQQQ")]
        [InlineData("WW")]
        [InlineData(" ")]
        [InlineData("")]
        [InlineData("#$@")]
        public void ShouldHaveError_WhenIsInvalid_IsEmpty(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }

        [Theory]
        [InlineData("QQQ")]
        [InlineData("WWW")]
        [InlineData("123")]
        [InlineData("F4F")]
        public void ShouldNotHaveError_WhenId_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }
    }
}
