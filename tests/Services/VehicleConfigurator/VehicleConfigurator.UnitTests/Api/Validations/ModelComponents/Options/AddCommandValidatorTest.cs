﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.ModelComponents.Options
{
    public class AddCommandValidatorTest
    {
        private readonly AddCommandValidator _validator = new AddCommandValidator();

        [Fact]
        public void ShouldHaveChildValidator_PrNumberValidator_ForId()
        {
            _validator.ShouldHaveChildValidator(c => c.Id, typeof(OptionIdValidator));
        }
    }
}
