﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.ModelComponents.Options
{
    public class FindByIdQueryValidatorTest
    {
        private readonly FindByIdQueryValidator _validator = new FindByIdQueryValidator();

        [Fact]
        public void ShouldHaveChildValidator_PrNumberValidator_ForId()
        {
            _validator.ShouldHaveChildValidator(c => c.Id, typeof(OptionIdValidator));
        }
    }
}
