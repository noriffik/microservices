﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.ModelComponents.Options
{
    public class UpdateCommandValidatorTest
    {
        private readonly UpdateCommandValidator _validator = new UpdateCommandValidator();

        [Fact]
        public void ShouldHaveChildValidator_PrNumberValidator_ForId()
        {
            _validator.ShouldHaveChildValidator(c => c.Id, typeof(OptionIdValidator));
        }
    }
}
