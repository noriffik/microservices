﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ColorTypes;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.ColorTypes
{
    public class FindByIdQueryValidatorTest
    {
        private readonly FindByIdQueryValidator _validator = new FindByIdQueryValidator();

        [Fact]
        public void ShouldHaveChildValidator_ColorTypeIdValidator_ForId()
        {
            _validator.ShouldHaveChildValidator(c => c.Id, typeof(ColorTypeIdValidator));
        }
    }
}
