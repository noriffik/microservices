﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ColorTypes;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.ColorTypes
{
    public class AddCommandValidatorTest
    {
        private readonly AddCommandValidator _validator = new AddCommandValidator();

        [Fact]
        public void ShouldHaveChildValidator_ColorTypeIdValidator_ForId()
        {
            _validator.ShouldHaveChildValidator(c => c.Id, typeof(ColorTypeIdValidator));
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void ShouldHaveError_WhenName_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Name, value);
        }

        [Theory]
        [InlineData("42")]
        [InlineData("W")]
        [InlineData("Name")]
        public void ShouldNotHaveError_WhenName_IsValid(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Name, value);
        }
    }
}
