﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ColorTypes;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.ColorTypes
{
    public class ColorTypeIdValidatorTest
    {
        private readonly ColorTypeIdValidator _validator = new ColorTypeIdValidator();

        [Theory]
        [InlineData("QQQQ")]
        [InlineData("WW")]
        [InlineData(" ")]
        [InlineData("")]
        [InlineData("#$@")]
        public void ShouldHaveError_WhenValue_IsInvalid(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }

        [Theory]
        [InlineData("WWW")]
        [InlineData("123")]
        [InlineData("F4F")]
        public void ShouldNotHaveError_WhenValue_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }
    }
}
