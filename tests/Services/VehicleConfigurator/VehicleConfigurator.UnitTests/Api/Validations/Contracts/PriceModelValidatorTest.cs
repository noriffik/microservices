﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Contracts;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Contracts
{
    public class PriceModelValidatorTest
    {
        private readonly PriceModelValidator _validator = new PriceModelValidator();

        [Fact]
        public void ShouldNotHaveError()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Base, 1);
            _validator.ShouldNotHaveValidationErrorFor(c => c.Retail, 2);
        }
    }
}
