﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;
using NexCore.VehicleConfigurator.Api.Application.Validations.Contracts;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.Packages;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Contracts
{
    public class VehicleSpecificationModelValidatorTest
    {
        private readonly VehicleSpecificationModelValidator _validator = new VehicleSpecificationModelValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenModelYear_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ModelYear, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenModelYear_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.ModelYear, value);
        }

        [Fact]
        public void ShouldHaveChildValidator_ModelKeyValidator_ForModelKey()
        {
            _validator.ShouldHaveChildValidator(c => c.ModelKey, typeof(ModelKeyValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_ColorIdValidator_ForColorId()
        {
            _validator.ShouldHaveChildValidator(c => c.ColorId, typeof(ColorIdValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_PrNumberSetValidator_ForAdditionalOptions()
        {
            _validator.ShouldHaveChildValidator(c => c.AdditionalOptions, typeof(PrNumberSetValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_PrNumberSetValidator_ForIncludedOptions()
        {
            _validator.ShouldHaveChildValidator(c => c.IncludedOptions, typeof(PrNumberSetValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_PackageIdValidator_ForPackageId()
        {
            _validator.ShouldHaveChildValidator(c => c.PackageId, typeof(PackageIdValidator));
        }
    }
}
