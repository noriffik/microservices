﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Contracts;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Contracts
{
    public class ContractTypeIdValidatorTest
    {
        private readonly ContractTypeIdValidator _validator = new ContractTypeIdValidator();

        [Theory]
        [InlineData("QQQQQ")]
        [InlineData("WWW")]
        [InlineData(" ")]
        [InlineData("")]
        [InlineData("#$@w")]
        public void ShouldHaveError_WhenValue_IsInvalid(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }

        [Theory]
        [InlineData("WWWW")]
        [InlineData("1234")]
        [InlineData("F4Fj")]
        public void ShouldNotHaveError_WhenValue_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }
    }
}
