﻿using AutoFixture;
using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts;
using NexCore.VehicleConfigurator.Api.Application.Validations.Contracts;
using NexCore.VehicleConfigurator.Api.Application.Validations.Contracts.VehicleSales;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Contracts.VehicleSales
{
    public class AddCommandValidatorTest
    {
        private readonly AddCommandValidator _validator = new AddCommandValidator();
        private readonly IFixture _fixture = new Fixture();

        [Fact]
        public void ShouldHaveError_WhenNumber_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Number, string.Empty);
        }

        [Fact]
        public void ShouldNotHaveError_WhenNumber_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Number, "number");
        }

        [Fact]
        public void ShouldHaveChildValidator_ContractTypeIdValidator_ForContractTypeId()
        {
            _validator.ShouldHaveChildValidator(c => c.ContractTypeId, typeof(ContractTypeIdValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_TotalPriceModelValidator_ForTotalPrice()
        {
            _validator.ShouldHaveChildValidator(c => c.TotalPrice, typeof(TotalPriceModelValidator));
        }

        [Fact]
        public void ShouldHaveError_WhenItemCount_IsLessThanTwo()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Payment, _fixture.CreateMany<PriceModel>(1));
        }

        [Theory]
        [InlineData(2)]
        [InlineData(12)]
        public void ShouldNotHaveError_WhenItemCount_IsGreaterThanOne(int count)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Payment, _fixture.CreateMany<PriceModel>(count));
        }

        [Fact]
        public void ShouldHaveChildValidator_PriceModelValidator_ForPayment()
        {
            _validator.ShouldHaveChildValidator(c => c.Payment, typeof(PriceModelValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_VehicleSpecificationModelValidator_ForVehicle()
        {
            _validator.ShouldHaveChildValidator(c => c.Vehicle, typeof(VehicleSpecificationModelValidator));
        }

        [Fact]
        public void ShouldHaveError_WhenVehicleStockAddress_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.VehicleStockAddress, string.Empty);
        }

        [Fact]
        public void ShouldNotHaveError_WhenVehicleStockAddress_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.VehicleStockAddress, "vehicleStockAddress");
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenVehicleDeliveryDays_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.VehicleDeliveryDays, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenVehicleDeliveryDays_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.VehicleDeliveryDays, value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenPrivateCustomerId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.PrivateCustomerId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenPrivateCustomerId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.PrivateCustomerId, value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenSalesManagerId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.SalesManagerId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenSalesManagerId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.SalesManagerId, value);
        }

        [Fact]
        public void ShouldHaveError_WhenDealerId_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.DealerId, string.Empty);
        }

        [Fact]
        public void ShouldHaveChildValidator_DealerIdValidator_ForDealerId()
        {
            _validator.ShouldHaveChildValidator(c => c.DealerId, typeof(DealerIdValidator));
        }

        [Fact]
        public void ShouldHaveError_WhenDealersCity_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.DealersCity, string.Empty);
        }

        [Fact]
        public void ShouldNotHaveError_WhenDealersCity_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.DealersCity, "dealersCity");
        }

        [Fact]
        public void ShouldHaveError_WhenDealersHeadPosition_IsEmpty()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.DealersHeadPosition, string.Empty);
        }

        [Fact]
        public void ShouldNotHaveError_WhenDealersHeadPosition_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.DealersHeadPosition, "dealersHeadPosition");
        }
    }
}
