﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Contracts;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Contracts
{
    public class DealerIdValidatorTest
    {
        private readonly DealerIdValidator _validator = new DealerIdValidator();

        [Theory]
        [InlineData("QQQQQ")]
        [InlineData("WWW")]
        [InlineData(" ")]
        [InlineData("")]
        [InlineData("#$@w")]
        public void ShouldHaveError_WhenValue_IsInvalid(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }

        [Theory]
        [InlineData("AAA12345")]
        [InlineData("12345678")]
        public void ShouldNotHaveError_WhenValue_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }
    }
}
