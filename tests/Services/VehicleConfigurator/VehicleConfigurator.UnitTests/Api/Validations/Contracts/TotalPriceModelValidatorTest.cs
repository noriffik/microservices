﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Contracts;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Contracts
{
    public class TotalPriceModelValidatorTest
    {
        private readonly TotalPriceModelValidator _validator = new TotalPriceModelValidator();

        [Fact]
        public void ShouldHaveChildValidator_PriceModelValidator_ForWithoutTax()
        {
            _validator.ShouldHaveChildValidator(c => c.WithoutTax, typeof(PriceModelValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_PriceModelValidator_ForWithTax()
        {
            _validator.ShouldHaveChildValidator(c => c.WithTax, typeof(PriceModelValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_PriceModelValidator_ForTax()
        {
            _validator.ShouldHaveChildValidator(c => c.Tax, typeof(PriceModelValidator));
        }
    }
}
