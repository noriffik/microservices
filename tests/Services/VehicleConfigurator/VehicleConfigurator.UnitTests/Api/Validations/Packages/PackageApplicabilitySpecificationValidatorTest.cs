﻿using FluentValidation.TestHelper;
using Moq;
using NexCore.VehicleConfigurator.Api.Application.Validations.Packages;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Packages
{
    public class PackageApplicabilitySpecificationValidatorTest
    {
        private const string S = "test-value";

        private readonly Mock<IApplicabilityParser> _parser;
        private readonly PackageApplicabilitySpecificationValidator _validator;
        private Applicability _applicability;
        
        public PackageApplicabilitySpecificationValidatorTest()
        {
            _parser = new Mock<IApplicabilityParser>();
            _validator = new PackageApplicabilitySpecificationValidator(_parser.Object);
        }

        [Fact]
        public void ShouldHaveError_WhenSerializer_CanNotDeserialize()
        {
            //Arrange
            _parser.Setup(p => p.TryParse(S, out _applicability))
                .Returns(false);

            //Assert
            _validator.TestValidate(S).ShouldHaveError();
        }

        [Fact]
        public void ShouldNotHaveError_CanDeserialize()
        {
            //Arrange
            _parser.Setup(p => p.TryParse(S, out _applicability)).Returns(true);

            //Assert
            _validator.TestValidate(S).ShouldNotHaveError();
        }
    }
}
