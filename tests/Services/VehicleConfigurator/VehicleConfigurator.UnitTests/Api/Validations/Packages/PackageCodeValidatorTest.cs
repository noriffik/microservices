﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Packages;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Packages
{
    public class PackageCodeValidatorTest
    {
        private readonly PackageCodeValidator _validator = new PackageCodeValidator();

        [Theory]
        [InlineData("q1w2e3")]
        [InlineData("WW")]
        [InlineData(" ")]
        [InlineData("")]
        [InlineData("#$@")]
        public void ShouldHaveError_WhenIsInvalid_IsEmpty(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }

        [Theory]
        [InlineData("WWW")]
        [InlineData("12w")]
        [InlineData("f40")]
        public void ShouldNotHaveError_WhenId_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }
    }
}
