﻿using FluentValidation.TestHelper;
using Moq;
using NexCore.VehicleConfigurator.Api.Application.Validations.Packages;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Packages
{
    public class UpdateApplicabilityCommandValidatorTest
    {
        private readonly UpdateApplicabilityCommandValidator _validator =
            new UpdateApplicabilityCommandValidator(new Mock<IApplicabilityParser>().Object);

        [Fact]
        public void ShouldHaveChildValidator_PackageIdValidator_ForId()
        {
            _validator.ShouldHaveChildValidator(c => c.Id, typeof(PackageIdValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_PrNumberSetValidator_ForPrNumbers()
        {
            _validator.ShouldHaveChildValidator(c => c.Applicability, typeof(PackageApplicabilitySpecificationValidator));
        }
    }
}
