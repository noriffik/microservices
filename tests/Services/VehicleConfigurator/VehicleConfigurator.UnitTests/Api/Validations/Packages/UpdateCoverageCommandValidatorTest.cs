﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Packages;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Packages
{
    public class UpdateCoverageCommandValidatorTest
    {
        private readonly UpdateCoverageCommandValidator _validator = new UpdateCoverageCommandValidator();

        [Fact]
        public void ShouldHaveChildValidator_ModelKeySetValidator_ForModelKeys()
        {
            _validator.ShouldHaveChildValidator(c => c.ModelKeys, typeof(BoundModelKeySetValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_PackageIdValidator_ForId()
        {
            _validator.ShouldHaveChildValidator(c => c.Id, typeof(PackageIdValidator));
        }
    }
}
