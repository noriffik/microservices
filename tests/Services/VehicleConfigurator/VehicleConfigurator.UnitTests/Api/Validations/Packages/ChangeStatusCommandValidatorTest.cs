﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Packages;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Packages
{
    public class ChangeStatusCommandValidatorTest
    {
        private readonly ChangeStatusCommandValidator _validator = new ChangeStatusCommandValidator();

        [Fact]
        public void ShouldHaveChildValidator_PackageIdValidator_ForId()
        {
            _validator.ShouldHaveChildValidator(c => c.Id, typeof(PackageIdValidator));
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(2)]
        public void ShouldHaveError_WhenStatus_IsInValid(PackageStatus value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Status, value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public void ShouldNotHaveError_WhenStatus_IsValid(PackageStatus value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Status, value);
        }
    }
}
