﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Packages;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Packages
{
    public class PackageIdValidatorTest
    {
        private readonly PackageIdValidator _validator = new PackageIdValidator();

        [Theory]
        [InlineData("q1w2e3")]
        [InlineData("WWw")]
        [InlineData(" ")]
        [InlineData("")]
        [InlineData("#$@")]
        public void ShouldHaveError_WhenIsInvalid_IsEmpty(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }

        [Theory]
        [InlineData("QQQQQ")]
        [InlineData("WWWWW")]
        [InlineData("123ew")]
        [InlineData("f4d40")]
        public void ShouldNotHaveError_WhenId_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }
    }
}
