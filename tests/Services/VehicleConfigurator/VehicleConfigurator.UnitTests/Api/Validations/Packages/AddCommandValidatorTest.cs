﻿using FluentValidation.TestHelper;
using Moq;
using NexCore.VehicleConfigurator.Api.Application.Validations.Packages;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using Xunit;
using AddCommandValidator = NexCore.VehicleConfigurator.Api.Application.Validations.Packages.AddCommandValidator;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Packages
{
    public class AddCommandValidatorTest
    {
        private readonly AddCommandValidator _validator = 
            new AddCommandValidator(new Mock<IApplicabilityParser>().Object);

        [Fact]
        public void ShouldHaveChildValidator_PackageIdValidator_ForId()
        {
            _validator.ShouldHaveChildValidator(c => c.Id, typeof(PackageIdValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_ModelKeySetValidator_ForModelKeys()
        {
            _validator.ShouldHaveChildValidator(c => c.ModelKeys, typeof(BoundModelKeySetValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_PrNumberSetValidator_ForPrNumbers()
        {
            _validator.ShouldHaveChildValidator(c => c.Applicability, typeof(PackageApplicabilitySpecificationValidator));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenCatalogId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.CatalogId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenCatalogId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.CatalogId, value);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-122.32)]
        [InlineData(-0.12)]
        public void ShouldHaveError_WhenPrice_IsInValid(decimal value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Price, value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(42.3)]
        [InlineData(14523.123)]
        public void ShouldNotHaveError_WhenPrice_IsValid(decimal value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Price, value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void ShouldHaveError_WhenName_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Name, value);
        }

        [Theory]
        [InlineData("42")]
        [InlineData("W")]
        [InlineData("Name")]
        public void ShouldNotHaveError_WhenName_IsValid(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Name, value);
        }
    }
}
