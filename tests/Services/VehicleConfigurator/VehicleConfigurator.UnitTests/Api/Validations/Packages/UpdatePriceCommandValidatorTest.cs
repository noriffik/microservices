﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Packages;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Packages
{
    public class UpdatePriceCommandValidatorTest
    {
        private readonly UpdatePriceCommandValidator _validator = new UpdatePriceCommandValidator();

        [Fact]
        public void ShouldHaveChildValidator_PackageIdValidator_ForId()
        {
            _validator.ShouldHaveChildValidator(c => c.Id, typeof(PackageIdValidator));
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-122.32)]
        public void ShouldHaveError_WhenPrice_IsInValid(decimal value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Price, value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(14523.123)]
        public void ShouldNotHaveError_WhenPrice_IsValid(decimal value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Price, value);
        }
    }
}
