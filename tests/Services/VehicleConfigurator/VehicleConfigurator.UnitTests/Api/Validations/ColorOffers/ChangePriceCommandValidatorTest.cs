﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ColorOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.ColorOffers
{
    public class ChangePriceCommandValidatorTest
    {
        private readonly ChangePriceCommandValidator _validator = new ChangePriceCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldHaveError_WhenVehicleOfferId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.VehicleOfferId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        public void ShouldNotHaveError_WhenVehicleOfferId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.VehicleOfferId, value);
        }

        [Fact]
        public void ShouldHaveChildValidator_ColorIdValidator_ForColorId()
        {
            _validator.ShouldHaveChildValidator(c => c.ColorId, typeof(ColorIdValidator));
        }

        [Fact]
        public void ShouldHaveError_WhenColorId_IsNull()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ColorId, null as string);
        }

        [Fact]
        public void ShouldHaveError_WhenPrice_IsInValid()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Price, -1M);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(42.42)]
        public void ShouldNotHaveError_WhenPrice_IsValid(decimal value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Price, value);
        }
    }
}
