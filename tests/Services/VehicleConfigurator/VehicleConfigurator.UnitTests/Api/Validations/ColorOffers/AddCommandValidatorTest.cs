﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;
using Xunit;
using AddCommandValidator = NexCore.VehicleConfigurator.Api.Application.Validations.ColorOffers.AddCommandValidator;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.ColorOffers
{
    public class AddCommandValidatorTest
    {
        private readonly AddCommandValidator _validator = new AddCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenVehicleOfferId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.VehicleOfferId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenVehicleOfferId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.VehicleOfferId, value);
        }

        [Theory]
        [InlineData(-0.123)]
        [InlineData(-1)]
        [InlineData(-12342.123)]
        public void ShouldHaveError_WhenPrice_IsInValid(decimal value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Price, value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1.234)]
        [InlineData(42)]
        [InlineData(14523.234)]
        public void ShouldNotHaveError_WhenPrice_IsValid(decimal value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Price, value);
        }

        [Fact]
        public void ShouldHaveChildValidator_ColorIdValidator_ForColorId()
        {
            _validator.ShouldHaveChildValidator(c => c.ColorId, typeof(ColorIdValidator));
        }
    }
}
