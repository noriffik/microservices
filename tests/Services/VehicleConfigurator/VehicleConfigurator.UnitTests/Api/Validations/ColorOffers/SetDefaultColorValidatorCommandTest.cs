﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ColorOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.ColorOffers
{
    public class SetDefaultColorCommandValidatorTest
    {
        private readonly SetDefaultColorCommandValidator _validator = new SetDefaultColorCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenVehicleOfferId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.VehicleOfferId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenVehicleOfferId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.VehicleOfferId, value);
        }

        [Fact]
        public void ShouldHaveChildValidator_ColorIdValidator_ForColorId()
        {
            _validator.ShouldHaveChildValidator(c => c.ColorId, typeof(ColorIdValidator));
        }
    }
}
