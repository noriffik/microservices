﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Vehicles
{
    public class BoundModelKeySetValidatorTest
    {

        private readonly BoundModelKeySetValidator _validator = new BoundModelKeySetValidator();

        [Theory]
        [InlineData("QQQQ RRR123")]
        [InlineData("WW RRR123")]
        [InlineData("#$@ GWE123")]
        [InlineData(" ")]
        [InlineData("")]
        public void ShouldHaveError_WhenOneOfModelKeys_IsInvalid_Or_IsEmpty(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }

        [Theory]
        [InlineData("123123")]
        [InlineData("F4F123")]
        [InlineData("QQQ123 QQR321")]
        [InlineData("123321 12E123")]
        [InlineData("F4F123 F4T123")]
        public void ShouldNotHaveError_WhenAllModelKeys_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }

        [Theory]
        [InlineData("QQQ123 RRR321")]
        [InlineData("123321 RHE123")]
        [InlineData("F4F123 W6T123")]
        public void ShouldHaveError_WhenModelKeys_HaveDifferentModelId(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }
    }
}
