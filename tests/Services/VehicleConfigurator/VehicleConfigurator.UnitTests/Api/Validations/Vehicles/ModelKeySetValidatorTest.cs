﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Vehicles
{
    public class ModelKeySetValidatorTest
    {
        private readonly ModelKeySetValidator _validator = new ModelKeySetValidator();

        [Theory]
        [InlineData("QQQQ RRR123")]
        [InlineData("WW RRR123")]
        [InlineData("#$@ GWE123")]
        public void ShouldHaveError_WhenOneOfModelKeys_IsInvalid(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }

        [Theory]
        [InlineData("123123")]
        [InlineData("F4F123")]
        [InlineData("QQQ123 RRR321")]
        [InlineData("123321 RHE123")]
        [InlineData("F4F123 W6T123")]
        [InlineData("")]
        public void ShouldNotHaveError_WhenAllModelKeys_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }
    }
}
