﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Vehicles
{
    public class VehicleDescriptionQueryValidatorTest
    {
        private readonly VehicleDescriptionQueryValidator _validator = new VehicleDescriptionQueryValidator();

        [Fact]
        public void ShouldHaveChildValidator_ModelKeyValidator_ModelKey()
        {
            _validator.ShouldHaveChildValidator(c => c.ModelKey, typeof(ModelKeyValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_ColorIdValidator_ColorId()
        {
            _validator.ShouldHaveChildValidator(c => c.ColorId, typeof(ColorIdValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_PrNumberSetValidator_PrNumberSet()
        {
            _validator.ShouldHaveChildValidator(c => c.PrNumberSet, typeof(PrNumberSetValidator));
        }
    }
}
