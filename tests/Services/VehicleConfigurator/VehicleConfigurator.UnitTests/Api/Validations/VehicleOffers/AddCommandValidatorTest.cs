﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;
using Xunit;
using AddCommandValidator = NexCore.VehicleConfigurator.Api.Application.Validations.VehicleOffers.AddCommandValidator;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.VehicleOffers
{
    public class AddCommandValidatorTest
    {
        private readonly AddCommandValidator _validator = new AddCommandValidator();

        [Fact]
        public void ShouldHaveChildValidator_ModelKeyValidator_ModelKey()
        {
            _validator.ShouldHaveChildValidator(c => c.ModelKey, typeof(ModelKeyValidator));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenCatalogId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.CatalogId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenCatalogId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.CatalogId, value);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-122.32)]
        [InlineData(-0.12)]
        public void ShouldHaveError_WhenPrice_IsInValid(decimal value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Price, value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(42.3)]
        [InlineData(14523.123)]
        public void ShouldNotHaveError_WhenPrice_IsValid(decimal value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Price, value);
        }
    }
}
