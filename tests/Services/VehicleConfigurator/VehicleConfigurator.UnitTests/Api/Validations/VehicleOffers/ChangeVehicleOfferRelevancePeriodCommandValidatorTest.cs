﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations;
using NexCore.VehicleConfigurator.Api.Application.Validations.VehicleOffers;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.VehicleOffers
{
    public class ChangeVehicleOfferRelevancePeriodCommandValidatorTest
    {
        private readonly ChangeVehicleOfferRelevancePeriodCommandValidator _validator = new ChangeVehicleOfferRelevancePeriodCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldHaveError_WhenVehicleOfferId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Id, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenVehicleOfferId_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Id, 1);
        }

        [Fact]
        public void ShouldHaveChildValidator_RelevancePeriodModelValidator_ForRelevancePeriod()
        {
            _validator.ShouldHaveChildValidator(c => c.RelevancePeriod, typeof(PeriodModelValidator));
        }
    }
}
