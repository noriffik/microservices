﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.VehicleOffers;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.VehicleOffers
{
    public class UpdatePriceCommandValidatorTest
    {
        private readonly UpdatePriceCommandValidator _validator = new UpdatePriceCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Id, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        public void ShouldNotHaveError_WhenId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Id, value);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-122.32)]
        public void ShouldHaveError_WhenPrice_IsInValid(decimal value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Price, value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(14523.123)]
        public void ShouldNotHaveError_WhenPrice_IsValid(decimal value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Price, value);
        }
    }
}
