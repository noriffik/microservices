﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Offers;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Offers
{
    public class AddCommandValidatorTest
    {
        private readonly AddCommandValidator _validator = new AddCommandValidator();

        [Fact]
        public void ShouldNotHaveError()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.ConfigurationId, 1);
        }

        [Fact]
        public void ShouldHaveError()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ConfigurationId, -1);
        }
    }
}
