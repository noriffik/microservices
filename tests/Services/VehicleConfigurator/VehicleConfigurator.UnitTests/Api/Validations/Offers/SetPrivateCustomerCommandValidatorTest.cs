﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Offers;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Offers
{
    public class SetPrivateCustomerCommandValidatorTest
    {
        private readonly SetPrivateCustomerCommandValidator _validator = new SetPrivateCustomerCommandValidator();

        [Fact]
        public void OfferId_ShouldNotHaveError()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.OfferId, 1);
        }

        [Fact]
        public void OfferId_ShouldHaveError()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.OfferId, -1);
        }

        [Fact]
        public void PrivateCustomerId_ShouldNotHaveError()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.PrivateCustomerId, 1);
        }

        [Fact]
        public void PrivateCustomerId_ShouldHaveError()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.PrivateCustomerId, -1);
        }
    }
}
