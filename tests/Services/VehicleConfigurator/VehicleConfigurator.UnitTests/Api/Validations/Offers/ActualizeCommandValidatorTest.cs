﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Offers;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Offers
{
    public class ActualizeCommandValidatorTest
    {
        private readonly ActualizeCommandValidator _validator = new ActualizeCommandValidator();

        [Fact]
        public void ShouldNotHaveError()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.OfferId, 1);
        }

        [Fact]
        public void ShouldHaveError()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.OfferId, -1);
        }
    }
}
