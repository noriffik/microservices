﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Offers;
using NexCore.VehicleConfigurator.Domain.Offers;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Offers
{
    public class ChangeStatusCommandValidatorTest
    {
        private readonly ChangeStatusCommandValidator _validator = new ChangeStatusCommandValidator();

        [Fact]
        public void ShouldHaveError()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Id, 0);
        }

        [Fact]
        public void ShouldNotHaveError()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Id, 1);
            _validator.ShouldNotHaveValidationErrorFor(c => c.Status, OfferStatus.Accepted);
        }
    }
}
