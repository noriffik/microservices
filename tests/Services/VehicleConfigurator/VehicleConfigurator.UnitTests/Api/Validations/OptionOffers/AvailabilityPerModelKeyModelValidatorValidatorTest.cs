﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionOffers
{
    public class AvailabilityPerModelKeyModelValidatorValidatorTest
    {
        private readonly AvailabilityPerModelKeyModelValidator _validator = new AvailabilityPerModelKeyModelValidator();

        [Fact]
        public void ShouldHaveChildValidator_ModelKeyValidator_ForModelKey()
        {
            _validator.ShouldHaveChildValidator(c => c.ModelKey, typeof(ModelKeyValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_AvailabilityModelValidator_ForAvailability()
        {
            _validator.ShouldHaveChildValidator(c => c.Availability, typeof(AvailabilityModelValidator));
        }
    }
}
