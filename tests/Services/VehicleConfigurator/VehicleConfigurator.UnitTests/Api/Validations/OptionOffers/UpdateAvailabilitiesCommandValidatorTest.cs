﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionOffers
{
    public class UpdateAvailabilitiesCommandValidatorTest
    {
        private readonly UpdateAvailabilitiesCommandValidator _validator = new UpdateAvailabilitiesCommandValidator();

        [Fact]
        public void ShouldHaveChildValidator_PrNumberValidator_ForPrNumber()
        {
            _validator.ShouldHaveChildValidator(c => c.PrNumber, typeof(PrNumberValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_OfferModelValidator_ForOffers()
        {
            _validator.ShouldHaveChildValidator(c => c.Offers, typeof(AvailabilityPerModelKeyModelValidator));
        }
    }
}
