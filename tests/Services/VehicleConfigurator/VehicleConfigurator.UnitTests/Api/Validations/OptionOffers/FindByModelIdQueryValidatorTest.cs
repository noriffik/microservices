﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Components.Models;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionOffers
{
    public class FindByModelIdQueryValidatorTest
    {
        private readonly FindByModelIdQueryValidator _validator = new FindByModelIdQueryValidator();

        [Fact]
        public void ShouldHaveChildValidator_PrNumberValidator_ForPrNumber()
        {
            _validator.ShouldHaveChildValidator(c => c.PrNumber, typeof(PrNumberValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_ModelIdValidator_ForModelId()
        {
            _validator.ShouldHaveChildValidator(c => c.ModelId, typeof(ModelIdValidator));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenCatalogId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.CatalogId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenCatalogId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.CatalogId, value);
        }
    }
}
