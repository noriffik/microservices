﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionOffers
{
    public class UpdateInclusionCommandValidatorTest
    {
        private readonly UpdateInclusionCommandValidator _validator = new UpdateInclusionCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenVehicleOfferId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.VehicleOfferId, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenVehicleOfferId_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.VehicleOfferId, 1);
        }

        [Fact]
        public void ShouldHaveChildValidator_PrNumberValidator_ForPrNumber()
        {
            _validator.ShouldHaveChildValidator(c => c.PrNumber, typeof(PrNumberValidator));
        }

        [Theory]
        [InlineData(Inclusion.Manual)]
        [InlineData(Inclusion.Automatic)]
        public void ShouldNotHaveError_WhenInclusion_IsInEnum(Inclusion inclusion)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Inclusion, inclusion);
        }
    }
}
