﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionOffers
{
    public class RuleSetModelValidatorTest
    {
        private readonly RuleSetModelValidator _validator = new RuleSetModelValidator();

        [Fact]
        public void ShouldHaveChildValidator_RuleModelValidator_ForRuleSet()
        {
            _validator.ShouldHaveChildValidator(c => c.RuleSet, typeof(RuleModelValidator));
        }
    }
}
