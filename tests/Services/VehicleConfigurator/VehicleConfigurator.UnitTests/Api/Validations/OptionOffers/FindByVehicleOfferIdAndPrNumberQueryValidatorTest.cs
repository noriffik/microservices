﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionOffers
{
    public class FindByVehicleOfferIdAndPrNumberQueryValidatorTest
    {
        private readonly FindByVehicleOfferIdAndPrNumberQueryValidator _validator = new FindByVehicleOfferIdAndPrNumberQueryValidator();

        [Fact]
        public void ShouldHaveChildValidator_PrNumberValidator_ForPrNumber()
        {
            _validator.ShouldHaveChildValidator(c => c.PrNumber, typeof(PrNumberValidator));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenVehicleOffer_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.VehicleOfferId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenVehicleOffer_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.VehicleOfferId, value);
        }
    }
}
