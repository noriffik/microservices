﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionOffers
{
    public class RuleModelValidatorTest
    {
        private readonly RuleModelValidator _validator = new RuleModelValidator();

        [Theory]
        [InlineData("")]
        [InlineData("VAA:")]
        [InlineData("VA:")]
        [InlineData("V")]
        [InlineData(null)]
        public void ShouldHaveError_WhenRuleName_IsInValid(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.RuleName, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenRuleName_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.RuleName, "ZA");
        }

        [Theory]
        [InlineData("A")]
        [InlineData("AA")]
        [InlineData("AAA A")]
        [InlineData("AAA AA")]
        [InlineData("")]
        [InlineData(null)]
        public void ShouldHaveError_WhenPrNumbers_IsInValid(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.PrNumbers, value);
        }

        [Theory]
        [InlineData("AAA")]
        [InlineData("AAA BBB")]
        [InlineData("AAA CCC DDD")]
        public void ShouldNotHaveError_WhenPrNumbers_IsValid(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.PrNumbers, value);
        }

    }
}
