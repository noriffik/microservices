﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionOffers
{
    public class UpdateAvailabilityCommandValidatorTest
    {
        private readonly UpdateAvailabilityCommandValidator _validator = new UpdateAvailabilityCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenVehicleOfferId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.VehicleOfferId, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenVehicleOfferId_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.VehicleOfferId, 1);
        }

        [Fact]
        public void ShouldHaveChildValidator_OptionIdValidator_ForOptionId()
        {
            _validator.ShouldHaveChildValidator(c => c.PrNumber, typeof(PrNumberValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_AvailabilityModelValidator_ForAvailability()
        {
            _validator.ShouldHaveChildValidator(c => c.Availability, typeof(AvailabilityModelValidator));
        }

    }
}
