﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionOffers
{
    public class ChangeRelevancePeriodCommandValidatorTest
    {
        private readonly ChangeRelevancePeriodCommandValidator _validator = new ChangeRelevancePeriodCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ShouldHaveError_WhenVehicleOfferId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.VehicleOfferId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        public void ShouldNotHaveError_WhenVehicleOfferId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.VehicleOfferId, value);
        }

        [Fact]
        public void ShouldHaveChildValidator_PrNumberValidator_ForPrNumber()
        {
            _validator.ShouldHaveChildValidator(c => c.PrNumber, typeof(PrNumberValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_RelevancePeriodModelValidator_ForRelevancePeriod()
        {
            _validator.ShouldHaveChildValidator(c => c.RelevancePeriod, typeof(PeriodModelValidator));
        }
    }
}
