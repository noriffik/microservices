﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using Xunit;
using DeleteCommandValidator = NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers.DeleteCommandValidator;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionOffers
{
    public class DeleteCommandValidatorTest
    {
        private readonly DeleteCommandValidator _validator = new DeleteCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenVehicleOfferId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.VehicleOfferId, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenVehicleOfferId_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.VehicleOfferId, 1);
        }

        [Fact]
        public void ShouldHaveChildValidator_OptionIdValidator_ForOptionId()
        {
            _validator.ShouldHaveChildValidator(c => c.PrNumber, typeof(PrNumberValidator));
        }
    }
}
