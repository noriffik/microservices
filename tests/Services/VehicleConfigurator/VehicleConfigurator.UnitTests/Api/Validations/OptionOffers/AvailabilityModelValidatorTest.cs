﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionOffers
{
    public class AvailabilityModelValidatorTest 
    {
        private readonly AvailabilityModelValidator _validator = new AvailabilityModelValidator();
        
        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        public void ShouldHaveError_When_Type_IsInvalid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Type, (AvailabilityType)value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public void ShouldNotHaveError_When_Type_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Type, (AvailabilityType)value);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        public void ShouldHaveError_When_Reason_IsInvalid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(m => m.Reason, (ReasonType)value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public void ShouldNotHaveError_When_Reason_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(m => m.Reason, (ReasonType)value);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-122.32)]
        [InlineData(-0.12)]
        public void ShouldHaveError_WhenPrice_IsInValid(decimal value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Price, value);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(42.3)]
        [InlineData(14523.123)]
        public void ShouldNotHaveError_WhenPrice_IsValid(decimal value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Price, value);
        }
    }
}
