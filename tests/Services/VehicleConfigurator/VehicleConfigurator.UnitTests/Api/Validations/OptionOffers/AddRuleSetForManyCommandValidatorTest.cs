﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionOffers
{
    public class AddRuleSetForManyCommandValidatorTest
    {
        private readonly AddRuleSetForManyCommandValidator _validator = new AddRuleSetForManyCommandValidator();

        [Theory]
        [InlineData(1)]
        [InlineData(99)]
        public void ShouldNotHaveValidationError_CheckCatalogId(int catalogId)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.CatalogId, catalogId);
        }

        [Theory]
        [InlineData(-2)]
        [InlineData(0)]
        public void ShouldHaveError_WhenCatalogId_IsInvalid(int catalogId)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.CatalogId, catalogId);
        }

        [Fact]
        public void ShouldHaveChildValidator_ForCheckingModelKeySet()
        {
            _validator.ShouldHaveChildValidator(c => c.ModelKeySet, typeof(ModelKeySetValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_ForCheckingOptionId()
        {
            _validator.ShouldHaveChildValidator(c => c.OptionId, typeof(OptionIdValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_ForCheckingRuleSet()
        {
            _validator.ShouldHaveChildValidator(c => c.RuleSet, typeof(RuleSetModelValidator));
        }
    }
}
