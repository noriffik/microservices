﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Colors
{
    public class ColorIdValidatorTest
    {
        private readonly ColorIdValidator _validator = new ColorIdValidator();

        [Theory]
        [InlineData("QQQQQ")]
        [InlineData("WWW")]
        [InlineData(" ")]
        [InlineData("")]
        [InlineData("#$@w")]
        public void ShouldHaveError_WhenValue_IsInvalid(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }

        [Theory]
        [InlineData("WWWW")]
        [InlineData("1234")]
        [InlineData("F4Fj")]
        public void ShouldNotHaveError_WhenValue_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }
    }
}
