﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Colors
{
    public class FindByIdQueryValidatorTest
    {
        private readonly FindByIdQueryValidator _validator = new FindByIdQueryValidator();

        [Fact]
        public void ShouldHaveChildValidator_ColorIdValidator_ForId()
        {
            _validator.ShouldHaveChildValidator(c => c.Id, typeof(ColorIdValidator));
        }
    }
}
