﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;
using NexCore.VehicleConfigurator.Api.Application.Validations.ColorTypes;
using Xunit;
using AddCommandValidator = NexCore.VehicleConfigurator.Api.Application.Validations.Colors.AddCommandValidator;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Colors
{
    public class AddCommandValidatorTest
    {
        private readonly AddCommandValidator _validator = new AddCommandValidator();

        [Fact]
        public void ShouldHaveChildValidator_ColorIdValidator_ForId()
        {
            _validator.ShouldHaveChildValidator(c => c.Id, typeof(ColorIdValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_ColorTypeIdValidator_ForTypeId()
        {
            _validator.ShouldHaveChildValidator(c => c.TypeId, typeof(ColorTypeIdValidator));
        }

        [Fact]
        public void ShouldHaveChildValidator_ArgbValidator_ForArgb()
        {
            _validator.ShouldHaveChildValidator(c => c.Argb, typeof(ArgbValidator));
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void ShouldHaveError_WhenName_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Name, value);
        }

        [Theory]
        [InlineData("42")]
        [InlineData("W")]
        [InlineData("Name")]
        public void ShouldNotHaveError_WhenName_IsValid(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Name, value);
        }
    }
}
