﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Colors
{
    public class ArgbValidatorTest
    {
        private readonly ArgbValidator _validator = new ArgbValidator();

        [Theory]
        [InlineData("0d0a009a0")]
        [InlineData("0d0a0")]
        [InlineData("qwerty")]
        [InlineData("")]
        [InlineData("   ")]
        [InlineData("#$@1233")]
        public void ShouldHaveError_WhenValue_IsInvalid(string value)
        {
            _validator.TestValidate(value).ShouldHaveError();
        }

        [Theory]
        [InlineData("000FFF0f")]
        [InlineData("abCdeF12")]
        [InlineData("f1f24b9a")]
        public void ShouldNotHaveError_WhenValue_IsValid(string value)
        {
            _validator.TestValidate(value).ShouldNotHaveError();
        }
    }
}
