﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Api.Application.Validations;
using NexCore.VehicleConfigurator.Api.Application.Validations.Catalogs;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Catalogs
{
    public class ChangeRelevancePeriodCommandValidatorTest
    {
        private readonly ChangeRelevancePeriodCommandValidator _validator = new ChangeRelevancePeriodCommandValidator();

        [Fact]
        public void ShouldHaveError()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.CatalogId, 0);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(333)]
        public void ShouldNotHaveError(int id)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.CatalogId, id);
        }

        [Fact]
        public void ShouldHaveChildValidator_RelevancePeriodModelValidator_ForRelevancePeriod()
        {
            _validator.ShouldHaveChildValidator(c => c.RelevancePeriod, typeof(PeriodModelValidator));
        }

        [Fact]
        public void ShouldHaveError_WhenPeriodIsNull()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.RelevancePeriod, (PeriodModel) null);
        }
    }
}
