﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Catalogs;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Catalogs
{
    public class AddCommandValidatorTest
    {
        private readonly AddCommandValidator _validator = new AddCommandValidator();
        
        [Theory]
        [InlineData(1)]
        [InlineData(2019)]
        [InlineData(2033)]
        public void ShouldNotHaveError(int year)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Year, year);
        }

        [Theory]
        [InlineData(-3)]
        [InlineData(0)]
        [InlineData(124141234)]
        public void ShouldHaveError(int year)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Year, year);
        }
    }
}
