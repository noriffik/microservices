﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Catalogs
{
    public class ChangeStatusCommandValidatorTest
    {
        private readonly ChangeStatusCommandValidator _validator = new ChangeStatusCommandValidator();

        [Fact]
        public void ShouldHaveError()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Id, 0);
        }

        [Fact]
        public void ShouldNotHaveError()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Id, 1);
            _validator.ShouldNotHaveValidationErrorFor(c => c.Status, CatalogStatus.Published);
        }
    }
}
