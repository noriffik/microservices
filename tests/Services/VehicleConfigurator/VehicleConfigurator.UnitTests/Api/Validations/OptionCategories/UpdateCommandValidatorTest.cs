﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.OptionCategories;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionCategories
{
    public class UpdateCommandValidatorTest
    {
        private readonly UpdateCommandValidator _validator = new UpdateCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenId_IsInvalid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Id, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Id, value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData("   ")]
        public void ShouldHaveError_WhenName_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Name, value);
        }

        [Theory]
        [InlineData("42")]
        [InlineData("W")]
        [InlineData("Name")]
        [InlineData(":-) #$@(#)*^")]
        public void ShouldNotHaveError_WhenName_IsValid(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Name, value);
        }
    }
}
