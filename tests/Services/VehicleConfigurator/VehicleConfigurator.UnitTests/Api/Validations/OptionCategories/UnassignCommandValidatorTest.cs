﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.OptionCategories;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionCategories
{
    public class UnassignCommandValidatorTest
    {
        private readonly UnassignCommandValidator _validator = new UnassignCommandValidator();

        [Fact]
        public void ShouldHaveChildValidator_PrNumberValidator_ForId()
        {
            _validator.ShouldHaveChildValidator(c => c.Id, typeof(OptionIdValidator));
        }
    }
}