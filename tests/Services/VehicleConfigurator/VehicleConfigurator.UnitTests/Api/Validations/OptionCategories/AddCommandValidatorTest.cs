﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.OptionCategories;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionCategories
{
    public class AddCommandValidatorTest
    {
        private readonly AddCommandValidator _validator = new AddCommandValidator();

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData("   ")]
        public void ShouldHaveError_WhenName_IsEmpty(string value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Name, value);
        }

        [Theory]
        [InlineData("42")]
        [InlineData("W")]
        [InlineData("Name")]
        [InlineData(":-) #$@(#)*^")]
        public void ShouldNotHaveError_WhenName_IsValid(string value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Name, value);
        }
    }
}
