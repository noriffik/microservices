﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.OptionCategories;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.OptionCategories
{
    public class AssignCommandValidatorTest
    {
        private readonly AssignCommandValidator _validator = new AssignCommandValidator();

        [Fact]
        public void ShouldHaveChildValidator_PrNumberValidator_ForId()
        {
            _validator.ShouldHaveChildValidator(c => c.OptionId, typeof(OptionIdValidator));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenOptionCategoryId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.CategoryId, value);
        }

        [Theory]
        [InlineData("1")]
        [InlineData("42")]
        [InlineData("14523")]
        public void ShouldNotHaveError_WhenOptionCategoryId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.CategoryId, value);
        }
    }
}
