﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Colors;
using NexCore.VehicleConfigurator.Api.Application.Validations.Configuration;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Configurations
{
    public class ChangeColorCommandValidatorTest
    {
        private readonly ChangeColorCommandValidator _validator = new ChangeColorCommandValidator();

        [Fact]
        public void ShouldHaveChildValidator_ColorIdValidator_ForColorId()
        {
            _validator.ShouldHaveChildValidator(c => c.ColorId, typeof(ColorIdValidator));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenConfigurationId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ConfigurationId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenConfigurationId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.ConfigurationId, value);
        }

    }
}
