﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Configuration;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Configurations
{
    public class DeleteCommandValidatorTest
    {
        private readonly DeleteCommandValidator _validator = new DeleteCommandValidator();

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenVehicleOfferId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ConfigurationId, value);
        }

        [Fact]
        public void ShouldNotHaveError_WhenVehicleOfferId_IsValid()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.ConfigurationId, 1);
        }
    }
}
