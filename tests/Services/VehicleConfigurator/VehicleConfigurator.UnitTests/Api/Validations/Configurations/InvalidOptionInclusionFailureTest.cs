﻿using AutoFixture;
using FluentValidation.Results;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Validations.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Rules;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Configurations
{
    public class InvalidOptionInclusionFailureTest
    {
        private readonly PrNumber _attemptedValue = PrNumber.Parse("PN0");

        private readonly RuleViolation _details = new Fixture().Create<RuleViolation>();

        [Fact]
        public void ExceptionForInvalidOptionInclusion()
        {
            //Arrange
            var inner = new UnavailableOptionException
            {
                Details = new[]
                {
                    _details
                }
            };
            
            var expected = new UnavailableOptionValidationException("name", _attemptedValue, inner.Details);

            //Act
            var actual = Assert.Throws<UnavailableOptionValidationException>(() =>
                OptionValidationExtensions.Throw(inner, "name", _attemptedValue));
            
            //Assert
            Assert.Equal(expected.Message, actual.Message);
            Assert.Equal(expected.Errors, actual.Errors, PropertyComparer<ValidationFailure>.Instance);
        }

        [Fact]
        public void InvalidOptionInclusionValidationException_Ctor_WithReasons()
        {
            //Arrange
            var inner = new UnavailableOptionException
            {
                Details = new[]
                {
                   _details
                }
            };

            var errors = inner.Details.Select(r =>
                new OptionValidationFailure("name", r, _attemptedValue));
            
            var expected = new UnavailableOptionValidationException("Unacceptable option operation", errors);

            //Act
            var actual = new UnavailableOptionValidationException("name", _attemptedValue, inner.Details);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<UnavailableOptionValidationException>.Instance);
        }
    }
}
