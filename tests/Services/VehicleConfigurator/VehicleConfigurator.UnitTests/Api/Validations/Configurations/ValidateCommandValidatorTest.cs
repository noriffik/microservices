﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Configuration;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Configurations
{
    public class ValidateCommandValidatorTest
    {
        private readonly ValidateCommandValidator _validator = new ValidateCommandValidator();

        [Fact]
        public void ShouldNotHaveError()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.ModelYear, 2019);
            _validator.ShouldNotHaveValidationErrorFor(c => c.ModelKey, "JO0986");
            _validator.ShouldNotHaveValidationErrorFor(c => c.PrNumberSet, "AAA BBB CCC");
        }

        [Fact]
        public void ShouldHaveError()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ModelYear, 0);
            _validator.ShouldHaveValidationErrorFor(c => c.ModelKey, "JO9");
            _validator.ShouldHaveValidationErrorFor(c => c.PrNumberSet, "AAAC");
        }
    }
}
