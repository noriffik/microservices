﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Configuration;
using NexCore.VehicleConfigurator.Api.Application.Validations.Packages;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Configurations
{
    public class IncludePackageCommandValidatorTest
    {
        private readonly IncludePackageCommandValidator _validator = new IncludePackageCommandValidator();

        [Fact]
        public void ShouldHaveChildValidator_PackageCodeValidator_ForPackageCode()
        {
            _validator.ShouldHaveChildValidator(c => c.PackageCode, typeof(PackageCodeValidator));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-12342)]
        public void ShouldHaveError_WhenId_IsInValid(int value)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ConfigurationId, value);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(42)]
        [InlineData(14523)]
        public void ShouldNotHaveError_WhenId_IsValid(int value)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.ConfigurationId, value);
        }
    }
}
