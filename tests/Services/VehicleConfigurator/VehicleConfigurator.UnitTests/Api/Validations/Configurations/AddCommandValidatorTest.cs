﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Configuration;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Configurations
{
    public class AddCommandValidatorTest
    {
        private readonly AddCommandValidator _validator = new AddCommandValidator();

        [Theory]
        [InlineData("HJKHGD", 1)]
        [InlineData("TR56GH", 2)]
        [InlineData("TE23JD", 3)]
        public void ShouldNotHaveError(string modelKey, int catalogId)
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.ModelKey, modelKey);
            _validator.ShouldNotHaveValidationErrorFor(c => c.CatalogId, catalogId);
        }

        [Theory]
        [InlineData("asd", 0)]
        [InlineData("ASDASDASd", 0)]
        [InlineData("ASDASDd", 0)]
        [InlineData("ASDA$#", 0)]
        public void ShouldHaveError(string modelKey, int catalogId)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ModelKey, modelKey);
            _validator.ShouldHaveValidationErrorFor(c => c.CatalogId, catalogId);
        }
    }
}
