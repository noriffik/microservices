﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Configuration;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Configurations
{
    public class ExcludeOptionCommandValidatorTest
    { 
        private readonly ExcludeOptionCommandValidator _validator;

        public ExcludeOptionCommandValidatorTest()
        {
            _validator = new ExcludeOptionCommandValidator();
        }

        [Fact]
        public void ShouldHaveChildValidator_PrNumberValidator_ForPrNumber()
        {
            _validator.ShouldHaveChildValidator(c => c.PrNumber, typeof(PrNumberValidator));
        }

        [Theory]
        [InlineData(0, "as")]
        [InlineData(null, "ASDASDASd")]
        [InlineData(0, "ASDASDd")]
        [InlineData(null, "ASDA$#")]
        public void ShouldHaveError(int configurationId, string option)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.ConfigurationId, configurationId);
            _validator.ShouldHaveValidationErrorFor(c => c.PrNumber, option);
        }
    }
}
