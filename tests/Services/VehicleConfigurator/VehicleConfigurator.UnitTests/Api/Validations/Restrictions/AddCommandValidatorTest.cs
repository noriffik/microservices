﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.ModelComponents.Options;
using NexCore.VehicleConfigurator.Api.Application.Validations.Restrictions;
using NexCore.VehicleConfigurator.Api.Application.Validations.Vehicles;
using Xunit;
using AddCommandValidator = NexCore.VehicleConfigurator.Api.Application.Validations.Restrictions.AddCommandValidator;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Restrictions
{
    public class AddCommandValidatorTest
    {
        private readonly AddCommandValidator _validator = new AddCommandValidator();

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void ShouldHaveAnError(int catalogId)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.CatalogId, catalogId);
        }

        [Fact]
        public void ShouldNotHaveAnError()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.CatalogId, 1);
        }

        [Fact]
        public void ShouldHaveChildValidators()
        {
            _validator.ShouldHaveChildValidator(c => c.ModelKey, typeof(ModelKeyValidator));
            _validator.ShouldHaveChildValidator(c => c.PrNumbers, typeof(PrNumberSetValidator));
            _validator.ShouldHaveChildValidator(c => c.RelevancePeriod, typeof(RelevancePeriodModelValidator));
        }
    }
}
