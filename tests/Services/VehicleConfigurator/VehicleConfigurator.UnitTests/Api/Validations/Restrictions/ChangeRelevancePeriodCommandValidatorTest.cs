﻿using FluentValidation.TestHelper;
using NexCore.VehicleConfigurator.Api.Application.Validations.Restrictions;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Validations.Restrictions
{
    public class ChangeRelevancePeriodCommandValidatorTest
    {
        private readonly ChangeRelevancePeriodCommandValidator _validator = new ChangeRelevancePeriodCommandValidator();

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void ShouldHaveAnError(int restrictionId)
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Id, restrictionId);
        }

        [Fact]
        public void ShouldNotHaveAnError()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Id, 1);
        }

        [Fact]
        public void ShouldHaveChildValidators()
        {
            _validator.ShouldHaveChildValidator(c => c.RelevancePeriod, typeof(RelevancePeriodModelValidator));
        }
    }
}
