﻿using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Api.Application.Queries.Offers;
using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.Infrastructure.Views;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Offers
{
    public class ByIdQueryFixture : QueryFixture<ByIdQuery, OfferVehicleModel>
    {
        //Entities
        private Offer _offer;
        private OfferVehicleView _vehicle;

        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);

            var seeder = new OfferSeeder(context);

            //Seed not expected offers
            await seeder.AddRange(3);

            //Seed expected offer
            _offer = await seeder.Add();

            await seeder.SeedAsync();

            _vehicle = await context.Query<OfferVehicleView>()
                .SingleAsync(o => o.OfferId == _offer.Id);
        }

        public override ByIdQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new ByIdQuery
            {
                Id = _offer.Id
            };
        }

        public override ByIdQuery GetUnpreparedQuery()
        {
            return new ByIdQuery { Id = 43990 };
        }

        public override OfferVehicleModel GetResponse()
        {
            ThrowSetupIsRequired();

            return new OfferVehicleModel
            {
                Offer = Mapper.Map<OfferModel>(_offer),
                Vehicle = Mapper.Map<VehicleModel>(_vehicle)
            };
        }
    }
}
