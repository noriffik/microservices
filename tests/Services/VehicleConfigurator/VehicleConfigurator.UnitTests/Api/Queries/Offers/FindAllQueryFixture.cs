﻿using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Api.Application.Queries.Offers;
using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.Infrastructure.Views;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Offers
{
    public class FindAllQueryFixture : QueryFixture<FindAllQuery, IEnumerable<OfferVehicleModel>>
    {
        //Entities
        private IList<Offer> _offers;
        private IList<OfferVehicleView> _vehicles;

        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);

            var seeder = new OfferSeeder(context);

            _offers = await seeder.AddRange(3);

            await seeder.SeedAsync();

            _vehicles = await context.Query<OfferVehicleView>().ToListAsync();
        }

        public override FindAllQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return GetUnpreparedQuery();
        }

        public override FindAllQuery GetUnpreparedQuery()
        {
            return new FindAllQuery();
        }

        public override IEnumerable<OfferVehicleModel> GetResponse()
        {
            ThrowSetupIsRequired();

            return _offers.Select(offer => new OfferVehicleModel
            {
                Offer = Mapper.Map<OfferModel>(offer),
                Vehicle = Mapper.Map<VehicleModel>(_vehicles.Single(v => v.OfferId == offer.Id))
            });
        }
    }
}
