﻿using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.Offers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Offers
{
    public class ByIdQueryHandlerTest
    {
        private readonly ByIdQueryFixture _fixture;

        public ByIdQueryHandlerTest()
        {
            _fixture = new ByIdQueryFixture();
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new ByIdQueryHandler(null, _fixture.Mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = InMemoryDbContextFactory.Instance.Create())
                {
                    return new ByIdQueryHandler(context, null);
                }
            });
        }

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                await _fixture.Setup(context);

                var handler = new ByIdQueryHandler(context, _fixture.Mapper);

                //Act
                var actual = await handler.Handle(_fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(_fixture.GetResponse(), actual, PropertyComparer<OfferVehicleModel>.Instance);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new ByIdQueryHandler(context, _fixture.Mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () => handler.Handle(null, CancellationToken.None));
            }
        }

        [Fact]
        public async Task Handle_WhenNothing_IsFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new ByIdQueryHandler(context, _fixture.Mapper);

                //Act
                var result = await handler.Handle(_fixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }
    }
}
