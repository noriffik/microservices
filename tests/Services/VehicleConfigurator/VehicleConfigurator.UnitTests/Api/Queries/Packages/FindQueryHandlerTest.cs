﻿using AutoMapper;
using Moq;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.Packages;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Packages
{
    public class FindQueryHandlerTest
    {
        private readonly IMapper _mapper;

        public FindQueryHandlerTest()
        {
            _mapper = new Mock<IMapper>().Object;
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new FindQueryHandler(null, _mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = InMemoryDbContextFactory.Instance.Create())
                {
                    return new FindQueryHandler(context, null);
                }
            });
        }

        //Find by CatalogId

        [Fact]
        public async Task HandleByCatalogId()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByCatalogIdQueryFixture();
                await fixture.Setup(context);

                var expected = fixture.GetResponse();

                var handler = new FindQueryHandler(context, fixture.Mapper);
                
                //Act
                var actual = await handler.Handle(fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PackageModel>.Instance);
            }
        }

        [Fact]
        public Task HandleByCatalogId_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new FindQueryHandler(context, _mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as ByCatalogIdQuery, CancellationToken.None));
            }
        }

        [Fact]
        public async Task HandleByCatalogId_WhenNothing_IsFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByCatalogIdQueryFixture();
                var handler = new FindQueryHandler(context, fixture.Mapper);

                //Act
                var result = await handler.Handle(fixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Empty(result);
            }
        }

        //Find by PackageId

        [Fact]
        public async Task HandleById()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByCatalogIdQueryFixture();
                await fixture.Setup(context);

                var handler = new FindQueryHandler(context, fixture.Mapper);
                var expected = fixture.GetResponse();

                //Act
                var actual = await handler.Handle(fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PackageModel>.Instance);
            }
        }

        [Fact]
        public Task HandleById_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new FindQueryHandler(context, _mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as ByCatalogIdQuery, CancellationToken.None));
            }
        }

        [Fact]
        public async Task HandleById_WhenNothing_IsFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new ByCatalogIdQueryFixture();
                var handler = new FindQueryHandler(context, fixture.Mapper);

                //Act
                var result = await handler.Handle(fixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Empty(result);
            }
        }
    }
}
