﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Api.Application.Queries.Packages;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Linq;
using System.Threading.Tasks;
using FindByIdQuery = NexCore.VehicleConfigurator.Api.Application.Queries.Packages.FindByIdQuery;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Packages
{
    public class FindByIdQueryFixture : QueryFixture<FindByIdQuery, PackageModel>
    {
        private const int CatalogId = 10;
        
        private Package _package;
        private PackageModelRecord _packageModelRecord;
        
        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);
            
            var seeder = new PackageSeeder(context);

            await SeedOptions(seeder);

            //Seed not expected packages
            await seeder.AddRange(3);

            //Seed expected packages and options
            var packageId = seeder.Create<PackageId>();
            _package = await seeder.Add(packageId, new
            {
                catalogId = CatalogId,
                modelKeySet = BoundModelKeySet.Parse(packageId.ModelId.Value + "9999")
            });

            //Assign option category to some of options
            var optionCategory = await seeder.Add<OptionCategory>(124);
            var options = seeder.Get<Option, OptionId>().ToArray();
            for (int i = 0; i < options.Length; i += 3)
                options[i].OptionCategoryId = optionCategory.Id;

            await seeder.SeedAsync();
            
            var optionRecords = options.Select(o => new OptionModelRecord
            {
                Option = o,
                Category = o.OptionCategoryId == null
                    ? null
                    : optionCategory
            });

            _packageModelRecord = new PackageModelRecord
            {
                Package = _package,
                Applicability = _package.Applicability.Select(s => new ApplicabilitySpecificationModelRecord
                {
                    Specification = s,
                    Options = optionRecords.Where(o => s.PrNumbers.Values.Contains(o.Option.PrNumber))
                })
            };
        }

        private static async Task SeedOptions(PackageSeeder seeder)
        {
            var options = await seeder.AddRange<Option, OptionId>(5);

            options.ToList().ForEach(o =>
            {
                if (o.OptionCategoryId.HasValue)
                    seeder.Add<OptionCategory>(o.OptionCategoryId.Value);
            });
        }

        public override FindByIdQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new FindByIdQuery
            {
                Id = _package.Id.Value
            };
        }

        public override FindByIdQuery GetUnpreparedQuery()
        {
            return new FindByIdQuery
            {
                Id = "99999"
            };
        }

        public override PackageModel GetResponse()
        {
            ThrowSetupIsRequired();
            
            return Mapper.Map<PackageModel>(_packageModelRecord);
        }
    }
}
