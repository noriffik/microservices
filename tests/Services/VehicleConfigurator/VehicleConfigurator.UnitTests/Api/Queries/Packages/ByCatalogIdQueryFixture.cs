﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Api.Application.Queries.Packages;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Packages
{
    public class ByCatalogIdQueryFixture : QueryFixture<ByCatalogIdQuery, IEnumerable<PackageModel>>
    {
        private const int CatalogId = 32;
        
        private readonly List<Package> _packages;
        private List<OptionModelRecord> _optionRecords;

        public ByCatalogIdQueryFixture()
        {
            _packages = new List<Package>();
        }

        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);
            
            var seeder = new PackageSeeder(context);

            //Setup options
            await SeedOptions(seeder);

            //Seed not expected packages
            await seeder.AddRange(3);

            //Seed expected packages and options
            for (var i = 0; i < 3; i++)
            {
                var packageId = seeder.Create<PackageId>();
                var package = await seeder.Add(packageId, new
                {
                    catalogId = CatalogId,
                    modelKeySet = BoundModelKeySet.Parse(packageId.ModelId.Value + "9999")
                });

                _packages.Add(package);
            }

            //Seed empty expected package
            _packages.Add(await seeder.Add(new {catalogId = CatalogId, prNumberSet = new PrNumberSet()}));

            //Assign option category to some of options
            var optionCategory = await seeder.Add<OptionCategory>(124);
            var options = seeder.Get<Option, OptionId>().ToArray();
            for (int i = 0; i < options.Length; i += 3)
                options[i].OptionCategoryId = optionCategory.Id;

            await seeder.SeedAsync();

            _optionRecords = options.Select(o => new OptionModelRecord
                {
                    Option = o,
                    Category = o.OptionCategoryId == null
                        ? null
                        : optionCategory
                })
                .ToList();
        }

        private static async Task SeedOptions(PackageSeeder seeder)
        {
            var options = (await seeder.AddRange<Option, OptionId>(5)).ToList();
            foreach (var option in options)
            {
                if (option.OptionCategoryId.HasValue)
                    await seeder.Add<OptionCategory>(option.OptionCategoryId.Value);
            }
        }

        public override ByCatalogIdQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new ByCatalogIdQuery
            {
                CatalogId = CatalogId
            };
        }

        public override ByCatalogIdQuery GetUnpreparedQuery()
        {
            return new ByCatalogIdQuery
            {
                CatalogId = 3934
            };
        }

        public override IEnumerable<PackageModel> GetResponse()
        {
            ThrowSetupIsRequired();

            foreach (var package in _packages)
            {
                var packageModelRecord = new PackageModelRecord
                {
                    Package = package,
                    Applicability = package.Applicability.Select(s => new ApplicabilitySpecificationModelRecord
                    {
                        Specification = s,
                        Options = _optionRecords.Where(o => s.PrNumbers.Values.Contains(o.Option.PrNumber))
                    })
                };

                yield return Mapper.Map<PackageModel>(packageModelRecord);
            }
        }
    }
}
