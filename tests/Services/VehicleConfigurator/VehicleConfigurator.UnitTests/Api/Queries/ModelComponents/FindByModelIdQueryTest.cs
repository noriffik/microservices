﻿using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Queries.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelComponents
{
    public abstract class FindByModelIdQueryTest<TComponent, TId>
        where TComponent : ModelComponent<TId>
        where TId : EntityId
    {
        private readonly Func<string, TId> _idFactory;
        
        protected FindByModelIdQueryTest(Func<string, TId> idFactory)
        {
            _idFactory = idFactory;
        }

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new ModelComponentSeeder<TComponent, TId>(context);
                await seeder.Add<TComponent, TId>();

                var model = await seeder.Add<Model, ModelId>(ModelId.Parse("AA"));

                var components = await seeder.AddRange(
                    Enumerable.Range(4, 6)
                        .Select(n => _idFactory(model.Id.Value + n))
                        .ToArray());

                await seeder.SeedAsync();

                var query = new FindByModelIdQuery<TComponent, TId>
                {
                    ModelId = model.Id.Value
                };
                var handler = new FindQueryHandler<TComponent, TId>(context);
                var expected = handler.ModelMapper.MapMany(components);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<ModelComponentModel>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new FindByModelIdQuery<TComponent, TId>
                {
                    ModelId = "99"
                };
                var handler = new FindQueryHandler<TComponent, TId>(context);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(actual);
            }
        }
    }
}
