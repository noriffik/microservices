﻿using NexCore.VehicleConfigurator.Api.Application.Queries.ModelComponents;
using NexCore.VehicleConfigurator.UnitTests.Fixtures;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelComponents
{
    public class FindQueryHandlerTest
    {
        private readonly FindQueryHandler<TestModelComponent, TestModelComponentId> _handler;
        
        public FindQueryHandlerTest()
        {
            var context = InMemoryDbContextFactory.Instance.Create();
            _handler = new FindQueryHandler<TestModelComponent, TestModelComponentId>(context);
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Injection.OfConstructor(typeof(FindQueryHandler<TestModelComponent, TestModelComponentId>)).HasNullGuard();
        }

        [Theory]
        [InlineData(typeof(FindByIdQuery<TestModelComponent, TestModelComponentId>))]
        [InlineData(typeof(FindAllQuery<TestModelComponent, TestModelComponentId>))]
        public virtual Task Handle_GivenRequest_IsNull_Throws(Type query)
        {
            return Assert.RequestHandler(_handler).ThrowsOnNull(query);
        }

        [Fact]
        public void ModelMapper()
        {
            Assert.Injection
                .OfProperty(_handler, nameof(_handler.ModelMapper))
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}
