﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelComponents.Equipments
{
    public class FindAllQueryTest : FindAllQueryTest<Equipment, EquipmentId>
    {
    }
}
