﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelComponents.Equipments
{
    public class FindByIdQueryTest : FindByIdQueryTest<Equipment, EquipmentId>
    {
    }
}
