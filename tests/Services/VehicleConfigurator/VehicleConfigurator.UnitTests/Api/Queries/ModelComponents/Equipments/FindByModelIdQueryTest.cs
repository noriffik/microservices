﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelComponents.Equipments
{
    public class FindByModelIdQueryTest : FindByModelIdQueryTest<Equipment, EquipmentId>
    {
        public FindByModelIdQueryTest() : base(EquipmentId.Parse)
        {
        }
    }
}
