﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelComponents.Engines
{
    public class FindByModelIdQueryTest : FindByModelIdQueryTest<Engine, EngineId>
    {
        public FindByModelIdQueryTest() : base(EngineId.Parse)
        {
        }
    }
}
