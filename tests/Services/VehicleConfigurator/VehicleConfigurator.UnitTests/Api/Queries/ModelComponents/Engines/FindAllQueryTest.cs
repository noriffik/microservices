﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelComponents.Engines
{
    public class FindAllQueryTest : FindAllQueryTest<Engine, EngineId>
    {
    }
}
