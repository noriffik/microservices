﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelComponents.Gearboxes
{
    public class FindByModelIdQueryTest : FindByModelIdQueryTest<Gearbox, GearboxId>
    {
        public FindByModelIdQueryTest() : base(GearboxId.Parse)
        {
        }
    }
}
