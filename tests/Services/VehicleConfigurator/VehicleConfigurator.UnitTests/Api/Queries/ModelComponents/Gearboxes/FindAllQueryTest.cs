﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelComponents.Gearboxes
{
    public class FindAllQueryTest : FindAllQueryTest<Gearbox, GearboxId>
    {
    }
}
