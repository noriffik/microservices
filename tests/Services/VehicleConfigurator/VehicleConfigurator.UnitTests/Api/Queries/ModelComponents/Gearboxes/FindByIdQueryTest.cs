﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelComponents.Gearboxes
{
    public class FindByIdQueryTest : FindByIdQueryTest<Gearbox, GearboxId>
    {
    }
}
