﻿using AutoFixture;
using NexCore.Domain;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Queries.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelComponents
{
    public abstract class FindByIdQueryTest<TComponent, TId>
        where TComponent : ModelComponent<TId>
        where TId : EntityId
    {
        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Assert
                var seeder = new ModelComponentSeeder<TComponent, TId>(context);
                await seeder.AddRange(3);

                var component = await seeder.Add();

                await seeder.SeedAsync();
                
                var query = new FindByIdQuery<TComponent, TId> {Id = component.Id.Value};
                var handler = new FindQueryHandler<TComponent, TId>(context);
                var expected = handler.ModelMapper.Map(component);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<ModelComponentModel>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new ModelComponentSeeder<TComponent, TId>(context);
                await seeder.AddRange(3);

                var id = seeder.Fixture.Create<TId>();

                await seeder.SeedAsync();

                var query = new FindByIdQuery<TComponent, TId> {Id = id.Value};
                var handler = new FindQueryHandler<TComponent, TId>(context);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }
    }
}