﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelComponents.Bodies
{
    public class FindByModelIdQueryTest : FindByModelIdQueryTest<Body, BodyId>
    {
        public FindByModelIdQueryTest() : base(BodyId.Parse)
        {
        }
    }
}
