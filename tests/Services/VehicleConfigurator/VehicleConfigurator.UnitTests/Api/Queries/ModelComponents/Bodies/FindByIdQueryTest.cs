﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelComponents.Bodies
{
    public class FindByIdQueryTest : FindByIdQueryTest<Body, BodyId>
    {
    }
}
