﻿using NexCore.Domain;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Queries.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelComponents
{
    public abstract class FindAllQueryTest<TComponent, TId>
        where TComponent : ModelComponent<TId>
        where TId : EntityId
    {
        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new ModelComponentSeeder<TComponent, TId>(context);
                var components = await seeder.AddRange(1115);

                await seeder.SeedAsync();
                
                var query = new FindAllQuery<TComponent, TId>();
                var handler = new FindQueryHandler<TComponent, TId>(context);
                var expected = handler.ModelMapper.MapMany(components);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<ModelComponentModel>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new FindAllQuery<TComponent, TId>();
                var handler = new FindQueryHandler<TComponent, TId>(context);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(actual);
            }
        }
    }
}
