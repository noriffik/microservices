﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ColorTypes
{
    public class FindByIdQueryFixture : QueryFixture<FindByIdQuery, ColorTypeModel>
    {
        private ColorType _colorType;
        
        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);
            
            var seeder = new EntitySeeder<ColorType, ColorTypeId>(context);
            seeder.Fixture.Customizations.Add(new ModelComponentBuilder());

            //Seed not expected ColorTypes
            await seeder.AddRange(5);

            //Seed expected ColorType
            _colorType = await seeder.Add();

            await seeder.SeedAsync();
        }

        public override FindByIdQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new FindByIdQuery
            {
                Id = _colorType.Id.Value
            };
        }

        public override FindByIdQuery GetUnpreparedQuery()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            return fixture.Build<FindByIdQuery>()
                .With(c => c.Id, fixture.Create<ColorTypeId>().Value)
                .Create();
        }

        public override ColorTypeModel GetResponse()
        {
            return new ColorTypeModel
            {
                Id = _colorType.Id.Value,
                Name = _colorType.Name
            };
        }
    }
}
