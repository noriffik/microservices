﻿using NexCore.Domain.Vehicles;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ColorTypes
{
    public class FindAllQueryFixture : QueryFixture<FindAllQuery, IEnumerable<ColorTypeModel>>
    {
        private List<ColorType> _colorTypes;
        
        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);
            
            var seeder = new EntitySeeder<ColorType, ColorTypeId>(context);
            seeder.Fixture.Customizations.Add(new ModelComponentBuilder());

            _colorTypes = (await seeder.AddRange(5)).ToList();

            await seeder.SeedAsync();
        }

        public override FindAllQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new FindAllQuery();
        }

        public override FindAllQuery GetUnpreparedQuery()
        {
            throw new NotSupportedException();
        }

        public override IEnumerable<ColorTypeModel> GetResponse()
        {
            return _colorTypes.Select(c => new ColorTypeModel
            {
                Id = c.Id.Value,
                Name = c.Name
            });
        }
    }
}
