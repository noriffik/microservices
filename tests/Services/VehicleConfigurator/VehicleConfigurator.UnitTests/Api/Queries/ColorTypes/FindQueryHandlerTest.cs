﻿using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ColorTypes
{
    public class FindQueryHandlerTest
    {
        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            var fixture = new FindByIdQueryFixture();

            Assert.Throws<ArgumentNullException>("context", () => new FindQueryHandler(null, fixture.Mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = InMemoryDbContextFactory.Instance.Create())
                {
                    return new FindQueryHandler(context, null);
                }
            });
        }
        
        [Fact]
        public async Task HandleById()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new FindByIdQueryFixture();

                await fixture.Setup(context);
                
                var handler = new FindQueryHandler(context, fixture.Mapper);

                //Act
                var actual = await handler.Handle(fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(fixture.GetResponse(), actual, PropertyComparer<ColorTypeModel>.Instance);
            }
        }

        [Fact]
        public Task HandleById_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new FindByIdQueryFixture();

                var handler = new FindQueryHandler(context, fixture.Mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as FindByIdQuery, CancellationToken.None));
            }
        }

        [Fact]
        public async Task HandleById_WhenNothing_IsFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new FindByIdQueryFixture();
                var handler = new FindQueryHandler(context, fixture.Mapper);

                //Act
                var result = await handler.Handle(fixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }

        //FindAll

        [Fact]
        public async Task HandleAll()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new FindAllQueryFixture();
                await fixture.Setup(context);

                var handler = new FindQueryHandler(context, fixture.Mapper);

                //Act
                var actual = await handler.Handle(fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(fixture.GetResponse(), actual, PropertyComparer<ColorTypeModel>.Instance);
            }
        }

        [Fact]
        public Task HandleAll_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = new FindAllQueryFixture();
                var handler = new FindQueryHandler(context, fixture.Mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as FindAllQuery, CancellationToken.None));
            }
        }
    }
}
