﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.Infrastructure.Views;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Configurations
{
    public class UnboundQueryFixture : QueryFixture<UnboundQuery, IEnumerable<QueryResponse>>
    {
        //Entities
        private Configuration[] _configurations;
        private VehicleOffer[] _vehicleOffers;
        private OfferedVehicleView[] _vehicles;
        
        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);
            
            var seeder = new ConfigurationSeeder(context);

            //Seed configurations
            _configurations = await seeder.AddRange(3);

            //Fetch expected vehicle offers
            _vehicleOffers = _configurations.Select(c => seeder.Get<VehicleOffer>()
                    .Where(v => v.ModelKey == c.ModelKey)
                    .Single(v => v.CatalogId == c.CatalogId))
                .ToArray();

            //Fetch expected vehicle components
            _vehicles = _configurations.Select(c => new OfferedVehicleView
                {
                    Model = seeder.Find<Model, ModelId>(c.ModelKey.ModelId),
                    Body = seeder.Find<Body, BodyId>(c.ModelKey.BodyId),
                    Equipment = seeder.Find<Equipment, EquipmentId>(c.ModelKey.EquipmentId),
                    Engine = seeder.Find<Engine, EngineId>(c.ModelKey.EngineId),
                    Gearbox = seeder.Find<Gearbox, GearboxId>(c.ModelKey.GearboxId)
                })
                .ToArray();

            await seeder.SeedAsync();
        }

        public override UnboundQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return GetUnpreparedQuery();
        }

        public override UnboundQuery GetUnpreparedQuery()
        {
            return new UnboundQuery();
        }

        public override IEnumerable<QueryResponse> GetResponse()
        {
            ThrowSetupIsRequired();

            return Enumerable.Range(0, _configurations.Length)
                .Select(n => new QueryResponse
                {
                    Configuration = Mapper.Map<ConfigurationModel>(_configurations[n]),
                    VehicleOffer = Mapper.Map<VehicleOfferModel>(_vehicleOffers[n]),
                    Vehicle = Mapper.Map<VehicleModel>(_vehicles[n])
                });
        }
    }
}
