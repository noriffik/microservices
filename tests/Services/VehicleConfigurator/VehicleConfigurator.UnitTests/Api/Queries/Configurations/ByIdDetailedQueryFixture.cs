﻿using Moq;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Models.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Pricing;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.Infrastructure.Views;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Configurations
{
    public class ByIdDetailedQueryFixture : QueryFixture<ByIdDetailedQuery, DetailedQueryResponse>
    {
        private readonly Mock<IConfigurationPricingService> _configurationPricingService;
        private readonly Mock<IPackagePricingService> _packagePricingService;
        private readonly Mock<IPricingService> _pricingService;

        private decimal[] _optionPrices;

        //Entities
        private Configuration _configuration;
        private VehicleOffer _vehicleOffer;
        private OfferedVehicleView _vehicle;
        private IDictionary<int, OptionCategory> _categories;
        private List<OptionOffer> _optionOffers;
        private Option[] _modelOptions;
        private Option[] _includedOptions;
        private Option[] _chosenOptions;
        private Color[] _colors;
        private ColorType[] _colorTypes;
        private Package[] _packages;
        private PackagePrice[] _packagePrices;
        private ConfigurationPrice _calculation;

        public readonly DateTime Now;

        public IConfigurationPricingService ConfigurationPricingService => _configurationPricingService.Object;

        public IPackagePricingService PackagePricingService => _packagePricingService.Object;

        public IPricingService PricingService => _pricingService.Object;

        public ByIdDetailedQueryFixture()
        {
            _configurationPricingService = new Mock<IConfigurationPricingService>();
            _packagePricingService = new Mock<IPackagePricingService>();
            _pricingService = new Mock<IPricingService>();

            Now = DateTime.Now;
        }

        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);

            //Setup retail prices
            _pricingService.Setup(s => s.CalculateRetail(It.IsAny<decimal>(), Now))
                .Returns<decimal, DateTime>((@base, time) => Task.FromResult(CalculateRetail(@base)));

            var seeder = new ConfigurationSeeder(context);

            //Setup 
            var prNumbers = new PrNumberSet(seeder.CreateMany<PrNumber>(9));
            var modelIds = seeder.CreateMany<ModelId>(2).ToList();
            var modelId = modelIds.Last();

            await SetupConfigurations(seeder, modelId);
            await SetupOptions(seeder, modelIds, prNumbers);

            //Available options for model
            _modelOptions = seeder.Get<Option, OptionId>()
                .Where(o => o.ModelId == modelId)
                .ToArray();

            //Assign options to categories
            _categories = (await seeder.AddRange<OptionCategory>(1, 2)).ToDictionary(c => c.Id);
            var optionsWithCategory = _modelOptions.Take(6);

            Enumerable.Range(1, 2).ToList().ForEach(categoryId =>
            {
                optionsWithCategory.Skip((categoryId - 1) * 3).Take(3 * categoryId)
                    .ToList()
                    .ForEach(o => o.OptionCategoryId = categoryId);
            });

            //Setup option availability
            const int purchasableOptionCount = 4;
            const int includedOptionCount = 2;
            const int offeredOptionCount = purchasableOptionCount + includedOptionCount;
            const int includedInConfigurationCount = 3;

            //Setup options with offers
            _optionPrices = seeder.CreateMany<decimal>(purchasableOptionCount).ToArray();

            var purchasable = Enumerable.Range(0, purchasableOptionCount)
                .Select(p => Availability.Purchasable(_optionPrices[p]));
            var included = Enumerable.Range(0, includedOptionCount)
                .Select(n => Availability.Included(ReasonType.SerialEquipment));

            var availabilities = purchasable
                .Concat(included)
                .ToList();

            var offeredOptions = _modelOptions.Take(offeredOptionCount).ToList();
            var optionAvailability = new Dictionary<PrNumber, Availability>(offeredOptionCount);
            for (var i = 0; i < offeredOptionCount; i++)
                optionAvailability[offeredOptions[i].PrNumber] = availabilities[i];

            foreach (var availability in optionAvailability)
            {
                if (_vehicleOffer.HasOptionOffer(availability.Key))
                    _vehicleOffer.DeleteOptionOffer(availability.Key);

                _vehicleOffer.AddOptionOffer(availability.Key, availability.Value);
            }

            _optionOffers = _vehicleOffer.Options.ToList();

            //Include chosen option offers
            _chosenOptions = offeredOptions.Take(includedInConfigurationCount).ToArray();
            _chosenOptions.Select(o => o.PrNumber)
                .ToList()
                .ForEach(_configuration.IncludeOption);

            //Include serial and chosen options
            _includedOptions = offeredOptions.Skip(purchasableOptionCount)
                .Take(includedOptionCount)
                .Concat(_chosenOptions)
                .ToArray();

            //Setup Package
            var packageOptions = _chosenOptions.Select(c => c.PrNumber).ToList();

            var requiredSpecifications = (IEnumerable<IApplicabilitySpecification>)packageOptions.SkipLast(1)
                .Select(o => new RequiredSpecification(o));
            var choiceSpecification = (IEnumerable<IApplicabilitySpecification>)packageOptions.TakeLast(1)
                .Select(o => new ChoiceSpecification(new PrNumberSet(o, _modelOptions.Last().PrNumber)));
            var packageSpecifications = requiredSpecifications.Concat(choiceSpecification);

            _packages = Enumerable.Range(0, 2)
                .Select(n =>
                {
                    var package = new Package(
                        PackageId.Next(_configuration.ModelKey.ModelId, seeder.Create<PackageCode>()),
                        _configuration.CatalogId,
                        seeder.Create<string>(),
                        new BoundModelKeySet(new[] { _configuration.ModelKey }),
                        new Applicability(packageSpecifications.ToArray()))
                    {
                        Price = seeder.Create<decimal>()
                    };
                    package.ChangeStatus(PackageStatus.Published);

                    return package;
                })
                .ToArray();

            await seeder.AddRange<Package, PackageId>(_packages);

            _packagePrices = seeder.CreateMany<PackagePrice>(_packages.Length).ToArray();

            for (var i = 0; i < _packages.Length; i++)
            {
                var package = _packages[i];
                _packagePricingService.Setup(s => s.Calculate(_vehicleOffer.Options, package, Now))
                    .ReturnsAsync(_packagePrices[i]);
            }

            _colors = seeder.Get<Color, ColorId>().ToArray();
            _colorTypes = seeder.Get<ColorType, ColorTypeId>().ToArray();

            await seeder.SeedAsync();

            //Set calculation
            _calculation = seeder.Create<ConfigurationPrice>();

            //Setup service
            _configurationPricingService.Setup(s => s.Calculate(_configuration.Id, Now))
                .ReturnsAsync(_calculation);
        }

        private async Task SetupOptions(ConfigurationSeeder seeder, IEnumerable<ModelId> modelIds, PrNumberSet prNumbers)
        {
            foreach (var modelId in modelIds)
            {
                foreach (var prNumber in prNumbers.Values)
                {
                    if (seeder.Has<Option, OptionId>(OptionId.Next(modelId, prNumber)))
                        continue;

                    await seeder.Add<Option, OptionId>(OptionId.Next(modelId, prNumber));
                }
            }
        }

        private async Task SetupConfigurations(ConfigurationSeeder seeder, ModelId modelId)
        {
            const int catalogId = 2;

            var modelKey = ModelKey.Parse(modelId.Value + "XXXX");

            //Seed not expected configurations
            await seeder.Add(1, new { catalogId = 1 });
            await seeder.Add(2, new { catalogId = 1, modelKey });
            await seeder.Add(3, new { catalogId, modelKey });

            //Seed expected configuration
            _configuration = await seeder.Add(4, new { catalogId, modelKey });

            //Fetch expected vehicle offer
            _vehicleOffer = seeder.Get<VehicleOffer>()
                .Where(v => v.CatalogId == catalogId)
                .Single(v => v.ModelKey == modelKey);

            //Setup vehicle offer base price
            _vehicleOffer.Price = seeder.Create<decimal>();

            //Fetch expected vehicle components
            _vehicle = new OfferedVehicleView
            {
                Model = seeder.Find<Model, ModelId>(modelKey.ModelId),
                Body = seeder.Find<Body, BodyId>(modelKey.BodyId),
                Equipment = seeder.Find<Equipment, EquipmentId>(modelKey.EquipmentId),
                Engine = seeder.Find<Engine, EngineId>(modelKey.EngineId),
                Gearbox = seeder.Find<Gearbox, GearboxId>(modelKey.GearboxId)
            };
        }

        private decimal CalculateRetail(decimal basedOn)
        {
            return basedOn * 10;
        }

        public override ByIdDetailedQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new ByIdDetailedQuery
            {
                Id = _configuration.Id
            };
        }

        public override ByIdDetailedQuery GetUnpreparedQuery()
        {
            return new ByIdDetailedQuery { Id = 239 };
        }

        public override DetailedQueryResponse GetResponse()
        {
            ThrowSetupIsRequired();

            var availableColors = GetColorsResponse().ToList();

            return new DetailedQueryResponse
            {
                Configuration = Mapper.Map<ConfigurationModel>(_configuration),
                VehicleOffer = Mapper.Map<VehicleOfferModel>(GetVehicleOfferModel()),
                Vehicle = Mapper.Map<VehicleModel>(_vehicle),
                Options = Enumerable.Range(0, _modelOptions.Length)
                    .Select(GetOptionResponse)
                    .ToList(),
                Packages = GetPackageResponse(),
                Prices = Mapper.Map<ConfigurationPricingModel>(_calculation),
                AvailableColors = availableColors,
                Color = availableColors.Single(c => c.Color.Id == _configuration.ColorId.Value)
            };
        }

        private AvailableOptionModel GetOptionResponse(int index)
        {
            Debug.Assert(_modelOptions.Length > index);

            var categoryId = _modelOptions[index].OptionCategoryId;

            return new AvailableOptionModel
            {
                Category = categoryId.HasValue
                    ? Mapper.Map<OptionCategoryModel>(_categories[categoryId.Value])
                    : null,
                Offer = GetOptionOfferModel(index),
                Option = Mapper.Map<OptionModel>(_modelOptions[index]),
                IsIncluded = _includedOptions.Contains(_modelOptions[index])
            };
        }

        private IEnumerable<PackageModel> GetPackageResponse()
        {
            var requiredSpecifications = _chosenOptions
                .SkipLast(1)
                .OrderBy(o => o.Id.Value)
                .Select(o => new ApplicabilitySpecificationModel
                {
                    Options = new[] { GetExtendedByCategoryOptionResponse(o) },
                    Type = ApplicabilitySpecificationType.Required
                }).ToList();

            var choiceSpecifications = _chosenOptions.TakeLast(1).Select(o => new ApplicabilitySpecificationModel
            {
                Options = new[] { o, _modelOptions.Last() }.Select(GetExtendedByCategoryOptionResponse),
                Type = ApplicabilitySpecificationType.Choice
            }).ToList();

            //Order by Specification.AsString
            var applicability = choiceSpecifications.Concat(requiredSpecifications)
                .OrderBy(s => s.Options.Select(o => o.Option.PrNumber).Aggregate((a, b) => $"{a}/{b}")).ToList();

            var models = new List<PackageModel>(_packages.Length);
            for (var i = 0; i < _packages.Length; i++)
            {
                var model = new PackageModel
                {
                    Id = _packages[i].Id.Value,
                    Name = _packages[i].Name,
                    Inclusion = PackageInclusion.Available,
                    Applicability = applicability,
                    Price = Mapper.Map<PackagePriceModel>(_packagePrices[i])
                };

                models.Add(model);
            }

            return models;
        }

        private ExtendedByCategoryOptionModel GetExtendedByCategoryOptionResponse(Option option)
        {
            var categoryId = option.OptionCategoryId;

            return new ExtendedByCategoryOptionModel
            {
                Category = categoryId.HasValue
                    ? Mapper.Map<OptionCategoryModel>(_categories[categoryId.Value])
                    : null,
                Option = Mapper.Map<OptionModel>(option)
            };
        }

        private OptionOfferModel GetOptionOfferModel(int index)
        {
            if (_optionOffers.Count <= index)
                return null;

            var model = Mapper.Map<OptionOfferModel>(_optionOffers[index]);

            model.Availability.Price.Retail = CalculateRetail(model.Availability.Price.Base);

            return model;
        }

        private VehicleOfferModel GetVehicleOfferModel()
        {
            var model = Mapper.Map<VehicleOfferModel>(_vehicleOffer);
            model.Price.Retail = CalculateRetail(model.Price.Base);

            return model;
        }

        private IEnumerable<ColorResponseModel> GetColorsResponse()
        {
            foreach (var colorOffer in _vehicleOffer.Colors)
            {
                var color = _colors.Single(c => c.Id == colorOffer.ColorId);
                var type = _colorTypes.Single(c => c.Id == color.TypeId);

                yield return new ColorResponseModel
                {
                    ColorOffer = new ColorOfferModel
                    {
                        Id = colorOffer.Id,
                        ColorId = colorOffer.ColorId.Value,
                        Relevance = colorOffer.Relevance,
                        RelevancePeriod = new RelevancePeriodModel
                        {
                            From = colorOffer.RelevancePeriod.From,
                            To = colorOffer.RelevancePeriod.To,
                        },
                        Price = new PriceModel
                        {
                            Base = colorOffer.Price,
                            Retail = CalculateRetail(colorOffer.Price)
                        }
                    },
                    Color = new ColorModel
                    {
                        Id = color.Id,
                        TypeId = color.TypeId.Value,
                        Name = color.Name,
                        Argb = color.Argb.ToArgb().ToString("X8")
                    },
                    Type = new ColorTypeModel
                    {
                        Id = type.Id,
                        Name = type.Name
                    }
                };
            }
        }
    }
}
