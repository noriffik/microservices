﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.Infrastructure.Views;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Configurations
{
    public class ByIdQueryFixture : QueryFixture<ByIdQuery, QueryResponse>
    {
        //Entities
        private Configuration _configuration;
        private VehicleOffer _vehicleOffer;
        private OfferedVehicleView _vehicle;
        
        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);
            
            var seeder = new ConfigurationSeeder(context);

            //Seed not expected configurations
            await seeder.AddRange(3);

            //Seed expected configuration
            _configuration = await seeder.Add();

            var modelKey = _configuration.ModelKey;

            //Seed option
            var option = await seeder.Add<Option, OptionId>(
                OptionId.Next(modelKey.ModelId, seeder.Create<PrNumber>()));

            //Fetch expected vehicle offer
            _vehicleOffer = seeder.Get<VehicleOffer>().Single(v => v.ModelKey == modelKey);
            _vehicleOffer.AddOptionOffer(option.PrNumber, seeder.Create<Availability>());

            //Include option in the configuration
            _configuration.IncludeOption(option.PrNumber);

            //Fetch expected vehicle components
            _vehicle = new OfferedVehicleView
            {
                Model = seeder.Find<Model, ModelId>(modelKey.ModelId),
                Body = seeder.Find<Body, BodyId>(modelKey.BodyId),
                Equipment = seeder.Find<Equipment, EquipmentId>(modelKey.EquipmentId),
                Engine = seeder.Find<Engine, EngineId>(modelKey.EngineId),
                Gearbox = seeder.Find<Gearbox, GearboxId>(modelKey.GearboxId)
            };

            await seeder.SeedAsync();
        }

        public override ByIdQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new ByIdQuery
            {
                Id = _configuration.Id
            };
        }

        public override ByIdQuery GetUnpreparedQuery()
        {
            return new ByIdQuery { Id = 239 };
        }

        public override QueryResponse GetResponse()
        {
            ThrowSetupIsRequired();

            return new QueryResponse
            {
                Configuration = Mapper.Map<ConfigurationModel>(_configuration),
                VehicleOffer = Mapper.Map<VehicleOfferModel>(_vehicleOffer),
                Vehicle = Mapper.Map<VehicleModel>(_vehicle)
            };
        }
    }
}
