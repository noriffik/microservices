﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Pricing;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Configurations.Pricing
{
    public class CalculationOptionModelMappingTest : IClassFixture<ModelMappingFixture>
    {
        private readonly ModelMappingFixture _fixture;

        public CalculationOptionModelMappingTest(ModelMappingFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void Map()
        {
            //Arrange
            var prNumber = _fixture.Create<PrNumber>();
            var price = _fixture.Create<Price>();

            var source = new Dictionary<PrNumber, Price>
            {
                { prNumber, price }
            };

            var destination = new List<CalculationOptionModel>(new[]
            {
                new CalculationOptionModel
                {
                    PrNumber = prNumber.Value,
                    Price = _fixture.Map<PriceModel>(price)
                }
            });

            //Act
            var result = _fixture.Map<IEnumerable<CalculationOptionModel>>(source);

            //Assert
            Assert.Equal(destination, result, PropertyComparer<IEnumerable<CalculationOptionModel>>.Instance);
        }
    }
}
