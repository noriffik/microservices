﻿using AutoFixture;
using AutoMapper;
using NexCore.VehicleConfigurator.Api.Infrastructure.AutoMapper;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Configurations.Pricing
{
    public class ModelMappingFixture : Fixture
    {
        public IMapper Mapper { get; }

        public ModelMappingFixture()
        {
            Customizations.Add(new ModelComponentBuilder());

            var mapperConfig = new MapperConfiguration(
                c => c.AddProfiles(typeof(DefaultProfile).Assembly));

            Mapper = mapperConfig.CreateMapper();
        }

        public TDestination Map<TDestination>(object source)
        {
            return Mapper.Map<TDestination>(source);
        }

        public TDestination Map<TSource, TDestination>(TSource source, TDestination destination)
        {
            return Mapper.Map(source, destination);
        }
    }
}
