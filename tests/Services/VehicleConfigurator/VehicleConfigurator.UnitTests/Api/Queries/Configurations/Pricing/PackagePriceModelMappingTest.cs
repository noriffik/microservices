﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Pricing;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Configurations.Pricing
{
    public class PackagePriceModelMappingTest : IClassFixture<ModelMappingFixture>
    {
        private readonly ModelMappingFixture _fixture;

        public PackagePriceModelMappingTest(ModelMappingFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void Map()
        {
            //Arrange
            var prNumber = _fixture.Create<PrNumber>();
            var price = _fixture.Create<Price>();

            var source = new PackagePrice(price, price, OptionsPrice.Create(new Dictionary<PrNumber, Price>
            {
                {prNumber, price}
            }));

            var destination = new PackagePriceModel
            {
                Options = new List<CalculationOptionModel>(new[]
                {
                    new CalculationOptionModel
                    {
                        PrNumber = prNumber.Value,
                        Price = _fixture.Map<PriceModel>(price)
                    }
                }),
                Price = _fixture.Map<PriceModel>(source.Price),
                Savings = _fixture.Map<PriceModel>(source.Savings)
            };

            //Act
            var result = _fixture.Map<PackagePriceModel>(source);

            //Assert
            Assert.Equal(destination, result, PropertyComparer<PackagePriceModel>.Instance);
        }
    }
}
