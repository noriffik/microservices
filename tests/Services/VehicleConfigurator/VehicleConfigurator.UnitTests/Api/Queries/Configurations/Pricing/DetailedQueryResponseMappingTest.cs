﻿using AutoFixture;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Configurations.Pricing
{
    public class DetailedQueryResponseMappingTest : IClassFixture<ModelMappingFixture>
    {
        private readonly ModelMappingFixture _fixture;

        public DetailedQueryResponseMappingTest(ModelMappingFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void Map()
        {
            //Arrange
            var source = _fixture.Create<ConfigurationPrice>();
            var destination = new DetailedQueryResponse();
            var expected = new DetailedQueryResponse
            {
                Prices = _fixture.Map<ConfigurationPricingModel>(source)
            };

            //Act
            var actual = _fixture.Map(source, destination);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<DetailedQueryResponse>.Instance);
        }

        [Fact]
        public void Map_GivenDestination_IsNull_ReturnsEmpty()
        {
            //Arrange
            var source = _fixture.Create<ConfigurationPrice>();
            var destination = new DetailedQueryResponse
            {
                Prices = _fixture.Map<ConfigurationPricingModel>(source)
            };

            //Act
            var result = _fixture.Map<DetailedQueryResponse>(source);

            //Assert
            Assert.Equal(destination, result, PropertyComparer<DetailedQueryResponse>.Instance);
        }
    }
}
