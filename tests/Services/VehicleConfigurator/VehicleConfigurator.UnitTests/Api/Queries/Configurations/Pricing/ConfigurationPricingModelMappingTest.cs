﻿using AutoFixture;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations.Pricing;
using NexCore.VehicleConfigurator.Domain.Configurations.Pricing;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Configurations.Pricing
{
    public class ConfigurationPricingModelMappingTest : IClassFixture<ModelMappingFixture>
    {
        private readonly ModelMappingFixture _fixture;

        public ConfigurationPricingModelMappingTest(ModelMappingFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void Map()
        {
            //Arrange
            var source = _fixture.Create<ConfigurationPrice>();
            var destination = new ConfigurationPricingModel
            {
                OnDate = source.OnDate,
                Options = _fixture.Map<OptionsPriceModel>(source.Options),
                Package = _fixture.Map<PackagePriceModel>(source.Package),
                PackageSavings = _fixture.Map<PackageSavingsModel>(source.PackageSavings),
                Total = _fixture.Map<PriceModel>(source.Total),
                Vehicle = _fixture.Map<PriceModel>(source.Vehicle),
                Color = _fixture.Map<PriceModel>(source.Color)
            };

            //Act
            var result = _fixture.Map<ConfigurationPricingModel>(source);

            //Assert
            Assert.Equal(destination, result, PropertyComparer<ConfigurationPricingModel>.Instance);
        }
    }
}
