﻿using NexCore.Common;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Configurations
{
    public class ByIdDetailedQueryHandlerTest : IClassFixture<ByIdDetailedQueryFixture>
    {
        private readonly ByIdDetailedQueryFixture _fixture;

        public ByIdDetailedQueryHandlerTest(ByIdDetailedQueryFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(
                "context", () => new ByIdDetailedQueryHandler(null, _fixture.Mapper, _fixture.ConfigurationPricingService, _fixture.PackagePricingService, _fixture.PricingService));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = InMemoryDbContextFactory.Instance.Create())
                {
                    return new ByIdDetailedQueryHandler(context, null, _fixture.ConfigurationPricingService, _fixture.PackagePricingService, _fixture.PricingService);
                }
            });
        }

        [Fact]
        public void Ctor_GivenConfigurationPricingService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("configurationPricingService", () =>
            {
                using (var context = InMemoryDbContextFactory.Instance.Create())
                {
                    return new ByIdDetailedQueryHandler(context, _fixture.Mapper, null, _fixture.PackagePricingService, _fixture.PricingService);
                }
            });
        }

        [Fact]
        public async Task Handle()
        {
            using (new SystemTimeContext(_fixture.Now))
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                await _fixture.Setup(context);

                var handler = new ByIdDetailedQueryHandler(context, _fixture.Mapper, _fixture.ConfigurationPricingService, _fixture.PackagePricingService, _fixture.PricingService);
                var expected = _fixture.GetResponse();

                //Act
                var actual = await handler.Handle(_fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<DetailedQueryResponse>.Instance);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new ByIdDetailedQueryHandler(context, _fixture.Mapper, _fixture.ConfigurationPricingService, _fixture.PackagePricingService, _fixture.PricingService);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () => handler.Handle(null, CancellationToken.None));
            }
        }

        [Fact]
        public async Task Handle_WhenNothing_IsFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new ByIdDetailedQueryHandler(context, _fixture.Mapper, _fixture.ConfigurationPricingService, _fixture.PackagePricingService, _fixture.PricingService);

                //Act
                var result = await handler.Handle(_fixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }
    }
}
