﻿using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Configurations
{
    public class ByCatalogIdQueryHandlerTest : IClassFixture<ByCatalogIdQueryFixture>
    {
        private readonly ByCatalogIdQueryFixture _fixture;

        public ByCatalogIdQueryHandlerTest(ByCatalogIdQueryFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new ByCatalogIdQueryHandler(null, _fixture.Mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = InMemoryDbContextFactory.Instance.Create())
                {
                    return new ByCatalogIdQueryHandler(context, null);
                }
            });
        }

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                await _fixture.Setup(context);

                var expected = _fixture.GetResponse();
                var handler = new ByCatalogIdQueryHandler(context, _fixture.Mapper);

                //Act
                var actual = await handler.Handle(_fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<QueryResponse>.Instance);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new ByCatalogIdQueryHandler(context, _fixture.Mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () => handler.Handle(null, CancellationToken.None));
            }
        }

        [Fact]
        public async Task Handle_WhenNothing_IsFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new ByCatalogIdQueryHandler(context, _fixture.Mapper);

                //Act
                var result = await handler.Handle(_fixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Empty(result);
            }
        }
    }
}
