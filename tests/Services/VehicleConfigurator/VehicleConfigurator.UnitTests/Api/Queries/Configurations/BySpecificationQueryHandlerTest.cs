﻿using MediatR;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Configurations
{
    public class BySpecificationQueryHandlerTest
    {
        [Fact]
        public async Task Handle()
        {
            //Arrange
            var year = new Year(2019);
            var specification = new ConfigurationSpecification(ModelKey.Parse("NS738Z"),
                PrNumberSet.Parse("EA7 WV2 YOU SPE"));
            var colorId = ColorId.Parse("CLR1");
            var configurationId = 213;
            var catalogId = 3;

            var configuration = new Configuration(configurationId, catalogId, specification.ModelKey, colorId);
            var query = new BySpecificationQuery
            {
                ModelYear = year.Value,
                ModelKey = specification.ModelKey.Value,
                PrNumbers = specification.PrNumbers.AsString
            };
            var expected = new DetailedQueryResponse();
            var work = new Mock<IUnitOfWork>();
            var mediator = new Mock<IMediator>();
            var configurationService = new Mock<IConfigurationService>();

            mediator.Setup(m => m.Send(It.Is<ByIdDetailedQuery>(q => q.Id == configurationId), CancellationToken.None))
                .ReturnsAsync(expected);

            work.Setup(w => w.Commit(CancellationToken.None))
                .Returns(Task.CompletedTask);

            using (Sequence.Create())
            {
                configurationService.Setup(s => s.Build(year, specification))
                    .InSequence()
                    .ReturnsAsync(configuration);
                configurationService.Setup(s => s.Remove(configurationId))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                var handler = new BySpecificationQueryHandler(mediator.Object, work.Object, configurationService.Object);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual);
            }
        }
    }
}
