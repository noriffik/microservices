﻿using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FindAllQuery = NexCore.VehicleConfigurator.Api.Application.Queries.Colors.FindAllQuery;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Colors
{
    public class FindAllQueryFixture : QueryFixture<FindAllQuery, IEnumerable<ColorModel>>
    {
        private List<Color> _colors;
        private List<ColorType> _types;

        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);

            var seeder = new ColorSeeder(context);

            _colors = (await seeder.AddRange(5)).ToList();

            await seeder.SeedAsync();

            _types = await context.ColorTypes.ToListAsync();
        }

        public override FindAllQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new FindAllQuery();
        }

        public override FindAllQuery GetUnpreparedQuery()
        {
            throw new NotSupportedException();
        }

        public override IEnumerable<ColorModel> GetResponse()
        {
            foreach (var color in _colors)
            {
                var type = _types.Single(t => t.Id == color.TypeId);

                yield return new ColorModel
                {
                    Id = color.Id.Value,
                    Name = color.Name,
                    Argb = color.Argb.ToArgb().ToString("X8"),
                    Type = new ColorTypeModel
                    {
                        Id = type.Id.Value,
                        Name = type.Name
                    }
                };
            }
        }
    }
}
