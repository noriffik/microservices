﻿using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using FindAllQuery = NexCore.VehicleConfigurator.Api.Application.Queries.Colors.FindAllQuery;
using FindByIdQuery = NexCore.VehicleConfigurator.Api.Application.Queries.Colors.FindByIdQuery;
using FindQueryHandler = NexCore.VehicleConfigurator.Api.Application.Queries.Colors.FindQueryHandler;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Colors
{
    public class FindQueryHandlerTest
    {
        private readonly FindByIdQueryFixture _byIdQueryFixture;
        private readonly FindAllQueryFixture _allQueryFixture;

        public FindQueryHandlerTest()
        {
            _byIdQueryFixture = new FindByIdQueryFixture();
            _allQueryFixture = new FindAllQueryFixture();
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new FindQueryHandler(null, _byIdQueryFixture.Mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = InMemoryDbContextFactory.Instance.Create())
                {
                    return new FindQueryHandler(context, null);
                }
            });
        }

        //FindByIdQuery

        [Fact]
        public async Task HandleById()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                await _byIdQueryFixture.Setup(context);

                var handler = new FindQueryHandler(context, _byIdQueryFixture.Mapper);

                //Act
                var actual = await handler.Handle(_byIdQueryFixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(_byIdQueryFixture.GetResponse(), actual, PropertyComparer<ColorModel>.Instance);
            }
        }

        [Fact]
        public Task HandleById_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new FindQueryHandler(context, _byIdQueryFixture.Mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as FindByIdQuery, CancellationToken.None));
            }
        }

        [Fact]
        public async Task HandleById_WhenNothing_IsFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new FindQueryHandler(context, _byIdQueryFixture.Mapper);

                //Act
                var result = await handler.Handle(_byIdQueryFixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }

        //FindAll

        [Fact]
        public async Task HandleAll()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                await _allQueryFixture.Setup(context);

                var handler = new FindQueryHandler(context, _allQueryFixture.Mapper);

                //Act
                var actual = await handler.Handle(_allQueryFixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(_allQueryFixture.GetResponse(), actual, PropertyComparer<ColorModel>.Instance);
            }
        }

        [Fact]
        public Task HandleAll_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new FindQueryHandler(context, _allQueryFixture.Mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as FindAllQuery, CancellationToken.None));
            }
        }
    }
}
