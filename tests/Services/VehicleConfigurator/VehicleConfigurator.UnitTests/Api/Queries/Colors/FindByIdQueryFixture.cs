﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Threading.Tasks;
using FindByIdQuery = NexCore.VehicleConfigurator.Api.Application.Queries.Colors.FindByIdQuery;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Colors
{
    public class FindByIdQueryFixture : QueryFixture<FindByIdQuery, ColorModel>
    {
        private Color _color;
        private ColorType _type;
        
        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);
            
            var seeder = new ColorSeeder(context);

            //Seed not expected Colors
            await seeder.AddRange(5);

            //Seed expected Color
            _color = await seeder.Add();
            _type = seeder.Find<ColorType, ColorTypeId>(_color.TypeId);

            await seeder.SeedAsync();
        }

        public override FindByIdQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new FindByIdQuery
            {
                Id = _color.Id.Value
            };
        }

        public override FindByIdQuery GetUnpreparedQuery()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            return fixture.Build<FindByIdQuery>()
                .With(c => c.Id, fixture.Create<ColorId>().Value)
                .Create();
        }

        public override ColorModel GetResponse()
        {
            return new ColorModel
            {
                Id = _color.Id.Value,
                Name = _color.Name,
                Argb = _color.Argb.ToArgb().ToString("X8"),
                Type = new ColorTypeModel
                {
                    Id = _type.Id.Value,
                    Name = _type.Name
                }
            };
        }
    }
}
