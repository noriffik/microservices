﻿using AutoFixture;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.OptionCategories
{
    public class OptionCategoryModelMapperTest
    {
        private readonly IMapper<OptionCategory, OptionCategoryModel> _mapper = new OptionCategoryModelMapper();

        [Fact]
        public void Map()
        {
            var optionCategory = new Fixture().CustomizeConstructor<OptionCategory>(10).Create<OptionCategory>();

            //Act
            var result = _mapper.Map(optionCategory);

            //Assert
            Assert.Equal(optionCategory.Id, result.Id);
            Assert.Equal(optionCategory.Name, result.Name);
        }

        [Fact]
        public void Map_GivenSource_IsNull_ReturnsNull()
        {
            //Act
            var result = _mapper.Map(null);

            //Assert
            Assert.Null(result);
        }
    }
}
