﻿using NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.OptionCategories
{
    public class FindQueryHandlerTest
    {
        private readonly FindQueryHandler _handler;

        public FindQueryHandlerTest()
        {
            var context = InMemoryDbContextFactory.Instance.Create();
            _handler = new FindQueryHandler(context);
        }

        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(FindQueryHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.RequestHandler(_handler).ThrowsOnNull(typeof(FindAllQuery));
        }

        [Fact]
        public void ModelMapper()
        {
            Assert.Injection
                .OfProperty(_handler, nameof(_handler.ModelMapper))
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}
