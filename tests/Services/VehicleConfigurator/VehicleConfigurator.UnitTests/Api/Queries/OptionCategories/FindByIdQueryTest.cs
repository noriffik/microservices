﻿using NexCore.Testing;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.OptionCategories
{
    public class FindByIdQueryTest
    {
        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new EntitySeeder<OptionCategory>(context);

                var category = await seeder.Add();

                await seeder.SeedAsync();

                var query = new FindByIdQuery {Id = category.Id};
                var handler = new FindQueryHandler(context);
                var expected = handler.ModelMapper.Map(category);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<OptionCategoryModel>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new FindByIdQuery {Id = 123};
                var handler = new FindQueryHandler(context);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }
    }
}
