﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Moq;
using NexCore.Application.Queries;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Models.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Engines;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Gearboxes;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;
using NexCore.VehicleConfigurator.Api.Application.Queries.Models;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers;
using NexCore.VehicleConfigurator.Api.Infrastructure.AutoMapper;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using FindAllQuery = NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.FindAllQuery;
using FindQueryHandler = NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.FindQueryHandler;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.VehicleOffers
{
    public class FindAllQueryTest
    {
        private readonly FindAllQuery _query;

        public FindAllQueryTest()
        {
            _query = new FindAllQuery();

        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                var vehicleSeeder = new VehicleOfferSeeder(context);
                var colorSeeder = new ColorSeeder(context);

                var vehicleOffers = await vehicleSeeder.AddRange(6);
                foreach (var vehicleOffer in vehicleOffers.TakeLast(3))
                {
                    var color = await colorSeeder.Add();
                    vehicleOffer.AddColorOffer(color.Id, colorSeeder.Create<decimal>());
                    vehicleOffer.SetDefaultColor(color.Id);
                    vehicleOffer.Publish();
                }

                await colorSeeder.SeedAsync();
                await vehicleSeeder.SeedAsync();

                var models = await context.Models.ToDictionaryAsync(k => k.Id);
                var bodies = await context.Bodies.ToDictionaryAsync(k => k.Id);
                var equipments = await context.Equipments.ToDictionaryAsync(k => k.Id);
                var engines = await context.Engines.ToDictionaryAsync(k => k.Id);
                var gearboxes = await context.Gearboxes.ToDictionaryAsync(k => k.Id);
                var colorOfferModels = vehicleOffers.ToDictionary(v => v.Id, v => GetDefaultColorOfferModel(v, context));

                var collection = vehicleOffers.Select(vehicleOffer =>
                {
                    var model = models[vehicleOffer.ModelId];
                    var body = bodies[vehicleOffer.BodyId];
                    var equipment = equipments[vehicleOffer.EquipmentId];
                    var engine = engines[vehicleOffer.EngineId];
                    var gearbox = gearboxes[vehicleOffer.GearboxId];

                    return new VehicleOfferModel
                    {
                        Id = vehicleOffer.Id,
                        ModelKey = vehicleOffer.ModelKey.Value,
                        CatalogId = vehicleOffer.CatalogId,
                        RelevancePeriod = new RelevancePeriodModel
                        {
                            From = vehicleOffer.RelevancePeriod.From,
                            To = vehicleOffer.RelevancePeriod.To
                        },
                        Model = new ModelModel { Id = model.Id, Name = model.Name },
                        Body = new ModelComponentModel { Id = body.Id, ModelId = body.ModelId, Name = body.Name },
                        Equipment = new ModelComponentModel { Id = equipment.Id, ModelId = equipment.ModelId, Name = equipment.Name },
                        Engine = new EngineModel { Id = engine.Id, ModelId = engine.ModelId, Name = engine.Name, FuelType = engine.FuelType },
                        Gearbox = new GearboxModel { Id = gearbox.Id, ModelId = gearbox.ModelId, Name = gearbox.Name, Type = gearbox.Type, GearBoxCategory = gearbox.GearBoxCategory },
                        Price = vehicleOffer.Price,
                        Status = vehicleOffer.Status,
                        Relevance = vehicleOffer.Relevance,
                        DefaultColor = colorOfferModels[vehicleOffer.Id]
                    };
                }).ToArray();

                var expected = new PagedResponse<VehicleOfferModel>(PageOptions.Default, collection.Length, collection);

                var mapperConfig = new MapperConfiguration(
                    c => c.AddProfiles(typeof(DefaultProfile).Assembly));
                var mapper = mapperConfig.CreateMapper();

                var handler = new FindQueryHandler(context, mapper);

                //Act
                var actual = await handler.Handle(_query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<VehicleOfferModel>>.Instance);
            }
        }

        private static ColorOfferModel GetDefaultColorOfferModel(VehicleOffer vehicleOffer, VehicleConfiguratorContext context)
        {
            if (vehicleOffer.DefaultColorId == null)
                return null;

            var colorOffer = vehicleOffer.DefaultColor;
            var color = context.Colors.Single(c => c.Id == colorOffer.ColorId);
            var colorType = context.ColorTypes.Single(c => c.Id == color.TypeId);

            return new ColorOfferModel
            {
                Id = colorOffer.Id,
                Price = colorOffer.Price,
                Relevance = colorOffer.Relevance,
                RelevancePeriod = new RelevancePeriodModel
                {
                    From = colorOffer.RelevancePeriod.From,
                    To = colorOffer.RelevancePeriod.To
                },
                Color = new ColorModel
                {
                    Id = color.Id,
                    Name = color.Name,
                    Argb = color.Argb.ToArgb().ToString("X8"),
                    Type = new ColorTypeModel
                    {
                        Id = colorType.Id,
                        Name = colorType.Name
                    }
                }
            };
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new FindQueryHandler(context, new Mock<IMapper>().Object);

                //Act
                var actual = await handler.Handle(_query, CancellationToken.None);

                //Assert
                Assert.Empty(actual.Items);
            }
        }
    }
}
