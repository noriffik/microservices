﻿using AutoMapper;
using Moq;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.VehicleOffers
{
    public class FindQueryHandlerTest
    {
        private readonly FindQueryHandler _handler;
        private readonly VehicleConfiguratorContext _context;

        public FindQueryHandlerTest()
        {
            _context = InMemoryDbContextFactory.Instance.Create();
            _handler = new FindQueryHandler(_context, new Mock<IMapper>().Object);
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new FindQueryHandler(null, new Mock<IMapper>().Object));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () => new FindQueryHandler(_context, null));
        }

        [Theory]
        [InlineData(typeof(FindByIdQuery))]
        [InlineData(typeof(FindAllQuery))]
        public Task Handle_WhenGivenRequest_IsNull_Throws(Type query)
        {
            return Assert.RequestHandler(_handler).ThrowsOnNull(query);
        }
    }
}
