﻿using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Api.Application.Models.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.AvailableColors;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.VehicleOffers.AvailableColors
{
    public class AvailableColorsQueryFixture : QueryFixture<AvailableColorsQuery, IEnumerable<AvailableColorModel>>
    {
        private VehicleOffer _vehicleOffer;
        private List<Color> _colors;
        private List<ColorType> _colorTypes;

        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);

            var colorSeeder = new ColorSeeder(context);
            var vehicleOfferSeeder = new VehicleOfferSeeder(context);

            var vehicleOffers = await vehicleOfferSeeder.AddRange(5);
            _colors = (await colorSeeder.AddRange(10)).ToList();

            //Add unexpected colorOffers
            foreach (var color in _colors.SkipLast(3))
                foreach (var vehicleOffer in vehicleOffers.SkipLast(2))
                    vehicleOffer.AddColorOffer(color.Id, vehicleOfferSeeder.Create<decimal>());

            //Add expected colorOffers
            _vehicleOffer = vehicleOffers.Last();
            foreach (var color in _colors.Skip(2).SkipLast(2))
                _vehicleOffer.AddColorOffer(color.Id, vehicleOfferSeeder.Create<decimal>());

            await colorSeeder.SeedAsync();

            _colorTypes = await context.ColorTypes.ToListAsync();
        }

        public override AvailableColorsQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new AvailableColorsQuery
            {
                VehicleOfferId = _vehicleOffer.Id
            };
        }

        public override AvailableColorsQuery GetUnpreparedQuery()
        {
            return new AvailableColorsQuery
            {
                VehicleOfferId = 100
            };
        }

        public override IEnumerable<AvailableColorModel> GetResponse()
        {
            foreach (var color in _colors)
            {
                var colorOffer = _vehicleOffer.Colors.SingleOrDefault(c => c.ColorId == color.Id);
                var colorType = _colorTypes.Single(t => t.Id == color.TypeId);

                yield return new AvailableColorModel
                {
                    Color = new ColorModel
                    {
                        Id = color.Id,
                        Name = color.Name,
                        Argb = color.Argb.ToArgb().ToString("X8"),
                        Type = new ColorTypeModel
                        {
                            Id = colorType.Id,
                            Name = colorType.Name
                        }
                    },
                    ColorOffer = colorOffer == null
                                 ? null
                                 : new AvailableColorModel.ColorOfferModel
                                 {
                                     Id = colorOffer.Id,
                                     Price = colorOffer.Price,
                                     Relevance = colorOffer.Relevance,
                                     RelevancePeriod = new RelevancePeriodModel
                                     {
                                         From = colorOffer.RelevancePeriod.From,
                                         To = colorOffer.RelevancePeriod.To
                                     }
                                 }
                };
            }
        }
    }
}
