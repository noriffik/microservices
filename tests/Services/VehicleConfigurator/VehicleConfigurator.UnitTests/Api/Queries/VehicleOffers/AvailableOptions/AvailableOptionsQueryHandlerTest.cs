﻿using AutoMapper;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.AvailableOptions;
using NexCore.VehicleConfigurator.Api.Infrastructure.AutoMapper;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.VehicleOffers.AvailableOptions
{
    public class AvailableOptionsQueryHandlerTest
    {
        private readonly IMapper _mapper;

        public AvailableOptionsQueryHandlerTest()
        {
            var mapperConfig = new MapperConfiguration(
                c => c.AddProfiles(typeof(DefaultProfile).Assembly));

            _mapper = mapperConfig.CreateMapper();
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(
                "context", () => new AvailableOptionsQueryHandler(null, _mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(
                "mapper", () => new AvailableOptionsQueryHandler(InMemoryDbContextFactory.Instance.Create(), null));
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                var seeder = new VehicleOfferSeeder(context);

                var vehicleOffer = await seeder.Add();

                var expected = await SeedOptionOffers(seeder, vehicleOffer);
            
                var query = new AvailableOptionsQuery { VehicleOfferId = vehicleOffer.Id };
                var handler = new AvailableOptionsQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<AvailableOptionModel>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_GivenVehicleOffer_DoesNotExist_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new AvailableOptionsQuery {VehicleOfferId = 123};
                var handler = new AvailableOptionsQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(actual);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                var handler = new AvailableOptionsQueryHandler(context, _mapper);

                return Assert.ThrowsAsync<ArgumentNullException>(
                    "request", () => handler.Handle(null, CancellationToken.None));
            }
        }

        private async Task<IEnumerable<AvailableOptionModel>> SeedOptionOffers(VehicleOfferSeeder seeder, VehicleOffer vehicleOffer)
        {
            //Add option for vehicle offer's model but not included in the offer
            var optionWithoutOffer = await seeder.Add<Option, OptionId>(
                OptionId.Next(vehicleOffer.ModelId, seeder.Create<PrNumber>()));
            
            //Assign category to option
            var category = await seeder.Add<OptionCategory>(1);
            optionWithoutOffer.OptionCategoryId = 1;

            //Add option for some other model
            await seeder.Add<Option, OptionId>(OptionId.Parse("XXXXX"));

            //Create option offer
            var optionWithOffer = await seeder.Add<Option, OptionId>(
                OptionId.Next(vehicleOffer.ModelId, seeder.Create<PrNumber>()));
            var optionOffer = vehicleOffer.AddOptionOffer(
                optionWithOffer.PrNumber, Availability.Purchasable(1000));
            optionOffer.AssignRules("[VA: ZZZ+[]]");

            //Create unintended vehicle offer
            var anotherVehicleOffer = await seeder.Add();
            anotherVehicleOffer.AddOptionOffer(
                optionWithoutOffer.PrNumber, seeder.Create<Availability>());

            await seeder.SeedAsync();

            //Create expected query response
            return new List<AvailableOptionModel>
            {
                new AvailableOptionModel
                {
                    Option = _mapper.Map<AvailableOptionModel.OptionModel>(optionWithoutOffer),
                    Category = _mapper.Map<AvailableOptionModel.CategoryModel>(category)
                },
                new AvailableOptionModel
                {
                    Option = _mapper.Map<AvailableOptionModel.OptionModel>(optionWithOffer),
                    Offer = _mapper.Map<AvailableOptionModel.OfferModel>(optionOffer)
                }
            };
        }
    }
}
