﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Moq;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Models.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Engines;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Gearboxes;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;
using NexCore.VehicleConfigurator.Api.Application.Queries.Models;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers;
using NexCore.VehicleConfigurator.Api.Infrastructure.AutoMapper;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using FindByIdQuery = NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.FindByIdQuery;
using FindQueryHandler = NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers.FindQueryHandler;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.VehicleOffers
{
    public class FindByIdQueryTest
    {
        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var vehicleOfferSeeder = new VehicleOfferSeeder(context);
                var colorSeeder = new ColorSeeder(context);

                //Seed unexpected
                await vehicleOfferSeeder.AddRange(3);
                await colorSeeder.AddRange(3);

                var vehicleOffer = await vehicleOfferSeeder.Add();
                var color = await colorSeeder.Add();
                var colorOffer = vehicleOffer.AddColorOffer(color.Id, colorSeeder.Create<decimal>());
                vehicleOffer.SetDefaultColor(color.Id);
                vehicleOffer.Publish();

                await vehicleOfferSeeder.SeedAsync();
                await colorSeeder.SeedAsync();

                var model = await context.Models.SingleAsync(m => m.Id == vehicleOffer.ModelId);
                var body = await context.Bodies.SingleAsync(b => b.Id == vehicleOffer.BodyId);
                var equipment = await context.Equipments.SingleAsync(e => e.Id == vehicleOffer.EquipmentId);
                var engine = await context.Engines.SingleAsync(e => e.Id == vehicleOffer.EngineId);
                var gearbox = await context.Gearboxes.SingleAsync(g => g.Id == vehicleOffer.GearboxId);
                var colorType = await context.ColorTypes.SingleAsync(t => t.Id == color.TypeId);

                var colorOfferModel = new ColorOfferModel
                {
                    Id = colorOffer.Id,
                    Price = colorOffer.Price,
                    Relevance = colorOffer.Relevance,
                    RelevancePeriod = new RelevancePeriodModel
                    {
                        From = colorOffer.RelevancePeriod.From,
                        To = colorOffer.RelevancePeriod.To
                    },
                    Color = new ColorModel
                    {
                        Id = color.Id,
                        Name = color.Name,
                        Argb = color.Argb.ToArgb().ToString("X8"),
                        Type = new ColorTypeModel
                        {
                            Id = colorType.Id,
                            Name = colorType.Name
                        }
                    }
                };

                var expected = new VehicleOfferModel
                {
                    Id = vehicleOffer.Id,
                    ModelKey = vehicleOffer.ModelKey.Value,
                    CatalogId = vehicleOffer.CatalogId,
                    RelevancePeriod = new RelevancePeriodModel
                    {
                        From = vehicleOffer.RelevancePeriod.From,
                        To = vehicleOffer.RelevancePeriod.To
                    },
                    Model = new ModelModel { Id = model.Id, Name = model.Name },
                    Body = new ModelComponentModel { Id = body.Id, ModelId = body.ModelId, Name = body.Name },
                    Equipment = new ModelComponentModel { Id = equipment.Id, ModelId = equipment.ModelId, Name = equipment.Name },
                    Engine = new EngineModel { Id = engine.Id, ModelId = engine.ModelId, Name = engine.Name, FuelType = engine.FuelType },
                    Gearbox = new GearboxModel { Id = gearbox.Id, ModelId = gearbox.ModelId, Name = gearbox.Name, Type = gearbox.Type, GearBoxCategory = gearbox.GearBoxCategory },
                    Price = vehicleOffer.Price,
                    Status = vehicleOffer.Status,
                    Relevance = vehicleOffer.Relevance,
                    DefaultColor = colorOfferModel
                };

                var mapperConfig = new MapperConfiguration(
                    c => c.AddProfiles(typeof(DefaultProfile).Assembly));
                var mapper = mapperConfig.CreateMapper();

                var query = new FindByIdQuery { Id = vehicleOffer.Id };
                var handler = new FindQueryHandler(context, mapper);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<VehicleOfferModel>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new FindByIdQuery { Id = 23 };
                var handler = new FindQueryHandler(context, new Mock<IMapper>().Object);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }
    }
}
