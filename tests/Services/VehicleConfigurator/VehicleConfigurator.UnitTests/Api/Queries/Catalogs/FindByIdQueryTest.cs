﻿using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Catalogs
{
    public class FindByIdQueryTest
    {
        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new CatalogSeeder(context);

                await seeder.AddRange(3);

                var catalog = await seeder.Add();

                await seeder.SeedAsync();
            
                var query = new FindByIdQuery { Id = catalog.Id };
                var handler = new FindQueryHandler(context);
                var expected = handler.ModelMapper.Map(catalog);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<CatalogModel>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new FindByIdQuery {Id = 333};
                var handler = new FindQueryHandler(context);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }
    }
}
