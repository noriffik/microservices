﻿using AutoFixture;
using NexCore.Testing;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;
using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Catalogs
{
    public class CatalogModelMapperTest
    {
        private readonly CatalogModelMapper _mapper = new CatalogModelMapper();

        [Fact]
        public void Map()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new DomainBuilder());

            var catalog = fixture.Create<Catalog>();

            var expected = new CatalogModel
            {
                Id = catalog.Id,
                RelevancePeriod = new PeriodModel
                {
                    From = catalog.RelevancePeriod.From.Value,
                    To = catalog.RelevancePeriod.To
                },
                Status = catalog.Status,
                Year = catalog.ModelYear.Value
            };

            //Act
            var result = _mapper.Map(catalog);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(expected, result, PropertyComparer<CatalogModel>.Instance);
        }

        [Fact]
        public void Map_GivenCatalog_IsNull_ReturnsNull()
        {
            Assert.Null(_mapper.Map(null));
        }

        [Fact]
        public void PeriodMapper()
        {   
            Assert.Injection.OfProperty(_mapper, "PeriodMapper")
                .DoesOverride()
                .HasNullGuard()
                .HasDefault();
        }
    }
}
