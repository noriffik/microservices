﻿using NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Catalogs
{
    public class FindQueryHandlerTest
    {
        private readonly FindQueryHandler _handler;

        public FindQueryHandlerTest()
        {
            var context = InMemoryDbContextFactory.Instance.Create();
            _handler = new FindQueryHandler(context);
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                var unused = new FindQueryHandler(null);
            });
        }

        [Theory]
        [InlineData(typeof(FindByIdQuery))]
        [InlineData(typeof(FindActualQuery))]
        [InlineData(typeof(FindAllQuery))]
        [InlineData(typeof(FindOutdatedQuery))]
        public Task Handle_GivenRequest_IsNull_Throws(Type query)
        {
            return Assert.RequestHandler(_handler).ThrowsOnNull(query);
        }

        [Fact]
        public void ModelMapper()
        {
            Assert.Injection
                .OfProperty(_handler, nameof(_handler.ModelMapper))
                .DoesOverride()
                .HasNullGuard()
                .HasDefault();
        }
    }
}
