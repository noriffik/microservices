﻿using NexCore.Common;
using NexCore.Domain;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Catalogs
{
    public class FindActualQueryTest
    {
        [Theory]
        [InlineData(CatalogStatus.Draft)]
        [InlineData(CatalogStatus.Published)]
        public async Task Handle(CatalogStatus status)
        {
            //Arrange
            var relevancePeriod = new Period(new DateTime(2001, 01, 24), new DateTime(2001, 01, 29));

            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                var seeder = new CatalogSeeder(context);

                await seeder.AddRange(3);

                await seeder.Add(new {modelYear = Year.From(2018), period = relevancePeriod});
                var catalog = await seeder.Add(new {modelYear = Year.From(2019), period = relevancePeriod});

                catalog.Status = status;

                await seeder.SeedAsync();
                
                var query = new FindActualQuery
                {
                    ModelYear = catalog.ModelYear.Value,
                    Status = catalog.Status
                };
                var handler = new FindQueryHandler(context);
                var expected = handler.ModelMapper.Map(catalog);
                
                using (new SystemTimeContext(relevancePeriod.From.GetValueOrDefault().AddDays(1)))
                {
                    //Act
                    var actual = await handler.Handle(query, CancellationToken.None);

                    //Assert
                    Assert.Equal(expected, actual, PropertyComparer<CatalogModel>.Instance);
                }
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new FindAllQuery();
                var handler = new FindQueryHandler(context);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(result);
            }
        }
    }
}
