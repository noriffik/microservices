﻿using NexCore.Testing;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Catalogs
{
    public class FindAllQueryTest
    {
        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new EntitySeeder<Catalog>(context);

                var catalogs = (await seeder.AddRange(7))
                    .OrderBy(c => c.RelevancePeriod.From)
                    .ThenBy(c => c.ModelYear.Value)
                    .ToList();

                await seeder.SeedAsync();
                
                var query = new FindAllQuery();
                var handler = new FindQueryHandler(context);
                var expected = handler.ModelMapper.MapMany(catalogs);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<CatalogModel>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new FindAllQuery();
                var handler = new FindQueryHandler(context);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(actual);
            }
        }
    }
}
