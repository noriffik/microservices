﻿using NexCore.Domain;
using NexCore.Testing;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Catalogs
{
    public class FindOutdatedQueryTest
    {
        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var catalogs = Enumerable.Range(2006, 3)
                    .Select(y => new Year(y))
                    .Select(y => new Catalog(y, new Period(y)))
                    .ToArray();

                var seeder = new EntitySeeder<Catalog>(context);

                await seeder.AddRange(catalogs);
            
                await seeder.SeedAsync();
            
                var query = new FindAllQuery();
                var handler = new FindQueryHandler(context);
                var expected = handler.ModelMapper.MapMany(catalogs);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<CatalogModel>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new FindOutdatedQuery();
                var handler = new FindQueryHandler(context);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(result);
            }
        }
    }
}
