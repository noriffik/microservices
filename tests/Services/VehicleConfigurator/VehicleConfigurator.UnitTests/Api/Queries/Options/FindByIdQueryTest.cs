﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelComponents;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Options
{
    public class FindByIdQueryTest : FindByIdQueryTest<Option, OptionId>
    {
    }
}
