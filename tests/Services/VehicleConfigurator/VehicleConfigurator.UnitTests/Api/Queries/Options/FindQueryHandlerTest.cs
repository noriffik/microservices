﻿using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Options
{
    public class FindQueryHandlerTest
    {
        private readonly FindQueryHandler _handler;

        public FindQueryHandlerTest()
        {
            var context = InMemoryDbContextFactory.Instance.Create();
            _handler = new FindQueryHandler(context);
        }

        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(FindQueryHandler)).HasNullGuard();
        }

        [Theory]
        [InlineData(typeof(FindByIdQuery))]
        [InlineData(typeof(FindByPrNumberSetQuery))]
        [InlineData(typeof(FindAllQuery))]
        public Task Handle_GivenRequest_IsNull_Throws(Type query)
        {
            return Assert.RequestHandler(_handler).ThrowsOnNull(query);
        }
    }
}
