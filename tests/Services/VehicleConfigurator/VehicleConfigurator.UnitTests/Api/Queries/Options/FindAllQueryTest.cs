﻿using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using FindAllQuery = NexCore.VehicleConfigurator.Api.Application.Queries.Options.FindAllQuery;
using FindQueryHandler = NexCore.VehicleConfigurator.Api.Application.Queries.Options.FindQueryHandler;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Options
{
    public class FindAllQueryTest
    {
        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new EntitySeeder<Option, OptionId>(context);
                seeder.Fixture.Customizations.Add(new ModelComponentBuilder());

                var category = await seeder.Add<OptionCategory>();

                var options = (await seeder.AddRange(3)).ToList();

                options.ForEach(o => o.OptionCategoryId = category.Id);

                await seeder.SeedAsync();

                var query = new FindAllQuery();
                var handler = new FindQueryHandler(context);
                var expected = options
                    .Select(o => new OptionModel
                    {
                        Id = o.Id.Value,
                        Name = o.Name,
                        ModelId = o.ModelId.Value,
                        PrNumber = o.PrNumber.Value,
                        Category = o.OptionCategoryId.HasValue
                            ? new OptionCategoryModel
                            {
                                Id = category.Id,
                                Name = category.Name
                            }
                            : null
                    });

                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<OptionModel>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new FindAllQuery();
                var handler = new FindQueryHandler(context);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);
                
                //Assert
                Assert.Empty(result);
            }
        }
    }
}
