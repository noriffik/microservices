﻿using AutoFixture;
using Moq;
using NexCore.Domain;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Options
{
    public class OptionModelMapperTest
    {
        //Mapper
        private readonly OptionModelMapper _mapper;

        //Entities
        private readonly OptionModelRecord _record;
        private readonly OptionModel _model;

        public OptionModelMapperTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _record = new OptionModelRecord
            {
                Option = fixture.Create<Option>(),
                Category = fixture.Create<OptionCategory>()
            };
            _model = new OptionModel
            {
                Id = _record.Option.Id.Value,
                Name = _record.Option.Name,
                ModelId = _record.Option.ModelId.Value,
                PrNumber = _record.Option.PrNumber.Value,
                Category = fixture.Create<OptionCategoryModel>()
            };

            //Mapper
            _mapper = new OptionModelMapper();
            
            //Dependencies
            var categoryMapper = new Mock<IMapper<OptionCategory, OptionCategoryModel>>();

            //Setup dependencies
            _mapper.CategoryMapper = categoryMapper.Object;
            categoryMapper.Setup(m => m.Map(_record.Category))
                .Returns(_model.Category);
        }

        [Fact]
        public void Map()
        {
            //Act
            var result = _mapper.Map(_record);

            //Assert
            Assert.Equal(_model, result, PropertyComparer<OptionModel>.Instance);
        }

        [Fact]
        public void Map_GivenSource_IsNull_ReturnsNull()
        {
            //Act
            var result = _mapper.Map(null);

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public void CategoryMapper()
        {
            Assert.Injection
                .OfProperty(_mapper, nameof(OptionModelMapper.CategoryMapper))
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}
