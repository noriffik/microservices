﻿using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Options
{
    public class FindByPrNumberSetQueryTest
    {
        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new ModelComponentSeeder<Option, OptionId>(context);
                await seeder.Add(OptionId.Parse("BB003"));
                await seeder.Add(OptionId.Parse("AA001"));

                var components = await seeder.AddRange(new[] {OptionId.Parse("BB001"), OptionId.Parse("BB002")});
                var records = components.Select(c => new OptionModelRecord {Option = c}).ToList();

                await seeder.SeedAsync();

                var query = new FindByPrNumberSetQuery
                {
                    ModelId = "BB",
                    PrNumberSet = components.Select(c => c.PrNumber.Value).Aggregate((a, b) => $"{a} {b}")
                };
                var handler = new FindQueryHandler(context);
                var expected = handler.ModelMapper.MapMany(records);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<OptionModel>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new FindByPrNumberSetQuery { PrNumberSet = "XXX YYY ZZZ" };
                var handler = new FindQueryHandler(context);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);
                
                //Assert
                Assert.Empty(result);
            }
        }
    }
}
