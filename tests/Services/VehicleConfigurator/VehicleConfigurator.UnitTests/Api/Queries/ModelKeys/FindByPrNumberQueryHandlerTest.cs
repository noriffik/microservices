﻿using NexCore.Testing;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Queries.ModelKeys;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ModelKeys
{
    public class FindByPrNumberQueryHandlerTest
    {
        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new FindByOptionIdQueryHandler(null));
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            const int catalogId = 1;

            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                var seeder = new VehicleOfferSeeder(context);

                var optionOffer = seeder.Create<OptionOffer>();

                var vehicleOffer = Enumerable.Range(1, 5)
                    .Select(i => seeder.Fixture.Construct<VehicleOffer>(new {catalogId})).ToList();

                var expectedVehicleOffers = vehicleOffer.Take(2).ToList();

                foreach (var vehicle in expectedVehicleOffers)
                {
                    vehicle.AddOptionOffer(optionOffer.PrNumber, optionOffer.Availability);
                }

                await seeder.AddRange(vehicleOffer.ToArray());

                await seeder.SeedAsync();
                
                var query = new FindByOptionIdQuery
                {
                    CatalogId = catalogId,
                    OptionId = optionOffer.OptionId.Value
                };
                var handler = new FindByOptionIdQueryHandler(context);
                var expected = expectedVehicleOffers.Select(v => v.ModelKey.Value);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<string>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new FindByOptionIdQuery
                {
                    CatalogId = 1,
                    OptionId = "00000"
                };
                var handler = new FindByOptionIdQueryHandler(context);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(actual);
            }
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new FindByOptionIdQueryHandler(context);

                //Assert
                await Assert.ThrowsAsync<ArgumentNullException>("request",
                    () => handler.Handle(null, CancellationToken.None));
            }
        }
    }
}
