﻿using AutoFixture;
using AutoMapper;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Api.Application.Queries.Vehicles;
using NexCore.VehicleConfigurator.Api.Infrastructure.AutoMapper;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Collections.Generic;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Engines;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Gearboxes;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;
using NexCore.VehicleConfigurator.Api.Application.Queries.Models;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionCategories;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Vehicles
{
    public class MapperTest
    {
        [Fact]
        public void Map()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var mapperConfig = new MapperConfiguration(
                c => c.AddProfiles(typeof(DefaultProfile).Assembly));
            var mapper = mapperConfig.CreateMapper();

            var modelKey = ModelKey.Parse("AA1234");

            var model = new Model(modelKey.ModelId) {Name = "testModel"};
            var body = new Body(modelKey.BodyId) {Name = "testBody"};
            var equipment = new Equipment(modelKey.EquipmentId) {Name = "testEquipment"};
            var engine = new Engine(modelKey.EngineId) {Name = "testEngine", FuelType = FuelType.Petrol};
            var gearbox = new Gearbox(modelKey.GearboxId, GearboxType.Parse("g1")) {Name = "testGearbox", GearBoxCategory = GearBoxCategory.Automatic};
            var optionCategory = new OptionCategory("test") {Name = "testOptionCategory"};
            var option = new Option(OptionId.Next(modelKey.ModelId, PrNumber.Parse("OPT"))){Name = "testOption", OptionCategoryId = optionCategory.Id};
            var colorType = new ColorType(ColorTypeId.Parse("CCC"), "colorTest") {Name = "testColorType"};
            var color = new Color(ColorId.Parse("3245"), System.Drawing.Color.Aqua, ColorTypeId.Parse("CCC"),
                "colorTest") {Name = "testColor", TypeId = colorType.Id};

            var record = new VehicleDescriptionModelRecord
            {
                Model = model,
                Body = body,
                Equipment = equipment,
                Engine = engine,
                Gearbox = gearbox,
                Options = new List<OptionModelRecord>
                {
                    new OptionModelRecord
                    {
                        Option = option,
                        Category = optionCategory
                    }
                },
                Color = new ColorModelRecord
                {
                    Color = color,
                    Type = colorType
                }
            };

            var expected = new VehicleDescriptionModel
            {
                Model = new ModelModel{Id = model.Id, Name = model.Name},
                Body = new ModelComponentModel{Id = body.Id, ModelId = model.Id, Name = body.Name},
                Equipment = new ModelComponentModel{Id = equipment.Id, ModelId = model.Id, Name = equipment.Name},
                Engine = new EngineModel{Id = engine.Id, ModelId = engine.ModelId, Name = engine.Name, FuelType = engine.FuelType},
                Gearbox = new GearboxModel{Id = gearbox.Id, ModelId = gearbox.ModelId, Name = gearbox.Name, Type = gearbox.Type, GearBoxCategory = gearbox.GearBoxCategory},
                Color = new ColorModel{Id = color.Id, Name = color.Name, Argb = color.Argb.ToArgb().ToString("X8"), 
                    Type = new ColorTypeModel{Id = colorType.Id, Name = colorType.Name}},
                Options = new []
                {
                    new OptionModel
                    {
                        Id = option.Id,
                        ModelId = option.ModelId,
                        Name = option.Name,
                        PrNumber = option.PrNumber.Value,
                        Category = new OptionCategoryModel
                        {
                            Id = optionCategory.Id,
                            Name = optionCategory.Name
                        }
                    }
                }
            };

            //Act
            var actual = mapper.Map<VehicleDescriptionModel>(record);
            
            //Assert
            Assert.NotNull(actual);
            Assert.Equal(expected.Model, actual.Model, PropertyComparer<ModelModel>.Instance);
            Assert.Equal(expected.Body, actual.Body, PropertyComparer<ModelComponentModel>.Instance);
            Assert.Equal(expected.Equipment, actual.Equipment, PropertyComparer<ModelComponentModel>.Instance);
            Assert.Equal(expected.Engine, actual.Engine, PropertyComparer<EngineModel>.Instance);
            Assert.Equal(expected.Gearbox, actual.Gearbox, PropertyComparer<GearboxModel>.Instance);
            Assert.Equal(expected.Color, actual.Color, PropertyComparer<ColorModel>.Instance);
            Assert.Equal(expected.Options, actual.Options, PropertyComparer<IEnumerable<OptionModel>>.Instance);

            Assert.Equal(expected, actual, PropertyComparer<VehicleDescriptionModel>.Instance);
        }
    }
}
