﻿using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Vehicles
{
    public class VehicleDescriptionQueryTest
    {
        private readonly VehicleDescriptionQueryFixture _vehicleDescriptionQueryFixture;

        public VehicleDescriptionQueryTest()
        {
            _vehicleDescriptionQueryFixture = new VehicleDescriptionQueryFixture();
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new VehicleDescriptionQueryHandler(null, _vehicleDescriptionQueryFixture.Mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = InMemoryDbContextFactory.Instance.Create())
                {
                    return new VehicleDescriptionQueryHandler(context, null);
                }
            });
        }

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                await _vehicleDescriptionQueryFixture.Setup(context);

                var handler = new VehicleDescriptionQueryHandler(context, _vehicleDescriptionQueryFixture.Mapper);

                //Act
                var actual = await handler.Handle(_vehicleDescriptionQueryFixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(_vehicleDescriptionQueryFixture.GetResponse(), actual, PropertyComparer<VehicleDescriptionModel>.Instance);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new VehicleDescriptionQueryHandler(context, _vehicleDescriptionQueryFixture.Mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null, CancellationToken.None));
            }
        }

        [Fact]
        public async Task Handle_WhenVehicleComponentNotFound_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                await _vehicleDescriptionQueryFixture.Setup(context);

                var handler = new VehicleDescriptionQueryHandler(context, _vehicleDescriptionQueryFixture.Mapper);
                
                //Act
                var e = await Assert.Validation.ThrowsAsync(
                    "ModelKey", () => handler.Handle(_vehicleDescriptionQueryFixture.GetUnpreparedQuery(), CancellationToken.None));
            
                //Assert
                e.WithMessage("Components not found for given ModelKey.");
            }
        }

        [Fact]
        public async Task Handle_WhenColorNotFound_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                await _vehicleDescriptionQueryFixture.Setup(context);

                var handler = new VehicleDescriptionQueryHandler(context, _vehicleDescriptionQueryFixture.Mapper);
                
                //Act
                var e = await Assert.Validation.ThrowsAsync(
                    "ColorId", () => handler.Handle(_vehicleDescriptionQueryFixture.GetUnpreparedQueryWithoutColor(), CancellationToken.None));
            
                //Assert
                e.WithMessage("Color not found.");
            }
        }

        [Fact]
        public async Task Handle_WhenOptionNotFound_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                await _vehicleDescriptionQueryFixture.Setup(context);

                var handler = new VehicleDescriptionQueryHandler(context, _vehicleDescriptionQueryFixture.Mapper);

                //Act
                var e = await Assert.Validation.ThrowsAsync(
                    "PrNumberSet", () => handler.Handle(_vehicleDescriptionQueryFixture.GetUnpreparedQueryWithoutOption(), CancellationToken.None));
            
                //Assert
                e.WithMessage("Options not found.");
            }
        }
    }
}
