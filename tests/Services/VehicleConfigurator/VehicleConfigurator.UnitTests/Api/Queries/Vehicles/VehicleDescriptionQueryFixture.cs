﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Engines;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Gearboxes;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;
using NexCore.VehicleConfigurator.Api.Application.Queries.Models;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Api.Application.Queries.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Vehicles
{
    public class VehicleDescriptionQueryFixture: QueryFixture<VehicleDescriptionQuery, VehicleDescriptionModel>
    {
        private ModelKey _modelKey;
        private Model _model;
        private Body _body;
        private Equipment _equipment;
        private Engine _engine;
        private Gearbox _gearbox;
        private Color _color;
        private ColorType _type;
        private PrNumberSet _prNumberSet;
        private IList<Option> _options = new List<Option>();

        private async Task SetupColor(VehicleConfiguratorContext context)
        {
            var seeder = new ColorSeeder(context);

            //Seed not expected Colors
            await seeder.AddRange(5);

            //Seed expected Color
            _color = await seeder.Add();
            _type = seeder.Find<ColorType, ColorTypeId>(_color.TypeId);

            await seeder.SeedAsync();
        }

        private async Task SetupOptions(ModelId modelId, PrNumberSet prNumberSet, VehicleConfiguratorContext context)
        {
            var seeder = new ModelComponentSeeder<Option, OptionId>(context);

            //Seed not expected Options
            await seeder.AddRange(5);

            //Seed expected Options
            foreach (var prNumber in prNumberSet.Values)
            {
                _options.Add(await seeder.Add(OptionId.Next(modelId, prNumber)));
            }

            await seeder.SeedAsync();
        }

        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);

            await SetupColor(context);

            _modelKey = ModelKey.Parse("AA1234");
            _prNumberSet = PrNumberSet.Parse("AAA BBB CCC");

            var seeder = new EntitySeeder<Model, ModelId>(context);
            seeder.Fixture.Customizations.Add(new ModelComponentBuilder());

            //Seed not expected Models
            await seeder.AddRange(5);

            //Seed expected Model
            _model = await seeder.Add(_modelKey.ModelId);

            _body = await seeder.Add<Body, BodyId>(_modelKey.BodyId);
            _equipment = await seeder.Add<Equipment, EquipmentId>(_modelKey.EquipmentId);
            _engine = await seeder.Add<Engine, EngineId>(_modelKey.EngineId);
            _gearbox = await seeder.Add<Gearbox, GearboxId>(_modelKey.GearboxId);

            await seeder.SeedAsync();

            await SetupOptions(_modelKey.ModelId, _prNumberSet, context);
        }

        public override VehicleDescriptionQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new VehicleDescriptionQuery
            {
                ModelKey = _modelKey,
                ColorId = _color.Id.Value,
                PrNumberSet = _prNumberSet.AsString
            };
        }

        public override VehicleDescriptionQuery GetUnpreparedQuery()
        {
            ThrowSetupIsRequired();
            return new VehicleDescriptionQuery
            {
                ModelKey = ModelKey.Parse("AAXXXX"),
                ColorId = _color.Id.Value, 
                PrNumberSet = _prNumberSet.AsString
            };
        }

        public VehicleDescriptionQuery GetUnpreparedQueryWithoutColor()
        {
            ThrowSetupIsRequired();
            return new VehicleDescriptionQuery
            {
                ModelKey = _modelKey,
                ColorId = "XXXX", 
                PrNumberSet = _prNumberSet.AsString
            };
        }

        public VehicleDescriptionQuery GetUnpreparedQueryWithoutOption()
        {
            ThrowSetupIsRequired();
            return new VehicleDescriptionQuery
            {
                ModelKey = _modelKey,
                ColorId = _color.Id.Value, 
                PrNumberSet = _prNumberSet.Expand(PrNumber.Parse("XXX")).AsString
            };
        }

        public override VehicleDescriptionModel GetResponse()
        {
            return new VehicleDescriptionModel
            {
                Model = new ModelModel
                {
                    Id = _model.Id, 
                    Name = _model.Name
                },
                Body = new ModelComponentModel
                {
                    Id = _body.Id, 
                    ModelId = _body.ModelId, 
                    Name = _body.Name
                },
                Equipment = new ModelComponentModel
                {
                    Id = _equipment.Id, 
                    ModelId = _equipment.ModelId, 
                    Name = _equipment.Name
                },
                Engine = new EngineModel
                {
                    Id = _engine.Id, 
                    ModelId = _engine.ModelId, 
                    Name = _engine.Name, 
                    FuelType = _engine.FuelType
                },
                Gearbox = new GearboxModel
                {
                    Id = _gearbox.Id, 
                    ModelId = _gearbox.ModelId, 
                    Name = _gearbox.Name, 
                    Type = _gearbox.Type, 
                    GearBoxCategory = _gearbox.GearBoxCategory
                },
                Color = new ColorModel
                {
                    Id = _color.Id,
                    Name = _color.Name,
                    Argb = _color.Argb.ToArgb().ToString("X8"),
                    Type = new ColorTypeModel
                    {
                        Id = _type.Id.Value,
                        Name = _type.Name
                    }
                },
                Options = Mapper.Map<IEnumerable<OptionModel>>(_options)
            };
        }
    }
}
