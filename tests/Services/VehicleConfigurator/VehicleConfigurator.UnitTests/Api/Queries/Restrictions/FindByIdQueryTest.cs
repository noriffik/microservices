﻿using AutoMapper;
using Moq;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Api.Application.Queries.Restrictions;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Restrictions
{
    public class FindByIdQueryTest
    {
        private readonly Mock<IMapper> _mapper;

        public FindByIdQueryTest()
        {
            _mapper = new Mock<IMapper>();
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new FindByIdQueryHandler(null, _mapper.Object));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                Assert.Throws<ArgumentNullException>("mapper", () => new FindByIdQueryHandler(context, null));
            }
        }

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new RestrictionSeeder(context);

                await seeder.AddRange(4);
                var restriction = await seeder.Add();
                await seeder.SeedAsync();

                var query = new FindByIdQuery { Id = restriction.Id };
                var handler = new FindByIdQueryHandler(context, _mapper.Object);

                var expected = new RestrictionModel
                {
                    Id = restriction.Id,
                    CatalogId = restriction.CatalogId,
                    PrNumbers = restriction.PrNumbers.AsString,
                    ModelKey = restriction.ModelKey.Value,
                    RelevancePeriod = new PeriodModel
                    {
                        From = restriction.RelevancePeriodFrom,
                        To = restriction.RelevancePeriodTo
                    },
                    IsEnabled = restriction.IsEnabled,
                    Description = restriction.Description
                };

                _mapper.Setup(m => m.Map<RestrictionModel>(restriction))
                    .Returns(expected);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<RestrictionModel>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                var query = new FindByIdQuery { Id = 999 };
                var handler = new FindByIdQueryHandler(context, _mapper.Object);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(actual);
            }
        }
    }
}
