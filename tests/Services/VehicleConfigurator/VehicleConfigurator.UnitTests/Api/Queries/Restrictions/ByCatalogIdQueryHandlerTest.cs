﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.Restrictions;
using NexCore.VehicleConfigurator.Api.Infrastructure.AutoMapper;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.Domain.Restrictions.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Restrictions
{
    public class ByCatalogIdQueryHandlerTest
    {
        private readonly Fixture _fixture;
        private readonly Mock<IEntityRepository> _repository;
        private readonly FindByCatalogIdQuery _query;
        private readonly FindByCatalogIdQueryHandler _handler;
        private readonly Mock<IMapper> _mapperMock;

        public ByCatalogIdQueryHandlerTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            _repository = new Mock<IEntityRepository>();

            var work = new Mock<IUnitOfWork>();
            _mapperMock = new Mock<IMapper>();

            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            _query = _fixture.Create<FindByCatalogIdQuery>();
            _handler = new FindByCatalogIdQueryHandler(work.Object, _mapperMock.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(FindByCatalogIdQueryHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                var restrictions = _fixture.CreateMany<Restriction>(3)
                    .ToList();

                var mapperConfig = new MapperConfiguration(
                    c => c.AddProfiles(typeof(DefaultProfile).Assembly));
                var mapper = mapperConfig.CreateMapper();

                var expected = mapper.Map<IEnumerable<RestrictionModel>>(restrictions);

                _repository.Setup(r =>
                        r.Find(It.Is<RestrictionByCatalogIdSpecification>(s => s.CatalogId == _query.CatalogId), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(restrictions);

                _mapperMock.Setup(m => m.Map<IEnumerable<RestrictionModel>>(restrictions))
                    .InSequence()
                    .Returns<IEnumerable<Restriction>>(source => mapper.Map<IEnumerable<RestrictionModel>>(source));

                //Act
                var result = await _handler.Handle(_query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, result, PropertyComparer<IEnumerable<RestrictionModel>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
