﻿using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Rules.Expressions;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.OptionOffers
{
    public class RuleSetExtendedModelMapperTest
    {
        private readonly RuleSetExtendedModelMapper _mapper = new RuleSetExtendedModelMapper();
        private readonly IRuleMessageComposer _composer = new RuleMessageComposer();

        [Fact]
        public void Map()
        {
            //Arrange 
            var ruleSet = "[ZA: AAA BBB+[[VO: ZZZ+[]][ZO: CCC DDD+[]]]][VA: XXX+[]]";
            var expected = new RuleSetExtendedModel
            {
                RuleSet = new List<RuleExtendedModel>
                {
                    new RuleExtendedModel
                    {
                        Rule = new RuleModel
                        {
                            RuleName = "ZA",
                            PrNumbers = "AAA BBB",
                            NestedRuleSet = new RuleSetModel
                            {
                                RuleSet =  new List<RuleModel>
                                {
                                    new RuleModel
                                    {
                                        RuleName = "VO",
                                        PrNumbers = "ZZZ",
                                        NestedRuleSet = null
                                    },
                                    new RuleModel
                                    {
                                        RuleName = "ZO",
                                        PrNumbers = "CCC DDD",
                                        NestedRuleSet = null
                                    }
                                }
                            }
                        },
                        Details = new RuleDetailsModel
                        {
                            RuleString = "[ZA: AAA BBB+[[VO: ZZZ+[]][ZO: CCC DDD+[]]]]",
                            Message = _composer.Compose("ZA", "AAA BBB")
                        }
                    },
                    new RuleExtendedModel
                    {
                        Rule = new RuleModel
                        {
                            RuleName = "VA",
                            PrNumbers = "XXX",
                            NestedRuleSet = null
                        },
                        Details = new RuleDetailsModel
                        {
                            RuleString = "[VA: XXX+[]]",
                            Message = _composer.Compose("VA", "XXX")
                        }
                    }
                }
            };

            //Act
            var result = _mapper.Map(ruleSet);

            //Assert
            Assert.Equal(expected, result, PropertyComparer<RuleSetExtendedModel>.Instance);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Map_WhenGivenSource_IsNull_ReturnNull(string source)
        {
            //Act
            var result = _mapper.Map(source);

            //Assert
            Assert.Null(result);
        }
    }
}
