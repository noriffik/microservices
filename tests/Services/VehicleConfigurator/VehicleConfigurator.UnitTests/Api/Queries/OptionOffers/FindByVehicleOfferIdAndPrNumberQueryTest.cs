﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using FindQueryHandler = NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers.FindQueryHandler;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.OptionOffers
{
    public class FindByVehicleOfferIdAndPrNumberQueryTest
    {
        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new VehicleOfferSeeder(context);
                var vehicleOffer = await seeder.Add();
                var option = await seeder.Add<Option, OptionId>(
                    OptionId.Next(vehicleOffer.ModelId, seeder.Create<PrNumber>()));

                var optionOffer = vehicleOffer.AddOptionOffer(
                    option.PrNumber, seeder.Create<Availability>());
                optionOffer.AssignRules("[VA: AXC+[]]");

                await seeder.SeedAsync();

                var record = new OptionOfferModelRecord
                {
                    OptionOffer = optionOffer,
                    Option = new OptionModelRecord
                    {
                        Option = option
                    },
                };
                var query = new FindByVehicleOfferIdAndPrNumberQuery
                {
                    VehicleOfferId = vehicleOffer.Id,
                    PrNumber = option.PrNumber.Value
                };
                var handler = new FindQueryHandler(context);
                var expected = handler.ModelMapper.Map(record);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<OptionOfferModel>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = VehicleOfferSeeder.CreateFixture();

                var query = fixture.Build<FindByVehicleOfferIdAndPrNumberQuery>()
                    .With(c => c.PrNumber, fixture.Create<PrNumber>().Value)
                    .Create();
                var handler = new FindQueryHandler(context);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }
    }
}
