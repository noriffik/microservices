﻿using AutoFixture;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.UnitTests.Extensions.Domain;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using NexCore.Common;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.OptionOffers
{
    public class RestrictionExtensionsTest : IDisposable
    {
        private readonly Fixture _fixture;
        private readonly ModelKey _modelKey;
        private readonly PrNumber _prNumber;
        private readonly PrNumberSet _prNumbers;
        private readonly SystemTimeContext _time;

        public RestrictionExtensionsTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            _modelKey = _fixture.Create<ModelKey>();
            _prNumber = _fixture.Create<PrNumber>();
            _prNumbers = _fixture.Create<PrNumberSet>().Expand(_prNumber);

            _time = new SystemTimeContext(DateTime.Now);
        }

        [Fact]
        public void ApplicableTo_WhenOptions_AreNot_UnderRestriction_ReturnsNull()
        {
            //Arrange
            var restrictions = Enumerable.Range(1, 3)
                .Select(i => _fixture.Create<Restriction>());

            //Act
            var result = restrictions.ApplicableTo(_fixture.Create<PrNumber>());

            //Assert
            Assert.Empty(result);
        }

        [Fact]
        public void ApplicableTo_GivenPrNumber_IsNull_Throws()
        {
            //Arrange
            var restrictions = new Restriction[] { };

            Assert.Throws<ArgumentNullException>("prNumber", () => restrictions.ApplicableTo(null));
        }

        [Fact]
        public void CompositePeriod_WhenPeriodsAreOverlap_ReturnsMost_Period()
        {
            //Arrange
            var a = new Period(new DateTime(1, 1, 1), new DateTime(1, 1, 6));
            var b = new Period(new DateTime(1, 1, 5), new DateTime(1, 1, 7));
            var c = new Period(new DateTime(1, 1, 8), new DateTime(1, 1, 10));

            var periods = new List<Period>(new[] { a, b, c });

            var expected = new Period(new DateTime(1, 1, 1), new DateTime(1, 1, 10));

            //Act
            var actual = periods.CompositePeriod();

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<Period>.Instance);
        }

        [Fact]
        public void CompositePeriod_WhenThere_IsPeriod_WhichTo_IsNull_ReturnsPeriod_WithTo_IsNull()
        {
            //Arrange
            var date = new DateTime(2019, 1, 1);
            var expected = new Period(date.AddDays(-5), null);

            var relevancePeriods = Enumerable.Range(1, 3)
                .Select(i => date.Range(i))
                .Concat(new[] {expected});

            //Act
            var actual = relevancePeriods.CompositePeriod();

            //Assert
            Assert.NotNull(actual);
            Assert.Equal(expected, actual, PropertyComparer<Period>.Instance);
        }

        [Fact]
        public void ApplicableTo_ReturnsRestrictions_ThatHave_MatchingModelKey()
        {
            //Arrange
            var relevancePeriod = _time.Now.Range(1);

            var expected = _fixture.Construct<Restriction>(new { modelKey = _modelKey, prNumbers = _prNumbers, relevancePeriod });
            var unexpected = _fixture.Construct<Restriction>(new { prNumbers = _prNumbers, relevancePeriod });

            var restrictions = new List<Restriction>(new[] { expected, unexpected });

            //Enable restrictions
            restrictions.ForEach(r => r.Enable());

            //Act
            var actual = restrictions.ApplicableTo(_modelKey, _prNumber).ToList();

            //Assert
            Assert.NotNull(actual);
            Assert.Equal(1, actual.Count);
            Assert.Contains(expected, actual);
        }

        [Fact]
        public void ApplicableTo_ReturnsRestrictions_ThatHave_MatchingPrNumber()
        {
            //Arrange
            var expected = _fixture.Construct<Restriction>(new { modelKey = _modelKey, prNumbers = _prNumbers, relevancePeriod = _time.Now.Range(1) });
            var unexpected = _fixture.Construct<Restriction>(new { modelKey = _modelKey, relevancePeriod = _time.Now.Range(2) });

            var restrictions = new List<Restriction>(new[] { expected, unexpected });

            //Enable restrictions
            restrictions.ForEach(r => r.Enable());

            //Act
            var actual = restrictions.ApplicableTo(_modelKey, _prNumber).ToList();

            //Assert
            Assert.NotNull(actual);
            Assert.Equal(1, actual.Count);
            Assert.Contains(expected, actual);
        }

        [Fact]
        public void ApplicableTo_ReturnsRestrictions_ThatAre_InsideCurrentPeriod()
        {
            //Arrange
            var expected = _fixture.Construct<Restriction>(new
            {
                modelKey = _modelKey, prNumbers = _prNumbers, relevancePeriod = _time.Now.Range(1)
            });
            var unexpected = _fixture.Construct<Restriction>(new
            {
                modelKey = _modelKey, prNumbers = _prNumbers, relevancePeriod = _time.Now.Past(2)
            });

            var restrictions = new List<Restriction>(new[] { expected, unexpected });

            //Enable restrictions
            restrictions.ForEach(r => r.Enable());

            //Act
            var actual = restrictions.ApplicableTo(_modelKey, _prNumber).ToList();

            //Assert
            Assert.NotNull(actual);
            Assert.Equal(1, actual.Count);
            Assert.Contains(expected, actual);
        }

        [Fact]
        public void ApplicableTo_ReturnsRestrictions_ThatAre_Enabled()
        {
            //Arrange
            var restrictions = Enumerable.Range(1, 2)
                .Select(i => _time.Now.Range(i))
                .Select(p => _fixture.Construct<Restriction>(new { modelKey = _modelKey, prNumbers = _prNumbers, relevancePeriod = p }))
                .ToList();

            var expected = restrictions.First();

            //Enable expected restriction
            expected.Enable();

            //Act
            var actual = restrictions.ApplicableTo(_modelKey, _prNumber).ToList();

            //Assert
            Assert.NotNull(actual);
            Assert.Equal(1, actual.Count);
            Assert.Contains(expected, actual);
        }

        public void Dispose()
        {
            _time?.Dispose();
        }
    }
}
