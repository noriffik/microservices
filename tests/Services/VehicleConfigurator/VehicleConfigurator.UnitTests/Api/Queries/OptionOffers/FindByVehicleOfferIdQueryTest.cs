﻿using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using FindQueryHandler = NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers.FindQueryHandler;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.OptionOffers
{
    public class FindByVehicleOfferIdQueryTest
    {
        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new VehicleOfferSeeder(context);
                var vehicleOffer = await seeder.Add();
                var options = Enumerable.Range(0, 6)
                    .Select(n => vehicleOffer.ModelId)
                    .Select(m => OptionId.Next(m, seeder.Create<PrNumber>()))
                    .Select(i => seeder.Create<Option, OptionId>(i))
                    .ToList();

                var records = new List<OptionOfferModelRecord>();
                foreach (var option in options)
                {
                    await seeder.Add<Option, OptionId>(option);

                    var optionOffer = vehicleOffer.AddOptionOffer(
                        option.Id.PrNumber, seeder.Create<Availability>());
                    optionOffer.AssignRules("[ZA: AAA BBB+[[VO: ZZZ+[]]]][ZO: CCC DDD+[]]");

                    records.Add(new OptionOfferModelRecord
                    {
                        Option = new OptionModelRecord
                        {
                            Option = option
                        },
                        OptionOffer = optionOffer
                    });
                }

                await seeder.SeedAsync();

                var query = new FindByVehicleOfferIdQuery {VehicleOfferId = vehicleOffer.Id};
                var handler = new FindQueryHandler(context);
                var expected = handler.ModelMapper.MapMany(records);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<OptionOfferModel>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new FindByVehicleOfferIdQuery {VehicleOfferId = 123};
                var handler = new FindQueryHandler(context);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(result);
            }
        }
    }
}
