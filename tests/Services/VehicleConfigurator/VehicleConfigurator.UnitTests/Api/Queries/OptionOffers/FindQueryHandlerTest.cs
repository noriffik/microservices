﻿using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.OptionOffers
{
    public class FindQueryHandlerTest
    {
        private readonly FindQueryHandler _handler;

        public FindQueryHandlerTest()
        {
            var context = InMemoryDbContextFactory.Instance.Create();
            _handler = new FindQueryHandler(context);
        }

        [Fact]
        public void Ctor_WhenGivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new FindQueryHandler(null as VehicleConfiguratorContext));
        }

        [Theory]
        [InlineData(typeof(FindByVehicleOfferIdQuery))]
        [InlineData(typeof(FindByVehicleOfferIdAndPrNumberQuery))]
        public Task Handle_WhenGivenRequest_IsNull_Throws(Type query)
        {
            return Assert.RequestHandler(_handler).ThrowsOnNull(query);
        }

        [Fact]
        public void ModelMapper()
        {
            Assert.Injection.OfProperty(_handler, nameof(_handler.ModelMapper))
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}
