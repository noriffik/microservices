﻿using AutoFixture;
using AutoMapper;
using Moq;
using NexCore.Domain;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Models.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers.ByComponents;
using NexCore.VehicleConfigurator.Api.Application.Queries.Restrictions;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Linq;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.OptionOffers.ByComponents
{
    public class OptionOfferModelMapperTest
    {
        private readonly Mock<IMapper> _autoMapper;

        private readonly OptionOfferByComponentModelMapper _mapper;


        public OptionOfferModelMapperTest()
        {
            _autoMapper = new Mock<IMapper>();

            _mapper = new OptionOfferByComponentModelMapper(_autoMapper.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(OptionOfferByComponentModelMapper)).HasNullGuard();
        }

        [Fact]
        public void Map_WhenGivenSource_IsNull_ReturnsNull()
        {
            //Act
            var result = _mapper.Map(null);

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public void Map()
        {
            var fixture = new Fixture();
            fixture.Behaviors
                .OfType<ThrowingRecursionBehavior>()
                .ToList()
                .ForEach(b => fixture.Behaviors.Remove(b));
            fixture.Behaviors.Add(new OmitOnRecursionBehavior(1));

            fixture.Customizations.Add(new ModelComponentBuilder());

            var source = fixture.Create<OptionOfferByComponentRecord>();

            var expected = fixture.Build<OptionOfferByComponentModel>()
                .With(m => m.Id, source.OptionOffer.Id)
                .With(m => m.OptionId, source.OptionOffer.OptionId.Value)
                .With(m => m.Inclusion, source.OptionOffer.Inclusion)
                .With(m => m.Relevance, source.OptionOffer.Relevance)
                .With(m => m.RelevancePeriod, new RelevancePeriodModel
                {
                    From = source.OptionOffer.RelevancePeriodFrom,
                    To = source.OptionOffer.RelevancePeriodTo
                })
                .With(m => m.IsRestricted, source.OptionOffer.IsRestricted)
                .With(m => m.RestrictionPeriod, new RestrictionPeriodModel
                {
                    From = source.RestrictionPeriod?.From,
                    To = source.RestrictionPeriod?.To
                })
                .Create();

            _mapper.OptionMapper = MappingFor(source.Option, expected.Option);
            _mapper.AvailabilityMapper = MappingFor(source.OptionOffer.Availability, expected.Availability);
            _autoMapper.Setup(m => m.Map<VehicleOfferModel>(source.VehicleOfferRecord)).Returns(expected.VehicleOffer);
            _mapper.RuleSetExtendedModelMapper = MappingFor(source.OptionOffer.RuleSet, expected.RuleSet);

            //Act
            var result = _mapper.Map(source);

            //Assert
            Assert.Equal(expected, result, PropertyComparer<OptionOfferByComponentModel>.Instance);
        }

        private static IMapper<TEntity, TModel> MappingFor<TEntity, TModel>(TEntity from, TModel to)
            where TEntity : class
            where TModel : class
        {
            var modelMapper = new Mock<IMapper<TEntity, TModel>>();

            modelMapper.Setup(m => m.Map(from)).Returns(to);

            return modelMapper.Object;
        }

        [Theory]
        [InlineData(nameof(OptionOfferByComponentModelMapper.OptionMapper))]
        [InlineData(nameof(OptionOfferByComponentModelMapper.AvailabilityMapper))]
        public void PropertyInjection(string mapperName)
        {
            //Arrange
            var mapper = new OptionOfferByComponentModelMapper(_autoMapper.Object);

            //Assert
            Assert.Injection.OfProperty(mapper, mapperName)
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}
