﻿using AutoMapper;
using Moq;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers.ByComponents;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.OptionOffers.ByComponents
{
    public class FindByModelIdQueryHandlerTest
    {
        private readonly VehicleConfiguratorContext _context;
        private readonly FindByModelIdQueryHandler _handler;

        public FindByModelIdQueryHandlerTest()
        {
            _context = InMemoryDbContextFactory.Instance.Create();
            _handler = new FindByModelIdQueryHandler(_context, new Mock<IMapper>().Object);
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new FindByModelIdQueryHandler(null, new Mock<IMapper>().Object));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () => new FindByModelIdQueryHandler(_context, null));
        }


        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.RequestHandler(_handler).ThrowsOnNull(typeof(FindByModelIdQuery));
        }

        [Fact]
        public void ModelMapper()
        {
            Assert.Injection.OfProperty(_handler, nameof(_handler.ModelMapper))
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}
