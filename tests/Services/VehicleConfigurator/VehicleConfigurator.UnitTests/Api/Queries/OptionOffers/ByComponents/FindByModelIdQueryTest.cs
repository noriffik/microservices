﻿using AutoFixture;
using AutoMapper;
using Moq;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers.ByComponents;
using NexCore.VehicleConfigurator.Api.Application.Queries.Options;
using NexCore.VehicleConfigurator.Api.Application.Queries.VehicleOffers;
using NexCore.VehicleConfigurator.Api.Infrastructure.AutoMapper;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure.Views;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using FindByModelIdQuery = NexCore.VehicleConfigurator.Api.Application.Queries.OptionOffers.ByComponents.FindByModelIdQuery;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.OptionOffers.ByComponents
{
    public class FindByModelIdQueryTest
    {
        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new VehicleOfferSeeder(context);

                var randomCatalogId = seeder.Create<int>();
                var modelId = seeder.Create<ModelId>();

                var vehicleOffers = new List<VehicleOffer>(6);
                for (var i = 0; i < 6; i++)
                {
                    var vehicleOffer = await seeder.Add(new
                    {
                        modelKey = ModelKey.Parse(modelId.Value + "QWE" + i),
                        catalogId = randomCatalogId
                    });

                    vehicleOffers.Add(vehicleOffer);
                }

                var unexpectedVehicleOffer = await seeder.Add(new
                {
                    modelKey = ModelKey.Parse(seeder.Create<ModelId>().Value + "0000"),
                    catalogId = randomCatalogId
                });

                vehicleOffers.Add(unexpectedVehicleOffer);

                var option = await seeder.Add<Option, OptionId>(
                    OptionId.Next(modelId, seeder.Create<PrNumber>()));

                var vehicleOfferRecords = vehicleOffers.ToDictionary(vehicleOffer => vehicleOffer.Id, vehicleOffer => new VehicleOfferModelRecord
                {
                    VehicleView = new OfferedVehicleView
                    {
                        Id = vehicleOffer.Id,
                        ModelKey = vehicleOffer.ModelKey,
                        Price = vehicleOffer.Price,
                        CatalogId = vehicleOffer.CatalogId,
                        Status = vehicleOffer.Status,
                        Model = seeder.Find<Model, ModelId>(vehicleOffer.ModelId),
                        Body = seeder.Find<Body, BodyId>(vehicleOffer.BodyId),
                        Equipment = seeder.Find<Equipment, EquipmentId>(vehicleOffer.EquipmentId),
                        Engine = seeder.Find<Engine, EngineId>(vehicleOffer.EngineId),
                        Gearbox = seeder.Find<Gearbox, GearboxId>(vehicleOffer.GearboxId),
                        Relevance = vehicleOffer.Relevance,
                        RelevancePeriodTo = vehicleOffer.RelevancePeriod.To,
                        RelevancePeriodFrom = vehicleOffer.RelevancePeriod.From,
                        DefaultColorId = null
                    },
                    DefaultColor = null
                });

                var records = new List<OptionOfferByComponentRecord>();
                foreach (var vehicleOffer in vehicleOffers)
                {
                    var optionOffer = vehicleOffer.AddOptionOffer(option.PrNumber, seeder.Create<Availability>());

                    records.Add(new OptionOfferByComponentRecord
                    {
                        Option = new OptionModelRecord
                        {
                            Option = option
                        },
                        OptionOffer = optionOffer,
                        VehicleOfferRecord = vehicleOfferRecords[vehicleOffer.Id]
                    });
                }

                await seeder.SeedAsync();

                var query = new FindByModelIdQuery
                {
                    PrNumber = option.PrNumber.Value,
                    CatalogId = randomCatalogId,
                    ModelId = modelId.Value
                };

                var mapperConfig = new MapperConfiguration(
                    c => c.AddProfiles(typeof(DefaultProfile).Assembly));
                var mapper = mapperConfig.CreateMapper();

                var handler = new FindByModelIdQueryHandler(context, mapper);
                var expected = handler.ModelMapper.MapMany(records.Take(6));

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<OptionOfferByComponentModel>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var fixture = VehicleOfferSeeder.CreateFixture();

                var query = fixture.Build<FindByModelIdQuery>()
                    .With(c => c.PrNumber, fixture.Create<PrNumber>().Value)
                    .With(c => c.ModelId, fixture.Create<ModelId>().Value)
                    .Create();
                var handler = new FindByModelIdQueryHandler(context, new Mock<IMapper>().Object);

                //Act
                var result = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(result);
            }
        }
    }
}
