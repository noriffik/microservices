﻿using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ColorOffers
{
    public class FindQueryHandlerTest
    {
        private readonly FindByVehicleOfferIdQueryFixture _byVehicleOfferIdFixture;
        private readonly FindByVehicleOfferIdAndColorIdQueryFixture _byVehicleOfferIdAndColorIdFixture;

        public FindQueryHandlerTest()
        {
            _byVehicleOfferIdFixture = new FindByVehicleOfferIdQueryFixture();
            _byVehicleOfferIdAndColorIdFixture = new FindByVehicleOfferIdAndColorIdQueryFixture();
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () =>
                new FindQueryHandler(null, _byVehicleOfferIdFixture.Mapper));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () =>
            {
                using (var context = InMemoryDbContextFactory.Instance.Create())
                {
                    return new FindQueryHandler(context, null);
                }
            });
        }

        //FindByVehicleOfferIdQuery

        [Fact]
        public async Task HandleByVehicleOfferId()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                await _byVehicleOfferIdFixture.Setup(context);

                var handler = new FindQueryHandler(context, _byVehicleOfferIdFixture.Mapper);

                //Act
                var actual = await handler.Handle(_byVehicleOfferIdFixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(_byVehicleOfferIdFixture.GetResponse(), actual, PropertyComparer<ColorOfferModel>.Instance);
            }
        }

        [Fact]
        public Task HandleByVehicleOfferId_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new FindQueryHandler(context, _byVehicleOfferIdFixture.Mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as FindByVehicleOfferIdQuery, CancellationToken.None));
            }
        }

        [Fact]
        public async Task HandleByVehicleOfferId_WhenNothing_IsFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new FindQueryHandler(context, _byVehicleOfferIdFixture.Mapper);

                //Act
                var result = await handler.Handle(_byVehicleOfferIdFixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Empty(result);
            }
        }

        //FindByVehicleOfferIdAndColorIdQuery

        [Fact]
        public async Task HandleByVehicleOfferIdAndColorId()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                await _byVehicleOfferIdAndColorIdFixture.Setup(context);

                var handler = new FindQueryHandler(context, _byVehicleOfferIdAndColorIdFixture.Mapper);

                //Act
                var actual = await handler.Handle(_byVehicleOfferIdAndColorIdFixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(_byVehicleOfferIdAndColorIdFixture.GetResponse(), actual, PropertyComparer<ColorOfferModel>.Instance);
            }
        }

        [Fact]
        public Task HandleByVehicleOfferIdAndColorId_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new FindQueryHandler(context, _byVehicleOfferIdAndColorIdFixture.Mapper);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                    handler.Handle(null as FindByVehicleOfferIdAndColorIdQuery, CancellationToken.None));
            }
        }

        [Fact]
        public async Task HandleByVehicleOfferIdAndColorId_WhenNothing_IsFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new FindQueryHandler(context, _byVehicleOfferIdAndColorIdFixture.Mapper);

                //Act
                var result = await handler.Handle(_byVehicleOfferIdAndColorIdFixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }
    }
}
