﻿using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Api.Application.Models.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ColorOffers
{
    public class FindByVehicleOfferIdAndColorIdQueryFixture :
        QueryFixture<FindByVehicleOfferIdAndColorIdQuery, ColorOfferModel>
    {
        private VehicleOffer _vehicleOffer;
        private Color _color;
        private ColorType _colorType;

        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);

            var colorSeeder = new ColorSeeder(context);
            var vehicleOfferSeeder = new VehicleOfferSeeder(context);

            var vehicleOffers = await vehicleOfferSeeder.AddRange(5);
            var colors = (await colorSeeder.AddRange(5)).ToList();

            foreach (var color in colors)
                foreach (var vehicleOffer in vehicleOffers)
                    vehicleOffer.AddColorOffer(color.Id, vehicleOfferSeeder.Create<decimal>());

            await colorSeeder.SeedAsync();

            _vehicleOffer = vehicleOffers.ElementAt(2);
            _color = colors.ElementAt(2);
            _colorType = await context.ColorTypes.SingleAsync(t => t.Id == _color.TypeId);
        }

        public override FindByVehicleOfferIdAndColorIdQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new FindByVehicleOfferIdAndColorIdQuery
            {
                VehicleOfferId = _vehicleOffer.Id,
                ColorId = _color.Id.Value
            };
        }

        public override FindByVehicleOfferIdAndColorIdQuery GetUnpreparedQuery()
        {
            return new FindByVehicleOfferIdAndColorIdQuery
            {
                VehicleOfferId = 100,
                ColorId = "QWER"
            };
        }

        public override ColorOfferModel GetResponse()
        {
            var colorOffer = _vehicleOffer.FindColorOfferOrThrow(_color.Id);

            return new ColorOfferModel
            {
                Id = colorOffer.Id,
                Price = colorOffer.Price,
                Relevance = colorOffer.Relevance,
                RelevancePeriod = new RelevancePeriodModel
                {
                    From = colorOffer.RelevancePeriod.From,
                    To = colorOffer.RelevancePeriod.To
                },
                Color = new ColorModel
                {
                    Id = _color.Id.Value,
                    Name = _color.Name,
                    Argb = _color.Argb.ToArgb().ToString("X8"),
                    Type = new ColorTypeModel
                    {
                        Id = _colorType.Id.Value,
                        Name = _colorType.Name
                    }
                }
            };
        }
    }
}
