﻿using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Api.Application.Models.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorOffers;
using NexCore.VehicleConfigurator.Api.Application.Queries.Colors;
using NexCore.VehicleConfigurator.Api.Application.Queries.ColorTypes;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.ColorOffers
{
    public class FindByVehicleOfferIdQueryFixture : QueryFixture<FindByVehicleOfferIdQuery, IEnumerable<ColorOfferModel>>
    {
        private VehicleOffer _vehicleOffer;
        private List<Color> _colors;
        private List<ColorType> _colorTypes;

        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);

            var colorSeeder = new ColorSeeder(context);
            var vehicleOfferSeeder = new VehicleOfferSeeder(context);

            var vehicleOffers = await vehicleOfferSeeder.AddRange(5);
            _colors = (await colorSeeder.AddRange(5)).ToList();

            foreach (var color in _colors)
                foreach (var vehicleOffer in vehicleOffers)
                    vehicleOffer.AddColorOffer(color.Id, vehicleOfferSeeder.Create<decimal>());

            await colorSeeder.SeedAsync();

            _vehicleOffer = vehicleOffers.ElementAt(2);
            _colorTypes = await context.ColorTypes.ToListAsync();
        }

        public override FindByVehicleOfferIdQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new FindByVehicleOfferIdQuery
            {
                VehicleOfferId = _vehicleOffer.Id
            };
        }

        public override FindByVehicleOfferIdQuery GetUnpreparedQuery()
        {
            return new FindByVehicleOfferIdQuery
            {
                VehicleOfferId = 100
            };
        }

        public override IEnumerable<ColorOfferModel> GetResponse()
        {
            foreach (var colorOffer in _vehicleOffer.Colors)
            {
                var color = _colors.Single(c => c.Id == colorOffer.ColorId);
                var colorType = _colorTypes.Single(t => t.Id == color.TypeId);

                yield return new ColorOfferModel
                {
                    Id = colorOffer.Id,
                    Price = colorOffer.Price,
                    Relevance = colorOffer.Relevance,
                    RelevancePeriod = new RelevancePeriodModel
                    {
                        From = colorOffer.RelevancePeriod.From,
                        To = colorOffer.RelevancePeriod.To
                    },
                    Color = new ColorModel
                    {
                        Id = color.Id.Value,
                        Name = color.Name,
                        Argb = color.Argb.ToArgb().ToString("X8"),
                        Type = new ColorTypeModel
                        {
                            Id = colorType.Id.Value,
                            Name = colorType.Name
                        }
                    }
                };
            }
        }
    }
}
