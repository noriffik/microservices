﻿using System.Threading.Tasks;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Api.Application.Queries.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Contracts.VehicleSales
{
    public class ByIdQueryFixture : QueryFixture<ByIdQuery, VehicleSaleContractModel>
    {
        //Entities
        private VehicleSaleContract _contract;

        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);

            var seeder = new OfferSeeder(context);

            //Seed not expected contracts
            await seeder.AddRange<VehicleSaleContract>(3);

            //Seed expected contract
            _contract = await seeder.Add<VehicleSaleContract>();

            await seeder.SeedAsync();
        }

        public override ByIdQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new ByIdQuery
            {
                Id = _contract.Id
            };
        }

        public override ByIdQuery GetUnpreparedQuery()
        {
            return new ByIdQuery { Id = 43990 };
        }

        public override VehicleSaleContractModel GetResponse()
        {
            ThrowSetupIsRequired();

            return Mapper.Map<VehicleSaleContractModel>(_contract);
        }
    }
}
