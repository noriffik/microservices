﻿using AutoMapper;
using Moq;
using NexCore.Application.Queries;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Api.Infrastructure.AutoMapper;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using FindAllQuery = NexCore.VehicleConfigurator.Api.Application.Queries.Contracts.VehicleSales.FindAllQuery;
using FindQueryHandler = NexCore.VehicleConfigurator.Api.Application.Queries.Contracts.VehicleSales.FindQueryHandler;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Contracts.VehicleSales
{
    public class FindQueryHandlerTest
    {
        private readonly FindQueryHandler _handler;
        private readonly VehicleConfiguratorContext _context;
        private readonly IMapper _mapper;

        public FindQueryHandlerTest()
        {
            _context = InMemoryDbContextFactory.Instance.Create();
            _handler = new FindQueryHandler(_context, new Mock<IMapper>().Object);
            _mapper = new MapperConfiguration(c => c.AddProfiles(typeof(DefaultProfile).Assembly)).CreateMapper();
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new FindQueryHandler(null, new Mock<IMapper>().Object));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper", () => new FindQueryHandler(_context, null));
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.RequestHandler(_handler).ThrowsOnNull(typeof(FindAllQuery));
        }

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new OfferSeeder(context);
                var contracts = await seeder.AddRange<VehicleSaleContract>(3);
               
                await seeder.SeedAsync();

                var contractsModels = _mapper.Map<VehicleSaleContractModel[]>(contracts);

                var handler = new FindQueryHandler(context, _mapper);

                var expected = new PagedResponse<VehicleSaleContractModel>(PageOptions.Default, contractsModels.Length, contractsModels);

                //Act
                var actual = await handler.Handle(new FindAllQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<VehicleSaleContractModel>>.Instance);
            }
        }


        [Fact]
        public async Task Handle_WhenNothing_IsFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new FindQueryHandler(context, _mapper);

                //Act
                var actual = await handler.Handle(new FindAllQuery(), CancellationToken.None);

                //Assert
                Assert.Empty(actual.Items);
            }
        }

        [Fact]
        public async Task Handle_With_DealerId()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new OfferSeeder(context);

                var contracts = await seeder.AddRange<VehicleSaleContract>(3);

                await seeder.SeedAsync();

                var requiredContractModels = new []{_mapper.Map<VehicleSaleContractModel>(contracts[0])};

                var handler = new FindQueryHandler(context, _mapper);

                var expected = new PagedResponse<VehicleSaleContractModel>(PageOptions.Default, requiredContractModels.Length, requiredContractModels);

                //Act
                var actual = await handler.Handle(new FindAllQuery {DealerId = contracts[0].DealerId.Value }, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<VehicleSaleContractModel>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_With_PrivateCustomerId()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new OfferSeeder(context);

                var contracts = await seeder.AddRange<VehicleSaleContract>(3);

                await seeder.SeedAsync();

                var requiredContractModels = new[] { _mapper.Map<VehicleSaleContractModel>(contracts[0]) };

                var handler = new FindQueryHandler(context, _mapper);

                var expected = new PagedResponse<VehicleSaleContractModel>(PageOptions.Default, requiredContractModels.Length, requiredContractModels);

                //Act
                var actual = await handler.Handle(new FindAllQuery { PrivateCustomerId = contracts[0].PrivateCustomerId }, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<PagedResponse<VehicleSaleContractModel>>.Instance);
            }
        }
    }
}
