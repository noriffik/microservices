﻿using NexCore.VehicleConfigurator.Api.Application.Queries.Models;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Models
{
    public class FindQueryHandlerTest
    {
        private readonly FindQueryHandler _handler;

        public FindQueryHandlerTest()
        {
            var context = InMemoryDbContextFactory.Instance.Create();
            _handler = new FindQueryHandler(context);
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Injection.OfConstructor(typeof(FindQueryHandler)).HasNullGuard();
        }

        [Theory]
        [InlineData(typeof(FindByIdQuery))]
        [InlineData(typeof(FindAllQuery))]
        public Task Handle_GivenRequest_IsNull_Throws(Type query)
        {
            return Assert.RequestHandler(_handler).ThrowsOnNull(query);
        }

        [Fact]
        public void ModelMapper()
        {
            Assert.Injection
                .OfProperty(_handler, nameof(_handler.ModelMapper))
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}
