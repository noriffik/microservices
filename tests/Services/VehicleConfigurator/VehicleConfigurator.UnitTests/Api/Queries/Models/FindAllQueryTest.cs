﻿using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Api.Application.Queries.Models;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using FindAllQuery = NexCore.VehicleConfigurator.Api.Application.Queries.Models.FindAllQuery;
using FindQueryHandler = NexCore.VehicleConfigurator.Api.Application.Queries.Models.FindQueryHandler;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Models
{
    public class FindAllQueryTest
    {
        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new EntitySeeder<Model, ModelId>(context);
                seeder.Fixture.Customizations.Add(new ModelComponentBuilder());

                var models = await seeder.AddRange(3);

                await seeder.SeedAsync();

                var query = new FindAllQuery();
                var handler = new FindQueryHandler(context);
                var expected = handler.ModelMapper.MapMany(models);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<IEnumerable<ModelModel>>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsEmpty()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new FindAllQuery();
                var handler = new FindQueryHandler(context);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Empty(actual);
            }
        }
    }
}
