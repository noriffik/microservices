﻿using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Api.Application.Queries.Models;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Models
{
    public class FindByIdQueryTest
    {
        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var seeder = new EntitySeeder<Model, ModelId>(context);
                seeder.Fixture.Customizations.Add(new ModelComponentBuilder());

                await seeder.AddRange(3);

                var component = await seeder.Add();

                await seeder.SeedAsync();
                
                var query = new FindByIdQuery {Id = component.Id.Value};
                var handler = new FindQueryHandler(context);
                var expected = handler.ModelMapper.Map(component);
                
                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(expected, actual, PropertyComparer<ModelModel>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var query = new FindByIdQuery {Id = "DX"};
                var handler = new FindQueryHandler(context);

                //Act
                var actual = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Null(actual);
            }
        }
    }
}
