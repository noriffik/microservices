﻿using AutoFixture;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Queries.Models;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Queries.Models
{
    public class ModelModelMapperTest
    {
        private readonly IMapper<Model, ModelModel> _mapper = new ModelModelMapper();

        [Fact]
        public void Map()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());
            var model = fixture.Create<Model>();

            //Act
            var result = _mapper.Map(model);

            //Assert
            Assert.Equal(result.Id, model.Id.Value);
            Assert.Equal(result.Name, model.Name);
        }

        [Fact]
        public void Map_GivenSource_IsNull_ReturnsNull()
        {
            //Act
            var result = _mapper.Map(null);

            //Assert
            Assert.Null(result);
        }
    }
}
