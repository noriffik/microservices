﻿using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using System.Collections.Generic;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Models.OptionOffers
{
    public class RuleSetModelMapperTest
    {
        private readonly RuleSetModelMapper _mapper = new RuleSetModelMapper();

        private readonly string _ruleString =  "[ZA: AAA BBB+[[VO: ZZZ+[]][ZO: CCC DDD+[]]]][VA: XXX+[]]";
        private readonly RuleSetModel _model = new RuleSetModel
        {
            RuleSet = new List<RuleModel>
            {
                new RuleModel
                {
                    RuleName = "ZA",
                    PrNumbers = "AAA BBB",
                    NestedRuleSet = new RuleSetModel
                    {
                        RuleSet =  new List<RuleModel>
                        {
                            new RuleModel
                            {
                                RuleName = "VO",
                                PrNumbers = "ZZZ",
                                NestedRuleSet = null
                            },
                            new RuleModel
                            {
                                RuleName = "ZO",
                                PrNumbers = "CCC DDD",
                                NestedRuleSet = null
                            }
                        }
                    }
                },
                new RuleModel
                {
                    RuleName = "VA",
                    PrNumbers = "XXX",
                    NestedRuleSet = null
                }
            }
        };

        [Fact]
        public void Map()
        {
            //Act
            var result = _mapper.Map(_ruleString);

            //Assert
            Assert.Equal(_model, result, PropertyComparer<RuleSetModel>.Instance);
        }

        [Fact]
        public void MapReverse()
        {
            //Act
            var result = _mapper.MapReverse(_model);

            //Assert
            Assert.Equal(_ruleString, result);
        }

        [Fact]
        public void Map_WhenGivenSource_IsNull_ReturnNull()
        {
            //Act
            var result = _mapper.Map("");

            //Assert
            Assert.Null(result);
        }
    }
}
