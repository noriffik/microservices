﻿using AutoFixture;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Models.OptionOffers
{
    public class AvailabilityModelMapperTest
    {
        private readonly AvailabilityModelMapper _mapper = new AvailabilityModelMapper();

        [Fact]
        public void Map()
        {
            //Arrange
            var source = new Fixture().Create<Availability>();

            //Act
            var result = _mapper.Map(source);

            //Assert
            Assert.Equal(source.Type, result.Type);
            Assert.Equal(source.Reason, result.Reason);
            Assert.Equal(source.Price, result.Price);
        }

        [Fact]
        public void Map_WhenGivenSource_IsNull_ReturnNull()
        {
            //Act
            var result = _mapper.Map(null);

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public void MapReverse_WithReason()
        {
            var source = new AvailabilityModel
            {
                Type = AvailabilityType.Included,
                Reason = ReasonType.SerialEquipment,
                Price = 1M
            };

            //Act
            var result = _mapper.MapReverse(source);

            //Assert
            Assert.Equal(source.Type, result.Type);
            Assert.Equal(source.Reason, result.Reason);
            Assert.Equal(0, result.Price);
        }

        [Fact]
        public void MapReverse_WithPrice()
        {
            //Arrange
            var source = new AvailabilityModel
            {
                Type = AvailabilityType.Purchasable,
                Reason = ReasonType.SerialEquipment,
                Price = 1M
            };

            //Act
            var result = _mapper.MapReverse(source);

            //Assert
            Assert.Equal(source.Type, result.Type);
            Assert.Null(result.Reason);
            Assert.Equal(source.Price, result.Price);
        }

        [Fact]
        public void MapReverse_WhenGivenSource_IsNull_Throws()
        {
            //Assert
            Assert.Throws<ArgumentNullException>("destination", () => _mapper.MapReverse(null as AvailabilityModel));
        }
    }
}
