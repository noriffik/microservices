﻿using AutoFixture;
using AutoMapper;
using NexCore.VehicleConfigurator.Api.Application.Queries.Offers;
using NexCore.VehicleConfigurator.Api.Infrastructure.AutoMapper;
using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Models.Offers
{
    public class OfferModelMappingTest
    {
        [Fact]
        public void Map()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Setup AutoMapper
            var mapperConfig = new MapperConfiguration(
                c => c.AddProfiles(typeof(DefaultProfile).Assembly));

            var mapper = mapperConfig.CreateMapper();

            //Entities and models
            var offer = fixture.Create<Offer>();

            //Act
            var result = mapper.Map<OfferModel>(offer);

            //Assert
            Assert.Equal(offer.Id, result.Id);
            Assert.Equal(offer.ModelYear.Value, result.ModelYear);
            Assert.Equal(offer.PackageId.Value, result.PackageId);
            Assert.Equal(offer.PrivateCustomerId, result.PrivateCustomerId);
            Assert.Equal(offer.Options, result.Options);
            Assert.NotNull(result.Price);
            Assert.Equal(offer.CreatedAt, result.CreatedAt);
        }
    }
}
