﻿using AutoFixture;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents;
using NexCore.VehicleConfigurator.UnitTests.Fixtures;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Models.ModelComponents
{
    public class ModelComponentModelMapperTest
    {
        private readonly IMapper<TestModelComponent, ModelComponentModel> _mapper
            = new ModelComponentModelMapper<TestModelComponent, TestModelComponentId>();

        [Fact]
        public void Map()
        {
            //Arrange
            var component = new Fixture()
                .CustomizeConstructor<TestModelComponent>(new { id = new TestModelComponentId("AAA")})
                .Create<TestModelComponent>();

            //Act
            var result = _mapper.Map(component);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(result.Id, component.Id.Value);
            Assert.Equal(result.Name, component.Name);
            Assert.Equal(result.ModelId, component.ModelId.Value);
        }

        [Fact]
        public void Map_GivenSource_IsNull_ReturnsNull()
        {
            //Act
            var result = _mapper.Map(null);

            //Assert
            Assert.Null(result);
        }
    }
}
