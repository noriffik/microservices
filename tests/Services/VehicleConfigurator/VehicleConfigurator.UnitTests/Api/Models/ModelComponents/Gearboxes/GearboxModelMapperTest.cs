﻿using AutoFixture;
using NexCore.VehicleConfigurator.Api.Application.Models.ModelComponents.Gearboxes;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Models.ModelComponents.Gearboxes
{
    public class GearboxModelMapperTest
    {
        private readonly GearboxModelMapper _mapper = new GearboxModelMapper();

        [Fact]
        public void Map()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());
            var gearbox = fixture.Create<Gearbox>();

            //Act
            var result = _mapper.Map(gearbox);

            //Assert
            var model = Assert.IsType<GearboxModel>(result);
            Assert.Equal(model.Id, gearbox.Id.Value);
            Assert.Equal(model.Name, gearbox.Name);
            Assert.Equal(model.ModelId, gearbox.ModelId.Value);
            Assert.Equal(model.Type, gearbox.Type.Value);
        }

        [Fact]
        public void Map_GivenSource_IsNull_ReturnsNull()
        {
            //Act
            var result = _mapper.Map(null);

            //Assert
            Assert.Null(result);
        }
    }
}
