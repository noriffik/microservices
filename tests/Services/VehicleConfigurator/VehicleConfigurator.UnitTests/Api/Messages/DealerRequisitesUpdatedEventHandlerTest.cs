﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Api.Application.Messages;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Dealers;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Messages
{
    public class DealerRequisitesUpdatedEventHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly Dealer _dealer;
        private readonly OrganizationRequisites _requisites;

        //Event and handler
        private readonly DealerRequisitesUpdatedEvent _event;
        private readonly DealerRequisitesUpdatedEventHandler _handler;

        public DealerRequisitesUpdatedEventHandlerTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            var mapper = new Mock<IMapper>();

            //Entities
            _dealer = fixture.Create<Dealer>();
            _requisites = fixture.Create<OrganizationRequisites>();

            //Event and handler
            _handler = new DealerRequisitesUpdatedEventHandler(_work.Object, mapper.Object);
            _event = fixture.Build<DealerRequisitesUpdatedEvent>()
                .With(e => e.DealerId, _dealer.Id.Value)
                .Create();

            //Setup mapper
            mapper.Setup(m => m.Map<OrganizationRequisites>(_event.Requisites))
                .Returns(_requisites);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DealerRequisitesUpdatedEventHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenEvent_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("event", () => _handler.Handle(null));
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            using (Sequence.Create())
            {
                _repository.Setup(r => r.SingleOrDefault<Dealer, DealerId>(_dealer.Id.Value))
                    .InSequence()
                    .ReturnsAsync(_dealer);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_event);
            }

            //Assert
            Assert.Equal(_requisites, _dealer.Requisites);
        }

        [Fact]
        public async Task Handle_WhenDealer_IsNotFound_DoesNothing()
        {
            //Arrange
            _repository.Setup(r => r.SingleOrDefault<Dealer, DealerId>(_dealer.Id.Value))
                .ReturnsAsync(null as Dealer)
                .Verifiable();

            //Act
            await _handler.Handle(_event);

            //Assert
            _repository.Verify();
            _work.Verify(w => w.Commit(It.IsAny<CancellationToken>()), Times.Never);
        }
    }
}
