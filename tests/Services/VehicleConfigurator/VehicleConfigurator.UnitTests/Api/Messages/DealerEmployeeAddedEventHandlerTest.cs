﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Messages;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Dealers;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Messages
{
    public class DealerEmployeeAddedEventHandlerTest
    {
        //Dependencies
        private readonly Mock<IMapper> _mapper;
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Employee _employee;

        //Event and handler
        private readonly DealerEmployeeAddedEvent _event;
        private readonly DealerEmployeeAddedEventHandler _handler;

        public DealerEmployeeAddedEventHandlerTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            //Dependencies
            _mapper = new Mock<IMapper>();
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entity
            _employee = fixture.Create<Employee>();

            //Event and handler
            _event = fixture.Create<DealerEmployeeAddedEvent>();
            _handler = new DealerEmployeeAddedEventHandler(_work.Object, _mapper.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DealerEmployeeAddedEventHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenEvent_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("event", () => _handler.Handle(null));
        }

        [Fact]
        public async Task Handle_WhenDealer_DoesNotExists_DoNothing()
        {
            using (Sequence.Create())
            {
                //Arrange
                _mapper.Setup(m => m.Map<Employee>(_event))
                    .InSequence()
                    .Returns(_employee);
                _repository.Setup(r => r.Has<Dealer, DealerId>(_employee.DealerId))
                    .InSequence()
                    .ReturnsAsync(false);

                //Act
                await _handler.Handle(_event);
            }
        }

        [Fact]
        public async Task Handle_WhenEmployee_AlreadyExists_DoNothing()
        {
            using (Sequence.Create())
            {
                //Arrange
                _mapper.Setup(m => m.Map<Employee>(_event))
                    .InSequence()
                    .Returns(_employee);
                _repository.Setup(r => r.Has<Dealer, DealerId>(_employee.DealerId))
                    .InSequence()
                    .ReturnsAsync(true);
                _repository.Setup(r => r.Has<Employee>(_employee.Id))
                    .InSequence()
                    .ReturnsAsync(true);

                //Act
                await _handler.Handle(_event);
            }
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _mapper.Setup(m => m.Map<Employee>(_event))
                    .InSequence()
                    .Returns(_employee);
                _repository.Setup(r => r.Has<Dealer, DealerId>(_employee.DealerId))
                    .InSequence()
                    .ReturnsAsync(true);
                _repository.Setup(r => r.Has<Employee>(_employee.Id))
                    .InSequence()
                    .ReturnsAsync(false);
                _repository.Setup(r => r.Add(_employee))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_event);
            }
        }
    }
}
