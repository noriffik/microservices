﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.Testing;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Messages;
using NexCore.VehicleConfigurator.Domain.Customers;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.Domain.Dealers.Specifications;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Messages
{
    public class DealerPrivateCustomerAddedEventHandlerTest
    {
        private readonly Fixture _fixture;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;
        private readonly Mock<IMapper> _mapper;

        //Entity
        private readonly PrivateCustomer _privateCustomer;

        //Event and handler
        private readonly DealerPrivateCustomerAddedEvent _message;
        private readonly DealerPrivateCustomerAddedEventHandler _handler;

        public DealerPrivateCustomerAddedEventHandlerTest()
        {
            _fixture = new Fixture();

            //Dependencies
            _mapper = new Mock<IMapper>();
            _repository = new Mock<IEntityRepository>();
            _work = new Mock<IUnitOfWork>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entity
            _privateCustomer = _fixture.Create<PrivateCustomer>();

            //Event and handler
            _message = _fixture.Create<DealerPrivateCustomerAddedEvent>();
            _handler = new DealerPrivateCustomerAddedEventHandler(_work.Object, _mapper.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DealerPrivateCustomerAddedEventHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle_WhenCreate()
        {
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Has<Dealer, DealerId>(It.Is<DealerByCompanyIdSpecification>(s => s.CompanyId == _message.OrganizationId), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(true);

                _repository.Setup(r =>
                        r.SingleOrDefault<PrivateCustomer>(_message.DealerPrivateCustomerId, CancellationToken.None))
                    .InSequence()
                    .ReturnsAsync(null as PrivateCustomer);

                _mapper.Setup(m => m.Map<PrivateCustomer>(_message))
                    .InSequence()
                    .Returns(_privateCustomer);

                _repository.Setup(r => r.Add(_privateCustomer))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_message);
            }
        }

        [Fact]
        public async Task Handle_WhenUpdate()
        {
            using (Sequence.Create())
            {
                //Arrange
                var privateCustomer = _fixture.Construct<PrivateCustomer>(new { privateCustomerId = _privateCustomer.Id });

                _repository.Setup(r => r.Has<Dealer, DealerId>(It.Is<DealerByCompanyIdSpecification>(s => s.CompanyId == _message.OrganizationId), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(true);

                _repository.Setup(r =>
                        r.SingleOrDefault<PrivateCustomer>(_message.DealerPrivateCustomerId, CancellationToken.None))
                    .InSequence()
                    .ReturnsAsync(privateCustomer);

                _mapper.Setup(m => m.Map<PrivateCustomer>(_message))
                    .InSequence()
                    .Returns(_privateCustomer);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_message);

                //Assert
                Assert.Equal(_privateCustomer, privateCustomer, PropertyComparer<PrivateCustomer>.Instance);
            }
        }

        [Fact]
        public async Task Handle_WhenDealer_IsNotExist_DoNothing()
        {
            //Act
            await _handler.Handle(_message);

            //Assert
            _work.Verify(w => w.Commit(It.IsAny<CancellationToken>()), Times.Never);
        }

        [Fact]
        public Task Handle_GivenEvent_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("event", () => _handler.Handle(null));
        }
    }
}
