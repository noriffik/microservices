﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Api.Application.Messages;
using NexCore.VehicleConfigurator.Domain.Customers;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Messages
{
    public class PrivateCustomerInfoUpdatedEventHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly PrivateCustomer _privateCustomer;
        private readonly PersonName _name;
        private readonly Passport _passport;
        private readonly TaxIdentificationNumber _taxNumber;

        //Event and handler
        private readonly DealerPrivateCustomerInfoUpdatedEvent _event;
        private readonly PrivateCustomerInfoUpdatedEventHandler _handler;

        public PrivateCustomerInfoUpdatedEventHandlerTest()
        {
            var fixture = new Fixture();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            var mapper = new Mock<IMapper>();

            //Entities
            _privateCustomer = fixture.Create<PrivateCustomer>();
            _name = fixture.Create<PersonName>();
            _passport = fixture.Create<Passport>();
            _taxNumber = fixture.Create<TaxIdentificationNumber>();

            //Event and handler
            _handler = new PrivateCustomerInfoUpdatedEventHandler(_work.Object, mapper.Object);
            _event = fixture.Create<DealerPrivateCustomerInfoUpdatedEvent>();

            //Setup mapper
            mapper.Setup(m => m.Map<PersonName>(_event.PersonName)).Returns(_name);
            mapper.Setup(m => m.Map<Passport>(_event.Passport)).Returns(_passport);
            mapper.Setup(m => m.Map<TaxIdentificationNumber>(_event.TaxNumber)).Returns(_taxNumber);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(PrivateCustomerInfoUpdatedEventHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenEvent_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("event", () => _handler.Handle(null));
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            using (Sequence.Create())
            {
                _repository.Setup(r => r.SingleOrDefault<PrivateCustomer>(_event.DealerPrivateCustomerId))
                    .InSequence()
                    .ReturnsAsync(_privateCustomer);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_event);
            }

            //Assert
            Assert.Equal(_name, _privateCustomer.PersonName);
            Assert.Equal(_passport, _privateCustomer.Passport);
            Assert.Equal(_taxNumber, _privateCustomer.TaxIdentificationNumber);
        }

        [Fact]
        public async Task Handle_WhenDealer_IsNotFound_DoesNothing()
        {
            //Arrange
            _repository.Setup(r => r.SingleOrDefault<PrivateCustomer>(_event.DealerPrivateCustomerId))
                .ReturnsAsync(null as PrivateCustomer)
                .Verifiable();

            //Act
            await _handler.Handle(_event);

            //Assert
            _repository.Verify();
            _work.Verify(w => w.Commit(It.IsAny<CancellationToken>()), Times.Never);
        }
    }
}
