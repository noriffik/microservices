﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.Testing;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Messages;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Dealers;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Messages
{
    public class DealerAddedEventHandlerTest
    {
        private readonly IFixture _fixture;
        //Dependencies
        private readonly Mock<IMapper> _mapper;
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entity
        private readonly Dealer _dealer;

        //Event and handler
        private readonly DealerAddedEvent _event;
        private readonly DealerAddedEventHandler _handler;

        public DealerAddedEventHandlerTest()
        {
            _fixture = new Fixture().Customize(new DealerIdCustomization());

            //Dependencies
            _mapper = new Mock<IMapper>();
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entity
            _dealer = _fixture.Create<Dealer>();

            //Event and handler
            _event = _fixture.Create<DealerAddedEvent>();
            _handler = new DealerAddedEventHandler(_work.Object, _mapper.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DealerAddedEventHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle_WhenDealerId_IsInUse()
        {
            var existDealer = _fixture.Construct<Dealer, DealerId>(_dealer.Id);

            using (Sequence.Create())
            {
                //Arrange
                _mapper.Setup(m => m.Map<Dealer>(_event))
                    .InSequence()
                    .Returns(_dealer);
                _repository.Setup(r => r.SingleOrDefault<Dealer, DealerId>(_dealer.Id))
                    .InSequence()
                    .ReturnsAsync(existDealer);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_event);
            }

            Assert.Equal(_dealer, existDealer, PropertyComparer<Dealer>.Instance);
        }

        [Fact]
        public async Task Handle_WhenDealerId_IsNotInUse()
        {
            using (Sequence.Create())
            {
                //Arrange
                _mapper.Setup(m => m.Map<Dealer>(_event))
                    .InSequence()
                    .Returns(_dealer);
                _repository.Setup(r => r.SingleOrDefault<Dealer, DealerId>(_dealer.Id))
                    .InSequence()
                    .ReturnsAsync(null as Dealer);
                _repository.Setup(r => r.Add<Dealer, DealerId>(_dealer))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_event);
            }
        }

        [Fact]
        public Task Handle_GivenEvent_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("event", () => _handler.Handle(null));
        }
    }
}
