﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Api.Application.Messages;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Dealers;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Messages
{
    public class DealerEmployeeNameUpdatedEventHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly Employee _employee;
        private readonly PersonName _personName;

        //Event and handler
        private readonly DealerEmployeeNameUpdatedEvent _event;
        private readonly DealerEmployeeNameUpdatedEventHandler _handler;

        public DealerEmployeeNameUpdatedEventHandlerTest()
        {
            var fixture = new Fixture().Customize(new DealerIdCustomization());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);
            var mapper = new Mock<IMapper>();

            //Entities
            _employee = fixture.Create<Employee>();
            _personName = fixture.Create<PersonName>();

            //Event and handler
            _event = fixture.Build<DealerEmployeeNameUpdatedEvent>()
                .With(e => e.DealerEmployeeId, _employee.Id)
                .Create();
            _handler = new DealerEmployeeNameUpdatedEventHandler(_work.Object, mapper.Object);

            //Setup mapper
            mapper.Setup(m => m.Map<PersonName>(_event.PersonName))
                .Returns(_personName);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(DealerEmployeeNameUpdatedEventHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenEvent_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("event", () => _handler.Handle(null));
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            using (Sequence.Create())
            {
                _repository.Setup(r => r.SingleOrDefault<Employee>(_employee.Id))
                    .InSequence()
                    .ReturnsAsync(_employee);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_event);
            }

            //Assert
            Assert.Equal(_personName, _employee.PersonName);
        }

        [Fact]
        public async Task Handle_WhenDealer_IsNotFound_DoesNothing()
        {
            //Arrange
            _repository.Setup(r => r.SingleOrDefault<Employee>(_employee.Id))
                .ReturnsAsync(null as Employee)
                .Verifiable();

            //Act
            await _handler.Handle(_event);

            //Assert
            _repository.Verify();
            _work.Verify(w => w.Commit(It.IsAny<CancellationToken>()), Times.Never);
        }
    }
}
