﻿using NexCore.VehicleConfigurator.Api.Localization;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Localization
{
    public class UahCurrencyWriterTest
    {
        [Theory]
        [InlineData(0, "нуль гривень, 00 копійок")]
        [InlineData(1.01123, "одна гривня, 01 копійка")]
        [InlineData(2.02, "дві гривні, 02 копійки")]
        [InlineData(3.03, "три гривні, 03 копійки")]
        [InlineData(4.04, "чотири гривні, 04 копійки")]
        [InlineData(5.05, "п'ять гривень, 05 копійок")]
        [InlineData(6.06234, "шість гривень, 06 копійок")]
        [InlineData(7.07234, "сім гривень, 07 копійок")]
        [InlineData(8.08, "вісім гривень, 08 копійок")]
        [InlineData(9.09, "дев'ять гривень, 09 копійок")]
        [InlineData(10.1, "десять гривень, 10 копійок")]
        [InlineData(11.11123, "одинадцять гривень, 11 копійок")]
        [InlineData(12.1299, "дванадцять гривень, 12 копійок")]
        [InlineData(13.13, "тринадцять гривень, 13 копійок")]
        [InlineData(14.14, "чотирнадцять гривень, 14 копійок")]
        [InlineData(15.15, "п'ятнадцять гривень, 15 копійок")]
        [InlineData(16.16, "шістнадцять гривень, 16 копійок")]
        [InlineData(17.17, "сімнадцять гривень, 17 копійок")]
        [InlineData(18.18, "вісімнадцять гривень, 18 копійок")]
        [InlineData(19.19, "дев'ятнадцять гривень, 19 копійок")]
        [InlineData(20.20, "двадцять гривень, 20 копійок")]
        [InlineData(22.22, "двадцять дві гривні, 22 копійки")]
        [InlineData(31.31, "тридцять одна гривня, 31 копійка")]
        [InlineData(19031.31, "дев'ятнадцять тисяч тридцять одна гривня, 31 копійка")]
        [InlineData(662.31, "шістсот шістдесят дві гривні, 31 копійка")]
        [InlineData(190310.00, "cто дев'яносто тисяч триста десять гривень, 00 копійок")]
        public void Write(decimal amount, string expected)
        {
            //Arrange
            var writer = new UahCurrencyWriter();

            //Act
            var actual = writer.Write(amount);

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
