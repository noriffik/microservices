﻿using Moq;
using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.VehicleSales.Mappers;
using NexCore.VehicleConfigurator.Api.Localization;
using NexCore.VehicleConfigurator.Domain.Pricing;
using System;
using System.Globalization;
using System.Linq;
using TemplateEngine.Docx;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Exports.Contracts.VehicleSales.Mappers
{
    public class PaymentDocxContentMapperTest
    {
        private const decimal FirstPaymentRetail = 20;
        private const string FirstPaymentRetailString = "200";
        private const int FirstPaymentBase = 10;
        private const int SecondPaymentBase = 30;

        private static readonly CultureInfo Culture = CultureInfo.CurrentCulture;

        private readonly Price[] _payments = {
            new Price(FirstPaymentBase, FirstPaymentRetail),
            new Price(SecondPaymentBase, 40)
        };

        private readonly Mock<ICurrencyWriter> _currencyWriter;
        private readonly PaymentDocxContentMapper _mapper;

        public PaymentDocxContentMapperTest()
        {
            _currencyWriter = new Mock<ICurrencyWriter>();
            _mapper = new PaymentDocxContentMapper(_currencyWriter.Object);
        }

        [Fact]
        public void Ctor_GivenCurrencyWriter_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("currencyWriter", () => new PaymentDocxContentMapper(null));
        }

        [Fact]
        public void Map()
        {
            //Arrange
            _currencyWriter.Setup(w => w.Write(FirstPaymentRetail))
                .Returns(FirstPaymentRetailString);

            var expected = new[]
            {
                new FieldContent("FirstPaymentRetail", FirstPaymentRetail.ToString(Culture)),
                new FieldContent("FirstPaymentRetailString", FirstPaymentRetailString),
                new FieldContent("FirstPaymentBase", FirstPaymentBase.ToString(Culture)),
                new FieldContent("SecondPaymentBase", SecondPaymentBase.ToString(Culture))
            };

            //Act
            var actual = _mapper.Map(_payments);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Map_GivenPrefix_IsSet()
        {
            //Arrange
            var fieldNames = new[]
            {
                "Prefix_FirstPaymentRetail",
                "Prefix_FirstPaymentRetailString",
                "Prefix_FirstPaymentBase",
                "Prefix_SecondPaymentBase"
            };

            //Act
            var fields = _mapper.Map(_payments, "Prefix_");

            //Assert
            Assert.NotNull(fields);
            Assert.Equal(fieldNames, fields.Select(f => f.Name));
        }

        [Fact]
        public void Map_GivenPayments_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("payments", () => _mapper.Map(null));
        }

        [Fact]
        public void Map_GivenPrefix_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prefix", () => _mapper.Map(_payments, null));
        }

        [Fact]
        public void Map_GivenPayments_HasLessThan_TwoItems_Throws()
        {
            //Arrange
            var payments = new[] {new Price(1, 1)};

            //Act
            var e = Assert.Throws<ArgumentException>("payments", () => _mapper.Map(payments));

            //Assert
            Assert.Contains("2", e.Message);
        }
    }
}
