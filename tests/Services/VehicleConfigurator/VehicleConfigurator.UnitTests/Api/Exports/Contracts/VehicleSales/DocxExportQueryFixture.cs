﻿using Moq;
using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts;
using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Domain.Contracts;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Api.Application.Queries;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.IO;
using System.Threading.Tasks;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Exports.Contracts.VehicleSales
{
    public class DocxExportQueryFixture : QueryFixture<DocxExportQuery, Stream>
    {
        //Entities
        private VehicleSaleContract _contract, _contractWithoutTemplate;

        public readonly Mock<IContractDocxWriter<VehicleSaleContent>> ContractWriter;
        public readonly Stream Result;

        public DocxExportQueryFixture()
        {
            ContractWriter = new Mock<IContractDocxWriter<VehicleSaleContent>>();
            Result = new MemoryStream();
        }

        public override async Task Setup(VehicleConfiguratorContext context)
        {
            await base.Setup(context);

            var seeder = new OfferSeeder(context);

            //Seed not expected contracts
            await seeder.AddRange<VehicleSaleContract>(3);

            //Seed expected contract
            _contract = await seeder.Add<VehicleSaleContract>();
            _contractWithoutTemplate = await seeder.Add<VehicleSaleContract>();

            //Seed contract template
            var template = await seeder.Add<ContractTemplate>(new {dealerId = _contract.DealerId.Value, typeId = _contract.TypeId});

            await seeder.SeedAsync();

            //Setup contract writer
            ContractWriter.Setup(w => w.Write(template.Content, _contract.Content))
                .ReturnsAsync(Result);
        }

        public override DocxExportQuery GetQuery()
        {
            ThrowSetupIsRequired();

            return new DocxExportQuery
            {
                Id = _contract.Id
            };
        }

        public DocxExportQuery GetQueryWithoutTemplate()
        {
            ThrowSetupIsRequired();

            return new DocxExportQuery
            {
                Id = _contractWithoutTemplate.Id
            };
        }

        public override DocxExportQuery GetUnpreparedQuery()
        {
            return new DocxExportQuery { Id = 43990 };
        }

        public override Stream GetResponse()
        {
            ThrowSetupIsRequired();

            return Result;
        }
    }
}
