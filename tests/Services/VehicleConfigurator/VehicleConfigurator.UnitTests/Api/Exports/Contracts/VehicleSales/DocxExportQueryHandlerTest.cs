﻿using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts;
using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Exports.Contracts.VehicleSales
{
    public class DocxExportQueryHandlerTest
    {
        private readonly DocxExportQueryFixture _fixture;

        public DocxExportQueryHandlerTest()
        {
            _fixture = new DocxExportQueryFixture();
        }

        [Fact]
        public void Ctor_GivenContext_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("context", () => new DocxExportQueryHandler(null, _fixture.ContractWriter.Object));
        }

        [Fact]
        public void Ctor_GivenContractWriter_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("contractWriter", () =>
            {
                using (var context = InMemoryDbContextFactory.Instance.Create())
                {
                    return new DocxExportQueryHandler(context, null);
                }
            });
        }

        [Fact]
        public async Task Handle()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                await _fixture.Setup(context);

                var handler = new DocxExportQueryHandler(context, _fixture.ContractWriter.Object);

                //Act
                var actual = await handler.Handle(_fixture.GetQuery(), CancellationToken.None);

                //Assert
                Assert.Equal(_fixture.GetResponse(), actual);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new DocxExportQueryHandler(context, _fixture.ContractWriter.Object);

                //Assert
                return Assert.ThrowsAsync<ArgumentNullException>("request", () => handler.Handle(null, CancellationToken.None));
            }
        }

        [Fact]
        public async Task Handle_WhenContract_IsNotFound_ReturnsNull()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                var handler = new DocxExportQueryHandler(context, _fixture.ContractWriter.Object);

                //Act
                var result = await handler.Handle(_fixture.GetUnpreparedQuery(), CancellationToken.None);

                //Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public async Task Handle_WhenTemplate_IsNotFound_Throws()
        {
            using (var context = InMemoryDbContextFactory.Instance.Create())
            {
                //Arrange
                await _fixture.Setup(context);

                var handler = new DocxExportQueryHandler(context, _fixture.ContractWriter.Object);

                //Act
                await Assert.ThrowsAsync<ContractTemplateNotFoundException>(() =>
                    handler.Handle(_fixture.GetQueryWithoutTemplate(), CancellationToken.None));
            }
        }
    }
}
