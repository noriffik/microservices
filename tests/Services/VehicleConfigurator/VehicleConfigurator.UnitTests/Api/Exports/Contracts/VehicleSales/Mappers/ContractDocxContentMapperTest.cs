﻿using AutoFixture;
using Moq;
using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.Mappers;
using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.VehicleSales.Mappers;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Contracts;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Dealers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TemplateEngine.Docx;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Exports.Contracts.VehicleSales.Mappers
{
    public class ContractDocxContentMapperTest
    {
        private readonly VehicleSaleContent _contract;
        private readonly List<FieldContent> _paymentFields;
        private readonly List<FieldContent> _priceFields;
        private readonly List<FieldContent> _vehicleFields;
        private readonly List<FieldContent> _dealerFields;
        private readonly List<FieldContent> _customerFields;
        private readonly Mock<PaymentDocxContentMapper> _paymentMapper;
        private readonly Mock<TotalPriceDocxContentMapper> _priceMapper;
        private readonly Mock<VehicleDocxContentMapper> _vehicleMapper;
        private readonly Mock<DealerDocxContentMapper> _dealerMapper;
        private readonly Mock<PrivateCustomerDocxContentMapper> _customerMapper;

        private readonly ContractDocxContentMapper _mapper;

        public ContractDocxContentMapperTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());
            fixture.Customize(new DealerIdCustomization());
            fixture.Customize(new ContractTypeIdCustomization());

            //Setup contract's content
            _contract = fixture.Create<VehicleSaleContent>();

            //Setup inner mapper's fields
            _paymentFields = fixture.CreateMany<FieldContent>().ToList();
            _priceFields = fixture.CreateMany<FieldContent>().ToList();
            _vehicleFields = fixture.CreateMany<FieldContent>().ToList();
            _dealerFields = fixture.CreateMany<FieldContent>().ToList();
            _customerFields = fixture.CreateMany<FieldContent>().ToList();

            //Setup payment mapper
            _paymentMapper = new Mock<PaymentDocxContentMapper>();
            _paymentMapper.Setup(m => m.Map(_contract.Payment, string.Empty))
                .Returns(_paymentFields);

            //Setup price mapper
            _priceMapper = new Mock<TotalPriceDocxContentMapper>();
            _priceMapper.Setup(m => m.Map(_contract.Price, 1, string.Empty))
                .Returns(_priceFields);

            //Setup vehicle specification mapper
            _vehicleMapper = new Mock<VehicleDocxContentMapper>();
            _vehicleMapper.Setup(m => m.Map(_contract.Vehicle, _contract.VehicleModelName, _contract.VehicleColorName,
                string.Empty)).ReturnsAsync(_vehicleFields);

            //Setup dealer mapper
            _dealerMapper = new Mock<DealerDocxContentMapper>();
            _dealerMapper.Setup(m => m.Map(_contract.Dealer.Requisites, _contract.DealersCity, _contract.DealersHeadPosition, string.Empty))
                .Returns(_dealerFields);

            //Setup private customer mapper
            _customerMapper = new Mock<PrivateCustomerDocxContentMapper>();
            _customerMapper.Setup(m => m.Map(_contract.PrivateCustomer, string.Empty))
                .Returns(_customerFields);

            //Setup contract mapper
            _mapper = new ContractDocxContentMapper(_paymentMapper.Object, _priceMapper.Object, _dealerMapper.Object,
                _vehicleMapper.Object, _customerMapper.Object);
        }

        [Fact]
        public void Ctor_GivenPaymentMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("paymentMapper",
                () => new ContractDocxContentMapper(null, _priceMapper.Object, _dealerMapper.Object, _vehicleMapper.Object,
                    _customerMapper.Object));
        }

        [Fact]
        public void Ctor_GivenPriceMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("priceMapper",
                () => new ContractDocxContentMapper(_paymentMapper.Object, null, _dealerMapper.Object, _vehicleMapper.Object,
                    _customerMapper.Object));
        }

        [Fact]
        public void Ctor_GivenDealerMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("dealerMapper",
                () => new ContractDocxContentMapper(_paymentMapper.Object, _priceMapper.Object, null, _vehicleMapper.Object,
                    _customerMapper.Object));
        }

        [Fact]
        public void Ctor_GivenVehicleMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("vehicleMapper",
                () => new ContractDocxContentMapper(_paymentMapper.Object, _priceMapper.Object, _dealerMapper.Object, null,
                    _customerMapper.Object));
        }

        [Fact]
        public void Ctor_GivenCustomerMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("customerMapper",
                () => new ContractDocxContentMapper(_paymentMapper.Object, _priceMapper.Object, _dealerMapper.Object, _vehicleMapper.Object,
                    null));
        }

        [Fact]
        public async Task Map()
        {
            //Arrange
            var contractFields = new[]
            {
                new FieldContent("Number", _contract.Number),
                new FieldContent("Date", _contract.Date.ToShortDateString()),
                new FieldContent("VehicleStockAddress", _contract.VehicleStockAddress),
                new FieldContent("VehicleDeliveryDays", _contract.VehicleDeliveryDays.ToString()),
                new FieldContent("SalesManagerShortName", _contract.SalesManagerName.ShortName)
            };
            
            var expected = contractFields.Concat(_paymentFields)
                .Concat(_priceFields)
                .Concat(_vehicleFields)
                .Concat(_dealerFields)
                .Concat(_customerFields)
                .OrderBy(f => f.Name);

            //Act
            var actual = await _mapper.Map(_contract);

            //Assert
            Assert.NotNull(actual);
            Assert.Equal(expected, actual.OrderBy(f => f.Name));
        }

        [Fact]
        public Task Map_GivenContract_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("contract", () => _mapper.Map(null));
        }
    }
}
