﻿using AutoFixture;
using Moq;
using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.Mappers;
using NexCore.VehicleConfigurator.Domain.Contracts;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Linq;
using System.Threading.Tasks;
using TemplateEngine.Docx;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Exports.Contracts.Mappers
{
    public class VehicleDocxContentMapperTest
    {
        private const string ModelName = "Octavia";
        private const string ColorName = "Red";

        private readonly VehicleSpecification _specification;
        private readonly VehicleDocxContentMapper _mapper;

        public VehicleDocxContentMapperTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _specification = fixture.Create<VehicleSpecification>();

            _mapper = new VehicleDocxContentMapper(SetupOptionsMapper());
        }

        private OptionDocxListContentMapper SetupOptionsMapper()
        {
            var mapper = new Mock<OptionDocxListContentMapper>();

            mapper.Setup(m => m.Map(
                    "AdditionalOptions", "AdditionalPrNumbers", _specification.AdditionalOptions, _specification.ModelKey.ModelId))
                .ReturnsAsync(new FieldContent("AdditionalOptions", string.Empty));

            mapper.Setup(m => m.Map(
                    "Prefix_AdditionalOptions", "Prefix_AdditionalPrNumbers", _specification.AdditionalOptions, _specification.ModelKey.ModelId))
                .ReturnsAsync(new FieldContent("Prefix_AdditionalOptions", string.Empty));

            mapper.Setup(m => m.Map(
                    "IncludedOptions", "IncludedPrNumbers", _specification.IncludedOptions, _specification.ModelKey.ModelId))
                .ReturnsAsync(new FieldContent("IncludedOptions", string.Empty));

            mapper.Setup(m => m.Map(
                    "Prefix_IncludedOptions", "Prefix_IncludedPrNumbers", _specification.IncludedOptions, _specification.ModelKey.ModelId))
                .ReturnsAsync(new FieldContent("Prefix_IncludedOptions", string.Empty));

            return mapper.Object;
        }

        [Fact]
        public void Ctor_GivenOptionsMappers_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("optionsMapper", () => new VehicleDocxContentMapper(null));
        }

        [Fact]
        public async Task Map()
        {
            //Arrange
            var expected = new[]
            {
                new FieldContent("ModelName", ModelName),
                new FieldContent("ModelYear", _specification.ModelYear.Value.ToString()), 
                new FieldContent("ColorName", ColorName), 
                new FieldContent("ColorId", _specification.ColorId.Value),
                new FieldContent("AdditionalOptions", string.Empty),
                new FieldContent("IncludedOptions", string.Empty)
            };

            //Act
            var actual = await _mapper.Map(_specification, ModelName, ColorName);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public Task Map_GivenSpecification_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("specification", () => _mapper.Map(null, ModelName, ColorName));
        }

        [Fact]
        public Task Map_GivenModelName_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("modelName", () => _mapper.Map(_specification, null, ColorName));
        }

        [Fact]
        public Task Map_GivenColorName_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("colorName", () => _mapper.Map(_specification, ModelName, null));
        }

        [Fact]
        public async Task Map_GivenPrefix_IsSet()
        {
            //Arrange
            var fieldNames = new[]
            {
                "Prefix_ModelName",
                "Prefix_ModelYear",
                "Prefix_ColorName",
                "Prefix_ColorId",
                "Prefix_AdditionalOptions",
                "Prefix_IncludedOptions"
            };

            //Act
            var fields = await _mapper.Map(_specification, ModelName, ColorName, "Prefix_");

            //Assert
            Assert.NotNull(fields);
            Assert.Equal(fieldNames, fields.Select(f => f.Name));
        }
    }
}
