﻿using AutoFixture;
using Moq;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.Mappers;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Contracts;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Dealers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TemplateEngine.Docx;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Exports.Contracts.Mappers
{
    public class OptionDocxListContentMapperTest
    {
        private const string ListName = "listName";
        private const string FieldName = "fieldName";

        private readonly ModelId _modelId;
        private readonly PrNumberSet _prNumbers;

        private readonly IFixture _fixture;

        private readonly Mock<IUnitOfWork> _work;

        private readonly OptionDocxListContentMapper _mapper;

        public OptionDocxListContentMapperTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());
            _fixture.Customize(new DealerIdCustomization());
            _fixture.Customize(new ContractTypeIdCustomization());

            _modelId = _fixture.Create<ModelId>();
            _prNumbers = _fixture.Create<PrNumberSet>();

            _work = new Mock<IUnitOfWork>();
            _mapper = new OptionDocxListContentMapper(_work.Object);
        }

        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work", () => new OptionDocxListContentMapper(null));
        }

        [Fact]
        public async Task Map()
        {
            //Arrange
            var options = _prNumbers.Values
                .Select(prNumber => new Option(OptionId.Next(_modelId, prNumber))
                {
                    Name = _fixture.Create<string>()
                })
                .ToList();

            _work.Setup(w => w.EntityRepository.Find<Option, OptionId>(It.Is<OptionWithPrNumberSpecification>(
                    s => s.PrNumbers.Equals(_prNumbers))))
                .ReturnsAsync(options);

            var expected = SetupContent(options, o => $"{o.PrNumber}, {o.Name}");

            //Act
            var actual = await _mapper.Map(ListName, FieldName, _prNumbers, _modelId);

            //Assert
            Assert.Equal(expected, actual);
        }

        private static ListContent SetupContent<T>(IEnumerable<T> values, Func<T, string> contentValue)
        {
            var content = new ListContent(ListName);

            foreach (var option in values)
            {
                content.AddItem(new FieldContent(FieldName, contentValue(option)));
            }

            return content;
        }

        [Fact]
        public async Task Map_WhenOptions_AreNotFound_PrintsPrNumbers()
        {
            //Arrange
            _work.Setup(w => w.EntityRepository.Find<Option, OptionId>(It.IsAny<OptionWithPrNumberSpecification>()))
                .ReturnsAsync(new List<Option>());

            var expected = SetupContent(_prNumbers.Values, n => n.Value);

            //Act
            var actual = await _mapper.Map(ListName, FieldName, _prNumbers, _modelId);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Task Map_GivenPrNumbers_IsEmpty()
        {
            //Arrange
            var expected = SetupContent(new[] { string.Empty }, n => n);

            //Act
            var actual = await _mapper.Map(ListName, FieldName, PrNumberSet.Empty, _modelId);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public Task Map_GivenListName_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("listName",
                () => _mapper.Map(null, FieldName, PrNumberSet.Empty, _modelId));
        }

        [Fact]
        public Task Map_GivenFieldName_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("fieldName",
                () => _mapper.Map(ListName, null, PrNumberSet.Empty, _modelId));
        }

        [Fact]
        public Task Map_GivenPrNumbers_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("prNumbers",
                () => _mapper.Map(ListName, FieldName, null, _modelId));
        }

        [Fact]
        public Task Map_GivenModelId_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("modelId",
                () => _mapper.Map(ListName, FieldName, PrNumberSet.Empty, null));
        }
    }
}