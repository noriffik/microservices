﻿using AutoFixture;
using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.Mappers;
using NexCore.VehicleConfigurator.Domain.Contracts;
using System;
using System.Globalization;
using System.Linq;
using TemplateEngine.Docx;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Exports.Contracts.Mappers
{
    public class TotalPriceDocxContentMapperTest
    {
        private const int Count = 2;

        private static readonly CultureInfo Culture = CultureInfo.CurrentCulture;

        private readonly TotalPrice _price;
        private readonly TotalPriceDocxContentMapper _mapper;

        public TotalPriceDocxContentMapperTest()
        {
            var fixture = new Fixture();

            _price = fixture.Create<TotalPrice>();

            _mapper = new TotalPriceDocxContentMapper();
        }

        [Fact]
        public void Map()
        {
            //Arrange
            
            var expected = new[]
            {
                new FieldContent("TotalPriceWithoutTaxRetail", _price.WithoutTax.Retail.ToString(Culture)),
                new FieldContent("TotalPriceTaxRetail", _price.Tax.Retail.ToString(Culture)),
                new FieldContent("TotalPriceWithTaxRetail", _price.WithTax.Retail.ToString(Culture)),
                new FieldContent("Count", Count.ToString(Culture)),
                new FieldContent("TotalCount", Count.ToString(Culture)),
                new FieldContent("TotalPriceWithTaxBase", _price.WithTax.Base.ToString(Culture))
            };

            //Act
            var actual = _mapper.Map(_price, Count);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Map_GivenPrefix_IsSet()
        {
            //Arrange
            var fieldNames = new[]
            {
                "Prefix_TotalPriceWithoutTaxRetail",
                "Prefix_TotalPriceTaxRetail",
                "Prefix_TotalPriceWithTaxRetail",
                "Prefix_Count",
                "Prefix_TotalCount",
                "Prefix_TotalPriceWithTaxBase"
            };

            //Act
            var fields = _mapper.Map(_price, Count, "Prefix_");

            //Assert
            Assert.NotNull(fields);
            Assert.Equal(fieldNames, fields.Select(f => f.Name));
        }

        [Fact]
        public void Map_GivenPrice_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("price", () => _mapper.Map(null, Count));
        }
    }
}
