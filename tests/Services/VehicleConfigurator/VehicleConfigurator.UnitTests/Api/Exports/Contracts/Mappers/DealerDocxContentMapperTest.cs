﻿using AutoFixture;
using NexCore.Legal;
using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.Mappers;
using System;
using System.Linq;
using TemplateEngine.Docx;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Exports.Contracts.Mappers
{
    public class DealerDocxContentMapperTest
    {
        private const string DealersCity = "Kyiv";
        private const string HeadPosition = "CEO";

        private readonly OrganizationRequisites _requisites;
        private readonly DealerDocxContentMapper _mapper;

        public DealerDocxContentMapperTest()
        {
            var fixture = new Fixture();

            _requisites = fixture.Create<OrganizationRequisites>();
            _mapper = new DealerDocxContentMapper();
        }

        [Fact]
        public void Map()
        {
            //Arrange
            var expected = new[]
            {
                new FieldContent("DealersCity", DealersCity),
                new FieldContent("DealerFullName", _requisites.Name.FullName),
                new FieldContent("DealersHeadPosition", HeadPosition),
                new FieldContent("DirectorFullName", _requisites.Director.ToString()),
                new FieldContent("DealerShortName", _requisites.Name.ShortName),
                new FieldContent("DealerAddress", _requisites.Address)
            };

            //Act
            var actual = _mapper.Map(_requisites, DealersCity, HeadPosition);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Map_GivenRequisites_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("requisites", () => _mapper.Map(null, DealersCity, HeadPosition));
        }

        [Fact]
        public void Map_GivenCity_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("city", () => _mapper.Map(_requisites, null, HeadPosition));
        }

        [Fact]
        public void Map_GivenHeadPosition_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("headPosition", () => _mapper.Map(_requisites, DealersCity, null));
        }

        [Fact]
        public void Map_GivenPrefix_IsSet()
        {
            //Arrange
            var fieldNames = new[]
            {
                "Prefix_DealersCity",
                "Prefix_DealerFullName",
                "Prefix_DealersHeadPosition",
                "Prefix_DirectorFullName",
                "Prefix_DealerShortName",
                "Prefix_DealerAddress"
            };

            //Act
            var fields = _mapper.Map(_requisites, DealersCity, HeadPosition, "Prefix_");

            //Assert
            Assert.NotNull(fields);
            Assert.Equal(fieldNames, fields.Select(f => f.Name));
        }
    }
}
