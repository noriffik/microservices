﻿using AutoFixture;
using Moq;
using NexCore.VehicleConfigurator.Api.Application.Exports.Contracts.Mappers;
using NexCore.VehicleConfigurator.Domain.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using TemplateEngine.Docx;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Exports.Contracts.Mappers
{
    public class PrivateCustomerDocxContentMapperTest
    {
        private readonly PrivateCustomer _customer;
        private readonly List<FieldContent> _passportFields;
        private readonly PrivateCustomerDocxContentMapper _mapper;

        public PrivateCustomerDocxContentMapperTest()
        {
            var fixture = new Fixture();
            
            _customer = fixture.Create<PrivateCustomer>();
            _passportFields = fixture.CreateMany<FieldContent>().ToList();
            _mapper = new PrivateCustomerDocxContentMapper(SetupPassportMapper());
        }

        private PassportDocxContentMapper SetupPassportMapper()
        {
            var mapper = new Mock<PassportDocxContentMapper>();

            mapper.Setup(m => m.Map(_customer.Passport, string.Empty))
                .Returns(_passportFields);

            mapper.Setup(m => m.Map(_customer.Passport, "Prefix_"))
                .Returns(_passportFields);

            return mapper.Object;
        }

        [Fact]
        public void Ctor_GivenPassportMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("passportMapper", () => new PrivateCustomerDocxContentMapper(null));
        }

        [Fact]
        public void Map()
        {
            //Arrange
            var expected = new[]
                {
                    new FieldContent("PCFullName", _customer.PersonName.ToString()),
                    new FieldContent("PCTelephone", _customer.Telephone),
                    new FieldContent("PCPhysicalAddress", _customer.PhysicalAddress),
                    new FieldContent("TaxIdentificationNumber", _customer.TaxIdentificationNumber.Number),
                    new FieldContent("PCShortName", _customer.PersonName.Lastname)
                }
                .Concat(_passportFields);

            //Act
            var actual = _mapper.Map(_customer);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Map_GivenCustomer_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("customer", () => _mapper.Map(null));
        }

        [Fact]
        public void Map_GivenPrefix_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("prefix", () => _mapper.Map(_customer, null));
        }

        [Fact]
        public void Map_GivenPrefix_IsSet()
        {
            //Arrange
            var fieldNames = new[]
            {
                "Prefix_PCFullName",
                "Prefix_PCTelephone",
                "Prefix_PCPhysicalAddress",
                "Prefix_TaxIdentificationNumber",
                "Prefix_PCShortName",
            }.Concat(_passportFields.Select(f => f.Name));

            //Act
            var fields = _mapper.Map(_customer, "Prefix_");

            //Assert
            Assert.NotNull(fields);
            Assert.Equal(fieldNames, fields.Select(f => f.Name));
        }
    }
}
