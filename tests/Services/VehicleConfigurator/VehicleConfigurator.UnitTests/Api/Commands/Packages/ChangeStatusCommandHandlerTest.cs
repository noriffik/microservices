﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Packages;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Packages
{
    public class ChangeStatusCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IPackageService> _service;
        private readonly Mock<IUnitOfWork> _work;

        //Handler and command
        private readonly ChangeStatusCommand _command;
        private readonly ChangeStatusCommandHandler _handler;

        public ChangeStatusCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _service = new Mock<IPackageService>();
            _work = new Mock<IUnitOfWork>();

            //Command
            _command = fixture.Build<ChangeStatusCommand>()
                .With(c => c.Id, fixture.Create<PackageId>().Value)
                .Create();

            //Handler
            _handler = new ChangeStatusCommandHandler(_service.Object, _work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ChangeStatusCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenCommandIsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.ChangeStatus(PackageId.Parse(_command.Id), _command.Status))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_WhenOffers_DoesNotContains_PackageOptions_Throws()
        {
            //Arrange
            _command.Status = PackageStatus.Published;
            _service.Setup(s => s.ChangeStatus(PackageId.Parse(_command.Id), _command.Status))
                .ThrowsAsync(new PackageRequirementViolationException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync("Status", () => _handler.Handle(_command, CancellationToken.None));
            
            //Assert
            e.WithMessage("test");
        }
    }
}
