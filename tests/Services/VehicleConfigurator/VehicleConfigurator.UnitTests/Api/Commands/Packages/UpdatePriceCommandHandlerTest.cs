﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Packages;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Packages
{
    public class UpdatePriceCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IPackageService> _service;
        private readonly Mock<IUnitOfWork> _work;

        //Handler and command
        private readonly UpdatePriceCommand _command;
        private readonly UpdatePriceCommandHandler _handler;

        public UpdatePriceCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _service = new Mock<IPackageService>();
            _work = new Mock<IUnitOfWork>();

            //Command
            _command = fixture.Build<UpdatePriceCommand>()
                .With(c => c.Id, fixture.Create<PackageId>().Value)
                .Create();

            //Handler
            _handler = new UpdatePriceCommandHandler(_service.Object, _work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ChangeStatusCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenCommandIsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.UpdatePrice(PackageId.Parse(_command.Id), _command.Price))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }
    }
}
