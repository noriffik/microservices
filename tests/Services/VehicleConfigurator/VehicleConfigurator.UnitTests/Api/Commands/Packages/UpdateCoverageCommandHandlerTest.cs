﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.Packages;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Packages
{
    public class UpdateCoverageCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IPackageService> _service;
        private readonly Mock<IUnitOfWork> _work;

        //Handler and command
        private readonly UpdateCoverageCommandHandler _handler;
        private readonly UpdateCoverageCommand _command;

        //Entities
        private readonly PackageId _packageId;
        private readonly BoundModelKeySet _modelKeys;

        public UpdateCoverageCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _service = new Mock<IPackageService>();
            _work = new Mock<IUnitOfWork>();

            //Entities
            _packageId = fixture.Create<PackageId>();
            _modelKeys = fixture.Create<BoundModelKeySet>();

            //Command
            _command = fixture.Build<UpdateCoverageCommand>()
                .With(c => c.Id, _packageId.Value)
                .With(c => c.ModelKeys, _modelKeys.AsString)
                .Create();

            //Handler
            _handler = new UpdateCoverageCommandHandler(_service.Object, _work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.UpdateCoverage(_packageId, _modelKeys))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_WhenEntity_IsNotFound_Throws()
        {
            //Arrange
            _service.Setup(s => s.UpdateCoverage(_packageId, _modelKeys))
                .ThrowsAsync(new InvalidEntityIdException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                string.Empty, () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }

        [Fact]
        public async Task Handle_WhenGivenModelId_IsDifferentToPackage_Throws()
        {
            //Arrange
            _service.Setup(s => s.UpdateCoverage(_packageId, _modelKeys))
                .ThrowsAsync(new SingleModelViolationException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.ModelKeys), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }

        [Fact]
        public async Task Handle_WhenShrinkCoverage_And_Package_IsPublished_Throws()
        {
            //Arrange
            _service.Setup(s => s.UpdateCoverage(_packageId, _modelKeys))
                .ThrowsAsync(new ProhibitedByStatusException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.Id), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }
    }
}
