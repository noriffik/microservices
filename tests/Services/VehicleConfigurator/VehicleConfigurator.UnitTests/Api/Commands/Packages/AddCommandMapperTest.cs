﻿using AutoFixture;
using Moq;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Commands.Packages;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Packages
{
    public class AddCommandMapperTest
    {
        private readonly AddCommandMapper _mapper;
        private readonly Mock<IApplicabilityParser> _parser;

        public AddCommandMapperTest()
        {
            _parser = new Mock<IApplicabilityParser>();
            _mapper = new AddCommandMapper(_parser.Object);
        }

        [Fact]
        public void Map_GivenCommand_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("command", () => _mapper.Map(null));
        }

        [Fact]
        public void Map()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());
            
            var expected = fixture.Create<Package>();

            _parser.Setup(s => s.Parse(expected.Applicability.AsString))
                .Returns(expected.Applicability);

            var command = fixture.Build<AddCommand>()
                .With(c => c.Id, expected.Id.Value)
                .With(c => c.CatalogId, expected.CatalogId)
                .With(c => c.Name, expected.Name)
                .With(c => c.Price, expected.Price)
                .With(c => c.ModelKeys, expected.ModelKeySet.AsString)
                .With(c => c.Applicability, expected.Applicability.AsString)
                .Create();

            //Act
            var actual = _mapper.Map(command);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<Package>.Instance);
        }
    }
}
