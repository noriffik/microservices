﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Packages;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.PackageApplicability;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Packages
{
    public class UpdateApplicabilityCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IPackageService> _service;
        private readonly Mock<IUnitOfWork> _work;

        //Entity
        private readonly Package _package;
        private readonly Applicability _applicability;

        //Handler and command
        private readonly UpdateApplicabilityCommandHandler _handler;
        private readonly UpdateApplicabilityCommand _command;

        public UpdateApplicabilityCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _service = new Mock<IPackageService>();
            _work = new Mock<IUnitOfWork>();

            //Entity
            _package = fixture.Create<Package>();
            _applicability = fixture.Create<Applicability>();

            //Command
            _command = fixture.Build<UpdateApplicabilityCommand>()
                .With(c => c.Id, _package.Id.Value)
                .Create();

            //Serializer
            var serializer = new Mock<IApplicabilityParser>();
            serializer.Setup(m => m.Parse(_command.Applicability)).Returns(_applicability);

            //Handler
            _handler = new UpdateApplicabilityCommandHandler(_service.Object, _work.Object, serializer.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.UpdateApplicability(_package.Id, _applicability))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }
    }
}
