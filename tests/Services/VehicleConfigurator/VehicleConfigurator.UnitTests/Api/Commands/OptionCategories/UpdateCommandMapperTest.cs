﻿using AutoFixture;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.OptionCategories
{
    public class UpdateCommandMapperTest
    {
        private readonly UpdateCommandMapper _mapper = new UpdateCommandMapper();
        private readonly UpdateCommand _command;
        private readonly OptionCategory _category;

        public UpdateCommandMapperTest()
        {
            var fixture = new Fixture();

            _category = fixture.Create<OptionCategory>();
            _command = fixture.Build<UpdateCommand>().With(c => c.Id, _category.Id).Create();
        }

        [Fact]
        public void MapId()
        {
            //Act
            var result = _mapper.MapId(_command);

            //Assert
            Assert.Equal(_command.Id, result);
        }

        [Fact]
        public void MapId_GivenCommand_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.MapId(null));
            Assert.Equal("command", e.ParamName);
        }

        [Fact]
        public void Map()
        {
            //Act
             _mapper.Map(_command, _category);

            //Assert
            Assert.Equal(_command.Name, _category.Name);
        }

        [Fact]
        public void Map_WhenGivenCommand_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.Map(null, _category));
            Assert.Equal("command", e.ParamName);
        }

        [Fact]
        public void Map_WhenGivenCategory_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.Map(_command, null));
            Assert.Equal("category", e.ParamName);
        }
    }
}
