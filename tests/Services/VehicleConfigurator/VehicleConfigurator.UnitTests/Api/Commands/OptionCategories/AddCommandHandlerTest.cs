﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.OptionCategories
{
    public class AddCommandHandlerTest
    {
        //Entities
        private readonly OptionCategory _optionCategory;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Handler and command
        private readonly AddCommandHandler _handler;
        private readonly AddCommand _command;

        public AddCommandHandlerTest()
        {
            var fixture = new Fixture()
                .CustomizeConstructor<OptionCategory>(5);

            //Entities
            _optionCategory = fixture.Create<OptionCategory>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            var mapper = new Mock<ICommandMapper<AddCommand, OptionCategory>>();

            //Handler and commands
            _handler = new AddCommandHandler(_work.Object)
            {
                CommandMapper = mapper.Object
            };
            _command = new AddCommand
            {
                Name = _optionCategory.Name
            };

            //Setup Dependencies
            _work.Setup(r => r.EntityRepository).Returns(_repository.Object);
            mapper.Setup(m => m.Map(_command)).Returns(_optionCategory);
        }

        [Fact]
        public void Ctor_VerifyInjections()
        {
            Assert.Injection.OfConstructor(typeof(AddCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Add(_optionCategory))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_optionCategory.Id, result);
            }
        }

        [Fact]
        public void Mapper()
        {
            //Arrange
            var handler = new AddCommandHandler(_work.Object);

            //Assert
            Assert.Injection.OfProperty(handler, nameof(handler.CommandMapper))
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}
