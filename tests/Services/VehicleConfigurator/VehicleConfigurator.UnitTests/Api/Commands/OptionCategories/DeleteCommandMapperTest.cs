﻿using NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.OptionCategories
{
    public class DeleteCommandMapperTest
    {
        private readonly DeleteCommandMapper _mapper = new DeleteCommandMapper();

        [Fact]
        public void Map()
        {
            //Arrange
            var command = new DeleteCommand { Id = 10 };

            //Act
            var result = _mapper.Map(command);

            //Assert
            Assert.Equal(command.Id, result);
        }

        [Fact]
        public void Map_GivenCommand_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.Map(null));
            Assert.Equal("command", e.ParamName);
        }
    }
}
