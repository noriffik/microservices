﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.OptionCategories
{
    public class AssignCommandHandlerTest
    {
        //Entities
        private readonly OptionId _id;
        private readonly int _optionCategoryId;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IOptionService> _optionService;

        //Handler and command
        private readonly AssignCommandHandler _handler;
        private readonly AssignCommand _command;

        public AssignCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entities
            _id = fixture.Create<OptionId>();
            _optionCategoryId = fixture.Create<int>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _optionService = new Mock<IOptionService>();

            //Handler and command
            _handler = new AssignCommandHandler(_work.Object, _optionService.Object);
            _command = new AssignCommand { OptionId = _id.Value, CategoryId = _optionCategoryId };
        }

        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(AssignCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _optionService.Setup(s => s.AssignToCategory(_id, _optionCategoryId))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_WhenEntity_IsNotFound_Throws()
        {
            //Arrange
            _optionService.Setup(s => s.AssignToCategory(_id, _optionCategoryId))
                .ThrowsAsync(new InvalidEntityIdException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                string.Empty, () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }
    }
}
