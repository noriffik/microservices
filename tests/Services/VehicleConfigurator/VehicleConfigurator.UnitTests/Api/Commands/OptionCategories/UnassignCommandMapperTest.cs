﻿using AutoFixture;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.OptionCategories
{
    public class UnassignCommandMapperTest
    {
        private readonly UnassignCommandMapper _mapper = new UnassignCommandMapper();
        private readonly UnassignCommand _command;
        private readonly Option _option;

        public UnassignCommandMapperTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _option = fixture.Create<Option>();
            _command = fixture.Build<UnassignCommand>().With(c => c.Id, _option.Id.Value).Create();
        }

        [Fact]
        public void MapId()
        {
            //Act
            var result = _mapper.MapId(_command);

            //Assert
            Assert.Equal(_command.Id, result.Value);
        }

        [Fact]
        public void MapId_WhenGivenCommand_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.MapId(null));
            Assert.Equal("command", e.ParamName);
        }

        [Fact]
        public void Map()
        {
            //Act
            _mapper.Map(_command, _option);

            //Assert
            Assert.Null(_option.OptionCategoryId);
        }

        [Fact]
        public void Map_WhenGivenCommand_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.Map(null, _option));
            Assert.Equal("command", e.ParamName);
        }

        [Fact]
        public void Map_WhenGivenOption_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.Map(_command, null));
            Assert.Equal("option", e.ParamName);
        }
    }
}