﻿using NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.OptionCategories
{
    public class AddCommandMapperTest
    {
        private readonly AddCommandMapper _mapper = new AddCommandMapper();

        [Fact]
        public void Map()
        {
            //Arrange
            var command = new AddCommand { Name = "Name" };

            //Act
            var result = _mapper.Map(command);

            //Assert
            Assert.Equal(command.Name, result.Name);
        }

        [Fact]
        public void Map_GivenCommand_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.Map(null));
            Assert.Equal("command", e.ParamName);
        }
    }
}
