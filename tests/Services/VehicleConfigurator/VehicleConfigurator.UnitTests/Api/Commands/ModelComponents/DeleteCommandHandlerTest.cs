﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.VehicleConfigurator.UnitTests.Fixtures;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.ModelComponents
{
    public class DeleteCommandHandlerTest
    {
        public class TestCommand : DeleteCommand<TestModelComponent, TestModelComponentId>
        {
        }

        public class TestCommandHandler : DeleteCommandHandler<TestModelComponent, TestModelComponentId>
        {
            public TestCommandHandler(IUnitOfWork work,
                IModelComponentService<TestModelComponent, TestModelComponentId> componentService,
                DeleteCommandMapper<TestModelComponent, TestModelComponentId> commandMapper)
                : base(work, componentService, commandMapper)
            {
            }
        }

        //Entities
        private readonly TestModelComponent _modelComponent;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IModelComponentService<TestModelComponent, TestModelComponentId>> _service;
        private readonly Mock<DeleteCommandMapper<TestModelComponent, TestModelComponentId>> _mapper;

        //Handler and command
        private readonly TestCommandHandler _handler;
        private readonly TestCommand _request;

        public DeleteCommandHandlerTest()
        {
            var fixture = new Fixture()
                .CustomizeConstructor<TestModelComponent, TestModelComponentId>(new TestModelComponentId("AAA"));

            _modelComponent = fixture.Create<TestModelComponent>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IModelComponentService<TestModelComponent, TestModelComponentId>>();
            _mapper = new Mock<DeleteCommandMapper<TestModelComponent, TestModelComponentId>>();
            
            //Handler and command
            _request = fixture.Build<TestCommand>().With(c => c.Id, _modelComponent.Id.Value).Create();
            _handler = new TestCommandHandler(_work.Object, _service.Object, _mapper.Object);

            //Setup dependencies
            _mapper.Setup(m => m.Map(_request)).Returns(_modelComponent.Id);
        }

        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection
                .OfConstructor(typeof(TestCommandHandler))
                .HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => 
                _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(r => r.Delete(_modelComponent.Id))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                
                //Act
                await _handler.Handle(_request, CancellationToken.None);
            }
        }
    }
}
