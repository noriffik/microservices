﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Gearboxes;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.ModelComponents.Gearboxes
{
    public class UpdateCommandMapperTest
    {
        private readonly UpdateCommandMapper<UpdateCommand> _mapper = 
            new UpdateCommandMapper<UpdateCommand>();

        [Fact]
        public void MapId()
        {
            //Arrange
            var command = new UpdateCommand
            {
                Id = "PRN",
                Name = "Name",
                Type = "Type",
                GearBoxCategory = GearBoxCategory.Automatic
            };

            //Act
            var result = _mapper.MapId(command);

            //Assert
            Assert.Equal(command.Id, result.Value);
        }

        [Fact]
        public void MapId_GivenCommand_IsNull_Throws()
        {
            //Act
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.MapId(null));

            //Assert
            Assert.Equal("command", e.ParamName);
        }

        [Fact]
        public void Map()
        {
            //Arrange
            var gearbox = new Gearbox(GearboxId.Parse("AA1"), GearboxType.Parse("AA3"));
            var command = new UpdateCommand
            {
                Id = "PRN",
                Name = "Name",
                Type = "AA4",
                GearBoxCategory = null
            };

            //Act
            _mapper.Map(command, gearbox);

            //Assert
            Assert.Equal(command.Name, gearbox.Name);
            Assert.Equal(command.Type, gearbox.Type.Value);
            Assert.Equal(command.GearBoxCategory, gearbox.GearBoxCategory);
        }

        [Fact]
        public void Map_GivenCommand_IsNull_Throws()
        {
            //Act
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.MapId(null));

            //Assert
            Assert.Equal("command", e.ParamName);
        }
    }
}
