﻿using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Gearboxes;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.ModelComponents.Gearboxes
{
    public class AddCommandMapperTest
    {
        private readonly AddCommandMapper _mapper = new AddCommandMapper();

        [Fact]
        public void Map()
        {
            //Arrange
            var command = new AddCommand
            {
                Id = "PRN",
                Name = "Name",
                Type = "TYPE",
                GearBoxCategory = GearBoxCategory.Automatic
            };

            //Act
            var result = _mapper.Map(command);

            //Assert
            Assert.Equal(command.Id, result.Id.Value);
            Assert.Equal(command.Name, result.Name);
            Assert.Equal(command.Type, result.Type.Value);
            Assert.Equal(command.GearBoxCategory, result.GearBoxCategory);
        }

        [Fact]
        public void Map_GivenCommand_IsNull_Throws()
        {
            //Act
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.Map(null));

            //Assert
            Assert.Equal("command", e.ParamName);
        }
    }
}
