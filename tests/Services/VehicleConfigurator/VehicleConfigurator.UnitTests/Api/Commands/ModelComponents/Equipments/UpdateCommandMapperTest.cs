﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Equipments;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.ModelComponents.Equipments
{
    public class UpdateCommandMapperTest
    {
        private readonly UpdateCommandMapper<UpdateCommand<Equipment, EquipmentId>> _mapper = 
            new UpdateCommandMapper<UpdateCommand<Equipment, EquipmentId>>();

        [Fact]
        public void MapId()
        {
            //Arrange
            var command = new UpdateCommand {Id = "PRN", Name = "Name"};

            //Act
            var result = _mapper.MapId(command);

            //Assert
            Assert.Equal(command.Id, result.Value);
        }

        [Fact]
        public void MapId_GivenCommand_IsNull_Throws()
        {
            //Act
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.MapId(null));

            //Assert
            Assert.Equal("command", e.ParamName);
        }

        [Fact]
        public void Map()
        {
            //Arrange
            var equipment = new Equipment(EquipmentId.Parse("AA1"));
            var command = new UpdateCommand {Id = "PRN", Name = "Name"};

            //Act
            _mapper.Map(command, equipment);

            //Assert
            Assert.Equal(command.Name, equipment.Name);
        }

        [Fact]
        public void Map_GivenCommand_IsNull_Throws()
        {
            //Act
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.MapId(null));

            //Assert
            Assert.Equal("command", e.ParamName);
        }
    }
}
