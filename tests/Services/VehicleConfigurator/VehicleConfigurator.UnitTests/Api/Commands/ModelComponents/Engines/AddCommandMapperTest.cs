﻿using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Engines;
using System;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.ModelComponents.Engines
{
    public class AddCommandMapperTest
    {
        private readonly AddCommandMapper _mapper = new AddCommandMapper();

        [Fact]
        public void Map()
        {
            //Arrange
            var command = new AddCommand {Id = "PRN", Name = "Name", FuelType = FuelType.Petrol};

            //Act
            var result = _mapper.Map(command);

            //Assert
            Assert.Equal(command.Id, result.Id.Value);
            Assert.Equal(command.Name, result.Name);
            Assert.Equal(command.FuelType, result.FuelType);
        }

        [Fact]
        public void Map_GivenCommand_IsNull_Throws()
        {
            //Act
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.Map(null));

            //Assert
            Assert.Equal("command", e.ParamName);
        }
    }
}
