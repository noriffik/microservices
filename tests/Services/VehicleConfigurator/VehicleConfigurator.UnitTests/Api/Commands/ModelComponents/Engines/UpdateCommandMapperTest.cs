﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Engines;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.ModelComponents.Engines
{
    public class UpdateCommandMapperTest
    {
        private readonly UpdateCommandMapper<UpdateCommand> _mapper = 
            new UpdateCommandMapper<UpdateCommand>();

        [Fact]
        public void MapId()
        {
            //Arrange
            var command = new UpdateCommand {Id = "PRN", Name = "Name"};

            //Act
            var result = _mapper.MapId(command);

            //Assert
            Assert.Equal(command.Id, result.Value);
        }

        [Fact]
        public void MapId_GivenCommand_IsNull_Throws()
        {
            //Act
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.MapId(null));

            //Assert
            Assert.Equal("command", e.ParamName);
        }

        [Fact]
        public void Map()
        {
            //Arrange
            var engine = new Engine(EngineId.Parse("AA1"));
            var command = new UpdateCommand {Id = "PRN", Name = "Name", FuelType = FuelType.Petrol};

            //Act
            _mapper.Map(command, engine);

            //Assert
            Assert.Equal(command.Name, engine.Name);
            Assert.Equal(command.FuelType, engine.FuelType);
        }

        [Fact]
        public void Map_GivenCommand_IsNull_Throws()
        {
            //Act
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.MapId(null));

            //Assert
            Assert.Equal("command", e.ParamName);
        }
    }
}
