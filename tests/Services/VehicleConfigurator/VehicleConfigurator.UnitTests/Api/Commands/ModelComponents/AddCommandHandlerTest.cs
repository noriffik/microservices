﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.VehicleConfigurator.UnitTests.Fixtures;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.ModelComponents
{
    public class AddCommandHandlerTest
    {
        public class TestCommand : AddCommand<TestModelComponent, TestModelComponentId>
        {
        }

        public class TestCommandHandler : AddCommandHandler<TestCommand, TestModelComponent, TestModelComponentId>
        {
            public TestCommandHandler(IUnitOfWork work,
                IModelComponentService<TestModelComponent, TestModelComponentId> componentService,
                ICommandMapper<TestCommand, TestModelComponent> commandMapper)
                : base(work, componentService, commandMapper)
            {
            }
        }

        //Entities
        private readonly TestModelComponent _modelComponent;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IModelComponentService<TestModelComponent, TestModelComponentId>> _service;
        private readonly Mock<ICommandMapper<TestCommand, TestModelComponent>> _mapper;

        //Handler and command
        private readonly TestCommandHandler _handler;
        private readonly TestCommand _request;

        public AddCommandHandlerTest()
        {
            var fixture = new Fixture()
                .CustomizeConstructor<TestModelComponent, TestModelComponentId>(new TestModelComponentId("AAA"));

            //Entities
            _modelComponent = fixture.Create<TestModelComponent>();
            
            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IModelComponentService<TestModelComponent, TestModelComponentId>>();
            _mapper = new Mock<ICommandMapper<TestCommand, TestModelComponent>>();
            
            //Handler and command
            _handler = new TestCommandHandler(_work.Object, _service.Object, _mapper.Object);
            _request = fixture.Build<TestCommand>().With(c => c.Id, _modelComponent.Id.Value).Create();

            //Setup dependencies
            _mapper.Setup(m => m.Map(_request)).Returns(_modelComponent);
        }

        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work", () =>
            {
                var unused = new TestCommandHandler(null, _service.Object, _mapper.Object);
            });
        }

        [Fact]
        public void Ctor_GivenComponentService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("componentService", () =>
            {
                var unused = new TestCommandHandler(_work.Object, null, _mapper.Object);
            });
        }

        [Fact]
        public void Ctor_GivenCommandMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("commandMapper", () =>
            {
                var unused = new TestCommandHandler(_work.Object, _service.Object, null);
            });
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(r => r.Add(_modelComponent))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                
                //Act
                var result = await _handler.Handle(_request, CancellationToken.None);

                //Assert
                Assert.Equal(_request.Id, result);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_WhenFailed_ToValidate_Throws()
        {
            //Arrange
            _service.Setup(s => s.Add(_modelComponent))
                .Throws(new InvalidEntityIdException(typeof(Model), _modelComponent.ModelId));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                "ModelId", () => _handler.Handle(_request, CancellationToken.None));
            
            //Assert
            e.WithValue(_modelComponent.ModelId.Value);
        }
    }
}
