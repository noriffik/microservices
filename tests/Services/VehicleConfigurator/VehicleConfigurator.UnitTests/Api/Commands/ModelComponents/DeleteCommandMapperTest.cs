﻿using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents;
using NexCore.VehicleConfigurator.UnitTests.Fixtures;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.ModelComponents
{
    public class DeleteCommandMapperTest
    {
        private readonly DeleteCommandMapper<TestModelComponent, TestModelComponentId> _mapper
            = new DeleteCommandMapper<TestModelComponent, TestModelComponentId>(
                v => new TestModelComponentId(v));

        [Fact]
        public void Map()
        {
            //Arrange
            var command = new DeleteCommand<TestModelComponent, TestModelComponentId> {Id = "PRN"};
            //Act
            var result = _mapper.Map(command);

            //Assert
            Assert.Equal(command.Id, result.Value);
        }

        [Fact]
        public void Map_GivenCommand_IsNull_Throws()
        {
            //Assert
            var e = Assert.Throws<ArgumentNullException>(() => _mapper.Map(null));
            Assert.Equal("command", e.ParamName);
        }
    }
}
