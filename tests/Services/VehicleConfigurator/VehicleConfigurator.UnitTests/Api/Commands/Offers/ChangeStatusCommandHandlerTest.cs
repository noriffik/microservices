﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Offers;
using NexCore.VehicleConfigurator.Domain.Offers;
using System;
using System.Threading;
using System.Threading.Tasks;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Offers
{
    public class ChangeStatusCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IOfferService> _service;

        //Handler and command
        private readonly ChangeStatusCommandHandler _handler;
        private readonly ChangeStatusCommand _command;

        public ChangeStatusCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            var offer = fixture.Create<Offer>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IOfferService>();

            //Handler and command
            _command = fixture.Build<ChangeStatusCommand>()
                .With(c => c.Id, offer.Id)
                .Create();

            _handler = new ChangeStatusCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.ChangeStatus(_command.Id, _command.Status))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_GivenCommand_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
