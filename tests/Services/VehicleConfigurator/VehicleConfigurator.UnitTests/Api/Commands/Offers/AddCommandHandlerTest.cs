﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Offers;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Offers
{
    public class AddCommandHandlerTest
    {
        //Entity
        private readonly int _configurationId;
        private readonly Offer _offer;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IOfferService> _service;

        //Command and Handler
        private readonly AddCommand _command;
        private readonly AddCommandHandler _handler;

        public AddCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _offer = fixture.Create<Offer>();
            _configurationId = fixture.Create<int>();

            //Command
            _command = fixture.Build<AddCommand>()
                .With(c => c.ConfigurationId, _configurationId)
                .Create();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IOfferService>();

            //Handler
            _handler = new AddCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work", () => new AddCommandHandler(null, _service.Object));
        }

        [Fact]
        public void Ctor_GivenService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("service", () => new AddCommandHandler(_work.Object, null));
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Trows()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.Add(_configurationId))
                    .InSequence()
                    .ReturnsAsync(_offer.Id);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(result, _offer.Id);
            }
        }

        [Fact]
        public async Task Handle_WhenGivenConfigurationId_NotFound_Throws()
        {
            //Arrange
            var inner = new InvalidEntityIdException(typeof(Configuration), _command.ConfigurationId);
            _service.Setup(s => s.Add(_configurationId)).Throws(inner);

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.ConfigurationId), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithValue(inner.Id).WithMessage(inner.Message);
        }
    }
}
