﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Offers;
using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Offers
{
    public class SetPrivateCustomerCommandHandlerTest
    {
        //Entity
        private readonly Offer _offer;
        private readonly int _privateCustomerId;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IOfferService> _service;

        //Command and Handler
        private readonly SetPrivateCustomerCommand _command;
        private readonly SetPrivateCustomerCommandHandler _handler;

        public SetPrivateCustomerCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _offer = fixture.Create<Offer>();
            _privateCustomerId = fixture.Create<int>();

            //Command
            _command = fixture.Build<SetPrivateCustomerCommand>()
                .With(c => c.OfferId, _offer.Id)
                .With(c => c.PrivateCustomerId, _privateCustomerId)
                .Create();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IOfferService>();

            //Handler
            _handler = new SetPrivateCustomerCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work", () => new SetPrivateCustomerCommandHandler(null, _service.Object));
        }

        [Fact]
        public void Ctor_GivenService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("service", () => new SetPrivateCustomerCommandHandler(_work.Object, null));
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Trows()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.SetPrivateCustomer(_offer.Id, _privateCustomerId))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }
    }
}
