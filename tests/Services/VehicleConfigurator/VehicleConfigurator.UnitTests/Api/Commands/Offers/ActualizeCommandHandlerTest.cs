﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Offers;
using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Offers
{
    public class ActualizeCommandHandlerTest
    {
        //Entity
        private readonly int _offerId;
        private readonly Offer _offer;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IOfferService> _service;

        //Command and Handler
        private readonly ActualizeCommand _command;
        private readonly ActualizeCommandHandler _handler;

        public ActualizeCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _offer = fixture.Create<Offer>();
            _offerId = fixture.Create<int>();

            //Command
            _command = fixture.Build<ActualizeCommand>()
                .With(c => c.OfferId, _offer.Id)
                .Create();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IOfferService>();

            //Handler
            _handler = new ActualizeCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work", () => new ActualizeCommandHandler(null, _service.Object));
        }

        [Fact]
        public void Ctor_GivenService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("service", () => new ActualizeCommandHandler(_work.Object, null));
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Trows()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.Actualize(_offer.Id))
                    .InSequence()
                    .ReturnsAsync(_offerId);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(result, _offerId);
            }
        }

        [Fact]
        public async Task Handle_WhenGivenOfferId_NotFound_Throws()
        {
            //Arrange
            var inner = new InvalidEntityIdException(typeof(Offer), _command.OfferId);
            _service.Setup(s => s.Actualize(_offer.Id)).Throws(inner);

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.OfferId), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithValue(inner.Id).WithMessage(inner.Message);
        }
    }
}
