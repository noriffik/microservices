﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Testing;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.Components;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.VehicleConfigurator.UnitTests.Fixtures;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Components
{
    public class DeleteCommandHandlerTest
    {
        public class TestCommand : DeleteCommand<TestComponent, TestId>
        {
        }

        public class TestCommandHandler : DeleteCommandHandler<TestComponent, TestId>
        {
            public TestCommandHandler(IUnitOfWork work,
                IComponentService<TestComponent, TestId> componentService,
                DeleteCommandMapper<TestComponent, TestId> commandMapper)
                : base(work, componentService, commandMapper)
            {
            }
        }

        private readonly IFixture _fixture = new Fixture()
            .CustomizeConstructor<TestComponent, TestId>(new TestId("AAA"));

        //Entities
        private readonly TestComponent _component;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IComponentService<TestComponent, TestId>> _service;
        private readonly Mock<DeleteCommandMapper<TestComponent, TestId>> _mapper;

        //Handler and command
        private readonly TestCommandHandler _handler;
        private readonly TestCommand _request;

        public DeleteCommandHandlerTest()
        {
            //Entities
            _component = _fixture.Create<TestComponent>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IComponentService<TestComponent, TestId>>();
            _mapper = new Mock<DeleteCommandMapper<TestComponent, TestId>>();
            
            //Handler and command
            _request = _fixture.Build<TestCommand>().With(c => c.Id, _component.Id.Value).Create();
            _handler = new TestCommandHandler(_work.Object, _service.Object, _mapper.Object);

            //Setup dependencies
            _mapper.Setup(m => m.Map(_request)).Returns(_component.Id);
        }

        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work", () =>
                new TestCommandHandler(null, _service.Object, _mapper.Object));
        }

        [Fact]
        public void Ctor_GivenService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("componentService", () =>
                new TestCommandHandler(_work.Object, null, _mapper.Object));
        }

        [Fact]
        public void Ctor_GivenCommandMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("commandMapper", () =>
                new TestCommandHandler(_work.Object, _service.Object, null));
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => 
                _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(r => r.Delete(_component.Id))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                
                //Act
                await _handler.Handle(_request, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_WhenComponent_IsInUse_Throws()
        {
            //Arrange
            var inner = new ComponentInUseException(_component.Id, _component.GetType());
            _service.Setup(r => r.Delete(_component.Id)).ThrowsAsync(inner);

            //Act
            var e = await Assert.Validation.ThrowsAsync(nameof(_request.Id), () => _handler.Handle(_request, CancellationToken.None));

            //Assert
            e.WithMessage(inner.Message).WithValue(_component.Id);
        }
    }
}
