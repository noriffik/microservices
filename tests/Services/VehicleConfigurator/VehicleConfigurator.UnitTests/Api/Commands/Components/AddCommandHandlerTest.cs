﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Testing;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands;
using NexCore.VehicleConfigurator.Api.Application.Commands.Components;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.VehicleConfigurator.UnitTests.Fixtures;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Components
{
    public class AddCommandHandlerTest
    {
        public class TestCommand : AddCommand<TestComponent, TestId>
        {
        }

        public class TestCommandHandler : AddCommandHandler<TestCommand, TestComponent, TestId>
        {
            public TestCommandHandler(IUnitOfWork work,
                IComponentService<TestComponent, TestId> componentService,
                ICommandMapper<TestCommand, TestComponent> commandMapper)
                : base(work, componentService, commandMapper)
            {
            }
        }

        private readonly IFixture _fixture = new Fixture()
            .CustomizeConstructor<TestComponent, TestId>(new TestId("AAA"));

        //Entities
        private readonly TestComponent _component;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IComponentService<TestComponent, TestId>> _service;
        private readonly Mock<ICommandMapper<TestCommand, TestComponent>> _mapper;

        //Handler and command
        private readonly TestCommandHandler _handler;
        private readonly TestCommand _request;

        public AddCommandHandlerTest()
        {
            //Entities
            _component = _fixture.Create<TestComponent>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IComponentService<TestComponent, TestId>>();
            _mapper = new Mock<ICommandMapper<TestCommand, TestComponent>>();
            
            //Handler and command
            _request = _fixture.Build<TestCommand>().With(c => c.Id, _component.Id.Value).Create();
            _handler = new TestCommandHandler(_work.Object, _service.Object, _mapper.Object);

            //Setup dependencies
            _mapper.Setup(m => m.Map(_request)).Returns(_component);
        }

        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work", () =>
                new TestCommandHandler(null, _service.Object, _mapper.Object));
        }

        [Fact]
        public void Ctor_GivenService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("componentService", () =>
                new TestCommandHandler(_work.Object, null, _mapper.Object));
        }

        [Fact]
        public void Ctor_GivenCommandMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("commandMapper", () =>
                new TestCommandHandler(_work.Object, _service.Object, null));
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => 
                _handler.Handle(null, CancellationToken.None));
        }
        
        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(r => r.Add(_component))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                
                //Act
                var result = await _handler.Handle(_request, CancellationToken.None);

                //Assert
                Assert.Equal(_request.Id, result);
            }
        }

        [Fact]
        public async Task Handle_WhenGivenTestComponentId_IsInUse_Throws()
        {
            //Arrange
            _service.Setup(s => s.Add(_component))
                .ThrowsAsync(new DuplicateEntityException(_component.GetType(), _component.Id));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                "TestComponentId", () => _handler.Handle(_request, CancellationToken.None));

            //Assert
            e.WithValue(_component.Id);
        }
    }
}
