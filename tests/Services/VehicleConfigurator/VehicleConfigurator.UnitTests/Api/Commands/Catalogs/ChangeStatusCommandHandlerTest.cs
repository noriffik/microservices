﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Catalogs
{
    public class ChangeStatusCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<ICatalogService> _service;

        //Handler and command
        private readonly ChangeStatusCommandHandler _handler;
        private readonly ChangeStatusCommand _command;

        public ChangeStatusCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new DomainBuilder());

            //Entity
            var catalog = fixture.Create<Catalog>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<ICatalogService>();

            //Handler and command
            _command = fixture.Build<ChangeStatusCommand>()
                .With(c => c.Id, catalog.Id)
                .Create();

            _handler = new ChangeStatusCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.ChangeStatus(_command.Id, _command.Status))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_GivenCommand_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
