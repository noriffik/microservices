﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands;
using NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Catalogs
{
    public class AddCommandHandlerTest
    {
        //Entities
        private readonly Catalog _catalog;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<ICatalogService> _service;
        private readonly Mock<ICommandMapper<AddCommand, Catalog>> _mapper;
        
        //Handler and command
        private readonly AddCommandHandler _handler;
        private readonly AddCommand _command;

        public AddCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new DomainBuilder());

            //Entity
            _catalog = fixture.Create<Catalog>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<ICatalogService>();
            _mapper = new Mock<ICommandMapper<AddCommand, Catalog>>();

            //Handler and command
            _handler = new AddCommandHandler(_work.Object, _service.Object)
            {
                Mapper = _mapper.Object
            };
            _command = fixture.Create<AddCommand>();

            //Setup dependencies
            _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);
        }

        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work", () =>
            {
                var unused = new AddCommandHandler(null, _service.Object);
            });
        }

        [Fact]
        public void Ctor_GivenComponentService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("service", () =>
            {
                var unused = new AddCommandHandler(_work.Object, null);
            });
        }

        [Fact]
        public void Mapper()
        {
            Assert.Injection.OfProperty(_handler, "Mapper")
                .HasDefault()
                .HasNullGuard()
                .HasDefault();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                _mapper.Setup(m => m.Map(_command))
                    .InSequence()
                    .Returns(_catalog);
                _service.Setup(s => s.Add(_catalog))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.NotNull(result);
            }
        }
        
        [Fact]
        public async Task Handle_WhenGivenCommand_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public Task Handle_WhenCatalog_ForGivenModelYear_AndRelevancePeriod_Exists_Throws()
        {
            //Arrange
            _mapper.Setup(m => m.Map(_command))
                .Returns(_catalog);

            _service.Setup(s => s.Add(_catalog))
                .ThrowsAsync(new DuplicateCatalogException());

            //Assert
            return Assert.Validation.ThrowsAsync(
                nameof(_command.RelevancePeriod), () => _handler.Handle(_command, CancellationToken.None));
        }
    }
}
