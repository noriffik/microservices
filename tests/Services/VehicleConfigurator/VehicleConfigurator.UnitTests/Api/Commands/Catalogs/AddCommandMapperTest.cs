﻿using AutoFixture;
using Moq;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Catalogs
{
    public class AddCommandMapperTest
    {
        //Entity
        private readonly Catalog _catalog;

        //Dependencies
        private readonly Mock<IPeriodModelMapper> _periodMapper;

        //Commands and mapper
        private readonly AddCommand _command;
        private readonly AddCommandMapper _mapper;

        public AddCommandMapperTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new DomainBuilder());

            //Entity
            _catalog = fixture.Create<Catalog>();

            //Dependencies
            _periodMapper = new Mock<IPeriodModelMapper>();

            //Commands and mapper
            _command = new AddCommand
            {
                RelevancePeriod = new PeriodModel
                {
                    From = _catalog.RelevancePeriod.From.Value,
                    To = _catalog.RelevancePeriod.To
                },
                Year = _catalog.ModelYear.Value
            };

            _mapper = new AddCommandMapper {PeriodMapper = _periodMapper.Object};
        }

        [Fact]
        public void Map()
        {
            //Arrange
            _periodMapper.Setup(m => m.MapReverse(_command.RelevancePeriod))
                .Returns(_catalog.RelevancePeriod);

            //Act
            var result = _mapper.Map(_command);
            
            //Assert
            Assert.NotNull(result);
            Assert.Equal(_catalog.ModelYear, result.ModelYear);
            Assert.Equal(_catalog.RelevancePeriod, result.RelevancePeriod);
        }
        
        [Fact]
        public void Map_WhenYear_IsInvalid_Throws()
        {
            //Arrange
            _command.Year = 12312312;

            //Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => _mapper.Map(_command));
        }

        [Fact]
        public void Map_GivenCommand_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("command", () => _mapper.Map(null));
        }
    }
}
