﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Testing.Fixtures.SpecimenBuilders.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Catalogs
{
    public class ChangeRelevancePeriodEndingCommandHandlerTest
    {
        //Entity
        private readonly Catalog _catalog;
        private readonly PeriodModel _periodModel;
        private readonly RelevancePeriod _relevancePeriod;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<ICatalogService> _service;

        //Handler and command
        private readonly ChangeRelevancePeriodCommandHandler _handler;
        private readonly ChangeRelevancePeriodCommand _command;

        public ChangeRelevancePeriodEndingCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new DomainBuilder());

            //Entity
            _catalog = fixture.Create<Catalog>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<ICatalogService>();

            //Handler and command
            _relevancePeriod = fixture.Create<RelevancePeriod>();

            _periodModel = fixture.Build<PeriodModel>()
                .With(m => m.From, _relevancePeriod.From)
                .With(m => m.To, _relevancePeriod.To)
                .Create();

            _command = fixture.Build<ChangeRelevancePeriodCommand>()
                .With(c => c.CatalogId, _catalog.Id)
                .With(c => c.RelevancePeriod, _periodModel)
                .Create();

            _handler = new ChangeRelevancePeriodCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.ChangeRelevancePeriod(_command.CatalogId, _relevancePeriod))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

              //Act
              await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_GivenCommand_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_WhenPeriodTo_IsInvalid_Throws()
        {
            //Arrange
            _service.Setup(s => s.ChangeRelevancePeriod(_command.CatalogId, _relevancePeriod))
                .ThrowsAsync(new InvalidPeriodException());

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                "period", () => _handler.Handle(_command, CancellationToken.None));
            
            //Assert
            e.WithMessage("Invalid period").WithValue(_relevancePeriod);
        }

        [Fact]
        public async Task Handle_WhenPeriod_IsOverlapping_Throws()
        {
            //Arrange
            _service.Setup(s => s.ChangeRelevancePeriod(_command.CatalogId, _relevancePeriod))
                .ThrowsAsync(new DuplicateCatalogException());

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                "period", () => _handler.Handle(_command, CancellationToken.None));
            
            //Assert
            e.WithMessage("Period is overlapping").WithValue(_relevancePeriod);
        }
    }
}
