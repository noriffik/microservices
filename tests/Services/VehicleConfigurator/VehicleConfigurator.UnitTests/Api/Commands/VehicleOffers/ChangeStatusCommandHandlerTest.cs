﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.VehicleOffers
{
    public class ChangeStatusCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IVehicleOfferService> _service;
        private readonly Mock<IUnitOfWork> _work;

        //Handler and command
        private readonly ChangeStatusCommandHandler _handler;
        private readonly ChangeStatusCommand _command;

        public ChangeStatusCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _service = new Mock<IVehicleOfferService>();
            _work = new Mock<IUnitOfWork>();

            //Command
            _command = fixture.Create<ChangeStatusCommand>();

            //Handler
            _handler = new ChangeStatusCommandHandler(_service.Object, _work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.ChangeStatus(_command.VehicleOfferId, _command.Status))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_WhenPublishRequirements_IsNotSatisfied_Throws()
        {
            //Arrange
            _service.Setup(s => s.ChangeStatus(_command.VehicleOfferId, _command.Status))
                .ThrowsAsync(new VehicleOfferPublishViolationException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                string.Empty, () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }
    }
}
