﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.VehicleOffers
{
    public class ChangeVehicleOfferRelevancePeriodCommandHandlerTest
    {
        private const int VehicleOfferId = 1;
        private readonly Fixture _fixture;

        //Entity
        private readonly RelevancePeriod _relevancePeriod;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IVehicleOfferService> _service;

        //Handler
        private readonly ChangeVehicleOfferRelevancePeriodCommandHandler _handler;

        public ChangeVehicleOfferRelevancePeriodCommandHandlerTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _relevancePeriod = new RelevancePeriod(_fixture.Create<DateTime>());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IVehicleOfferService>();

            //Handler
            _handler = new ChangeVehicleOfferRelevancePeriodCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ChangeVehicleOfferRelevancePeriodCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenCommand_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                _handler.Handle(null as ChangeVehicleOfferRelevancePeriodCommand, CancellationToken.None));
        }

        [Fact]
        public Task Handle_ResetRelevancePeriod_GivenCommand_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                _handler.Handle(null as ResetVehicleOfferRelevancePeriodCommand, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_ChangeRelevancePeriod()
        {
            using (Sequence.Create())
            {
                //Arrange
                var relevancePeriod = _fixture.Build<PeriodModel>()
                    .With(m => m.From, _relevancePeriod.From)
                    .With(m => m.To, _relevancePeriod.To)
                    .Create();
                var command = _fixture.Build<ChangeVehicleOfferRelevancePeriodCommand>()
                    .With(c => c.Id, VehicleOfferId)
                    .With(c => c.RelevancePeriod, relevancePeriod)
                    .Create();

                _service.Setup(s => s.ChangeRelevancePeriod(command.Id, _relevancePeriod))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_ResetRelevancePeriod()
        {
            using (Sequence.Create())
            {
                //Arrange
                var command = new ResetVehicleOfferRelevancePeriodCommand { Id = VehicleOfferId };

                _service.Setup(s => s.ChangeRelevancePeriod(command.Id, RelevancePeriod.Empty))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(command, CancellationToken.None);
            }
        }
    }
}
