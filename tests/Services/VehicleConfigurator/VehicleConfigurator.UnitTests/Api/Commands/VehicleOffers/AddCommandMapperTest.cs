﻿using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.VehicleOffers
{
    public class AddCommandMapperTest
    {
        private readonly AddCommandMapper _mapper = new AddCommandMapper();

        [Fact]
        public void Map_WhenGivenCommand_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("command", () => _mapper.Map(null));
        }

        [Fact]
        public void Map()
        {
            //Arrange
            var command = new AddCommand {ModelKey = "STRING", CatalogId = 1, Price = 1.5M};

            //Act
            var vehicleOffer = _mapper.Map(command);

            //Assert
            Assert.Equal(command.ModelKey, vehicleOffer.ModelKey.Value);
            Assert.Equal(command.CatalogId, vehicleOffer.CatalogId);
            Assert.Equal(command.Price, vehicleOffer.Price);
        }
    }
}
