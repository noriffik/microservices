﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.VehicleOffers
{
    public class AddCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IVehicleOfferService> _service;
        private readonly Mock<IUnitOfWork> _work;

        //Entity
        private readonly VehicleOffer _vehicleOffer;

        //Handler and command
        private readonly AddCommandHandler _handler;
        private readonly AddCommand _command;

        public AddCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _vehicleOffer = fixture.Create<VehicleOffer>();

            //Command
            _command = fixture.Create<AddCommand>();

            //Dependencies
            _service = new Mock<IVehicleOfferService>();
            _work = new Mock<IUnitOfWork>();

            //Mapper
            var commandMapper = new Mock<ICommandMapper<AddCommand, VehicleOffer>>();
            commandMapper.Setup(m => m.Map(_command)).Returns(_vehicleOffer);

            //Handler and command
            _handler = new AddCommandHandler(_service.Object, _work.Object)
            {
                CommandMapper = commandMapper.Object
            };
        }

        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(AddCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.Add(_vehicleOffer))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_vehicleOffer.Id, result);
            }
        }

        [Fact]
        public async Task Handle_WhenEntity_IsNotFound_Throws()
        {
            //Arrange
            _service.Setup(s => s.Add(_vehicleOffer))
                .ThrowsAsync(new InvalidEntityIdException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                string.Empty, () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }

        [Fact]
        public async Task Handle_GivenModelKey_IsAlreadyInCatalog_Throws()
        {
            //Arrange
            var inner = new DuplicateVehicleOfferException(_vehicleOffer.CatalogId, _vehicleOffer.ModelKey);
            _service.Setup(s => s.Add(_vehicleOffer))
                .ThrowsAsync(inner);

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.ModelKey), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage(inner.Message).WithValue(_vehicleOffer.ModelKey);
        }

        [Fact]
        public void Mapper()
        {
            //Arrange
            var handler = new AddCommandHandler(_service.Object, _work.Object);

            //Assert
            Assert.Injection.OfProperty(handler, nameof(handler.CommandMapper))
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}
