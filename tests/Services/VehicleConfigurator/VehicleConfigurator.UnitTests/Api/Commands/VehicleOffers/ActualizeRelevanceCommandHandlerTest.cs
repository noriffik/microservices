﻿using AutoFixture;
using Moq;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NexCore.Common;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.VehicleOffers
{
    public class ActualizeRelevanceCommandHandlerTest
    {
        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ActualizeRelevanceCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var date = fixture.Create<DateTime>();

            //Setup VehicleOffers
            var vehicleOffers = fixture.CreateMany<VehicleOffer>(3).ToList();

            var irrelevantPeriod = new RelevancePeriod(date.AddDays(-2), date.AddDays(-1));
            var relevantPeriod = new RelevancePeriod(date, date.AddDays(1));

            vehicleOffers.Take(2).ToList().ForEach(v => v.ChangeRelevancePeriod(irrelevantPeriod));
            vehicleOffers.TakeLast(1).Single().ChangeRelevancePeriod(relevantPeriod);

            //Dependencies
            var work = new Mock<IUnitOfWork>();
            var repository = new Mock<IEntityRepository>();
            work.Setup(w => w.EntityRepository).Returns(repository.Object);

            //Command and Handler
            var command = fixture.Build<ActualizeRelevanceCommand<VehicleOffer>>()
                .With(c => c.CatalogId, fixture.Create<int>())
                .Create();

            var handler = new ActualizeRelevanceCommandHandler(work.Object);

            repository.Setup(r =>
                    r.Find(It.Is<VehicleOfferCatalogIdSpecification>(s => s.CatalogId == command.CatalogId), It.IsAny<CancellationToken>()))
                .ReturnsAsync(vehicleOffers);

            using (new SystemTimeContext(date))
            {
                //Act
                await handler.Handle(command, CancellationToken.None);
            }

            //Assert
            Assert.True(vehicleOffers.Take(2).All(v => v.Relevance == Relevance.Irrelevant));
            Assert.True(vehicleOffers.Last().Relevance == Relevance.Relevant);
        }

    }
}
