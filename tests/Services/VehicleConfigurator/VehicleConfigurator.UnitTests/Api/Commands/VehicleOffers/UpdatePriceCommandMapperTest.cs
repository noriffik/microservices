﻿using AutoFixture;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.VehicleOffers
{
    public class UpdatePriceCommandMapperTest
    {
        private readonly UpdatePriceCommandMapper _mapper = new UpdatePriceCommandMapper();
        private readonly UpdatePriceCommand _command;
        private readonly VehicleOffer _vehicleOffer;

        public UpdatePriceCommandMapperTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _vehicleOffer = fixture.Create<VehicleOffer>();
            _command = fixture.Build<UpdatePriceCommand>()
                .With(c => c.Id, _vehicleOffer.Id)
                .Create();
        }

        [Fact]
        public void MapId()
        {
            //Act
            var result = _mapper.MapId(_command);

            //Assert
            Assert.Equal(_command.Id, result);
        }

        [Fact]
        public void MapId_WhenGivenCommand_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("command", () => _mapper.MapId(null));
        }

        [Fact]
        public void Map()
        {
            //Act
            _mapper.Map(_command, _vehicleOffer);

            //Assert
            Assert.Equal(_command.Price, _vehicleOffer.Price);
        }

        [Fact]
        public void Map_WhenGivenCommand_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("command", () => 
                _mapper.Map(null, _vehicleOffer));
        }

        [Fact]
        public void Map_WhenGivenVehicleOffer_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("vehicleOffer", () => 
                _mapper.Map(_command, null));
        }
    }
}
