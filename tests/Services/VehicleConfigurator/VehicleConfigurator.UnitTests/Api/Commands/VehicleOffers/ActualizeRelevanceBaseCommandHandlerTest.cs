﻿using AutoFixture;
using Moq;
using Moq.Protected;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Testing;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NexCore.Common;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.VehicleOffers
{
    public class ActualizeRelevanceBaseCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly DateTime _date;
        private readonly IReadOnlyCollection<Catalog> _catalogs;
        private readonly IReadOnlyCollection<VehicleOffer> _vehicleOffers;

        //Commands and handler
        private readonly ActualizeRelevanceCommand<TestAggregateRoot> _commandWithoutCatalogId;
        private readonly ActualizeRelevanceCommand<TestAggregateRoot> _commandWithCatalogId;
        private readonly Mock<ActualizeRelevanceBaseCommandHandler<TestAggregateRoot>> _handler;
        private readonly Mock<IUnitOfWork> _work;

        public ActualizeRelevanceBaseCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entities
            _date = fixture.Create<DateTime>();
            _catalogs = Enumerable.Range(1, 3).Select(id => fixture.Construct<Catalog>(id)).ToList();

            _vehicleOffers = fixture.CreateMany<VehicleOffer>(3).ToList();

            //Commands and handler
            _commandWithoutCatalogId = fixture.Build<ActualizeRelevanceCommand<TestAggregateRoot>>()
                .With(c => c.CatalogId, null as int?)
                .Create();
            _commandWithCatalogId = fixture.Build<ActualizeRelevanceCommand<TestAggregateRoot>>()
                .With(c => c.CatalogId, fixture.Create<int>())
                .Create();

            _handler = new Mock<ActualizeRelevanceBaseCommandHandler<TestAggregateRoot>>(_work.Object)
            {
                CallBase = true
            };
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Object.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_WithoutCatalogId()
        {
            using (new SystemTimeContext(_date))
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.Find(It.IsAny<RelevantPublishedCatalogSpecification>(), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_catalogs);
                _repository.Setup(r =>
                        r.Find(It.Is<VehicleOfferCatalogIdSpecification>(s => s.CatalogId == _catalogs.First().Id), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffers);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Object.Handle(_commandWithoutCatalogId, CancellationToken.None);

                //Assert
                Assert.All(_vehicleOffers, vehicleOffer => _handler.Protected()
                    .Verify("ActualizePotentiallyIrrelevant", Times.Once(), vehicleOffer, _date));
            }
        }

        [Fact]
        public async Task Handle_WithCatalogId()
        {
            using (new SystemTimeContext(_date))
            using (Sequence.Create())
            {
                //Arrange
                _repository.Setup(r => r.HasRequired<Catalog>(_commandWithCatalogId.CatalogId.Value, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r =>
                        r.Find(It.Is<VehicleOfferCatalogIdSpecification>(s => s.CatalogId == _commandWithCatalogId.CatalogId.Value), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_vehicleOffers);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Object.Handle(_commandWithCatalogId, CancellationToken.None);

                //Assert
                Assert.All(_vehicleOffers, vehicleOffer => _handler.Protected()
                    .Verify("ActualizePotentiallyIrrelevant", Times.Once(), vehicleOffer, _date));
            }
        }
    }
}
