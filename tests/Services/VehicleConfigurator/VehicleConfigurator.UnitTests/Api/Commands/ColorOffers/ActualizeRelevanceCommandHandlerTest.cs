﻿using AutoFixture;
using Moq;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NexCore.Common;
using Xunit;
using ActualizeRelevanceCommandHandler = NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers.ActualizeRelevanceCommandHandler;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.ColorOffers
{
    public class ActualizeRelevanceCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly DateTime _date;
        private readonly IReadOnlyCollection<VehicleOffer> _vehicleOffers;
        private readonly IReadOnlyCollection<ColorOffer> _relevantColorOffers;
        private readonly IReadOnlyCollection<ColorOffer> _irrelevantColorOffers;

        //Commands and handler
        private readonly ActualizeRelevanceCommand<ColorOffer> _command;
        private ActualizeRelevanceCommandHandler _handler;


        public ActualizeRelevanceCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            var work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entities
            _date = fixture.Create<DateTime>();
            _vehicleOffers = fixture.CreateMany<VehicleOffer>(3).ToList();

            //Create PrNumbers
            var relevantColorIds = fixture.CreateMany<ColorId>(3).ToList();
            var irrelevantColorIds = fixture.CreateMany<ColorId>(3).ToList();

            //Add ColorOffers to VehicleOffer
            var colorOffers = new List<ColorOffer>();
            foreach (var colorId in relevantColorIds.Concat(irrelevantColorIds))
            {
                var colorOffer = _vehicleOffers.First().AddColorOffer(colorId, fixture.Create<decimal>());
                colorOffers.Add(colorOffer);
            }

            //Setup ColorOffers
            _relevantColorOffers = colorOffers.Where(c => relevantColorIds.Contains(c.ColorId)).ToList();
            foreach (var colorOffer in _relevantColorOffers)
                colorOffer.ChangeRelevance(Relevance.Irrelevant);

            _irrelevantColorOffers = colorOffers.Where(c => irrelevantColorIds.Contains(c.ColorId)).ToList();
            foreach (var colorOffer in _irrelevantColorOffers)
                colorOffer.ChangeRelevancePeriod(new RelevancePeriod(_date.AddDays(3)));

            //Commands and handler
            _command = fixture.Build<ActualizeRelevanceCommand<ColorOffer>>()
                .With(c => c.CatalogId, fixture.Create<int>())
                .Create();
            _handler = new ActualizeRelevanceCommandHandler(work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ActualizeRelevanceCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            _repository.Setup(r =>
                    r.Find(It.Is<VehicleOfferCatalogIdSpecification>(s => s.CatalogId == _command.CatalogId.Value), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_vehicleOffers);

            //Act
            using (new SystemTimeContext(_date))
            {
                await _handler.Handle(_command, CancellationToken.None);
            }

            //Assert
            Assert.All(_relevantColorOffers, o => Assert.Equal(Relevance.Relevant, o.Relevance));
            Assert.All(_irrelevantColorOffers, o => Assert.Equal(Relevance.Irrelevant, o.Relevance));
        }
    }
}
