﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers;
using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.ColorOffers
{
    public class ChangeColorOfferRelevancePeriodCommandHandlerTest
    {
        //Entities
        private readonly RelevancePeriod _relevancePeriod;
        private readonly ColorId _colorId;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IVehicleOfferService> _service;

        //Commands and handler
        private readonly ResetColorOfferRelevancePeriodCommand _resetCommand;
        private readonly ChangeColorOfferRelevancePeriodCommand _changeCommand;
        private readonly ChangeRelevancePeriodCommandHandler _handler;

        public ChangeColorOfferRelevancePeriodCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entities
            _colorId = fixture.Create<ColorId>();
            _relevancePeriod = new RelevancePeriod(fixture.Create<DateTime>());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IVehicleOfferService>();

            //Commands and handler
            var relevancePeriod = fixture.Build<PeriodModel>()
                .With(m => m.From, _relevancePeriod.From)
                .With(m => m.To, _relevancePeriod.To)
                .Create();
            _changeCommand = fixture.Build<ChangeColorOfferRelevancePeriodCommand>()
                .With(c => c.ColorId, _colorId.Value)
                .With(c => c.RelevancePeriod, relevancePeriod)
                .Create();
            _resetCommand = fixture.Build<ResetColorOfferRelevancePeriodCommand>()
                .With(c => c.ColorId, _colorId.Value)
                .Create();
            _handler = new ChangeRelevancePeriodCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ChangeRelevancePeriodCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_ChangeRelevancePeriod_GivenCommand_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                _handler.Handle(null as ChangeColorOfferRelevancePeriodCommand, CancellationToken.None));
        }

        [Fact]
        public Task Handle_ResetRelevancePeriod_GivenCommand_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                _handler.Handle(null as ChangeColorOfferRelevancePeriodCommand, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_ChangeRelevancePeriod()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.ChangeColorOfferRelevancePeriod(_changeCommand.VehicleOfferId, _colorId, _relevancePeriod))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_changeCommand, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_ChangeRelevancePeriod_WhenColorOffer_IsNotFound_Throws()
        {
            //Arrange
            _service.Setup(s => s.ChangeColorOfferRelevancePeriod(_changeCommand.VehicleOfferId, _colorId, _relevancePeriod))
                .Throws(new ColorOfferNotFoundException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_changeCommand.ColorId), () => _handler.Handle(_changeCommand, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }

        [Fact]
        public async Task Handle_ResetRelevancePeriod()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.ChangeColorOfferRelevancePeriod(_resetCommand.VehicleOfferId, _colorId, RelevancePeriod.Empty))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_resetCommand, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_ResetRelevancePeriod_WhenColorOffer_IsNotFound_Throws()
        {
            //Arrange
            _service.Setup(s => s.ChangeColorOfferRelevancePeriod(_resetCommand.VehicleOfferId, _colorId, RelevancePeriod.Empty))
                .Throws(new ColorOfferNotFoundException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_resetCommand.ColorId), () => _handler.Handle(_resetCommand, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }
    }
}
