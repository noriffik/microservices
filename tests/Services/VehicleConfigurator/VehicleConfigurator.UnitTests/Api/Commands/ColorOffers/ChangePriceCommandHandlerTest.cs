﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.ColorOffers
{
    public class ChangePriceCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IVehicleOfferService> _service;
        private readonly Mock<IUnitOfWork> _work;

        //Entity
        private readonly ColorId _colorId;

        //Command and handler
        private readonly ChangePriceCommand _command;
        private readonly ChangePriceCommandHandler _handler;

        public ChangePriceCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IVehicleOfferService>();

            //Entities
            _colorId = fixture.Create<ColorId>();

            //Command and handler
            _command = fixture.Build<ChangePriceCommand>()
                .With(c => c.ColorId, _colorId.Value)
                .Create();
            _handler = new ChangePriceCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ChangePriceCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.ChangeColorOfferPrice(_command.VehicleOfferId, _colorId, _command.Price))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_WhenColorOffer_IsNotFound_Throws()
        {
            //Arrange
            _service.Setup(s => s.ChangeColorOfferPrice(_command.VehicleOfferId, _colorId, _command.Price))
                .ThrowsAsync(new ColorOfferNotFoundException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.ColorId), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }
    }
}
