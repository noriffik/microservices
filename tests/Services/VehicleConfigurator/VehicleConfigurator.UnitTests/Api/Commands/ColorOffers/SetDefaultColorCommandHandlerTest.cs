﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.ColorOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.ColorOffers
{
    public class SetDefaultColorCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IVehicleOfferService> _service;
        private readonly Mock<IUnitOfWork> _work;

        //Entity
        private readonly ColorId _colorId;

        //Handler and command
        private readonly SetDefaultColorCommandHandler _handler;
        private readonly SetDefaultColorCommand _command;

        public SetDefaultColorCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _service = new Mock<IVehicleOfferService>();
            _work = new Mock<IUnitOfWork>();

            //Entity
            _colorId = fixture.Create<ColorId>();

            //Command
            _command = fixture.Build<SetDefaultColorCommand>()
                .With(c => c.ColorId, _colorId.Value)
                .Create();

            //Handler
            _handler = new SetDefaultColorCommandHandler(_service.Object, _work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(SetDefaultColorCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.SetDefaultColor(_command.VehicleOfferId, _colorId))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_WhenColorOffer_ForDefaultColor_IsNotFound_Throws()
        {
            //Arrange
            _service.Setup(s => s.SetDefaultColor(_command.VehicleOfferId, _colorId))
                .ThrowsAsync(new DefaultColorViolationException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.ColorId), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }
    }
}
