﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.Common;
using NexCore.DealerNetwork;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Legal;
using NexCore.Testing;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts;
using NexCore.VehicleConfigurator.Api.Infrastructure.AutoMapper;
using NexCore.VehicleConfigurator.Domain.Contracts;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Domain.Customers;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.Domain.Offers;
using NexCore.VehicleConfigurator.Domain.Pricing;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Contracts;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Dealers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Contracts.VehicleSales
{
    public class AddCommandHandlerTest
    {
        //Entity
        private readonly VehicleSaleContract _contract;
        private readonly Offer _offer;
        private readonly Model _model;
        private readonly Color _color;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly DateTime _date;
        private readonly PropertyComparer _comparer;

        //Command and Handler
        private readonly AddCommand _command;
        private readonly AddWithOfferCommand _withOfferCommand;
        private readonly AddCommandHandler _handler;

        public AddCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());
            fixture.Customize(new DealerIdCustomization());
            fixture.Customize(new ContractTypeIdCustomization());

            _comparer = new PropertyComparer { Options = { CollectionComparison = CollectionComparison.Unordered } };

            _date = DateTime.UtcNow;

            var mapperConfig = new MapperConfiguration(
                c => c.AddProfiles(typeof(DefaultProfile).Assembly));
            var mapper = mapperConfig.CreateMapper();

            //Entity
            _model = fixture.Create<Model>();
            _color = fixture.Create<Color>();
            _offer = fixture.Create<Offer>();

            var typeId = fixture.Create<ContractTypeId>();

            var content = fixture.Construct<VehicleSaleContent>(new
            {
                price = new TotalPrice(mapper.Map<Price>(_offer.Price.Total)),
                vehicle = new VehicleSpecification(_offer),
                salesManagerName = new LegalPersonName("John", "Doe", "Michelson"),
                vehicleModelName = _model.Name,
                vehicleColorName = _color.Name
            });

            _contract = new VehicleSaleContract(typeId, content, 1);

            //Command
            _command = fixture.Build<AddCommand>()
                .With(c => c.Number, _contract.Number)
                .With(c => c.ContractTypeId, _contract.TypeId.Value)
                .With(c => c.TotalPrice, mapper.Map<TotalPriceModel>(_contract.Content.Price))
                .With(c => c.Payment, mapper.Map<IEnumerable<PriceModel>>(_contract.Content.Payment))
                .With(c => c.Vehicle,  mapper.Map<VehicleSpecificationModel>(_contract.Content.Vehicle))
                .With(c => c.VehicleStockAddress, content.VehicleStockAddress)
                .With(c => c.VehicleDeliveryDays, content.VehicleDeliveryDays)
                .With(c => c.PrivateCustomerId, _contract.PrivateCustomerId)
                .With(c => c.SalesManagerId, _contract.SalesManagerId)
                .With(c => c.DealerId, _contract.DealerId.Value)
                .With(c => c.DealersCity, content.DealersCity)
                .With(c => c.DealersHeadPosition, content.DealersHeadPosition)
                .Create();

            _withOfferCommand = fixture.Build<AddWithOfferCommand>()
                .With(c => c.Number, _contract.Number)
                .With(c => c.ContractTypeId, _contract.TypeId.Value)
                .With(c => c.Payment, mapper.Map<IEnumerable<PriceModel>>(_contract.Content.Payment))
                .With(c => c.OfferId, _offer.Id)
                .With(c => c.VehicleStockAddress, content.VehicleStockAddress)
                .With(c => c.VehicleDeliveryDays, content.VehicleDeliveryDays)
                .With(c => c.PrivateCustomerId, _contract.PrivateCustomerId)
                .With(c => c.SalesManagerId, _contract.SalesManagerId)
                .With(c => c.DealerId, _contract.DealerId.Value)
                .With(c => c.DealersCity, content.DealersCity)
                .With(c => c.DealersHeadPosition, content.DealersHeadPosition)
                .Create();

            //Dependencies
            _work = new Mock<IUnitOfWork>();

            //Handler
            _handler = new AddCommandHandler(_work.Object, mapper);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Trows()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null as AddCommand, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_WithOffer_GivenRequest_IsNull_Trows()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null as AddWithOfferCommand, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            using (new SystemTimeContext(_date))
            {
                //Arrange
                _work.Setup(w => w.EntityRepository.HasRequired<ContractType, ContractTypeId>(
                        ContractTypeId.Parse(_command.ContractTypeId), CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.EntityRepository.Require<Dealer, DealerId>(DealerId.Parse(_command.DealerId), CancellationToken.None))
                    .InSequence()
                    .ReturnsAsync(_contract.Content.Dealer);

                _work.Setup(w => w.EntityRepository.Require<PrivateCustomer>(_command.PrivateCustomerId, CancellationToken.None))
                    .InSequence()
                    .ReturnsAsync(_contract.Content.PrivateCustomer);

                _work.Setup(w => w.EntityRepository.Require<Model, ModelId>(_offer.ModelId, CancellationToken.None))
                    .InSequence()
                    .ReturnsAsync(_model);

                _work.Setup(w => w.EntityRepository.Require<Color, ColorId>(ColorId.Parse(_command.Vehicle.ColorId), CancellationToken.None))
                    .InSequence()
                    .ReturnsAsync(_color);

                _work.Setup(w => w.EntityRepository.Add(It.Is<VehicleSaleContract>(v => _comparer.Equals(v, _contract))))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_contract.Id, result);
            }
        }

        [Fact]
        public async Task Handle_WithOffer()
        {
            using (Sequence.Create())
            using (new SystemTimeContext(_date))
            {
                //Arrange
                _work.Setup(w => w.EntityRepository.HasRequired<ContractType, ContractTypeId>(ContractTypeId.Parse(_withOfferCommand.ContractTypeId), CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.EntityRepository.Require<Dealer, DealerId>(DealerId.Parse(_withOfferCommand.DealerId), CancellationToken.None))
                    .InSequence()
                    .ReturnsAsync(_contract.Content.Dealer);

                _work.Setup(w => w.EntityRepository.Require<PrivateCustomer>(_withOfferCommand.PrivateCustomerId, CancellationToken.None))
                    .InSequence()
                    .ReturnsAsync(_contract.Content.PrivateCustomer);

                _work.Setup(w => w.EntityRepository.Require<Offer>(_withOfferCommand.OfferId, CancellationToken.None))
                    .InSequence()
                    .ReturnsAsync(_offer);

                _work.Setup(w => w.EntityRepository.Require<Model, ModelId>(_offer.ModelId, CancellationToken.None))
                    .InSequence()
                    .ReturnsAsync(_model);

                _work.Setup(w => w.EntityRepository.Require<Color, ColorId>(ColorId.Parse(_command.Vehicle.ColorId), CancellationToken.None))
                    .InSequence()
                    .ReturnsAsync(_color);

                _work.Setup(w => w.EntityRepository.Add(It.Is<VehicleSaleContract>(v => _comparer.Equals(v, _contract))))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_withOfferCommand, CancellationToken.None);

                //Assert
                Assert.Equal(_contract.Id, result);
            }
        }
    }
}
