﻿using AutoFixture;
using AutoMapper;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts;
using NexCore.VehicleConfigurator.Api.Infrastructure.AutoMapper;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Contracts;
using NexCore.VehicleConfigurator.Domain.Pricing;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Contracts
{
    public class MapperTest
    {
        private readonly IMapper _mapper;

        public MapperTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var mapperConfig = new MapperConfiguration(
                c => c.AddProfiles(typeof(DefaultProfile).Assembly));
            _mapper = mapperConfig.CreateMapper();
        }

        [Fact]
        public void Map_TotalPrice()
        {
            //Arrange
            var incoming = new TotalPriceModel
            {
                WithoutTax = new PriceModel
                {
                    Base = 10,
                    Retail = 100
                },
                WithTax = new PriceModel
                {
                    Base = 12,
                    Retail = 120
                },
                Tax = new PriceModel
                {
                    Base = 2,
                    Retail = 20
                }
            };

            var expected = new TotalPrice(new Price(10, 100), new Price(12, 120), new Price(2, 20));

            //Act
            var actual = _mapper.Map<TotalPrice>(incoming);
            
            //Assert
            Assert.Equal(expected, actual, PropertyComparer<TotalPrice>.Instance);
        }

        [Fact]
        public void Map_PriceModel()
        {
            //Arrange
            var incoming = new PriceModel
                {
                    Base = 10,
                    Retail = 100
                };

            var expected = new Price(10, 100);

            //Act
            var actual = _mapper.Map<Price>(incoming);
            
            //Assert
            Assert.NotNull(actual);
            Assert.Equal(expected, actual, PropertyComparer<Price>.Instance);
        }

        [Fact]
        public void Map_Vehicle()
        {
            //Arrange
            var incoming = new VehicleSpecificationModel
            {
                ModelYear = 1984,
                ModelKey = "AA1349",
                ColorId = "A123",
                AdditionalOptions = "AAA BBB",
                IncludedOptions = "CCC DDD",
                PackageId = "AA123"
            };

            var expected = new VehicleSpecification(new Year(1984), ModelKey.Parse("AA1349"), ColorId.Parse("A123"))
            {
                PackageId = PackageId.Parse("AA123"),
                AdditionalOptions = PrNumberSet.Parse("AAA BBB"),
                IncludedOptions = PrNumberSet.Parse("CCC DDD")
            };

            //Act
            var actual = _mapper.Map<VehicleSpecification>(incoming);
            
            //Assert
            Assert.Equal(expected, actual, PropertyComparer<VehicleSpecification>.Instance);
        }
    }
}
