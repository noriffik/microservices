﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.OptionOffers
{
    public class UpdateAvailabilityCommandHandlerTest
    {
        //Entity
        private readonly VehicleOffer _vehicleOffer;
        private readonly OptionOffer _optionOffer;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IOptionOfferService> _service;

        //Handler and command
        private readonly UpdateAvailabilityCommandHandler _handler;
        private readonly UpdateAvailabilityCommand _command;

        public UpdateAvailabilityCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _vehicleOffer = fixture.Create<VehicleOffer>();
            _optionOffer = fixture.Create<OptionOffer>();
            
            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IOptionOfferService>();
            var mapper = new Mock<ITwoWayMapper<Availability, AvailabilityModel>>();

            //Handler and command
            _handler = new UpdateAvailabilityCommandHandler(_work.Object, _service.Object)
            {
                AvailabilityMapper = mapper.Object
            };
            _command = new UpdateAvailabilityCommand
            {
                VehicleOfferId = _vehicleOffer.Id,
                PrNumber = _optionOffer.PrNumber.Value,
                Availability = fixture.Create<AvailabilityModel>()
            };

            //Setup dependencies
            mapper.Setup(m => m.MapReverse(_command.Availability)).Returns(_optionOffer.Availability);
        }

        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(UpdateAvailabilityCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange               
                _service.Setup(s => s.UpdateAvailability(_command.VehicleOfferId, _optionOffer.PrNumber, _optionOffer.Availability))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_WhenVehicleOffer_IsNotFound_Throws()
        {
            //Arrange
            var inner = new InvalidEntityIdException(_vehicleOffer.GetType(), _vehicleOffer.Id);
            _service.Setup(s => s.UpdateAvailability(_vehicleOffer.Id, _optionOffer.PrNumber, _optionOffer.Availability))
                .Throws(inner);
           
            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.VehicleOfferId), () => _handler.Handle(_command, CancellationToken.None));
            
            //Assert
            e.WithMessage(inner.Message).WithValue(_command.VehicleOfferId);
        }

        [Fact]
        public async Task Handle_WhenOptionOffer_IsNotFound_Throws()
        {
            //Arrange
            var inner = new OptionOfferNotFoundException();
            _service.Setup(s => s.UpdateAvailability(_vehicleOffer.Id, _optionOffer.PrNumber, _optionOffer.Availability))
                .Throws(inner);

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.PrNumber), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage(inner.Message).WithValue(_command.PrNumber);
        }
        
        [Fact]
        public void Mapper()
        {
            //Arrange
            var handler = new UpdateAvailabilityCommandHandler(_work.Object, _service.Object);

            //Assert
            Assert.Injection.OfProperty(handler, nameof(handler.AvailabilityMapper))
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}
