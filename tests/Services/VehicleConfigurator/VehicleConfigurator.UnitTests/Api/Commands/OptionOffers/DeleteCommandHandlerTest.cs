﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.OptionOffers
{
    public class DeleteCommandHandlerTest
    {
        //Entities
        private readonly VehicleOffer _vehicleOffer;
        private readonly OptionOffer _optionOffer;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IOptionOfferService> _service;

        //Handler and command
        private readonly DeleteCommandHandler _handler;
        private readonly DeleteCommand _command;

        public DeleteCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _vehicleOffer = fixture.Create<VehicleOffer>();
            _optionOffer = fixture.Create<OptionOffer>();
            
            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IOptionOfferService>();

            //Handler and command
            _handler = new DeleteCommandHandler(_work.Object, _service.Object);
            _command = new DeleteCommand
            {
                VehicleOfferId = _vehicleOffer.Id,
                PrNumber = _optionOffer.PrNumber.Value
            };
        }

        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(DeleteCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange               
                _service.Setup(s => s.Delete(_command.VehicleOfferId, _optionOffer.PrNumber))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }
        
        [Fact]
        public async Task Handle_WhenVehicleOffer_DoesNotExist_Throws()
        {
            //Arrange
            _service.Setup(s => s.Delete(_vehicleOffer.Id, _optionOffer.PrNumber))
                .ThrowsAsync(new InvalidEntityIdException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                string.Empty, () => _handler.Handle(_command, CancellationToken.None));
            
            //Assert
            e.WithMessage("test");
        }

        [Fact]
        public async Task Handle_WhenOptionOffer_DoesNotExist_DoesNothing()
        {
            //Arrange
            _service.Setup(s => s.Delete(_vehicleOffer.Id, _optionOffer.PrNumber))
                .ThrowsAsync(new OptionOfferNotFoundException("test"));

            //Act
            await _handler.Handle(_command, CancellationToken.None);

            //Assert
            Assert.True(true);
        }
    }
}
