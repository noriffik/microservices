﻿using AutoFixture;
using Moq;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NexCore.Common;
using Xunit;
using ActualizeRelevanceCommandHandler = NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers.ActualizeRelevanceCommandHandler;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.OptionOffers
{
    public class ActualizeRelevanceCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly DateTime _date;
        private readonly IReadOnlyCollection<VehicleOffer> _vehicleOffers;
        private readonly IReadOnlyCollection<OptionOffer> _relevantOptionOffers;
        private readonly IReadOnlyCollection<OptionOffer> _irrelevantOptionOffers;

        //Commands and handler
        private readonly ActualizeRelevanceCommand<OptionOffer> _command;
        private readonly ActualizeRelevanceCommandHandler _handler;

        public ActualizeRelevanceCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            var work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entities
            _date = fixture.Create<DateTime>();
            _vehicleOffers = fixture.CreateMany<VehicleOffer>(3).ToList();

            //Create PrNumbers
            var relevantPrNumbers = fixture.CreateMany<PrNumber>(3).ToList();
            var irrelevantPrNumbers = fixture.CreateMany<PrNumber>(3).ToList();

            //Add OptionOffers to VehicleOffer
            var optionOffers = new List<OptionOffer>();
            foreach (var prNumber in relevantPrNumbers.Concat(irrelevantPrNumbers))
            {
                var optionOffer = _vehicleOffers.First().AddOptionOffer(prNumber, fixture.Create<Availability>());
                optionOffers.Add(optionOffer);
            }

            //Setup OptionOffers
            _relevantOptionOffers = optionOffers.Where(o => relevantPrNumbers.Contains(o.PrNumber)).ToList();
            foreach (var optionOffer in _relevantOptionOffers)
                optionOffer.ChangeRelevance(Relevance.Irrelevant);

            _irrelevantOptionOffers = optionOffers.Where(o => irrelevantPrNumbers.Contains(o.PrNumber)).ToList();
            foreach (var optionOffer in _irrelevantOptionOffers)
                optionOffer.ChangeRelevancePeriod(new RelevancePeriod(_date.AddDays(3)));

            //Commands and handler
            _command = fixture.Build<ActualizeRelevanceCommand<OptionOffer>>()
                .With(c => c.CatalogId, fixture.Create<int>())
                .Create();
            _handler = new ActualizeRelevanceCommandHandler(work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ActualizeRelevanceCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            _repository.Setup(r =>
                    r.Find(It.Is<VehicleOfferCatalogIdSpecification>(s => s.CatalogId == _command.CatalogId.Value), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_vehicleOffers);

            //Act
            using (new SystemTimeContext(_date))
            {
                await _handler.Handle(_command, CancellationToken.None);
            }

            //Assert
            Assert.All(_relevantOptionOffers, o => Assert.Equal(Relevance.Relevant, o.Relevance));
            Assert.All(_irrelevantOptionOffers, o => Assert.Equal(Relevance.Irrelevant, o.Relevance));
        }
    }
}
