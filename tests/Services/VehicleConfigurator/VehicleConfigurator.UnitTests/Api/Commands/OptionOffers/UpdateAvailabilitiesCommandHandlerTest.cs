﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.OptionOffers
{
    public class UpdateAvailabilitiesCommandHandlerTest
    {
        //Entity
        private readonly VehicleOffer _vehicleOffer;
        private readonly OptionOffer _optionOffer;
        private readonly BoundModelKeyDictionary<Availability> _optionOffers;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IOptionOfferService> _service;

        //Handler and command
        private readonly UpdateAvailabilitiesCommandHandler _handler;
        private readonly UpdateAvailabilitiesCommand _command;

        public UpdateAvailabilitiesCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _vehicleOffer = fixture.Create<VehicleOffer>();
            _optionOffer = fixture.Create<OptionOffer>();
            var offerModel = new AvailabilityPerModelKeyModel
            {
                ModelKey = _vehicleOffer.ModelKey.Value,
                Availability = fixture.Create<AvailabilityModel>()
            };
            _optionOffers = new BoundModelKeyDictionary<Availability>(
                new[]
                {
                    new KeyValuePair<ModelKey, Availability>(
                        _vehicleOffer.ModelKey, _optionOffer.Availability)
                });

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IOptionOfferService>();
            var mapper = new Mock<ITwoWayMapper<Availability, AvailabilityModel>>();

            //Handler and command
            _handler = new UpdateAvailabilitiesCommandHandler(_work.Object, _service.Object)
            {
                AvailabilityMapper = mapper.Object
            };
            _command = new UpdateAvailabilitiesCommand
            {
                PrNumber = _optionOffer.PrNumber.Value,
                Offers = new[] { offerModel }
            };

            //Setup dependencies
            mapper.Setup(m => m.MapReverse(offerModel.Availability)).Returns(_optionOffer.Availability);
        }

        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(UpdateAvailabilitiesCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange               
                _service.Setup(s => s.UpdateAvailabilities(_optionOffer.PrNumber, _optionOffers))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_WhenVehicleOffer_IsNotFound_Throws()
        {
            //Arrange
            _service.Setup(s => s.UpdateAvailabilities(_optionOffer.PrNumber, _optionOffers))
                .Throws(new InvalidEntityIdException(typeof(VehicleOffer), _vehicleOffer.Id));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                "VehicleOfferId", () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithValue(_vehicleOffer.Id);
        }

        [Fact]
        public async Task Handle_WhenOptionOffers_AreNotFor_SameModel_Throws()
        {
            //Arrange
            _service.Setup(s => s.UpdateAvailabilities(_optionOffer.PrNumber, _optionOffers))
                .Throws(new SingleModelViolationException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.Offers), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }

        [Fact]
        public void Mapper()
        {
            //Arrange
            var handler = new UpdateAvailabilitiesCommandHandler(_work.Object, _service.Object);

            //Assert
            Assert.Injection.OfProperty(handler, nameof(handler.AvailabilityMapper))
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}
