﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.OptionOffers
{
    public class AddCommandHandlerTest
    {
        //Entities
        private readonly VehicleOffer _vehicleOffer;
        private readonly OptionOffer _optionOffer;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IOptionOfferService> _service;

        //Handler and command
        private readonly AddCommandHandler _handler;
        private readonly AddCommand _command;

        public AddCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entities
            _vehicleOffer = fixture.Create<VehicleOffer>();
            _optionOffer = fixture.Create<OptionOffer>();

            //Command
            var availability = fixture.Create<AvailabilityModel>();
            _command = new AddCommand
            {
                VehicleOfferId = _vehicleOffer.Id,
                PrNumber = _optionOffer.PrNumber.Value,
                Availability = availability
            };

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IOptionOfferService>();
            var mapper = new Mock<ITwoWayMapper<Availability, AvailabilityModel>>();

            //Handler and command
            _handler = new AddCommandHandler(_work.Object, _service.Object)
            {
                AvailabilityMapper = mapper.Object
            };

            //Setup dependencies
            mapper.Setup(m => m.MapReverse(_command.Availability)).Returns(_optionOffer.Availability);
        }

        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(AddCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.Add(
                        _vehicleOffer.Id, _optionOffer.PrNumber, _optionOffer.Availability, default(Inclusion)))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_WhenVehicleOffer_DoesNotExist_Throws()
        {
            //Arrange
            _service.Setup(s => s.Add(_vehicleOffer.Id, _optionOffer.PrNumber, _optionOffer.Availability, default(Inclusion)))
                .ThrowsAsync(new InvalidEntityIdException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(string.Empty, () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }

        [Fact]
        public async Task Handle_WhenOptionOffer_AlreadyExists_Throws()
        {
            //Arrange
            var inner = new DuplicateOptionOfferException(_vehicleOffer.ModelKey, _optionOffer.PrNumber);
            _service.Setup(s => s.Add(_vehicleOffer.Id, _optionOffer.PrNumber, _optionOffer.Availability, default(Inclusion)))
                .ThrowsAsync(inner);

            //Assert
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.PrNumber), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage(inner.Message).WithValue(inner.PrNumber);
        }

        [Fact]
        public void Mapper()
        {
            //Arrange
            var handler = new AddCommandHandler(_work.Object, _service.Object);

            //Assert
            Assert.Injection.OfProperty(handler, nameof(handler.AvailabilityMapper))
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}
