﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.OptionOffers
{
    public class ChangeOptionOfferRelevancePeriodCommandHandlerTest
    {
        //Entities
        private readonly RelevancePeriod _relevancePeriod;
        private readonly PrNumber _prNumber;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IOptionOfferService> _service;

        //Commands and handler
        private readonly ResetOptionOfferRelevancePeriodCommand _resetCommand;
        private readonly ChangeOptionOfferRelevancePeriodCommand _changeCommand;
        private readonly ChangeOptionOfferRelevancePeriodCommandHandler _handler;

        public ChangeOptionOfferRelevancePeriodCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entities
            _prNumber = fixture.Create<PrNumber>();
            _relevancePeriod = new RelevancePeriod(fixture.Create<DateTime>());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IOptionOfferService>();

            //Commands and handler
            var relevancePeriod = fixture.Build<PeriodModel>()
                .With(m => m.From, _relevancePeriod.From)
                .With(m => m.To, _relevancePeriod.To)
                .Create();
            _changeCommand = fixture.Build<ChangeOptionOfferRelevancePeriodCommand>()
                .With(c => c.PrNumber, _prNumber.Value)
                .With(c => c.RelevancePeriod, relevancePeriod)
                .Create();
            _resetCommand = fixture.Build<ResetOptionOfferRelevancePeriodCommand>()
                .With(c => c.PrNumber, _prNumber.Value)
                .Create();
            _handler = new ChangeOptionOfferRelevancePeriodCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ChangeOptionOfferRelevancePeriodCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_ChangeRelevancePeriod_GivenCommand_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                _handler.Handle(null as ChangeOptionOfferRelevancePeriodCommand, CancellationToken.None));
        }

        [Fact]
        public Task Handle_ResetRelevancePeriod_GivenCommand_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () =>
                _handler.Handle(null as ChangeOptionOfferRelevancePeriodCommand, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_ChangeRelevancePeriod()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.ChangeRelevancePeriod(_changeCommand.VehicleOfferId, _prNumber, _relevancePeriod))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_changeCommand, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_ChangeRelevancePeriod_WhenOptionOffer_IsNotFound_Throws()
        {
            //Arrange
            _service.Setup(s => s.ChangeRelevancePeriod(_changeCommand.VehicleOfferId, _prNumber, _relevancePeriod))
                .Throws(new OptionOfferNotFoundException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_changeCommand.PrNumber), () => _handler.Handle(_changeCommand, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }

        [Fact]
        public async Task Handle_ResetRelevancePeriod()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.ChangeRelevancePeriod(_resetCommand.VehicleOfferId, _prNumber, RelevancePeriod.Empty))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_resetCommand, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_ResetRelevancePeriod_WhenOptionOffer_IsNotFound_Throws()
        {
            //Arrange
            _service.Setup(s => s.ChangeRelevancePeriod(_resetCommand.VehicleOfferId, _prNumber, RelevancePeriod.Empty))
                .Throws(new OptionOfferNotFoundException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_resetCommand.PrNumber), () => _handler.Handle(_resetCommand, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }
    }
}
