﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.OptionOffers
{
    public class AddRuleSetCommandHandlerTest
    {
        //Entity
        private readonly VehicleOffer _vehicleOffer;
        private readonly OptionOffer _optionOffer;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IOptionOfferService> _service;

        //Handler and command
        private readonly AddRuleSetCommandHandler _handler;
        private readonly AddRuleSetCommand _command;
        private readonly AddRuleSetForManyCommand _commandMany;
        private readonly ModelKeySet _modelKeySet;

        public AddRuleSetCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            _modelKeySet = fixture.Create<ModelKeySet>();

            //Entity
            _vehicleOffer = fixture.Create<VehicleOffer>();
            _optionOffer = fixture.Create<OptionOffer>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IOptionOfferService>();
            var mapper = new Mock<ITwoWayMapper<string, RuleSetModel>>();

            //Handler and commands
            _handler = new AddRuleSetCommandHandler(_work.Object, _service.Object)
            {
                RuleSetMapper = mapper.Object
            };
            _command = new AddRuleSetCommand
            {
                VehicleOfferId = _vehicleOffer.Id,
                PrNumber = _optionOffer.PrNumber.Value
            };

            _commandMany = new AddRuleSetForManyCommand
            {
                CatalogId = _vehicleOffer.CatalogId,
                ModelKeySet = _modelKeySet.AsString,
                OptionId = _optionOffer.OptionId.Value
            };

            //Setup dependencies
            mapper.Setup(m => m.MapReverse(_command.RuleSet)).Returns(_optionOffer.RuleSet);
        }

        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(AddRuleSetCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange               
                _service.Setup(s => s.AddRuleSet(_command.VehicleOfferId, _optionOffer.PrNumber, _optionOffer.RuleSet))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null as AddRuleSetCommand, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_WhenVehicleOffer_IsNotFound_Throws()
        {
            //Arrange
            var inner = new InvalidEntityIdException(_vehicleOffer.GetType(), _vehicleOffer.Id);
            _service.Setup(s => s.AddRuleSet(_vehicleOffer.Id, _optionOffer.PrNumber, _optionOffer.RuleSet))
                .Throws(inner);

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                    nameof(_command.VehicleOfferId), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage(inner.Message).WithValue(_command.VehicleOfferId);
        }

        [Fact]
        public async Task Handle_WhenOptionOffer_IsNotFound_Throws()
        {
            //Arrange
            var inner = new OptionOfferNotFoundException();
            _service.Setup(s => s.AddRuleSet(_vehicleOffer.Id, _optionOffer.PrNumber, _optionOffer.RuleSet))
                .Throws(inner);

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                    nameof(_command.PrNumber), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage(inner.Message).WithValue(_command.PrNumber);
        }

        [Fact]
        public async Task Handle_ForMany()
        {
            using (Sequence.Create())
            {
                //Arrange               
                _service.Setup(s => s.AddRuleSet(_commandMany.CatalogId, ModelKeySet.Parse(_commandMany.ModelKeySet), _optionOffer.OptionId, _optionOffer.RuleSet))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_commandMany, CancellationToken.None);
            }
        }

        [Fact]
        public Task Handle_ForManyGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null as AddRuleSetForManyCommand, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_ForManyWhenOptionOffer_IsNotFound_Throws()
        {
            //Arrange
            var inner = new OptionOfferNotFoundException();

            _service.Setup(s => s.AddRuleSet(_commandMany.CatalogId, _modelKeySet, _optionOffer.OptionId, _optionOffer.RuleSet))
                .ThrowsAsync(inner);

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_commandMany.OptionId), () => _handler.Handle(_commandMany, CancellationToken.None));

            //Assert
            e.WithMessage(inner.Message).WithValue(_commandMany.OptionId);
        }

        [Fact]
        public async Task Handle_WhenInvalidOption_Throws()
        {
            //Arrange
            var inner = new SelfReferenceRuleException();

            _service.Setup(s => s.AddRuleSet(_commandMany.CatalogId, _modelKeySet, _optionOffer.OptionId, _optionOffer.RuleSet))
                .ThrowsAsync(inner);

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_commandMany.OptionId), () => _handler.Handle(_commandMany, CancellationToken.None));

            //Assert
            e.WithMessage(inner.Message).WithValue(_commandMany.OptionId);
        }

        [Fact]
        public async Task Handle_WhenRuleHasConflict_Throws()
        {
            //Arrange
            var inner = new RuleConflictException();

            _service.Setup(s => s.AddRuleSet(_commandMany.CatalogId, _modelKeySet, _optionOffer.OptionId, _optionOffer.RuleSet))
                .ThrowsAsync(inner);

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_commandMany.OptionId), () => _handler.Handle(_commandMany, CancellationToken.None));

            //Assert
            e.WithMessage(inner.Message).WithValue(_commandMany.OptionId);
        }

        [Fact]
        public void Mapper()
        {
            //Arrange
            var handler = new AddRuleSetCommandHandler(_work.Object, _service.Object);

            //Assert
            Assert.Injection.OfProperty(handler, nameof(handler.RuleSetMapper))
                .HasDefault()
                .HasNullGuard()
                .DoesOverride();
        }
    }
}
