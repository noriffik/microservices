﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.OptionOffers
{
    public class RemoveRuleCommandHandlerTest
    {
        //Entity
        private readonly VehicleOffer _vehicleOffer;
        private readonly OptionOffer _optionOffer;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IOptionOfferService> _service;

        //Handler and command
        private readonly RemoveRuleCommandHandler _handler;
        private readonly RemoveRuleCommand _command;

        public RemoveRuleCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _vehicleOffer = fixture.Create<VehicleOffer>();
            _optionOffer = fixture.Create<OptionOffer>();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IOptionOfferService>();

            //Handler and commands
            _handler = new RemoveRuleCommandHandler(_work.Object, _service.Object);
            _command = new RemoveRuleCommand
            {
                VehicleOfferId = _vehicleOffer.Id,
                PrNumber = _optionOffer.PrNumber.Value
            };
        }

        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(RemoveRuleCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null as RemoveRuleCommand, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_WhenVehicleOffer_IsNotFound_Throws()
        {
            //Arrange
            var inner = new InvalidEntityIdException(_vehicleOffer.GetType(), _vehicleOffer.Id);
            _service.Setup(s => s.RemoveRule(_vehicleOffer.Id, _optionOffer.PrNumber))
                .Throws(inner);

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.VehicleOfferId), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage(inner.Message).WithValue(_command.VehicleOfferId);
        }

        [Fact]
        public async Task Handle_WhenOptionOffer_IsNotFound_Throws()
        {
            //Arrange
            var inner = new OptionOfferNotFoundException();
            _service.Setup(s => s.RemoveRule(_vehicleOffer.Id, _optionOffer.PrNumber))
                .Throws(inner);

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.PrNumber), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage(inner.Message).WithValue(_command.PrNumber);
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange               
                _service.Setup(s => s.RemoveRule(_command.VehicleOfferId, _optionOffer.PrNumber))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }
    }
}
