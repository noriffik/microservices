﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.OptionOffers
{
    public class UpdateInclusionCommandHandlerTest
    {
        //Entities
        private readonly PrNumber _prNumber;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IOptionOfferService> _service;

        //Handler and command
        private readonly UpdateInclusionCommandHandler _handler;
        private readonly UpdateInclusionCommand _command;

        public UpdateInclusionCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entities
            _prNumber = fixture.Create<PrNumber>();

            //Command
            _command = new UpdateInclusionCommand
            {
                PrNumber = _prNumber.Value,
            };

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IOptionOfferService>();

            //Handler and command
            _handler = new UpdateInclusionCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(AddCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.UpdateInclusion(
                        _command.VehicleOfferId, _prNumber, _command.Inclusion))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_WhenOptionOffer_IsNotFound_Throws()
        {
            //Arrange
            _service.Setup(s => s.UpdateInclusion(_command.VehicleOfferId, _prNumber, _command.Inclusion))
                .ThrowsAsync(new OptionOfferNotFoundException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.PrNumber), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }
    }
}
