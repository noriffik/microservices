﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Configurations
{
    public class ExcludePackageCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IConfigurationService> _service;
        private readonly Mock<IUnitOfWork> _work;

        //Handler and command
        private readonly ExcludePackageCommand _command;
        private readonly ExcludePackageCommandHandler _handler;

        public ExcludePackageCommandHandlerTest()
        {
            //Dependencies
            _service = new Mock<IConfigurationService>();
            _work = new Mock<IUnitOfWork>();

            //Command
            _command = new Fixture().Create<ExcludePackageCommand>();

            //Handler
            _handler = new ExcludePackageCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ExcludePackageCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.ExcludePackage(_command.ConfigurationId))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }
    }
}
