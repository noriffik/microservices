﻿using AutoFixture;
using Moq;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Rules;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Configurations
{
    public class ValidateCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IConfigurationValidationService> _validator;

        //Command and Handler
        private readonly ValidateCommand _command;
        private readonly ValidateCommandHandler _handler;

        public ValidateCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());
            var catalog = fixture.Create<Catalog>();
            var modelKey = fixture.Create<ModelKey>();
            var prNumberSet = fixture.Create<PrNumberSet>();

            //Command
            _command = fixture.Build<ValidateCommand>()
                .With(c => c.ModelYear, catalog.ModelYear.Value)
                .With(c => c.ModelKey, modelKey.Value)
                .With(c => c.PrNumberSet, prNumberSet.AsString)
                .Create();

            //Dependencies
            _validator = new Mock<IConfigurationValidationService>();

            //Handler
            _handler = new ValidateCommandHandler(_validator.Object);
        }

        [Fact]
        public void Ctor_GivenValidator_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("validator", () => new ValidateCommandHandler(null));
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            _validator.Setup(s => s.ValidateConfiguration(_command.ModelYear, _command.ModelKey, _command.PrNumberSet))
                .Returns(Task.CompletedTask);

            //Act
            var result = await _handler.Handle(_command, CancellationToken.None);

            //Assert
            Assert.Equal(ValidationResponse.Positive(), result, PropertyComparer<ValidationResponse>.Instance);
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async void Handle_WhenOptions_IsUnavailable_Throws()
        {
            //Arrange
            var details = new Fixture().Create<RuleViolation>();
            var e = new UnavailableOptionException { Details = new List<RuleViolation> { details } };

            _validator.Setup(s => s.ValidateConfiguration(_command.ModelYear, _command.ModelKey, _command.PrNumberSet))
                .Throws(e);

            //Act
            var result = await _handler.Handle(_command, CancellationToken.None);
            var failure = ValidationResponse.Negative(e.Message, e.Details);

            //Assert
            Assert.Equal(failure, result, PropertyComparer<object>.Instance);
        }
    }
}
