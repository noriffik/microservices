﻿using AutoFixture;
using Moq;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Configurations
{
    public class CopyCommandHandlerTest
    {
        private readonly IFixture _fixture = new Fixture();

        //Dependencies
        private readonly Mock<IConfigurationService> _service;
        private readonly Mock<IUnitOfWork> _work;

        //Handler and command
        private readonly CopyCommand _command;
        private readonly CopyCommandHandler _handler;

        public CopyCommandHandlerTest()
        {
            //Dependencies
            _service = new Mock<IConfigurationService>();
            _work = new Mock<IUnitOfWork>();

            //Command
            _command = _fixture.Create<CopyCommand>();

            //Handler
            _handler = new CopyCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(CopyCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        public async Task Handle()
        {
            //Arrange
            var configuration = _fixture.Create<Configuration>();
            _service.Setup(s => s.Copy(_command.ConfigurationId))
                .ReturnsAsync(configuration);

            //Act
            var result = await _handler.Handle(_command, CancellationToken.None);

            //Assert
            Assert.Equal(configuration.Id, result);
            _service.Verify(s => s.Copy(_command.ConfigurationId), Times.Once);
            _work.Verify(w => w.Commit(It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}