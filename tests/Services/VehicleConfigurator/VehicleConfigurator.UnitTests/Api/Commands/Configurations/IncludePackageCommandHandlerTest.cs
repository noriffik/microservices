﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Configurations
{
    public class IncludePackageCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IConfigurationService> _service;
        private readonly Mock<IUnitOfWork> _work;

        //Handler and command
        private readonly IncludePackageCommand _command;
        private readonly IncludePackageCommandHandler _handler;

        //Entities
        private readonly PackageCode _packageCode;

        public IncludePackageCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _service = new Mock<IConfigurationService>();
            _work = new Mock<IUnitOfWork>();

            //Entities
            _packageCode = fixture.Create<PackageCode>();

            //Command
            _command = fixture.Build<IncludePackageCommand>()
                .With(c => c.PackageCode, _packageCode.Value)
                .Create();

            //Handler
            _handler = new IncludePackageCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(IncludePackageCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.IncludePackage(_command.ConfigurationId, _packageCode))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_WhenRequirementViolation_Throws()
        {
            //Arrange
            _service.Setup(s => s.IncludePackage(_command.ConfigurationId, _packageCode))
                .ThrowsAsync(new PackageRequirementViolationException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                nameof(_command.ConfigurationId), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }
    }
}
