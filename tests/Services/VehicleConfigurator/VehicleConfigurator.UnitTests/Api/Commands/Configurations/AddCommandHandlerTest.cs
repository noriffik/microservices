﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Configurations
{
    public class AddCommandHandlerTest
    {
        //Entity
        private readonly Configuration _configuration;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IConfigurationService> _service;

        //Command and Handler
        private readonly AddCommand _command;
        private readonly AddCommandHandler _handler;

        public AddCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _configuration = fixture.Create<Configuration>();

            //Command
            _command = fixture.Build<AddCommand>()
                .With(c => c.CatalogId, _configuration.CatalogId)
                .With(c => c.ModelKey, _configuration.ModelKey.Value)
                .Create();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IConfigurationService>();

            //Handler
            _handler = new AddCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Trows()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.Add(_configuration.CatalogId, _configuration.ModelKey))
                    .InSequence()
                    .ReturnsAsync(_configuration);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_configuration.Id, result);
            }
        }
    }
}
