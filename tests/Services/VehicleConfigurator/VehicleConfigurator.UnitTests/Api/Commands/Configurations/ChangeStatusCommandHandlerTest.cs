﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Configurations
{
    public class ChangeStatusCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IConfigurationService> _service;

        //Command and Handler
        private readonly ChangeStatusCommand _command;
        private readonly ChangeStatusCommandHandler _handler;

        public ChangeStatusCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());
            
            //Command
            _command = fixture.Create<ChangeStatusCommand>();
                

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IConfigurationService>();

            //Handler
            _handler = new ChangeStatusCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work", () => new ChangeStatusCommandHandler(null, _service.Object));
        }

        [Fact]
        public void Ctor_GivenService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("service", () => new ChangeStatusCommandHandler(_work.Object, null));
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Trows()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.ChangeStatus(_command.ConfigurationId, _command.Status))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }
    }
}
