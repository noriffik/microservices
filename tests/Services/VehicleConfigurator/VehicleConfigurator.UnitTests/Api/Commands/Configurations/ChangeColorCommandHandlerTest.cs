﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Configurations
{
    public class ChangeColorCommandHandlerTest
    {
        //Entity
        private readonly ColorId _colorId;

        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IConfigurationService> _service;

        //Command and Handler
        private readonly ChangeColorCommand _command;
        private readonly ChangeColorCommandHandler _handler;

        public ChangeColorCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Entity
            _colorId = fixture.Create<ColorId>();

            //Command
            _command = fixture.Build<ChangeColorCommand>()
                .With(c => c.ColorId, _colorId.Value)
                .Create();

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IConfigurationService>();

            //Handler
            _handler = new ChangeColorCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(ChangeColorCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Trows()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.ChangeColor(_command.ConfigurationId, _colorId))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_WhenColorOffer_NotFound_Throws()
        {
            //Arrange
            _service.Setup(s => s.ChangeColor(_command.ConfigurationId, _colorId))
                .ThrowsAsync(new ColorOfferNotFoundException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(nameof(_command.ColorId), () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("test");
            e.WithValue(_command.ColorId);
        }
    }
}
