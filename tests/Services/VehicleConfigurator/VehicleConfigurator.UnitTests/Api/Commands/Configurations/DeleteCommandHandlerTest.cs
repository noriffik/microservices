﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Domain.Configurations;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Configurations
{
    public class DeleteCommandHandlerTest
    {
        private readonly IFixture _fixture = new Fixture();

        //Dependencies
        private readonly Mock<IConfigurationService> _service;
        private readonly Mock<IUnitOfWork> _work;

        //Handler and command
        private readonly DeleteCommand _command;
        private readonly DeleteCommandHandler _handler;

        public DeleteCommandHandlerTest()
        {
            //Dependencies
            _service = new Mock<IConfigurationService>();
            _work = new Mock<IUnitOfWork>();

            //Command
            _command = _fixture.Create<DeleteCommand>();

            //Handler
            _handler = new DeleteCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor_VerifyInjection()
        {
            Assert.Injection.OfConstructor(typeof(DeleteCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange               
                _service.Setup(s => s.Remove(_command.ConfigurationId))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }
        }
    }
}
