﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Api.Application.Validations.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Rules;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Configurations
{
    public class ToggleOptionCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IConfigurationService> _service;

        //Command and Handler
        private readonly IncludeOptionCommand _includeCommand;
        private readonly ExcludeOptionCommand _excludeCommand;
        private readonly ToggleOptionCommandHandler _handler;
        private readonly PrNumber _prNumber;

        public ToggleOptionCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());
            _prNumber = fixture.Create<PrNumber>();

            //Command
            _includeCommand = fixture.Build<IncludeOptionCommand>()
                .With(c => c.PrNumber, _prNumber.Value)
                .Create();

            _excludeCommand = fixture.Build<ExcludeOptionCommand>()
                .With(c => c.PrNumber, _prNumber.Value)
                .Create();

            //Dependencies
            _prNumber = PrNumber.Parse(_excludeCommand.PrNumber);
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IConfigurationService>();

            //Handler
            _handler = new ToggleOptionCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work", () => new ToggleOptionCommandHandler(null, _service.Object));
        }

        [Fact]
        public void Ctor_GivenService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("service", () => new ToggleOptionCommandHandler(_work.Object, null));
        }

        [Fact]
        public async Task HandleInclude()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.IncludeOption(_includeCommand.ConfigurationId, _prNumber))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_includeCommand, CancellationToken.None);
            }
        }

        [Fact]
        public async Task HandleInclude_GivenRequest_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null as IncludeOptionCommand, CancellationToken.None));
        }

        [Fact]
        public async void HandleInclude_WhenOption_IsUnavailable_Throws()
        {
            //Arrange
            var details = new Fixture().Create<RuleViolation>();
            var e = new UnavailableOptionException { Details = new List<RuleViolation> { details } };

            _service.Setup(s => s.IncludeOption(_includeCommand.ConfigurationId, _prNumber))
                .Throws(e);

            //Act
            var error = await Assert.ThrowsAsync<UnavailableOptionValidationException>(() => _handler.Handle(_includeCommand, CancellationToken.None));
            var failure = error.Errors.Single().FormattedMessagePlaceholderValues["PrNumber"];

            //Assert
            Assert.Equal(failure, details, PropertyComparer<object>.Instance);
        }

        [Fact]
        public async Task HandleExclude()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.ExcludeOption(_excludeCommand.ConfigurationId, _prNumber))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_excludeCommand, CancellationToken.None);
            }
        }

        [Fact]
        public async Task HandleExclude_GivenRequest_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null as IncludeOptionCommand, CancellationToken.None));
        }

        [Fact]
        public async Task HandleExclude_WhenOption_IsRequired_ByPackage_Throws()
        {
            //Arrange
            var inner = new PackageRequirementViolationException();
            _service.Setup(s => s.ExcludeOption(_excludeCommand.ConfigurationId, _prNumber))
                .Throws(inner);

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                    string.Empty, () => _handler.Handle(_excludeCommand, CancellationToken.None));

            //Assert
            e.WithMessage(inner.Message);
        }
    }
}
