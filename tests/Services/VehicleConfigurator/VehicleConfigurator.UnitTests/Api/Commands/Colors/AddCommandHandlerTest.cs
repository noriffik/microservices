﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Colors;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Colors
{
    public class AddCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IColorService> _service;
        private readonly Mock<IUnitOfWork> _work;

        //Entity
        private readonly Color _color;

        //Handler and command
        private readonly AddCommandHandler _handler;
        private readonly AddCommand _command;

        public AddCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _service = new Mock<IColorService>();
            _work = new Mock<IUnitOfWork>();

            //Entity
            _color = fixture.Create<Color>();

            //Command
            _command = fixture.Create<AddCommand>();

            //Mapper
            var mapper = new Mock<IMapper>();
            mapper.Setup(m => m.Map<Color>(_command)).Returns(_color);

            //Handler
            _handler = new AddCommandHandler(_service.Object, _work.Object, mapper.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(AddCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.Add(_color))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_color.Id.Value, result);
            }
        }

        [Fact]
        public async Task Handle_WhenEntityId_IsInvalid_Throws()
        {
            //Arrange
            _service.Setup(s => s.Add(_color))
                .ThrowsAsync(new InvalidEntityIdException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                string.Empty, () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }
    }
}
