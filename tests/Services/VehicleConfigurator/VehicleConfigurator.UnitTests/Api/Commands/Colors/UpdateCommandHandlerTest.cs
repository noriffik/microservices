﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.Colors;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Colors
{
    public class UpdateCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly Color _color;
        private readonly ColorTypeId _colorTypeId;
        private readonly System.Drawing.Color _argb;

        //Command and handler
        private readonly UpdateCommand _command;
        private readonly UpdateCommandHandler _handler;

        public UpdateCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            var mapper = new Mock<IMapper>();
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entities
            _color = fixture.Create<Color>();
            _colorTypeId = fixture.Create<ColorTypeId>();
            _argb = fixture.Create<System.Drawing.Color>();

            //Command and handler
            _command = fixture.Build<UpdateCommand>()
                .With(c => c.Id, _color.Id.Value)
                .With(c => c.TypeId, _colorTypeId.Value)
                .Create();
            _handler = new UpdateCommandHandler(_work.Object, mapper.Object);

            //Setup mapper
            mapper.Setup(m => m.Map<System.Drawing.Color>(_command.Argb))
                .Returns(_argb);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(UpdateCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            using (Sequence.Create())
            {
                _repository.Setup(r => r.HasRequired<ColorType, ColorTypeId>(_colorTypeId, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _repository.Setup(r => r.Require<Color, ColorId>(_color.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_color);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }

            //Assert
            Assert.Equal(_command.Name, _color.Name);
            Assert.Equal(_argb, _color.Argb);
            Assert.Equal(_colorTypeId, _color.TypeId);
        }
    }
}
