﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.ColorTypes;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.ColorTypes
{
    public class AddCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IColorTypeService> _service;
        private readonly Mock<IUnitOfWork> _work;

        //Entity
        private readonly ColorType _colorType;

        //Handler and command
        private readonly AddCommandHandler _handler;
        private readonly AddCommand _command;

        public AddCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _service = new Mock<IColorTypeService>();
            _work = new Mock<IUnitOfWork>();

            //Entity
            _colorType = fixture.Create<ColorType>();

            //Command
            _command = fixture.Create<AddCommand>();

            //Mapper
            var mapper = new Mock<IMapper>();
            mapper.Setup(m => m.Map<ColorType>(_command)).Returns(_colorType);

            //Handler
            _handler = new AddCommandHandler(_service.Object, _work.Object, mapper.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(VehicleConfigurator.Api.Application.Commands.Packages.AddCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_WhenGivenRequest_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                _service.Setup(s => s.Add(_colorType))
                    .InSequence()
                    .Returns(Task.CompletedTask);
                _work.Setup(w => w.Commit(CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(_command, CancellationToken.None);

                //Assert
                Assert.Equal(_colorType.Id.Value, result);
            }
        }

        [Fact]
        public async Task Handle_WhenEntity_AlreadyExist_Throws()
        {
            //Arrange
            _service.Setup(s => s.Add(_colorType))
                .ThrowsAsync(new DuplicateEntityException("test"));

            //Act
            var e = await Assert.Validation.ThrowsAsync(
                string.Empty, () => _handler.Handle(_command, CancellationToken.None));

            //Assert
            e.WithMessage("test");
        }

    }
}
