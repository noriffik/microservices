﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Api.Application.Commands.ColorTypes;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.ColorTypes
{
    public class RenameCommandHandlerTest
    {
        //Dependencies
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;

        //Entities
        private readonly ColorType _colorType;

        //Command and handler
        private readonly RenameCommand _command;
        private readonly RenameCommandHandler _handler;

        public RenameCommandHandlerTest()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();
            _work.Setup(w => w.EntityRepository).Returns(_repository.Object);

            //Entities
            _colorType = fixture.Create<ColorType>();

            //Command and handler
            _command = fixture.Build<RenameCommand>()
                .With(c => c.Id, _colorType.Id.Value)
                .Create();
            _handler = new RenameCommandHandler(_work.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(RenameCommandHandler)).HasNullGuard();
        }

        [Fact]
        public Task Handle_GivenCommand_IsNull_Throws()
        {
            return Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            //Arrange
            using (Sequence.Create())
            {
                _repository.Setup(r => r.Require<ColorType, ColorTypeId>(_colorType.Id, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_colorType);
                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(_command, CancellationToken.None);
            }

            //Assert
            Assert.Equal(_command.Name, _colorType.Name);
        }
    }
}
