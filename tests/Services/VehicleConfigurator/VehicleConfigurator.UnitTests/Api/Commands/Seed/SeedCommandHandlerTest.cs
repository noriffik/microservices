﻿using System;
using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Infrastructure.Seeds;
using NexCore.VehicleConfigurator.Api.Application.Commands.Seeds;
using NexCore.VehicleConfigurator.Infrastructure;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Seed
{
    public class SeedCommandHandlerTest
    {
        private readonly Mock<IDbContextSeeder<VehicleConfiguratorContext>> _seeder;
        private readonly SeedCommandHandler _handler;

        public SeedCommandHandlerTest()
        {
            _seeder = new Mock<IDbContextSeeder<VehicleConfiguratorContext>>();
            _handler = new SeedCommandHandler(_seeder.Object);
        }

        [Fact]
        public void Ctor()
        {
            Assert.Injection.OfConstructor(typeof(SeedCommandHandler)).HasNullGuard();
        }

        [Fact]
        public async Task Handle_GivenCommand_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request", () => _handler.Handle(null, CancellationToken.None));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                var fixture = new Fixture();
                var command = fixture.Create<SeedCommand>();

                _seeder.Setup(s => s.Seed())
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(command, CancellationToken.None);
            }
        }
    }
}
