﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.Domain.Restrictions.Specifications;
using NexCore.VehicleConfigurator.UnitTests.Extensions.Domain;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NexCore.Common;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Restrictions
{
    public class ActualizeRelevanceCommandHandlerTest
    {
        private readonly Catalog _catalog;
        private readonly Fixture _fixture;
        private readonly ActualizeRelevanceCommandHandler _handler;
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IEntityRepository> _repository;
        private readonly PrNumberSet _toRestricted;
        private readonly VehicleOffer[] _vehicleOffers;
        private readonly ModelKeySet _modelKeySet;
        private readonly PrNumberSet _toUnrestricted;

        public ActualizeRelevanceCommandHandlerTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            _catalog = _fixture.Create<Catalog>();

            var optionOffers = _fixture.CreateMany<OptionOffer>(7)
                .ToList();

            var mustBeRestricted = optionOffers
                .Take(2)
                .ToList();

            _toRestricted = PrNumberSet.Parse(mustBeRestricted
                .Select(o => o.PrNumber.Value)
                .Aggregate((a, b) => $"{a} {b}"));

            var mustBeUnRestricted =
                optionOffers.Skip(2)
                    .Take(2)
                    .ToList();

            _toUnrestricted = PrNumberSet.Parse(mustBeUnRestricted
                .Select(o => o.PrNumber.Value)
                .Aggregate((a, b) => $"{a} {b}"));

            //Setup vehicle offers
            _vehicleOffers = SetupVehicleOffers(optionOffers, _toUnrestricted);
            _modelKeySet = new ModelKeySet(_vehicleOffers.Select(v => v.ModelKey));

            //Dependencies
            _work = new Mock<IUnitOfWork>();
            _repository = new Mock<IEntityRepository>();

            //Handler
            _handler = new ActualizeRelevanceCommandHandler(_work.Object);

            //Setup
            _work.Setup(w => w.EntityRepository)
                .Returns(_repository.Object);
        }

        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work", () => new ActualizeRelevanceCommandHandler(null));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            using (var time = new SystemTimeContext(DateTime.Now))
            {
                //Arrange
                var date = time.Now;

                //Setup restrictions
                var restrictions = SetupRestrictions(_vehicleOffers, _toRestricted, _toUnrestricted, date);

                var command = new ActualizeRelevanceCommand { CatalogId = _catalog.Id };

                _repository.Setup(r => r.Require<Catalog>(command.CatalogId.Value, It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(_catalog);

                _repository.Setup(r =>
                        r.Find(It.Is<RestrictionByCatalogIdSpecification>(s => s.CatalogId == _catalog.Id),
                            It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(restrictions);

                _repository.SetupSequence(r =>
                        r.Find(
                            It.Is<VehicleOffersByModelKeySetAndCatalogIdSpecification>(s =>
                                s.CatalogId == _catalog.Id && s.ModelKeySet == _modelKeySet),
                            It.IsAny<CancellationToken>()))
                    .ReturnsAsync(_vehicleOffers);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(command, CancellationToken.None);

                //Assert
                Assert.True(_vehicleOffers.SelectMany(v => v.Options).Take(2).All(o => o.IsRestricted));
                Assert.False(_vehicleOffers.SelectMany(v => v.Options).Skip(2).Take(2).All(o => o.IsRestricted));
            }
        }

        [Fact]
        public async Task Handle_WithoutCatalogId()
        {
            using (Sequence.Create())
            using (var time = new SystemTimeContext(DateTime.Now))
            {
                //Arrange
                var date = time.Now;

                var restrictions = SetupRestrictions(_vehicleOffers, _toRestricted, _toUnrestricted, date);

                var command = new ActualizeRelevanceCommand();

                _repository.Setup(r =>
                        r.Find(It.IsAny<RelevantPublishedCatalogSpecification>(), It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(new[] { _catalog });

                _repository.Setup(r =>
                        r.Find(It.Is<RestrictionByCatalogIdSpecification>(s => s.CatalogId == _catalog.Id),
                            It.IsAny<CancellationToken>()))
                    .InSequence()
                    .ReturnsAsync(restrictions);

                _repository.SetupSequence(r =>
                        r.Find(
                            It.Is<VehicleOffersByModelKeySetAndCatalogIdSpecification>(s =>
                                s.CatalogId == _catalog.Id && s.ModelKeySet == _modelKeySet),
                            It.IsAny<CancellationToken>()))
                    .ReturnsAsync(_vehicleOffers);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(command, CancellationToken.None);

                //Assert
                Assert.True(_vehicleOffers.SelectMany(v => v.Options).Take(2).All(o => o.IsRestricted));
                Assert.False(_vehicleOffers.SelectMany(v => v.Options).Skip(2).Take(2).All(o => o.IsRestricted));
            }
        }

        [Fact]
        public async Task Handle_CommandIsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request",
                () => _handler.Handle(null, CancellationToken.None));
        }

        private IEnumerable<Restriction> SetupRestrictions(VehicleOffer[] vehicleOffers, PrNumberSet toRestricted, PrNumberSet toUnrestricted,
          DateTime date)
        {
            //Relevant restrictions
            var relevantRestrictions = Enumerable.Range(1, 2)
                .Select(i => _fixture.Construct<Restriction>(new
                {
                    catalogId = _catalog.Id,
                    modelKey = vehicleOffers[i - 1].ModelKey,
                    prNumbers = toRestricted,
                    relevancePeriod = date.Range(i)
                }))
                .ToList();

            //Irrelevant restrictions
            var irrelevantRestrictions = Enumerable.Range(1, 2)
                .Select(i => _fixture.Construct<Restriction>(new
                {
                    catalogId = _catalog.Id,
                    modelKey = vehicleOffers[i - 1].ModelKey,
                    prNumbers = toUnrestricted,
                    relevancePeriod = date.Past(i - 1)
                }))
                .ToList();

            //Enable restrictions
            var restrictions = relevantRestrictions.Concat(irrelevantRestrictions).ToList();

            restrictions.ForEach(r => r.Enable());

            return restrictions;
        }

        private VehicleOffer[] SetupVehicleOffers(List<OptionOffer> optionOffers, PrNumberSet prNumbersToUnrestricted)
        {
            var vehicleOffers = Enumerable.Range(0, 2)
                .Select(i => _fixture.Construct<VehicleOffer>(new { catalogId = _catalog.Id })).ToArray();

            //Add options to vehicles
            foreach (var v in vehicleOffers)
            {
                optionOffers.ForEach(o => v.AddOptionOffer(o.PrNumber, Availability.Purchasable(_fixture.Create<decimal>())));
            }

            //Activate restriction options
            foreach (var v in vehicleOffers)
            {
                v.Options.Where(o => prNumbersToUnrestricted.Has(o.PrNumber)).ToList().ForEach(o => o.EnforceRestriction());
            }

            return vehicleOffers;
        }
    }
}
