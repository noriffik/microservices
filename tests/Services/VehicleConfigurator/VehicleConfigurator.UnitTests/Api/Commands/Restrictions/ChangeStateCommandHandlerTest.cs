﻿using AutoFixture;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Restrictions
{
    public class ChangeStateCommandHandlerTest
    {
        private readonly Fixture _fixture;
        private readonly Mock<IRestrictionService> _service;
        private readonly Mock<IUnitOfWork> _work;
        private readonly ChangeStateCommandHandler _handler;

        public ChangeStateCommandHandlerTest()
        {
            _fixture = new Fixture();

            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IRestrictionService>();

            _handler = new ChangeStateCommandHandler(_work.Object, _service.Object);
        }

        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work", () => new ChangeStateCommandHandler(null, _service.Object));
        }

        [Fact]
        public void Ctor_GivenService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("service", () => new ChangeStateCommandHandler(_work.Object, null));
        }

        [Fact]
        public async Task Handle_Enable_GivenRequest_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request",
                () => _handler.Handle(null as EnableCommand, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_Enable()
        {
            using (Sequence.Create())
            {
                //Arrange
                var command = _fixture.Create<EnableCommand>();

                _service.Setup(s => s.Enable(command.Id))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(command, CancellationToken.None);
            }
        }

        [Fact]
        public async Task Handle_Disable_GivenRequest_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request",
                () => _handler.Handle(null as DisableCommand, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_Disable()
        {
            using (Sequence.Create())
            {
                //Arrange
                var command = _fixture.Create<DisableCommand>();

                _service.Setup(s => s.Disable(command.Id))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                //Act
                await _handler.Handle(command, CancellationToken.None);
            }
        }
    }
}
