﻿using AutoFixture;
using AutoMapper;
using NexCore.Domain;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Restrictions
{
    public class CommandAutoMapperProfileTest
    {
        private readonly IMapper _mapper;
        private readonly IFixture _fixture;

        public CommandAutoMapperProfileTest()
        {
            var mapperConfig = new MapperConfiguration(
                c => c.AddProfiles(typeof(CommandAutoMapperProfile).Assembly));

            _mapper = mapperConfig.CreateMapper();

            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());
        }

        [Fact]
        public void Map_RelevancePeriodModel()
        {
            //Arrange
            var expected = _fixture.Create<Period>();
            var model = RelevancePeriodModel.Create(expected);

            //Act
            var actual = _mapper.Map<Period>(model);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<Period>.Instance);
        }

        [Fact]
        public void Map_AddCommand()
        {
            //Arrange
            var expected = _fixture.Create<Restriction>();
            var command = new AddCommand
            {
                CatalogId = expected.CatalogId,
                ModelKey = expected.ModelKey,
                PrNumbers = expected.PrNumbers,
                RelevancePeriod = _mapper.Map<RelevancePeriodModel>(expected.RelevancePeriod),
                Description = expected.Description
            };

            //Act
            var actual = _mapper.Map<Restriction>(command);

            //Assert
            Assert.Equal(expected, actual, PropertyComparer<Restriction>.Instance);
        }
    }
}
