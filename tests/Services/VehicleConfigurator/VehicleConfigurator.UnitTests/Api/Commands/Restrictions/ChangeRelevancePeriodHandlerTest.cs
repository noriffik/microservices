﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Restrictions
{
    public class ChangeRelevancePeriodHandlerTest
    {
        private readonly Mock<IRestrictionService> _service;
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IMapper> _mapper;
        private readonly ChangeRelevancePeriodCommandHandler _handler;
        private readonly IFixture _fixture;

        public ChangeRelevancePeriodHandlerTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());

            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IRestrictionService>();
            _mapper = new Mock<IMapper>();

            _handler = new ChangeRelevancePeriodCommandHandler(_work.Object, _service.Object, _mapper.Object);
        }

        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work",
                () => new ChangeRelevancePeriodCommandHandler(null, _service.Object, _mapper.Object));
        }

        [Fact]
        public void Ctor_GivenService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("service",
                () => new ChangeRelevancePeriodCommandHandler(_work.Object, null, _mapper.Object));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper",
                () => new ChangeRelevancePeriodCommandHandler(_work.Object, _service.Object, null));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                var command = _fixture.Create<ChangeRelevancePeriodCommand>();
                var period = _fixture.Create<Period>();

                _mapper.Setup(m => m.Map<Period>(command.RelevancePeriod))
                    .InSequence()
                    .Returns(period);

                _service.Setup(s => s.ChangeRelevancePeriod(command.Id, period))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                _work.Setup(w => w.Commit(CancellationToken.None))
                    .InSequence()
                    .Returns(Task.CompletedTask);

                await _handler.Handle(command, CancellationToken.None);
            }
        }
    }
}
