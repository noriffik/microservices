﻿using AutoFixture;
using AutoMapper;
using Moq;
using Moq.Sequences;
using NexCore.Domain;
using NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.UnitTests.Api.Commands.Restrictions
{
    public class AddCommandHandlerTest
    {
        private readonly AddCommandHandler _handler;
        private readonly Mock<IRestrictionService> _service;
        private readonly Mock<IUnitOfWork> _work;
        private readonly Mock<IMapper> _mapper;
        private readonly IFixture _fixture;

        public AddCommandHandlerTest()
        {
            _fixture = new Fixture();
            _fixture.Customizations.Add(new ModelComponentBuilder());
            
            _work = new Mock<IUnitOfWork>();
            _service = new Mock<IRestrictionService>();
            _mapper = new Mock<IMapper>();
            
            _handler = new AddCommandHandler(_work.Object, _service.Object, _mapper.Object);
        }

        [Fact]
        public void Ctor_GivenWork_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("work", () => new AddCommandHandler(null, _service.Object, _mapper.Object));
        }

        [Fact]
        public void Ctor_GivenService_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("restrictionService", () => new AddCommandHandler(_work.Object, null, _mapper.Object));
        }

        [Fact]
        public void Ctor_GivenMapper_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("mapper",
                () => new AddCommandHandler(_work.Object, _service.Object, null));
        }

        [Fact]
        public async Task Handle()
        {
            using (Sequence.Create())
            {
                //Arrange
                var restriction = _fixture.Create<Restriction>();
                var command = _fixture.Create<AddCommand>();

                _mapper.Setup(m => m.Map<Restriction>(command))
                    .InSequence()
                    .Returns(restriction);

                _service.Setup(s => s.Add(restriction))
                    .InSequence()
                    .ReturnsAsync(restriction);

                _work.Setup(w => w.Commit(It.IsAny<CancellationToken>()))
                    .Returns(Task.CompletedTask);

                //Act
                var result = await _handler.Handle(command, CancellationToken.None);

                //Assert
                Assert.NotNull(result);
                Assert.Equal(restriction.Id, result);
            }
        }

        [Fact]
        public async Task Handle_GivenRequest_IsNull_Throws()
        {
            await Assert.ThrowsAsync<ArgumentNullException>("request",
                () => _handler.Handle(null, CancellationToken.None));
        }
    }
}
