﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NexCore.CurrencyExchange.Abstract;
using NexCore.EventBus.Abstract;
using NexCore.Infrastructure.Utility;
using NexCore.Testing.Extensions.Moq;
using NexCore.VehicleConfigurator.Infrastructure;
using System;

namespace NexCore.VehicleConfigurator.FunctionalTests
{
    public class ServiceFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            base.ConfigureWebHost(builder);

            builder.UseContentRoot(".");

            builder.ConfigureServices(services =>
            {
                var serviceProvider = new ServiceCollection()
                    .AddEntityFrameworkInMemoryDatabase()
                    .BuildServiceProvider();

                services.AddScoped(c =>
                {
                    return new VehicleConfiguratorContext(
                        new DbContextOptionsBuilder<VehicleConfiguratorContext>()
                            .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                            .UseInMemoryDatabase(nameof(VehicleConfiguratorContext))
                            .UseInternalServiceProvider(serviceProvider)
                            .Options)
                    {
                        ResilientTransaction = ResilientTransactionStub()
                    };
                });

                services.AddScoped(s => StubCurrencyExchangeService());
                services.AddScoped(s => EventBusStub());
            });
        }

        private static IResilientTransaction ResilientTransactionStub()
        {
            var transaction = new Mock<IResilientTransaction>();

            transaction.SetupExecuteAsync();

            return transaction.Object;
        }

        private static IEventBus EventBusStub()
        {
            return new Mock<IEventBus>().Object;
        }

        private static ICurrencyExchangeService StubCurrencyExchangeService()
        {
            var service = new Mock<ICurrencyExchangeService>();
            service.Setup(s => s.GetRate(
                    It.IsAny<Currency>(), It.IsAny<Currency>(), It.IsAny<DateTime>()))
                .ReturnsAsync(1);

            return service.Object;
        }

        protected override IWebHostBuilder CreateWebHostBuilder()
        {
            return WebHost.CreateDefaultBuilder()
                .UseStartup<TStartup>();
        }

        public IServiceScope CreateScope()
        {
            return Server.Host.Services.CreateScope();
        }
    }
}
