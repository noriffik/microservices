﻿namespace NexCore.VehicleConfigurator.FunctionalTests.Fixtures
{
    class ConnectionStringProvider
    {
        public static string Get(string name) => TestConfiguration.Get["ConnectionString"]
            .Replace("%DATABASE%", $"NexCore.Services.VehicleConfigurator_{name}");
    }
}