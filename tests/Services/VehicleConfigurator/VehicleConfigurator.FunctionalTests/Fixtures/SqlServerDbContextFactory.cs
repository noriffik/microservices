﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NexCore.VehicleConfigurator.Infrastructure;
using System.Reflection;

namespace NexCore.VehicleConfigurator.FunctionalTests.Fixtures
{
    public class SqlServerDbContextFactory
    {
        public VehicleConfiguratorContext Create(string databaseName)
        {
            var connectionString = ConnectionStringProvider.Get(databaseName);

            var migrationAssembly = typeof(VehicleConfiguratorContext).GetTypeInfo().Assembly.GetName().Name;
            
            var services = new ServiceCollection()
                .AddEntityFrameworkSqlServer();

            var options = new DbContextOptionsBuilder<VehicleConfiguratorContext>()
                .UseInternalServiceProvider(services.BuildServiceProvider())
                .UseSqlServer(
                    connectionString,
                    sqlOptions => {sqlOptions
                        .MigrationsAssembly(migrationAssembly)
                        .EnableRetryOnFailure(10);
                    })
                .EnableSensitiveDataLogging()
                .Options;

            var mediator = new Mock<IMediator>();

            return new VehicleConfiguratorContext(options, mediator.Object);
        }
    }
}
