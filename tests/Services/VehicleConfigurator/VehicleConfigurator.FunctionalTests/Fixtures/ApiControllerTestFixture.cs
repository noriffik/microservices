﻿using AutoFixture;
using Microsoft.Extensions.DependencyInjection;
using NexCore.VehicleConfigurator.FunctionalTests.Api;
using NexCore.VehicleConfigurator.Infrastructure;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using System;
using System.Net.Http;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Fixtures
{
    public abstract class ApiControllerTestFixture : IClassFixture<ServiceFactory<TestStartup>>, IDisposable
    {
        protected readonly HttpClient Client;
        protected readonly VehicleConfiguratorContext DbContext;
        protected readonly IFixture Fixture;

        private readonly IServiceScope _scope;

        protected ApiControllerTestFixture(ServiceFactory<TestStartup> factory)
        {
            Client = factory.CreateClient();
            _scope = factory.CreateScope();
            DbContext = _scope.ServiceProvider.GetRequiredService<VehicleConfiguratorContext>();

            Fixture = new Fixture();
            Fixture.Customizations.Add(new ModelComponentBuilder());
        }
        
        public void Dispose()
        {
            DbContext.Database.EnsureDeleted();
            
            _scope.Dispose();
        }
    }
}
