﻿using Microsoft.EntityFrameworkCore;
using NexCore.VehicleConfigurator.Infrastructure;
using System;

namespace NexCore.VehicleConfigurator.FunctionalTests.Fixtures
{
    public abstract class SpecificationFixture : IDisposable
    {
        protected string DatabaseName { get; }

        protected readonly SqlServerDbContextFactory DbContextFactory;

        protected SpecificationFixture(string databaseName)
        {
            DatabaseName = databaseName;
            DbContextFactory = new SqlServerDbContextFactory();

            SetupDatabase();
        }

        public VehicleConfiguratorContext CreateContext()
        {
            return DbContextFactory.Create(DatabaseName);
        }

        protected void SetupDatabase()
        {
            using (var context = CreateContext())
            {
                context.Database.Migrate();

                SeedDatabase(context);
            }
        }

        protected virtual void SeedDatabase(VehicleConfiguratorContext context)
        {
        }

        public virtual void Dispose()
        {
            using (var context = CreateContext())
            {
                context.Database.EnsureDeleted();
            }
        }
    }
}
