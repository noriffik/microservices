﻿using Microsoft.Extensions.Configuration;

namespace NexCore.VehicleConfigurator.FunctionalTests
{
    static class TestConfiguration
    {
        private static IConfigurationRoot _root;

        public static IConfigurationRoot Get => _root ?? (_root = new ConfigurationBuilder()
                                                    .AddJsonFile("appsettings.json", true)
                                                    .AddUserSecrets("20870C0A-45E9-4A01-BEC9-B95D304928D5")
                                                    .Build());
    }
}
