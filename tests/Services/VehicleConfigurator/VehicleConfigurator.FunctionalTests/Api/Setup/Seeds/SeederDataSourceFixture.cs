﻿using NexCore.Infrastructure.Seeds;
using System;
using System.IO;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Setup.Seeds
{
    public class SeederDataSourceFixture
    {
        private class ResourceReaderProvider : ITextReaderProvider
        {
            private readonly string _name;

            public ResourceReaderProvider(string name)
            {
                _name = name ?? throw new ArgumentNullException(nameof(name));
            }

            public TextReader Get()
            {
                return new StringReader(Properties.Resources.ResourceManager.GetString(_name));
            }
        }

        public static ITextReaderProvider SetupProvider(string name)
        {
            return new ResourceReaderProvider(name);
        }
    }
}