﻿using NexCore.Infrastructure.Seeds;
using System;
using System.IO;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Setup.Seeds
{
    public class TextFileReaderProviderTest
    {
        private const string FilePath = "./TestFile.txt";

        [Fact]
        public void Get()
        {
            using (var expectedReader = new StreamReader(FilePath))
            {
                //Arrange
                var expected = expectedReader.ReadToEnd();
                var provider = new TextFileReaderProvider(FilePath);

                //Act
                using (var actualReader = provider.Get())
                {
                    //Assert
                    Assert.Equal(expected, actualReader.ReadToEnd());
                }
            }
        }

        [Fact]
        public void Ctor_GivenFilePath_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("filePath", () => new TextFileReaderProvider(null));
        }
    }
}
