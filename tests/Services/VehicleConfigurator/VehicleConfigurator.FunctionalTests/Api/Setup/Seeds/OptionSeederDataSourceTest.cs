﻿using AutoFixture;
using AutoMapper;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Setup.Seeds;
using NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Setup.Seeds
{
    public class OptionSeederDataSourceTest
    {
        private readonly IMapper _mapper;

        public OptionSeederDataSourceTest()
        {
            var mapperConfiguration = new MapperConfiguration(
                c => c.AddProfile(new SeederAutoMapperProfile()));

            _mapper = mapperConfiguration.CreateMapper();
        }

        [Fact]
        public void Get()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var expected = fixture.Create<Option>();
            var text = $"OptionId;Name;CategoryId\n{expected.Id.Value};{expected.Name};{expected.OptionCategoryId}";
            var dataSource = new OptionSeederDataSource(_mapper, text);

            //Act
            var actual = dataSource.Get();

            //Assert
            Assert.Contains(expected, actual, PropertyComparer<Option>.Instance);
        }

        [Fact]
        public void Get_WhenUsingDefaultText()
        {
            //Arrange
            var dataSource = new OptionSeederDataSource(
                _mapper, SeederDataSourceFixture.SetupProvider("Options"));

            //Assert
            Assert.NotEmpty(dataSource.Get());
        }
    }
}
