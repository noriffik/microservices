﻿using AutoFixture;
using AutoMapper;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Setup.Seeds;
using NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Setup.Seeds
{
    public class CategoryPerOptionSeederDataSourceTest
    {
        private readonly IMapper _mapper;

        public CategoryPerOptionSeederDataSourceTest()
        {
            var mapperConfiguration = new MapperConfiguration(
                c => c.AddProfile(new SeederAutoMapperProfile()));

            _mapper = mapperConfiguration.CreateMapper();
        }

        [Fact]
        public void Get()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var expected = fixture.Create<CategoryPerOptionRecord>();
            var text = $"OptionId;CategoryId;\n{expected.OptionId};{expected.CategoryId}";
            var dataSource = new CategoryPerOptionSeederDataSource(_mapper, text);

            //Act
            var actual = dataSource.Get();

            //Assert
            Assert.Contains(expected, actual, PropertyComparer<CategoryPerOptionRecord>.Instance);
        }

        [Fact]
        public void Get_WhenUsingDefaultText()
        {
            //Arrange
            var dataSource = new CategoryPerOptionSeederDataSource(
                _mapper, SeederDataSourceFixture.SetupProvider("CategoryPerOption"));

            //Assert
            Assert.NotEmpty(dataSource.Get());
        }
    }
}
