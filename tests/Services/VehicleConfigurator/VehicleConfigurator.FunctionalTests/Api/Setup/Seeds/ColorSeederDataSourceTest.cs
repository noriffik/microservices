﻿using AutoFixture;
using AutoMapper;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Setup.Seeds;
using NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Setup.Seeds
{
    public class ColorSeederDataSourceTest
    {
        private readonly IMapper _mapper;

        public ColorSeederDataSourceTest()
        {
            var mapperConfiguration = new MapperConfiguration(
                c => c.AddProfile(new SeederAutoMapperProfile()));

            _mapper = mapperConfiguration.CreateMapper();
        }

        [Fact]
        public void Get()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var expected = fixture.Create<Color>();
            var text = "ColorId;Argb;TypeId;Name\n" +
                       $"{expected.Id.Value};{expected.Argb.ToArgb():X8};{expected.TypeId.Value};{expected.Name}";
            var dataSource = new ColorSeederDataSource(_mapper, text);

            //Act
            var actual = dataSource.Get();

            //Assert
            Assert.Contains(expected, actual, PropertyComparer<Color>.Instance);
        }
        
        [Fact]
        public void Get_WhenUsingDefaultText()
        {
            //Arrange
            var dataSource = new ColorSeederDataSource(
                _mapper, SeederDataSourceFixture.SetupProvider("Colors"));

            //Assert
            Assert.NotEmpty(dataSource.Get());
        }
    }
}
