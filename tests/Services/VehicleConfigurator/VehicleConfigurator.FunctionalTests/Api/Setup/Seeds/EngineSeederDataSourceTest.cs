﻿using AutoFixture;
using AutoMapper;
using NexCore.VehicleConfigurator.Api.Setup.Seeds;
using NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Setup.Seeds
{
    public class EngineSeederDataSourceTest
    {
        private readonly IMapper _mapper;

        public EngineSeederDataSourceTest()
        {
            var mapperConfiguration = new MapperConfiguration(
                c => c.AddProfile(new SeederAutoMapperProfile()));
            
            _mapper = mapperConfiguration.CreateMapper();
        }

        [Fact]
        public void Get()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var expected = fixture.Create<Engine>();
            var fuelType = expected.FuelType.HasValue ? (int)expected.FuelType.Value : 0;
            var text = $"ModelId;Key;Name;FuelType\n{expected.ModelId};{expected.Id.Value[2]};{expected.Name};{fuelType}";
            var dataSource = new EngineSeederDataSource(_mapper, text);

            //Act
            var actual = dataSource.Get();
            
            //Assert
            Assert.Contains(expected, actual);
        }

        [Fact]
        public void Get_WhenUsingDefaultText()
        {
            //Arrange
            var dataSource = new EngineSeederDataSource(
                _mapper, SeederDataSourceFixture.SetupProvider("Engines"));
            
            //Assert
            Assert.NotEmpty(dataSource.Get());
        }
    }
}
