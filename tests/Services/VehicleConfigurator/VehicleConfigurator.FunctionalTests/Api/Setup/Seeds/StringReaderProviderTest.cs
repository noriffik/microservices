﻿using AutoFixture;
using NexCore.Infrastructure.Seeds;
using System;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Setup.Seeds
{
    public class StringReaderProviderTest
    {
        [Fact]
        public void Get()
        {
            //Arrange
            var fixture = new Fixture();
            var expected = fixture.Create<string>();
            var provider = new StringReaderProvider(expected);

            //Act
            using (var actual = provider.Get())
            {
                //Assert
                Assert.Equal(expected, actual.ReadToEnd());
            }
        }

        [Fact]
        public void Ctor_GivenText_IsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>("text", () => new StringReaderProvider(null));
        }
    }
}
