﻿using AutoFixture;
using AutoMapper;
using NexCore.VehicleConfigurator.Api.Setup.Seeds;
using NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Setup.Seeds
{
    public class PackageSeederDataSourceTest
    {
        private readonly IMapper _mapper;

        public PackageSeederDataSourceTest()
        {
            var mapperConfiguration = new MapperConfiguration(
                c => c.AddProfile(new SeederAutoMapperProfile()));
            
            _mapper = mapperConfiguration.CreateMapper();
        }

        [Fact]
        public void Get()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var expected = fixture.Create<Package>();
            var text = $"ModelKeySet;Id;Price;Applicability;Name;CatalogId\n{expected.ModelKeySet};{expected.Id};{expected.Price};{expected.Applicability.AsString};{expected.Name};{expected.CatalogId}";
            var dataSource = new PackageSeederDataSource(_mapper, text);

            //Act
            var actual = dataSource.Get();
            
            //Assert
            Assert.Contains(expected, actual);
        }

        [Fact]
        public void Get_WhenUsingDefaultText()
        {
            //Arrange
            var dataSource = new PackageSeederDataSource(
                _mapper, SeederDataSourceFixture.SetupProvider("Packages"));
            
            //Assert
            Assert.NotEmpty(dataSource.Get());
        }
    }
}
