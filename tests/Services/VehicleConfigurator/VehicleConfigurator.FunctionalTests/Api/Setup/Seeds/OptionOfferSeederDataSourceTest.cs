﻿using AutoFixture;
using AutoMapper;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Setup.Seeds;
using NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Setup.Seeds
{
    public class OptionOfferSeederDataSourceTest
    {
        private readonly IMapper _mapper;

        public OptionOfferSeederDataSourceTest()
        {
            var mapperConfiguration = new MapperConfiguration(
                c => c.AddProfile(new SeederAutoMapperProfile()));

            _mapper = mapperConfiguration.CreateMapper();
        }

        [Fact]
        public void Get()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var expected = fixture.Create<OptionOfferSeederModel>();
            var text = "PrNumber;Type;Reason;Price;ModelKey;CatalogId" +
                       $"\n{expected.PrNumber};{expected.Availability.Type};{expected.Availability.Reason};" +
                       $"{expected.Availability.Price};{expected.ModelKey};{expected.CatalogId}";
            var dataSource = new OptionOfferSeederDataSource(_mapper, text);

            //Act
            var actual = dataSource.Get();

            //Assert
            Assert.Contains(expected, actual, PropertyComparer<OptionOfferSeederModel>.Instance);
        }

        [Fact]
        public void Get_WhenUsingDefaultText()
        {
            //Arrange
            var dataSource = new OptionOfferSeederDataSource(_mapper,
                SeederDataSourceFixture.SetupProvider("OptionOffersNJ"),
                SeederDataSourceFixture.SetupProvider("OptionOffersNH"),
                SeederDataSourceFixture.SetupProvider("OptionOffers5E"),
                SeederDataSourceFixture.SetupProvider("OptionOffers3V"),
                SeederDataSourceFixture.SetupProvider("OptionOffersNU"),
                SeederDataSourceFixture.SetupProvider("OptionOffersNS"));

            //Assert
            Assert.NotEmpty(dataSource.Get());
        }
    }
}
