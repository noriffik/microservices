﻿using AutoFixture;
using AutoMapper;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Setup.Seeds;
using NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Setup.Seeds
{
    public class DefaultColorSeederDataSourceTest
    {
        private readonly IMapper _mapper;

        public DefaultColorSeederDataSourceTest()
        {
            var mapperConfiguration = new MapperConfiguration(
                c => c.AddProfile(new SeederAutoMapperProfile()));

            _mapper = mapperConfiguration.CreateMapper();
        }

        [Fact]
        public void Get()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var expected = fixture.Create<DefaultColorRecord>();
            var text = $"CatalogId;ModelKey;ColorId\n{expected.CatalogId};{expected.ModelKey.Value};{expected.ColorId.Value}";
            var dataSource = new DefaultColorSeederDataSource(_mapper, text);

            //Act
            var actual = dataSource.Get();

            //Assert
            Assert.Contains(expected, actual, PropertyComparer<DefaultColorRecord>.Instance);
        }

        [Fact]
        public void Get_WhenUsingDefaultText()
        {
            //Arrange
            var dataSource = new DefaultColorSeederDataSource(
                _mapper, SeederDataSourceFixture.SetupProvider("DefaultColors"));

            //Assert
            Assert.NotEmpty(dataSource.Get());
        }
    }
}
