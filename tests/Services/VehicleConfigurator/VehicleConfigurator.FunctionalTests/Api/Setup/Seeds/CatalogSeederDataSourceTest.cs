﻿using AutoFixture;
using AutoMapper;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Setup.Seeds;
using NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Setup.Seeds
{
    public class CatalogSeederDataSourceTest
    {
        private readonly IMapper _mapper;

        public CatalogSeederDataSourceTest()
        {
            var mapperConfiguration = new MapperConfiguration(
                c => c.AddProfile(new SeederAutoMapperProfile()));
            
            _mapper = mapperConfiguration.CreateMapper();
        }

        [Fact]
        public void Get()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var expected = fixture.Create<Catalog>();
            var text = $"Year;From;To;Status\n{expected.ModelYear.Value};{expected.RelevancePeriod.From:yyyy-MM-dd};{expected.RelevancePeriod?.To:yyyy-MM-dd};{expected.Status}";
            var dataSource = new CatalogSeederDataSource(_mapper, text);

            //Act
            var actual = dataSource.Get();

            //Assert
            Assert.Contains(expected, actual, PropertyComparer<Catalog>.Instance);
        }

        [Fact]
        public void Get_WhenUsingDefaultText()
        {
            //Arrange
            var dataSource = new CatalogSeederDataSource(
                _mapper, SeederDataSourceFixture.SetupProvider("Catalogs"));

            //Assert
            Assert.NotEmpty(dataSource.Get());
        }
    }
}
