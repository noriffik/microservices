﻿using AutoFixture;
using AutoMapper;
using NexCore.VehicleConfigurator.Api.Setup.Seeds;
using NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Setup.Seeds
{
    public class ColorTypeSeederDataSourceTest
    {
        private readonly IMapper _mapper;

        public ColorTypeSeederDataSourceTest()
        {
            var mapperConfiguration = new MapperConfiguration(
                c => c.AddProfile(new SeederAutoMapperProfile()));

            _mapper = mapperConfiguration.CreateMapper();
        }

        [Fact]
        public void Get()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var expected = fixture.Create<ColorType>();
            var text = $"ColorTypeId;Name\n{expected.Id.Value};{expected.Name}";
            var dataSource = new ColorTypeSeederDataSource(_mapper, text);

            //Act
            var actual = dataSource.Get();

            //Assert
            Assert.Contains(expected, actual);
        }

        [Fact]
        public void Get_WhenUsingDefaultText()
        {
            //Arrange
            var dataSource = new ColorTypeSeederDataSource(
                _mapper, SeederDataSourceFixture.SetupProvider("ColorTypes"));

            //Assert
            Assert.NotEmpty(dataSource.Get());
        }
    }
}
