﻿using AutoFixture;
using AutoMapper;
using NexCore.VehicleConfigurator.Api.Setup.Seeds;
using NexCore.VehicleConfigurator.Api.Setup.Seeds.DataSources;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Setup.Seeds
{
    public class BodySeederDataSourceTest
    {
        private readonly IMapper _mapper;

        public BodySeederDataSourceTest()
        {
            var mapperConfiguration = new MapperConfiguration(
                c => c.AddProfile(new SeederAutoMapperProfile()));
            
            _mapper = mapperConfiguration.CreateMapper();
        }

        [Fact]
        public void Get()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var expected = fixture.Create<Body>();
            var text = $"ModelId;Key;Name\n{expected.ModelId};{expected.Id.Value[2]};{expected.Name}";
            var dataSource = new BodySeederDataSource(_mapper, text);

            //Act
            var actual = dataSource.Get();
            
            //Assert
            Assert.Contains(expected, actual);
        }

        [Fact]
        public void Get_WhenUsingDefaultText()
        {
            //Arrange
            var dataSource = new BodySeederDataSource(
                _mapper, SeederDataSourceFixture.SetupProvider("Bodies"));
            
            //Assert
            Assert.NotEmpty(dataSource.Get());
        }
    }
}
