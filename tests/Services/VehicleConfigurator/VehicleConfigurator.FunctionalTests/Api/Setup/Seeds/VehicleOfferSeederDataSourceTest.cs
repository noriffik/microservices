﻿using AutoFixture;
using AutoMapper;
using NexCore.Testing;
using NexCore.VehicleConfigurator.Api.Setup.Seeds;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Domain.Vehicles;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Setup.Seeds
{
    public class VehicleOfferSeederDataSourceTest
    {
        private readonly IMapper _mapper;

        public VehicleOfferSeederDataSourceTest()
        {
            var mapperConfiguration = new MapperConfiguration(
                c => c.AddProfile(new SeederAutoMapperProfile()));

            _mapper = mapperConfiguration.CreateMapper();
        }

        [Fact]
        public void Get()
        {
            //Arrange
            var fixture = new Fixture();
            fixture.Customizations.Add(new ModelComponentBuilder());

            var expected = fixture.Create<VehicleOffer>();
            var text = $"CatalogId;ModelKey;Price\n{expected.CatalogId};{expected.ModelKey.Value};{expected.Price}";
            var dataSource = new VehicleOfferSeederDataSource(_mapper, text);

            //Act
            var actual = dataSource.Get();

            //Assert
            Assert.Contains(expected, actual, PropertyComparer<VehicleOffer>.Instance);
        }

        [Fact]
        public void Get_WhenUsingDefaultText()
        {
            //Arrange
            var dataSource = new VehicleOfferSeederDataSource(
                _mapper, SeederDataSourceFixture.SetupProvider("VehicleOffers"));

            //Assert
            Assert.NotEmpty(dataSource.Get());
        }
    }
}
