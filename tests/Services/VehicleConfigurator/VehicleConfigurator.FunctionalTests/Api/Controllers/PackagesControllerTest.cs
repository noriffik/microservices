﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.Packages;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers
{
    public class PackagesControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/catalogs/packages";
        
        public PackagesControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var seeder = new PackageSeeder(DbContext);

            var package = seeder.Create();

            await seeder.AddDependencies(package);

            await seeder.SeedAsync();

            var command = new AddCommand
            {
                Applicability = package.Applicability.AsString,
                CatalogId = package.CatalogId,
                Id = package.Id,
                ModelKeys = package.ModelKeySet.AsString,
                Name = package.Name,
                Price = package.Price
            };

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add_WhenCatalog_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new PackageSeeder(DbContext);

            var package = seeder.Create();

            await seeder.AddDependencies(package);

            await seeder.SeedAsync();

            var command = new AddCommand
            {
                Applicability = package.Applicability.AsString,
                CatalogId = Fixture.Create<int>(),
                Id = package.Id,
                ModelKeys = package.ModelKeySet.AsString,
                Name = package.Name,
                Price = package.Price
            };

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Add_WhenOneOfPrNumbers_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new PackageSeeder(DbContext);

            var package = seeder.Create();

            await seeder.AddDependencies(package);

            await seeder.SeedAsync();

            var command = new AddCommand
            {
                Applicability = package.Applicability.AsString + " " + seeder.Create<PrNumber>(),
                CatalogId = package.CatalogId,
                Id = package.Id,
                ModelKeys = package.ModelKeySet.AsString,
                Name = package.Name,
                Price = package.Price
            };

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Add_WhenOneOfModelComponents_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new PackageSeeder(DbContext);

            var package = await seeder.Add();

            await seeder.AddDependencies(package);

            await seeder.SeedAsync();

            var command = new AddCommand
            {
                Applicability = package.Applicability.AsString,
                CatalogId = package.CatalogId,
                Id = package.Id,
                ModelKeys = package.ModelKeySet.AsString + " " + seeder.Create<ModelKey>(),
                Name = package.Name,
                Price = package.Price
            };

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var seeder = new PackageSeeder(DbContext);

            var package = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{package.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenPackage_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task ChangeStatus()
        {
            //Arrange
            var seeder = new PackageSeeder(DbContext);

            var package = await seeder.Add();

            await seeder.SeedAsync();

            var command = Fixture.Build<ChangeStatusCommand>()
                .With(c => c.Id, package.Id.Value)
                .Create();
            
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/status/change", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ChangeStatus_WhenPackage_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<ChangeStatusCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/status/change", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task UpdatePrice()
        {
            //Arrange
            var seeder = new PackageSeeder(DbContext);

            var package = await seeder.Add();

            await seeder.SeedAsync();

            var command = Fixture.Build<UpdatePriceCommand>()
                .With(c => c.Id, package.Id.Value)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/price/update", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task UpdatePrice_WhenPackage_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<UpdatePriceCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/price/update", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task UpdateCoverage()
        {
            //Arrange
            var packageSeeder = new PackageSeeder(DbContext);
            var vehicleOfferSeeder = new VehicleOfferSeeder(DbContext, packageSeeder.Fixture);

            var package = await packageSeeder.Add();
            var modelKey = (await vehicleOfferSeeder.Add()).ModelKey;

            await DbContext.SaveChangesAsync();
            
            var command = new UpdateCoverageCommand
            {
                Id = package.Id,
                ModelKeys = package.ModelKeySet.AsString + " " + modelKey.Value
            };

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/coverage/update", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task UpdateCoverage_WhenModelKeys_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new PackageSeeder(DbContext);

            var package = await seeder.Add();

            await seeder.SeedAsync();

            var command = new UpdateCoverageCommand
            {
                Id = package.Id,
                ModelKeys = package.ModelKeySet.AsString + "invalid"
            };

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/coverage/update", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task GetByCatalogId()
        {
            //Arrange
            var seeder = new PackageSeeder(DbContext);

            var package = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/catalog/{package.CatalogId}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetByCatalogId_WhenPackage_IsNotFound_ReturnsOk()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/catalog/{Fixture.Create<int>()}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task UpdateApplicability()
        {
            //Arrange
            var seeder = new PackageSeeder(DbContext);

            var package = await seeder.Add();

            await seeder.SeedAsync();

            var command = new UpdateApplicabilityCommand
            {
                Applicability = package.Applicability
                    .SkipLast(1)
                    .Select(s => s.AsString)
                    .Aggregate((a, b) => $"{a} {b}"),
                Id = package.Id
            };

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/applicability/update", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task UpdateApplicability_WhenPackage_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new PackageSeeder(DbContext);

            var package = seeder.Create();

            await seeder.AddDependencies(package);

            await seeder.SeedAsync();

            var command = new UpdateApplicabilityCommand
            {
                Applicability = package.Applicability.AsString,
                Id = package.Id
            };

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/applicability/update", command);
            
            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
