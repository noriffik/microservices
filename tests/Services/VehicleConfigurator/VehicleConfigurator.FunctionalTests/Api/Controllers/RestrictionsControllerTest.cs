﻿using AutoFixture;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.Restrictions;
using NexCore.VehicleConfigurator.Domain.Restrictions;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers
{
    public class RestrictionsControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/restrictions";

        private readonly RestrictionSeeder _seeder;

        public RestrictionsControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
            _seeder = new RestrictionSeeder(DbContext);
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var restriction = await _seeder.AddDependencies(_seeder.Create());
            await _seeder.SeedAsync();

            var command = new AddCommand
            {
                CatalogId = restriction.CatalogId,
                ModelKey = restriction.ModelKey,
                PrNumbers = restriction.PrNumbers,
                RelevancePeriod = RelevancePeriodModel.Create(restriction.RelevancePeriod)
            };

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add_WhenCatalog_WasNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<AddCommand>();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var restriction = await _seeder.Add();
            await _seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{restriction.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetByCatalogId()
        {
            //Arrange
            const int catalogId = 1;
            await _seeder.AddRange<Restriction>(3, new { catalogId });
            await _seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{catalogId}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ChangeRelevancePeriod()
        {
            //Arrange
            var restriction = await _seeder.Add();
            await _seeder.SeedAsync();

            var periodModel = new RelevancePeriodModel
            {
                From = DateTime.MinValue,
                To = DateTime.MaxValue
            };

            var command = Fixture.Build<ChangeRelevancePeriodCommand>()
                .With(c => c.Id, restriction.Id)
                .With(c => c.RelevancePeriod, periodModel)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/period", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ChangeRelevancePeriod_WhenRestrictionWasNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<ChangeRelevancePeriodCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/period", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Enable()
        {
            //Arrange
            var restriction = await _seeder.Add();
            await _seeder.SeedAsync();

            var command = new EnableCommand { Id = restriction.Id };

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/enable/{command.Id}", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Enable_WhenRestrictionWasNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<EnableCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/enable/{command.Id}", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Disable()
        {
            //Arrange
            var restriction = await _seeder.Add();
            await _seeder.SeedAsync();

            var command = new DisableCommand { Id = restriction.Id };

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/disable/{command.Id}", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Disable_WhenRestrictionWasNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<DisableCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/disable/{command.Id}", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Actualize()
        {
            //Arrange
            var restriction = await _seeder.Add();
            await _seeder.SeedAsync();

            var endpoint = $"{BaseUrl}/actualize/{restriction.CatalogId}";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Actualize_GivenWhenCatalog_NotFound_ReturnsBadRequest()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/actualize/999";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
