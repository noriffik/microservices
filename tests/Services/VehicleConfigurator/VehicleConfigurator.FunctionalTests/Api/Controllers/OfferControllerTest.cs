﻿using AutoFixture;
using NexCore.Common;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.Offers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers
{
    public class OfferControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/offers";

        public OfferControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var seeder = new OfferSeeder(DbContext);

            var offer = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{offer.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenOffer_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetMany()
        {
            //Arrange
            var seeder = new OfferSeeder(DbContext);

            await seeder.AddRange(7);

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetMany_WhenNotFound_ReturnsOk()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var seeder = new OfferSeeder(DbContext);

            var configuration = await seeder.Configuration.Add();
            configuration.ChangeStatus(ConfigurationStatus.Completed);

            var command = Fixture.Build<AddCommand>()
                .With(c => c.ConfigurationId, configuration.Id)
                .Create();

            await DbContext.SaveChangesAsync();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add_CommandQuery_ConfigurationId_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Build<AddCommand>()
                .With(c => c.ConfigurationId, 0)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Add_GivenCommand_ConfigurationId_NotFound_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new OfferSeeder(DbContext);

            var offer = seeder.Add();

            var command = Fixture.Build<AddCommand>()
                .With(c => c.ConfigurationId, offer.Id)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Actualize()
        {
            //Arrange
            var seeder = new OfferSeeder(DbContext);

            var configuration = await seeder.Configuration.Add();
            configuration.ChangeStatus(ConfigurationStatus.Completed);

            var date = SystemTimeProvider.Instance.Get();
            var catalog = seeder.Configuration.Find<Catalog>(configuration.CatalogId);
            catalog.ChangeRelevancePeriod(new Period(date.AddDays(-10)));
            catalog.Status = CatalogStatus.Published;

            await seeder.Configuration.SeedAsync();

            var offer = await seeder.Add(new { modelYear = catalog.ModelYear, modelKey = configuration.ModelKey });

            await seeder.SeedAsync();

            var command = Fixture.Build<ActualizeCommand>()
                .With(c => c.OfferId, offer.Id)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/actualize", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Actualize_CommandQuery_OfferId_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Build<ActualizeCommand>()
                .With(c => c.OfferId, 0)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/actualize", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Actualize_GivenCommand_OfferId_NotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<ActualizeCommand>();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/actualize", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task SetPrivateCustomer()
        {
            //Arrange
            var seeder = new OfferSeeder(DbContext);

            var configuration = await seeder.Configuration.Add();
            configuration.ChangeStatus(ConfigurationStatus.Completed);

            var offer = await seeder.Add(1, new { configurationId = configuration.Id });

            await seeder.SeedAsync();

            var command = Fixture.Build<SetPrivateCustomerCommand>()
                .With(c => c.OfferId, offer.Id)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/customer/private", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task SetPrivateCustomer_GivenCommand_OfferId_NotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<SetPrivateCustomerCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/customer/private", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ChangeStatus()
        {
            //Arrange
            var seeder = new OfferSeeder(DbContext);

            var catalog = await seeder.Add();

            await seeder.SeedAsync();

            var command = Fixture.Build<ChangeStatusCommand>()
                .With(c => c.Id, catalog.Id)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/status", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ChangeStatus_WhenOffer_NotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<ChangeStatusCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/status", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
