﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.Colors;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers
{
    public class ColorsControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/colors";

        public ColorsControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var seeder = new ColorSeeder(DbContext);

            var color = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{color.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenColor_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<ColorId>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetMany()
        {
            //Arrange
            var seeder = new ColorSeeder(DbContext);

            await seeder.AddRange(5);

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetMany_WhenNotFound_ReturnsOk()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var seeder = new ColorSeeder(DbContext);

            var color = seeder.Create();

            await seeder.AddDependencies(color);

            await seeder.SeedAsync();

            var command = seeder.Build<AddCommand>()
                .With(c => c.Id, color.Id.Value)
                .With(c => c.TypeId, color.TypeId.Value)
                .With(c => c.Argb, seeder.Create<int>().ToString("X8"))
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add_WhenColor_AlreadyExist_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new ColorSeeder(DbContext);

            var color = await seeder.Add();

            await seeder.SeedAsync();

            var command = seeder.Build<AddCommand>()
                .With(c => c.Id, color.Id.Value)
                .With(c => c.TypeId, color.TypeId.Value)
                .With(c => c.Argb, seeder.Create<int>().ToString("X8"))
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Update()
        {
            //Arrange
            var seeder = new ColorSeeder(DbContext);

            var color = await seeder.Add();
            var colorType = await seeder.Add<ColorType, ColorTypeId>();

            await seeder.SeedAsync();

            var command = seeder.Build<UpdateCommand>()
                .With(c => c.Id, color.Id.Value)
                .With(c => c.Argb, seeder.Create<System.Drawing.Color>().ToArgb().ToString("X8"))
                .With(c => c.TypeId, colorType.Id.Value)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Update_WhenColorType_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new ColorSeeder(DbContext);

            var color = await seeder.Add();

            await seeder.SeedAsync();

            var command = seeder.Build<UpdateCommand>()
                .With(c => c.Id, color.Id.Value)
                .With(c => c.Argb, seeder.Create<System.Drawing.Color>().ToArgb().ToString("X8"))
                .With(c => c.TypeId, seeder.Create<ColorTypeId>().Value)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
