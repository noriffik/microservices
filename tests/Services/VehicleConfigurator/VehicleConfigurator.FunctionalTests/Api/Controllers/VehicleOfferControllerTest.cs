﻿using AutoFixture;
using Microsoft.EntityFrameworkCore;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.VehicleOffers;
using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers
{
    public class VehicleOfferControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/catalogs/vehicle";
        private const int PageNumber = 1;
        private const int PageSize = 3;
        private readonly string _queryString = $"?pageNumber={PageNumber}&pageSize={PageSize}";

        public VehicleOfferControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var seeder = new VehicleOfferSeeder(DbContext);

            var vehicleOffer = seeder.Create();

            await seeder.AddDependencies(vehicleOffer);

            await seeder.SeedAsync();

            var command = seeder.Build<AddCommand>()
                .With(c => c.ModelKey, vehicleOffer.ModelKey.Value)
                .With(c => c.CatalogId, vehicleOffer.CatalogId)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add_WhenCatalog_NotFound_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new VehicleOfferSeeder(DbContext);

            var vehicleOffer = seeder.Create();

            await seeder.AddDependencies(vehicleOffer);

            await seeder.SeedAsync();

            var command = seeder.Build<AddCommand>()
                .With(c => c.ModelKey, vehicleOffer.ModelKey.Value)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Add_GivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var seeder = new VehicleOfferSeeder(DbContext);

            var vehicleOffer = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{vehicleOffer.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetMany()
        {
            //Arrange
            var seeder = new VehicleOfferSeeder(DbContext);

            await seeder.AddRange(6);

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many/{_queryString}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetMany_WhenNotFound_ReturnsOk()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many/{_queryString}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetByCatalogId()
        {
            //Arrange
            const int catalogId = 23;

            var seeder = new VehicleOfferSeeder(DbContext);

            await seeder.AddRange(2, new { catalogId });

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/catalog/{catalogId}/many{_queryString}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetByCatalogId_WhenNotFound_ReturnsOk()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/catalog/{Fixture.Create<int>()}/many/{_queryString}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetAvailableOptions()
        {
            //Arrange
            var seeder = new VehicleOfferSeeder(DbContext);

            var vehicleOffer = seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{vehicleOffer.Id}/available/options");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetAvailableOptions_WhenNotFound_ReturnsOk()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<int>()}/available/options");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetAvailableColors()
        {
            //Arrange
            var vehicleOfferSeeder = new VehicleOfferSeeder(DbContext);
            var colorSeeder = new ColorSeeder(DbContext);

            var vehicleOffers = await vehicleOfferSeeder.AddRange(3);
            var colors = await colorSeeder.AddRange(6);

            foreach (var color in colors.SkipLast(2))
                foreach (var vehicleOffer in vehicleOffers)
                    vehicleOffer.AddColorOffer(color.Id, Fixture.Create<decimal>());

            await DbContext.SaveChangesAsync();

            var endpoint = $"{BaseUrl}/{vehicleOffers.Last().Id}/available/colors";

            //Act
            var response = await Client.GetAsync(endpoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetAvailableColors_WhenNothing_IsFound_ReturnsSuccess()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/{Fixture.Create<int>()}/available/colors";

            //Act
            var response = await Client.GetAsync(endpoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task UpdatePrice()
        {
            //Arrange
            var seeder = new VehicleOfferSeeder(DbContext);

            var vehicleOffer = await seeder.Add();

            await seeder.SeedAsync();

            var command = seeder.Build<UpdatePriceCommand>()
                .With(c => c.Id, vehicleOffer.Id)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update/price", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task UpdatePrice_WhenNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<UpdatePriceCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update/price", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task UpdatePrice_GivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update/price", null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ChangeStatus()
        {
            //Arrange
            var seeder = new VehicleOfferSeeder(DbContext);

            var vehicleOffer = await seeder.Add();
            var color = seeder.Create<Color>();
            await seeder.Add<ColorType, ColorTypeId>(color.TypeId);
            await seeder.Add<Color, ColorId>(color);
            vehicleOffer.AddColorOffer(color.Id, seeder.Create<decimal>());
            vehicleOffer.SetDefaultColor(color.Id);

            await seeder.SeedAsync();

            var command = seeder.Build<ChangeStatusCommand>()
                .With(c => c.VehicleOfferId, vehicleOffer.Id)
                .With(c => c.Status, VehicleOfferStatus.Published)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/status", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ChangeStatus_WhenNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<ChangeStatusCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/status", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ChangeStatus_GivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/status", null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ChangeRelevancePeriod()
        {
            //Arrange
            var seeder = new VehicleOfferSeeder(DbContext);
            var offer = await seeder.Add();
            await seeder.SeedAsync();

            var catalog = await DbContext.Catalogs.FirstOrDefaultAsync();

            var periodModel = new PeriodModel
            {
                From = catalog.RelevancePeriod.From.GetValueOrDefault().AddDays(1),
                To = catalog.RelevancePeriod.To?.AddDays(-1)
            };

            var endpoint = $"{BaseUrl}/{offer.Id}/change/relevance-period";

            //Act
            var response = await Client.PutCommand(endpoint, periodModel);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ChangeRelevancePeriod_WhenVehicleOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var periodModel = new PeriodModel { From = Fixture.Create<DateTime>() };
            var endpoint = $"{BaseUrl}/{999}/change/relevance-period";

            //Act
            var response = await Client.PutCommand(endpoint, periodModel);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ResetRelevancePeriod()
        {
            //Arrange
            var seeder = new VehicleOfferSeeder(DbContext);
            var vehicleOffer = await seeder.Add();
            await seeder.SeedAsync();

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/reset/relevance-period";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ResetRelevancePeriod_WhenVehicleOfferWasNotFound_ReturnsBadRequest()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/{999}/reset/relevance-period";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Actualize()
        {
            //Arrange
            var seeder = new VehicleOfferSeeder(DbContext);
            await seeder.Add();
            await seeder.SeedAsync();

            var endpoint = $"{BaseUrl}/actualize/relevance/many";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            response.EnsureSuccessStatusCode();
        }
    }
}
