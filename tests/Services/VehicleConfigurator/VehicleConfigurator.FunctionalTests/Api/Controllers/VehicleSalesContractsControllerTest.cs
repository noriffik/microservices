﻿using AutoFixture;
using NexCore.DealerNetwork;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Api.Application.Models.Contracts;
using NexCore.VehicleConfigurator.Domain.Contracts;
using NexCore.VehicleConfigurator.Domain.Contracts.VehicleSales;
using NexCore.VehicleConfigurator.Domain.Customers;
using NexCore.VehicleConfigurator.Domain.Dealers;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using NexCore.VehicleConfigurator.FunctionalTests.Properties;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers
{
    public class VehicleSalesContractsControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/contracts/vehicle-sales";

        public VehicleSalesContractsControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var seeder = new OfferSeeder(DbContext);

            var contract = await seeder.Add<VehicleSaleContract>();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{contract.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenContract_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var seeder = new OfferSeeder(DbContext);
            
            var privateCustomer = await seeder.Add<PrivateCustomer>();

            var dealer = await seeder.Add<Dealer, DealerId>();

            var model = await seeder.Add<Model, ModelId>();

            var color = await seeder.Add<Color, ColorId>();

            var contractType = await seeder.Add<ContractType, ContractTypeId>();
            
            await DbContext.SaveChangesAsync();

            await seeder.SeedAsync();

            var vehicle = new VehicleSpecificationModel
            {
                ModelYear = 1349,
                ModelKey = model.Id.Value+"1234",
                ColorId = color.Id.Value,
                AdditionalOptions = "AAA",
                IncludedOptions = "CCC",
                PackageId = "AA123"
            };

            var command = seeder.Build<AddCommand>()
                .With(c => c.ContractTypeId, contractType.Id.Value)
                .With(c => c.PrivateCustomerId, privateCustomer.Id)
                .With(c => c.DealerId, dealer.Id.Value)
                .With(c => c.Vehicle, vehicle)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task AddWithOffer()
        {
            //Arrange
            var seeder = new OfferSeeder(DbContext);

            var privateCustomer = await seeder.Add<PrivateCustomer>();

            var dealer = await seeder.Add<Dealer, DealerId>();

            var contractType = await seeder.Add<ContractType, ContractTypeId>();

            var model = await seeder.Add<Model, ModelId>();

            var offer = await seeder.Add(new { modelId = model.Id });

            await DbContext.SaveChangesAsync();

            await seeder.SeedAsync();
            
            var command = seeder.Build<AddWithOfferCommand>()
                .With(c => c.ContractTypeId, contractType.Id.Value)
                .With(c => c.PrivateCustomerId, privateCustomer.Id)
                .With(c => c.DealerId, dealer.Id.Value)
                .With(c => c.OfferId, offer.Id)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add-with-offer", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            var seeder = new OfferSeeder(DbContext);

            await seeder.AddRange<VehicleSaleContract>(6);

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync(BaseUrl);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetAll_With_DealerId()
        {
            //Arrange
            var seeder = new OfferSeeder(DbContext);

            var contracts = await seeder.AddRange<VehicleSaleContract>(6);

            await seeder.SeedAsync();

            var queryString = $"?DealerId={contracts[0].DealerId.Value}";

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{queryString}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetAll_With_PrivateCustomerId()
        {
            //Arrange
            var seeder = new OfferSeeder(DbContext);

            var contracts = await seeder.AddRange<VehicleSaleContract>(6);

            await seeder.SeedAsync();

            var queryString = $"?PrivateCustomerId={contracts[0].PrivateCustomerId}";

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{queryString}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetAll_WhenNotFound_ReturnsOk()
        {
            //Act
            var response = await Client.GetAsync(BaseUrl);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_Docx()
        {
            //Arrange
            var seeder = new OfferSeeder(DbContext);
            
            var dealer = await seeder.Add<Dealer, DealerId>();
            var template = await seeder.Add<ContractTemplate>(new
                {dealerId = dealer.Id.Value, content = Resources.ContractTemplateTest});
            var content = seeder.Fixture.Construct<VehicleSaleContent>(new {dealer});
            var contract = await seeder.Add<VehicleSaleContract>(new { typeId = template.TypeId, content });

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{contract.Id}/docx");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_Docx_WhenContract_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/1234/docx");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
