﻿using AutoFixture;
using NexCore.Common;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.Catalogs;
using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Api.Application.Queries.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers
{
    public class CatalogsControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/catalogs";
        
        public CatalogsControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var seeder = new CatalogSeeder(DbContext);

            var catalog = await seeder.Add();
            
            var command = new AddCommand
            {
                RelevancePeriod = new PeriodModel
                {
                    From = catalog.RelevancePeriod.From.GetValueOrDefault(),
                    To = catalog.RelevancePeriod.To
                },
                Year = catalog.ModelYear
            };

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add_WhenId_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Build<AddCommand>()
                .With(c => c.Year, 1123124)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Add_WhenPeriod_IsInUse_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new CatalogSeeder(DbContext);

            var catalog = await seeder.Add();

            await seeder.SeedAsync();
            
            var command = new AddCommand
            {
                RelevancePeriod = new PeriodModel
                {
                    From = catalog.RelevancePeriod.From.GetValueOrDefault(),
                    To = catalog.RelevancePeriod.To
                },
                Year = catalog.ModelYear
            };

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var seeder = new CatalogSeeder(DbContext);

            var catalog = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{catalog.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenCatalog_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetMany()
        {
            //Arrange
            var seeder = new CatalogSeeder(DbContext);

            await seeder.AddRange(7);

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetMany_WhenNotFound_ReturnsOk()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Outdated()
        {
            //Arrange
            var seeder = new CatalogSeeder(DbContext);

            await seeder.AddRange(8);
            
            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many/outdated");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Outdated_WhenNotFound_ReturnsOk()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many/outdated");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetActual()
        {
            //Arrange
            var seeder = new CatalogSeeder(DbContext);

            var relevancePeriod = new Period(Year.From(SystemTimeProvider.Instance.Get()));
            var catalog = await seeder.Add(new {modelYear = new Year(2019), period = relevancePeriod});

            await seeder.SeedAsync();

            var query = new FindActualQuery
            {
                ModelYear = catalog.ModelYear.Value,
                Status = catalog.Status
            };

            var endpoint = $"{BaseUrl}/single/actual/{query.ModelYear}/{query.Status}";
            
            //Act
            var response = await Client.GetAsync(endpoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetActual_WhenCatalog_IsNotFound_ReturnsNotFound()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/single/actual/{Fixture.Create<int>()}/{CatalogStatus.Published}";
            
            //Act
            var response = await Client.GetAsync(endpoint);

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task ChangeStatus()
        {
            //Arrange
            var seeder = new CatalogSeeder(DbContext);

            var catalog = await seeder.Add();

            await seeder.SeedAsync();

            var command = Fixture.Build<ChangeStatusCommand>()
                .With(c => c.Id, catalog.Id)
                .Create();
            
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/status", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ChangeStatus_WhenCatalog_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<ChangeStatusCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/status", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ChangePeriod()
        {
            //Arrange
            var seeder = new CatalogSeeder(DbContext);

            var catalog = await seeder.Add();

            await seeder.SeedAsync();

            var periodModel = new PeriodModel
            {
                From = catalog.RelevancePeriod.From.GetValueOrDefault(),
                To = catalog.RelevancePeriod.To
            };

            var command = new ChangeRelevancePeriodCommand
            {
                CatalogId = catalog.Id,
                RelevancePeriod = periodModel
            };

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/{catalog.Id}/change/relevance-period", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ChangePeriod_WhenCatalog_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<ChangeRelevancePeriodCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/{command.CatalogId}/change/relevance-period", command.RelevancePeriod);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ChangePeriod_WhenDateTime_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new CatalogSeeder(DbContext);

            var catalog = await seeder.Add();

            await seeder.SeedAsync();

            var periodModel = new PeriodModel
            {
                From = catalog.RelevancePeriod.From.GetValueOrDefault(),
                To = DateTime.MinValue
            };
            
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/{catalog.Id}/change/relevance-period", periodModel);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
