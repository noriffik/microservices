﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers.ModelComponents
{
    public class EquipmentsControllerTest : ModelComponentControllerTest<Equipment, EquipmentId>
    {
        public EquipmentsControllerTest(ServiceFactory<TestStartup> factory) : base("api/model/components/equipment", factory)
        {
        }
    }
}
