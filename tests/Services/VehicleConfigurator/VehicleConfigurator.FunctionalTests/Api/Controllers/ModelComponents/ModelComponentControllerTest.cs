﻿using AutoFixture;
using NexCore.Domain;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents;
using NexCore.VehicleConfigurator.Domain.Vehicles.BuildingBlocks;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers.ModelComponents
{
    public abstract class ModelComponentControllerTest<TComponent, TId> : ApiControllerTestFixture
        where TComponent : ModelComponent<TId>
        where TId : EntityId
    {
        protected readonly string BaseUrl;

        protected ModelComponentControllerTest(string baseUrl, ServiceFactory<TestStartup> factory) : base(factory)
        {
            BaseUrl = baseUrl;
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var seeder = new ModelComponentSeeder<TComponent, TId>(DbContext);

            //Arrange
            var entity = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{entity.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenEntity_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<TId>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetMany()
        {
            //Arrange
            var seeder = new ModelComponentSeeder<TComponent, TId>(DbContext);

            await seeder.AddRange(3);

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public virtual async Task GetByModel()
        {
            //Arrange
            var seeder = new ModelComponentSeeder<TComponent, TId>(DbContext);

            var entity = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many/model/{entity.Id.Value.Substring(0, 2)}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public virtual async Task Add()
        {
            //Arrange
            var seeder = new ModelComponentSeeder<TComponent, TId>(DbContext);

            var entity = seeder.Create();
            
            await seeder.AddDependencies(entity);

            await seeder.SeedAsync();

            var command = seeder.Build<AddCommand<TComponent, TId>>()
                .With(c => c.Id, entity.Id.Value)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public virtual async Task Add_GivenCommand_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<AddCommand<TComponent, TId>>();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public virtual async Task Add_WhenId_IsInUse_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new ModelComponentSeeder<TComponent, TId>(DbContext);

            var entity = await seeder.Add();

            await seeder.SeedAsync();

            var command = seeder.Build<AddCommand<TComponent, TId>>()
                .With(c => c.Id, entity.Id.Value)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
        
        [Fact]
        public virtual async Task Add_GivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public virtual async Task Update()
        {
            //Arrange
            var seeder = new ModelComponentSeeder<TComponent, TId>(DbContext);

            var body = await seeder.Add();

            await seeder.SeedAsync();

            var command = seeder.Build<UpdateCommand<TComponent, TId>>()
                .With(c => c.Id, body.Id.Value)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public virtual async Task Update_WhenId_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<UpdateCommand<TComponent, TId>>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public virtual async Task Update_GivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update", null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public virtual async Task Update_WhenComponent_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Build<UpdateCommand<TComponent, TId>>()
                .With(c => c.Id, Fixture.Create<TId>().Value)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Delete()
        {
            //Arrange
            var seeder = new ModelComponentSeeder<TComponent, TId>(DbContext);

            var entity = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.DeleteAsync($"{BaseUrl}/remove/{entity.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Delete_WhenComponent_IsNotFound_ReturnsOk()
        {
            //Act
            var response = await Client.DeleteAsync($"{BaseUrl}/remove/{Fixture.Create<TId>()}");

            //Assert
            response.EnsureSuccessStatusCode();
        }
    }
}
