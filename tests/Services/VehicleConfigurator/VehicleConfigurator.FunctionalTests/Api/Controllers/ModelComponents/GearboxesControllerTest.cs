﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Gearboxes;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers.ModelComponents
{
    public class GearboxesControllerTest : ModelComponentControllerTest<Gearbox, GearboxId>
    {
        public GearboxesControllerTest(ServiceFactory<TestStartup> factory) : base("api/model/components/gearbox", factory)
        {
        }

        [Fact]
        public override async Task Add()
        {
            //Arrange
            var seeder = new ModelComponentSeeder<Gearbox, GearboxId>(DbContext);

            var entity = seeder.Create();
            var command = seeder.Build<AddCommand>()
                .With(c => c.Id, entity.Id.Value)
                .With(c => c.Type, entity.Type.Value)
                .Create();

            await seeder.AddDependencies(entity);

            await seeder.SeedAsync();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public override async Task Add_WhenId_IsInUse_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new ModelComponentSeeder<Gearbox, GearboxId>(DbContext);

            var entity = await seeder.Add();

            await seeder.SeedAsync();

            var command = seeder.Build<AddCommand>()
                .With(c => c.Id, entity.Id.Value)
                .With(c => c.Type, entity.Type.Value)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public override async Task Update()
        {
            //Arrange
            var seeder = new ModelComponentSeeder<Gearbox, GearboxId>(DbContext);

            var gearbox = await seeder.Add();

            await seeder.SeedAsync();

            var command = seeder.Build<UpdateCommand>()
                .With(c => c.Id, gearbox.Id.Value)
                .With(c => c.Type, gearbox.Type.Value)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }
        
        [Fact]
        public override async Task Update_WhenComponent_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Build<UpdateCommand>()
                .With(c => c.Id, Fixture.Create<GearboxId>().Value)
                .With(c => c.Type, Fixture.Create<GearboxType>().Value)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
