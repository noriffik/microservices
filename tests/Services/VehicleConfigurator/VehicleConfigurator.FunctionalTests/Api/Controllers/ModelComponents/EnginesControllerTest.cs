﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers.ModelComponents
{
    public class EnginesControllerTest : ModelComponentControllerTest<Engine, EngineId>
    {
        public EnginesControllerTest(ServiceFactory<TestStartup> factory) : base("api/model/components/engine",factory)
        {
        }
    }
}
