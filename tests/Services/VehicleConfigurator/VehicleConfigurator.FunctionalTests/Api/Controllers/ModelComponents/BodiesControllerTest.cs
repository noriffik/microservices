﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Vehicles;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers.ModelComponents
{
    public class BodiesControllerTest : ModelComponentControllerTest<Body, BodyId>
    {
        public BodiesControllerTest(ServiceFactory<TestStartup> factory) : base("api/model/components/body", factory)
        {
        }
    }
}
