﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.ModelComponents.Options;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers.ModelComponents
{
    public class OptionsControllerTest : ModelComponentControllerTest<Option, OptionId>
    {
        public OptionsControllerTest(ServiceFactory<TestStartup> factory)
            : base("api/model/components/option", factory)
        {
        }

        [Fact]
        public async Task GetByPrNumberSet()
        {
            //Arrange
            var seeder = new ModelComponentSeeder<Option, OptionId>(DbContext);

            var modelId = seeder.Create<ModelId>();
            var optionIds = seeder.CreateMany<PrNumber>(3).Select(n => OptionId.Next(modelId, n)).ToArray();
            
            await seeder.AddRange(optionIds);

            await seeder.SeedAsync();

            var prNumbers = optionIds.Select(o => o.PrNumber.Value).Aggregate((a, b) => $"{a} {b}");
            var endpoint =  $"{BaseUrl}/many/pr-number-set/{modelId}/{prNumbers}";

            //Act
            var response = await Client.GetAsync(endpoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }
        
        [Fact]
        public async Task Add_GivenPrNumber_IsInUse_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new ModelComponentSeeder<Option, OptionId>(DbContext);

            var option = await seeder.Add();

            await seeder.SeedAsync();

            var command = seeder.Build<AddCommand>()
                .With(c => c.Id, option.Id.Value)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
