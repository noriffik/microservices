﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers
{
    public class ModelKeysControllerTest : ApiControllerTestFixture
    {
        private const int CatalogId = 1;

        private const string BaseUrl = "api/modelkeys";

        public ModelKeysControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task GetMany()
        {
            //Arrange
            var seeder = new VehicleOfferSeeder(DbContext);
            
            var optionOffer = seeder.Create<OptionOffer>();
            var vehicleOffer = Enumerable.Range(1, 5)
                .Select(i => seeder.Fixture.Construct<VehicleOffer>(new {CatalogId})).ToList();

            var expectedVehicleOffers = vehicleOffer.Take(2).ToList();

            foreach (var vehicle in expectedVehicleOffers)
            {
                vehicle.AddOptionOffer(optionOffer.PrNumber, optionOffer.Availability);
            }

            await seeder.AddRange(vehicleOffer.ToArray());

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many/{CatalogId}/{optionOffer.OptionId}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetMany_WhenNotFound_ReturnsOk()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/many/{Fixture.Create<int>()}/{Fixture.Create<OptionId>()}";

            //Act
            var response = await Client.GetAsync(endpoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }
    }
}
