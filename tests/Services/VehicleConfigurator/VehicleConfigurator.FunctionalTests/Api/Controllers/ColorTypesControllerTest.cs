﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Api.Application.Commands.ColorTypes;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers
{
    public class ColorTypesControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/colors/types";

        public ColorTypesControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var command = Fixture.Build<AddCommand>()
                .With(c => c.Id, Fixture.Create<ColorTypeId>().Value)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add_WhenColorType_AlreadyExist_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new EntitySeeder<ColorType, ColorTypeId>(DbContext, Fixture);

            var colorType = await seeder.Add();

            await seeder.SeedAsync();

            var command = seeder.Build<AddCommand>()
                .With(c => c.Id, colorType.Id.Value)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var seeder = new EntitySeeder<ColorType, ColorTypeId>(DbContext, Fixture);

            var colorType = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{colorType.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenColorType_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<ColorTypeId>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetMany()
        {
            //Arrange
            var seeder = new EntitySeeder<ColorType, ColorTypeId>(DbContext, Fixture);

            await seeder.AddRange(5);

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetMany_WhenNotFound_ReturnsOk()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Rename()
        {
            //Arrange
            var seeder = new EntitySeeder<ColorType, ColorTypeId>(DbContext, Fixture);

            var colorType = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/{colorType.Id.Value}/rename", Fixture.Create<string>());

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Rename_WhenColorType_IsNotFound_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/{Fixture.Create<ColorTypeId>().Value}/rename",
                Fixture.Create<string>());

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
