﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionCategories;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers
{
    public class OptionCategoriesControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/components/option/category";
        
        public OptionCategoriesControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var command = Fixture.Create<AddCommand>();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add_GivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var seeder = new EntitySeeder<OptionCategory>(DbContext, Fixture);

            var category = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{category.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenCategory_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task Assign()
        {
            //Arrange
            var seeder = new EntitySeeder<OptionCategory>(DbContext, Fixture);

            var category = await seeder.Add();
            var option = await seeder.Add<Option, OptionId>();

            await seeder.SeedAsync();

            var command = Fixture.Build<AssignCommand>()
                .With(c => c.OptionId, option.Id.Value)
                .With(c => c.CategoryId, category.Id)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/assign", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Assign_WhenOption_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new EntitySeeder<OptionCategory>(DbContext, Fixture);

            var optionCategory = await seeder.Add();

            await seeder.SeedAsync();
            
            var command = Fixture.Build<AssignCommand>()
                .With(c => c.CategoryId, optionCategory.Id)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/assign", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Assign_WhenOptionCategory_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new EntitySeeder<OptionCategory>(DbContext, Fixture);

            var option = await seeder.Add<Option, OptionId>();
            await seeder.SeedAsync();

            var command = seeder.Build<AssignCommand>()
                .With(c => c.OptionId, option.Id.Value)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/assign", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Assign_GivenCommand_IsInvalid_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/assign", null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task UnAssign()
        {
            //Arrange
            var seeder = new EntitySeeder<OptionCategory>(DbContext, Fixture);

            var option = await seeder.Add<Option, OptionId>();
            option.OptionCategoryId = seeder.Add().Id;

            await seeder.SeedAsync();

            var command = seeder.Build<UnassignCommand>()
                .With(c => c.Id, option.Id.Value)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/unassign", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task UnAssign_WhenOption_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<UnassignCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/unassign", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task UnAssign_GivenCommand_IsInvalid_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/unassign", null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Update()
        {
            //Arrange
            var seeder = new EntitySeeder<OptionCategory>(DbContext, Fixture);

            var category = await seeder.Add();

            await seeder.SeedAsync();

            var command = Fixture.Build<UpdateCommand>()
                .With(c => c.Id, category.Id)
                .Create();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Update_WhenNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<UpdateCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Update_GivenCommand_IsInvalid_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update", null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
        
        [Fact]
        public async Task Delete()
        {
            //Arrange
            var seeder = new EntitySeeder<OptionCategory>(DbContext, Fixture);

            var category = seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.DeleteAsync($"{BaseUrl}/remove/{category.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Delete_WhenNotFound_ReturnsSuccessResult()
        {
            //Act
            var response = await Client.DeleteAsync($"{BaseUrl}/remove/{Fixture.Create<int>()}");

            //Assert
            response.EnsureSuccessStatusCode();
        }
    }
}
