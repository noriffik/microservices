﻿using AutoFixture;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.Configurations;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Configurations;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using NexCore.VehicleConfigurator.UnitTests.Extensions.Domain;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers
{
    public class ConfigurationsControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/configurations";
        
        public ConfigurationsControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext); 

            var configuration = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{configuration.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenConfiguration_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<int>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetMany()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext);

            await seeder.AddRange(2);

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetMany_WhenNotFound_ReturnsOk()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }
        
        [Fact]
        public async Task Preview()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext);

            var modelYear = Year.From(2011);
            var period = new Period(DateTime.UtcNow.AddDays(-1));
            var catalog = await seeder.Add<Catalog>(12, new {modelYear, period});
            catalog.Status = CatalogStatus.Published;

            var configuration = seeder.Create(new {catalogId = catalog.Id});

            await seeder.AddDependencies(configuration);

            await DbContext.SaveChangesAsync();

            var endpoint = $"{BaseUrl}/preview/{catalog.ModelYear}/{configuration.ModelKey}/{configuration.Options.AsString}";

            //Act
            var response = await Client.GetAsync(endpoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetDetails()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext);
            
            var configuration = await seeder.Add();

            await seeder.SeedAsync();
            
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{configuration.Id}/details");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetDetails_WhenConfiguration_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<int>()}/details");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetByCatalogId()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext);

            await seeder.AddRange(2, new {catalogId = 1});

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/catalog/1/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetByCatalogId_WhenNotFound_ReturnsSuccess()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/catalog/1/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext);

            var configuration = seeder.Create();

            await seeder.AddDependencies(configuration);

            await seeder.SeedAsync();

            var command = seeder.Build<AddCommand>()
                .With(c => c.ModelKey, configuration.ModelKey.Value)
                .With(c => c.CatalogId, configuration.CatalogId)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add_WhenCatalog_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext);

            var configuration = seeder.Create();

            await seeder.AddDependencies(configuration);

            await seeder.SeedAsync();

            var command = new AddCommand
            {
                CatalogId = 1245,
                ModelKey = configuration.ModelKey
            };

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Copy()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext);

            var configuration = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/copy/{configuration.Id}", null);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Copy_WhenConfiguration_IsNotFound_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PostCommand($"{BaseUrl}/copy/{Fixture.Create<int>()}", null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task IncludeOption()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext);

            var option = seeder.Create<PrNumber>();
            var configuration = await seeder.Add();
            var vehicle = seeder.Get<VehicleOffer>().First();

            vehicle.AddOptionOffer(option, seeder.Create<Availability>());

            await seeder.SeedAsync();

            var command = new IncludeOptionCommand
            {
                ConfigurationId = configuration.Id,
                PrNumber = option.Value
            };
            var endpoint = $"{BaseUrl}/include/option";

            //Act
            var response = await Client.PutCommand(endpoint, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task IncludeOption_WhenConfiguration_WasNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<IncludeOptionCommand>();
            var endpoint = $"{BaseUrl}/include/option";

            //Act
            var response = await Client.PutCommand(endpoint, command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ExcludeOption()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext);

            var option = seeder.Create<PrNumber>();
            var configuration = await seeder.Add();
            var vehicle = seeder.Get<VehicleOffer>().First();

            vehicle.AddOptionOffer(option, seeder.Create<Availability>());
            configuration.IncludeOption(option);

            await seeder.SeedAsync();

            var command = new ExcludeOptionCommand
            {
                ConfigurationId = configuration.Id,
                PrNumber = option.Value
            };
            
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/exclude/option", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Exclude_WhenConfiguration_WasNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<ExcludeOptionCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/exclude/option", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ChangeStatus()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext);

            var configuration = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var command = new ChangeStatusCommand
            {
                ConfigurationId = configuration.Id,
                Status = ConfigurationStatus.Completed
            };

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/status", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ChangeStatus_WhenConfiguration_IsNotFound_ReturnsBadRequest()
        {
            //Act
            var command = Fixture.Create<ChangeStatusCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/status", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task IncludePackage()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext);

            var configuration = await seeder.Add();

            var packageCode = seeder.Create<PackageCode>();
            var packageId = PackageId.Next(configuration.ModelKey.ModelId, packageCode);
            var package = await seeder.Add<Package, PackageId>(packageId, new
            {
                catalogId = configuration.CatalogId,
                modelKeySet = BoundModelKeySet.Parse(configuration.ModelKey.Value),
            });

            package.ChangeStatus(PackageStatus.Published);

            //Seed package's options
            await seeder.AddRange<Option, OptionId>(
                OptionId.Next(configuration.ModelKey.ModelId, package.Applicability.PrNumbers));

            //Include options needed by package
            configuration.IncludeOptions(package.ApplicableWith());

            await seeder.SeedAsync();

            var command = new IncludePackageCommand
            {
                ConfigurationId = configuration.Id,
                PackageCode = packageCode
            };

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/include/package", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task IncludePackage_WhenConfiguration_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<IncludePackageCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/include/package", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ExcludePackage()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext);
            var configuration = await seeder.Add();

            var package = await seeder.Add<Package, PackageId>(
                PackageId.Next(configuration.ModelKey.ModelId, PackageCode.Parse("QWE")),
                new
                {
                    catalogId = configuration.CatalogId,
                    modelKeySet = BoundModelKeySet.Parse(configuration.ModelKey.Value),
                });

            //Seed package's options
            await seeder.AddRange<Option, OptionId>(
                OptionId.Next(configuration.ModelKey.ModelId, package.Applicability.PrNumbers));

            //Include options needed by package
            configuration.IncludeOptions(package.ApplicableWith());

            //Include package
            configuration.IncludePackage(package.Id);

            await seeder.SeedAsync();

            var command = new ExcludePackageCommand {ConfigurationId = configuration.Id};
            
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/exclude/package", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ExcludePackage_WhenConfiguration_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<ExcludePackageCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/exclude/package", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ChangeColor()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext);
            
            var configuration = await seeder.Add();
            var vehicleOffer = seeder.Get<VehicleOffer>()
                .Where(o => o.CatalogId == configuration.CatalogId)
                .Single(o => o.ModelKey == configuration.ModelKey);

            var color = await seeder.Add<Color, ColorId>();
            await seeder.Add<ColorType, ColorTypeId>(color.TypeId);
            vehicleOffer.AddColorOffer(color.Id, 100);

            await seeder.SeedAsync();

            var command = new ChangeColorCommand
            {
                ColorId = color.Id,
                ConfigurationId = configuration.Id
            };

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/color", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ChangeColor_WhenColorOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext);

            var configuration = await seeder.Add();
            var colorType = await seeder.Add<ColorType, ColorTypeId>();
            var color = await seeder.Add<Color, ColorId>(new {typeId = colorType.Id});

            await seeder.SeedAsync();

            var command = new ChangeColorCommand
            {
                ColorId = color.Id,
                ConfigurationId = configuration.Id
            };

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/change/color", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Validate_WhenNotValid_ReturnsOk()
        {
            //Arrange
            
            var seeder = new ConfigurationSeeder(DbContext);
            
            var modelYear = Year.From(2011);
            var period = new Period(DateTime.UtcNow.AddDays(-1));
            var catalog = await seeder.Add<Catalog>(12, new {modelYear, period});
            catalog.Status = CatalogStatus.Published;

            var prNumber = PrNumber.Parse("AAA");

            var vehicleOffer = await seeder.Add<VehicleOffer>(13, new
            {
                catalogId = catalog.Id
            });
            vehicleOffer.AddOptionOffer(prNumber, Availability.Purchasable(1.5M));
            vehicleOffer.AddOptionOfferRuleSet(prNumber, "[VA: BBB +[]]");
            
            var configuration = seeder.Create(new
            {
                catalogId = vehicleOffer.CatalogId,
                modelKey = vehicleOffer.ModelKey
            });

            await seeder.AddDependencies(configuration);

            await DbContext.SaveChangesAsync();
            
            var endpoint = $"{BaseUrl}/validate/{catalog.ModelYear.Value}/{vehicleOffer.ModelKey.Value}/BBB";

            //Act
            var response = await Client.GetAsync(endpoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Validate_WhenValid_ReturnsOk()
        {
            //Arrange
            
            var seeder = new ConfigurationSeeder(DbContext);
            
            var modelYear = Year.From(2011);
            var period = new Period(DateTime.UtcNow.AddDays(-1));
            var catalog = await seeder.Add<Catalog>(12, new {modelYear, period});
            catalog.Status = CatalogStatus.Published;

            var prNumber = PrNumber.Parse("AAA");

            var vehicleOffer = await seeder.Add<VehicleOffer>(13, new
            {
                catalogId = catalog.Id
            });
            vehicleOffer.AddOptionOffer(prNumber, Availability.Purchasable(1.5M));
            
            var configuration = seeder.Create(new
            {
                catalogId = vehicleOffer.CatalogId,
                modelKey = vehicleOffer.ModelKey
            });

            await seeder.AddDependencies(configuration);

            await DbContext.SaveChangesAsync();
            
            var endpoint = $"{BaseUrl}/validate/{catalog.ModelYear.Value}/{vehicleOffer.ModelKey.Value}/BBB";

            //Act
            var response = await Client.GetAsync(endpoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Remove()
        {
            //Arrange
            var seeder = new ConfigurationSeeder(DbContext);

            var configuration = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.DeleteAsync($"{BaseUrl}/remove/{configuration.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Remove_WhenConfiguration_IsNotFound_ReturnsSuccess()
        {
            //Act
            var response = await Client.DeleteAsync($"{BaseUrl}/remove/{777}");

            //Assert
            response.EnsureSuccessStatusCode();
        }
    }
}
