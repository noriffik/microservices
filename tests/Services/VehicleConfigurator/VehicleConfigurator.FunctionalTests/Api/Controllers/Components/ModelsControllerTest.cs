﻿using AutoFixture;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.Testing.Seeders;
using NexCore.VehicleConfigurator.Api.Application.Commands.Components.Models;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers.Components
{
    public class ModelsControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/model";
        
        public ModelsControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task Get()
        {
            //Arrange
            var seeder = new EntitySeeder<Model, ModelId>(DbContext, Fixture);

            var model = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{model.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Get_WhenModel_IsNotFound_ReturnsNotFound()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{Fixture.Create<ModelId>()}");

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetMany()
        {
            //Arrange
            var seeder = new EntitySeeder<Model, ModelId>(DbContext, Fixture);

            await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetMany_WhenNotFound_ReturnsOk()
        {
            //Act
            var response = await Client.GetAsync($"{BaseUrl}/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var command = Fixture.Build<AddCommand>()
                .With(c => c.Id, Fixture.Create<ModelId>().Value)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add_WhenId_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<AddCommand>();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Add_WhenId_IsInUse_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new EntitySeeder<Model, ModelId>(DbContext, Fixture);

            var model = await seeder.Add();

            await seeder.SeedAsync();

            var command = seeder.Build<AddCommand>()
                .With(c => c.Id, model.Id.Value)
                .Create();

            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
        
        [Fact]
        public async Task Add_GivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PostCommand($"{BaseUrl}/add", null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
        
        [Fact]
        public async Task Delete()
        {
            //Arrange
            var seeder = new EntitySeeder<Model, ModelId>(DbContext, Fixture);

            var model = await seeder.Add();

            await seeder.SeedAsync();

            //Act
            var response = await Client.DeleteAsync($"{BaseUrl}/remove/{model.Id}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Delete_WhenNotFound_ReturnsSuccessResult()
        {
            //Act
            var response = await Client.DeleteAsync($"{BaseUrl}/remove/{Fixture.Create<ModelId>()}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Update()
        {
            //Arrange
            var seeder = new EntitySeeder<Model, ModelId>(DbContext, Fixture);

            var model = await seeder.Add();

            await seeder.SeedAsync();

            var command = seeder.Build<UpdateCommand>()
                .With(c => c.Id, model.Id.Value)
                .Create();
            
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update", command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Update_WhenId_IsInvalid_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<UpdateCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Update_GivenCommand_IsNull_ReturnsBadRequest()
        {
            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update", null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Update_WhenModel_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var command = Fixture.Create<UpdateCommand>();

            //Act
            var response = await Client.PutCommand($"{BaseUrl}/update", command);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
