﻿using AutoFixture;
using NexCore.Common;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers
{
    public class ColorOffersControllerTest : ApiControllerTestFixture
    {
        private const string BaseUrl = "api/catalogs/vehicle";

        public ColorOffersControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
        }

        [Fact]
        public async Task GetByVehicleOfferIdAndColorId()
        {
            //Arrange
            var vehicleOfferSeeder = new VehicleOfferSeeder(DbContext);
            var colorSeeder = new ColorSeeder(DbContext);

            var vehicleOffer = await vehicleOfferSeeder.Add();
            var color = await colorSeeder.Add();
            vehicleOffer.AddColorOffer(color.Id, Fixture.Create<decimal>());

            await DbContext.SaveChangesAsync();

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/color/{color.Id.Value}";

            //Act
            var response = await Client.GetAsync(endpoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetByVehicleOfferIdAndColorId_WhenIsNotFound_ReturnsNotFound()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/{Fixture.Create<int>()}/color/{Fixture.Create<ColorId>().Value}";

            //Act
            var response = await Client.GetAsync(endpoint);

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetByVehicleOfferId()
        {
            //Arrange
            var vehicleOfferSeeder = new VehicleOfferSeeder(DbContext);
            var colorSeeder = new ColorSeeder(DbContext);

            var vehicleOffer = await vehicleOfferSeeder.Add();
            var colors = await colorSeeder.AddRange(3);
            foreach (var color in colors)
                vehicleOffer.AddColorOffer(color.Id, Fixture.Create<decimal>());

            await DbContext.SaveChangesAsync();

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/color/many";

            //Act
            var response = await Client.GetAsync(endpoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var vehicleOfferSeeder = new VehicleOfferSeeder(DbContext);
            var colorSeeder = new ColorSeeder(DbContext);

            var vehicleOffer = await vehicleOfferSeeder.Add();
            var color = await colorSeeder.Add();

            await DbContext.SaveChangesAsync();

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/color/add/{color.Id}";

            //Act
            var response = await Client.PostCommand(endpoint, Fixture.Create<decimal>());

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add_WhenVehicleOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new ColorSeeder(DbContext);

            var color = seeder.Add();

            await DbContext.SaveChangesAsync();

            var endpoint = $"{BaseUrl}/{Fixture.Create<int>()}/color/add/{color.Id}";

            //Act
            var response = await Client.PostCommand(endpoint, Fixture.Create<decimal>());

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task SetDefaultColor()
        {
            //Arrange
            var vehicleOfferSeeder = new VehicleOfferSeeder(DbContext);
            var colorSeeder = new ColorSeeder(DbContext);

            var vehicleOffer = await vehicleOfferSeeder.Add();
            var color = await colorSeeder.Add();
            vehicleOffer.AddColorOffer(color.Id, vehicleOfferSeeder.Create<decimal>());

            await DbContext.SaveChangesAsync();

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/color/default/{color.Id}";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task SetDefaultColor_WhenColorOffer_ForDefaultColor_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var vehicleOfferSeeder = new VehicleOfferSeeder(DbContext);

            var vehicleOffer = vehicleOfferSeeder.Add();

            await vehicleOfferSeeder.SeedAsync();

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/color/default/{Fixture.Create<ColorId>()}";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ChangeRelevancePeriod()
        {
            var vehicleOfferSeeder = new VehicleOfferSeeder(DbContext);
            var colorSeeder = new ColorSeeder(DbContext);

            var vehicleOffer = await vehicleOfferSeeder.Add();
            var color = await colorSeeder.Add();
            vehicleOffer.AddColorOffer(color.Id, Fixture.Create<decimal>());

            await DbContext.SaveChangesAsync();

            var catalog = vehicleOfferSeeder.Find<Catalog>(vehicleOffer.CatalogId);
            var periodModel = new PeriodModel
            {
                From = catalog.RelevancePeriod.From.GetValueOrDefault(),
                To = catalog.RelevancePeriod.To
            };

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/color/{color.Id.Value}/change/relevance-period";

            //Act
            var response = await Client.PutCommand(endpoint, periodModel);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ChangeRelevancePeriod_WhenVehicleOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var periodModel = new PeriodModel { From = Fixture.Create<DateTime>() };
            var endpoint = $"{BaseUrl}/{Fixture.Create<int>()}/color/{Fixture.Create<ColorId>().Value}/change/relevance-period";

            //Act
            var response = await Client.PutCommand(endpoint, periodModel);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ResetRelevancePeriod()
        {
            var vehicleOfferSeeder = new VehicleOfferSeeder(DbContext);
            var colorSeeder = new ColorSeeder(DbContext);

            var vehicleOffer = await vehicleOfferSeeder.Add();
            var color = await colorSeeder.Add();
            var catalog = vehicleOfferSeeder.Find<Catalog>(vehicleOffer.CatalogId);

            var colorOffer = vehicleOffer.AddColorOffer(color.Id, Fixture.Create<decimal>());
            colorOffer.ChangeRelevancePeriod(new RelevancePeriod(catalog.RelevancePeriod));

            await DbContext.SaveChangesAsync();

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/color/{color.Id.Value}/reset/relevance-period";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ResetRelevancePeriod_WhenVehicleOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/{Fixture.Create<int>()}/color/{Fixture.Create<ColorId>().Value}/reset/relevance-period";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ActualizeRelevanceInCatalog()
        {
            //Arrange
            var vehicleOfferSeeder = new VehicleOfferSeeder(DbContext);
            var colorSeeder = new ColorSeeder(DbContext);

            var vehicleOffer = await vehicleOfferSeeder.Add();
            var color = await colorSeeder.Add();
            vehicleOffer.AddColorOffer(color.Id, Fixture.Create<decimal>());

            await DbContext.SaveChangesAsync();

            var endpoint = $"{BaseUrl}/color/relevance/actualize/catalog/{vehicleOffer.CatalogId}";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ActualizeRelevanceInCatalog_WhenCatalog_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/color/relevance/actualize/catalog/{Fixture.Create<int>()}";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ActualizeRelevanceInRelevantCatalogs()
        {
            //Arrange
            var vehicleOfferSeeder = new VehicleOfferSeeder(DbContext);
            var colorSeeder = new ColorSeeder(DbContext);

            var vehicleOffer = await vehicleOfferSeeder.Add();
            var color = await colorSeeder.Add();
            vehicleOffer.AddColorOffer(color.Id, Fixture.Create<decimal>());

            var date = SystemTimeProvider.Instance.Get();
            var catalog = vehicleOfferSeeder.Find<Catalog>(vehicleOffer.CatalogId);
            catalog.ChangeRelevancePeriod(new Period(date.AddDays(-5)));

            await DbContext.SaveChangesAsync();

            var endpoint = $"{BaseUrl}/color/relevance/actualize";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ChangePrice()
        {
            var vehicleOfferSeeder = new VehicleOfferSeeder(DbContext);
            var colorSeeder = new ColorSeeder(DbContext);

            var vehicleOffer = await vehicleOfferSeeder.Add();
            var color = await colorSeeder.Add();
            vehicleOffer.AddColorOffer(color.Id, Fixture.Create<decimal>());

            await DbContext.SaveChangesAsync();

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/color/{color.Id.Value}/change/price";
            var price = Fixture.Create<decimal>();

            //Act
            var response = await Client.PutCommand(endpoint, price);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ChangePrice_WhenVehicleOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/{Fixture.Create<int>()}/color/{Fixture.Create<ColorId>().Value}/change/price";
            var price = Fixture.Create<decimal>();

            //Act
            var response = await Client.PutCommand(endpoint, price);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
