﻿using AutoFixture;
using NexCore.Common;
using NexCore.Domain;
using NexCore.Domain.Vehicles;
using NexCore.Testing.Extensions;
using NexCore.VehicleConfigurator.Api.Application.Commands.OptionOffers;
using NexCore.VehicleConfigurator.Api.Application.Models;
using NexCore.VehicleConfigurator.Api.Application.Models.OptionOffers;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Api.Controllers
{
    public class OptionOfferControllerTest : ApiControllerTestFixture
    {
        private enum SeedingOptions
        {
            None,
            SeedOption,
            WithOptionOffer,
            All = SeedOption | WithOptionOffer
        }

        private const string BaseUrl = "api/catalogs/vehicle";

        //Entities and models
        private readonly PrNumber _prNumber;
        private readonly Inclusion _inclusion;
        private readonly AvailabilityModel _availabilityModel;
        private readonly IEnumerable<AvailabilityPerModelKeyModel> _offers;
        private readonly RuleSetModel _ruleSetModel;

        public OptionOfferControllerTest(ServiceFactory<TestStartup> factory) : base(factory)
        {
            //Entities and models
            _availabilityModel = new AvailabilityModel
            {
                Type = AvailabilityType.Included,
                Reason = ReasonType.SerialEquipment,
                Price = 10
            };
            _ruleSetModel = new RuleSetModel
            {
                RuleSet = new List<RuleModel>
                {
                    new RuleModel{RuleName = "VA", PrNumbers = "AAA BBB", NestedRuleSet = null},
                    new RuleModel{RuleName = "ZA", PrNumbers = "CCC DDD", NestedRuleSet = null}
                }
            };
            _prNumber = Fixture.Create<PrNumber>();
            _inclusion = Fixture.Create<Inclusion>();

            _offers = new List<AvailabilityPerModelKeyModel>
            {
                new AvailabilityPerModelKeyModel
                {
                    ModelKey = "AA1111",
                    Availability = _availabilityModel
                }
            };
        }

        private async Task<VehicleOffer> SeedVehicleOffer(SeedingOptions options = SeedingOptions.None)
        {
            var seeder = new VehicleOfferSeeder(DbContext);

            var vehicleOffer = await seeder.Add();

            if (options.HasFlag(SeedingOptions.WithOptionOffer))
                vehicleOffer.AddOptionOffer(_prNumber, Availability.Purchasable(1.5M));

            if (options.HasFlag(SeedingOptions.SeedOption))
                await seeder.Add<Option, OptionId>(OptionId.Next(vehicleOffer.ModelId, _prNumber));

            await seeder.SeedAsync();

            return vehicleOffer;
        }

        [Fact]
        public async Task GetByVehicleOfferIdAndPrNumber()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.All);

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{vehicleOffer.Id}/option/{_prNumber}");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetByVehicleOfferIdAndPrNumber_WhenOptionOffer_IsNotFound_ReturnsNotFound()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer();

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/{Fixture.Create<PrNumber>()}";

            //Act
            var response = await Client.GetAsync(endpoint);

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetByVehicleOfferId()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.All);

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{vehicleOffer.Id}/option/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetByVehicleOfferId_WhenOptionOffers_AreNotFound_ReturnsSuccess()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer();

            //Act
            var response = await Client.GetAsync($"{BaseUrl}/{vehicleOffer.Id}/option/many");

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetByModelId()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.All);

            var endpoint = $"{BaseUrl}/catalog/{vehicleOffer.CatalogId}/model/{vehicleOffer.ModelId}/option/{_prNumber}/many";

            //Act
            var response = await Client.GetAsync(endpoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetByModelId_WhenOptionOffers_AreNotFound_ReturnsSuccess()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer();

            var endpoint = $"{BaseUrl}/catalog/{vehicleOffer.CatalogId}/model/{vehicleOffer.ModelId}/option/{_prNumber}/many";

            //Act
            var response = await Client.GetAsync(endpoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.SeedOption);

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/add/{_prNumber}";

            //Act
            var response = await Client.PostCommand(endpoint, _availabilityModel);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Add_WhenVehicleOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var seeder = new ModelComponentSeeder<Option, OptionId>(DbContext);

            await seeder.Add();

            await seeder.SeedAsync();

            var endpoint = $"{BaseUrl}/{Fixture.Create<int>()}/option/add/{_prNumber}";

            //Act
            var response = await Client.PostCommand(endpoint, _availabilityModel);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Add_WhenOption_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer();

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/add/{Fixture.Create<PrNumber>()}";

            //Act
            var response = await Client.PostCommand(endpoint, _availabilityModel);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Add_WhenOption_WasAlreadyAdded_ReturnsBadRequest()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.All);

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/add/{_prNumber}";

            //Act
            var response = await Client.PostCommand(endpoint, _availabilityModel);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task Remove()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.All);

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/remove/{_prNumber}";

            //Act
            var response = await Client.DeleteAsync(endpoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Remove_WhenOptionOffer_IsNotFound_ReturnsSuccess()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer();

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/remove/{Fixture.Create<PrNumber>()}";

            //Act
            var response = await Client.DeleteAsync(endpoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task UpdateAvailability()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.All);

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/update/availability/{_prNumber}";

            //Act
            var response = await Client.PutCommand(endpoint, _availabilityModel);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task UpdateAvailability_WhenVehicleOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/{Fixture.Create<int>()}/option/update/availability/{Fixture.Create<PrNumber>()}";

            //Act
            var response = await Client.PutCommand(endpoint, _availabilityModel);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task UpdateAvailability_WhenOptionOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.SeedOption);

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/update/availability/{Fixture.Create<PrNumber>()}";

            //Act
            var response = await Client.PutCommand(endpoint, _availabilityModel);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task UpdateAvailabilities()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.All);

            _offers.First().ModelKey = vehicleOffer.ModelKey.Value;

            var endpoint = $"{BaseUrl}/option/update/many/availability/{_prNumber}";

            //Act
            var response = await Client.PutCommand(endpoint, _offers);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task UpdateAvailabilities_WhenNotFound_ReturnsBadRequest()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/option/update/many/availability/{Fixture.Create<PrNumber>()}";

            //Act
            var response = await Client.PutCommand(endpoint, _offers);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task UpdateInclusion()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.All);

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/update/inclusion/{_prNumber}";

            //Act
            var response = await Client.PutCommand(endpoint, _inclusion);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task UpdateInclusion_WhenVehicleOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/{Fixture.Create<int>()}/option/update/inclusion/{_prNumber}";

            //Act
            var response = await Client.PutCommand(endpoint, _inclusion);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task AddRuleSet()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.All);

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/add/rule-set/{_prNumber}";

            //Act
            var response = await Client.PutCommand(endpoint, _ruleSetModel);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task AddRuleSet_WithDeepDependencyConflict_ReturnsBadRequest()
        {
            //Arrange

            var seeder = new VehicleOfferSeeder(DbContext);
            
            var prNumberA = PrNumber.Parse("AAA");
            var prNumberB = PrNumber.Parse("BBB");
            var prNumberC = PrNumber.Parse("CCC");
            
            var ruleSetModel = new RuleSetModel
            {
                RuleSet = new List<RuleModel>
                {
                    new RuleModel{RuleName = "ZA", PrNumbers = "AAA", NestedRuleSet = null}
                }
            };


            var vehicleOffer = await seeder.Add();

            vehicleOffer.AddOptionOffer(prNumberA, Availability.Purchasable(1.5M));
            vehicleOffer.AddOptionOfferRuleSet(prNumberA, "[ZA: BBB+[]]");

            vehicleOffer.AddOptionOffer(prNumberB, Availability.Purchasable(1.5M));
            vehicleOffer.AddOptionOfferRuleSet(prNumberB, "[ZA: CCC+[]]");

            vehicleOffer.AddOptionOffer(prNumberC, Availability.Purchasable(1.5M));

            await seeder.Add<Option, OptionId>(OptionId.Next(vehicleOffer.ModelId, prNumberA));
            await seeder.Add<Option, OptionId>(OptionId.Next(vehicleOffer.ModelId, prNumberB));
            await seeder.Add<Option, OptionId>(OptionId.Next(vehicleOffer.ModelId, prNumberC));

            await seeder.SeedAsync();

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/add/rule-set/{prNumberC}";

            //Act
            var response = await Client.PutCommand(endpoint, ruleSetModel);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task AddRuleSet_WhenVehicleOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/{Fixture.Create<int>()}/option/add/rule-set/{Fixture.Create<PrNumber>()}";

            //Act
            var response = await Client.PutCommand(endpoint, _ruleSetModel);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task AddRuleSet_WhenOptionOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.SeedOption);

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/add/rule-set/{Fixture.Create<PrNumber>()}";

            //Act
            var response = await Client.PutCommand(endpoint, _ruleSetModel);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task AddRuleSetForMany()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.All);

            var command = new AddRuleSetForManyCommand
            {
                CatalogId = vehicleOffer.CatalogId,
                ModelKeySet = ModelKeySet.Parse(vehicleOffer.ModelKey.Value).AsString,
                OptionId = vehicleOffer.Options.First().OptionId.Value,
                RuleSet = _ruleSetModel
            };

            var endpoint = $"{BaseUrl}/option/add/rule-set/for/many";

            //Act
            var response = await Client.PutCommand(endpoint, command);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task RemoveRule()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.All);

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/remove/{_prNumber}";

            //Act
            var response = await Client.DeleteAsync(endpoint);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task RemoveRule_WhenVehicleOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/{Fixture.Create<int>()}/option/remove/rule-set/{Fixture.Create<PrNumber>()}";

            //Act
            var response = await Client.DeleteAsync(endpoint);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task RemoveRule_WhenOptionOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.SeedOption);

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/remove/rule-set/{Fixture.Create<PrNumber>()}";

            //Act
            var response = await Client.DeleteAsync(endpoint);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }


        [Fact]
        public async Task ChangeRelevancePeriod()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.All);

            var optionOffer = vehicleOffer.Options.First();
            var catalog = await DbContext.Catalogs.FindAsync(vehicleOffer.CatalogId);

            var periodModel = new PeriodModel
            {
                From = catalog.RelevancePeriod.From.GetValueOrDefault(),
                To = catalog.RelevancePeriod.To
            };

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/{optionOffer.PrNumber.Value}/change/relevance-period";

            //Act
            var response = await Client.PutCommand(endpoint, periodModel);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ChangeRelevancePeriod_WhenVehicleOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var periodModel = new PeriodModel { From = Fixture.Create<DateTime>() };
            var endpoint = $"{BaseUrl}/{Fixture.Create<int>()}/option/{Fixture.Create<PrNumber>()}/change/relevance-period";

            //Act
            var response = await Client.PutCommand(endpoint, periodModel);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ResetRelevancePeriod()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.All);

            var optionOffer = vehicleOffer.Options.First();
            var catalog = await DbContext.Catalogs.FindAsync(vehicleOffer.CatalogId);

            var relevancePeriod = new RelevancePeriod(catalog.RelevancePeriod);

            optionOffer.ChangeRelevancePeriod(relevancePeriod);

            var endpoint = $"{BaseUrl}/{vehicleOffer.Id}/option/{optionOffer.PrNumber.Value}/reset/relevance-period";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ResetRelevancePeriod_WhenVehicleOffer_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/{Fixture.Create<int>()}/option/{Fixture.Create<PrNumber>()}/reset/relevance-period";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ActualizeRelevanceInCatalog()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.All);
            var endpoint = $"{BaseUrl}/option/relevance/actualize/catalog/{vehicleOffer.CatalogId}";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task ActualizeRelevanceInCatalog_WhenCatalog_IsNotFound_ReturnsBadRequest()
        {
            //Arrange
            var endpoint = $"{BaseUrl}/option/relevance/actualize/catalog/{Fixture.Create<int>()}";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task ActualizeRelevanceInRelevantCatalogs()
        {
            //Arrange
            var vehicleOffer = await SeedVehicleOffer(SeedingOptions.All);
            var catalog = DbContext.Catalogs.Single(c => c.Id == vehicleOffer.CatalogId);
            var date = SystemTimeProvider.Instance.Get();
            catalog.ChangeRelevancePeriod(new Period(date.AddDays(-5)));

            var endpoint = $"{BaseUrl}/option/relevance/actualize";

            //Act
            var response = await Client.PutCommand(endpoint, null);

            //Assert
            response.EnsureSuccessStatusCode();
        }
    }
}
