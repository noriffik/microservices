﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NexCore.VehicleConfigurator.FunctionalTests.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("NexCore.VehicleConfigurator.FunctionalTests.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ModelId;Key;Name
        ///NJ;3;Kurzheck
        ///NJ;5;Combi
        ///NH;1;Spaceback
        ///NH;3;Stufenheck
        ///NU;7;Kurzheck
        ///3V;3;Stufenheck
        ///3V;5;Combi
        ///NS;7;Kurzheck
        ///NS;8;Kurzheck ex NiNo
        ///5E;3;Stufenheck
        ///5E;5;Combi
        ///5E;6;Stufenheck ex NiNo
        ///.
        /// </summary>
        internal static string Bodies {
            get {
                return ResourceManager.GetString("Bodies", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Year;From;To;Status
        ///2019;2019-01-28;;1.
        /// </summary>
        internal static string Catalogs {
            get {
                return ResourceManager.GetString("Catalogs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OptionId;CategoryId
        ///NJ0TD;1
        ///NJ1D7;2
        ///NJ1KT;4
        ///NJ3L3;2
        ///NJ5MQ;1
        ///NJ6E1;2
        ///NJ6E4;2
        ///NJ6E7;2
        ///NJ6K2;4
        ///NJ7Y8;4
        ///NJ7X1;2
        ///NJ7X2;2
        ///NJ8T2;2
        ///NJ8T6;2
        ///NJ8T9;2
        ///NJ8SK;5
        ///NJ9S5;2
        ///NJ9WT;6
        ///NJ9ZV;2
        ///NJEM1;4
        ///NJKA1;2
        ///NJPCC;7
        ///NJPD4;4
        ///NJPD7;4
        ///NJPDB;4
        ///NJPDR;4
        ///NJPDS;4
        ///NJPDT;4
        ///NJPE4;4
        ///NJPE5;4
        ///NJPG5;2
        ///NJPH2;2
        ///NJPHH;2
        ///NJPJ1;8
        ///NJPJA;8
        ///NJPJE;8
        ///NJPJF;8
        ///NJPJI;8
        ///NJPK9;5
        ///NJPKE;4
        ///NJPL1;10
        ///NJPL2;10
        ///NJPL3;10
        ///NJPL8;10
        ///NJPLI;2
        ///NJPNA;6
        ///NJPNB;6
        ///NJPR1;2
        ///NJPR4;2
        ///NJPR6;5
        ///NJPS0;6
        ///NJPSI;11
        ///NJPSJ;11
        ///NJPTY;2        /// [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string CategoryPerOption {
            get {
                return ResourceManager.GetString("CategoryPerOption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ColorId;Argb;TypeId;Name
        ///K4K4;FF2F75B5;UNI;Energy Blau
        ///8T8T;FFFF0000;SPE;Corrida Rot
        ///9P9P;FFFFFFFF;SPE;Candy Weiß.
        /// </summary>
        internal static string Colors {
            get {
                return ResourceManager.GetString("Colors", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ColorTypeId;Name
        ///UNI;Не металік
        ///SPE;Спецлак
        ///MET;Металік
        ///FLT;Fleet.
        /// </summary>
        internal static string ColorTypes {
            get {
                return ResourceManager.GetString("ColorTypes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] ContractTemplateTest {
            get {
                object obj = ResourceManager.GetObject("ContractTemplateTest", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CatalogId;ModelKey;ColorId
        ///1;NJ33M4;K4K4
        ///1;NJ33KC;K4K4
        ///1;NJ34KC;K4K4
        ///1;NJ34N5;K4K4
        ///1;NJ34ND;K4K4
        ///1;NJ53M4;K4K4
        ///1;NJ53KC;K4K4
        ///1;NJ54KC;K4K4.
        /// </summary>
        internal static string DefaultColors {
            get {
                return ResourceManager.GetString("DefaultColors", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ModelId;Key;Name;FuelType
        ///NJ;C;1,0/44 kW MPI;0
        ///NJ;E;1,0/55 kW MPI;0
        ///NJ;F;1,0/60 kW TSI;0
        ///NJ;H;1,6/66 kW MPI;0
        ///NJ;K;1,6/81 kW MPI;0
        ///NJ;M;1,0/70 kW TSI;0
        ///NJ;N;1,0/81kW TSI;0
        ///NH;M;1,0/70 kW TSI;0
        ///NH;N;1,0/81 kW TSI;0
        ///NH;L;1,6/81 kW SRE;0
        ///NH;R;1,4/92 kW TSI;0
        ///NU;B;1,0/85 kW TSI;0
        ///NU;N;1,5/110 kW TSI;0
        ///NU;U;1.4 110 kW TSI;0
        ///NU;P;2,0/140 kW TSI;0
        ///NU;2;1,6/85 kW TDI CR;1
        ///NU;5;2,0/110 kW TDI CR;1
        ///NU;4;2,0/140 kW TDI CR;1
        ///NU;6;2,0/105 kW TDI;1
        ///3V;K;1,4/110 kW TSI (ohne ACT);0
        ///3V;N;1,5/110 kW TSI [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Engines {
            get {
                return ResourceManager.GetString("Engines", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ModelId;Key;Name
        ///NJ;2;Active
        ///NJ;3;Ambition 
        ///NJ;4;Style
        ///NJ;7;Monte Carlo 
        ///NH;2;Active
        ///NH;3;Ambition
        ///NH;4;Style
        ///NU;2;Active 
        ///NU;3;Ambition  
        ///NU;4;Style
        ///NU;S;Scout
        ///NU;R;Sportline
        ///3V;2;Active
        ///3V;3;Ambition
        ///3V;4;Style
        ///3V;5;L&amp;K
        ///3V;S;Scout
        ///3V;R;Sportline 
        ///NS;2;Active
        ///NS;3;Ambition
        ///NS;4;Style
        ///NS;5;L&amp;K  
        ///NS;S;Scout  
        ///NS;6;RS
        ///NS;R;Sportline
        ///5E;2;Active
        ///5E;3;Ambition 
        ///5E;4;Style
        ///5E;5;L&amp;K
        ///5E;6;RS
        ///5E;S;Scout
        ///
        ///.
        /// </summary>
        internal static string Equipments {
            get {
                return ResourceManager.GetString("Equipments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ModelId;Key;Name;Type;Category
        ///NJ;4;5° mech.;5MG;0
        ///NJ;5;6° mech.;6MG;0
        ///NJ;C;6° automat;6AG;2
        ///NJ;D;7° automat;7DSG;2
        ///NH;4;5° mech.;5MG;0
        ///NH;5;6° mech.;6MG;0
        ///NH;C;6° automat;6AG;2
        ///NH;D;7° automat;7DSG;2
        ///NU;5;6° mech.;6MG;0
        ///NU;D;7° automat;7DSG;2
        ///NU;E;8° automat;8DSG;2
        ///NU;Y;6° mech. 4x4;6MG 4X4;1
        ///NU;Z;7° automat 4X4;7DSG 4X4;3
        ///NU;C;6° automat;6MG;2
        ///3V;5;6° mech.;6MG;0
        ///3V;C;6° automat.;6DSG;2
        ///3V;D;7° automat.;7DSG;2
        ///3V;X;6° automat. 4x4 ;6DSG 4X4;3
        ///3V;Y;6° mech. 4x4;6MG 4X4;1
        ///3V;Z;7° automat [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Gearboxes {
            get {
                return ResourceManager.GetString("Gearboxes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Id;Name
        ///NJ;Fabia
        ///NH;Rapid
        ///NU;Karoq
        ///3V;Superb
        ///NS;Kodiaq
        ///5E;Octavia.
        /// </summary>
        internal static string Models {
            get {
                return ResourceManager.GetString("Models", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name
        ///Інтер&apos;єр
        ///Комфорт і обладнання
        ///Комфорт і функціональність
        ///Безпека
        ///Світло
        ///Аудіо та телефон
        ///Екстер&apos;єр
        ///Колеса
        ///Шини та колеса
        ///Кермо
        ///SimplyClever
        ///Інше.
        /// </summary>
        internal static string OptionCategories {
            get {
                return ResourceManager.GetString("OptionCategories", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PrNumber;Type;Reason;Price;ModelKey;CatalogId
        ///219;Included;NationalStandard;;3V32PD;1
        ///220;Included;SerialEquipment;;3V32PD;1
        ///221;Included;SerialEquipment;;3V32PD;1.
        /// </summary>
        internal static string OptionOffers3V {
            get {
                return ResourceManager.GetString("OptionOffers3V", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PrNumber;Type;Reason;Price;ModelKey;CatalogId
        ///116;Included;SerialEquipment;;5E32H4;1
        ///117;Included;SerialEquipment;;5E32H4;1.
        /// </summary>
        internal static string OptionOffers5E {
            get {
                return ResourceManager.GetString("OptionOffers5E", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PrNumber;Type;Reason;Price;ModelKey;CatalogId
        ///063;Included;SerialEquipment;;NH32M4;1
        ///064;Included;SerialEquipment;;NH32M4;1
        ///065;Included;SerialEquipment;;NH32M4;1.
        /// </summary>
        internal static string OptionOffersNH {
            get {
                return ResourceManager.GetString("OptionOffersNH", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PrNumber;Type;Reason;Price;ModelKey;CatalogId
        ///000;Included;SerialEquipment;;NJ33M4;1
        ///001;Included;SerialEquipment;;NJ33M4;1
        ///002;Included;SerialEquipment;;NJ33M4;1.
        /// </summary>
        internal static string OptionOffersNJ {
            get {
                return ResourceManager.GetString("OptionOffersNJ", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PrNumber;Type;Reason;Price;ModelKey;CatalogId
        ///351;Included;SerialEquipment;;NS73KY;1
        ///352;Included;SerialEquipment;;NS73KY;1
        ///353;Included;SerialEquipment;;NS73KY;1.
        /// </summary>
        internal static string OptionOffersNS {
            get {
                return ResourceManager.GetString("OptionOffersNS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PrNumber;Type;Reason;Price;ModelKey;CatalogId
        ///305;Included;SerialEquipment;;NU73ND;1
        ///307;Included;SerialEquipment;;NU73ND;1
        ///308;Included;SerialEquipment;;NU73ND;1
        ///309;Included;SerialEquipment;;NU73ND;1
        ///310;Included;SerialEquipment;;NU73ND;1.
        /// </summary>
        internal static string OptionOffersNU {
            get {
                return ResourceManager.GetString("OptionOffersNU", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OptionId;Name;CategoryId
        ///NJ000;&quot;Галогенові фари H4 спереду з функцією денного світла &quot;&quot;DAY LIGHT&quot;&quot;&quot;;1
        ///NJ001;Показники повороту інтегровані у зовнішні дзеркала заднього виду;2
        ///NJ002;Бампери пофарбовані у колір кузова;3
        ///NJ003;Автоматичні аварійні вогні у випадку аварії;4
        ///NJ004;Електронная система блокуванння міжколісного диференціала XDS;
        ///NSEA7;Подовження гарантії 4 роки/90 000 км;
        ///NSEA2;Подовження гарантії 4 роки/120 000 км;5
        ///NSUNI;Не металік (K4K4);
        ///NSSPE;&quot;Спецлак (9P9P, M3M3)&quot;;7
        ///NSMET;Mеталік;7
        /// [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Options {
            get {
                return ResourceManager.GetString("Options", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ModelKeySet;Id;Price;Applicability;Name;CatalogId
        ///NJ33M4 NJ33KC NJ53M4 NJ53KC;NJWU1;541;9S5 PDB PL2 RAB;Ambition;1
        ///NJ34KC NJ34N5 NJ34ND NJ54KC NJ54N5 NJ54ND;NJWU2;468;9S5 PHH PL2 9ZV;Style;1
        ///5E33H4 5E33HC 5E33M5 5E33MD 5E33PD 5E338D 5E338Z 5E3385 5E339Z 5E53H4 5E53HC 5E53M5 5E53MD 5E53PD 5E538D 5E538Z 5E5385 5E539Z;5EWUN;770;PJ0/PJ1 PLU RAC PT3 9WT 8SP PK2;Ambition;1
        ///5E32H4 5E32HC 5E32M5 5E52H4 5E52HC 5E52M5;5EWUM;269;PT2 PW1 RA2;Active;1
        ///5E33H4 5E33HC 5E33M5 5E33MD 5E33PD 5E338D 5E338Z 5E3385 5E339Z 5 [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Packages {
            get {
                return ResourceManager.GetString("Packages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CatalogId;ModelKey;Price;
        ///1;NJ33M4;14474;.
        /// </summary>
        internal static string VehicleOffers {
            get {
                return ResourceManager.GetString("VehicleOffers", resourceCulture);
            }
        }
    }
}
