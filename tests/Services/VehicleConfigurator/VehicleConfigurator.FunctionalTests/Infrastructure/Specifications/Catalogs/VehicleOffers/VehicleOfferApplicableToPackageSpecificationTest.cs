﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Infrastructure.Specifications.Catalogs.VehicleOffers
{
    public class VehicleOfferApplicableToPackageSpecificationTest : SpecificationFixture
    {
        public VehicleOfferApplicableToPackageSpecificationTest() : base(nameof(VehicleOfferApplicableToPackageSpecification))
        {
        }

        [Fact]
        public async Task Find()
        {
            using (var context = CreateContext())
            {
                //Arrange
                var seeder = new VehicleOfferSeeder(context);
            
                //Seed catalog
                var catalog = await seeder.Add<Catalog>(1);

                //Seed package
                var package = await seeder.Add<Package, PackageId>(new {catalogId = catalog.Id});

                //Seed package's options
                var prNumbers = package.Applicability.SelectMany(s => s.PrNumbers.Values).ToList();
                prNumbers.ForEach(prNumber => seeder.Add<Option, OptionId>());

                //Reset option's category id
                var options = seeder.Get<Option, OptionId>().ToList();
                options.ForEach(o => o.OptionCategoryId = null);
            
                //Seed vehicle offer with option offers
                var vehicleOffer = await seeder.Add(new {modelKey = package.ModelKeySet.Values.First(), catalogId = catalog.Id});
                foreach (var prNumber in package.Applicability.PrNumbers.Values)
                {
                    vehicleOffer.AddOptionOffer(prNumber, seeder.Create<Availability>());
                }
            
                var expected = new[] {vehicleOffer};

                //Seed unexpected vehicle offers
                await seeder.Add(new {modelKey = package.ModelKeySet.Values.Last(), catalogId = catalog.Id});
            
                var specification = new VehicleOfferApplicableToPackageSpecification(package.Applicability);

                await seeder.SeedAsync();

                //Act
                var results = await context.EntityRepository.Find(specification, CancellationToken.None);

                Assert.Equal(expected, results);
            }
        }
    }
}
