﻿using NexCore.Domain.Vehicles;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.VehicleOffers;
using NexCore.VehicleConfigurator.Domain.Vehicles;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Infrastructure.Specifications.Catalogs.VehicleOffers
{
    public class VehicleOfferOptionIdSpecificationTest : SpecificationFixture
    {
        public VehicleOfferOptionIdSpecificationTest() : base(nameof(VehicleOfferOptionIdSpecification))
        {
        }

        [Fact]
        public async Task Find()
        {
            using (var context = CreateContext())
            {
                //Arrange
                var seeder = new VehicleOfferSeeder(context);

                var componentIds = new[] { "1234", "12qw", "qw89" };

                //Seed unexpected VehicleOffers
                var unexpectedModelId = seeder.Create<ModelId>();
                var unexpectedModelKeys = componentIds.Select(ids => ModelKey.Parse(unexpectedModelId.Value + ids));
                foreach (var modelKey in unexpectedModelKeys)
                {
                    var vehicleOffer = await seeder.Add(new { modelKey });
                    var optionId = OptionId.Next(vehicleOffer.ModelId, seeder.Create<PrNumber>());
                    await seeder.Add<Option, OptionId>(optionId);

                    vehicleOffer.AddOptionOffer(optionId.PrNumber, seeder.Create<Availability>());
                }

                //Seed expected VehicleOffers
                var expectedModelId = seeder.Create<ModelId>();
                var expectedModelKeys = componentIds.Select(ids => ModelKey.Parse(expectedModelId.Value + ids));

                var expectedOptionId = OptionId.Next(expectedModelId, seeder.Create<PrNumber>());
                await seeder.Add<Option, OptionId>(expectedOptionId);

                var expected = new List<VehicleOffer>();
                foreach (var modelKey in expectedModelKeys)
                {
                    var vehicleOffer = await seeder.Add(new { modelKey });
                    expected.Add(vehicleOffer);

                    vehicleOffer.AddOptionOffer(expectedOptionId.PrNumber, seeder.Create<Availability>());
                }

                var specification = new VehicleOfferOptionIdSpecification(expectedOptionId);

                await seeder.SeedAsync();

                //Act
                var results = await context.EntityRepository.Find(specification, CancellationToken.None);

                //Assert
                Assert.Equal(expected, results);
            }
        }
    }
}
