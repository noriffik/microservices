﻿using NexCore.Domain;
using NexCore.VehicleConfigurator.Domain.Catalogs;
using NexCore.VehicleConfigurator.Domain.Catalogs.Specifications.Catalogs;
using NexCore.VehicleConfigurator.FunctionalTests.Fixtures;
using NexCore.VehicleConfigurator.UnitTests.Fixtures.Infrastructure.Seeders;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace NexCore.VehicleConfigurator.FunctionalTests.Infrastructure.Specifications.Catalogs.Catalogs
{
    public class CatalogOverlappedPeriodAndModelYearSpecificationTest : SpecificationFixture
    {
        public CatalogOverlappedPeriodAndModelYearSpecificationTest()
            : base(nameof(CatalogOverlappedPeriodAndModelYearSpecification))
        {
        }

        [Fact]
        public async Task Find()
        {
            using (var context = CreateContext())
            {
                //Arrange
                var seeder = new CatalogSeeder(context);

                var from = seeder.Create<DateTime>();
                var to = from.AddDays(50);
                var modelYear = seeder.Create<Year>();

                //Seed expected catalogs
                var expected = new List<Catalog>
                {
                    await seeder.Add(new {modelYear, period = new Period(@from.AddDays(-20), to.AddDays(-20))}),
                    await seeder.Add(new {modelYear, period = new Period(@from.AddDays(20), to.AddDays(20))}),
                    await seeder.Add(new {modelYear, period = new Period(@from.AddDays(20), to.AddDays(-20))}),
                    await seeder.Add(new {modelYear, period = new Period(@from.AddDays(-20), to.AddDays(20))})
                };

                //Seed nonOverlapped published catalogs
                await seeder.Add(new { modelYear, period = new Period(from.AddDays(-80), to.AddDays(-60)) });
                await seeder.Add(new { modelYear, period = new Period(from.AddDays(80), to.AddDays(80)) });

                //Seed overlapped catalogs with another model year
                await seeder.Add(new
                {
                    modelYear = new Year(modelYear.Value + 1),
                    period = new Period(from.AddDays(20), to.AddDays(20))
                });
                await seeder.Add(new
                {
                    modelYear = new Year(modelYear.Value + 2),
                    period = new Period(from.AddDays(20), to.AddDays(-20))
                });

                var specification = new CatalogOverlappedPeriodAndModelYearSpecification(modelYear, new Period(from, to));

                await seeder.SeedAsync();

                //Act
                var results = await context.EntityRepository.Find(specification, CancellationToken.None);

                //Assert
                Assert.Equal(expected, results);
            }
        }
    }
}
